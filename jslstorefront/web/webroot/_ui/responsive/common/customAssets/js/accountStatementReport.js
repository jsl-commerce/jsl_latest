window.onload = function() {
	$
			.ajax({
				type : "GET",
				url : ACC.config.encodedContextPath
						+ "/reports/reportsJson?report=ACCOUNTSTATEMENTREPORT&CSRFToken="
						+ ACC.config.CSRFToken,
				success : function(response) {
					createTable(response);
					postTableCreation();
				},
				error : function(response) {
					$("#successDataCheck").hide();
					$('#loading-wrapper').hide();
					$("#failureDataCheck").show();
				}
			});
}

function createTable(json) {
	$("#customerId").text(json.customerId);
	$("#customerName").text(json.customerName);
	var table = document.getElementById("tableBody");
	json.accountStatements.forEach(function(item, index) {
		var row = table.insertRow(index);
		row.id = "row_" + index;

		var cell0 = row.insertCell(0);
		cell0.id = "cmpnyCode_" + index;
		if (item.companycode == 'JSHL') {
			cell0.innerText = 'Jindal Stainless (Hisar) Limited';
		}
		if (item.companycode == 'JSL') {
			cell0.innerText = 'Jindal Stainless Limited';
		}
		var cell1 = row.insertCell(1);
		cell1.id = "postDate_" + index;
		cell1.innerText = item.postDate;
		var cell2 = row.insertCell(2);
		cell2.id = "documentNo_" + index;
		cell2.innerText = item.documentNumber;
		var cell3 = row.insertCell(3);
		cell3.id = "documentDescription_" + index;
		cell3.innerText = item.documentDescription;
		var cell4 = row.insertCell(4);
		cell4.id = "refInstNumber_" + index;
		cell4.innerText = item.refInstNumber;
		var cell5 = row.insertCell(5);
		cell5.id = "reference_" + index;
		cell5.innerText = item.reference;
		var cell6 = row.insertCell(6);
		cell6.id = "spl_" + index;
		cell6.innerText = item.spl;
		var cell7 = row.insertCell(7);
		cell7.id = "itemDescription_" + index;
		cell7.innerText = item.itemDescription;
		var cell8 = row.insertCell(8);
		cell8.id = "dueDate_" + index;
		cell8.innerText = item.dueDate;
		var cell9 = row.insertCell(9);
		cell9.id = "amount_" + index;
		cell9.innerText = item.amount;
		var cell10 = row.insertCell(10);
		cell10.id = "currency_" + index;
		cell10.innerText = item.currency;
		var cell11 = row.insertCell(11);
		cell11.id = "debitAmount_" + index;
		cell11.innerText = item.debitAmount;
		var cell12 = row.insertCell(12);
		cell12.id = "creditAmount_" + index;
		cell12.innerText = item.creditAmount;
	});
}

function postTableCreation() {
	$("#loading-wrapper").hide();
	var table = document.getElementById('tableBody');
	var totalDebit = 0;
	var totalCredit = 0;
	for (var i = 0; i < table.rows.length - 1; i++) {
		totalDebit = totalDebit + parseFloat($('#debitAmount_' + i).text());
		totalCredit = totalCredit + parseFloat($('#creditAmount_' + i).text());
	}
	$('#totalDebit').text(totalDebit.toFixed(2));
	$('#totalCredit').text(totalCredit.toFixed(2));
}

function filterData(flag) {
	if ($('#aStartDate').val() != "" && $('#aEndDate').val() != "") {
		var frm = new Date($('#aStartDate').val()+' 00:00:00'); 
		var to = new Date($('#aEndDate').val()+' 23:59:59');
		if (frm > to) {
			alert("start date can not be greater than end date");
			$('#aStartDate').val("");
			$('#aEndDate').val("");
		} else {
			var slectedCompany = $('#compny_data').val();
			var table = document.getElementById('tableBody');
			var totalDebit = 0;
			var totalCredit = 0;
			for (var i = 0; i < table.rows.length - 1; i++) {
				var date = new Date($('#postDate_' + i).text());
				if (date <= to && date >= frm) {
					var companyCodeForRow = $("#cmpnyCode_" + i).text();
					if ('jshl' == slectedCompany) {
						if ('Jindal Stainless (Hisar) Limited' == companyCodeForRow) {
							$('#row_' + i).show();
							totalDebit = totalDebit
									+ parseFloat($('#debitAmount_' + i).text());
							totalCredit = totalCredit
									+ parseFloat($('#creditAmount_' + i).text());
						} else {
							$('#row_' + i).hide();
						}
					} else if ('jsl' == slectedCompany) {
						if ('Jindal Stainless Limited' == companyCodeForRow) {
							$('#row_' + i).show();
							totalDebit = totalDebit
									+ parseFloat($('#debitAmount_' + i).text());
							totalCredit = totalCredit
									+ parseFloat($('#creditAmount_' + i).text());
						} else {
							$('#row_' + i).hide();
						}
					} else {
						$('#row_' + i).show();
						totalDebit = totalDebit
								+ parseFloat($('#debitAmount_' + i).text());
						totalCredit = totalCredit
								+ parseFloat($('#creditAmount_' + i).text());
					}

				} else {
					$('#row_' + i).hide();
				}
			}

			$('#totalDebit').text(totalDebit.toFixed(2));
			$('#totalCredit').text(totalCredit.toFixed(2));
		}
	} else if('button' == flag) {
		alert("kindly enter the date range");
	}else if('drop' == flag){
		filterDataForCompany();
	}

}

function filterDataForCompany() {
	var slectedCompany = $('#compny_data').val();
	var table = document.getElementById('tableBody');
	var totalDebit = 0;
	var totalCredit = 0;
	for (var i = 0; i < table.rows.length - 1; i++) {
		var companyCodeForRow = $("#cmpnyCode_" + i).text();
		if ('jshl' == slectedCompany) {
			if ('Jindal Stainless (Hisar) Limited' == companyCodeForRow) {
				$('#row_' + i).show();
				totalDebit = totalDebit
				+ parseFloat($('#debitAmount_' + i).text());
		totalCredit = totalCredit
				+ parseFloat($('#creditAmount_' + i).text());
			} else {
				$('#row_' + i).hide();
			}
		} else if ('jsl' == slectedCompany) {
			if ('Jindal Stainless Limited' == companyCodeForRow) {
				$('#row_' + i).show();
				totalDebit = totalDebit
				+ parseFloat($('#debitAmount_' + i).text());
		totalCredit = totalCredit
				+ parseFloat($('#creditAmount_' + i).text());
			} else {
				$('#row_' + i).hide();
			}
		} else {
			$('#row_' + i).show();
			totalDebit = totalDebit
			+ parseFloat($('#debitAmount_' + i).text());
	totalCredit = totalCredit
			+ parseFloat($('#creditAmount_' + i).text());
		}
	}
	$('#totalDebit').text(totalDebit.toFixed(2));
	$('#totalCredit').text(totalCredit.toFixed(2));
}
function resetData() {
	$('#aStartDate').val("");
	$('#aEndDate').val("");
	filterDataForCompany();
}
