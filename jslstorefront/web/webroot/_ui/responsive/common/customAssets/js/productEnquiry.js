var productList;
var enquiryList = [];
var extendedProduct = [];
   window.onload = function(){
	   $("#loading-wrapper").hide();
	   var arr = [];
	   $.ajax({
		   type: "GET",
		   url: ACC.config.encodedContextPath +"/enquiry/getProductsJson",
		   success: function(response){
			   productList = JSON.parse(response);
			   
			   if(productList[0].typeCode == "Flat"){
				   var prodTypeSelect = document.getElementById("prodTypeSel");
				   productList[0].productType.forEach(function(item){
					   arr.push(item.productCode);
				   });
				   arr.sort();
				   arr.forEach(function(item){
					   prodTypeSelect.options.add(new Option(item)); 
				   });
			   }
		   },
	   	   error: function(response){
	   	   }
	   });
   }
   
   function resetDropDowns(id){
	   switch(id){
	   case "prodTypeSel":
		   document.getElementById("seriesSel").innerHTML = "<option value='' selected>Select</option>";
		  			   
	   case "seriesSel":
		   document.getElementById("gradeSel").innerHTML = "<option value='' selected>Select</option>";
								   
	   case "gradeSel":
		   document.getElementById("finishSel").innerHTML = "<option value='' selected>Select</option>";
							   
	   case "finishSel":
		   document.getElementById("thkSel").innerHTML = "<option value='' selected>Select</option>";
		   $("#thkInput").hide();
			
	   case "thkSel":
		   document.getElementById("wdthSel").innerHTML = "<option value='' selected>Select</option>";
		   $("#wdthInput").hide();
					   
	   case "wdthSel":
		   document.getElementById("lenSel").innerHTML = "<option value='' selected>Select</option>";
		   $("#lenInput").hide();
	   }						   
   }
   
   var isProductCoilType = false;
   function populateNext(id){
	   resetDropDowns(id);
	   if($("#prodTypeSel").val().includes('Coil')){
		   document.getElementById("lenSel").disabled = true;
		   $("#lenSel").addClass("disabledLength");
		   document.getElementById("lenSel").innerHTML = "<option value='' selected>N/A</option>";
		   isProductCoilType = true;
	   }else{
		   isProductCoilType = false;
		   document.getElementById("lenSel").disabled = false;
		   $("#lenSel").removeClass("disabledLength");
	   }
	   
	   var arr = []; 
	   switch(id){
	   case "prodTypeSel":
		   var seriesSel = document.getElementById("seriesSel");
		   productList[0].productType.forEach(function(item){
			   if(item.productCode == $("#prodTypeSel").val()){
				   item.series.forEach(function(item){
					   arr.push(item.seriesCode);
				   });
			   }
		   });
		   arr.sort().forEach(function(item){
			   seriesSel.options.add(new Option(item, item));
		   });
		break;
		
	   case "seriesSel":
		   var gradeSel = document.getElementById("gradeSel");
		   productList[0].productType.forEach(function(item){
			   if(item.productCode == $("#prodTypeSel").val()){
				   item.series.forEach(function(item){
					   if(item.seriesCode == $("#seriesSel").val()){
						   item.grades.forEach(function(item){
							   arr.push(item.gradeCode);
						   });
					   }
				   });
			   }
		   });
		   arr.sort().forEach(function(item){
			   gradeSel.options.add(new Option(item, item));
		   });
		break;
		
	   case "gradeSel":
		   var finishSel = document.getElementById("finishSel");
		   productList[0].productType.forEach(function(item){
			   if(item.productCode == $("#prodTypeSel").val()){
				   item.series.forEach(function(item){
					   if(item.seriesCode == $("#seriesSel").val()){
						   item.grades.forEach(function(item){
						   		if(item.gradeCode == $("#gradeSel").val()){
						   			item.finish.forEach(function(item){
						   				arr.push(item.finishCode);
						   			});	
						   		}
						   });
					   }
		   			});
			   }
		   });
		   arr.sort().forEach(function(item){
			   finishSel.options.add(new Option(item, item));
		   });
		break;
		
	   case "finishSel":
		   var thkSel = document.getElementById("thkSel");
		   productList[0].productType.forEach(function(item){
			   if(item.productCode == $("#prodTypeSel").val()){
				   item.series.forEach(function(item){
					   if(item.seriesCode == $("#seriesSel").val()){
						   item.grades.forEach(function(item){
						   		if(item.gradeCode == $("#gradeSel").val()){
						   			item.finish.forEach(function(item){
						   				if(item.finishCode == $("#finishSel").val()){
						   					item.thickness.forEach(function(item){
						   						arr.push(item.thicknessValue);
						   					});
						   				}
						   			});	
						   		}
						   });
					   }
		   			});
			   }
		   });
		   arr.sort().forEach(function(item){
			   thkSel.options.add(new Option(item, item));
		   });
		break;
		
	   case "thkSel":
		   if($("#thkSel").val() == "others"){
			   $("#thkInput").show();
		   }else{
			   $("#thkInput").hide();
		   }
		   var wdthSel = document.getElementById("wdthSel");
		   productList[0].productType.forEach(function(item){
			   if(item.productCode == $("#prodTypeSel").val()){
				   item.series.forEach(function(item){
					   if(item.seriesCode == $("#seriesSel").val()){
						   item.grades.forEach(function(item){
						   		if(item.gradeCode == $("#gradeSel").val()){
						   			item.finish.forEach(function(item){
						   				if(item.finishCode == $("#finishSel").val()){
						   					item.thickness.forEach(function(item){
						   						if(item.thicknessValue == $("#thkSel").val()){
						   							item.width.forEach(function(item){
						   								arr.push(item.widthValue);
						   							});
						   						}
						   					});
						   				}
						   			});	
						   		}
						   });
					   }
		   			});
			   }
		   });
		   arr.sort().forEach(function(item){
			   wdthSel.options.add(new Option(item, item));
		   });
		break;
		
	   case "wdthSel":
		   if($("#wdthSel").val() == "others"){
			   $("#wdthInput").show();
		   }else{
			   $("#wdthInput").hide();
		   }
		   var lenSel = document.getElementById("lenSel");
		   productList[0].productType.forEach(function(item){
			   if(item.productCode == $("#prodTypeSel").val()){
				   item.series.forEach(function(item){
					   if(item.seriesCode == $("#seriesSel").val()){
						   item.grades.forEach(function(item){
						   		if(item.gradeCode == $("#gradeSel").val()){
						   			item.finish.forEach(function(item){
						   				if(item.finishCode == $("#finishSel").val()){
						   					item.thickness.forEach(function(item){
						   						if(item.thicknessValue == $("#thkSel").val()){
						   							item.width.forEach(function(item){
						   								if(item.widthValue == $("#wdthSel").val()){
						   									item.length.forEach(function(item){
						   										arr.push(item);
						   									});
						   								}
						   							});
						   						}
						   					});
						   				}
						   			});	
						   		}
						   });
					   }
		   			});
			   }
		   });
		   arr.sort().forEach(function(item){
			   lenSel.options.add(new Option(item, item));
		   });
		break;
		
	   case "lenSel":
		   if($("#lenSel").val() == "others"){
			   $("#lenInput").show();
		   }else{
			   $("#lenInput").hide();
		   }
		break;		   
	   default: console.log("something went wrong");
	   }
   }
   
   function clearSelections(){
	   $("#prodTypeSel")[0].selectedIndex = $("#edgeSel")[0].selectedIndex = $("#ilpSel")[0].selectedIndex = $("#pvcSel")[0].selectedIndex = 
		   $("#inspSel")[0].selectedIndex = 0; 
	   $('#comments').val("");
	   $("#qty").val("");
	   $("#thkInput").val("");
	   $("#wdthInput").val("");
	   $("#lenInput").val("");
	   resetDropDowns("prodTypeSel");
   }
   
   function populateTable(){
	   var table = document.getElementById("productTable");
	   table.innerHTML = "";
	   if(!$.isEmptyObject(extendedProduct)){
		   extendedProduct.forEach(function(index, i){
			   var row = table.insertRow(i);
			   var cell0 = row.insertCell(0);
			   cell0.innerHTML = i+1;
			   var cell1 = row.insertCell(1);
			   cell1.innerHTML = index.productType.name;
			   var cell2 = row.insertCell(2);
			   cell2.innerHTML = index.series.name;
			   var cell3 = row.insertCell(3);
			   cell3.innerHTML = index.grade.name;
			   var cell4 = row.insertCell(4);
			   cell4.innerHTML = index.finish.name;
			   var cell5 = row.insertCell(5);
			   cell5.innerHTML = index.thickness.name;
			   var cell6 = row.insertCell(6);
			   cell6.innerHTML = index.width.name;
			   var cell7 = row.insertCell(7);
			   cell7.innerHTML = index.length.name
			   var cell8 = row.insertCell(8);
			   cell8.innerHTML = index.edgeCondition.name;
			   var cell9 = row.insertCell(9);
			   cell9.innerHTML = index.ilp.name;
			   var cell10 = row.insertCell(10);
			   cell10.innerHTML = index.pvc.name;
			   var cell11 = row.insertCell(11);
			   cell11.innerHTML = index.inspection.name;
			   var cell12 = row.insertCell(12);
			   cell12.innerHTML = index.quantity.name;
			   
			   var deletebtn = document.createElement("i");
		       deletebtn.classList.add("delButton");
			   deletebtn.onclick = function(arg){return function(){removeProduct(arg)} }(i);
			   deletebtn.innerHTML = "<i class='fa fa-trash' title='Remove Product'></i>";
			   var cell13 = row.insertCell(13);
			   cell13.appendChild(deletebtn);
			   
		   });
	   }else{
		   table.innerHTML = "<tr>" + "<td colspan='14'><span><center>No Products Yet</center></span></td>" + "</tr>";
	   }
   }
   
   function removeProduct(i){
	   enquiryList.splice(i,1);
	   extendedProduct.splice(i,1);
	   populateTable();
   }
   
   function extendedProductPopulator(){
	   var extProduct = new Object();
	   extProduct.productType = {
			   name: $("#prodTypeSel :selected").text(),
			   value: $("#prodTypeSel :selected").val()
	   }
	   extProduct.series = {
			   name: $("#seriesSel :selected").text(),
			   value: $("#seriesSel :selected").val()
	   }
	   extProduct.grade = {
			   name: $("#gradeSel :selected").text(),
			   value: $("#gradeSel :selected").val()
	   }
	   extProduct.finish = {
			   name: $("#finishSel :selected").text(),
			   value: $("#finishSel :selected").val()
	   }
	   
	   if($("#thkSel").val() != "others"){
		   extProduct.thickness = {
				   name: $("#thkSel :selected").text(),
				   value: $("#thkSel :selected").val()
		   }
	   }else{
		   extProduct.thickness = {
				   name: $("#thkInput").val(),
				   value: $("#thkInput").val()
		   }
	   }
	   
	   if($("#wdthSel").val() != "others"){
		   extProduct.width = {
				   name: $("#wdthSel :selected").text(),
				   value: $("#wdthSel :selected").val()
		   }
	   }else{
		   extProduct.width = {
				   name: $("#wdthInput").val(),
				   value: $("#wdthInput").val()
		   }
	   }
	   
	   if($("#lenSel").val() != "others" && isProductCoilType == false){
		   extProduct.length = {
				   name: $("#lenSel :selected").text(),
				   value: $("#lenSel :selected").val()
		   }
	   }
	   else if($("#lenSel").val() == "" && isProductCoilType == true){
		   extProduct.length = {
				   name: "N/A",
				   value: "0"
		   }
	   }
	   else{
		   extProduct.length = {
				   name: $("#lenInput").val(),
				   value: $("#lenInput").val()
		   }
	   }
	   
	   extProduct.edgeCondition = {
			   name: $("#edgeSel :selected").text(),
			   value: $("#edgeSel :selected").val()
	   }
	   extProduct.ilp = {
			   name: $("#ilpSel :selected").text(),
			   value: $("#ilpSel :selected").val()
	   }
	   extProduct.pvc = {
			   name: $("#pvcSel :selected").text(),
			   value: $("#pvcSel :selected").val()
	   }
	   extProduct.inspection = {
			   name: $("#inspSel :selected").text(),
			   value: $("#inspSel :selected").val()
	   }
	   extProduct.quantity = {
			   name: $("#qty").val(),
			   value: $("#qty").val()
	   }
	   extProduct.specialRemarks = {
			   name: "",
			   value: ""
	   }
	   extendedProduct.push(extProduct);
   }
   
   function addProduct(){
	   var product = new Object();
	   product.productType = $("#prodTypeSel").val();
	   product.series = $("#seriesSel").val();
	   product.grade = $("#gradeSel").val();
	   product.finish = $("#finishSel").val();
	   
	   if($("#thkSel").val() != "others"){
	   		product.thickness = $("#thkSel").val();
	   }else{
			product.thickness = $("#thkInput").val();
	   }
	   
	   if($("#wdthSel").val() != "others"){
		   product.width = $("#wdthSel").val();   
	   }else{
		   product.width = $("#wdthInput").val();
	   }
	   
	   if($("#lenSel").val() != "others" && isProductCoilType == false){
		   product.length = $("#lenSel").val();
	   }
	   else if($("#lenSel").val() == "" && isProductCoilType == true){
		   product.length = "0" ;
	   }
	   else{
		   product.length = $("#lenInput").val();
	   }
	   
	   product.edgeCondition = $("#edgeSel").val();
	   product.ilp = $("#ilpSel").val();
	   product.pvc = $("#pvcSel").val();
	   product.inspection = $("#inspSel").val();
	   product.quantity = $("#qty").val();
	   product.specialRemarks = "";
	   if(product.productType != "" && product.series != "" && product.grade != "" && product.finish != "" && product.thickness != ""
		   && product.width != "" && product.length != "" && product.edgeCondition != "" && product.ilp != "" && product.pvc != ""
			   && product.inspection != "" && product.quantity != ""){
		   if(Number(product.thickness) >= 0 && Number(product.width) >= 0 && Number(product.length) >= 0 && Number(product.quantity) > 0){
			   enquiryList.push(product);
			   extendedProductPopulator();
//			   clearSelections();
			   populateTable();
			   isProductCoilType = false;
		   }else if(Number(product.quantity) == 0){
			   var msg = "Quantity can't be zero!";
			   popUp(msg, 3000, "red");
		   }else{
			   var msg = "Kindly Enter only positive values";
			   popUp(msg, 3000, "red");
		   }
	   }else{
		   var msg = "Kindly enter all the values";
		   popUp(msg, 3000, "red");
	   }
   }
   
   function sendList(){
	   if(!$.isEmptyObject(enquiryList)){
		   $("#loading-wrapper").show();
		   var data = {
				   "enquiryEntries": enquiryList,
				   "comments": $("#comments").val(),
				   "user": "null"
		   }
		   	enquiryList = [];
			extendedProduct = [];
			clearSelections();
			populateTable();
		   $.ajax({
			   type: "POST",
			   url: ACC.config.encodedContextPath + "/enquiry/sendProductsList",
			   data: JSON.stringify(data),
			   contentType:   "application/json; charset=utf-8",
			   dataType: "json",
			   success: function(response){
				   $("#loading-wrapper").hide();
				 var msg = "EnquiryId: " + response.enquiryId  +" successfully submitted. For further queries please contact: order.services@jindalstainless.com";
				 showModal(msg);
				 isProductCoilType = false;
			   },
			   error: function(response){
				   $("#loading-wrapper").hide();
				   var msg = "Something went wrong! Enquiry Creation Failed";
				   popUp(msg, 3000, "red");
			   }
		   });
	   }
	   else{
		   var msg = "Please Enter at least one product";
		   popUp(msg, 3000, "red");
	   }
   }
   
   function popUp(msg, duration, color){
	   var snackbar = document.getElementById("snackBar");
   	   snackbar.innerHTML = msg;
       snackbar.style.backgroundColor = color;
       snackbar.className = "show";
       setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, duration);
   }
   
   function showModal(msg){
	   document.getElementById("modalMessage").innerHTML = msg;
	   $("#myModal").show();
   }
   
   function hideModal(){
	   $("#myModal").hide();
   }