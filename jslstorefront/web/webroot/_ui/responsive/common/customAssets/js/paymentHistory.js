var today = new Date();
var threeMonthsAgo = new Date(new Date().setDate(today.getDate() - 90));
$("#aStartDate").datepicker({
	format: 'mm/dd/yyyy',
	autoclose: true
});

$("#startDateIcon").on('click', function () {
	$("#aStartDate").show().focus();
});


$('#aEndDate').datepicker({
	format: 'mm/dd/yyyy',
	autoclose: true
});

$("#endDateIcon").on('click', function () {
	$("#aEndDate").show().focus();
});

$('.searchByParameters input[type="radio"]').change(function () {

	if (this.value == 'Search By Payment Date') {
		$("#aStartDate").val("");
		$("#aEndDate").val("");
		document.getElementById("aStartDate").disabled = false;
		document.getElementById("aEndDate").disabled = false;
		document.getElementById("dateButtonId").disabled = false;
		document.getElementById("resetFilterDateButtonId").disabled = false;
		document.getElementById("noInvoiceDataFound").style.display = "none";

		var table = document.getElementById("paymentHistoryMainTable");
		for (var i = 0; i < table.rows.length - 1; i++) {
			var toggleTest = $("#accordion_service_" + i).is(":hidden");
			if (toggleTest == false) {
				toggleAccordion('#accordion_service_' + i);
			}
			if ($("#cmpnyCode").val() == $("#cmpnyCode_" + i).text()) {
				$("#row_" + i).show();
			} else {
				$("#row_" + i).hide();
			}
		}
	}
	if (this.value == 'all') {
		$("#aStartDate").val("");
		$("#aEndDate").val("");
		document.getElementById("aStartDate").disabled = true;
		document.getElementById("aEndDate").disabled = true;
		document.getElementById("dateButtonId").disabled = true;
		document.getElementById("resetFilterDateButtonId").disabled = true;
		document.getElementById("noInvoiceDataFound").style.display = "none";

		var table = document.getElementById("paymentHistoryMainTable");
		for (var i = 0; i < table.rows.length - 1; i++) {
			var toggleTest = $("#accordion_service_" + i).is(":hidden");
			if (toggleTest == false) {
				toggleAccordion('#accordion_service_' + i);
			}
			if ($("#cmpnyCode").val() == $("#cmpnyCode_" + i).text()) {
				$("#row_" + i).show();
			} else {
				$("#row_" + i).hide();
			}
		}
	}
});

function checkpaymentTypeSelected() {
	document.getElementById("showPaymentHistoryPageId").style.display = "block";
	document.getElementById("cmpnyCode").options[0].disabled = true;

	resetFilterDate();
	if ($("#cmpnyCode").val() !== 'all') {
		var table = document.getElementById("paymentHistoryMainTable");
		for (var i = 0; i < table.rows.length - 1; i++) {
			var toggleTest = $("#accordion_service_" + i).is(":hidden");
			if (toggleTest == false) {
				toggleAccordion('#accordion_service_' + i);
			}
			if ($("#cmpnyCode").val() == $("#cmpnyCode_" + i).text()) {
				$("#row_" + i).show();
				if (($("#status_" + i).text()).toLowerCase() == "pending" && $("#pendingPaymentDate_" + i).text() != null && $("#paymentMode_" + i).text() != 'B') {
					var sysTime = new Date();
					var paymentDate = new Date($("#pendingPaymentDate_" + i).text().replace("IST", ""));
					if ((sysTime - paymentDate) > 172800000) {
						document.getElementById("reInitiatePayment_" + i).style.display = "block";
					}
				}
			} else {
				$("#row_" + i).hide();
			}
		}
	}

}

function toggleAccordion(id) {
	$(id).toggle();
}

function searchByPayAgainstInvoiceDateWise() {
	noDateDataFoundFlag = true;
	var frmDate = new Date($('#aStartDate').val());
	var toDate = new Date($('#aEndDate').val());

	var isFromDateValid = isValidDate(frmDate);
	var isToDateValid = isValidDate(toDate);

	frmDate = formatDate(frmDate);
	toDate = formatDate(toDate);

	if (isFromDateValid == true && isToDateValid == true) {
		var table = document.getElementById("paymentHistoryMainTable");

		for (var i = 0; i < table.rows.length - 1; i++) {
			var toggleTest = $("#accordion_service_" + i).is(":hidden");
			if (toggleTest == false) {
				toggleAccordion('#accordion_service_' + i);
			}
			var paymentDate = new Date($("#paymentDate_" + i).text());
			paymentDate = formatDate(paymentDate);
			if (paymentDate >= frmDate && paymentDate <= toDate && $("#cmpnyCode").val() == $("#cmpnyCode_" + i).text()) {
				$("#row_" + i).show();
				noDateDataFoundFlag = false;
			} else {
				$("#row_" + i).hide();
			}
		}

		if (noDateDataFoundFlag == false) {
			document.getElementById("noInvoiceDataFound").style.display = "none";
		} else {
			document.getElementById("noInvoiceDataFound").style.display = "block";
			/*document.getElementById("paymentHistorytbodyID").style.display = "none";*/
			var table = document.getElementById("paymentHistoryMainTable");
			for (var i = 0; i < table.rows.length - 1; i++) {
				var toggleTest = $("#accordion_service_" + i).is(":hidden");
				if (toggleTest == false) {
					toggleAccordion('#accordion_service_' + i);
				}
				/*if($("#cmpnyCode").val()==$("#cmpnyCode_" + i).text()){
					$("#row_" + i).show();
				}else{
					$("#row_" + i).hide();
				}*/
			}
			noDateDataFoundFlag = true;
		}

	} else {
		alert('Please Select From and To date.');
	}


}

function isValidDate(d) {
	return d instanceof Date && !isNaN(d);
}

function resetFilterDate() {
	$("#aStartDate").val("");
	$("#aEndDate").val("");
	document.getElementById("aStartDate").disabled = true;
	document.getElementById("aEndDate").disabled = true;
	document.getElementById("dateButtonId").disabled = true;
	document.getElementById("resetFilterDateButtonId").disabled = true;
	document.getElementById("serachInvoiceAllData").checked = true;
	document.getElementById("noInvoiceDataFound").style.display = "none";
	noDateDataFoundFlag = true;

	var table = document.getElementById("paymentHistoryMainTable");
	for (var i = 0; i < table.rows.length - 1; i++) {
		var toggleTest = $("#accordion_service_" + i).is(":hidden");
		if (toggleTest == false) {
			toggleAccordion('#accordion_service_' + i);
		}
		if ($("#cmpnyCode").val() == $("#cmpnyCode_" + i).text()) {
			$("#row_" + i).show();
		} else {
			$("#row_" + i).hide();
		}
	}

}

function formatDate(date) {
	var d = new Date(date),
		month = '' + (d.getMonth() + 1),
		day = '' + d.getDate(),
		year = d.getFullYear();

	if (month.length < 2) month = '0' + month;
	if (day.length < 2) day = '0' + day;

	return [month, day, year].join('/');
}

var noDateDataFoundFlag = true;

function submitPendingPaymentData(id) {
	$("#loading-wrapper").show();
	var repaymentJsonData = {
		"jslRefNumber": $("#jslTranId_" + id).text().trim()
	}
	$.ajax({
		type: "POST",
		url: ACC.config.encodedContextPath + "/my-account/submit-pending-payment?CSRFToken=" + ACC.config.CSRFToken,
		data: JSON.stringify(repaymentJsonData),
		contentType: "application/json; charset=utf-8",
		success: function (response) {
			submitPaymentForm(response);
		},
		error: function (response) {
			$("#loading-wrapper").hide();
		}
	});

}

function submitPaymentForm(responseJson) {
	my_form = document.createElement('FORM');
	my_form.name = 'paymentForm';
	my_form.method = 'POST';
	my_form.action = responseJson.actionUrl;

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'access_token';
	my_tb.value = responseJson.access_token;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'merchant_code';
	my_tb.value = responseJson.merchantCode;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'comp_code';
	my_tb.value = responseJson.companyCode;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'cust_code';
	my_tb.value = responseJson.customerCode;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'jsl_ref_no';
	my_tb.value = responseJson.jslReferenceNumber;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'fi_doc_number';
	my_tb.value = responseJson.fiDocumentNumber;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'po_no';
	my_tb.value = responseJson.poNumber;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'jsl_ref_amt';
	my_tb.value = responseJson.totalAmount;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'invoice_amount';
	my_tb.value = responseJson.invoiceAmount;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'sale_order_no';
	my_tb.value = responseJson.saleOrderNumber;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'payment_source';
	my_tb.value = responseJson.paymentSource;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'fis_yr';
	my_tb.value = responseJson.fiscalYear;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'pmt_type_adv';
	my_tb.value = responseJson.paymentTypeAdvance;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'inv_no';
	my_tb.value = responseJson.invoiceNumber;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'gst_inv_number';
	my_tb.value = responseJson.gstInvoiceNumber;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'pstng_date';
	my_tb.value = responseJson.postingDate;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'doc_date';
	my_tb.value = responseJson.documentDate;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'bus_area';
	my_tb.value = responseJson.busArea;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'Name1';
	my_tb.value = responseJson.customerName;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'bldat';
	my_tb.value = responseJson.paymentDate;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'bank_accno';
	my_tb.value = responseJson.bankAccountNumber;
	my_form.appendChild(my_tb);

	my_form.appendChild(my_tb);
	document.body.appendChild(my_form);
	my_form.submit();
}