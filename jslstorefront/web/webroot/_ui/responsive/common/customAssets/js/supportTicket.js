var primDefects = primaryDefects;
var addedBatches = [];
var addedBatchesInfo = [];

window.onload = function () {
	$("#loading-wrapper").hide();
	updateTable();
};


$(".invoicevalueDetails li .invoice_value").click(function () {
	$(this).next(".bill_details").toggleClass("active");
	$(this).find(".lni-plus").toggleClass("lni-minus").toggleClass("lni-plus");

});

$(function () {
	$('#compType').change(function () {
		if (this.value == 'Service') {
			resetForm();
			$(".materialType").hide();
			$(".serviceType").show();
		} else {
			resetForm();
			$(".materialType").show();
			$(".serviceType").hide();
		}
	});
});


function createjson() {
	if ($('#compType').val() == "Service") {
		if ($("#serviceSubCat").val() != "") {
			var json = {
				"typeOfComplaint": $("#compType").val(),
				"natureOfComplaints": $("#serviceSubCat").val(),
				"complaintDescription": $("#com_desc").val()
			}
		} else {
			var msg = "Kindly fill the complete form.";
			popUp(msg, 3000, "red");
			return;
		}
	} else if ($('#compType').val() == "Material") {
		if ($("#compType").val() != "" && $("input[name='optradio']:checked").val() != undefined && $("#natureOfComplaint").val() != "" &&
			$("#invoiceNumber").val() != "" && !$.isEmptyObject(addedBatches) && $("#materialLocation").val() != "" &&
			$("#contactPersonName").val() != "" && $("#contactNo").val() != "") {
			var json = {
				"typeOfComplaint": $("#compType").val(),
				"presentMaterialCondition": $("input[name='optradio']:checked").val(),
				"natureOfComplaints": $("#natureOfComplaint").val(),
				"invoiceNumber": $("#invoiceId").text(),
				"invoiceDetails": addedBatches,
				"attachment1": null,
				"attachment2": null,
				"attachment3": null,
				"attachment4": null,
				"materialLocation": $("#materialLocation").val(),
				"contactPersonName": $("#contactPersonName").val(),
				"contactNumber": $("#contactNo").val(),
				"complaintDescription": $("#complaintDesc").val(),
				"salesOffice": $("#salesOffice").val(),
				"invoiceCompanyCode": $("#invoiceCompanyCode").val(),
				"salesGroup": $("#salesGroup").val(),
				"salesDistrict": $("#salesDistrict").val(),
				"salesOrg": $("#salesOrg").val(),
				"kamName": $("#invoiceKamName").val(),
				"kamId": $("#invoiceKamId").val()
			}
		} else {
			var msg = "Kindly fill the complete form.";
			popUp(msg, 3000, "red");
		}
	}
	return json;
}

function sendList() {
	var json = createjson();
	if ($.isEmptyObject(json)) {
		return;
	} else {
		if($('#compType').val() == "Material"){
			$("#loading-wrapper").show();
			formData = new FormData();
			var attachedFiles = [];
			var size = 0; //size is in bytes
			for (i = 1; i <= 4; i++) {
				if (document.getElementById("attachment" + i).files[0] == undefined) {
					continue;
				} else {
					formData.append("file", document.getElementById("attachment" + i).files[0]);
					size = size + parseInt(document.getElementById("attachment" + i).files[0].size);
				}

			}
			if (size > 10000000) {
				$("#loading-wrapper").hide();
				var msg = "Attachment size can not be more than 10 MB."
				popUp(msg, 3000, "red");
			} else if ($("#attachment1").val() == '' && $("#attachment2").val() == '' && $("#attachment3").val() == '' && $("#attachment4").val() == '') {
				var msg = "Please attach atleast one photograph."
				popUp(msg, 3000, "red");
				$("#loading-wrapper").hide();
			} else {
				formData.append('customerComplaintDto', new Blob([JSON.stringify(json)], {
					type: "application/json"
				}));

				$.ajax({
					type: "POST",
					url: ACC.config.encodedContextPath + "/my-account/register-complaint?CSRFToken=" + ACC.config.CSRFToken,
					data: formData,
					enctype: "multipart/form-data",
					contentType: false,
					processData: false,
					success: function (response) {
						$("#loading-wrapper").hide();
						var msg = "Support Ticket with ID: " + response.c4cComplaintId + " created successfully.";
						showModal(msg);
						resetForm();
					},
					error: function (response) {
						$("#loading-wrapper").hide();
						var msg = "Something went wrong. The request cannot be processed right now. Please try again later."
						popUp(msg, 3000, "red");
					}
				});
			}
		}else if($('#compType').val() == "Service"){
			$("#loading-wrapper").show();
			formData = new FormData();
			formData.append('customerComplaintDto', new Blob([JSON.stringify(json)], {
				type: "application/json"
			}));
			$.ajax({
				type: "POST",
				url: ACC.config.encodedContextPath + "/my-account/register-complaint?CSRFToken=" + ACC.config.CSRFToken,
				data: formData,
				enctype: "multipart/form-data",
				contentType: false,
				processData: false,
				success: function (response) {
					$("#loading-wrapper").hide();
					var msg = "Support Ticket with ID: " + response.c4cComplaintId + " created successfully.";
					showModal(msg);
					resetForm();
				},
				error: function (response) {
					$("#loading-wrapper").hide();
					var msg = "Something went wrong. The request cannot be processed right now. Please try again later."
					popUp(msg, 3000, "red");
				}
			});
		}
		else{
			console.log('Do Nothing');
		}
	}
}

function resetForm() {
	$("input[name='optradio']:checked").checked = false;
	$("#natureOfComplaint").val("");
	$("#invoiceNumber").val("");
	$("#materialLocation").val("");
	$("#contactPersonName").val("");
	$("#contactNo").val("");
	$("#complaintDesc").val("");
	$("#com_desc").val("");
	$("#serviceSubCat").val("");
	$("#attachment1").val("");
	$("#attachment2").val("");
	$("#attachment3").val("");
	$("#attachment4").val("");
	$("#invoiceId").html("");
	$("#invoiceSearch").val("");
	$(".invoicevalueDetails").hide();
	document.getElementById("batchTableBody").innerHTML = "";
	addedBatches = [];
	addedBatchesInfo = [];
	updateTable();
	size = 0;
}

function searchInvoice() {
	$("#loading-wrapper").show();
	document.getElementById("batchTableBody").innerHTML = '';
	$(".invoicevalueDetails").hide();
	if ($("#invoiceSearch").val() != "") {
		if (!$.isEmptyObject(addedBatches) && $("#invoiceSearch").val() != $("#invoiceId").text()) {
			var decision = confirm("The added batches will be removed if you choose another invoice. Do you wish to Continue?");
			if (decision) {
				addedBatches = [];
				addedBatchesInfo = [];
				updateTable();
			}
		}
		var searchData = {
			"searchCriteria": "invoiceNumber",
			"searchValue": $("#invoiceSearch").val().trim()
		};
		$.ajax({
			type: "POST",
			url: ACC.config.encodedContextPath + "/reports/fetchInvoiceDetails?CSRFToken=" + ACC.config.CSRFToken,
			data: JSON.stringify(searchData),
			contentType: "application/json; charset=utf-8",
			success: function (response) {
				if (response.length == 0) {
					var msg = "Invoice number not found.";
					popUp(msg, 3000, "red");
				} else {
					showBatches(response);
				}
				$("#loading-wrapper").hide();
			},
			error: function (response) {
				var msg = "Something went wrong. Please try again later";
				$("#loading-wrapper").hide();
				popUp(msg, 3000, "red");
			}
		});
	} else {
		$("#loading-wrapper").hide();
		var msg = "Please enter invoice number to search.";
		popUp(msg, 3000, "red");
		$(".invoicevalueDetails").hide();
	}
}

function showBatches(response) {
	for (var j = 0; j < response.length; j++) {
		$("#invoiceId").html(response[j].invoiceNumber + "<i class='lni-plus'></i>");
		$("#salesOffice").val(response[j].salesOffice);
		$("#invoiceCompanyCode").val(response[j].invoiceCompanyCode);
		$("#salesGroup").val(response[j].salesGroup);
		$("#salesDistrict").val(response[j].salesDistrict);
		$("#salesOrg").val(response[j].salesOrg);
		$("#invoiceKamName").val(response[j].invoiceKamName);
		$("#invoiceKamId").val(response[j].invoiceKamId);

		$(".invoicevalueDetails").show();
		var table = document.getElementById("batchTableBody");
		var typeOfNatureSelected = document.getElementById("natureOfComplaint").value;
		for (var i = 0; i < response[j].material.length; i++) {
			var row = table.insertRow(i);
			row.id = "row_" + j;
			var cell0 = row.insertCell(0);
			var button = document.createElement("button");
			button.id = "addBatchBtn_" + j;
			button.classList.add("btn", "btn-primary");
			button.onclick = function (args) {
				return function () {
					addBatch(args);
				}
			}(button.id);
			button.innerHTML = "Add";
			cell0.appendChild(button);
			var cell1 = row.insertCell(1);
			cell1.id = "batchNo_" + j;
			cell1.innerText = response[j].material[i].batchNumber;
			var cell2 = row.insertCell(2);
			cell2.id = "grade_" + j;
			cell2.innerText = response[j].material[i].grade;
			var cell3 = row.insertCell(3);
			cell3.id = "thk_" + j;
			cell3.innerText = response[j].material[i].thickness;
			var cell4 = row.insertCell(4);
			cell4.id = "wdth_" + j;
			cell4.innerText = response[j].material[i].width;
			var cell5 = row.insertCell(5);
			cell5.id = "len_" + j;
			cell5.innerText = response[j].material[i].length;
			var cell6 = row.insertCell(6);
			cell6.id = "finish_" + j;
			cell6.innerText = response[j].material[i].finish;
			var cell7 = row.insertCell(7);
			cell7.id = "edge_" + j;
			cell7.innerText = response[j].material[i].edge;
			var cell8 = row.insertCell(8);
			var sel = document.createElement("select");
			sel.id = "primDefect_" + j;
			sel.innerHTML = populatePrimDefects(typeOfNatureSelected);
			cell8.appendChild(sel);
			var cell9 = row.insertCell(9);
			cell9.id = "suppQty_" + j;
			cell9.innerText = response[j].material[i].supplyQuantity;
			var cell10 = row.insertCell(10);
			var input = document.createElement("input");
			input.id = "complaintQty_" + j;
			input.type = "number";
			input.min = 0;
			input.max = response[j].material[i].supplyQuantity;
			input.classList.add("form-control");
			cell10.appendChild(input);
			var cell11 = row.insertCell(11);
			cell11.id = "plant_" + j;
			cell11.innerText = response[j].material[i].plant;
			var cell12 = row.insertCell(12);
			cell12.id = "invoiceDate_" + j;
			if (response[j].material[i].invoiceDate != undefined || response[0].material[i].invoiceDate != null) {
				cell12.innerText = response[j].material[i].invoiceDate.split(' ')[0];
			} else {
				cell12.innerText = response[j].material[i].invoiceDate;
			}
			var cell13 = row.insertCell(13);
			cell13.id = "kamName_" + j;
			cell13.innerText = response[j].material[i].kamName;
			var cell14 = row.insertCell(14);
			cell14.id = "soNo_" + j;
			cell14.innerText = response[j].material[i].soNumber;
			var cell15 = row.insertCell(15);
			var input = document.createElement("input");
			input.id = "productCode_" + j;
			input.type = "hidden";
			input.value = response[j].material[i].productCode;
			cell15.appendChild(input);
			var input2 = document.createElement("input");
			input2.id = "kamId_" + j;
			input2.type = "hidden";
			input2.value = response[j].material[i].kamId;
			cell15.appendChild(input2);
			var input3 = document.createElement("input");
			input3.id = "gradeGroup_" + j;
			input3.type = "hidden";
			input3.value = response[j].material[i].gradeGroup;
			cell15.appendChild(input3);
			var input4 = document.createElement("input");
			input4.id = "usage_" + j;
			input4.type = "hidden";
			input4.value = response[j].material[i].usage;
			cell15.appendChild(input4);
			var input5 = document.createElement("input");
			input5.id = "standard_" + j;
			input5.type = "hidden";
			input5.value = response[j].material[i].standard;
			cell15.appendChild(input5);
			var input6 = document.createElement("input");
			input6.id = "series_" + j;
			input6.type = "hidden";
			input6.value = response[j].material[i].series;
			cell15.appendChild(input6);
			var input7 = document.createElement("input");
			input7.id = "compCode_" + j;
			input7.type = "hidden";
			input7.value = response[j].material[i].companyCode;
			var input8 = document.createElement("input");
			input8.id = "productType_" + j;
			input8.type = "hidden";
			input8.value = response[j].material[i].productType;
			cell15.appendChild(input8);
		}
	}
}

function populatePrimDefects(typeOfNatureSelected) {
	var code = "<option value=''>Select</option>";
	if (typeOfNatureSelected == "Surface Related") {
		primDefects.surfaceRelated.sort().forEach(function (elem, index) {
			code = code + "<option value='" + elem + "'>" + elem + "</option>";
		});
	} else if (typeOfNatureSelected == "Dimension Related") {
		primDefects.dimensionRelated.sort().forEach(function (elem, index) {
			code = code + "<option value='" + elem + "'>" + elem + "</option>";
		});
	} else if (typeOfNatureSelected == "Weight Related") {
		primDefects.weightRelated.sort().forEach(function (elem, index) {
			code = code + "<option value='" + elem + "'>" + elem + "</option>";
		});
	} else if (typeOfNatureSelected == "Property Related") {
		primDefects.propertyRelated.sort().forEach(function (elem, index) {
			code = code + "<option value='" + elem + "'>" + elem + "</option>";
		});
	} else if (typeOfNatureSelected == "Transit Related") {
		primDefects.transitRelated.sort().forEach(function (elem, index) {
			code = code + "<option value='" + elem + "'>" + elem + "</option>";
		});
	} else if (typeOfNatureSelected == "Shape Related") {
		primDefects.shapeRelated.sort().forEach(function (elem, index) {
			code = code + "<option value='" + elem + "'>" + elem + "</option>";
		});
	}

	return code;
}

function updatePrimDefect() {
	var table = document.getElementById("batchTableBody");
	var typeOfNatureSelected = document.getElementById("natureOfComplaint").value;
	for (var i = 0; i < table.rows.length; i++) {
		if (document.getElementById("primDefect_" + i)) {
			document.getElementById("primDefect_" + i).innerHTML = populatePrimDefects(typeOfNatureSelected);
		}
	}
}

function searchBatch() {
	var batchNo = $("#batchSearch").val();
	var table = document.getElementById("batchTable");
	for (var i = 0; i < table.rows.length - 1; i++) {
		if (batchNo != "" && $("#batchNo_" + i).text().indexOf(batchNo) > -1) {
			$("#row_" + i).show();
		} else {
			$("#row_" + i).hide();
		}
		if (batchNo == "") {
			$("#row_" + i).show();
		}
	}
}

function addBatch(id) {
	var num = id.split("_")[1];
	var invoiceNumber = $("#invoiceSearch").val().trim();
	var batchNumber = $("#batchNo_" + num).text();
		$.ajax({
			type: "GET",
			url: ACC.config.encodedContextPath + "/my-account/isBatchAlreadyAdded?invoiceNumber="+invoiceNumber+"&batchNumber="+batchNumber,
			contentType: "application/json; charset=utf-8",
			success: function (response) {
				if (response.length != 0 && response.status!="Closed") {
					var complaintNumber = response.complaintNumber;
					var msg = "Complaint "+complaintNumber + " is already raised for this batch , you can log after closure of previous complaint."
					popUp(msg, 3000, "red");
				} else {
					if (!$.isEmptyObject(addedBatches)) {
						var alreadyAdded = false;
						addedBatches.forEach(function (elem, index) {
							if (elem.batchNumber == $("#batchNo_" + num).text()) {
								var msg = "Batch already added.";
								popUp(msg, 3000, "red");
								alreadyAdded = true;
								return;
							}
						});
						if (alreadyAdded) {
							return;
						}
					}
					if ($("#primDefect_" + num).val() != "" && $("#complaintQty_" + num).val() > 0 && $("#complaintQty_" + num).val() <= Number($("#suppQty_" + num).text())) {
						var batch = {
							"batchNumber": $("#batchNo_" + num).text(),
							"primaryDefect": $("#primDefect_" + num).val(),
							"soNumber": $("#soNo_" + num).text(),
							"supplyQuantity": $("#suppQty_" + num).text(),
							"complaintQuantity": $("#complaintQty_" + num).val(),
							"invoiceDate": $("#invoiceDate_" + num).text(),
							"kamName": $("#kamName_" + num).text(),
							"kamId": $("#kamId_" + num).val(),
							"plant": $("#plant_" + num).text(),
							"companyCode": $("#compCode_" + num).text(),
							"grade": $("#grade_" + num).text(),
							"thickness": $("#thk_" + num).text().replace(/[^0-9\.]/g, ''),
							"width": $("#wdth_" + num).text().replace(/[^0-9\.]/g, ''),
							"length": $("#len_" + num).text().replace(/[^0-9\.]/g, ''),
							"finish": $("#finish_" + num).text(),
							"edge": $("#edge_" + num).text(),
							"productCode": $("#productCode_" + num).val(),
							"gradeGroup": $("#gradeGroup_" + num).val(),
							"usage": $("#usage_" + num).val(),
							"standard": $("#standard_" + num).val(),
							"series": $("#series_" + num).val()
						};
						var info = {
							"plant": $("#plant_" + num).text(),
							"compCode": $("#compCode_" + num).text(),
							"grade": $("#grade_" + num).text(),
							"thk": $("#thk_" + num).text(),
							"wdth": $("#wdth_" + num).text(),
							"len": $("#len_" + num).text(),
							"finish": $("#finish_" + num).text(),
							"edge": $("#edge_" + num).text(),
						};
						addedBatches.push(batch);
						addedBatchesInfo.push(info);
						var msg = "Batch added Successfully.";
						popUp(msg, 3000, "green");
						updateTable();
					} else if ($("#primDefect_" + num).val() == "") {
						var msg = "Kindly select a Primary Defect.";
						popUp(msg, 3000, "red");
					} else if ($("#complaintQty_" + num).val() > Number($("#suppQty_" + num).text())) {
						var msg = "Complaint quantity cannot be greater than supplied quantity.";
						popUp(msg, 3000, "red");
					} else if (!$("#complaintQty_" + num).val()) {
						var msg = "kindly enter complaint quantity.";
						popUp(msg, 3000, "red");
					}
				}
				$("#loading-wrapper").hide();
			},
			error: function (response) {
				var msg = "Something went wrong. Please try again later";
				$("#loading-wrapper").hide();
				popUp(msg, 3000, "red");
			}
		});
}

function updateTable() {
	var table = document.getElementById("addedBatches");
	table.innerHTML = "";
	if (!$.isEmptyObject(addedBatches)) {
		for (var i = 0; i < addedBatches.length; i++) {
			var row = table.insertRow(i);
			var cell0 = row.insertCell(0);
			cell0.innerText = addedBatches[i].batchNumber;
			var cell1 = row.insertCell(1);
			cell1.innerText = addedBatchesInfo[i].grade;
			var cell2 = row.insertCell(2);
			cell2.innerText = addedBatchesInfo[i].thk;
			var cell3 = row.insertCell(3);
			cell3.innerText = addedBatchesInfo[i].wdth;
			var cell4 = row.insertCell(4);
			cell4.innerText = addedBatchesInfo[i].len;
			var cell5 = row.insertCell(5);
			cell5.innerText = addedBatchesInfo[i].finish;
			var cell6 = row.insertCell(6);
			cell6.innerText = addedBatchesInfo[i].edge;
			var cell7 = row.insertCell(7);
			cell7.innerText = addedBatches[i].primaryDefect;
			var cell8 = row.insertCell(8);
			cell8.innerText = addedBatches[i].supplyQuantity;
			var cell9 = row.insertCell(9);
			cell9.innerText = addedBatches[i].complaintQuantity;
			var cell10 = row.insertCell(10);
			cell10.innerText = addedBatchesInfo[i].plant;
			var cell11 = row.insertCell(11);
			cell11.innerText = addedBatches[i].invoiceDate.split(' ')[0];
			var cell12 = row.insertCell(12);
			cell12.innerText = addedBatches[i].kamName;
			var cell13 = row.insertCell(13);
			cell13.innerText = addedBatches[i].soNumber;
			var cell14 = row.insertCell(14);
			var delButton = document.createElement("i");
			delButton.onclick = function (arg) {
				return function () {
					removeBatch(arg)
				}
			}(i);
			delButton.classList.add("fa", "fa-trash");
			cell14.appendChild(delButton);
		}
	} else {
		table.innerHTML = "<tr>" + "<td colspan='16'><span><center>No batches added to Complaint.</center></span></td>" + "</tr>";
	}
}

function removeBatch(i) {
	addedBatches.splice(i, 1);
	addedBatchesInfo.splice(i, 1);
	updateTable();
}

function showModal(msg) {
	document.getElementById("modalMessage").innerHTML = msg;
	$("#myModal").show();
}

function hideModal() {
	$("#myModal").hide();
}

function popUp(msg, duration, color) {
	var snackbar = document.getElementById("snackBar");
	snackbar.innerHTML = msg;
	snackbar.style.backgroundColor = color;
	snackbar.className = "show";
	setTimeout(function () {
		snackbar.className = snackbar.className.replace("show", "");
	}, duration);
}