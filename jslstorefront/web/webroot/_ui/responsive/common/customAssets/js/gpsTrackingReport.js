window.onload=function(){
	$.ajax({
		type: "GET",
		url: ACC.config.encodedContextPath + "/reports/reportsJson?report=SHIPMENTDETAILSREPORT&CSRFToken=" + ACC.config.CSRFToken,
		success: function(response){
			createTable(response);
			postTableCreation();
		},
		error: function(response){
			 $("#successDataCheck").hide();
	 		 $('#loading-wrapper').hide();		  
	 		 $("#failureDataCheck").show();
		}
	});
}

function createTable(json){
	$("#customerId").text(json.customerId);
	$("#customerName").text(json.customerName);
	var table = document.getElementById("tableBody");
	json.shipmentDetails.forEach(function(item, index){
		var row = table.insertRow(index);
		row.id = "row_" + index;
		
		var cell0 = row.insertCell(0);
		cell0.id = "cmpnyCode_" + index;
		cell0.innerText=item.companyName;
//		if(item.companycode=='JS01'){
//			cell0.innerText = 'Jindal Stainless (Hisar) Limited';
//		}
//		if(item.companycode=='JS03'){
//			cell0.innerText = 'Jindal Stainless Limited';
//		}
		var cell1 = row.insertCell(1);
		cell1.id = "documentNo_" + index;
		cell1.innerText = item.documentNumber;
		var cell2 = row.insertCell(2);
		cell2.id = "trackingNo_" + index;
		cell2.innerText = item.trackingNo;
		var cell3 = row.insertCell(3);
		cell3.id = "dispatchCity" + index;
		cell3.innerText = item.dispatchCity;
		var cell4 = row.insertCell(4);
		cell4.id = "vehicleNo" + index;
		cell4.innerText = item.vehicleNo;
		var cell5 = row.insertCell(5);
		cell5.id = "transportationZone" + index;
		cell5.innerText = item.transporationZone;
		var cell6 = row.insertCell(6);
		cell6.id = "vehicleOutDate" + index;
		cell6.innerText = item.exitDate;
		var cell7 = row.insertCell(7);
		cell7.id = "vehicleOutTime" + index;
		cell7.innerText = item.exitTime;
		var cell8 = row.insertCell(8);
		cell8.id = "tripStatus" + index;
		cell8.innerText = item.tripStaus;
		var cell9 = row.insertCell(9);
		cell9.id = "tripStatusDate" + index;
		cell9.innerText = item.tripStatusDate;
		var cell10 = row.insertCell(10);
		cell10.id = "tripStatusTime" + index;
		cell10.innerText = item.tripStatusTime;
		var cell11 = row.insertCell(11);
		cell11.id = "trackingUrl" + index;
		if(item.trackingUrl=="")
			{
			cell11.innerText="Not Available";
			}
		else
			{
			var elLink = document.createElement('a');
			elLink.href = item.trackingUrl;
			elLink.innerHTML = 'Tracking Url';
			cell11.appendChild(elLink);
			}
		
		var cell12 = row.insertCell(12);
		cell12.id = "leadDay" + index;
		cell12.innerText = item.leadDay;
	});
}


function postTableCreation(){
	$("#loading-wrapper").hide();
//	var table = document.getElementById('tableBody');
//	var totalDebit = 0;
//	var totalCredit = 0;
//	for(var i = 0; i < table.rows.length-1; i++){
//		totalDebit = totalDebit + parseFloat($('#debitAmount_' + i).text());
//		totalCredit = totalCredit + parseFloat($('#creditAmount_' + i).text());
//	}
//	$('#totalDebit').text(totalDebit.toFixed(2));
//	$('#totalCredit').text(totalCredit.toFixed(2));
}

//function filterData(){
//	if($('#aStartDate').val() != "" && $('#aEndDate').val() != ""){
//	  var frm = new Date($('#aStartDate').val());
//	  var to = new Date($('#aEndDate').val());
//	  if(frm > to){
//		  alert("start date can not be greater than end date");
//		  $('#aStartDate').val("");
//		  $('#aEndDate').val("");
//	  }else{
//	  var table = document.getElementById('tableBody');
//	  var totalDebit = 0;
//	  var totalCredit = 0;
//	  for(var i = 0; i < table.rows.length-1; i++){
//		  var date = new Date($('#postDate_' + i).text());
//		  if(date <= to && date >= frm){
//			  $('#row_' + i).show();
//			  totalDebit = totalDebit + parseFloat($('#debitAmount_' + i).text());
//			  totalCredit = totalCredit + parseFloat($('#creditAmount_' + i).text());
//		  }else{
//			  $('#row_' + i).hide();
//		  }
//	  }
//	  $('#totalDebit').text(totalDebit.toFixed(2));
//	  $('#totalCredit').text(totalCredit.toFixed(2));
//	  }
//	}else{
//		alert("kindly enter the date range");
//	}
//}


//function filterDataForCompany(){
//	
//	var slectedCompany=$('#compny_data').val();
//	var table = document.getElementById('tableBody');
//	for(var i = 0; i < table.rows.length-1; i++){
//		var companyCodeForRow=$("#cmpnyCode_" + i).text();
//		if('jshl'==slectedCompany){
//			if('Jindal Stainless (Hisar) Limited'==companyCodeForRow){
//				 $('#row_' + i).show();
//			}
//			else{
//				  $('#row_' + i).hide();
//			 }
//		}
//		else if('jsl'==slectedCompany){
//			if('Jindal Stainless Limited'==companyCodeForRow){
//				 $('#row_' + i).show();
//			}
//			else{
//				  $('#row_' + i).hide();
//			 }
//		}
//		else{
//			$('#row_' + i).show();
//		}
//	  }
//}
//function resetData(){
//	$('#aStartDate').val("");
//	$('#aEndDate').val("");
//	var totalDebit = 0;
//	var totalCredit = 0;
//	var table = document.getElementById('tableBody');
//	for(var i = 0; i < table.rows.length-1; i++){
//		$('#row_' + i).show();
//		totalDebit = totalDebit + parseFloat($('#debitAmount_' + i).text());
//		totalCredit = totalCredit + parseFloat($('#creditAmount_' + i).text());
//	}
//	$('#totalDebit').text(totalDebit.toFixed(2));
//	$('#totalCredit').text(totalCredit.toFixed(2));
//}
