window.onload = function(){
	$.ajax({
		type: "GET",
		url: ACC.config.encodedContextPath + "/reports/reportsJson?report=ORDERBOOKINGREPORT&CSRFToken=" + ACC.config.CSRFToken,
		success: function(response){
			createTable(response);
			postTableCreation();
			
		},
		error: function(response){
			    $("#successDataCheck").hide();
		 		 $('#loading-wrapper').hide();		  
		 		 $("#failureDataCheck").show();
		}
	});
}
	
	function createTable(json){
		var table = document.getElementById("tableBody");
		json.forEach(function(item, index){
			var row = table.insertRow(index);
			row.id = "row_" + index;
			var cell0 = row.insertCell(0);
			cell0.id = "sDoc_" + index;
			cell0.innerText = item.salesOrderNumber;
			var cell1 = row.insertCell(1);
			cell1.id = "item_" + index;
			cell1.innerText = item.item;
			var cell2 = row.insertCell(2);
			cell2.id = "productType_" + index;
			cell2.innerText = item.productType;
			cell2.classList.add("prdType");
			var cell3 = row.insertCell(3);
			cell3.id = "poNum_" + index;
			cell3.innerText = item.poNumber;
			var cell4 = row.insertCell(4);
			cell4.id = "poDate_" + index;
			cell4.innerText = item.poDate;
			var cell5 = row.insertCell(5);
			cell5.id = "quality_" + index;
			cell5.innerText = item.quality;
			cell5.classList.add("quality");
			var cell6 = row.insertCell(6);
			cell6.id = "grade_" + index;
			cell6.innerText = item.grade;
			cell6.classList.add("grade");
			var cell7 = row.insertCell(7);
			cell7.id = "thk_" + index;
			cell7.innerText = item.thickness;
			var cell8 = row.insertCell(8);
			cell8.id = "wdth_" + index;
			cell8.innerText = item.width;
			var cell9 = row.insertCell(9);
			cell9.id = "len_" + index;
			cell9.innerText = item.length;
			var cell10 = row.insertCell(10);
			cell10.id = "finish_" + index;
			cell10.innerText = item.finish;
			var cell11 = row.insertCell(11);
			cell11.id = "edge_" + index;
			cell11.innerText = item.edgeCondition;
			var cell12 = row.insertCell(12);
			cell12.id = "soDate_" + index;
			cell12.innerText = item.salesOrderDate;
			var cell13 = row.insertCell(13);
			cell13.id = "ordQty_" + index;
			cell13.innerText = item.orderItemQuantity;
			var cell14 = row.insertCell(14);
			cell14.id = "basePrice_" + index;
			cell14.innerText = item.basicPrice;
		});
	}
	
	function postTableCreation(){
		$("#loading-wrapper").hide();
		var qualityCells = document.getElementsByClassName("quality");
		var gradeCells = document.getElementsByClassName("grade");
		var prdTypeCells = document.getElementsByClassName("prdType");
		var qualityOptions = new Set();
		var gradeOptions = new Set();
		var prdTypeOptions = new Set();
		
		for(var i = 0; i < qualityCells.length; i++){
			if(!qualityOptions.has(qualityCells[i].innerText)){
				qualityOptions.add(qualityCells[i].innerText);
			}
		}
		qualityOptions.forEach(function(elem){
			qualitySel.options.add(new Option(elem, elem));
		});
		
		for(var i = 0; i < gradeCells.length; i++){
			if(!gradeOptions.has(gradeCells[i].innerText)){
				gradeOptions.add(gradeCells[i].innerText);
			}
		}
		gradeOptions.forEach(function(elem){
			gradeSel.options.add(new Option(elem, elem));
		});
		
		for(var i = 0; i < prdTypeCells.length; i++){
			if(!prdTypeOptions.has(prdTypeCells[i].innerText)){
				prdTypeOptions.add(prdTypeCells[i].innerText);
			}
		}
		prdTypeOptions.forEach(function(elem){
			prdTypeSel.options.add(new Option(elem, elem));
		});
		filterData("gradeSel");
		filterData("qualitySel");
		filterData("prdTypeSel");
	}

function filterData(id){
	var graphMap = new Map();
	var table = document.getElementById("tableBody");
	if(id === "gradeSel"){
		var fieldId = "#grade_";
		var sel = $("#gradeSel").val();
		var container = "gradeGraph";
	}else if(id === "qualitySel"){
		var fieldId = "#quality_";
		var sel = $("#qualitySel").val();
		var container = "qualityGraph";
	}else if(id === "prdTypeSel"){
		var fieldId = "#productType_";
		var sel = $("#prdTypeSel").val();
		var container = "prdTypeGraph";
	}
	const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
	for(var i = 0; i < table.rows.length; i++){
		if($(fieldId + i).text() == sel || sel == ""){
			var date = new Date($("#soDate_" + i).text());
			var day = date.getDate();
			var month = months[date.getMonth()];
			date = day + " " + month;
			var qty = parseFloat($("#ordQty_" + i).text());
			if(!graphMap.has(date)){
				graphMap.set(date, qty); 
			}else{
				var sum = graphMap.get(date);
				sum = sum + qty;
				graphMap.set(date, sum);
			}
		}
	}
	createGraph(graphMap, container);
}

function createGraph(graphMap, container){
	var dates = [];
	var totalQty = [];
	for(var a of graphMap.keys()){
		dates.push(a);
	}
	for(var b of graphMap.values()){
		totalQty.push(b);
	}
	var flagArray = totalQty.filter(function(elem){
		if(elem > 0){
			return true;
		}
		return false;
	});
	
	if(flagArray.length != 0){
		var data = [
			{
				x: dates,
				y: totalQty,
				type: 'bar',
				
			}
		];
		if(container=='gradeGraph'){
			var layout = {
					  title: {
					    text:'<b>Grade Vs Order Quantity</b>',
					    font: {
					      size: 14
					    },
					    xref: 'paper',
					    x: 0.05,
					  },
					  xaxis: {
					    title: {
					      text: '<b>Doc. Date</b>',
					      font :{
	         		    	  size : 12
	         		      }
					    },
					  },
					  yaxis: {
					    title: {
					      text: '<b>Order Quantity</b>',
					      font :{
	         		    	  size : 12
	         		      }
					    }
					  }
					};
		}
		if(container=='qualityGraph'){
			var layout = {
					  title: {
					    text:'<b>Quality Vs Order Quantity</b>',
					    font: {
					      size: 14
					    },
					    xref: 'paper',
					    x: 0.05,
					  },
					  xaxis: {
					    title: {
					      text: '<b>Doc. Date</b>',
					      font :{
	         		    	  size : 12
	         		      }
					    },
					  },
					  yaxis: {
					    title: {
					      text: '<b>Order Quantity</b>',
					      font :{
	         		    	  size : 12
	         		      }
					    }
					  }
					};
		}
		if(container=='prdTypeGraph'){
			var layout = {
					  title: {
					    text:'<b>Product Type Vs Order Quantity</b>',
					    font: {
					      size: 14
					    },
					    xref: 'paper',
					    x: 0.05,
					  },
					  xaxis: {
					    title: {
					      text: '<b>Doc. Date</b>',
					      font :{
	         		    	  size : 12
	         		      }
					    },
					  },
					  yaxis: {
					    title: {
					      text: '<b>Order Quantity</b>',
					      font :{
	         		    	  size : 12
	         		      }
					    }
					  }
					};
		}
		
	
		
		 Plotly.newPlot(container, data, layout ,{
		        showSendToCloud: true,
		        displayModeBar: false
		    });
	}else{
		$("#prdTypeVsQty").html("<div class='reportTitle'>Product Type Vs Order Quantity</div>");
		$("#qualityVsQty").html("<div class='reportTitle'>Quality Vs Order Quantity</div>");
		$("#gradeVsQty").html("<div class='reportTitle'>Grade Vs Order Quantity</div>");
		$("#gradeGraph").html('<div class="noGraphData"><marquee class="mrq_data">No data available for graphical representation</marquee></div>');
		$("#qualityGraph").html('<div class="noGraphData"><marquee class="mrq_data">No data available for graphical representation</marquee></div>');
		$("#prdTypeGraph").html('<div class="noGraphData"><marquee class="mrq_data">No data available for graphical representation</marquee></div>');
	}
}

function resetData(id){
	$("#" + id).val("");
	filterData(id);
}
