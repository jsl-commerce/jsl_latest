var totalDueAmount = 0.0;
var payableAmt = 0.0;
var balanceDueAmt = 0.0;
var selectAllFlag = false;
var paymentArray = [];
var paymentAdvanceArray = [];
var paymentRowSelectedFlag = false;
var advancePaymentRowSelectedFlag = false;
var selectedAdvanceSearchedOption = "";
var totalAdvPayableAmt = 0.0;

var today = new Date();
var threeMonthsAgo = new Date(new Date().setDate(today.getDate() - 90));
$("#aStartDate").datepicker({
	format: 'mm/dd/yyyy',
	autoclose: true,
	endDate: today
});

$("#startDateIcon").on('click', function () {
	$("#aStartDate").show().focus();
});


$('#aEndDate').datepicker({
	format: 'mm/dd/yyyy',
	autoclose: true,
	endDate: today
});

$("#endDateIcon").on('click', function () {
	$("#aEndDate").show().focus();
});

function formatDate(date) {
	date = date.replace("IST", "");
	if (date.includes('.') && date.length == 10) {
		return date;
	} else {
		var d = new Date(date);
		var month = '' + (d.getMonth() + 1);
		var day = '' + d.getDate();
		var year = d.getFullYear();

		if (month.length < 2)
			month = '0' + month;
		if (day.length < 2)
			day = '0' + day;

		return [ day, month, year ].join('.');
	}
}

function formatChangedDate(date){
	var newChangedDate = date.substring(3, 5) + "/" + date.substring(0, 2) + "/" + date.substring(6, 10);
	return newChangedDate ;
}

function resetFilterInvoice() {
	document.getElementById("serachInvoiceData").value = '';
	totalDueAmount = 0.0;
	payableAmt = 0.0;
	balanceDueAmt = 0.0;

	var table = document.getElementById("paymentInvoiceTable");

	document.getElementById("chkBoxAll").checked = false;
	var table = document.getElementById("paymentInvoiceTable");
	for (var i = 0; i < table.rows.length - 1; i++) {
		document.getElementById("chkBox_" + i).checked = false;
		if ($("#cmpnyCode").val() == $("#cmpnyCode_" + i).text()) {
			totalDueAmount = parseFloat($("#amtDue_" + i).text()) + totalDueAmount;
			var invDate = formatDate($("#invDate_" + i).text());
			$("#invDate_" + i).text(invDate);
			var dueDate = formatDate($("#dueDate_" + i).text());
			$("#dueDate_" + i).text(dueDate);
			$("#row_" + i).show();
		} else {
			$("#row_" + i).hide();
		}

	}

	document.getElementById("totalDueAmountInvoice").value = totalDueAmount.toFixed(2);
	document.getElementById("totalBalanceDueAmt").value = totalDueAmount.toFixed(2);
	document.getElementById("totalPayableAmt").value = "0.00";
	document.getElementById("serachInvoiceAllData").checked = true;
	document.getElementById("serachInvoiceData").value = '';
	document.getElementById("serachInvoiceData").disabled = true;
	document.getElementById("invoiceButtonId").disabled = true;

	document.getElementById("aStartDate").value = '';
	document.getElementById("aEndDate").value = '';
	document.getElementById("aStartDate").disabled = true;
	document.getElementById("aEndDate").disabled = true;
	document.getElementById("dateButtonId").disabled = true;
	document.getElementById("resetFilterIvoiceButtonId").disabled = true;
	document.getElementById("resetFilterDateButtonId").disabled = true;

	document.getElementById("noInvoiceDataFound").style.display = "none";
	countSelectedRows = 0;

}

function resetFilterDate() {
	totalDueAmount = 0.0;
	payableAmt = 0.0;
	balanceDueAmt = 0.0;

	document.getElementById("aStartDate").value = '';
	document.getElementById("aEndDate").value = '';
	var table = document.getElementById("paymentInvoiceTable");

	document.getElementById("chkBoxAll").checked = false;
	var table = document.getElementById("paymentInvoiceTable");
	for (var i = 0; i < table.rows.length - 1; i++) {
		document.getElementById("chkBox_" + i).checked = false;
		if ($("#cmpnyCode").val() == $("#cmpnyCode_" + i).text()) {
			totalDueAmount = parseFloat($("#amtDue_" + i).text()) + totalDueAmount;
			var invDate = formatDate($("#invDate_" + i).text());
			$("#invDate_" + i).text(invDate);
			var dueDate = formatDate($("#dueDate_" + i).text());
			$("#dueDate_" + i).text(dueDate);
			$("#row_" + i).show();
		} else {
			$("#row_" + i).hide();
		}
	}

	document.getElementById("totalDueAmountInvoice").value = totalDueAmount.toFixed(2);
	document.getElementById("totalBalanceDueAmt").value = totalDueAmount.toFixed(2);
	document.getElementById("totalPayableAmt").value = "0.00";
	document.getElementById("serachInvoiceAllData").checked = true;
	document.getElementById("noInvoiceDataFound").style.display = "none";
	document.getElementById("serachInvoiceData").value = '';
	document.getElementById("serachInvoiceData").disabled = true;
	document.getElementById("invoiceButtonId").disabled = true;

	document.getElementById("aStartDate").value = '';
	document.getElementById("aEndDate").value = '';
	document.getElementById("aStartDate").disabled = true;
	document.getElementById("aEndDate").disabled = true;
	document.getElementById("dateButtonId").disabled = true;
	document.getElementById("resetFilterIvoiceButtonId").disabled = true;
	document.getElementById("resetFilterDateButtonId").disabled = true;
	countSelectedRows = 0;

}

function checkpaymentTypeSelected() {
	document.getElementById("pai").disabled = false;
	document.getElementById("pa").disabled = false;
	document.getElementById("paymentOptionId").style.display = "block";
	document.getElementById("cmpnyCode").options[0].disabled = true;

	if ($("#cmpnyCode").val() !== 'all' && $('.paymentTypeId:checked').val() == 'Pay against Invoice') {

		resetPayAgainstInvoiceForm();

		var table = document.getElementById("paymentInvoiceTable");

		for (var i = 0; i < table.rows.length - 1; i++) {
			if ($("#cmpnyCode").val() == $("#cmpnyCode_" + i).text()) {
				totalDueAmount = parseFloat($("#amtDue_" + i).text()) + totalDueAmount;
				var invDate = formatDate($("#invDate_" + i).text());
				$("#invDate_" + i).text(invDate);
				var dueDate = formatDate($("#dueDate_" + i).text());
				$("#dueDate_" + i).text(dueDate);
				$("#row_" + i).show();
			} else {
				$("#row_" + i).hide();
			}


		}
		document.getElementById("totalDueAmountInvoice").value = totalDueAmount.toFixed(2);
		document.getElementById("totalBalanceDueAmt").value = totalDueAmount.toFixed(2);
		document.getElementById("totalPayableAmt").value = "0.00";

		document.getElementById("noInvoiceDataFound").style.display = "none";
	} else if ($("#cmpnyCode").val() != 'all' && $('.paymentTypeId:checked').val() == 'Pay Advance') {
		resetPayAdvanceForm();
		$('.searchByAdvanceParameters input[type="radio"]').prop('checked', false);
	}

}

$('.checkedType input').change(function () {

		if ($("#cmpnyCode").val() == 'all') {
			alert('Please select the Comapany for "Payment Against Invoice"');

		} else {
			$("#paymentTypeID").removeClass("levelTop");
			if (this.value == 'Pay against Invoice') {
				document.getElementById("cmpnyCode").options[0].disabled = true;
				$(".payAdvance").hide();
				$(".payInvoice").show();

				resetPayAgainstInvoiceForm();

				document.getElementById("totalPayableAmt").value = "0.00";
				document.getElementById("totalBalanceDueAmt").value = "0.00";

				var table = document.getElementById("paymentInvoiceTable");

				for (var i = 0; i < table.rows.length - 1; i++) {
					if ($("#cmpnyCode").val() == $("#cmpnyCode_" + i).text()) {
						totalDueAmount = parseFloat($("#amtDue_" + i).text()) + totalDueAmount;
						var invDate = formatDate($("#invDate_" + i).text());
						$("#invDate_" + i).text(invDate);
						var dueDate = formatDate($("#dueDate_" + i).text());
						$("#dueDate_" + i).text(dueDate);
						$("#row_" + i).show();

					} else {
						$("#row_" + i).hide();
					}


				}
				document.getElementById("totalDueAmountInvoice").value = totalDueAmount.toFixed(2);
				document.getElementById("totalBalanceDueAmt").value = totalDueAmount.toFixed(2);
				document.getElementById("totalPayableAmt").value = "0.00";
				document.getElementById("noInvoiceDataFound").style.display = "none";


			} else {
				$(".payAdvance").show();
				$(".payInvoice").hide();
				resetPayAdvanceForm();
			}

		}
	}

);

function resetPayAdvanceForm() {
	document.getElementById("serachInvoiceDataAdvance").value = '';
	document.getElementById("serachInvoiceDataAdvance").disabled = true;
	document.getElementById("searchAdvanceInvoiceId").disabled = true;
	document.getElementById("serachPOData").value = '';
	document.getElementById("serachPOData").disabled = true;
	document.getElementById("searchAdvancePoId").disabled = true;
	advancePaymentRowSelectedFlag = false;
	document.getElementById("serachPOData").disabled = true;
	document.getElementById("advTable").innerHTML = '';
	document.getElementById("chkBoxAdvanceAll").checked = false;
	document.getElementById("noDataFound").style.display = 'none';
	totalDueAmount = 0.0;
	payableAmt = 0.0;
	balanceDueAmt = 0.0;
	countSelectedRows = 0;
}

function resetPayAgainstInvoiceForm() {
	document.getElementById("serachInvoiceData").value = '';
	document.getElementById("serachInvoiceData").disabled = true;
	document.getElementById("invoiceButtonId").disabled = true;
	document.getElementById("aStartDate").value = '';
	document.getElementById("aEndDate").value = '';
	document.getElementById("aStartDate").disabled = true;
	document.getElementById("aEndDate").disabled = true;
	document.getElementById("dateButtonId").disabled = true;
	document.getElementById("serachInvoiceAllData").checked = true;
	document.getElementById("resetFilterIvoiceButtonId").disabled = true;
	document.getElementById("resetFilterDateButtonId").disabled = true;
	paymentRowSelectedFlag = false;
	totalDueAmount = 0.0;
	payableAmt = 0.0;
	balanceDueAmt = 0.0;
	countSelectedRows = 0;
	document.getElementById("totalPayableAmt").value = "0.00";
	document.getElementById("totalBalanceDueAmt").value = "0.00";
	document.getElementById("totalDueAmountInvoice").value = "0.00";

	var table = document.getElementById("paymentInvoiceTable");

	document.getElementById("chkBoxAll").checked = false;
	var table = document.getElementById("paymentInvoiceTable");
	for (var i = 0; i < table.rows.length - 1; i++) {
		document.getElementById("chkBox_" + i).checked = false;
		var invDate = formatDate($("#invDate_" + i).text());
		$("#invDate_" + i).text(invDate);
		var dueDate = formatDate($("#dueDate_" + i).text());
		$("#dueDate_" + i).text(dueDate);
		$("#row_" + i).show();

	}
}

$('.searchByParameters input[type="radio"]').change(function () {

	if (this.value == 'Search By Invoice') {

		document.getElementById("serachInvoiceData").disabled = false;
		document.getElementById("aStartDate").disabled = true;
		document.getElementById("aEndDate").disabled = true;
		document.getElementById("aStartDate").value = '';
		document.getElementById("aEndDate").value = '';
		document.getElementById("noInvoiceDataFound").style.display = "none";
		document.getElementById("dateButtonId").disabled = true;
		document.getElementById("invoiceButtonId").disabled = false;
		document.getElementById("resetFilterIvoiceButtonId").disabled = false;
		document.getElementById("resetFilterDateButtonId").disabled = true;
		document.getElementById("serachInvoiceData").focus();
		countSelectedRows = 0;
		totalDueAmount = 0.0;
		payableAmt = 0.0;
		balanceDueAmt = 0.0;

		var table = document.getElementById("paymentInvoiceTable");
		for (var i = 0; i < table.rows.length - 1; i++) {
			if ($("#cmpnyCode").val() == $("#cmpnyCode_" + i).text()) {
				document.getElementById("chkBox_" + i).checked = false;
				totalDueAmount = parseFloat($("#amtDue_" + i).text()) + totalDueAmount;
				var invDate = formatDate($("#invDate_" + i).text());
				$("#invDate_" + i).text(invDate);
				var dueDate = formatDate($("#dueDate_" + i).text());
				$("#dueDate_" + i).text(dueDate);
				$("#row_" + i).show();
			} else {
				$("#row_" + i).hide();
			}
		}
		document.getElementById("totalDueAmountInvoice").value = totalDueAmount.toFixed(2);
		document.getElementById("totalBalanceDueAmt").value = totalDueAmount.toFixed(2);
		document.getElementById("totalPayableAmt").value = "0.00";
	}

	if (this.value == 'all') {
		document.getElementById("serachInvoiceData").disabled = true;
		document.getElementById("aStartDate").disabled = true;
		document.getElementById("aEndDate").disabled = true;
		document.getElementById("totalDueAmountInvoice").value = totalDueAmount.toFixed(2);
		document.getElementById("totalBalanceDueAmt").value = totalDueAmount.toFixed(2);
		document.getElementById("totalPayableAmt").value = "0.00";
		document.getElementById("serachInvoiceData").value = '';
		document.getElementById("aStartDate").value = '';
		document.getElementById("aEndDate").value = '';
		document.getElementById("noInvoiceDataFound").style.display = "none";
		document.getElementById("dateButtonId").disabled = true;
		document.getElementById("invoiceButtonId").disabled = true;
		document.getElementById("resetFilterIvoiceButtonId").disabled = true;
		document.getElementById("resetFilterDateButtonId").disabled = true;
		countSelectedRows = 0;
		totalDueAmount = 0.0;
		payableAmt = 0.0;
		balanceDueAmt = 0.0;

		var table = document.getElementById("paymentInvoiceTable");
		for (var i = 0; i < table.rows.length - 1; i++) {
			if ($("#cmpnyCode").val() == $("#cmpnyCode_" + i).text()) {
				document.getElementById("chkBox_" + i).checked = false;
				totalDueAmount = parseFloat($("#amtDue_" + i).text()) + totalDueAmount;
				var invDate = formatDate($("#invDate_" + i).text());
				$("#invDate_" + i).text(invDate);
				var dueDate = formatDate($("#dueDate_" + i).text());
				$("#dueDate_" + i).text(dueDate);
				$("#row_" + i).show();
			} else {
				$("#row_" + i).hide();
			}
		}

	}

	if (this.value == 'Search By Due Date') {
		document.getElementById("serachInvoiceData").disabled = true;
		document.getElementById("aStartDate").disabled = false;
		document.getElementById("aEndDate").disabled = false;
		document.getElementById("serachInvoiceData").value = '';
		document.getElementById("noInvoiceDataFound").style.display = "none";
		var table = document.getElementById("paymentInvoiceTable");
		document.getElementById("dateButtonId").disabled = false;
		document.getElementById("invoiceButtonId").disabled = true;
		document.getElementById("resetFilterIvoiceButtonId").disabled = true;
		document.getElementById("resetFilterDateButtonId").disabled = false;
		countSelectedRows = 0;
		totalDueAmount = 0.0;
		payableAmt = 0.0;
		balanceDueAmt = 0.0;

		for (var i = 0; i < table.rows.length - 1; i++) {
			if ($("#cmpnyCode").val() == $("#cmpnyCode_" + i).text()) {
				document.getElementById("chkBox_" + i).checked = false;
				totalDueAmount = parseFloat($("#amtDue_" + i).text()) + totalDueAmount;
				var invDate = formatDate($("#invDate_" + i).text());
				$("#invDate_" + i).text(invDate);
				var dueDate = formatDate($("#dueDate_" + i).text());
				$("#dueDate_" + i).text(dueDate);
				$("#row_" + i).show();
			} else {
				$("#row_" + i).hide();
			}
		}
		document.getElementById("totalDueAmountInvoice").value = totalDueAmount.toFixed(2);
		document.getElementById("totalBalanceDueAmt").value = totalDueAmount.toFixed(2);
		document.getElementById("totalPayableAmt").value = "0.00";
	}


});

function searchByPayAgainstInvoice() {
	var noInvoiceFoundFlag = true;
	var invoiceDataValue = $("#serachInvoiceData").val();
	if (invoiceDataValue != '') {
		var table = document.getElementById("paymentInvoiceTable");
		for (var i = 0; i < table.rows.length - 1; i++) {
			if (invoiceDataValue != "" && $("#cmpnyCode").val() == $("#cmpnyCode_" + i).text() && $("#gstInvNo_" + i).text().indexOf(invoiceDataValue) > -1) {
				var invDate = formatDate($("#invDate_" + i).text());
				$("#invDate_" + i).text(invDate);
				var dueDate = formatDate($("#dueDate_" + i).text());
				$("#dueDate_" + i).text(dueDate);
				$("#row_" + i).show();
				noInvoiceFoundFlag = false;
			} else {
				$("#row_" + i).hide();

			}
			if (invoiceDataValue == "") {
				var invDate = formatDate($("#invDate_" + i).text());
				$("#invDate_" + i).text(invDate);
				var dueDate = formatDate($("#dueDate_" + i).text());
				$("#dueDate_" + i).text(dueDate);
				$("#row_" + i).show();
			}
		}

		if (noInvoiceFoundFlag == false) {
			document.getElementById("noInvoiceDataFound").style.display = "none";
		} else {
			document.getElementById("noInvoiceDataFound").style.display = "block";
		}
	} else {
		alert('Please Enter Invoice Number.');
	}
}

function isValidDate(d) {
	return d instanceof Date && !isNaN(d);
}

function searchByPayAgainstInvoiceDateWise() {
	var noDateDataFoundFlag = true;
	var frmDate = new Date($('#aStartDate').val());
	var toDate = new Date($('#aEndDate').val());
	var isFromDateValid = isValidDate(frmDate);
	var isToDateValid = isValidDate(toDate);

	if (isFromDateValid == true && isToDateValid == true) {
		var table = document.getElementById("paymentInvoiceTable");

		for (var i = 0; i < table.rows.length - 1; i++) {
			var chagedDate = formatChangedDate($("#dueDate_" + i).text());
			var dueDate = new Date(chagedDate);
			if (dueDate >= frmDate && dueDate <= toDate && $("#cmpnyCode").val() == $("#cmpnyCode_" + i).text()) {
				var invDate = formatDate($("#invDate_" + i).text());
				$("#invDate_" + i).text(invDate);
				var dueDate = formatDate($("#dueDate_" + i).text());
				$("#dueDate_" + i).text(dueDate);
				$("#row_" + i).show();
				noDateDataFoundFlag = false;
			} else {
				$("#row_" + i).hide();
			}
		}

		if (noDateDataFoundFlag == false) {
			document.getElementById("noInvoiceDataFound").style.display = "none";
		} else {
			document.getElementById("noInvoiceDataFound").style.display = "block";
		}

	} else {
		alert('Please Select From and To date.');
	}
}

function selectAllRowCheckBox() {
	if (document.getElementById("chkBoxAll").checked == true) {
		var table = document.getElementById("paymentInvoiceTable");
		for (var i = 0; i < table.rows.length - 1; i++) {
			document.getElementById("chkBox_" + i).checked = true;
			if ($("#cmpnyCode").val() == $("#cmpnyCode_" + i).text()) {
				var invDate = formatDate($("#invDate_" + i).text());
				$("#invDate_" + i).text(invDate);
				var dueDate = formatDate($("#dueDate_" + i).text());
				$("#dueDate_" + i).text(dueDate);
				$("#row_" + i).show();
			} else {
				$("#row_" + i).hide();
			}

		}
		selectAllFlag = true;
		payableAmt = totalDueAmount.toFixed(2);
		balanceDueAmt = 0.0;
		document.getElementById("totalPayableAmt").value = totalDueAmount.toFixed(2);
		document.getElementById("totalBalanceDueAmt").value = "0.00";
	}
	if (document.getElementById("chkBoxAll").checked == false) {
		var table = document.getElementById("paymentInvoiceTable");
		for (var i = 0; i < table.rows.length - 1; i++) {
			document.getElementById("chkBox_" + i).checked = false;
			if ($("#cmpnyCode").val() == $("#cmpnyCode_" + i).text()) {
				var invDate = formatDate($("#invDate_" + i).text());
				$("#invDate_" + i).text(invDate);
				var dueDate = formatDate($("#dueDate_" + i).text());
				$("#dueDate_" + i).text(dueDate);
				$("#row_" + i).show();
			} else {
				$("#row_" + i).hide();
			}
		}
		payableAmt = 0.0;
		balanceDueAmt = totalDueAmount.toFixed(2);
		document.getElementById("totalPayableAmt").value = "0.00";
		document.getElementById("totalBalanceDueAmt").value = totalDueAmount.toFixed(2);
	}
}

function selectAdvanceAllRowCheckBox() {
	if (document.getElementById("chkBoxAdvanceAll").checked == true) {
		var table = document.getElementById("advTable");
		for (var i = 0; i < table.rows.length; i++) {
			document.getElementById("chkBoxr_" + i).checked = true;
			if ($("#cmpnyCode").val() == $("#cmpnyCode_" + i).text()) {
				$("#row_" + i).show();
			} else {
				$("#row_" + i).hide();
			}
		}
		selectAllFlag = true;

	}
	if (document.getElementById("chkBoxAdvanceAll").checked == false) {
		var table = document.getElementById("advTable");
		for (var i = 0; i < table.rows.length; i++) {
			document.getElementById("chkBoxr_" + i).checked = false;
			if ($("#cmpnyCode").val() == $("#cmpnyCode_" + i).text()) {
				$("#row_" + i).show();
			} else {
				$("#row_" + i).hide();
			}
		}

	}
}

function submitAdvancePaymentConfirmationModal() {
	createAdvanceJsonDataAndPost();
}

function hideAdvancePaymentConfirmationModal() {
	$("#paymentAdvanceConfirmationModal").hide();
	$("#paymentAdvanceModal").hide();
}

function submitPaymentAdvanceData() {
	if (document.getElementById("serachInvoiceDataAdvance").value == '' && document.getElementById("serachPOData").value == '') {
		alert('Please search on Sale Order No. or Purchase Order No.');
	} else if ((document.getElementById("serachInvoiceDataAdvance").value != '' || document.getElementById("serachPOData").value != '') && advancePaymentRowSelectedFlag == false) {
		alert('Please select atleast one row for advance payment.');
	} else if ((document.getElementById("serachInvoiceDataAdvance").value != '' || document.getElementById("serachPOData").value != '') && advancePaymentRowSelectedFlag == true && totalAdvPayableAmt == 0.0) {
		alert('Please select the amount for the selected row.');
	} else {
		totalCheckedAdvPayableAmt = 0;
		userAllComment = "";
		$("#paymentAdvanceModal").hide();
		var tableAdvance = document.getElementById("advTable");
		for (var i = 0; i < tableAdvance.rows.length; i++) {
			if (document.getElementById("chkBoxr_" + i).checked == true && $("#advPayableAmt_" + i).val() != '') {
				if(parseFloat($("#advPayableAmt_" + i).val()) > parseFloat($("#advOrderValue_" + i).text())){  
					alert("advance amount can not be more than Order value");
					return;
				}else{
					var payableAdvRowAmt = parseFloat($("#advPayableAmt_" + i).val());
					totalCheckedAdvPayableAmt = payableAdvRowAmt + parseFloat(totalCheckedAdvPayableAmt);
					if (totalCheckedAdvPayableAmt > 0) {
						$("#paymentAdvanceConfirmationModalMessage").html("Please confirm to initiate advance payment of Rs. " + totalCheckedAdvPayableAmt.toFixed(2));
						$("#paymentAdvanceConfirmationModal").show();
					}
					var userRowComments = ($("#advRemark_" + i).val());
					userAllComment = userAllComment + "," + userRowComments;
				}				
			} else if (document.getElementById("chkBoxr_" + i).checked == true && $("#advPayableAmt_" + i).val() == '') {

				alert('Please fill the payable amount.');
				$("#paymentAdvanceConfirmationModal").hide();
				return;
			} else if (document.getElementById("chkBoxr_" + i).checked == false && $("#advPayableAmt_" + i).val() != '') {

				alert('Please select the checkbox.');
				$("#paymentAdvanceConfirmationModal").hide();
				return;
			}
		}
	}
}
var totalCheckedAdvPayableAmt = 0.0;
var userAllComment = "";

function createAdvanceJsonDataAndPost() {
	if (advancePaymentRowSelectedFlag) {
		if (selectedAdvanceSearchedOption == "Search By Sale Order No") {
			var paymentAdvanceFinalJsonData = {
				"searchCriteria": "soNumber",
				"searchValue": searchedSaleOrdNumber,
				"advanceAmount": totalCheckedAdvPayableAmt,
				"paymentNote": userAllComment
			};
		} else if (selectedAdvanceSearchedOption == "Search By Purchase Order No") {
			var paymentAdvanceFinalJsonData = {
				"searchCriteria": "soNumber",
				"searchValue": searchedSaleOrdNumber,
				"advanceAmount": totalCheckedAdvPayableAmt,
				"paymentNote": userAllComment
			};
		}

		$("#loading-wrapper").show();
		$("#paymentInvoiceModal").hide();
		$("#paymentConfirmationModal").hide();
		$("#paymentAdvanceModal").hide();
		$("#paymentAdvanceConfirmationModal").hide();

		console.log(paymentAdvanceFinalJsonData);
		$.ajax({
			type: "POST",
			url: ACC.config.encodedContextPath + "/my-account/submit-make-payment?CSRFToken=" + ACC.config.CSRFToken,
			data: JSON.stringify(paymentAdvanceFinalJsonData),
			contentType: "application/json; charset=utf-8",
			success:   function  (response) {
				paymentAdvanceArray = [];
				totalAdvPayableAmt = 0.0;
				advancePaymentRowSelectedFlag = false;
				countSelectedAdvRows = 0;
				paymentAdvanceFinalJsonData = {};
				$("#paymentAdvanceConfirmationModal").hide();
				resetAdvanceFields();
				totalCheckedAdvPayableAmt = 0;
				searchedSaleOrdNumber = "";
				userAllComment = "";
				submitPaymentForm(response);
			},
			error: function (response) {
				paymentAdvanceArray = [];
				countSelectedAdvRows = 0;
				advancePaymentRowSelectedFlag = false;
				showAdvancePaymentModal('Transaction Confirmation Status', 'Something went wrong.Please try after some time.');          
				paymentAdvanceFinalJsonData = {};
				resetAdvanceFields();
				totalCheckedAdvPayableAmt = 0;
				userAllComment = "";
				$("#paymentAdvanceConfirmationModal").hide();
				$("#loading-wrapper").hide();
			}
		});
	} else {
		alert('Please click on the checkbox and fill payable amount.');
		return;
	}

}

function resetAdvanceFields() {
	var tableAdvance = document.getElementById("advTable");
	for (var i = 0; i < tableAdvance.rows.length; i++) {
		document.getElementById("chkBoxr_" + i).checked = false;
		document.getElementById("advPayableAmt_" + i).value = "";
		document.getElementById("advRemark_" + i).value = "";
	}

}


function submitPaymentConfirmationModal() {
	createJsonDataAndPost();
}

function hidePaymentConfirmationModal() {
	$("#paymentConfirmationModal").hide();
}

var totalCheckedInvPayableAmt = 0;

function submitPaymentData() {
	totalCheckedInvPayableAmt = 0;
	if (paymentRowSelectedFlag == true) {
		$("#paymentInvoiceModal").hide();
		var table = document.getElementById("paymentInvoiceTable");
		for (var i = 0; i < table.rows.length - 1; i++) {
			if (document.getElementById("chkBox_" + i).checked == true) {
				var payableInvRowAmt = parseFloat($("#amtDue_" + i).text());
				totalCheckedInvPayableAmt = payableInvRowAmt + parseFloat(totalCheckedInvPayableAmt);
			}
		}
		if (totalCheckedInvPayableAmt > 0) {
			$("#paymentConfirmationModalMessage").html("Please confirm to initiate payment of Rs. " + totalCheckedInvPayableAmt.toFixed(2));
			$("#paymentConfirmationModal").show();
		} else {
			alert('Please select atleast one invoice for payment');
		}


	} else {
		alert('Please select atleast one invoice for payment');
	}
}


function createJsonDataAndPost() {
	var table = document.getElementById("paymentInvoiceTable");
	for (var i = 0; i < table.rows.length - 1; i++) {
		if (document.getElementById("chkBox_" + i).checked == true) {
			var paymentJson = {
				"invoiceNumber": $("#invNo_" + i).text()
			};
			paymentArray.push(paymentJson);
			console.log(paymentArray);
		}
	}
	var paymentFinalJsonData = {
		"paymentdetailshistory": paymentArray,
		"searchCriteria": "invoice"
	};
	$("#loading-wrapper").show();
	$("#paymentInvoiceModal").hide();
	$("#paymentConfirmationModal").hide();
	$("#paymentAdvanceModal").hide();
	$("#paymentAdvanceConfirmationModal").hide();

	console.log(paymentFinalJsonData);
	$.ajax({
		type: "POST",
		url: ACC.config.encodedContextPath + "/my-account/submit-make-payment?CSRFToken=" + ACC.config.CSRFToken,
		data: JSON.stringify(paymentFinalJsonData),
		contentType: "application/json; charset=utf-8",
		success:   function  (response) {
			paymentArray = [];
			paymentRowSelectedFlag = false;
			totalCheckedInvPayableAmt = 0;
			submitPaymentForm(response);
		},
		error: function (response) {
			paymentArray = [];
			paymentRowSelectedFlag = false;
			totalCheckedInvPayableAmt = 0;
			$("#loading-wrapper").hide();
			showPaymentInvoiceModal('Transaction Confirmation Status', 'Something went wrong.Please try after some time.');
		}
	});

}
var countSelectedRows = 0;

function submitPaymentForm(responseJson) {
	my_form = document.createElement('FORM');
	my_form.name = 'paymentForm';
	my_form.method = 'POST';
	my_form.action = responseJson.actionUrl;

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'access_token';
	my_tb.value = responseJson.access_token;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'merchant_code';
	my_tb.value = responseJson.merchantCode;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'comp_code';
	my_tb.value = responseJson.companyCode;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'cust_code';
	my_tb.value = responseJson.customerCode;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'jsl_ref_no';
	my_tb.value = responseJson.jslReferenceNumber;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'fi_doc_number';
	my_tb.value = responseJson.fiDocumentNumber;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'po_no';
	my_tb.value = responseJson.poNumber;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'jsl_ref_amt';
	my_tb.value = responseJson.totalAmount;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'invoice_amount';
	my_tb.value = responseJson.invoiceAmount;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'sale_order_no';
	my_tb.value = responseJson.saleOrderNumber;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'payment_source';
	my_tb.value = responseJson.paymentSource;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'fis_yr';
	my_tb.value = responseJson.fiscalYear;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'pmt_type_adv';
	my_tb.value = responseJson.paymentTypeAdvance;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'inv_no';
	my_tb.value = responseJson.invoiceNumber;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'gst_inv_number';
	my_tb.value = responseJson.gstInvoiceNumber;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'pstng_date';
	my_tb.value = responseJson.postingDate;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'doc_date';
	my_tb.value = responseJson.documentDate;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'bus_area';
	my_tb.value = responseJson.busArea;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'name1';
	my_tb.value = responseJson.customerName;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'bldat';
	my_tb.value = responseJson.paymentDate;
	my_form.appendChild(my_tb);

	my_tb = document.createElement('INPUT');
	my_tb.type = 'HIDDEN';
	my_tb.name = 'bank_accno';
	my_tb.value = responseJson.bankAccountNumber;
	my_form.appendChild(my_tb);

	my_form.appendChild(my_tb);
	document.body.appendChild(my_form);
	my_form.submit();
}

function selectChkBoxInvoiceRow(id) {
	if (document.getElementById(id).checked == true) {
		if (countSelectedRows >= 20) {
			document.getElementById(id).checked = false;
			alert('You can not select more than 20 invoices for payment.');
		} else {
			countSelectedRows = countSelectedRows + 1;
			var selectedBoxId = id.split("_")[1];
			var payableRowAmt = $("#amtDue_" + selectedBoxId).text();
			payableAmt = parseFloat(payableRowAmt) + payableAmt;
			document.getElementById("totalPayableAmt").value = payableAmt.toFixed(2);
			balanceDueAmt = totalDueAmount - payableAmt;
			document.getElementById("totalBalanceDueAmt").value = balanceDueAmt.toFixed(2);
			paymentRowSelectedFlag = true;
		}
	} else {
		countSelectedRows = countSelectedRows - 1;
		var selectedBoxId = id.split("_")[1];
		var payableRowAmt = parseFloat($("#amtDue_" + selectedBoxId).text());
		payableAmt = payableAmt - parseFloat(payableRowAmt);
		document.getElementById("totalPayableAmt").value = payableAmt.toFixed(2);
		document.getElementById("totalBalanceDueAmt").value = (parseFloat(balanceDueAmt) + parseFloat(payableRowAmt)).toFixed(2);
		balanceDueAmt = parseFloat(balanceDueAmt) + parseFloat(payableRowAmt);
		if (selectAllFlag == true) {
			document.getElementById("chkBoxAll").checked = false;

		}
	}
}

$('.searchByAdvanceParameters input[type="radio"]').change(function () {

	if (this.value == 'Search By Sale Order No') {
		document.getElementById("serachPOData").disabled = true;
		document.getElementById("serachInvoiceDataAdvance").disabled = false;
		document.getElementById("searchAdvanceInvoiceId").disabled = false;
		document.getElementById("serachPOData").disabled = true;
		document.getElementById("searchAdvancePoId").disabled = true;
		document.getElementById("serachPOData").value = '';
		document.getElementById("noDataFound").style.display = "none";
		document.getElementById("advTable").innerHTML = '';
		document.getElementById("chkBoxAdvanceAll").checked = false;
		selectedAdvanceSearchedOption = "Search By Sale Order No";
	}

	if (this.value == 'Search By Purchase Order No') {
		document.getElementById("serachPOData").disabled = false;
		document.getElementById("serachInvoiceDataAdvance").disabled = true;
		document.getElementById("searchAdvanceInvoiceId").disabled = true;
		document.getElementById("serachInvoiceDataAdvance").value = '';
		document.getElementById("searchAdvancePoId").disabled = false;
		document.getElementById("noDataFound").style.display = "none";
		document.getElementById("advTable").innerHTML = '';
		document.getElementById("chkBoxAdvanceAll").checked = false;
		selectedAdvanceSearchedOption = "Search By Purchase Order No";

	}


});
var searchedSaleOrdNumber = "";

function createTable(json) {
	document.getElementById("advTable").innerHTML = '';
	document.getElementById("chkBoxAdvanceAll").checked = false;
	var table = document.getElementById("advTable");

	json.forEach(function (item, index) {
		var row = table.insertRow(index);
		row.id = "row_" + index;
		var cell0 = row.insertCell(0);
		var checkBox = document.createElement("input");
		checkBox.type = 'checkbox';
		checkBox.id = 'chkBoxr_' + index;
		var id = checkBox.id;
		cell0.appendChild(checkBox);
		checkBox.onclick = function (arg) {
			return function () {
				selectChkBoxAdvanceRow(arg)
			}
		}(id);
		var cell1 = row.insertCell(1);
		cell1.id = "advSaleOrdNo_" + index;
		cell1.innerText = item.saleOrderNumber;
		searchedSaleOrdNumber = item.saleOrderNumber;
		var cell2 = row.insertCell(2);
		cell2.id = "advPurchanseOrdNo_" + index;
		cell2.innerText = item.purchaseOrderNumber;
		var cell3 = row.insertCell(3);
		cell3.id = "gstInvNo_" + index;
		cell3.innerText = item.invoiceNumber;
		cell3.classList.add("hiddenAdvFields");
		var cell4 = row.insertCell(4);
		cell4.id = "advSaleOrdDate_" + index;
		cell4.innerText = formatSaleOrdrDate(item.saleOrderDate);
		var cell5 = row.insertCell(5);
		cell5.id = "advPlant_" + index;
		cell5.innerText = item.plant;
		cell5.classList.add("hiddenAdvFields");
		var cell6 = row.insertCell(6);
		cell6.id = "advCmpyCode_" + index;
		if (item.invoiceCompanyCode == 'JS01') {
			item.invoiceCompanyCode = 'Jindal Stainless (Hisar) Limited';
		}
		if (item.invoiceCompanyCode == 'JS03') {
			item.invoiceCompanyCode = 'Jindal Stainless Limited';
		}
		cell6.innerText = item.companyCode;
		cell6.classList.add("hiddenAdvFields");
		var cell7 = row.insertCell(7);
		cell7.id = "advOrderValue_" + index;
		cell7.innerText = parseFloat(item.saleOrderAmount).toFixed(2);
		var cell8 = row.insertCell(8);
		cell8.id = "advBusinessArea_" + index;
		cell8.innerText = item.businessArea;
		cell8.classList.add("hiddenAdvFields");
		var cell9 = row.insertCell(9);
		var payInput = document.createElement("input");
		payInput.type = 'number';
		payInput.id = 'advPayableAmt_' + index;
		cell9.appendChild(payInput);
		var cell10 = row.insertCell(10);
		var remark = document.createElement("input");
		remark.type = 'text';
		remark.id = 'advRemark_' + index;
		cell10.appendChild(remark);
	});
}
var countSelectedAdvRows = 0;

function selectChkBoxAdvanceRow(id) {
	if (document.getElementById(id).checked == true) {
		if (countSelectedAdvRows >= 1) {
			document.getElementById(id).checked = false;
			alert('You can not select more than one sale order for advance payment.');
		} else {
			var selectedAdvBoxId = id.split("_")[1];
			if ($("#advPayableAmt_" + selectedAdvBoxId).val() == "") {
				alert('Please fill the payable amount first.');
				document.getElementById(id).checked = false;
			} else {
				advancePaymentRowSelectedFlag = true;
				countSelectedAdvRows = countSelectedAdvRows + 1;
				var payableAdvRowAmt = parseFloat($("#advPayableAmt_" + selectedAdvBoxId).val());
				totalAdvPayableAmt = payableAdvRowAmt + parseFloat(totalAdvPayableAmt);
			}

		}
	} else {
		countSelectedAdvRows = countSelectedAdvRows - 1;
		var selectedAdvBoxId = id.split("_")[1];
		var payableAdvRowAmt = parseFloat($("#advPayableAmt_" + selectedAdvBoxId).val());
		totalAdvPayableAmt = parseFloat(totalAdvPayableAmt) - payableAdvRowAmt;
	}

}

function searchByAdvanceInvoice() {
	document.getElementById("advTable").innerHTML = '';
	if (document.getElementById("serachInvoiceDataAdvance").value == "") {
		document.getElementById("showTableAdvance").style.display = "none";
		document.getElementById("noDataFound").style.display = "block";
		document.getElementById("noDataFound").innerHTML = "Please enter Sale Order No.";

	}
	if (document.getElementById("serachInvoiceDataAdvance").value !== "") {
		var invoiceAdvanceValue = document.getElementById("serachInvoiceDataAdvance").value;
		var searchInvoiceJsonData = {
			"searchCriteria": "soNumber",
			"searchValue": invoiceAdvanceValue,
			"companyCode": $("#cmpnyCode").val()
		}
		$.ajax({
			type: "POST",
			url: ACC.config.encodedContextPath + "/my-account/make-adv-payment?CSRFToken=" + ACC.config.CSRFToken,
			data: JSON.stringify(searchInvoiceJsonData),
			contentType: "application/json; charset=utf-8",
			success:   function  (response) {
				if (response.length !== 0) {
					createTable(response);
				} else {
					document.getElementById("noDataFound").style.display = "block";
					document.getElementById("noDataFound").innerHTML = "No Data Found."
				}
			},
			error: function (response) {
				document.getElementById("noDataFound").style.display = "block";
				document.getElementById("noDataFound").innerHTML = "Something went wrong.Please try after some time."
			}
		});
		document.getElementById("showTableAdvance").style.display = "block";
		document.getElementById("noDataFound").style.display = "none";
	}
}

function searchByAdvancePO() {
	document.getElementById("advTable").innerHTML = '';
	if (document.getElementById("serachPOData").value == "") {
		document.getElementById("showTableAdvance").style.display = "none";
		document.getElementById("noDataFound").style.display = "block";
		document.getElementById("noDataFound").innerHTML = "Please enter Purchase Order No.";
	}

	if (document.getElementById("serachPOData").value !== "") {
		var poAdvanceValue = document.getElementById("serachPOData").value;
		var searchPoJsonData = {
			"searchCriteria": "poNumber",
			"searchValue": poAdvanceValue,
			"companyCode": $("#cmpnyCode").val()
		}
		$.ajax({
			type: "POST",
			url: ACC.config.encodedContextPath + "/my-account/make-adv-payment?CSRFToken=" + ACC.config.CSRFToken,
			data: JSON.stringify(searchPoJsonData),
			contentType: "application/json; charset=utf-8",
			success:   function  (response) {
				if (response.length !== 0) {
					createTable(response);
				} else {
					document.getElementById("noDataFound").style.display = "block";
					document.getElementById("noDataFound").innerHTML = "No Data Found."
				}

			},
			error: function (response) {
				document.getElementById("noDataFound").style.display = "block";
				document.getElementById("noDataFound").innerHTML = "Something went wrong.Please try after some time."
			}
		});

		document.getElementById("showTableAdvance").style.display = "block";
		document.getElementById("noDataFound").style.display = "none";
	}
}

function showPaymentInvoiceModal(title, msg) {
	document.getElementById("paymentInvoiceTitle").innerHTML = title;
	document.getElementById("paymentModalMessage").innerHTML = msg;
	$("#paymentInvoiceModal").show();
	$("#paymentConfirmationModal").hide();
	$("#paymentAdvanceModal").hide();
	$("#paymentAdvanceConfirmationModal").hide();
}

function hidePaymentModal() {
	$("#paymentInvoiceModal").hide();
}

function showAdvancePaymentModal(title, msg) {
	document.getElementById("paymentAdvanceTitle").innerHTML = title;
	document.getElementById("advanceModalMessage").innerHTML = msg;
	$("#paymentAdvanceModal").show();
	$("#paymentAdvanceConfirmationModal").hide();
	$("#paymentInvoiceModal").hide();
	$("#paymentConfirmationModal").hide();
}

function hideAdvancePaymentModal() {
	$("#paymentAdvanceModal").hide();
}

function formatSaleOrdrDate(saleOrdDate) {
	var newSaleOrdDate = saleOrdDate.substring(6, 8) + "." + saleOrdDate.substring(4, 6) + "." + saleOrdDate.substring(0, 4);
	return newSaleOrdDate;
}