var complaintResponse = "";
var oneMonthAgo = new Date(
	new Date().getFullYear(),
	new Date().getMonth() - 1,
	new Date().getDate()
);

var twoMonthAgo = new Date(
	new Date().getFullYear(),
	new Date().getMonth() - 2,
	new Date().getDate()
);

window.onload = function () {

	window.dataLayer = window.dataLayer || [];

	function gtag() {
		dataLayer.push(arguments);
	}
	gtag('js', new Date());
	/* alert('gTag called from simmple banner login page.');*/
	gtag('config', 'UA-142673878-1');

	$.ajax({
		type: "GET",
		url: ACC.config.encodedContextPath + "/auction/isAdminDashBoardUser?CSRFToken=" + ACC.config.CSRFToken,
		success: function (response) {
			/*alert(response);*/
			if (response) {
				console.log('auction admin user');
				$("#adminUser").show();
				$("#normalUser").hide();
				$("#complaintAdminUser").hide();

				$.ajax({
					type: "GET",
					url: ACC.config.encodedContextPath + "/reports/customerDashboard?CSRFToken=" + ACC.config.CSRFToken,
					success: function (response) {
						injectData(response);
					},
					error: function (response) {
						/* alert("Something went wrong!");*/
						$("#successDataCheck").hide();
						$('#loading-wrapper').hide();
						$("#failureDataCheck").show();

					}
				});

			} else {

				$.ajax({
					type: "GET",
					url: ACC.config.encodedContextPath + "/isSupportTicketAdminUser?CSRFToken=" + ACC.config.CSRFToken,
					success: function (response) {
						if (response) {
							console.log('complaint admin user');
							$("#adminUser").hide();
							$("#normalUser").hide();
							$("#complaintAdminUser").show();
							$.ajax({
								type: "GET",
								url: ACC.config.encodedContextPath + "/observer/complaints?CSRFToken=" + ACC.config.CSRFToken,
								success: function (response) {
									complaintResponse = response;
									injectComplaintData(response);
									$('#loading-wrapper').hide();
									console.log(response);
								},
								error: function (response) {
									/* alert("Something went wrong!");*/
									$("#successDataCheck").hide();
									$('#loading-wrapper').hide();
									$("#failureDataCheck").show();


								}
							});

						} else {
							console.log('normal user');
							$("#adminUser").hide();
							$("#normalUser").show();
							$("#complaintAdminUser").hide();

						$.ajax({
							type: "GET",
							url: ACC.config.encodedContextPath + "/reports/customerDashboard?CSRFToken=" + ACC.config.CSRFToken,
							success: function (response) {
								injectData(response);
							},
							error: function (response) {
								/* alert("Something went wrong!");*/
								$("#successDataCheck").hide();
								$('#loading-wrapper').hide();
								$("#failureDataCheck").show();


							}
							});
						}
						 injectData(response);
					},
					error: function (response) {
						$("#successDataCheck").hide();
						$('#loading-wrapper').hide();
						$("#failureDataCheck").show();

					}
				});
			}
		},
		error: function (response) {
			alert(response);
		}
	});
}

function round(v) {
	return (v >= 0 || -1) * Math.round(Math.abs(v));
}

function injectComplaintData(json) {
	/*var complaintResponse = {
	        "isSupportTicketAdminUser": true,
	        "complaintList": [{
	                "complaintNumber": "180",
	                "typeOfComplaint": "Service",
	                "presentMaterialCondition": null,
	                "natureOfComplaints": null,
	                "invoiceNumber": null,
	                "invoiceDetails": [{
	                    "batchNumber": "180-AAAAAAA",
	                    "grade": "304",
	                    "thickness": "1.200",
	                    "width": "1250.000",
	                    "length": "2300.000",
	                    "finish": "2B",
	                    "edge": "Trim Edge",
	                    "primaryDefect": "Dent",
	                    "soNumber": "0586005871",
	                    "supplyQuantity": "2.04",
	                    "complaintQuantity": "2",
	                    "plant": "7020",
	                    "invoiceDate": "2016-03-25",
	                    "kamName": "Kartik Patel"
	                }, {
	                    "batchNumber": "180-BBBBBBB",
	                    "grade": "304",
	                    "thickness": "1.200",
	                    "width": "1250.000",
	                    "length": "2300.000",
	                    "finish": "2B",
	                    "edge": "Trim Edge",
	                    "primaryDefect": "Dent",
	                    "soNumber": "0586005871",
	                    "supplyQuantity": "2.04",
	                    "complaintQuantity": "2",
	                    "plant": "7020",
	                    "invoiceDate": "2016-03-25",
	                    "kamName": "Kartik Patel"
	                }],
	                "attachment": null,
	                "materialLocation": null,
	                "contactPersonName": null,
	                "contactNumber": null,
	                "complaintDescription": "jkljlk",
	                "status": "CREATED",
	                "salesOffice": null,
	                "salesGroup": null,
	                "salesDistrict": null,
	                "salesOrg": null,
	                "invoiceCompanyCode": null,
	                "createdOn": "Tue Jul 02 16:41:43 IST 2019",
	                "customerFeedBackComment": "",
	                "customerFeedBackRating": "5",
	                "formattedCreatedOn": "06/01/2019",
	                "kamName": null,
	                "kamId": null,
	                "qualityPersonName": null,
	                "settlementRemarks": null
	            },
	            {
	                "complaintNumber": "181",
	                "typeOfComplaint": "Service",
	                "presentMaterialCondition": null,
	                "natureOfComplaints": null,
	                "invoiceNumber": null,
	                "invoiceDetails": [{
	                    "batchNumber": "181-AAAAAAA",
	                    "grade": "304",
	                    "thickness": "1.200",
	                    "width": "1250.000",
	                    "length": "2300.000",
	                    "finish": "2B",
	                    "edge": "Trim Edge",
	                    "primaryDefect": "Dent",
	                    "soNumber": "0586005871",
	                    "supplyQuantity": "2.04",
	                    "complaintQuantity": "2",
	                    "plant": "7020",
	                    "invoiceDate": "2016-03-25",
	                    "kamName": "Kartik Patel"
	                }, {
	                    "batchNumber": "181-BBBBBBB",
	                    "grade": "304",
	                    "thickness": "1.200",
	                    "width": "1250.000",
	                    "length": "2300.000",
	                    "finish": "2B",
	                    "edge": "Trim Edge",
	                    "primaryDefect": "Dent",
	                    "soNumber": "0586005871",
	                    "supplyQuantity": "2.04",
	                    "complaintQuantity": "2",
	                    "plant": "7020",
	                    "invoiceDate": "2016-03-25",
	                    "kamName": "Kartik Patel"
	                }],
	                "attachment": null,
	                "materialLocation": null,
	                "contactPersonName": null,
	                "contactNumber": null,
	                "complaintDescription": "",
	                "status": "CREATED",
	                "salesOffice": null,
	                "salesGroup": null,
	                "salesDistrict": null,
	                "salesOrg": null,
	                "invoiceCompanyCode": null,
	                "createdOn": "Tue Jul 02 16:41:59 IST 2019",
	                "customerFeedBackComment": "",
	                "customerFeedBackRating": "1",
	                "formattedCreatedOn": "05/30/2019",
	                "kamName": null,
	                "kamId": null,
	                "qualityPersonName": null,
	                "settlementRemarks": null
	            },
	            {
	                "complaintNumber": "189",
	                "typeOfComplaint": "Service",
	                "presentMaterialCondition": null,
	                "natureOfComplaints": null,
	                "invoiceNumber": null,
	                "invoiceDetails": [{
	                    "batchNumber": "189-AAAAAAA",
	                    "grade": "304",
	                    "thickness": "1.200",
	                    "width": "1250.000",
	                    "length": "2300.000",
	                    "finish": "2B",
	                    "edge": "Trim Edge",
	                    "primaryDefect": "Dent",
	                    "soNumber": "0586005871",
	                    "supplyQuantity": "2.04",
	                    "complaintQuantity": "2",
	                    "plant": "7020",
	                    "invoiceDate": "2016-03-25",
	                    "kamName": "Kartik Patel"
	                }, {
	                    "batchNumber": "189-BBBBBBB",
	                    "grade": "304",
	                    "thickness": "1.200",
	                    "width": "1250.000",
	                    "length": "2300.000",
	                    "finish": "2B",
	                    "edge": "Trim Edge",
	                    "primaryDefect": "Dent",
	                    "soNumber": "0586005871",
	                    "supplyQuantity": "2.04",
	                    "complaintQuantity": "2",
	                    "plant": "7020",
	                    "invoiceDate": "2016-03-25",
	                    "kamName": "Kartik Patel"
	                }],
	                "attachment": null,
	                "materialLocation": null,
	                "contactPersonName": null,
	                "contactNumber": null,
	                "complaintDescription": "hjh",
	                "status": "CREATED",
	                "salesOffice": null,
	                "salesGroup": null,
	                "salesDistrict": null,
	                "salesOrg": null,
	                "invoiceCompanyCode": null,
	                "createdOn": "Tue Jul 02 18:07:20 IST 2019",
	                "customerFeedBackComment": "",
	                "customerFeedBackRating": "2",
	                "formattedCreatedOn": "02/02/2019",
	                "kamName": null,
	                "kamId": null,
	                "qualityPersonName": null,
	                "settlementRemarks": null
	            },
	            {
	                "complaintNumber": "192",
	                "typeOfComplaint": "Service",
	                "presentMaterialCondition": null,
	                "natureOfComplaints": null,
	                "invoiceNumber": null,
	                "invoiceDetails": [{
	                    "batchNumber": "192-AAAAAAA",
	                    "grade": "304",
	                    "thickness": "1.200",
	                    "width": "1250.000",
	                    "length": "2300.000",
	                    "finish": "2B",
	                    "edge": "Trim Edge",
	                    "primaryDefect": "Dent",
	                    "soNumber": "0586005871",
	                    "supplyQuantity": "2.04",
	                    "complaintQuantity": "2",
	                    "plant": "7020",
	                    "invoiceDate": "2016-03-25",
	                    "kamName": "Kartik Patel"
	                }, {
	                    "batchNumber": "192-BBBBBBB",
	                    "grade": "304",
	                    "thickness": "1.200",
	                    "width": "1250.000",
	                    "length": "2300.000",
	                    "finish": "2B",
	                    "edge": "Trim Edge",
	                    "primaryDefect": "Dent",
	                    "soNumber": "0586005871",
	                    "supplyQuantity": "2.04",
	                    "complaintQuantity": "2",
	                    "plant": "7020",
	                    "invoiceDate": "2016-03-25",
	                    "kamName": "Kartik Patel"
	                }],
	                "attachment": null,
	                "materialLocation": null,
	                "contactPersonName": null,
	                "contactNumber": null,
	                "complaintDescription": "jh",
	                "status": "CREATED",
	                "salesOffice": null,
	                "salesGroup": null,
	                "salesDistrict": null,
	                "salesOrg": null,
	                "invoiceCompanyCode": null,
	                "createdOn": "Tue Jul 02 18:19:26 IST 2019",
	                "customerFeedBackComment": "",
	                "customerFeedBackRating": "5",
	                "formattedCreatedOn": "07/02/2019",
	                "kamName": null,
	                "kamId": null,
	                "qualityPersonName": null,
	                "settlementRemarks": null
	            },{
	                "complaintNumber": "201",
	                "typeOfComplaint": "Material",
	                "presentMaterialCondition": null,
	                "natureOfComplaints": null,
	                "invoiceNumber": null,
	                "invoiceDetails": [{
	                    "batchNumber": "201-AAAAAAA",
	                    "grade": "304",
	                    "thickness": "1.200",
	                    "width": "1250.000",
	                    "length": "2300.000",
	                    "finish": "2B",
	                    "edge": "Trim Edge",
	                    "primaryDefect": "Dent",
	                    "soNumber": "0586005871",
	                    "supplyQuantity": "2.04",
	                    "complaintQuantity": "2",
	                    "plant": "7020",
	                    "invoiceDate": "2016-03-25",
	                    "kamName": "Kartik Patel"
	                }, {
	                    "batchNumber": "201-BBBBBBB",
	                    "grade": "304",
	                    "thickness": "1.200",
	                    "width": "1250.000",
	                    "length": "2300.000",
	                    "finish": "2B",
	                    "edge": "Trim Edge",
	                    "primaryDefect": "Dent",
	                    "soNumber": "0586005871",
	                    "supplyQuantity": "2.04",
	                    "complaintQuantity": "2",
	                    "plant": "7020",
	                    "invoiceDate": "2016-03-25",
	                    "kamName": "Kartik Patel"
	                }],
	                "attachment": null,
	                "materialLocation": null,
	                "contactPersonName": null,
	                "contactNumber": null,
	                "complaintDescription": "jkljlk",
	                "status": "Closed",
	                "salesOffice": null,
	                "salesGroup": null,
	                "salesDistrict": null,
	                "salesOrg": null,
	                "invoiceCompanyCode": null,
	                "createdOn": "Tue Jul 02 16:41:43 IST 2019",
	                "customerFeedBackComment": "",
	                "customerFeedBackRating": "5",
	                "formattedCreatedOn": "07/02/2019",
	                "kamName": null,
	                "kamId": null,
	                "qualityPersonName": null,
	                "settlementRemarks": null
	            },{
	                "complaintNumber": "202",
	                "typeOfComplaint": "Service",
	                "presentMaterialCondition": null,
	                "natureOfComplaints": null,
	                "invoiceNumber": null,
	                "invoiceDetails": [{
	                    "batchNumber": "202-AAAAAAA",
	                    "grade": "304",
	                    "thickness": "1.200",
	                    "width": "1250.000",
	                    "length": "2300.000",
	                    "finish": "2B",
	                    "edge": "Trim Edge",
	                    "primaryDefect": "Dent",
	                    "soNumber": "0586005871",
	                    "supplyQuantity": "2.04",
	                    "complaintQuantity": "2",
	                    "plant": "7020",
	                    "invoiceDate": "2016-03-25",
	                    "kamName": "Kartik Patel"
	                }, {
	                    "batchNumber": "202-BBBBBBB",
	                    "grade": "304",
	                    "thickness": "1.200",
	                    "width": "1250.000",
	                    "length": "2300.000",
	                    "finish": "2B",
	                    "edge": "Trim Edge",
	                    "primaryDefect": "Dent",
	                    "soNumber": "0586005871",
	                    "supplyQuantity": "2.04",
	                    "complaintQuantity": "2",
	                    "plant": "7020",
	                    "invoiceDate": "2016-03-25",
	                    "kamName": "Kartik Patel"
	                }],
	                "attachment": null,
	                "materialLocation": null,
	                "contactPersonName": null,
	                "contactNumber": null,
	                "complaintDescription": "jkljlk",
	                "status": "Resolved",
	                "salesOffice": null,
	                "salesGroup": null,
	                "salesDistrict": null,
	                "salesOrg": null,
	                "invoiceCompanyCode": null,
	                "createdOn": "Tue Jul 02 16:41:43 IST 2019",
	                "customerFeedBackComment": "",
	                "customerFeedBackRating": "5",
	                "formattedCreatedOn": "07/02/2019",
	                "kamName": null,
	                "kamId": null,
	                "qualityPersonName": null,
	                "settlementRemarks": null
	            },{
	                "complaintNumber": "203",
	                "typeOfComplaint": "Service",
	                "presentMaterialCondition": null,
	                "natureOfComplaints": null,
	                "invoiceNumber": null,
	                "invoiceDetails": [{
	                    "batchNumber": "203-AAAAAAA",
	                    "grade": "304",
	                    "thickness": "1.200",
	                    "width": "1250.000",
	                    "length": "2300.000",
	                    "finish": "2B",
	                    "edge": "Trim Edge",
	                    "primaryDefect": "Dent",
	                    "soNumber": "0586005871",
	                    "supplyQuantity": "2.04",
	                    "complaintQuantity": "2",
	                    "plant": "7020",
	                    "invoiceDate": "2016-03-25",
	                    "kamName": "Kartik Patel"
	                }, {
	                    "batchNumber": "203-BBBBBBB",
	                    "grade": "304",
	                    "thickness": "1.200",
	                    "width": "1250.000",
	                    "length": "2300.000",
	                    "finish": "2B",
	                    "edge": "Trim Edge",
	                    "primaryDefect": "Dent",
	                    "soNumber": "0586005871",
	                    "supplyQuantity": "2.04",
	                    "complaintQuantity": "2",
	                    "plant": "7020",
	                    "invoiceDate": "2016-03-25",
	                    "kamName": "Kartik Patel"
	                }],
	                "attachment": null,
	                "materialLocation": null,
	                "contactPersonName": null,
	                "contactNumber": null,
	                "complaintDescription": "jkljlk",
	                "status": "Open",
	                "salesOffice": null,
	                "salesGroup": null,
	                "salesDistrict": null,
	                "salesOrg": null,
	                "invoiceCompanyCode": null,
	                "createdOn": "Tue Jul 02 16:41:43 IST 2019",
	                "customerFeedBackComment": "",
	                "customerFeedBackRating": "5",
	                "formattedCreatedOn": "07/02/2019",
	                "kamName": null,
	                "kamId": null,
	                "qualityPersonName": null,
	                "settlementRemarks": null
	            },
	        ]
	    };*/
	var complaintTableBody = document.getElementById("complaintTableBody");
	complaintResponse.complaintList.forEach(function (item, index) {
		var row = complaintTableBody.insertRow(index);
		row.id = "complaintTableRow_" + index;
		var cell0 = row.insertCell(0);
		cell0.classList.add("complaint_underline");
		var ticketLink = document.createElement("a");
		ticketLink.onclick = function (arg) {
			return function () {
				showTicketDetails(arg)
			}
		}(index);
		ticketLink.innerHTML = item.complaintNumber;
		ticketLink.id = "complaintNumber_" + index;
		cell0.appendChild(ticketLink);
		var cell1 = row.insertCell(1);
		cell1.innerText = item.typeOfComplaint;
		cell1.id = "typeOfComplaint_" + index;
		cell1.classList.add("typeOfComplaint");
		var cell2 = row.insertCell(2);
		cell2.innerText = item.invoiceNumber;
		cell2.id = "invoiceNumber_" + index;
		cell2.classList.add("invoiceNumber");
		var cell3 = row.insertCell(3);
		if (item.typeOfComplaint == "Material") {
			cell3.innerText = item.natureOfComplaints;
		} else {
			cell3.innerText = "NA";
		}
		cell3.id = "natureOfComplaints_" + index;
		cell3.classList.add("natureOfComplaints");
		var cell4 = row.insertCell(4);
		cell4.innerText = item.formattedCreatedOn;
		cell4.id = "formattedCreatedOn_" + index;
		cell4.classList.add("formattedCreatedOn");
		var cell5 = row.insertCell(5);
		cell5.innerText = item.status;
		cell5.id = "status_" + index;
		cell5.classList.add("status");
	});

	createComplaintGraphs('all');
	$('#loading-wrapper').hide();
}

function searchByDateFilter() {

	var noDateDataFoundFlag = true;
	var frmDate = new Date($('#aStartDate').val());
	var toDate = new Date($('#aEndDate').val());

	var isFromDateValid = isValidDate(frmDate);
	var isToDateValid = isValidDate(toDate);

	if (isFromDateValid == true && isToDateValid == true) {
		var complaintTable = document.getElementById("complaintTableBody");

		for (var i = 0; i < complaintTable.rows.length; i++) {
			var dueDate = new Date($("#createdOn_" + i).text());
			if (dueDate >= frmDate && dueDate <= toDate) {
				$("#row_" + i).show();
				noDateDataFoundFlag = false;
			} else {
				$("#row_" + i).hide();
			}
		}

		if (noDateDataFoundFlag == false) {
			document.getElementById("noInvoiceDataFound").style.display = "none";
		} else {
			document.getElementById("noInvoiceDataFound").style.display = "block";
		}

	} else {
		alert('Please Select From and To date.');
	}

}

function isValidDate(d) {
	return d instanceof Date && !isNaN(d);
}

function showTicketDetails(id) {
	/*alert(id);*/

	/*var complaintResponse = {
	        "isSupportTicketAdminUser": true,
	        "complaintList": [{
	                "complaintNumber": "180",
	                "typeOfComplaint": "Service",
	                "presentMaterialCondition": null,
	                "natureOfComplaints": null,
	                "invoiceNumber": null,
	                "invoiceDetails": [{
	                    "batchNumber": "180-AAAAAAA",
	                    "grade": "304",
	                    "thickness": "1.200",
	                    "width": "1250.000",
	                    "length": "2300.000",
	                    "finish": "2B",
	                    "edge": "Trim Edge",
	                    "primaryDefect": "Dent",
	                    "soNumber": "0586005871",
	                    "supplyQuantity": "2.04",
	                    "complaintQuantity": "2",
	                    "plant": "7020",
	                    "invoiceDate": "2016-03-25",
	                    "kamName": "Kartik Patel"
	                }, {
	                    "batchNumber": "180-BBBBBBB",
	                    "grade": "304",
	                    "thickness": "1.200",
	                    "width": "1250.000",
	                    "length": "2300.000",
	                    "finish": "2B",
	                    "edge": "Trim Edge",
	                    "primaryDefect": "Dent",
	                    "soNumber": "0586005871",
	                    "supplyQuantity": "2.04",
	                    "complaintQuantity": "2",
	                    "plant": "7020",
	                    "invoiceDate": "2016-03-25",
	                    "kamName": "Kartik Patel"
	                }],
	                "attachment": null,
	                "materialLocation": null,
	                "contactPersonName": null,
	                "contactNumber": null,
	                "complaintDescription": "jkljlk",
	                "status": "CREATED",
	                "salesOffice": null,
	                "salesGroup": null,
	                "salesDistrict": null,
	                "salesOrg": null,
	                "invoiceCompanyCode": null,
	                "createdOn": "Tue Jul 02 16:41:43 IST 2019",
	                "customerFeedBackComment": "",
	                "customerFeedBackRating": "5",
	                "formattedCreatedOn": "06/01/2019",
	                "kamName": null,
	                "kamId": null,
	                "qualityPersonName": null,
	                "settlementRemarks": null
	            },
	            {
	                "complaintNumber": "181",
	                "typeOfComplaint": "Service",
	                "presentMaterialCondition": null,
	                "natureOfComplaints": null,
	                "invoiceNumber": null,
	                "invoiceDetails": [{
	                    "batchNumber": "181-AAAAAAA",
	                    "grade": "304",
	                    "thickness": "1.200",
	                    "width": "1250.000",
	                    "length": "2300.000",
	                    "finish": "2B",
	                    "edge": "Trim Edge",
	                    "primaryDefect": "Dent",
	                    "soNumber": "0586005871",
	                    "supplyQuantity": "2.04",
	                    "complaintQuantity": "2",
	                    "plant": "7020",
	                    "invoiceDate": "2016-03-25",
	                    "kamName": "Kartik Patel"
	                }, {
	                    "batchNumber": "181-BBBBBBB",
	                    "grade": "304",
	                    "thickness": "1.200",
	                    "width": "1250.000",
	                    "length": "2300.000",
	                    "finish": "2B",
	                    "edge": "Trim Edge",
	                    "primaryDefect": "Dent",
	                    "soNumber": "0586005871",
	                    "supplyQuantity": "2.04",
	                    "complaintQuantity": "2",
	                    "plant": "7020",
	                    "invoiceDate": "2016-03-25",
	                    "kamName": "Kartik Patel"
	                }],
	                "attachment": null,
	                "materialLocation": null,
	                "contactPersonName": null,
	                "contactNumber": null,
	                "complaintDescription": "",
	                "status": "CREATED",
	                "salesOffice": null,
	                "salesGroup": null,
	                "salesDistrict": null,
	                "salesOrg": null,
	                "invoiceCompanyCode": null,
	                "createdOn": "Tue Jul 02 16:41:59 IST 2019",
	                "customerFeedBackComment": "",
	                "customerFeedBackRating": "1",
	                "formattedCreatedOn": "05/30/2019",
	                "kamName": null,
	                "kamId": null,
	                "qualityPersonName": null,
	                "settlementRemarks": null
	            },
	            {
	                "complaintNumber": "189",
	                "typeOfComplaint": "Service",
	                "presentMaterialCondition": null,
	                "natureOfComplaints": null,
	                "invoiceNumber": null,
	                "invoiceDetails": [{
	                    "batchNumber": "189-AAAAAAA",
	                    "grade": "304",
	                    "thickness": "1.200",
	                    "width": "1250.000",
	                    "length": "2300.000",
	                    "finish": "2B",
	                    "edge": "Trim Edge",
	                    "primaryDefect": "Dent",
	                    "soNumber": "0586005871",
	                    "supplyQuantity": "2.04",
	                    "complaintQuantity": "2",
	                    "plant": "7020",
	                    "invoiceDate": "2016-03-25",
	                    "kamName": "Kartik Patel"
	                }, {
	                    "batchNumber": "189-BBBBBBB",
	                    "grade": "304",
	                    "thickness": "1.200",
	                    "width": "1250.000",
	                    "length": "2300.000",
	                    "finish": "2B",
	                    "edge": "Trim Edge",
	                    "primaryDefect": "Dent",
	                    "soNumber": "0586005871",
	                    "supplyQuantity": "2.04",
	                    "complaintQuantity": "2",
	                    "plant": "7020",
	                    "invoiceDate": "2016-03-25",
	                    "kamName": "Kartik Patel"
	                }],
	                "attachment": null,
	                "materialLocation": null,
	                "contactPersonName": null,
	                "contactNumber": null,
	                "complaintDescription": "hjh",
	                "status": "CREATED",
	                "salesOffice": null,
	                "salesGroup": null,
	                "salesDistrict": null,
	                "salesOrg": null,
	                "invoiceCompanyCode": null,
	                "createdOn": "Tue Jul 02 18:07:20 IST 2019",
	                "customerFeedBackComment": "",
	                "customerFeedBackRating": "2",
	                "formattedCreatedOn": "02/02/2019",
	                "kamName": null,
	                "kamId": null,
	                "qualityPersonName": null,
	                "settlementRemarks": null
	            },
	            {
	                "complaintNumber": "192",
	                "typeOfComplaint": "Service",
	                "presentMaterialCondition": null,
	                "natureOfComplaints": null,
	                "invoiceNumber": null,
	                "invoiceDetails": [{
	                    "batchNumber": "192-AAAAAAA",
	                    "grade": "304",
	                    "thickness": "1.200",
	                    "width": "1250.000",
	                    "length": "2300.000",
	                    "finish": "2B",
	                    "edge": "Trim Edge",
	                    "primaryDefect": "Dent",
	                    "soNumber": "0586005871",
	                    "supplyQuantity": "2.04",
	                    "complaintQuantity": "2",
	                    "plant": "7020",
	                    "invoiceDate": "2016-03-25",
	                    "kamName": "Kartik Patel"
	                }, {
	                    "batchNumber": "192-BBBBBBB",
	                    "grade": "304",
	                    "thickness": "1.200",
	                    "width": "1250.000",
	                    "length": "2300.000",
	                    "finish": "2B",
	                    "edge": "Trim Edge",
	                    "primaryDefect": "Dent",
	                    "soNumber": "0586005871",
	                    "supplyQuantity": "2.04",
	                    "complaintQuantity": "2",
	                    "plant": "7020",
	                    "invoiceDate": "2016-03-25",
	                    "kamName": "Kartik Patel"
	                }],
	                "attachment": null,
	                "materialLocation": null,
	                "contactPersonName": null,
	                "contactNumber": null,
	                "complaintDescription": "jh",
	                "status": "CREATED",
	                "salesOffice": null,
	                "salesGroup": null,
	                "salesDistrict": null,
	                "salesOrg": null,
	                "invoiceCompanyCode": null,
	                "createdOn": "Tue Jul 02 18:19:26 IST 2019",
	                "customerFeedBackComment": "",
	                "customerFeedBackRating": "5",
	                "formattedCreatedOn": "07/02/2019",
	                "kamName": null,
	                "kamId": null,
	                "qualityPersonName": null,
	                "settlementRemarks": null
	            },{
	                "complaintNumber": "201",
	                "typeOfComplaint": "Material",
	                "presentMaterialCondition": null,
	                "natureOfComplaints": null,
	                "invoiceNumber": null,
	                "invoiceDetails": [{
	                    "batchNumber": "201-AAAAAAA",
	                    "grade": "304",
	                    "thickness": "1.200",
	                    "width": "1250.000",
	                    "length": "2300.000",
	                    "finish": "2B",
	                    "edge": "Trim Edge",
	                    "primaryDefect": "Dent",
	                    "soNumber": "0586005871",
	                    "supplyQuantity": "2.04",
	                    "complaintQuantity": "2",
	                    "plant": "7020",
	                    "invoiceDate": "2016-03-25",
	                    "kamName": "Kartik Patel"
	                }, {
	                    "batchNumber": "201-BBBBBBB",
	                    "grade": "304",
	                    "thickness": "1.200",
	                    "width": "1250.000",
	                    "length": "2300.000",
	                    "finish": "2B",
	                    "edge": "Trim Edge",
	                    "primaryDefect": "Dent",
	                    "soNumber": "0586005871",
	                    "supplyQuantity": "2.04",
	                    "complaintQuantity": "2",
	                    "plant": "7020",
	                    "invoiceDate": "2016-03-25",
	                    "kamName": "Kartik Patel"
	                }],
	                "attachment": null,
	                "materialLocation": null,
	                "contactPersonName": null,
	                "contactNumber": null,
	                "complaintDescription": "jkljlk",
	                "status": "Closed",
	                "salesOffice": null,
	                "salesGroup": null,
	                "salesDistrict": null,
	                "salesOrg": null,
	                "invoiceCompanyCode": null,
	                "createdOn": "Tue Jul 02 16:41:43 IST 2019",
	                "customerFeedBackComment": "",
	                "customerFeedBackRating": "5",
	                "formattedCreatedOn": "07/02/2019",
	                "kamName": null,
	                "kamId": null,
	                "qualityPersonName": null,
	                "settlementRemarks": null
	            },{
	                "complaintNumber": "202",
	                "typeOfComplaint": "Service",
	                "presentMaterialCondition": null,
	                "natureOfComplaints": null,
	                "invoiceNumber": null,
	                "invoiceDetails": [{
	                    "batchNumber": "202-AAAAAAA",
	                    "grade": "304",
	                    "thickness": "1.200",
	                    "width": "1250.000",
	                    "length": "2300.000",
	                    "finish": "2B",
	                    "edge": "Trim Edge",
	                    "primaryDefect": "Dent",
	                    "soNumber": "0586005871",
	                    "supplyQuantity": "2.04",
	                    "complaintQuantity": "2",
	                    "plant": "7020",
	                    "invoiceDate": "2016-03-25",
	                    "kamName": "Kartik Patel"
	                }, {
	                    "batchNumber": "202-BBBBBBB",
	                    "grade": "304",
	                    "thickness": "1.200",
	                    "width": "1250.000",
	                    "length": "2300.000",
	                    "finish": "2B",
	                    "edge": "Trim Edge",
	                    "primaryDefect": "Dent",
	                    "soNumber": "0586005871",
	                    "supplyQuantity": "2.04",
	                    "complaintQuantity": "2",
	                    "plant": "7020",
	                    "invoiceDate": "2016-03-25",
	                    "kamName": "Kartik Patel"
	                }],
	                "attachment": null,
	                "materialLocation": null,
	                "contactPersonName": null,
	                "contactNumber": null,
	                "complaintDescription": "jkljlk",
	                "status": "Resolved",
	                "salesOffice": null,
	                "salesGroup": null,
	                "salesDistrict": null,
	                "salesOrg": null,
	                "invoiceCompanyCode": null,
	                "createdOn": "Tue Jul 02 16:41:43 IST 2019",
	                "customerFeedBackComment": "",
	                "customerFeedBackRating": "5",
	                "formattedCreatedOn": "07/02/2019",
	                "kamName": null,
	                "kamId": null,
	                "qualityPersonName": null,
	                "settlementRemarks": null
	            },{
	                "complaintNumber": "203",
	                "typeOfComplaint": "Service",
	                "presentMaterialCondition": null,
	                "natureOfComplaints": null,
	                "invoiceNumber": null,
	                "invoiceDetails": [{
	                    "batchNumber": "203-AAAAAAA",
	                    "grade": "304",
	                    "thickness": "1.200",
	                    "width": "1250.000",
	                    "length": "2300.000",
	                    "finish": "2B",
	                    "edge": "Trim Edge",
	                    "primaryDefect": "Dent",
	                    "soNumber": "0586005871",
	                    "supplyQuantity": "2.04",
	                    "complaintQuantity": "2",
	                    "plant": "7020",
	                    "invoiceDate": "2016-03-25",
	                    "kamName": "Kartik Patel"
	                }, {
	                    "batchNumber": "203-BBBBBBB",
	                    "grade": "304",
	                    "thickness": "1.200",
	                    "width": "1250.000",
	                    "length": "2300.000",
	                    "finish": "2B",
	                    "edge": "Trim Edge",
	                    "primaryDefect": "Dent",
	                    "soNumber": "0586005871",
	                    "supplyQuantity": "2.04",
	                    "complaintQuantity": "2",
	                    "plant": "7020",
	                    "invoiceDate": "2016-03-25",
	                    "kamName": "Kartik Patel"
	                }],
	                "attachment": null,
	                "materialLocation": null,
	                "contactPersonName": null,
	                "contactNumber": null,
	                "complaintDescription": "jkljlk",
	                "status": "Open",
	                "salesOffice": null,
	                "salesGroup": null,
	                "salesDistrict": null,
	                "salesOrg": null,
	                "invoiceCompanyCode": null,
	                "createdOn": "Tue Jul 02 16:41:43 IST 2019",
	                "customerFeedBackComment": "",
	                "customerFeedBackRating": "5",
	                "formattedCreatedOn": "07/02/2019",
	                "kamName": null,
	                "kamId": null,
	                "qualityPersonName": null,
	                "settlementRemarks": null
	            },
	        ]
	    };*/
	var complaintNumber = $("#complaintNumber_" + id).text();
	console.log(complaintResponse);
	document.getElementById("complaintSubMaterialTableBody").innerHTML = "";
	var complaintSubMaterialTableBody = document.getElementById("complaintSubMaterialTableBody");

	document.getElementById("complaintServiceTableBody").innerHTML = "";
	var complaintServiceTableBody = document.getElementById("complaintServiceTableBody");

	if ($("#typeOfComplaint_" + id).text() == "Material") {
		for (var i = 0; i < complaintResponse.complaintList.length; i++) {
			if (complaintResponse.complaintList[i].complaintNumber == complaintNumber) {
				for (var j = 0; j < complaintResponse.complaintList[i].invoiceDetails.length; j++) {
					var row = complaintSubMaterialTableBody.insertRow(j);
					row.id = "complaintSubTableRow_" + j;
					var cell0 = row.insertCell(0);
					cell0.innerText = complaintResponse.complaintList[i].invoiceDetails[j].batchNumber;
					var cell1 = row.insertCell(1);
					cell1.innerText = complaintResponse.complaintList[i].invoiceDetails[j].grade;
					var cell2 = row.insertCell(2);
					cell2.innerText = complaintResponse.complaintList[i].invoiceDetails[j].thickness;
					var cell3 = row.insertCell(3);
					cell3.innerText = complaintResponse.complaintList[i].invoiceDetails[j].width;
					var cell4 = row.insertCell(4);
					cell4.innerText = complaintResponse.complaintList[i].invoiceDetails[j].length;
					var cell5 = row.insertCell(5);
					cell5.innerText = complaintResponse.complaintList[i].invoiceDetails[j].finish;
					var cell6 = row.insertCell(6);
					cell6.innerText = complaintResponse.complaintList[i].invoiceDetails[j].edge;
					var cell7 = row.insertCell(7);
					cell7.innerText = complaintResponse.complaintList[i].invoiceDetails[j].primaryDefect;
					var cell8 = row.insertCell(8);
					cell8.innerText = complaintResponse.complaintList[i].invoiceDetails[j].soNumber;
					var cell9 = row.insertCell(9);
					cell9.innerText = complaintResponse.complaintList[i].invoiceDetails[j].supplyQuantity;
					var cell10 = row.insertCell(10);
					cell10.innerText = complaintResponse.complaintList[i].invoiceDetails[j].complaintQuantity;
					var cell11 = row.insertCell(11);
					cell11.innerText = complaintResponse.complaintList[i].invoiceDetails[j].plant;
					var cell12 = row.insertCell(12);
					cell12.innerText = complaintResponse.complaintList[i].invoiceDetails[j].invoiceDate;
					var cell13 = row.insertCell(13);
					cell13.innerText = complaintResponse.complaintList[i].invoiceDetails[j].kamName;
				}
			}
		}
		document.getElementById("materialTable").style.display = "block";
		document.getElementById("serviceTable").style.display = "none";
	} else if ($("#typeOfComplaint_" + id).text() == "Service") {
		for (var i = 0; i < complaintResponse.complaintList.length; i++) {
			if (complaintResponse.complaintList[i].complaintNumber == complaintNumber) {
				var row = complaintServiceTableBody.insertRow(0);
				row.id = "complaintSubServiceTableRow_" + i;
				var cell0 = row.insertCell(0);
				cell0.innerText = complaintResponse.complaintList[i].natureOfComplaints;
				var cell1 = row.insertCell(1);
				cell1.innerText = complaintResponse.complaintList[i].complaintDescription;

			}
		}
		document.getElementById("materialTable").style.display = "none";
		document.getElementById("serviceTable").style.display = "block";
	}
}

function injectData(json) {
	var accountTableBody = document.getElementById("accountTableBody");
	json.accountSummary.forEach(function (item, index) {
		var row = accountTableBody.insertRow(index);
		row.id = "accTableRow_" + index;
		var cell0 = row.insertCell(0);
		cell0.id = "companyCode_" + index;
		if(item.companyCode=='JS01'){
			cell0.innerText = 'JSHL';
			}
			if(item.companyCode=='JS03'){
			cell0.innerText = 'JSL';
			}
		cell0.classList.add("companyCode_");
		var cell1 = row.insertCell(1);
		cell1.innerText = item.osBal;
		cell1.id = "osBal_" + index;
		cell1.classList.add("osBal");
		/* var cell1 = row.insertCell(1);
		 cell1.innerText = item.creditLimit;
		 cell1.id = "creditLimit_" + index;
		 cell1.classList.add("creditLimit");*/
		var cell2 = row.insertCell(2);
		cell2.innerText = item.overDue;
		cell2.id = "overDue_" + index;
		cell2.classList.add("overDue");
		var cell3 = row.insertCell(3);
		cell3.innerText = item.overDueLess30;
		cell3.id = "overDueLess30_" + index;
		cell3.classList.add("overDueLess30");
		var cell4 = row.insertCell(4);
		cell4.innerText = item.overDueBetween60;
		cell4.id = "overDueBetween60_" + index;
		cell4.classList.add("overDueBetween60");
		var cell5 = row.insertCell(5);
		cell5.innerText = item.overDueGreater60;
		cell5.id = "overDueGreater60_" + index;
		cell5.classList.add("overDueGreater60");
	});

	var billingSummaryTableBody = document.getElementById("billingSummaryTableBody");
	json.billingSummary.forEach(function (item, index) {
		var row = billingSummaryTableBody.insertRow(index);
		row.id = "billingSummaryRow_" + index;
		var cell0 = row.insertCell(0);
		cell0.innerText = item.series;
		cell0.id = "billSeries_" + index;
		cell0.classList.add("billSeries");
		var cell1 = row.insertCell(1);
		cell1.innerText = item.yesterdaySale;
		cell1.id = "billYesterdaySale_" + index;
		cell1.classList.add("billYesterdaySale");
		var cell2 = row.insertCell(2);
		cell2.innerText = item.mtd;
		cell2.id = "billMtd_" + index;
		cell2.classList.add("billMtd");
		/* var cell3 = row.insertCell(3);
		 cell3.innerText = item.ytd;
		 cell3.id = "billYtd_" + index;
		 cell3.classList.add("billYtd");*/
	});

	var orderBookingTableBody = document.getElementById("orderBookingTableBody");
	json.orderBooking.forEach(function (item, index) {
		var row = orderBookingTableBody.insertRow(index);
		row.id = "orderBookingRow_" + index;
		var cell0 = row.insertCell(0);
		cell0.innerText = item.series;
		cell0.id = "orderBookingSeries_" + index;
		cell0.classList.add("orderBookingSeries");
		var cell1 = row.insertCell(1);
		cell1.innerText = item.yesterdayBooking;
		cell1.id = "orderBookingYesterdayBooking_" + index;
		cell1.classList.add("orderBookingYesterdayBooking");
		var cell2 = row.insertCell(2);
		cell2.innerText = item.mtd;
		cell2.id = "orderBookingMtd_" + index;
		cell2.classList.add("orderBookingMtd");
		/*var cell3 = row.insertCell(3);
		cell3.innerText = item.ytd;
		cell3.id = "orderBookingYtd_" + index;
		cell3.classList.add("orderBookingYtd");*/
	});

	var popPlantTableBody = document.getElementById("popPlantTableBody");
	json.popPlant.forEach(function (item, index) {
		var row = popPlantTableBody.insertRow(index);
		row.id = "popPlantRow_" + index;
		var cell0 = row.insertCell(0);
		cell0.innerText = item.series;
		cell0.id = "popPlantSeries_" + index;
		cell0.classList.add("popPlantSeries");
		var cell1 = row.insertCell(1);
		cell1.innerText = item.crap;
		cell1.id = "popPlantCrap_" + index;
		cell1.classList.add("popPlantCrap");
		var cell2 = row.insertCell(2);
		cell2.innerText = item.hrap;
		cell2.id = "popPlantHrap_" + index;
		cell2.classList.add("popPlantHrap");
		var cell3 = row.insertCell(3);
		cell3.innerText = item.plate;
		cell3.id = "popPlantPlate_" + index;
		cell3.classList.add("popPlantPlate");
		var cell4 = row.insertCell(4);
		cell4.innerText = item.hrBlack;
		cell4.id = "popPlantHrBlack_" + index;
		cell4.classList.add("popPlantHrBlack");
		var cell5 = row.insertCell(5);
		cell5.id = "totalPopPlantValue_" + index;
	});

	var popYardTableBody = document.getElementById("popYardTableBody");
	json.popYard.forEach(function (item, index) {
		var row = popYardTableBody.insertRow(index);
		row.id = "popYardRow_" + index;
		var cell0 = row.insertCell(0);
		cell0.innerText = item.series;
		cell0.id = "popYardSeries_" + index;
		cell0.classList.add("popYardSeries");
		var cell1 = row.insertCell(1);
		cell1.innerText = item.crap;
		cell1.id = "popYardCrap_" + index;
		cell1.classList.add("popYardCrap");
		var cell2 = row.insertCell(2);
		cell2.innerText = item.hrap;
		cell2.id = "popYardHrap_" + index;
		cell2.classList.add("popYardHrap");
		var cell3 = row.insertCell(3);
		cell3.innerText = item.plate;
		cell3.id = "popYardPlate_" + index;
		cell3.classList.add("popYardPlate");
		var cell4 = row.insertCell(4);
		cell4.innerText = item.hrBlack;
		cell4.id = "popYardHrBlack_" + index;
		cell4.classList.add("popYardHrBlack");
		var cell5 = row.insertCell(5);
		cell5.id = "totalPopYardValue_" + index;
	});

	createGraphs();
	$('#loading-wrapper').hide();
	
	

	if ($.cookie("loggedInUser")==null) {	
		ACC.colorbox.open("",{
	        inline:true,
	        transition: "elastic", // fade,none,elastic
	        width: "75%",
	        height: "75%",
	        href: "#cyberCrimeIntimationMessage",
	        width:"620px",
	        scrolling: true,
	        onComplete: function(){
	        	$(this).colorbox.resize();
	        }
	    });
		$.cookie("loggedInUser", true, {expires: new Date().addHours(24), secure: true});
	}
}

Date.prototype.addHours = function(h) {
	  this.setTime(this.getTime() + (h*60*60*1000));
	  return this;
	}

function filterOnComplaintTypeData() {
	var selectedComplaintType = document.getElementById("cmplaintType").value;
	document.getElementById("materialTable").innerHTML = "";
	document.getElementById("serviceTable").innerHTML = "";
	createComplaintGraphs(selectedComplaintType);
	document.getElementById("statusType").value = "all";
	document.getElementById("ageingType").value = "all";
}

function filterOnAgeingData() {
	var selectedFilterOnAgeing = document.getElementById("ageingType").value;
	document.getElementById("materialTable").innerHTML = "";
	document.getElementById("serviceTable").innerHTML = "";
	createComplaintGraphs(selectedFilterOnAgeing);
	document.getElementById("cmplaintType").value = "all";
	document.getElementById("statusType").value = "all";
}

function filterOnStatusData() {
	var selectedFilterOnStatus = document.getElementById("statusType").value;
	document.getElementById("materialTable").innerHTML = "";
	document.getElementById("serviceTable").innerHTML = "";
	createComplaintGraphs(selectedFilterOnStatus);
	document.getElementById("cmplaintType").value = "all";
	document.getElementById("ageingType").value = "all";
}

function resetcomplaintData() {
	createComplaintGraphs("all");
	document.getElementById("cmplaintType").value = "all";
	document.getElementById("statusType").value = "all";
	document.getElementById("ageingType").value = "all";
}

function createComplaintGraphs(type) {
	var complaintTable = document.getElementById("complaintTableBody");
	var graphMap = new Map();
	var openStatusMap = new Map();
	var inProcessStatusMap = new Map();
	var reslovedStatusMap = new Map();
	var closedStatusMap = new Map();
	var createdStatusMap = new Map();
	var openTicketSum = 0;
	var inProcessTicketSum = 0;
	var resolveTicketSum = 0;
	var closedTicketSum = 0;
	var createdTicketSum = 0;

	var count = 0;
	for (var i = 0; i < complaintTable.rows.length; i++) {
		if (type == 'all') {
			var ticketStatus = document.getElementById("status_" + i).innerText;
			if (ticketStatus == "Open") {
				openTicketSum = openTicketSum + 1;
				openStatusMap.set(ticketStatus, openTicketSum);

			} else if (ticketStatus == "In Process") {
				inProcessTicketSum = inProcessTicketSum + 1;
				inProcessStatusMap.set(ticketStatus, inProcessTicketSum);
			} else if (ticketStatus == "Resolved") {
				resolveTicketSum = resolveTicketSum + 1;
				reslovedStatusMap.set(ticketStatus, resolveTicketSum);
			} else if (ticketStatus == "Closed") {
				closedTicketSum = closedTicketSum + 1;
				closedStatusMap.set(ticketStatus, closedTicketSum);
			} else if (ticketStatus == "CREATED") {
				createdTicketSum = createdTicketSum + 1;
				createdStatusMap.set(ticketStatus, createdTicketSum);
			}
			count = count + 1;
		} else if (type == 'materialType' && $("#typeOfComplaint_" + i).text() == "Material") {
			var ticketStatus = document.getElementById("status_" + i).innerText;
			if (ticketStatus == "Open") {
				openTicketSum = openTicketSum + 1;
				openStatusMap.set(ticketStatus, openTicketSum);
			} else if (ticketStatus == "In Process") {
				inProcessTicketSum = inProcessTicketSum + 1;
				inProcessStatusMap.set(ticketStatus, inProcessTicketSum);
			} else if (ticketStatus == "Resolved") {
				resolveTicketSum = resolveTicketSum + 1;
				reslovedStatusMap.set(ticketStatus, resolveTicketSum);
			} else if (ticketStatus == "Closed") {
				closedTicketSum = closedTicketSum + 1;
				closedStatusMap.set(ticketStatus, closedTicketSum);
			} else if (ticketStatus == "CREATED") {
				createdTicketSum = createdTicketSum + 1;
				createdStatusMap.set(ticketStatus, createdTicketSum);
			}
			count = count + 1;
		} else if (type == 'serviceType' && $("#typeOfComplaint_" + i).text() == "Service") {
			var ticketStatus = document.getElementById("status_" + i).innerText;
			if (ticketStatus == "Open") {
				openTicketSum = openTicketSum + 1;
				openStatusMap.set(ticketStatus, openTicketSum);

			} else if (ticketStatus == "In Process") {
				inProcessTicketSum = inProcessTicketSum + 1;
				inProcessStatusMap.set(ticketStatus, inProcessTicketSum);
			} else if (ticketStatus == "Resolved") {
				resolveTicketSum = resolveTicketSum + 1;
				reslovedStatusMap.set(ticketStatus, resolveTicketSum);
			} else if (ticketStatus == "Closed") {
				closedTicketSum = closedTicketSum + 1;
				closedStatusMap.set(ticketStatus, closedTicketSum);
			} else if (ticketStatus == "CREATED") {
				createdTicketSum = createdTicketSum + 1;
				createdStatusMap.set(ticketStatus, createdTicketSum);
			}
			count = count + 1;
		} else if (type == 'open' && $("#status_" + i).text() == "Open") {
			var ticketStatus = document.getElementById("status_" + i).innerText;
			if (ticketStatus == "Open") {
				openTicketSum = openTicketSum + 1;
				openStatusMap.set(ticketStatus, openTicketSum);

			} else if (ticketStatus == "In Process") {
				inProcessTicketSum = inProcessTicketSum + 1;
				inProcessStatusMap.set(ticketStatus, inProcessTicketSum);
			} else if (ticketStatus == "Resolved") {
				resolveTicketSum = resolveTicketSum + 1;
				reslovedStatusMap.set(ticketStatus, resolveTicketSum);
			} else if (ticketStatus == "Closed") {
				closedTicketSum = closedTicketSum + 1;
				closedStatusMap.set(ticketStatus, closedTicketSum);
			} else if (ticketStatus == "CREATED") {
				createdTicketSum = createdTicketSum + 1;
				createdStatusMap.set(ticketStatus, createdTicketSum);
			}
			count = count + 1;
		} else if (type == 'inprocess' && $("#status_" + i).text() == "In Process") {
			var ticketStatus = document.getElementById("status_" + i).innerText;
			if (ticketStatus == "Open") {
				openTicketSum = openTicketSum + 1;
				openStatusMap.set(ticketStatus, openTicketSum);

			} else if (ticketStatus == "In Process") {
				inProcessTicketSum = inProcessTicketSum + 1;
				inProcessStatusMap.set(ticketStatus, inProcessTicketSum);
			} else if (ticketStatus == "Resolved") {
				resolveTicketSum = resolveTicketSum + 1;
				reslovedStatusMap.set(ticketStatus, resolveTicketSum);
			} else if (ticketStatus == "Closed") {
				closedTicketSum = closedTicketSum + 1;
				closedStatusMap.set(ticketStatus, closedTicketSum);
			} else if (ticketStatus == "CREATED") {
				createdTicketSum = createdTicketSum + 1;
				createdStatusMap.set(ticketStatus, createdTicketSum);
			}
			count = count + 1;
		} else if (type == 'created' && $("#status_" + i).text() == "CREATED") {
			var ticketStatus = document.getElementById("status_" + i).innerText;
			if (ticketStatus == "Open") {
				openTicketSum = openTicketSum + 1;
				openStatusMap.set(ticketStatus, openTicketSum);

			} else if (ticketStatus == "In Process") {
				inProcessTicketSum = inProcessTicketSum + 1;
				inProcessStatusMap.set(ticketStatus, inProcessTicketSum);
			} else if (ticketStatus == "Resolved") {
				resolveTicketSum = resolveTicketSum + 1;
				reslovedStatusMap.set(ticketStatus, resolveTicketSum);
			} else if (ticketStatus == "Closed") {
				closedTicketSum = closedTicketSum + 1;
				closedStatusMap.set(ticketStatus, closedTicketSum);
			} else if (ticketStatus == "CREATED") {
				createdTicketSum = createdTicketSum + 1;
				createdStatusMap.set(ticketStatus, createdTicketSum);
			}
			count = count + 1;
		} else if (type == 'resolved' && $("#status_" + i).text() == "Resolved") {
			var ticketStatus = document.getElementById("status_" + i).innerText;
			if (ticketStatus == "Open") {
				openTicketSum = openTicketSum + 1;
				openStatusMap.set(ticketStatus, openTicketSum);

			} else if (ticketStatus == "In Process") {
				inProcessTicketSum = inProcessTicketSum + 1;
				inProcessStatusMap.set(ticketStatus, inProcessTicketSum);
			} else if (ticketStatus == "Resolved") {
				resolveTicketSum = resolveTicketSum + 1;
				reslovedStatusMap.set(ticketStatus, resolveTicketSum);
			} else if (ticketStatus == "Closed") {
				closedTicketSum = closedTicketSum + 1;
				closedStatusMap.set(ticketStatus, closedTicketSum);
			} else if (ticketStatus == "CREATED") {
				createdTicketSum = createdTicketSum + 1;
				createdStatusMap.set(ticketStatus, createdTicketSum);
			}
			count = count + 1;
		} else if (type == 'closed' && $("#status_" + i).text() == "Closed") {
			var ticketStatus = document.getElementById("status_" + i).innerText;
			if (ticketStatus == "Open") {
				openTicketSum = openTicketSum + 1;
				openStatusMap.set(ticketStatus, openTicketSum);

			} else if (ticketStatus == "In Process") {
				inProcessTicketSum = inProcessTicketSum + 1;
				inProcessStatusMap.set(ticketStatus, inProcessTicketSum);
			} else if (ticketStatus == "Resolved") {
				resolveTicketSum = resolveTicketSum + 1;
				reslovedStatusMap.set(ticketStatus, resolveTicketSum);
			} else if (ticketStatus == "Closed") {
				closedTicketSum = closedTicketSum + 1;
				closedStatusMap.set(ticketStatus, closedTicketSum);
			} else if (ticketStatus == "CREATED") {
				createdTicketSum = createdTicketSum + 1;
				createdStatusMap.set(ticketStatus, createdTicketSum);
			}
			count = count + 1;
		} else if (type == 'less30' && (new Date($("#formattedCreatedOn_" + i).text()) > oneMonthAgo && new Date($("#formattedCreatedOn_" + i).text()) < new Date())) {
			var ticketStatus = document.getElementById("status_" + i).innerText;
			if (ticketStatus == "Open") {
				openTicketSum = openTicketSum + 1;
				openStatusMap.set(ticketStatus, openTicketSum);
			} else if (ticketStatus == "In Process") {
				inProcessTicketSum = inProcessTicketSum + 1;
				inProcessStatusMap.set(ticketStatus, inProcessTicketSum);
			} else if (ticketStatus == "Resolved") {
				resolveTicketSum = resolveTicketSum + 1;
				reslovedStatusMap.set(ticketStatus, resolveTicketSum);
			} else if (ticketStatus == "Closed") {
				closedTicketSum = closedTicketSum + 1;
				closedStatusMap.set(ticketStatus, closedTicketSum);
			} else if (ticketStatus == "CREATED") {
				createdTicketSum = createdTicketSum + 1;
				createdStatusMap.set(ticketStatus, createdTicketSum);
			}
			count = count + 1;
		} else if (type == 'betw30to60' && (new Date($("#formattedCreatedOn_" + i).text()) > twoMonthAgo && new Date($("#formattedCreatedOn_" + i).text()) < oneMonthAgo)) {
			var ticketStatus = document.getElementById("status_" + i).innerText;
			if (ticketStatus == "Open") {
				openTicketSum = openTicketSum + 1;
				openStatusMap.set(ticketStatus, openTicketSum);
			} else if (ticketStatus == "In Process") {
				inProcessTicketSum = inProcessTicketSum + 1;
				inProcessStatusMap.set(ticketStatus, inProcessTicketSum);
			} else if (ticketStatus == "Resolved") {
				resolveTicketSum = resolveTicketSum + 1;
				reslovedStatusMap.set(ticketStatus, resolveTicketSum);
			} else if (ticketStatus == "Closed") {
				closedTicketSum = closedTicketSum + 1;
				closedStatusMap.set(ticketStatus, closedTicketSum);
			} else if (ticketStatus == "CREATED") {
				createdTicketSum = createdTicketSum + 1;
				createdStatusMap.set(ticketStatus, createdTicketSum);
			}
			count = count + 1;
		} else if (type == 'greater60' && new Date($("#formattedCreatedOn_" + i).text()) < twoMonthAgo) {
			var ticketStatus = document.getElementById("status_" + i).innerText;
			if (ticketStatus == "Open") {
				openTicketSum = openTicketSum + 1;
				openStatusMap.set(ticketStatus, openTicketSum);
			} else if (ticketStatus == "In Process") {
				inProcessTicketSum = inProcessTicketSum + 1;
				inProcessStatusMap.set(ticketStatus, inProcessTicketSum);
			} else if (ticketStatus == "Resolved") {
				resolveTicketSum = resolveTicketSum + 1;
				reslovedStatusMap.set(ticketStatus, resolveTicketSum);
			} else if (ticketStatus == "Closed") {
				closedTicketSum = closedTicketSum + 1;
				closedStatusMap.set(ticketStatus, closedTicketSum);
			} else if (ticketStatus == "CREATED") {
				createdTicketSum = createdTicketSum + 1;
				createdStatusMap.set(ticketStatus, createdTicketSum);
			}
			count = count + 1;
		}

	}

	var trace1 = {
		x: Array.from(openStatusMap.keys()),
		y: Array.from(openStatusMap.values()),
		name: 'OPEN',
		type: 'bar'
	};

	var trace2 = {
		x: Array.from(inProcessStatusMap.keys()),
		y: Array.from(inProcessStatusMap.values()),
		name: 'IN PROCESS',
		type: 'bar'
	};
	var trace3 = {
		x: Array.from(reslovedStatusMap.keys()),
		y: Array.from(reslovedStatusMap.values()),
		name: 'RESOLVED',
		type: 'bar'
	};
	var trace4 = {
		x: Array.from(closedStatusMap.keys()),
		y: Array.from(closedStatusMap.values()),
		name: 'CLOSED',
		type: 'bar'
	};
	var trace5 = {
		x: Array.from(createdStatusMap.keys()),
		y: Array.from(createdStatusMap.values()),
		name: 'CREATED',
		type: 'bar'
	};

	var data = [trace1, trace2, trace3, trace4, trace5];


	var layout = {
		height: 300,
		width: 1200,
		margin: {
			l: 100,
			r: 10,
			b: 50,
			t: 120,
			pad: 0
		},
		showlegend: true,
		/*   legend: {"orientation": "h"},*/
		title: '<b>Complaint Ticket</b>',
		"titlefont": {
			"size": 16
		},
		yaxis: {
			title: {
				text: '<b>Count</b>',
				font: {
					size: 12
				}
			}
		},
		xaxis: {
			title: {
				text: '<b>Status</b>',
				font: {
					size: 12
				}
			}
		}
	}

	Plotly.newPlot("containerComplaint", data, layout, {
		showSendToCloud: true,
		displayModeBar: false
	});


}

function createGraphs() {
	// Account Summary

	var accountSummaryTable = document.getElementById("accountTableBody");
	var graphMap = new Map();
	var overdueSum = 0.0;
	var outstandingSum = 0.0;
	var overDueless30Sum = 0.0;
	var overduebetween30To60Sum = 0.0;
	var overdurgreater60Sum = 0.0;
	var accountSummaryChk = false;


	for (var i = 0; i < accountSummaryTable.rows.length; i++) {
		var outStandingAmt = document.getElementById("osBal_" + i).innerText;
		var overDueAmt = document.getElementById("overDue_" + i).innerText;
		var less30 = document.getElementById("overDueLess30_" + i).innerText;
		var betw30To60 = document.getElementById("overDueBetween60_" + i).innerText;
		var greater60 = document.getElementById("overDueGreater60_" + i).innerText;
		
		var outstandingSumWithComma = round(outStandingAmt).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		document.getElementById("osBal_" + i).innerText = outstandingSumWithComma;
	
		var overdueSumWithComma = round(overDueAmt).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		document.getElementById("overDue_" + i).innerText = overdueSumWithComma;
	
		var overDueless30SumWithComma = round(less30).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		document.getElementById("overDueLess30_" + i).innerText = overDueless30SumWithComma;
	
		var overduebetween30To60SumWithComma = round(betw30To60).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		document.getElementById("overDueBetween60_" + i).innerText = overduebetween30To60SumWithComma;
	
		var overdurgreater60SumSumWithComma = round(greater60).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		document.getElementById("overDueGreater60_" + i).innerText = overdurgreater60SumSumWithComma;

		outstandingSum = outstandingSum + parseFloat(outStandingAmt);
		overdueSum = overdueSum + parseFloat(overDueAmt);
		overDueless30Sum = overDueless30Sum + parseFloat(less30);
		overduebetween30To60Sum = overduebetween30To60Sum + parseFloat(betw30To60);
		overdurgreater60Sum = overdurgreater60Sum + parseFloat(greater60);
		if (overDueless30Sum > 0 || overduebetween30To60Sum > 0 || overdurgreater60Sum > 0) {
			accountSummaryChk = true;
		}
	}



//	document.getElementById("reportTitleGpsTrackingSummary").innerHTML = '<div class="reportTitleData"><b>GPS Tracking Report</b></div>';
//	document.getElementById("containerGpsTrackingSummary").innerHTML = '<div class="noGraphData"><marquee class="mrq_data">No data available for graphical representation</marquee></div>';
	if (overDueless30Sum <= 0 && overduebetween30To60Sum <= 0 && overdurgreater60Sum <= 0) {
		document.getElementById("reportTitleAccountSummary").innerHTML = '<div class="reportTitleData"><b>Account Summary Report</b></div>';
		document.getElementById("containerAccountSummary").innerHTML = '<div class="noGraphData"><marquee class="mrq_data">No data available for graphical representation</marquee></div>';

	} else {
		graphMap.set('Ageing < 30', Math.round(overDueless30Sum));
		graphMap.set('Ageing 30 - 60', Math.round(overduebetween30To60Sum));
		graphMap.set('Ageing > 60', Math.round(overdurgreater60Sum));

		var ageingArray = [];
		var ageingAmountArray = [];
		for (var a of graphMap.keys()) {
			ageingArray.push(a);
		}
		for (var b of graphMap.values()) {
			ageingAmountArray.push(b);
		}

		var data = [{
			values: ageingAmountArray,
			labels: ageingArray,
			marker: {
				colors: [
					'#f2476a',
					'#add8e6',
					'#90ee90',
					'#eb2d3a',
					'#fb654e',
					'#FFCC99',
					'#99FFCC',
					'#CCCC99',
					'#CCCCCC',
					'#CCCCFF',
					'#CCFF99',
					'#CCFFCC',
					'#CCFFFF',
					'#FFCCCC',
					'#FFFF99',
					'#FFCCFF',
					'#FFFFCC',
				]
			},
			type: 'pie'
		}];


		var layout = {
			height: 350,
			width: 450,
			margin: {
				l: 10,
				r: 50,
				b: 50,
				t: 120
			},
			showlegend: true,
			legend: {
				"orientation": "h"
			},
			title: '<b>Account Summary Report</b>',
			"titlefont": {
				"size": 14
			}
		}


		Plotly.newPlot("containerAccountSummary", data, layout, {
			showSendToCloud: true,
			displayModeBar: false
		});

	}


	// Billing  Summary Report

	var billingTable = document.getElementById("billingSummaryTableBody");
	var graphMap = new Map();
	var mtdBalChk = false; var totalYstrdaySale = 0 ; var totalMTD = 0;
	for (var i = 0; i < billingTable.rows.length; i++) {
		var series = document.getElementById("billSeries_" + i).innerText;
		var ystrdySale = Math.round(parseFloat(document.getElementById("billYesterdaySale_" + i).innerText));
		/* var ytd = Math.round(parseFloat(document.getElementById("billYtd_" + i).innerText));*/
		var mtdBal = Math.round(parseFloat(document.getElementById("billMtd_" + i).innerText));
		
		totalYstrdaySale = totalYstrdaySale + parseInt(ystrdySale);
		totalMTD = totalMTD + parseInt(mtdBal);
		
		document.getElementById("totalYesrterDaySale").innerText = totalYstrdaySale;
		document.getElementById("totalMTDSale").innerText = totalMTD;

		if (mtdBal !== 0) {
			mtdBalChk = true;
		}

		document.getElementById("billYesterdaySale_" + i).innerText = ystrdySale;
		/*document.getElementById("billYtd_" + i).innerText = ytd;*/
		document.getElementById("billMtd_" + i).innerText = mtdBal;

		if (!graphMap.has(series)) {
			graphMap.set(series, mtdBal);
		} else {
			var sum = graphMap.get(series);
			sum = sum + mtdBal;
			graphMap.set(series, sum);
		}
	}

	if (mtdBalChk == false) {
		document.getElementById("reportTitleBillingSummary").innerHTML = '<div class="reportTitleData"><b>Billing Summary Report</b></div>';
		document.getElementById("containerBillingSummary").innerHTML = '<div class="noGraphData"><marquee class="mrq_data">No data available for graphical representation</<marquee></div>';

	} else {
		var series = [];
		var mtdBal = [];
		for (var a of graphMap.keys()) {
			series.push(a);
		}
		for (var b of graphMap.values()) {
			mtdBal.push(b);
		}

		/*var data = [
		       {
		              values: [100,200],
		              labels: [300 , 400],
		              type: 'pie'
		       }
		];*/

		var data = [{
			values: mtdBal,
			labels: series,
			marker: {
				colors: [
					'#f2476a',
					'#add8e6',
					'#90ee90',
					'#eb2d3a',
					'#fb654e',
					'#FFCC99',
					'#99FFCC',
					'#CCCC99',
					'#CCCCCC',
					'#CCCCFF',
					'#CCFF99',
					'#CCFFCC',
					'#CCFFFF',
					'#FFCCCC',
					'#FFFF99',
					'#FFCCFF',
					'#FFFFCC',
				]
			},
			type: 'pie'
		}];


		var layout = {
			height: 350,
			width: 450,
			margin: {
				l: 10,
				r: 50,
				b: 50,
				t: 120
			},
			showlegend: true,
			legend: {
				"orientation": "h"
			},
			title: '<b>Billing Summary Report</b>',
			"titlefont": {
				"size": 14
			}
		}

		Plotly.newPlot("containerBillingSummary", data, layout, {
			showSendToCloud: true,
			displayModeBar: false
		});

	}


	// Order Booking Report  

	var orderBookingTable = document.getElementById("orderBookingTableBody");
	var graphMap = new Map();
	var mtdBalChk = false; var totalYstrdayBookingSale = 0 ; var totalBookingMTDSale = 0;
	for (var i = 0; i < orderBookingTable.rows.length; i++) {
		var series = document.getElementById("orderBookingSeries_" + i).innerText;
		var ystrdyBooking = Math.round(parseFloat(document.getElementById("orderBookingYesterdayBooking_" + i).innerText));
		var mtdBal = Math.round(parseFloat(document.getElementById("orderBookingMtd_" + i).innerText));
		/*var ytd = Math.round(parseFloat(document.getElementById("orderBookingYtd_" + i).innerText));*/
		
		totalYstrdayBookingSale = totalYstrdayBookingSale + parseInt(ystrdyBooking);
		totalBookingMTDSale = totalBookingMTDSale + parseInt(mtdBal);
		
		document.getElementById("totalYesrterBookingSale").innerText = totalYstrdayBookingSale;
		document.getElementById("totalMTDBookingSale").innerText = totalBookingMTDSale;

		if (mtdBal !== 0) {
			mtdBalChk = true;
		}

		document.getElementById("orderBookingYesterdayBooking_" + i).innerText = ystrdyBooking;
		document.getElementById("orderBookingMtd_" + i).innerText = mtdBal;
		/*document.getElementById("orderBookingYtd_" + i).innerText = ytd;*/


		if (!graphMap.has(series)) {
			graphMap.set(series, mtdBal);
		} else {
			var sum = graphMap.get(series);
			sum = sum + mtdBal;
			graphMap.set(series, sum);
		}
	}

	if (mtdBalChk == false) {
		document.getElementById("reportTitleOrderBookingSummary").innerHTML = '<div class="reportTitleData"><b>Order Booking Report</b></div>';
		document.getElementById("containerOrderBooking").innerHTML = '<div class="noGraphData"><marquee class="mrq_data">No data available for graphical representation</marquee></div>';

	} else {
		var seriesSaleArray = [];
		var mtdBalsSalesArray = [];
		for (var a of graphMap.keys()) {
			seriesSaleArray.push(a);
		}
		for (var b of graphMap.values()) {
			mtdBalsSalesArray.push(b);
		}

		/*var data = [
		       {
		              values: [100,200],
		              labels: [300 , 400],
		              type: 'pie'
		       }
		];*/

		var data = [{
			values: mtdBalsSalesArray,
			labels: seriesSaleArray,
			marker: {
				colors: [
					'#f2476a',
					'#add8e6',
					'#90ee90',
					'#eb2d3a',
					'#fb654e',
					'#FFCC99',
					'#99FFCC',
					'#CCCC99',
					'#CCCCCC',
					'#CCCCFF',
					'#CCFF99',
					'#CCFFCC',
					'#CCFFFF',
					'#FFCCCC',
					'#FFFF99',
					'#FFCCFF',
					'#FFFFCC',
				]
			},
			type: 'pie'
		}];


		var layout = {
			height: 350,
			width: 450,
			margin: {
				l: 10,
				r: 50,
				b: 50,
				t: 120
			},
			showlegend: true,
			legend: {
				"orientation": "h"
			},
			title: '<b>Order Booking Report</b>',
			"titlefont": {
				"size": 14
			}
		}

		Plotly.newPlot("containerOrderBooking", data, layout, {
			showSendToCloud: true,
			displayModeBar: false
		});
	}


	//Pop Plant Report

	var popPlantTable = document.getElementById("popPlantTableBody");
	var crapMap = new Map();
	var hrapMap = new Map();
	var plateMap = new Map();
	var hrMap = new Map(); 
	var popPlantChk = false; var totalCRAPSale = 0 ; var totalHRAPSale = 0 ; var totalPLATESale = 0 ; var totalHRBLACKSale = 0 ;
	var totalAllRowsCountValue = 0;
	for (var i = 0; i < popPlantTable.rows.length; i++) {
		var totalAllSeriesSaleRowWise = 0;
		var series = document.getElementById("popPlantSeries_" + i).innerText;
		var crap = Math.round(parseFloat(document.getElementById("popPlantCrap_" + i).innerText));
		var hrap = Math.round(parseFloat(document.getElementById("popPlantHrap_" + i).innerText));
		var plate = Math.round(parseFloat(document.getElementById("popPlantPlate_" + i).innerText));
		var hrBlack = Math.round(parseFloat(document.getElementById("popPlantHrBlack_" + i).innerText));

		totalAllSeriesSaleRowWise = crap + hrap + plate + hrBlack ; 
		document.getElementById("totalPopPlantValue_"+ i).innerText = totalAllSeriesSaleRowWise;
		document.getElementById("totalPopPlantValue_"+ i).style.fontWeight = "bold";
		totalAllRowsCountValue =  totalAllRowsCountValue + totalAllSeriesSaleRowWise ; 
		
		totalCRAPSale = totalCRAPSale + parseInt(crap);
		totalHRAPSale = totalHRAPSale + parseInt(hrap);
		totalPLATESale = totalPLATESale + parseInt(plate);
		totalHRBLACKSale = totalHRBLACKSale + parseInt(hrBlack);
		
		
		document.getElementById("totalPlantCRAPSale").innerText = totalCRAPSale;
		document.getElementById("totalPlantHRAPSale").innerText = totalHRAPSale;
		document.getElementById("totalPlantPlateSale").innerText = totalPLATESale;
		document.getElementById("totalPlantHRBLACKSale").innerText = totalHRBLACKSale;
		
		document.getElementById("totalAllPlantRowsSale").innerText = totalAllRowsCountValue;

		if (crap !== 0 || hrap !== 0 || plate !== 0 || hrBlack !== 0) {
			popPlantChk = true;
		}

		document.getElementById("popPlantCrap_" + i).innerText = crap;
		document.getElementById("popPlantHrap_" + i).innerText = hrap;
		document.getElementById("popPlantPlate_" + i).innerText = plate;
		document.getElementById("popPlantHrBlack_" + i).innerText = hrBlack;

		if (!crapMap.has(series)) {
			crapMap.set(series, crap);
		} else {
			var sum = crapMap.get(series);
			sum = sum + crap;
			crapMap.set(series, sum);
		}
		if (!hrapMap.has(series)) {
			hrapMap.set(series, hrap);
		} else {
			var sum = hrapMap.get(series);
			sum = sum + hrap;
			hrapMap.set(series, sum);
		}
		if (!plateMap.has(series)) {
			plateMap.set(series, plate);
		} else {
			var sum = plateMap.get(series);
			sum = sum + plate;
			plateMap.set(series, sum);
		}
		if (!hrMap.has(series)) {
			hrMap.set(series, hrBlack);
		} else {
			var sum = hrMap.get(series);
			sum = sum + hrBlack;
			hrMap.set(series, sum);
		}
	}

	if (popPlantChk == false) {
		document.getElementById("reportTitlePopPlantSummary").innerHTML = '<div class="reportTitleData"><b>Pending Order Position (Plant)</b></div>';
		document.getElementById("containerPopPlant").innerHTML = '<div class="noGraphData"><marquee class="mrq_data">No data available for graphical representation</marquee></div>';

	} else {
		var trace1 = {
			x: Array.from(crapMap.keys()),
			y: Array.from(crapMap.values()),
			name: 'CRAP',
			type: 'bar'
		};

		var trace2 = {
			x: Array.from(hrapMap.keys()),
			y: Array.from(hrapMap.values()),
			name: 'HRAP',
			type: 'bar'
		};
		var trace3 = {
			x: Array.from(plateMap.keys()),
			y: Array.from(plateMap.values()),
			name: 'PLATE',
			type: 'bar'
		};
		var trace4 = {
			x: Array.from(hrMap.keys()),
			y: Array.from(hrMap.values()),
			name: 'HR BLACK',
			type: 'bar'
		};

		var data = [trace1, trace2, trace3, trace4];


		var layout = {
			height: 300,
			width: 600,
			margin: {
				l: 100,
				r: 10,
				b: 50,
				t: 70,
				pad: 0
			},
			showlegend: true,
			/*   legend: {"orientation": "h"},*/
			title: '<b>Pending Order Position (Plant)</b>',
			"titlefont": {
				"size": 14
			},
			yaxis: {
				title: {
					text: '<b>Quantity (MT)</b>',
					font: {
						size: 12
					}
				}
			},
			xaxis: {
				title: {
					text: '<b>Series</b>',
					font: {
						size: 12
					}
				}
			}
		}

		Plotly.newPlot("containerPopPlant", data, layout, {
			showSendToCloud: true,
			displayModeBar: false
		});


	}


	/*var data = [
	      {
	             values: [100,200],
	             labels: [300 , 400],
	             type: 'pie'
	      }
	];*/


	//Pop Yard Report

	var popYardTable = document.getElementById("popYardTableBody");
	var crapMap = new Map();
	var hrapMap = new Map();
	var plateMap = new Map();
	var hrMap = new Map();
	var popYardChk = false;var totalYardCRAPSale = 0 ; var totalYardHRAPSale = 0 ; var totalYardPLATESale = 0 ; var totalYardHRBLACKSale = 0 ;
	var totalAllRowsYardCountValue = 0;
	for (var i = 0; i < popYardTable.rows.length; i++) {
		var totalAllSeriesYardSaleRowWise = 0;
		var series = document.getElementById("popYardSeries_" + i).innerText;
		var crap = Math.round(parseFloat(document.getElementById("popYardCrap_" + i).innerText));
		var hrap = Math.round(parseFloat(document.getElementById("popYardHrap_" + i).innerText));
		var plate = Math.round(parseFloat(document.getElementById("popYardPlate_" + i).innerText));
		var hrBlack = Math.round(parseFloat(document.getElementById("popYardHrBlack_" + i).innerText));
		
		totalAllSeriesYardSaleRowWise = crap + hrap + plate + hrBlack ; 
		document.getElementById("totalPopYardValue_"+ i).innerText = totalAllSeriesYardSaleRowWise;
		document.getElementById("totalPopYardValue_"+ i).style.fontWeight = "bold";
		totalAllRowsYardCountValue =  totalAllRowsYardCountValue + totalAllSeriesYardSaleRowWise ; 
		
		totalYardCRAPSale = totalYardCRAPSale + parseInt(crap);
		totalYardHRAPSale = totalYardHRAPSale + parseInt(hrap);
		totalYardPLATESale = totalYardPLATESale + parseInt(plate);
		totalYardHRBLACKSale = totalYardHRBLACKSale + parseInt(hrBlack);
		
		
		document.getElementById("totalYardCRAPSale").innerText = totalYardCRAPSale;
		document.getElementById("totalYardHRAPSale").innerText = totalYardHRAPSale;
		document.getElementById("totalYardPlateSale").innerText = totalYardPLATESale;
		document.getElementById("totalYardHRBLACKSale").innerText = totalYardHRBLACKSale;
		
		document.getElementById("totalAllYardRowsSale").innerText = totalAllRowsYardCountValue;

		if (crap !== 0 || hrap !== 0 || plate !== 0 || hrBlack !== 0) {
			popYardChk = true;
		}

		document.getElementById("popYardCrap_" + i).innerText = crap;
		document.getElementById("popYardHrap_" + i).innerText = hrap;
		document.getElementById("popYardPlate_" + i).innerText = plate;
		document.getElementById("popYardHrBlack_" + i).innerText = hrBlack;

		if (!crapMap.has(series)) {
			crapMap.set(series, crap);
		} else {
			var sum = crapMap.get(series);
			sum = sum + crap;
			crapMap.set(series, sum);
		}
		if (!hrapMap.has(series)) {
			hrapMap.set(series, hrap);
		} else {
			var sum = hrapMap.get(series);
			sum = sum + hrap;
			hrapMap.set(series, sum);
		}
		if (!plateMap.has(series)) {
			plateMap.set(series, plate);
		} else {
			var sum = plateMap.get(series);
			sum = sum + plate;
			plateMap.set(series, sum);
		}
		if (!hrMap.has(series)) {
			hrMap.set(series, hrBlack);
		} else {
			var sum = hrMap.get(series);
			sum = sum + hrBlack;
			hrMap.set(series, sum);
		}
	}

	if (popYardChk == false) {
		document.getElementById("reportTitlePopYardSummary").innerHTML = '<div class="reportTitleData"><b>Pending Order Position (Yard)</b></div>';
		document.getElementById("containerPopYard").innerHTML = '<div class="noGraphData"><marquee class="mrq_data">No data available for graphical representation</marquee></div>';

	} else {
		var trace1 = {
			x: Array.from(crapMap.keys()),
			y: Array.from(crapMap.values()),
			name: 'CRAP',
			type: 'bar'
		};

		var trace2 = {
			x: Array.from(hrapMap.keys()),
			y: Array.from(hrapMap.values()),
			name: 'HRAP',
			type: 'bar'
		};
		var trace3 = {
			x: Array.from(plateMap.keys()),
			y: Array.from(plateMap.values()),
			name: 'PLATE',
			type: 'bar'
		};
		var trace4 = {
			x: Array.from(hrMap.keys()),
			y: Array.from(hrMap.values()),
			name: 'HR BLACK',
			type: 'bar'
		};

		var data = [trace1, trace2, trace3, trace4];


		var layout = {
			height: 300,
			width: 600,
			margin: {
				l: 100,
				r: 10,
				b: 50,
				t: 70,
				pad: 0
			},
			showlegend: true,
			/*   legend: {"orientation": "h"},*/
			title: '<b>Pending Order Position (Yard)</b>',
			"titlefont": {
				"size": 14
			},
			yaxis: {
				title: {
					text: '<b>Quantity (MT)</b>',
					font: {
						size: 12
					}
				}
			},
			xaxis: {
				title: {
					text: '<b>Series</b>',
					font: {
						size: 12
					}
				}
			}
		}

		Plotly.newPlot("containerPopYard", data, layout, {
			showSendToCloud: true,
			displayModeBar: false
		});
	}


	/*var data = [
	      {
	             values: [100,200],
	             labels: [300 , 400],
	             type: 'pie'
	      }
	];*/


}