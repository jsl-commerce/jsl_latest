function login() {
    var userName = document.forms["loginForm"]["userName"].value;
    var password = document.forms["loginForm"]["paswd"].value;
    if (userName === '' || password === '') {
        document.getElementById('errorLogin').innerHTML = 'Please enter username and password.';
        // document.getElementById('#myModal').style.display = block;
    } else {
        document.getElementById('gstLoginId').disabled = false;
    }
}

$(function() {
    $('#email, #pwd').keyup(function() {
        if ($(this).val().length == '') {
            document.getElementById('gstLoginId').disabled = false;
        } else {
            document.getElementById('gstLoginId').disabled = true;
        }
    });
});