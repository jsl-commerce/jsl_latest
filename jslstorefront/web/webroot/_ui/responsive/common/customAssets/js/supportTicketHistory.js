window.onload = function () {
	$("#loading-wrapper").hide();
	$(".materialTypeComplaint").show();
	$(".serviceTypeComplaint").hide();
	$("#feedBackModal").hide();
};


function toggleAccordion(id) {
	$(id).toggle();
}

function rateFeedBack(id) {
	var selectedRateId = id.split("_")[1];
	openFeedBackModel(selectedRateId);
}

function setComplaintNumber(id) {
	localStorage.removeItem("complaintNumber");
	var selectedRateId = id.split("_")[1];
	var complaintNumber = $("#ticketID_" + selectedRateId).text().trim();
	localStorage.setItem("complaintNumber", complaintNumber);
}

function closeModal() {
	$("#feedBackModal").hide();
	$("#comment").val("");
	$("#firstStar").removeClass("addStarColor");
	$("#twoStar").removeClass("addStarColor");
	$("#threetStar").removeClass("addStarColor");
	$("#fourStar").removeClass("addStarColor");
	$("#fiveStar").removeClass("addStarColor");
	starCount = 0;
}

function openFeedBackModel(id) {
	$("#ratingModal").val(id);
	$("#feedBackModal").show();
}

var starCount = 0;

function starClicked(id) {
	if ($('#' + id).is('.addStarColor') == true) {
		if (id == 'firstStar') {
			$('#twoStar').removeClass("addStarColor");
			$('#threetStar').removeClass("addStarColor");
			$('#fourStar').removeClass("addStarColor");
			$('#fiveStar').removeClass("addStarColor");
			starCount = 1;
		}
		if (id == 'twoStar') {
			$('#threetStar').removeClass("addStarColor");
			$('#fourStar').removeClass("addStarColor");
			$('#fiveStar').removeClass("addStarColor");
			starCount = 2;
		}
		if (id == 'threetStar') {
			$('#fourStar').removeClass("addStarColor");
			$('#fiveStar').removeClass("addStarColor");
			starCount = 3;
		}
		if (id == 'fourStar') {
			$('#fiveStar').removeClass("addStarColor");
			starCount = 4;
		}
		if (id == 'fiveStar') {
			starCount = 5;
		}
	} else {
		if (id == 'firstStar') {
			$('#' + id).addClass("addStarColor");
			starCount = 1;
		} else if (id == 'twoStar') {
			$('#' + id).addClass("addStarColor");
			$('#firstStar').addClass("addStarColor");
			starCount = 2;
		} else if (id == 'threetStar') {
			$('#' + id).addClass("addStarColor");
			$('#twoStar').addClass("addStarColor");
			$('#firstStar').addClass("addStarColor");
			starCount = 3;
		} else if (id == 'fourStar') {
			$('#' + id).addClass("addStarColor");
			$('#threetStar').addClass("addStarColor");
			$('#twoStar').addClass("addStarColor");
			$('#firstStar').addClass("addStarColor");
			starCount = 4;
		} else if (id == 'fiveStar') {
			$('#' + id).addClass("addStarColor");
			$('#fourStar').addClass("addStarColor");
			$('#threetStar').addClass("addStarColor");
			$('#twoStar').addClass("addStarColor");
			$('#firstStar').addClass("addStarColor");
			starCount = 5;
		}

	}

}

function submitFeedback() {
	$("#loading-wrapper").show();
	if (starCount != 0) {
		var modalId = $("#ratingModal").val();
		var complaintId = $("#serviceticketID_" + modalId).text().trim();
		var feedBackcomment = $("#comment").val();
		var ratingData = {
			"complaintNumber": complaintId,
			"customerFeedBackComment": feedBackcomment,
			"customerFeedBackRating": starCount
		}
		$.ajax({
			type: "POST",
			url: ACC.config.encodedContextPath + "/my-account/submitFeedBack?CSRFToken=" + ACC.config.CSRFToken,
			data: JSON.stringify(ratingData),
			contentType:   "application/json; charset=utf-8",
			dataType:   "json",
			success:   function  (response) {
				$("#loading-wrapper").hide();
				$("#comment").val("");
				$("#feedBackModal").hide();
				$("#rateFeedback_" + modalId).addClass("isDisabled");
				$("#rateFeedback_" + modalId).html("FeedBack Submitted");
				$("#firstStar").removeClass("addStarColor");
				$("#twoStar").removeClass("addStarColor");
				$("#threetStar").removeClass("addStarColor");
				$("#fourStar").removeClass("addStarColor");
				$("#fiveStar").removeClass("addStarColor");
				starCount = 0;
				$("#feedBackServiceTitle").html("Status");
				$("#feedBackServiceModalMessage").html("FeedBack submitted successfully for complaint Number : " + complaintId);
				$("#feedbackServiceSubmitModal").show();


			},
			error: function (response) {
				alert('Something went Wrong.Feedback not submitted.Please try again later.');
				$("#comment").val("");
				$("#feedBackModal").hide();
				$("#firstStar").removeClass("addStarColor");
				$("#twoStar").removeClass("addStarColor");
				$("#threetStar").removeClass("addStarColor");
				$("#fourStar").removeClass("addStarColor");
				$("#fiveStar").removeClass("addStarColor");
				starCount = 0;
			}
		});
	} else {
		alert('Please select at least one rating');
	}


}

$(function () {
	$('#complaintTypeHistory').change(function () {
		if (this.value == 'Service') {

			$(".materialTypeComplaint").hide();
			$(".serviceTypeComplaint").show();
		} else {

			$(".materialTypeComplaint").show();
			$(".serviceTypeComplaint").hide();
		}
	});
});

function hidefeedbackServiceModal() {
	$("#feedbackServiceSubmitModal").hide();
}

function formatDate(date) {
	date = date.replace("IST", "");
	var d = new Date(date),
		month = '' + (d.getMonth() + 1),
		day = '' + d.getDate(),
		year = d.getFullYear();

	if (month.length < 2) month = '0' + month;
	if (day.length < 2) day = '0' + day;

	return [month, day, year].join('/');
}

function hideIntrnalFeedbackModal() {
	$("input[name='optradio']").prop("checked", false);
	$("#internalFeedbackSubmitModal").hide();
}

var internalFeedbackComplaintNumber = "";
var selectedFeedBackRateId = "";

function shareInternalStatus(id) {
	localStorage.removeItem("complaintNumber");
	var selectedRateId = id.split("_")[1];
	selectedFeedBackRateId = selectedRateId;
	var complaintNumber = $("#ticketID_" + selectedRateId).text().trim();
	internalFeedbackComplaintNumber = complaintNumber;
	localStorage.setItem("complaintNumber", complaintNumber);
	$("input[name='optradio']").prop("checked", false);
	$("#internalFeedbackSubmitModal").show();

}

function submitIntrnalFeedbackStatus() {
	$("#internalFeedbackSubmitModal").hide();
	$("#loading-wrapper").show();
	var internalFeedbackStatus = {
		"complaintNumber": internalFeedbackComplaintNumber,
		"customerSatisfaction": $("input[name='optradio']:checked").val()

	}
	$.ajax({
		type: "POST",
		url: ACC.config.encodedContextPath + "/my-account/submitCustomerSatisfaction?CSRFToken=" + ACC.config.CSRFToken,
		data: JSON.stringify(internalFeedbackStatus),
		contentType:   "application/json; charset=utf-8",
		dataType:   "json",
		success:   function  (response) {
			$("#loading-wrapper").hide();
			$("#internalFeedbackSubmitModal").hide();
			var msg = "Feedback submitted Successfully for the complaint Number : " + internalFeedbackComplaintNumber;
			popUp(msg, 3000, "green");
			document.getElementById("submitBtnInternalFeedback").disabled = true;
			if ($("input[name='optradio']:checked").val() == "Accepted") {
				$("#internalFeedback_" + selectedFeedBackRateId).addClass('feedBackColor');
				$("#internalFeedback_" + selectedFeedBackRateId).html("FeedBack Submitted");
			}
			internalFeedbackComplaintNumber = "";
			localStorage.removeItem("complaintNumber");
			selectedFeedBackRateId = "";

		},
		error: function (response) {
			$("#loading-wrapper").hide();
			$("#internalFeedbackSubmitModal").hide();
			var msg = "Something went wrong. Please try after some time.";
			popUp(msg, 3000, "red");
			internalFeedbackComplaintNumber = "";
			localStorage.removeItem("complaintNumber");
			document.getElementById("submitBtnInternalFeedback").disabled = false;
			selectedFeedBackRateId = "";
		}
	});

}

function popUp(msg, duration, color) {
	var snackbar = document.getElementById("snackBar");
	snackbar.innerHTML = msg;
	snackbar.style.backgroundColor = color;
	snackbar.className = "show";
	setTimeout(function () {
		snackbar.className = snackBar.className.replace("show", "");
	}, duration);
}

function feedbackSelection() {
	document.getElementById("submitBtnInternalFeedback").disabled = false;

}

function viewSettlementRemark(id, remark, remarkType) {
	var selectedRemarkId = id.split("_")[1];
	if(remarkType=='materialTypeRemark'){
		$("#settlementRemarkModalTitle").html("Settlement Remark for Complaint Number : " + $('#ticketID_' + selectedRemarkId).text().trim());
	}
	if(remarkType=='serviceTypeRemark'){
		$("#settlementRemarkModalTitle").html("Settlement Remark for Complaint Number : " + $('#serviceticketID_' + selectedRemarkId).text().trim());
	}
	$("#settlementRemarkModalMessage").html(remark);
	$("#settlementRemarkModal").show();
}

function hidesettlementRemarkModal() {
	$("#settlementRemarkModal").hide();
}