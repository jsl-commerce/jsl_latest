var questionArray = [];
var map = new Map();

map.set('Q1', "");
map.set('Q2', "");
map.set('Q3', "");
map.set('Q4', "");
map.set('Q5', "");
map.set('Q6', "");
map.set('Q7', "");
map.set('Q8', "");
map.set('Q9', "");
map.set('Q10', "");
map.set('Q11', "");
map.set('Q12', "");
map.set('Q13', "");

$('.Q1 input').change(function () {
	map.set('Q1', $("input[type='radio'][name='optradio1']:checked").val());
});
$('.Q2 input').change(function () {
	map.set('Q2', $("input[type='radio'][name='optradio2']:checked").val());
});
$('.Q3 input').change(function () {
	map.set('Q3', $("input[type='radio'][name='optradio3']:checked").val());
});
$('.Q4 input').change(function () {
	map.set('Q4', $("input[type='radio'][name='optradio4']:checked").val());
});
$('.Q5 input').change(function () {
	map.set('Q5', $("input[type='radio'][name='optradio5']:checked").val());
});
$('.Q6 input').change(function () {
	map.set('Q6', $("input[type='radio'][name='optradio6']:checked").val());
});
$('.Q7 input').change(function () {
	map.set('Q7', $("input[type='radio'][name='optradio7']:checked").val());
});
$('.Q8 input').change(function () {
	map.set('Q8', $("input[type='radio'][name='optradio8']:checked").val());
});
$('.Q9 input').change(function () {
	map.set('Q9', $("input[type='radio'][name='optradio9']:checked").val());
});
$('.Q10 input').change(function () {
	map.set('Q10', $("input[type='radio'][name='optradio10']:checked").val());
});
$('.Q11 input').change(function () {
	map.set('Q11', $("input[type='radio'][name='optradio11']:checked").val());
});
$('.Q12 input').change(function () {
	map.set('Q12', $("input[type='radio'][name='optradio12']:checked").val());
});
$('.Q13 input').change(function () {
	map.set('Q13', $("input[type='radio'][name='optradio13']:checked").val());
});


function submitFeedbackData() {
	if (map.get("Q1") == "") {
		alert('Kindly submit all the mandatory (*) question.');
	} else {
		var feedbackQuestionsJson = {
			"q1": map.get("Q1"),
			"q2": map.get("Q2"),
			"q3": map.get("Q3"),
			"q4": map.get("Q4"),
			"q5": map.get("Q5"),
			"q6": map.get("Q6"),
			"q7": map.get("Q7"),
			"q8": map.get("Q8"),
			"q9": map.get("Q9"),
			"q10": map.get("Q10"),
			"q11": map.get("Q11"),
			"q12": map.get("Q12"),
			"q13": map.get("Q13"),
			"comments": $('#custFeedback').val().trim(),
			"complaintNumber": localStorage.getItem("complaintNumber")
		};

		$.ajax({
			type: "POST",
			url: ACC.config.encodedContextPath + "/my-account/submitFeedBackForm?CSRFToken=" + ACC.config.CSRFToken,
			data: JSON.stringify(feedbackQuestionsJson),
			contentType: "application/json; charset=utf-8",
			success:   function  (response) {
				if (response == true) {
					$("#feedBackTitle").html("Status");
					$("#feedBackModalMessage").html("FeedBack submitted successfully for complaint Number : " + localStorage.getItem("complaintNumber"));
					$("#feedbackSubmitModal").show();
					map.clear();
					localStorage.removeItem("complaintNumber");
				} else {
					console.log('');
				}

			},
			error: function (response) {
				localStorage.removeItem("complaintNumber");
			}
		});

	}
}

function hidefeedbackModal() {
	$("#feedbackSubmitModal").hide();
}

function showAllOptionalFeedBack() {
	if (document.getElementById("optionalChxBox").checked == true) {
		document.getElementById("optionalFeedBackId").style.display = "block";
	} else {
		document.getElementById("optionalFeedBackId").style.display = "none";
	}
}