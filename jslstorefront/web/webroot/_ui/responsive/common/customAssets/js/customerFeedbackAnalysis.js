		function filterOnFinish(){
			var finishSearchedVal = document.getElementById("finishSel").value;
			//var qualitySearchedVal = document.getElementById("qualitySel").value;
			
		    var tab360 = document.getElementById("cust360PopTable"); 
		    var mrktBalSumTotal = 0;
			var gradeMap = new Map();
		    var grdeCells = document.getElementsByClassName("grde");
			var gradeArray = [];
			var mktBalSumArray = [];
			var marketBalCells = document.getElementsByClassName("mktBalSel");
	
		    for(i = 0 ; i < tab360.rows.length-1 ; i++) 
		    {     
		    	var tr = document.getElementById("row_" + i);
		    	 var finish = document.getElementById("finish_" + i).innerText; 
		    	//var quality = document.getElementById("quality_" + i).innerText; 
		    	
		    	
		    	if(finish === finishSearchedVal || finishSearchedVal.length === 0){
		    		//document.getElementById("row_" + i).style.display = "table-row";
		    		 mrktBalSumTotal = mrktBalSumTotal + parseFloat(document.getElementById("mktBal_" + i).innerText);

		 			        if (!gradeMap.has(grdeCells[i].innerText)) {
		 			            gradeMap.set(grdeCells[i].innerText, marketBalCells[i].innerText);

		 			        } else {
		 			            var old = gradeMap.get(grdeCells[i].innerText);
		 			            var newValue = parseFloat(old) + parseFloat(marketBalCells[i].innerText);
		 			            gradeMap.set(grdeCells[i].innerText, newValue.toString());

		 			        }
		 			   
		 			    for (var val of gradeMap.keys()) {

		 			        gradeArray.push(val);
		 			    }
		 			    for (var val of gradeMap.values()) {

		 			        mktBalSumArray.push(val);
		 			    }
		 			    
		 			   var flagArray = mktBalSumArray.filter(function(elem){
		 			    	if(elem > 0){
		 			    		return true;
		 			    	}
		 			    	return false;
		 			    });
		 			    
		 			    if(flagArray.length != 0){
		 			    //graph js
		 			    document.getElementById("containerFinish").innerHTML = "";
		 			    
		 			   var data = [{
		 			        values: mktBalSumArray,
		 			        labels: gradeArray,
		 			        marker: {
		 			            colors: [
		 			            	'#FFCC99',
		 				              '#99FFCC',
		 				              '#CCCC99',
		 				              '#CCCCCC',
		 				              '#CCCCFF',
		 				              '#CCFF99',
		 				              '#CCFFCC',
		 				              '#CCFFFF',
		 				              '#FFCCCC',
		 				              '#FFFF99',
		 				              '#FFCCFF',
		 				              '#FFFFCC',
		 			            ]},
		 			      		        type: 'pie'
		 			    }];

		 			   var layout = {
		 					  height: 400,
				    		  width: 450,
				    		  margin: {
				    			    l: 10,
				    			    r: 55,
				    			    b:50,
				    			    t: 70,
				    			    pad: 10
				    			  },
				    		  showlegend: true,
				    		  legend: {"orientation": "h"},
					        title: '<b>Finish Vs Marketing Balance</b>',
					        "titlefont": {
							    "size": 14
							  }
					    }

		 			    Plotly.newPlot('containerFinish', data, layout, {
		 			        showSendToCloud: true ,
		 			       displayModeBar: false
		 			    });
		 			    }else{
		 			    	$("#productTypeVsMktBal").html("<div class='reportTitle'>Customer Id Vs Rating</div>");
							$("#qualityVsMktBal").html("<div class='reportTitle'>Feedback Questions Vs Rating</div>");
							$("#finishVsMktBal").html("<div class='reportTitle'>Finish Vs Marketing Balance</div>");
							$("#containerProduct").html('<div class="noGraphData"><marquee class="mrq_data">No data available for graphical representation</marquee></div>');
							$("#containerQuality").html('<div class="noGraphData"><marquee class="mrq_data">No data available for graphical representation</marquee></div>');
							$("#containerFinish").html('<div class="noGraphData"><marquee class="mrq_data">No data available for graphical representation</marquee></div>');
				    	}
		 			    
		 			    // new graph
		 			    
		 			/*    var data = [{
		 				  values: mktBalSumArray,
		 				  labels: gradeArray,
		 				  domain: {column: 0},
		 				  name: 'GHG Emissions',
		 				  hoverinfo: 'label+percent+name',
		 				  hole: .4,
		 				  type: 'pie'
		 				}];
		 			    
		 			  var layout = {
		 					  title: 'Global Emissions 1990-2011',
		 					  annotations: [
		 					    {
		 					      font: {
		 					        size: 20
		 					      },
		 					      showarrow: false,
		 					      text: 'GHG',
		 					      x: 0.17,
		 					      y: 0.5
		 					    },
		 					    {
		 					      font: {
		 					        size: 20
		 					      },
		 					      showarrow: false,
		 					      text: 'CO2',
		 					      x: 0.82,
		 					      y: 0.5
		 					    }
		 					  ],
		 					  height: 300,
		 					  width: 500,
		 					  showlegend: false,
		 					  grid: {rows: 1, columns: 2}
		 					};

		 					Plotly.newPlot('containerFinish', data, layout); */


		 			    
		    		
		    	}
		    	
		    }
		}
		
		function filterOnQuality(){
			var qualitySearchedVal = document.getElementById("qualitySel").value;
			
		    var tab360 = document.getElementById("cust360PopTable"); 
		    var mrktBalSumTotal = 0;
			var gradeMap = new Map();
		    var grdeCells = document.getElementsByClassName("grde");
			var gradeArray = [];
			var mktBalSumArray = [];
			var marketBalCells = document.getElementsByClassName("mktBalSel");
	
		    for(i = 0 ; i < tab360.rows.length-1 ; i++) 
		    {     
		    	var tr = document.getElementById("row_" + i);
		    	 //var finish = document.getElementById("finish_" + i).innerText; 
		    	var quality = document.getElementById("quality_" + i).innerText; 
		    	
		    	
		    	if(quality === qualitySearchedVal || qualitySearchedVal.length === 0){
		    		//document.getElementById("row_" + i).style.display = "table-row";
		    		
		    		
		 		   
		 		        mrktBalSumTotal = mrktBalSumTotal + parseFloat(document.getElementById("mktBal_" + i).innerText);

		 			        if (!gradeMap.has(grdeCells[i].innerText)) {
		 			            gradeMap.set(grdeCells[i].innerText, marketBalCells[i].innerText);

		 			        } else {
		 			            var old = gradeMap.get(grdeCells[i].innerText);
		 			            var newValue = parseFloat(old) + parseFloat(marketBalCells[i].innerText);
		 			            gradeMap.set(grdeCells[i].innerText, newValue.toString());

		 			        }
		 			   
		 			    for (var val of gradeMap.keys()) {

		 			        gradeArray.push(val);
		 			    }
		 			    for (var val of gradeMap.values()) {

		 			        mktBalSumArray.push(val);
		 			    }
		 			    
		 			   var flagArray = mktBalSumArray.filter(function(elem){
		 			    	if(elem > 0){
		 			    		return true;
		 			    	}
		 			    	return false;
		 			    });
		 			    
		 			    if(flagArray.length != 0){
		 			    
		 			    //graph js
		 			    document.getElementById("containerQuality").innerHTML = "";
		 			   var data = [{
		 			        values: mktBalSumArray,
		 			        labels: gradeArray,
		 			        marker: {
		 			            colors: [
		 			            	'#FFCC99',
		 				              '#99FFCC',
		 				              '#CCCC99',
		 				              '#CCCCCC',
		 				              '#CCCCFF',
		 				              '#CCFF99',
		 				              '#CCFFCC',
		 				              '#CCFFFF',
		 				              '#FFCCCC',
		 				              '#FFFF99',
		 				              '#FFCCFF',
		 				              '#FFFFCC',
		 			            ]},
		 			      		        type: 'pie'
		 			    }];

		 			   var layout = {
		 					  height: 400,
				    		  width: 450,
				    		  margin: {
				    			    l: 10,
				    			    r: 55,
				    			    b:50,
				    			    t: 70,
				    			    pad: 10
				    			  },
				    		  showlegend: true,
				    		  legend: {"orientation": "h"},
					        title: '<b>Feedback Questions Vs Rating</b>',
					        "titlefont": {
							    "size": 14
							  }
					    }

		 			    Plotly.newPlot('containerQuality', data, layout, {
		 			        showSendToCloud: true ,
		 			       displayModeBar: false
		 			    });
		    	}else{
		 			  $("#productTypeVsMktBal").html("<div class='reportTitle'>Customer ID Vs Rating</div>");
						$("#qualityVsMktBal").html("<div class='reportTitle'>Feedback Questions Vs Rating</div>");
						$("#finishVsMktBal").html("<div class='reportTitle'>Finish Vs Marketing Balance</div>");
						$("#containerProduct").html('<div class="noGraphData"><marquee class="mrq_data">No data available for graphical representation</marquee></div>');
						$("#containerQuality").html('<div class="noGraphData"><marquee class="mrq_data">No data available for graphical representation</marquee></div>');
						$("#containerFinish").html('<div class="noGraphData"><marquee class="mrq_data">No data available for graphical representation</marquee></div>');
		    	}   
		    		
		    	}
		    	
		    }
		}
		
		function filterOnProductType(){
			var productSearchedVal = document.getElementById("productSel").value;	
			//alert("productSearchedVal: "+productSearchedVal);
		    var tab360 = document.getElementById("cust360PopTable"); 
		    var mrktBalSumTotal = 0;
			var gradeMap = new Map();
		    var grdeCells = document.getElementsByClassName("grde");
			var gradeArray = [];
			var mktBalSumArray = [];
			var marketBalCells = document.getElementsByClassName("mktBalSel");
			//alert("hello");
		    for(i = 0 ; i < tab360.rows.length-1 ; i++) 
		    {     
		    	var tr = document.getElementById("row_" + i);
		    	 //var finish = document.getElementById("finish_" + i).innerText; 
		    	var product = document.getElementById("productType_" + i).innerText; 
		    	//alert("product : "+product);
		    	
		    	if(product === productSearchedVal || productSearchedVal.length === 0){
		    		
		 		        mrktBalSumTotal = mrktBalSumTotal + parseFloat(document.getElementById("mktBal_" + i).innerText);

		 			        if (!gradeMap.has(grdeCells[i].innerText)) {
		 			            gradeMap.set(grdeCells[i].innerText, marketBalCells[i].innerText);

		 			        } else {
		 			            var old = gradeMap.get(grdeCells[i].innerText);
		 			            var newValue = parseFloat(old) + parseFloat(marketBalCells[i].innerText);
		 			            gradeMap.set(grdeCells[i].innerText, newValue.toString());

		 			        }
		 			   
		 			    for (var val of gradeMap.keys()) {

		 			        gradeArray.push(val);
		 			    }
		 			    for (var val of gradeMap.values()) {

		 			        mktBalSumArray.push(val);
		 			    }
		 			   var flagArray = mktBalSumArray.filter(function(elem){
		 			    	if(elem > 0){
		 			    		return true;
		 			    	}
		 			    	return false;
		 			    });
		 			    
		 			    if(flagArray.length != 0){
		 			    
		 			    //graph js
		 			    document.getElementById("containerProduct").innerHTML = "";
		 			   var data = [{
		 			        values: mktBalSumArray,
		 			        labels: gradeArray,
		 			        marker: {
		 			            colors: [
		 			            	'#FFCC99',
		 				              '#99FFCC',
		 				              '#CCCC99',
		 				              '#CCCCCC',
		 				              '#CCCCFF',
		 				              '#CCFF99',
		 				              '#CCFFCC',
		 				              '#CCFFFF',
		 				              '#FFCCCC',
		 				              '#FFFF99',
		 				              '#FFCCFF',
		 				              '#FFFFCC',
		 			            ]},
		 			      		        type: 'pie'
		 			    }];
		 			    
		 			    

		 			   var layout = {
		 					  height: 400,
				    		  width: 450,
				    		  margin: {
				    			    l: 10,
				    			    r: 55,
				    			    b:50,
				    			    t: 70,
				    			    pad: 10
				    			  },
				    		  showlegend: true,
				    		  legend: {"orientation": "h"},
					        title: '<b>Customer ID Vs Rating</b>',
					        "titlefont": {
							    "size": 14
							  }
					    }

		 			    Plotly.newPlot('containerProduct', data, layout, {
		 			        showSendToCloud: true,
		 			       displayModeBar: false
		 			    });
		    	}else{
		    		$("#productTypeVsMktBal").html("<div class='reportTitle'>Customer ID Vs Rating</div>");
					$("#qualityVsMktBal").html("<div class='reportTitle'>Feedback Questions Vs Rating</div>");
					$("#finishVsMktBal").html("<div class='reportTitle'>Finish Vs Marketing Balance</div>");
					$("#containerProduct").html('<div class="noGraphData"><marquee class="mrq_data">No data available for graphical representation</marquee></div>');
					$("#containerQuality").html('<div class="noGraphData"><marquee class="mrq_data">No data available for graphical representation</marquee></div>');
					$("#containerFinish").html('<div class="noGraphData"><marquee class="mrq_data">No data available for graphical representation</marquee></div>');
		    	}
		    		
		    	}
		    	
		    }
		}
		
		window.onload = function() {
			$.ajax({
				type: "GET",
				url: ACC.config.encodedContextPath + "/support/getFeedbackData",
				success: function(response){
					createTable(response);
					postTableCreation();
				},
				error: function(response){
					//createTable(response);
					//postTableCreation();
					 //$("#successDataCheck").hide();
			 		 $('#loading-wrapper').hide();		  
			 		// $("#failureDataCheck").show();
				}
			});
		}
		
		/*var complaintResponse = {
			        "complaintList": [{
			                "CustomerID": "11111",
			                "ComplaintNumber": "180",
			                "Q1": "Good",
			                "Q2": "Excellent",
			                "Q3": "Good",
			                "Q4": "Average",
			                "Q5": "Fair",
			                "Q6": "Good",
			                "Q7": "Poor",
			                "Q8": "Good",
			                "Q9": "Fair",
			                "Q10": "Excellent",
			                "Q11": "Poor",
			                "Q12": "Good",
			                "Q13": "Good",
			                "UserCmnt": "Its is done",
			                "FeedBackDate": "09/25/2018",
			            },
			            {
			               "CustomerID": "22222",
			                "ComplaintNumber": "181",
			                "Q1": "Poor",
			                "Q2": "Average",
			                "Q3": "Excellent",
			                "Q4": "Good",
			                "Q5": "Good",
			                "Q6": "Fair",
			                "Q7": "Good",
			                "Q8": "Poor",
			                "Q9": "Poor",
			                "Q10": "Good",
			                "Q11": "Good",
			                "Q12": "Average",
			                "Q13": "Good",
			                "UserCmnt": "Its is Good",
			                "FeedBackDate": "11/20/2018"
			            }]};*/
		
		function createTable(json){
		    var complaintTableBody = document.getElementById("customerFeedbackTableBody");
		    json.forEach(function(item, index) {
		        var row = complaintTableBody.insertRow(index);
		        row.id = "complaintTableRow_" + index;	        
		        var cell0 = row.insertCell(0);
		        cell0.innerText = item.customerNumber;
		        cell0.id = "CustomerID_" + index;
		        cell0.classList.add("prdType");
		        var cell1 = row.insertCell(1);
		        cell1.innerText = item.complaintNumber;
		        cell1.id = "ComplaintNumber_" + index;	        
		        var cell2 = row.insertCell(2);
		        cell2.innerText = item.q1;
		        cell2.id = "Q1_" + index;
		        cell2.classList.add("quality");
		        var cell3 = row.insertCell(3);
		        cell3.innerText = item.q2;
		        cell3.id = "Q2_" + index;
		        cell3.classList.add("quality");
		        var cell4 = row.insertCell(4);
		        cell4.innerText = item.q3;
		        cell4.id = "Q3_" + index;
		        cell4.classList.add("quality");
		        var cell5 = row.insertCell(5);
		        cell5.innerText = item.q4;
		        cell5.id = "Q4_" + index;
		        cell5.classList.add("quality");
		        var cell6 = row.insertCell(6);
		        cell6.innerText = item.q5;
		        cell6.id = "Q5_" + index;
		        cell6.classList.add("quality");
		        var cell7 = row.insertCell(7);
		        cell7.innerText = item.q6;
		        cell7.id = "Q6_" + index;
		        cell7.classList.add("quality");
		        var cell8 = row.insertCell(8);
		        cell8.innerText = item.q7;
		        cell8.id = "Q7_" + index;
		        cell8.classList.add("quality");
		        var cell9 = row.insertCell(9);
		        cell9.innerText = item.q8;
		        cell9.id = "Q8_" + index;
		        cell9.classList.add("quality");
		        var cell10 = row.insertCell(10);
		        cell10.innerText = item.q9;
		        cell10.id = "Q9_" + index;
		        cell10.classList.add("quality");
		        var cell11 = row.insertCell(11);
		        cell11.innerText = item.q10;
		        cell11.id = "Q10_" + index;
		        cell11.classList.add("quality");
		        var cell12 = row.insertCell(12);
		        cell12.innerText = item.q11;
		        cell12.id = "Q11_" + index;
		        cell12.classList.add("quality");
		        var cell13 = row.insertCell(13);
		        cell13.innerText = item.q12;
		        cell13.id = "Q12_" + index;
		        cell13.classList.add("quality");
		        var cell14 = row.insertCell(14);
		        cell14.innerText = item.q13;
		        cell14.id = "Q13_" + index;
		        var cell15 = row.insertCell(15);
		        cell15.innerText = item.comments;
		        cell15.id = "userComment_" + index;
		        var cell16 = row.insertCell(16);
		        cell16.innerText = formatDate(item.feedbackDate);
		        cell16.id = "feedbackDate_" + index;

		        
		      /*  var cell1 = row.insertCell(1);
		        cell1.classList.add("complaint_underline");
		        var ticketLink = document.createElement("a");
		        ticketLink.onclick = function(arg) {
		            return function() {
		                showTicketDetails(arg)
		            }
		        }(index);
		        ticketLink.innerHTML = item.complaintNumber;
		        ticketLink.id = "complaintNumber_" + index;
		        cell0.appendChild(ticketLink);
		        var cell1 = row.insertCell(1);
		        cell1.innerText = item.FeedBackDate;
		        cell1.id = "typeOfComplaint_" + index;
		        cell1.classList.add("typeOfComplaint");
		        var cell2 = row.insertCell(2);
		        cell2.innerText = item.invoiceNumber;
		        cell2.id = "invoiceNumber_" + index;
		        cell2.classList.add("invoiceNumber");
		        var cell3 = row.insertCell(3);
		        cell3.innerText = item.natureOfComplaints;
		        cell3.id = "natureOfComplaints_" + index;
		        cell3.classList.add("natureOfComplaints");
		        var cell4 = row.insertCell(4);
		        cell4.innerText = item.formattedCreatedOn ;        
		        cell4.id = "formattedCreatedOn_" + index;
		        cell4.classList.add("formattedCreatedOn");
		        var cell5 = row.insertCell(5);
		        cell5.innerText = item.status;
		        cell5.id = "status_" + index;
		        cell5.classList.add("status");
		    });  */
			});
		}
		
		function postTableCreation(){
			$("#loading-wrapper").hide();
			var qualityOptions = new Set();
			var qualitySel = document.getElementById("qualitySel");
			
			var productOptions = new Set();
			var productSel = document.getElementById("productSel");
			 var productCells = document.getElementsByClassName("prdType");
			    for (var i = 0; i < productCells.length; i++) {

			        if (!productOptions.has(productCells[i].innerText)) {
			        	productOptions.add(productCells[i].innerText);
			        }
			    }
			    productOptions.forEach(function(elem) {
			    	productSel.options.add(new Option(elem, elem));
			    });
			    
			    
		    var qualityCells = document.getElementsByClassName("quality");
		    for (var i = 0; i < qualityCells.length; i++) {

		        if (!qualityOptions.has(qualityCells[i].innerText)) {
		            qualityOptions.add(qualityCells[i].innerText);
		        }
		    }
		    qualityOptions.forEach(function(elem) {
		        qualitySel.options.add(new Option(elem, elem));
		    });
		    
		    createGraph('all');
//////////////


		    
		}/*else{
			$("#productTypeVsMktBal").html("<div class='reportTitle'>Customer ID Vs Rating</div>");
			$("#qualityVsMktBal").html("<div class='reportTitle'>Feedback Questions Vs Rating</div>");
			$("#finishVsMktBal").html("<div class='reportTitle'>Finish Vs Marketing Balance</div>");
			$("#containerProduct").html('<div class="noGraphData"><marquee class="mrq_data">No data available for graphical representation</marquee></div>');
			$("#containerQuality").html('<div class="noGraphData"><marquee class="mrq_data">No data available for graphical representation</marquee></div>');
			$("#containerFinish").html('<div class="noGraphData"><marquee class="mrq_data">No data available for graphical representation</marquee></div>');
		}

		} */
		function createQuestionsGraph(selectedQuestionType){
			var poorFeedbackMap = new Map();
		    var fairFeedbackMap = new Map();
		    var averageFeedbackMap = new Map();
		    var goodFeedbackMap = new Map();
		    var excellentFeedbackMap = new Map();
		    
		    var poorFeedbackSum = 0;
		    var fairFeedbackSum = 0;
		    var averageFeedbackSum = 0;
		    var goodFeedbackSum = 0;
		    var excellentFeedbackSum = 0;
		    
			 var complaintFeedbackTable = document.getElementById("customerFeedbackTableBody");
			    for (var i = 0; i < complaintFeedbackTable.rows.length; i++) {
			    if(selectedQuestionType.toUpperCase() == 'Q1'){
			    	if(document.getElementById("Q1_" + i).innerText=="Poor"){
			    		poorFeedbackSum = poorFeedbackSum + 1 ;
			    		poorFeedbackMap.set("Q1", poorFeedbackSum);
			    	}if(document.getElementById("Q1_" + i).innerText=="Fair"){
			    		fairFeedbackSum = fairFeedbackSum + 1 ;
			    		fairFeedbackMap.set("Q1", fairFeedbackSum);
			    	}
			    	if(document.getElementById("Q1_" + i).innerText=="Average"){
			    		averageFeedbackSum = averageFeedbackSum + 1 ;
			    		averageFeedbackMap.set("Q1", averageFeedbackSum);
			    	}
			    	if(document.getElementById("Q1_" + i).innerText=="Good"){
			    		goodFeedbackSum = goodFeedbackSum + 1 ;
			    		goodFeedbackMap.set("Q1", goodFeedbackSum);
			    	}
			    	if(document.getElementById("Q1_" + i).innerText=="Excellent"){
			    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
			    		excellentFeedbackMap.set("Q1", excellentFeedbackSum);
			    	}
			    	
			    	}
			    if(selectedQuestionType.toUpperCase() == 'Q2'){
			    	if(document.getElementById("Q2_" + i).innerText=="Poor"){
			    		poorFeedbackSum = poorFeedbackSum + 1 ;
			    		poorFeedbackMap.set("Q2", poorFeedbackSum);
			    	}if(document.getElementById("Q2_" + i).innerText=="Fair"){
			    		fairFeedbackSum = fairFeedbackSum + 1 ;
			    		fairFeedbackMap.set("Q2", fairFeedbackSum);
			    	}
			    	if(document.getElementById("Q2_" + i).innerText=="Average"){
			    		averageFeedbackSum = averageFeedbackSum + 1 ;
			    		averageFeedbackMap.set("Q2", averageFeedbackSum);
			    	}
			    	if(document.getElementById("Q2_" + i).innerText=="Good"){
			    		goodFeedbackSum = goodFeedbackSum + 1 ;
			    		goodFeedbackMap.set("Q2", goodFeedbackSum);
			    	}
			    	if(document.getElementById("Q2_" + i).innerText=="Excellent"){
			    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
			    		excellentFeedbackMap.set("Q2", excellentFeedbackSum);
			    	}
			    	
			    	}
			    if(selectedQuestionType.toUpperCase() == 'Q3'){
			    	if(document.getElementById("Q3_" + i).innerText=="Poor"){
			    		poorFeedbackSum = poorFeedbackSum + 1 ;
			    		poorFeedbackMap.set("Q3", poorFeedbackSum);
			    	}if(document.getElementById("Q3_" + i).innerText=="Fair"){
			    		fairFeedbackSum = fairFeedbackSum + 1 ;
			    		fairFeedbackMap.set("Q3", fairFeedbackSum);
			    	}
			    	if(document.getElementById("Q3_" + i).innerText=="Average"){
			    		averageFeedbackSum = averageFeedbackSum + 1 ;
			    		averageFeedbackMap.set("Q3", averageFeedbackSum);
			    	}
			    	if(document.getElementById("Q3_" + i).innerText=="Good"){
			    		goodFeedbackSum = goodFeedbackSum + 1 ;
			    		goodFeedbackMap.set("Q3", goodFeedbackSum);
			    	}
			    	if(document.getElementById("Q3_" + i).innerText=="Excellent"){
			    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
			    		excellentFeedbackMap.set("Q3", excellentFeedbackSum);
			    	}
			    	
			    	}
			    if(selectedQuestionType.toUpperCase() == 'Q4'){
			    	if(document.getElementById("Q4_" + i).innerText=="Poor"){
			    		poorFeedbackSum = poorFeedbackSum + 1 ;
			    		poorFeedbackMap.set("Q4", poorFeedbackSum);
			    	}if(document.getElementById("Q4_" + i).innerText=="Fair"){
			    		fairFeedbackSum = fairFeedbackSum + 1 ;
			    		fairFeedbackMap.set("Q4", fairFeedbackSum);
			    	}
			    	if(document.getElementById("Q4_" + i).innerText=="Average"){
			    		averageFeedbackSum = averageFeedbackSum + 1 ;
			    		averageFeedbackMap.set("Q4", averageFeedbackSum);
			    	}
			    	if(document.getElementById("Q4_" + i).innerText=="Good"){
			    		goodFeedbackSum = goodFeedbackSum + 1 ;
			    		goodFeedbackMap.set("Q4", goodFeedbackSum);
			    	}
			    	if(document.getElementById("Q4_" + i).innerText=="Excellent"){
			    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
			    		excellentFeedbackMap.set("Q4", excellentFeedbackSum);
			    	}
			    	
			    	}
			    if(selectedQuestionType.toUpperCase() == 'Q5'){
			    	if(document.getElementById("Q5_" + i).innerText=="Poor"){
			    		poorFeedbackSum = poorFeedbackSum + 1 ;
			    		poorFeedbackMap.set("Q5", poorFeedbackSum);
			    	}if(document.getElementById("Q5_" + i).innerText=="Fair"){
			    		fairFeedbackSum = fairFeedbackSum + 1 ;
			    		fairFeedbackMap.set("Q5", fairFeedbackSum);
			    	}
			    	if(document.getElementById("Q5_" + i).innerText=="Average"){
			    		averageFeedbackSum = averageFeedbackSum + 1 ;
			    		averageFeedbackMap.set("Q5", averageFeedbackSum);
			    	}
			    	if(document.getElementById("Q5_" + i).innerText=="Good"){
			    		goodFeedbackSum = goodFeedbackSum + 1 ;
			    		goodFeedbackMap.set("Q5", goodFeedbackSum);
			    	}
			    	if(document.getElementById("Q5_" + i).innerText=="Excellent"){
			    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
			    		excellentFeedbackMap.set("Q5", excellentFeedbackSum);
			    	}
			    	
			    	}
			    if(selectedQuestionType.toUpperCase() == 'Q6'){
			    	if(document.getElementById("Q6_" + i).innerText=="Poor"){
			    		poorFeedbackSum = poorFeedbackSum + 1 ;
			    		poorFeedbackMap.set("Q6", poorFeedbackSum);
			    	}if(document.getElementById("Q6_" + i).innerText=="Fair"){
			    		fairFeedbackSum = fairFeedbackSum + 1 ;
			    		fairFeedbackMap.set("Q6", fairFeedbackSum);
			    	}
			    	if(document.getElementById("Q6_" + i).innerText=="Average"){
			    		averageFeedbackSum = averageFeedbackSum + 1 ;
			    		averageFeedbackMap.set("Q6", averageFeedbackSum);
			    	}
			    	if(document.getElementById("Q6_" + i).innerText=="Good"){
			    		goodFeedbackSum = goodFeedbackSum + 1 ;
			    		goodFeedbackMap.set("Q6", goodFeedbackSum);
			    	}
			    	if(document.getElementById("Q6_" + i).innerText=="Excellent"){
			    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
			    		excellentFeedbackMap.set("Q6", excellentFeedbackSum);
			    	}
			    	
			    	}
			    if(selectedQuestionType.toUpperCase() == 'Q7'){
			    	if(document.getElementById("Q7_" + i).innerText=="Poor"){
			    		poorFeedbackSum = poorFeedbackSum + 1 ;
			    		poorFeedbackMap.set("Q7", poorFeedbackSum);
			    	}if(document.getElementById("Q7_" + i).innerText=="Fair"){
			    		fairFeedbackSum = fairFeedbackSum + 1 ;
			    		fairFeedbackMap.set("Q7", fairFeedbackSum);
			    	}
			    	if(document.getElementById("Q7_" + i).innerText=="Average"){
			    		averageFeedbackSum = averageFeedbackSum + 1 ;
			    		averageFeedbackMap.set("Q7", averageFeedbackSum);
			    	}
			    	if(document.getElementById("Q7_" + i).innerText=="Good"){
			    		goodFeedbackSum = goodFeedbackSum + 1 ;
			    		goodFeedbackMap.set("Q7", goodFeedbackSum);
			    	}
			    	if(document.getElementById("Q7_" + i).innerText=="Excellent"){
			    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
			    		excellentFeedbackMap.set("Q7", excellentFeedbackSum);
			    	}
			    	
			    	}
			    if(selectedQuestionType.toUpperCase() == 'Q8'){
			    	if(document.getElementById("Q8_" + i).innerText=="Poor"){
			    		poorFeedbackSum = poorFeedbackSum + 1 ;
			    		poorFeedbackMap.set("Q8", poorFeedbackSum);
			    	}if(document.getElementById("Q8_" + i).innerText=="Fair"){
			    		fairFeedbackSum = fairFeedbackSum + 1 ;
			    		fairFeedbackMap.set("Q8", fairFeedbackSum);
			    	}
			    	if(document.getElementById("Q8_" + i).innerText=="Average"){
			    		averageFeedbackSum = averageFeedbackSum + 1 ;
			    		averageFeedbackMap.set("Q8", averageFeedbackSum);
			    	}
			    	if(document.getElementById("Q8_" + i).innerText=="Good"){
			    		goodFeedbackSum = goodFeedbackSum + 1 ;
			    		goodFeedbackMap.set("Q8", goodFeedbackSum);
			    	}
			    	if(document.getElementById("Q8_" + i).innerText=="Excellent"){
			    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
			    		excellentFeedbackMap.set("Q8", excellentFeedbackSum);
			    	}
			    	
			    	}
			    if(selectedQuestionType.toUpperCase() == 'Q9'){
			    	if(document.getElementById("Q9_" + i).innerText=="Poor"){
			    		poorFeedbackSum = poorFeedbackSum + 1 ;
			    		poorFeedbackMap.set("Q9", poorFeedbackSum);
			    	}if(document.getElementById("Q9_" + i).innerText=="Fair"){
			    		fairFeedbackSum = fairFeedbackSum + 1 ;
			    		fairFeedbackMap.set("Q9", fairFeedbackSum);
			    	}
			    	if(document.getElementById("Q9_" + i).innerText=="Average"){
			    		averageFeedbackSum = averageFeedbackSum + 1 ;
			    		averageFeedbackMap.set("Q9", averageFeedbackSum);
			    	}
			    	if(document.getElementById("Q9_" + i).innerText=="Good"){
			    		goodFeedbackSum = goodFeedbackSum + 1 ;
			    		goodFeedbackMap.set("Q9", goodFeedbackSum);
			    	}
			    	if(document.getElementById("Q9_" + i).innerText=="Excellent"){
			    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
			    		excellentFeedbackMap.set("Q9", excellentFeedbackSum);
			    	}
			    	
			    	}
			    if(selectedQuestionType.toUpperCase() == 'Q10'){
			    	if(document.getElementById("Q10_" + i).innerText=="Poor"){
			    		poorFeedbackSum = poorFeedbackSum + 1 ;
			    		poorFeedbackMap.set("Q10", poorFeedbackSum);
			    	}if(document.getElementById("Q10_" + i).innerText=="Fair"){
			    		fairFeedbackSum = fairFeedbackSum + 1 ;
			    		fairFeedbackMap.set("Q10", fairFeedbackSum);
			    	}
			    	if(document.getElementById("Q10_" + i).innerText=="Average"){
			    		averageFeedbackSum = averageFeedbackSum + 1 ;
			    		averageFeedbackMap.set("Q10", averageFeedbackSum);
			    	}
			    	if(document.getElementById("Q10_" + i).innerText=="Good"){
			    		goodFeedbackSum = goodFeedbackSum + 1 ;
			    		goodFeedbackMap.set("Q10", goodFeedbackSum);
			    	}
			    	if(document.getElementById("Q10_" + i).innerText=="Excellent"){
			    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
			    		excellentFeedbackMap.set("Q10", excellentFeedbackSum);
			    	}
			    	
			    	}
			    if(selectedQuestionType.toUpperCase() == 'Q11'){
			    	if(document.getElementById("Q11_" + i).innerText=="Poor"){
			    		poorFeedbackSum = poorFeedbackSum + 1 ;
			    		poorFeedbackMap.set("Q11", poorFeedbackSum);
			    	}if(document.getElementById("Q11_" + i).innerText=="Fair"){
			    		fairFeedbackSum = fairFeedbackSum + 1 ;
			    		fairFeedbackMap.set("Q11", fairFeedbackSum);
			    	}
			    	if(document.getElementById("Q11_" + i).innerText=="Average"){
			    		averageFeedbackSum = averageFeedbackSum + 1 ;
			    		averageFeedbackMap.set("Q11", averageFeedbackSum);
			    	}
			    	if(document.getElementById("Q11_" + i).innerText=="Good"){
			    		goodFeedbackSum = goodFeedbackSum + 1 ;
			    		goodFeedbackMap.set("Q11", goodFeedbackSum);
			    	}
			    	if(document.getElementById("Q11_" + i).innerText=="Excellent"){
			    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
			    		excellentFeedbackMap.set("Q11", excellentFeedbackSum);
			    	}
			    	
			    	}
			    if(selectedQuestionType.toUpperCase() == 'Q12'){
			    	if(document.getElementById("Q12_" + i).innerText=="Poor"){
			    		poorFeedbackSum = poorFeedbackSum + 1 ;
			    		poorFeedbackMap.set("Q12", poorFeedbackSum);
			    	}if(document.getElementById("Q12_" + i).innerText=="Fair"){
			    		fairFeedbackSum = fairFeedbackSum + 1 ;
			    		fairFeedbackMap.set("Q12", fairFeedbackSum);
			    	}
			    	if(document.getElementById("Q12_" + i).innerText=="Average"){
			    		averageFeedbackSum = averageFeedbackSum + 1 ;
			    		averageFeedbackMap.set("Q12", averageFeedbackSum);
			    	}
			    	if(document.getElementById("Q12_" + i).innerText=="Good"){
			    		goodFeedbackSum = goodFeedbackSum + 1 ;
			    		goodFeedbackMap.set("Q12", goodFeedbackSum);
			    	}
			    	if(document.getElementById("Q12_" + i).innerText=="Excellent"){
			    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
			    		excellentFeedbackMap.set("Q12", excellentFeedbackSum);
			    	}
			    	
			    	}
			    if(selectedQuestionType.toUpperCase() == 'Q13'){
			    	if(document.getElementById("Q13_" + i).innerText=="Poor"){
			    		poorFeedbackSum = poorFeedbackSum + 1 ;
			    		poorFeedbackMap.set("Q13", poorFeedbackSum);
			    	}if(document.getElementById("Q13_" + i).innerText=="Fair"){
			    		fairFeedbackSum = fairFeedbackSum + 1 ;
			    		fairFeedbackMap.set("Q13", fairFeedbackSum);
			    	}
			    	if(document.getElementById("Q13_" + i).innerText=="Average"){
			    		averageFeedbackSum = averageFeedbackSum + 1 ;
			    		averageFeedbackMap.set("Q13", averageFeedbackSum);
			    	}
			    	if(document.getElementById("Q13_" + i).innerText=="Good"){
			    		goodFeedbackSum = goodFeedbackSum + 1 ;
			    		goodFeedbackMap.set("Q13", goodFeedbackSum);
			    	}
			    	if(document.getElementById("Q13_" + i).innerText=="Excellent"){
			    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
			    		excellentFeedbackMap.set("Q13", excellentFeedbackSum);
			    	}
			    	
			    	}

			    }
			    
			    var trace1 = {
			    		x: Array.from(poorFeedbackMap.keys()),
			            y: Array.from(poorFeedbackMap.values()),
			            name: 'POOR',
			            type: 'bar'
			        };

			        var trace2 = {
			            x: Array.from(fairFeedbackMap.keys()),
			            y: Array.from(fairFeedbackMap.values()),
			            name: 'FAIR',
			            type: 'bar'
			        };
			        var trace3 = {
			            x: Array.from(averageFeedbackMap.keys()),
			            y: Array.from(averageFeedbackMap.values()),
			            name: 'AVERAGE',
			            type: 'bar'
			        };
			        var trace4 = {
			            x: Array.from(goodFeedbackMap.keys()),
			            y: Array.from(goodFeedbackMap.values()),
			            name: 'GOOD',
			            type: 'bar'
			        };
			        var trace5 = {
			            x: Array.from(excellentFeedbackMap.keys()),
			            y: Array.from(excellentFeedbackMap.values()),
			            name: 'EXCELLENT',
			            type: 'bar'
			        };

			        var data = [trace1, trace2, trace3, trace4, trace5];




			        var layout = {
			            height: 300,
			            width: 1200,
			            margin: {
			                l: 100,
			                r: 10,
			                b: 50,
			                t: 120,
			                pad: 0
			            },
			            barmode: 'group',
			            showlegend: true,
			            /*   legend: {"orientation": "h"},*/
			            title: '<b>Complaint Ticket</b>',
			            "titlefont": {
			                "size": 16
			            },
			            yaxis: {
			                title: {
			                    text: '<b>Count</b>',
			                    font: {
			                        size: 12
			                    }
			                }
			            },
			            xaxis: {
			                title: {
			                    text: '<b>Status</b>',
			                    font: {
			                        size: 12
			                    }
			                }
			            }
			        }

			        Plotly.newPlot("containerProduct", data, layout, {
			            showSendToCloud: true,
			            displayModeBar: false
			        });
			    
		}
		
		function createCustomerIdGraph(customerIdSelected){
			var poorFeedbackMap = new Map();
		    var fairFeedbackMap = new Map();
		    var averageFeedbackMap = new Map();
		    var goodFeedbackMap = new Map();
		    var excellentFeedbackMap = new Map();
		    
		    var poorFeedbackSum = 0;
		    var fairFeedbackSum = 0;
		    var averageFeedbackSum = 0;
		    var goodFeedbackSum = 0;
		    var excellentFeedbackSum = 0;
		    
		    var complaintFeedbackTable = document.getElementById("customerFeedbackTableBody");
		    for (var i = 0; i < complaintFeedbackTable.rows.length; i++) {
		    if(document.getElementById("CustomerID_" + i).innerText==customerIdSelected){  
		    	if(document.getElementById("Q1_" + i).innerText=="Poor"){
		    		poorFeedbackSum = poorFeedbackSum + 1 ;
		    		poorFeedbackMap.set("Poor", poorFeedbackSum);
		    	}if(document.getElementById("Q1_" + i).innerText=="Fair"){
		    		fairFeedbackSum = fairFeedbackSum + 1 ;
		    		fairFeedbackMap.set("Fair", fairFeedbackSum);
		    	}
		    	if(document.getElementById("Q1_" + i).innerText=="Average"){
		    		averageFeedbackSum = averageFeedbackSum + 1 ;
		    		averageFeedbackMap.set("Average", averageFeedbackSum);
		    	}
		    	if(document.getElementById("Q1_" + i).innerText=="Good"){
		    		goodFeedbackSum = goodFeedbackSum + 1 ;
		    		goodFeedbackMap.set("Good", goodFeedbackSum);
		    	}
		    	if(document.getElementById("Q1_" + i).innerText=="Excellent"){
		    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
		    		excellentFeedbackMap.set("Excellent", excellentFeedbackSum);
		    	}
		    	
		    	if(document.getElementById("Q2_" + i).innerText=="Poor"){
		    		poorFeedbackSum = poorFeedbackSum + 1 ;
		    		poorFeedbackMap.set("Poor", poorFeedbackSum);
		    	}if(document.getElementById("Q2_" + i).innerText=="Fair"){
		    		fairFeedbackSum = fairFeedbackSum + 1 ;
		    		fairFeedbackMap.set("Fair", fairFeedbackSum);
		    	}
		    	if(document.getElementById("Q2_" + i).innerText=="Average"){
		    		averageFeedbackSum = averageFeedbackSum + 1 ;
		    		averageFeedbackMap.set("Average", averageFeedbackSum);
		    	}
		    	if(document.getElementById("Q2_" + i).innerText=="Good"){
		    		goodFeedbackSum = goodFeedbackSum + 1 ;
		    		goodFeedbackMap.set("Good", goodFeedbackSum);
		    	}
		    	if(document.getElementById("Q2_" + i).innerText=="Excellent"){
		    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
		    		excellentFeedbackMap.set("Excellent", excellentFeedbackSum);
		    	}
		    	
		      	if(document.getElementById("Q3_" + i).innerText=="Poor"){
		    		poorFeedbackSum = poorFeedbackSum + 1 ;
		    		poorFeedbackMap.set("Poor", poorFeedbackSum);
		    	}if(document.getElementById("Q3_" + i).innerText=="Fair"){
		    		fairFeedbackSum = fairFeedbackSum + 1 ;
		    		fairFeedbackMap.set("Fair", fairFeedbackSum);
		    	}
		    	if(document.getElementById("Q3_" + i).innerText=="Average"){
		    		averageFeedbackSum = averageFeedbackSum + 1 ;
		    		averageFeedbackMap.set("Average", averageFeedbackSum);
		    	}
		    	if(document.getElementById("Q3_" + i).innerText=="Good"){
		    		goodFeedbackSum = goodFeedbackSum + 1 ;
		    		goodFeedbackMap.set("Good", goodFeedbackSum);
		    	}
		    	if(document.getElementById("Q3_" + i).innerText=="Excellent"){
		    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
		    		excellentFeedbackMap.set("Excellent", excellentFeedbackSum);
		    	}
		    	
		      	if(document.getElementById("Q4_" + i).innerText=="Poor"){
		    		poorFeedbackSum = poorFeedbackSum + 1 ;
		    		poorFeedbackMap.set("Poor", poorFeedbackSum);
		    	}if(document.getElementById("Q4_" + i).innerText=="Fair"){
		    		fairFeedbackSum = fairFeedbackSum + 1 ;
		    		fairFeedbackMap.set("Fair", fairFeedbackSum);
		    	}
		    	if(document.getElementById("Q4_" + i).innerText=="Average"){
		    		averageFeedbackSum = averageFeedbackSum + 1 ;
		    		averageFeedbackMap.set("Average", averageFeedbackSum);
		    	}
		    	if(document.getElementById("Q4_" + i).innerText=="Good"){
		    		goodFeedbackSum = goodFeedbackSum + 1 ;
		    		goodFeedbackMap.set("Good", goodFeedbackSum);
		    	}
		    	if(document.getElementById("Q4_" + i).innerText=="Excellent"){
		    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
		    		excellentFeedbackMap.set("Excellent", excellentFeedbackSum);
		    	}
		    	
		      	if(document.getElementById("Q5_" + i).innerText=="Poor"){
		    		poorFeedbackSum = poorFeedbackSum + 1 ;
		    		poorFeedbackMap.set("Poor", poorFeedbackSum);
		    	}if(document.getElementById("Q5_" + i).innerText=="Fair"){
		    		fairFeedbackSum = fairFeedbackSum + 1 ;
		    		fairFeedbackMap.set("Fair", fairFeedbackSum);
		    	}
		    	if(document.getElementById("Q5_" + i).innerText=="Average"){
		    		averageFeedbackSum = averageFeedbackSum + 1 ;
		    		averageFeedbackMap.set("Average", averageFeedbackSum);
		    	}
		    	if(document.getElementById("Q5_" + i).innerText=="Good"){
		    		goodFeedbackSum = goodFeedbackSum + 1 ;
		    		goodFeedbackMap.set("Good", goodFeedbackSum);
		    	}
		    	if(document.getElementById("Q5_" + i).innerText=="Excellent"){
		    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
		    		excellentFeedbackMap.set("Q5", excellentFeedbackSum);
		    	}
		    	
		      	if(document.getElementById("Q6_" + i).innerText=="Poor"){
		    		poorFeedbackSum = poorFeedbackSum + 1 ;
		    		poorFeedbackMap.set("Poor", poorFeedbackSum);
		    	}if(document.getElementById("Q6_" + i).innerText=="Fair"){
		    		fairFeedbackSum = fairFeedbackSum + 1 ;
		    		fairFeedbackMap.set("Fair", fairFeedbackSum);
		    	}
		    	if(document.getElementById("Q6_" + i).innerText=="Average"){
		    		averageFeedbackSum = averageFeedbackSum + 1 ;
		    		averageFeedbackMap.set("Average", averageFeedbackSum);
		    	}
		    	if(document.getElementById("Q6_" + i).innerText=="Good"){
		    		goodFeedbackSum = goodFeedbackSum + 1 ;
		    		goodFeedbackMap.set("Good", goodFeedbackSum);
		    	}
		    	if(document.getElementById("Q6_" + i).innerText=="Excellent"){
		    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
		    		excellentFeedbackMap.set("Excellent", excellentFeedbackSum);
		    	}
		    	
		      	if(document.getElementById("Q7_" + i).innerText=="Poor"){
		    		poorFeedbackSum = poorFeedbackSum + 1 ;
		    		poorFeedbackMap.set("Poor", poorFeedbackSum);
		    	}if(document.getElementById("Q7_" + i).innerText=="Fair"){
		    		fairFeedbackSum = fairFeedbackSum + 1 ;
		    		fairFeedbackMap.set("Fair", fairFeedbackSum);
		    	}
		    	if(document.getElementById("Q7_" + i).innerText=="Average"){
		    		averageFeedbackSum = averageFeedbackSum + 1 ;
		    		averageFeedbackMap.set("Average", averageFeedbackSum);
		    	}
		    	if(document.getElementById("Q7_" + i).innerText=="Good"){
		    		goodFeedbackSum = goodFeedbackSum + 1 ;
		    		goodFeedbackMap.set("Good", goodFeedbackSum);
		    	}
		    	if(document.getElementById("Q7_" + i).innerText=="Excellent"){
		    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
		    		excellentFeedbackMap.set("Excellent", excellentFeedbackSum);
		    	}
		    	
		      	if(document.getElementById("Q8_" + i).innerText=="Poor"){
		    		poorFeedbackSum = poorFeedbackSum + 1 ;
		    		poorFeedbackMap.set("Poor", poorFeedbackSum);
		    	}if(document.getElementById("Q8_" + i).innerText=="Fair"){
		    		fairFeedbackSum = fairFeedbackSum + 1 ;
		    		fairFeedbackMap.set("Fair", fairFeedbackSum);
		    	}
		    	if(document.getElementById("Q8_" + i).innerText=="Average"){
		    		averageFeedbackSum = averageFeedbackSum + 1 ;
		    		averageFeedbackMap.set("Average", averageFeedbackSum);
		    	}
		    	if(document.getElementById("Q8_" + i).innerText=="Good"){
		    		goodFeedbackSum = goodFeedbackSum + 1 ;
		    		goodFeedbackMap.set("Good", goodFeedbackSum);
		    	}
		    	if(document.getElementById("Q8_" + i).innerText=="Excellent"){
		    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
		    		excellentFeedbackMap.set("Excellent", excellentFeedbackSum);
		    	}
		    	
		      	if(document.getElementById("Q9_" + i).innerText=="Poor"){
		    		poorFeedbackSum = poorFeedbackSum + 1 ;
		    		poorFeedbackMap.set("Poor", poorFeedbackSum);
		    	}if(document.getElementById("Q9_" + i).innerText=="Fair"){
		    		fairFeedbackSum = fairFeedbackSum + 1 ;
		    		fairFeedbackMap.set("Fair", fairFeedbackSum);
		    	}
		    	if(document.getElementById("Q9_" + i).innerText=="Average"){
		    		averageFeedbackSum = averageFeedbackSum + 1 ;
		    		averageFeedbackMap.set("Average", averageFeedbackSum);
		    	}
		    	if(document.getElementById("Q9_" + i).innerText=="Good"){
		    		goodFeedbackSum = goodFeedbackSum + 1 ;
		    		goodFeedbackMap.set("Good", goodFeedbackSum);
		    	}
		    	if(document.getElementById("Q9_" + i).innerText=="Excellent"){
		    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
		    		excellentFeedbackMap.set("Excellent", excellentFeedbackSum);
		    	}
		    	
		      	if(document.getElementById("Q10_" + i).innerText=="Poor"){
		    		poorFeedbackSum = poorFeedbackSum + 1 ;
		    		poorFeedbackMap.set("Poor", poorFeedbackSum);
		    	}if(document.getElementById("Q10_" + i).innerText=="Fair"){
		    		fairFeedbackSum = fairFeedbackSum + 1 ;
		    		fairFeedbackMap.set("Fair", fairFeedbackSum);
		    	}
		    	if(document.getElementById("Q10_" + i).innerText=="Average"){
		    		averageFeedbackSum = averageFeedbackSum + 1 ;
		    		averageFeedbackMap.set("Average", averageFeedbackSum);
		    	}
		    	if(document.getElementById("Q10_" + i).innerText=="Good"){
		    		goodFeedbackSum = goodFeedbackSum + 1 ;
		    		goodFeedbackMap.set("Good", goodFeedbackSum);
		    	}
		    	if(document.getElementById("Q10_" + i).innerText=="Excellent"){
		    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
		    		excellentFeedbackMap.set("Excellent", excellentFeedbackSum);
		    	}
		    	
		      	if(document.getElementById("Q11_" + i).innerText=="Poor"){
		    		poorFeedbackSum = poorFeedbackSum + 1 ;
		    		poorFeedbackMap.set("Poor", poorFeedbackSum);
		    	}if(document.getElementById("Q11_" + i).innerText=="Fair"){
		    		fairFeedbackSum = fairFeedbackSum + 1 ;
		    		fairFeedbackMap.set("Fair", fairFeedbackSum);
		    	}
		    	if(document.getElementById("Q11_" + i).innerText=="Average"){
		    		averageFeedbackSum = averageFeedbackSum + 1 ;
		    		averageFeedbackMap.set("Average", averageFeedbackSum);
		    	}
		    	if(document.getElementById("Q11_" + i).innerText=="Good"){
		    		goodFeedbackSum = goodFeedbackSum + 1 ;
		    		goodFeedbackMap.set("Good", goodFeedbackSum);
		    	}
		    	if(document.getElementById("Q11_" + i).innerText=="Excellent"){
		    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
		    		excellentFeedbackMap.set("Excellent", excellentFeedbackSum);
		    	}
		    	
		      	if(document.getElementById("Q12_" + i).innerText=="Poor"){
		    		poorFeedbackSum = poorFeedbackSum + 1 ;
		    		poorFeedbackMap.set("Poor", poorFeedbackSum);
		    	}if(document.getElementById("Q12_" + i).innerText=="Fair"){
		    		fairFeedbackSum = fairFeedbackSum + 1 ;
		    		fairFeedbackMap.set("Fair", fairFeedbackSum);
		    	}
		    	if(document.getElementById("Q12_" + i).innerText=="Average"){
		    		averageFeedbackSum = averageFeedbackSum + 1 ;
		    		averageFeedbackMap.set("Average", averageFeedbackSum);
		    	}
		    	if(document.getElementById("Q12_" + i).innerText=="Good"){
		    		goodFeedbackSum = goodFeedbackSum + 1 ;
		    		goodFeedbackMap.set("Good", goodFeedbackSum);
		    	}
		    	if(document.getElementById("Q12_" + i).innerText=="Excellent"){
		    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
		    		excellentFeedbackMap.set("Excellent", excellentFeedbackSum);
		    	}
		    	
		      	if(document.getElementById("Q13_" + i).innerText=="Poor"){
		    		poorFeedbackSum = poorFeedbackSum + 1 ;
		    		poorFeedbackMap.set("Poor", poorFeedbackSum);
		    	}if(document.getElementById("Q13_" + i).innerText=="Fair"){
		    		fairFeedbackSum = fairFeedbackSum + 1 ;
		    		fairFeedbackMap.set("Fair", fairFeedbackSum);
		    	}
		    	if(document.getElementById("Q13_" + i).innerText=="Average"){
		    		averageFeedbackSum = averageFeedbackSum + 1 ;
		    		averageFeedbackMap.set("Average", averageFeedbackSum);
		    	}
		    	if(document.getElementById("Q13_" + i).innerText=="Good"){
		    		goodFeedbackSum = goodFeedbackSum + 1 ;
		    		goodFeedbackMap.set("Good", goodFeedbackSum);
		    	}
		    	if(document.getElementById("Q13_" + i).innerText=="Excellent"){
		    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
		    		excellentFeedbackMap.set("Excellent", excellentFeedbackSum);
		    	}	
		    }else{
		    	
		    }
		    	
		    }
		    
		    var trace1 = {
		    		x: Array.from(poorFeedbackMap.keys()),
		            y: Array.from(poorFeedbackMap.values()),
		            name: 'Poor',
		            type: 'bar'
		        };

		        var trace2 = {
		            x: Array.from(fairFeedbackMap.keys()),
		            y: Array.from(fairFeedbackMap.values()),
		            name: 'Fair',
		            type: 'bar'
		        };
		        var trace3 = {
		            x: Array.from(averageFeedbackMap.keys()),
		            y: Array.from(averageFeedbackMap.values()),
		            name: 'Average',
		            type: 'bar'
		        };
		        var trace4 = {
		            x: Array.from(goodFeedbackMap.keys()),
		            y: Array.from(goodFeedbackMap.values()),
		            name: 'Good',
		            type: 'bar'
		        };
		        var trace5 = {
		            x: Array.from(excellentFeedbackMap.keys()),
		            y: Array.from(excellentFeedbackMap.values()),
		            name: 'Excellent',
		            type: 'bar'
		        };

		        var data = [trace1, trace2, trace3, trace4, trace5];




		        var layout = {
		            height: 300,
		            width: 1200,
		            margin: {
		                l: 100,
		                r: 10,
		                b: 50,
		                t: 120,
		                pad: 0
		            },
		            barmode: 'group',
		            showlegend: true,
		            /*   legend: {"orientation": "h"},*/
		            title: '<b>Complaint Ticket</b>',
		            "titlefont": {
		                "size": 16
		            },
		            yaxis: {
		                title: {
		                    text: '<b>Count</b>',
		                    font: {
		                        size: 12
		                    }
		                }
		            },
		            xaxis: {
		                title: {
		                    text: '<b>Status</b>',
		                    font: {
		                        size: 12
		                    }
		                }
		            }
		        }

		        Plotly.newPlot("containerProduct", data, layout, {
		            showSendToCloud: true,
		            displayModeBar: false
		        });
		}
		
		function createRatingGraph(ratingSelected){
			var q1Map = new Map();
			var q2Map = new Map();
			var q3Map = new Map();
			var q4Map = new Map();
			var q5Map = new Map();
			var q6Map = new Map();
			var q7Map = new Map();
			var q8Map = new Map();
			var q9Map = new Map();
			var q10Map = new Map();
			var q11Map = new Map();
			var q12Map = new Map();
			var q13Map = new Map();
		  
		    
		    var q1sum = 0;
		    var q2sum = 0;
		    var q3sum = 0;
		    var q4sum = 0;
		    var q5sum = 0;
		    var q6sum = 0;
		    var q7sum = 0;
		    var q8sum = 0;
		    var q9sum = 0;
		    var q10sum = 0;
		    var q11sum = 0;
		    var q12sum = 0;
		    var q13sum = 0;
		   
		    
		    var complaintFeedbackTable = document.getElementById("customerFeedbackTableBody");
		    for (var i = 0; i < complaintFeedbackTable.rows.length; i++) {
		      
		    	if(document.getElementById("Q1_" + i).innerText==ratingSelected){
		    		q1sum = q1sum + 1 ;
		    		q1Map.set("Q1", q1sum);
		    	}
		    	
		    	if(document.getElementById("Q2_" + i).innerText==ratingSelected){
		    		q2sum = q2sum + 1 ;
		    		q2Map.set("Q2", q2sum);
		    	}
		    	
		      	if(document.getElementById("Q3_" + i).innerText==ratingSelected){
		      		q3sum = q3sum + 1 ;
		    		q3Map.set("Q3", q3sum);
		    	}
		    	
		      	if(document.getElementById("Q4_" + i).innerText==ratingSelected){
		      		q4sum = q4sum + 1 ;
		    		q4Map.set("Q4", q4sum);
		    	}
		      	if(document.getElementById("Q5_" + i).innerText==ratingSelected){
		    		q5sum = q5sum + 1 ;
		    		q5Map.set("Q5", q5sum);
		    	}
		    	
		    	if(document.getElementById("Q6_" + i).innerText==ratingSelected){
		    		q6sum = q6sum + 1 ;
		    		q6Map.set("Q6", q6sum);
		    	}
		    	
		      	if(document.getElementById("Q7_" + i).innerText==ratingSelected){
		      		q7sum = q7sum + 1 ;
		    		q7Map.set("Q7", q7sum);
		    	}
		    	
		      	if(document.getElementById("Q9_" + i).innerText==ratingSelected){
		      		q9sum = q9sum + 1 ;
		    		q9Map.set("Q9", q9sum);
		    	}
		      	if(document.getElementById("Q10_" + i).innerText==ratingSelected){
		    		q10sum = q10sum + 1 ;
		    		q10Map.set("Q10", q10sum);
		    	}
		    	
		    	if(document.getElementById("Q11_" + i).innerText==ratingSelected){
		    		q11sum = q11sum + 1 ;
		    		q11Map.set("Q11", q11sum);
		    	}
		    	
		      	if(document.getElementById("Q12_" + i).innerText==ratingSelected){
		      		q12sum = q12sum + 1 ;
		    		q12Map.set("Q12", q12sum);
		    	}
		    	
		      	if(document.getElementById("Q13_" + i).innerText==ratingSelected){
		      		q13sum = q13sum + 1 ;
		    		q13Map.set("Q13", q13sum);
		    	}
		    	
		      	
		    	
		    }
		    
		    var trace1 = {
		    		x: Array.from(q1Map.keys()),
		            y: Array.from(q1Map.values()),
		            name: 'Q1',
		            type: 'bar'
		        };

		        var trace2 = {
		            x: Array.from(q2Map.keys()),
		            y: Array.from(q2Map.values()),
		            name: 'Q2',
		            type: 'bar'
		        };
		        var trace3 = {
		            x: Array.from(q3Map.keys()),
		            y: Array.from(q3Map.values()),
		            name: 'Q3',
		            type: 'bar'
		        };
		        var trace4 = {
		            x: Array.from(q4Map.keys()),
		            y: Array.from(q4Map.values()),
		            name: 'Q4',
		            type: 'bar'
		        };
		        var trace5 = {
		            x: Array.from(q5Map.keys()),
		            y: Array.from(q5Map.values()),
		            name: 'Q5',
		            type: 'bar'
		        };
		        var trace6 = {
			            x: Array.from(q6Map.keys()),
			            y: Array.from(q6Map.values()),
			            name: 'Q6',
			            type: 'bar'
			        };
		        var trace7 = {
			            x: Array.from(q7Map.keys()),
			            y: Array.from(q7Map.values()),
			            name: 'Q7',
			            type: 'bar'
			        };
		        var trace8 = {
			            x: Array.from(q8Map.keys()),
			            y: Array.from(q8Map.values()),
			            name: 'Q8',
			            type: 'bar'
			        };
		        var trace9 = {
			            x: Array.from(q9Map.keys()),
			            y: Array.from(q9Map.values()),
			            name: 'Q9',
			            type: 'bar'
			        };
		        var trace10 = {
			            x: Array.from(q10Map.keys()),
			            y: Array.from(q10Map.values()),
			            name: 'Q10',
			            type: 'bar'
			        };
		        var trace11 = {
			            x: Array.from(q11Map.keys()),
			            y: Array.from(q11Map.values()),
			            name: 'Q11',
			            type: 'bar'
			        };
		        var trace12 = {
			            x: Array.from(q12Map.keys()),
			            y: Array.from(q12Map.values()),
			            name: 'Q12',
			            type: 'bar'
			        };
		        var trace13 = {
			            x: Array.from(q13Map.keys()),
			            y: Array.from(q13Map.values()),
			            name: 'Q13',
			            type: 'bar'
			        };

		        var data = [trace1, trace2, trace3, trace4, trace5,trace6, trace7, trace8, trace9, trace10,trace11, trace12, trace13];




		        var layout = {
		            height: 300,
		            width: 1200,
		            margin: {
		                l: 100,
		                r: 10,
		                b: 50,
		                t: 120,
		                pad: 0
		            },
		            barmode: 'group',
		            showlegend: true,
		            /*   legend: {"orientation": "h"},*/
		            title: '<b>Complaint Ticket</b>',
		            "titlefont": {
		                "size": 16
		            },
		            yaxis: {
		                title: {
		                    text: '<b>Count</b>',
		                    font: {
		                        size: 12
		                    }
		                }
		            },
		            xaxis: {
		                title: {
		                    text: '<b>Status</b>',
		                    font: {
		                        size: 12
		                    }
		                }
		            }
		        }

		        Plotly.newPlot("containerProduct", data, layout, {
		            showSendToCloud: true,
		            displayModeBar: false
		        });
		}
		
		
		function createGraph(type){
			var poorFeedbackMap = new Map();
		    var fairFeedbackMap = new Map();
		    var averageFeedbackMap = new Map();
		    var goodFeedbackMap = new Map();
		    var excellentFeedbackMap = new Map();
		    
		    var poorFeedbackSum = 0;
		    var fairFeedbackSum = 0;
		    var averageFeedbackSum = 0;
		    var goodFeedbackSum = 0;
		    var excellentFeedbackSum = 0;
		    
		    var complaintFeedbackTable = document.getElementById("customerFeedbackTableBody");
		    for (var i = 0; i < complaintFeedbackTable.rows.length; i++) {	    	    		
		    	if(document.getElementById("Q1_" + i).innerText=="Poor"){
		    		poorFeedbackSum = poorFeedbackSum + 1 ;
		    		poorFeedbackMap.set("Poor", poorFeedbackSum);
		    	}if(document.getElementById("Q1_" + i).innerText=="Fair"){
		    		fairFeedbackSum = fairFeedbackSum + 1 ;
		    		fairFeedbackMap.set("Fair", fairFeedbackSum);
		    	}
		    	if(document.getElementById("Q1_" + i).innerText=="Average"){
		    		averageFeedbackSum = averageFeedbackSum + 1 ;
		    		averageFeedbackMap.set("Average", averageFeedbackSum);
		    	}
		    	if(document.getElementById("Q1_" + i).innerText=="Good"){
		    		goodFeedbackSum = goodFeedbackSum + 1 ;
		    		goodFeedbackMap.set("Good", goodFeedbackSum);
		    	}
		    	if(document.getElementById("Q1_" + i).innerText=="Excellent"){
		    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
		    		excellentFeedbackMap.set("Excellent", excellentFeedbackSum);
		    	}
		    	
		    	if(document.getElementById("Q2_" + i).innerText=="Poor"){
		    		poorFeedbackSum = poorFeedbackSum + 1 ;
		    		poorFeedbackMap.set("Poor", poorFeedbackSum);
		    	}if(document.getElementById("Q2_" + i).innerText=="Fair"){
		    		fairFeedbackSum = fairFeedbackSum + 1 ;
		    		fairFeedbackMap.set("Fair", fairFeedbackSum);
		    	}
		    	if(document.getElementById("Q2_" + i).innerText=="Average"){
		    		averageFeedbackSum = averageFeedbackSum + 1 ;
		    		averageFeedbackMap.set("Average", averageFeedbackSum);
		    	}
		    	if(document.getElementById("Q2_" + i).innerText=="Good"){
		    		goodFeedbackSum = goodFeedbackSum + 1 ;
		    		goodFeedbackMap.set("Good", goodFeedbackSum);
		    	}
		    	if(document.getElementById("Q2_" + i).innerText=="Excellent"){
		    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
		    		excellentFeedbackMap.set("Excellent", excellentFeedbackSum);
		    	}
		    	
		      	if(document.getElementById("Q3_" + i).innerText=="Poor"){
		    		poorFeedbackSum = poorFeedbackSum + 1 ;
		    		poorFeedbackMap.set("Poor", poorFeedbackSum);
		    	}if(document.getElementById("Q3_" + i).innerText=="Fair"){
		    		fairFeedbackSum = fairFeedbackSum + 1 ;
		    		fairFeedbackMap.set("Fair", fairFeedbackSum);
		    	}
		    	if(document.getElementById("Q3_" + i).innerText=="Average"){
		    		averageFeedbackSum = averageFeedbackSum + 1 ;
		    		averageFeedbackMap.set("Average", averageFeedbackSum);
		    	}
		    	if(document.getElementById("Q3_" + i).innerText=="Good"){
		    		goodFeedbackSum = goodFeedbackSum + 1 ;
		    		goodFeedbackMap.set("Good", goodFeedbackSum);
		    	}
		    	if(document.getElementById("Q3_" + i).innerText=="Excellent"){
		    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
		    		excellentFeedbackMap.set("Excellent", excellentFeedbackSum);
		    	}
		    	
		      	if(document.getElementById("Q4_" + i).innerText=="Poor"){
		    		poorFeedbackSum = poorFeedbackSum + 1 ;
		    		poorFeedbackMap.set("Poor", poorFeedbackSum);
		    	}if(document.getElementById("Q4_" + i).innerText=="Fair"){
		    		fairFeedbackSum = fairFeedbackSum + 1 ;
		    		fairFeedbackMap.set("Fair", fairFeedbackSum);
		    	}
		    	if(document.getElementById("Q4_" + i).innerText=="Average"){
		    		averageFeedbackSum = averageFeedbackSum + 1 ;
		    		averageFeedbackMap.set("Average", averageFeedbackSum);
		    	}
		    	if(document.getElementById("Q4_" + i).innerText=="Good"){
		    		goodFeedbackSum = goodFeedbackSum + 1 ;
		    		goodFeedbackMap.set("Good", goodFeedbackSum);
		    	}
		    	if(document.getElementById("Q4_" + i).innerText=="Excellent"){
		    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
		    		excellentFeedbackMap.set("Excellent", excellentFeedbackSum);
		    	}
		    	
		      	if(document.getElementById("Q5_" + i).innerText=="Poor"){
		    		poorFeedbackSum = poorFeedbackSum + 1 ;
		    		poorFeedbackMap.set("Poor", poorFeedbackSum);
		    	}if(document.getElementById("Q5_" + i).innerText=="Fair"){
		    		fairFeedbackSum = fairFeedbackSum + 1 ;
		    		fairFeedbackMap.set("Fair", fairFeedbackSum);
		    	}
		    	if(document.getElementById("Q5_" + i).innerText=="Average"){
		    		averageFeedbackSum = averageFeedbackSum + 1 ;
		    		averageFeedbackMap.set("Average", averageFeedbackSum);
		    	}
		    	if(document.getElementById("Q5_" + i).innerText=="Good"){
		    		goodFeedbackSum = goodFeedbackSum + 1 ;
		    		goodFeedbackMap.set("Good", goodFeedbackSum);
		    	}
		    	if(document.getElementById("Q5_" + i).innerText=="Excellent"){
		    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
		    		excellentFeedbackMap.set("Q5", excellentFeedbackSum);
		    	}
		    	
		      	if(document.getElementById("Q6_" + i).innerText=="Poor"){
		    		poorFeedbackSum = poorFeedbackSum + 1 ;
		    		poorFeedbackMap.set("Poor", poorFeedbackSum);
		    	}if(document.getElementById("Q6_" + i).innerText=="Fair"){
		    		fairFeedbackSum = fairFeedbackSum + 1 ;
		    		fairFeedbackMap.set("Fair", fairFeedbackSum);
		    	}
		    	if(document.getElementById("Q6_" + i).innerText=="Average"){
		    		averageFeedbackSum = averageFeedbackSum + 1 ;
		    		averageFeedbackMap.set("Average", averageFeedbackSum);
		    	}
		    	if(document.getElementById("Q6_" + i).innerText=="Good"){
		    		goodFeedbackSum = goodFeedbackSum + 1 ;
		    		goodFeedbackMap.set("Good", goodFeedbackSum);
		    	}
		    	if(document.getElementById("Q6_" + i).innerText=="Excellent"){
		    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
		    		excellentFeedbackMap.set("Excellent", excellentFeedbackSum);
		    	}
		    	
		      	if(document.getElementById("Q7_" + i).innerText=="Poor"){
		    		poorFeedbackSum = poorFeedbackSum + 1 ;
		    		poorFeedbackMap.set("Poor", poorFeedbackSum);
		    	}if(document.getElementById("Q7_" + i).innerText=="Fair"){
		    		fairFeedbackSum = fairFeedbackSum + 1 ;
		    		fairFeedbackMap.set("Fair", fairFeedbackSum);
		    	}
		    	if(document.getElementById("Q7_" + i).innerText=="Average"){
		    		averageFeedbackSum = averageFeedbackSum + 1 ;
		    		averageFeedbackMap.set("Average", averageFeedbackSum);
		    	}
		    	if(document.getElementById("Q7_" + i).innerText=="Good"){
		    		goodFeedbackSum = goodFeedbackSum + 1 ;
		    		goodFeedbackMap.set("Good", goodFeedbackSum);
		    	}
		    	if(document.getElementById("Q7_" + i).innerText=="Excellent"){
		    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
		    		excellentFeedbackMap.set("Excellent", excellentFeedbackSum);
		    	}
		    	
		      	if(document.getElementById("Q8_" + i).innerText=="Poor"){
		    		poorFeedbackSum = poorFeedbackSum + 1 ;
		    		poorFeedbackMap.set("Poor", poorFeedbackSum);
		    	}if(document.getElementById("Q8_" + i).innerText=="Fair"){
		    		fairFeedbackSum = fairFeedbackSum + 1 ;
		    		fairFeedbackMap.set("Fair", fairFeedbackSum);
		    	}
		    	if(document.getElementById("Q8_" + i).innerText=="Average"){
		    		averageFeedbackSum = averageFeedbackSum + 1 ;
		    		averageFeedbackMap.set("Average", averageFeedbackSum);
		    	}
		    	if(document.getElementById("Q8_" + i).innerText=="Good"){
		    		goodFeedbackSum = goodFeedbackSum + 1 ;
		    		goodFeedbackMap.set("Good", goodFeedbackSum);
		    	}
		    	if(document.getElementById("Q8_" + i).innerText=="Excellent"){
		    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
		    		excellentFeedbackMap.set("Excellent", excellentFeedbackSum);
		    	}
		    	
		      	if(document.getElementById("Q9_" + i).innerText=="Poor"){
		    		poorFeedbackSum = poorFeedbackSum + 1 ;
		    		poorFeedbackMap.set("Poor", poorFeedbackSum);
		    	}if(document.getElementById("Q9_" + i).innerText=="Fair"){
		    		fairFeedbackSum = fairFeedbackSum + 1 ;
		    		fairFeedbackMap.set("Fair", fairFeedbackSum);
		    	}
		    	if(document.getElementById("Q9_" + i).innerText=="Average"){
		    		averageFeedbackSum = averageFeedbackSum + 1 ;
		    		averageFeedbackMap.set("Average", averageFeedbackSum);
		    	}
		    	if(document.getElementById("Q9_" + i).innerText=="Good"){
		    		goodFeedbackSum = goodFeedbackSum + 1 ;
		    		goodFeedbackMap.set("Good", goodFeedbackSum);
		    	}
		    	if(document.getElementById("Q9_" + i).innerText=="Excellent"){
		    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
		    		excellentFeedbackMap.set("Excellent", excellentFeedbackSum);
		    	}
		    	
		      	if(document.getElementById("Q10_" + i).innerText=="Poor"){
		    		poorFeedbackSum = poorFeedbackSum + 1 ;
		    		poorFeedbackMap.set("Poor", poorFeedbackSum);
		    	}if(document.getElementById("Q10_" + i).innerText=="Fair"){
		    		fairFeedbackSum = fairFeedbackSum + 1 ;
		    		fairFeedbackMap.set("Fair", fairFeedbackSum);
		    	}
		    	if(document.getElementById("Q10_" + i).innerText=="Average"){
		    		averageFeedbackSum = averageFeedbackSum + 1 ;
		    		averageFeedbackMap.set("Average", averageFeedbackSum);
		    	}
		    	if(document.getElementById("Q10_" + i).innerText=="Good"){
		    		goodFeedbackSum = goodFeedbackSum + 1 ;
		    		goodFeedbackMap.set("Good", goodFeedbackSum);
		    	}
		    	if(document.getElementById("Q10_" + i).innerText=="Excellent"){
		    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
		    		excellentFeedbackMap.set("Excellent", excellentFeedbackSum);
		    	}
		    	
		      	if(document.getElementById("Q11_" + i).innerText=="Poor"){
		    		poorFeedbackSum = poorFeedbackSum + 1 ;
		    		poorFeedbackMap.set("Poor", poorFeedbackSum);
		    	}if(document.getElementById("Q11_" + i).innerText=="Fair"){
		    		fairFeedbackSum = fairFeedbackSum + 1 ;
		    		fairFeedbackMap.set("Fair", fairFeedbackSum);
		    	}
		    	if(document.getElementById("Q11_" + i).innerText=="Average"){
		    		averageFeedbackSum = averageFeedbackSum + 1 ;
		    		averageFeedbackMap.set("Average", averageFeedbackSum);
		    	}
		    	if(document.getElementById("Q11_" + i).innerText=="Good"){
		    		goodFeedbackSum = goodFeedbackSum + 1 ;
		    		goodFeedbackMap.set("Good", goodFeedbackSum);
		    	}
		    	if(document.getElementById("Q11_" + i).innerText=="Excellent"){
		    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
		    		excellentFeedbackMap.set("Excellent", excellentFeedbackSum);
		    	}
		    	
		      	if(document.getElementById("Q12_" + i).innerText=="Poor"){
		    		poorFeedbackSum = poorFeedbackSum + 1 ;
		    		poorFeedbackMap.set("Poor", poorFeedbackSum);
		    	}if(document.getElementById("Q12_" + i).innerText=="Fair"){
		    		fairFeedbackSum = fairFeedbackSum + 1 ;
		    		fairFeedbackMap.set("Fair", fairFeedbackSum);
		    	}
		    	if(document.getElementById("Q12_" + i).innerText=="Average"){
		    		averageFeedbackSum = averageFeedbackSum + 1 ;
		    		averageFeedbackMap.set("Average", averageFeedbackSum);
		    	}
		    	if(document.getElementById("Q12_" + i).innerText=="Good"){
		    		goodFeedbackSum = goodFeedbackSum + 1 ;
		    		goodFeedbackMap.set("Good", goodFeedbackSum);
		    	}
		    	if(document.getElementById("Q12_" + i).innerText=="Excellent"){
		    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
		    		excellentFeedbackMap.set("Excellent", excellentFeedbackSum);
		    	}
		    	
		      	if(document.getElementById("Q13_" + i).innerText=="Poor"){
		    		poorFeedbackSum = poorFeedbackSum + 1 ;
		    		poorFeedbackMap.set("Poor", poorFeedbackSum);
		    	}if(document.getElementById("Q13_" + i).innerText=="Fair"){
		    		fairFeedbackSum = fairFeedbackSum + 1 ;
		    		fairFeedbackMap.set("Fair", fairFeedbackSum);
		    	}
		    	if(document.getElementById("Q13_" + i).innerText=="Average"){
		    		averageFeedbackSum = averageFeedbackSum + 1 ;
		    		averageFeedbackMap.set("Average", averageFeedbackSum);
		    	}
		    	if(document.getElementById("Q13_" + i).innerText=="Good"){
		    		goodFeedbackSum = goodFeedbackSum + 1 ;
		    		goodFeedbackMap.set("Good", goodFeedbackSum);
		    	}
		    	if(document.getElementById("Q13_" + i).innerText=="Excellent"){
		    		excellentFeedbackSum = excellentFeedbackSum + 1 ;
		    		excellentFeedbackMap.set("Excellent", excellentFeedbackSum);
		    	}		    		
		    	
		    }
		    
		    var trace1 = {
		    		x: Array.from(poorFeedbackMap.keys()),
		            y: Array.from(poorFeedbackMap.values()),
		            name: 'Poor',
		            type: 'bar'
		        };

		        var trace2 = {
		            x: Array.from(fairFeedbackMap.keys()),
		            y: Array.from(fairFeedbackMap.values()),
		            name: 'Fair',
		            type: 'bar'
		        };
		        var trace3 = {
		            x: Array.from(averageFeedbackMap.keys()),
		            y: Array.from(averageFeedbackMap.values()),
		            name: 'Average',
		            type: 'bar'
		        };
		        var trace4 = {
		            x: Array.from(goodFeedbackMap.keys()),
		            y: Array.from(goodFeedbackMap.values()),
		            name: 'Good',
		            type: 'bar'
		        };
		        var trace5 = {
		            x: Array.from(excellentFeedbackMap.keys()),
		            y: Array.from(excellentFeedbackMap.values()),
		            name: 'Excellent',
		            type: 'bar'
		        };

		        var data = [trace1, trace2, trace3, trace4, trace5];




		        var layout = {
		            height: 300,
		            width: 1200,
		            margin: {
		                l: 100,
		                r: 10,
		                b: 50,
		                t: 120,
		                pad: 0
		            },
		            barmode: 'group',
		            showlegend: true,
		            /*   legend: {"orientation": "h"},*/
		            title: '<b>Complaint Ticket</b>',
		            "titlefont": {
		                "size": 16
		            },
		            yaxis: {
		                title: {
		                    text: '<b>Count</b>',
		                    font: {
		                        size: 12
		                    }
		                }
		            },
		            xaxis: {
		                title: {
		                    text: '<b>Status</b>',
		                    font: {
		                        size: 12
		                    }
		                }
		            }
		        }

		        Plotly.newPlot("containerProduct", data, layout, {
		            showSendToCloud: true,
		            displayModeBar: false
		        });
		}
		
		function resetCustomerIdFilter(){
			document.getElementById("productSel").value = "";
			$("#productSel").val("allCustomerID");
			filterOnCustomerID();
			
		}
		function resetQuestionFilter(){
			 $("#finishSel").val("allQuestion");
			 filterOnQuestions();
		}

		function resetRatingFilter(){
			$("#qualitySel").val("allRatingVal");
			filterOnRating();
		}
		
		function filterOnCategory(){
			var categorySearchedVal = $("#categoryID").val();
			console.log(categorySearchedVal);
			if(categorySearchedVal=="all"){
				document.getElementById("displayCustId").style.display = "none";
				document.getElementById("displayQuestId").style.display = "none";
				document.getElementById("displayRatingId").style.display = "none";
				createGraph('all');
			}
			else if(categorySearchedVal=="custId"){
				document.getElementById("displayCustId").style.display = "block";
				document.getElementById("displayQuestId").style.display = "none";
				document.getElementById("displayRatingId").style.display = "none";
				$("#productSel").val("allCustomerID");
				createGraph('all');
			}
			else if(categorySearchedVal=="questn"){
				document.getElementById("displayQuestId").style.display = "block";
				document.getElementById("displayCustId").style.display = "none";
				document.getElementById("displayRatingId").style.display = "none";
				$("#finishSel").val("allQuestion");
				createGraph('all');
			}
			else if(categorySearchedVal=="rating"){
				document.getElementById("displayRatingId").style.display = "block";
				document.getElementById("displayCustId").style.display = "none";
				document.getElementById("displayQuestId").style.display = "none";
				$("#qualitySel").val("allRatingVal");
				createGraph('all');
			}
			
		}
		
		function formatDate(date) {
			date = date.replace("IST", "");
		    var d = new Date(date),
		        month = '' + (d.getMonth() + 1),
		        day = '' + d.getDate(),
		        year = d.getFullYear();

		    if (month.length < 2) month = '0' + month;
		    if (day.length < 2) day = '0' + day;

		    return [month, day , year].join('/');
		}
		
		function filterOnCustomerID(){
			var selectedCustId = $("#productSel").val();
			console.log(selectedCustId);
			if(selectedCustId=="" || selectedCustId== null || selectedCustId == "allCustomerID"){
				createGraph('all');
			}else{
				createCustomerIdGraph(selectedCustId);
			}
		}
		function filterOnQuestions(){
			var selectedQuestionsType = $("#finishSel").val();
			console.log(selectedQuestionsType);
			if(selectedQuestionsType=="" || selectedQuestionsType== null || selectedQuestionsType == "allQuestion"){
				createGraph('all');
			}else{
				createQuestionsGraph(selectedQuestionsType);
			}
		}
		function filterOnRating(){
			var selectedRatingType = $("#qualitySel").val();
			console.log(selectedRatingType);
			if(selectedRatingType=="" || selectedRatingType== null || selectedRatingType == "allRatingVal"){
				createGraph('all');
			}else{
				createRatingGraph(selectedRatingType);
			}
		}
