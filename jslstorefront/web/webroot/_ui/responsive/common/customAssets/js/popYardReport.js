		function filterOnFinish(){
			var finishSearchedVal = document.getElementById("finishSel").value;
			//var qualitySearchedVal = document.getElementById("qualitySel").value;
			
		    var tab360 = document.getElementById("cust360PopTable"); 
		    var mrktBalSumTotal = 0;
			var gradeMap = new Map();
		    var grdeCells = document.getElementsByClassName("grde");
			var gradeArray = [];
			var mktBalSumArray = [];
			var marketBalCells = document.getElementsByClassName("mktBalSel");
	
		    for(i = 0 ; i < tab360.rows.length-1 ; i++) 
		    {     
		    	var tr = document.getElementById("row_" + i);
		    	 var finish = document.getElementById("finish_" + i).innerText; 
		    	//var quality = document.getElementById("quality_" + i).innerText; 
		    	
		    	
		    	if(finish === finishSearchedVal || finishSearchedVal.length === 0){
		    		//document.getElementById("row_" + i).style.display = "table-row";
		    		 mrktBalSumTotal = mrktBalSumTotal + parseFloat(document.getElementById("mktBal_" + i).innerText);

		 			        if (!gradeMap.has(grdeCells[i].innerText)) {
		 			            gradeMap.set(grdeCells[i].innerText, marketBalCells[i].innerText);

		 			        } else {
		 			            var old = gradeMap.get(grdeCells[i].innerText);
		 			            var newValue = parseFloat(old) + parseFloat(marketBalCells[i].innerText);
		 			            gradeMap.set(grdeCells[i].innerText, newValue.toString());

		 			        }
		 			   
		 			    for (var val of gradeMap.keys()) {

		 			        gradeArray.push(val);
		 			    }
		 			    for (var val of gradeMap.values()) {

		 			        mktBalSumArray.push(val);
		 			    }
		 			   var flagArray = mktBalSumArray.filter(function(elem){
		 			    	if(elem > 0){
		 			    		return true;
		 			    	}
		 			    	return false;
		 			    });
		 			    
		 			    if(flagArray.length != 0){
		 			    //graph js
		 			    document.getElementById("containerFinish").innerHTML = "";
		 			    
		 			   var data = [{
		 			        values: mktBalSumArray,
		 			        labels: gradeArray,
		 			        marker: {
		 			            colors: [
		 			            	'#f2476a',
		 			            	'#add8e6',
		 			            	'#90ee90',
		 			            	'#eb2d3a',
		 			            	'#fb654e',
		 			            	'#FFCC99',
		 				              '#99FFCC',
		 				              '#CCCC99',
		 				              '#CCCCCC',
		 				              '#CCCCFF',
		 				              '#CCFF99',
		 				              '#CCFFCC',
		 				              '#CCFFFF',
		 				              '#FFCCCC',
		 				              '#FFFF99',
		 				              '#FFCCFF',
		 				              '#FFFFCC',
		 			            ]},
		 			      		        type: 'pie'
		 			    }];

		 			   var layout = {
		 					  height: 400,
				    		  
				    		  margin: {
				    			    l: 10,
				    			    r: 55,
				    			    b:50,
				    			    t: 70,
				    			    pad: 10
				    			  },
				    		  showlegend: true,
				    		  legend: {"orientation": "h"},
					        title: '<b>Finish Vs Marketing Balance</b>',
					        "titlefont": {
							    "size": 14
							  }
					    }

		 			    Plotly.newPlot('containerFinish', data, layout, {
		 			        showSendToCloud: true ,
		 			       displayModeBar: false
		 			    });
		 			 }else{
		 				$("#productTypeVsMktBal").html("<div class='reportTitle'>Product Type Vs Marketing Balance</div>");
						$("#qualityVsMktBal").html("<div class='reportTitle'>Quality Vs Marketing Balance</div>");
						$("#finishVsMktBal").html("<div class='reportTitle'>Finish Vs Marketing Balance</div>");
						$("#containerProduct").html('<div class="noGraphData"><marquee class="mrq_data">No data available for graphical representation</marquee></div>');
						$("#containerQuality").html('<div class="noGraphData"><marquee class="mrq_data">No data available for graphical representation</marquee></div>');
						$("#containerFinish").html('<div class="noGraphData"><marquee class="mrq_data">No data available for graphical representation</marquee></div>');
		 			 }
		 			    
		 			    // new graph
		 			    
		 			/*    var data = [{
		 				  values: mktBalSumArray,
		 				  labels: gradeArray,
		 				  domain: {column: 0},
		 				  name: 'GHG Emissions',
		 				  hoverinfo: 'label+percent+name',
		 				  hole: .4,
		 				  type: 'pie'
		 				}];
		 			    
		 			  var layout = {
		 					  title: 'Global Emissions 1990-2011',
		 					  annotations: [
		 					    {
		 					      font: {
		 					        size: 20
		 					      },
		 					      showarrow: false,
		 					      text: 'GHG',
		 					      x: 0.17,
		 					      y: 0.5
		 					    },
		 					    {
		 					      font: {
		 					        size: 20
		 					      },
		 					      showarrow: false,
		 					      text: 'CO2',
		 					      x: 0.82,
		 					      y: 0.5
		 					    }
		 					  ],
		 					  height: 300,
		 					  width: 500,
		 					  showlegend: false,
		 					  grid: {rows: 1, columns: 2}
		 					};

		 					Plotly.newPlot('containerFinish', data, layout); */


		 			    
		    		
		    	}else{
		    		//document.getElementById("row_" + i).style.display = "";
		    		 //$("#row_" + i).hide();
		    	}
		    	
		    }
		}
		
		function filterOnQuality(){
			var qualitySearchedVal = document.getElementById("qualitySel").value;
			
		    var tab360 = document.getElementById("cust360PopTable"); 
		    var mrktBalSumTotal = 0;
			var gradeMap = new Map();
		    var grdeCells = document.getElementsByClassName("grde");
			var gradeArray = [];
			var mktBalSumArray = [];
			var marketBalCells = document.getElementsByClassName("mktBalSel");
	
		    for(i = 0 ; i < tab360.rows.length-1 ; i++) 
		    {     
		    	var tr = document.getElementById("row_" + i);
		    	 //var finish = document.getElementById("finish_" + i).innerText; 
		    	var quality = document.getElementById("quality_" + i).innerText; 
		    	
		    	
		    	if(quality === qualitySearchedVal || qualitySearchedVal.length === 0){
		    		//document.getElementById("row_" + i).style.display = "table-row";
		    		
		    		
		 		   
		 		        mrktBalSumTotal = mrktBalSumTotal + parseFloat(document.getElementById("mktBal_" + i).innerText);

		 			        if (!gradeMap.has(grdeCells[i].innerText)) {
		 			            gradeMap.set(grdeCells[i].innerText, marketBalCells[i].innerText);

		 			        } else {
		 			            var old = gradeMap.get(grdeCells[i].innerText);
		 			            var newValue = parseFloat(old) + parseFloat(marketBalCells[i].innerText);
		 			            gradeMap.set(grdeCells[i].innerText, newValue.toString());

		 			        }
		 			   
		 			    for (var val of gradeMap.keys()) {

		 			        gradeArray.push(val);
		 			    }
		 			    for (var val of gradeMap.values()) {

		 			        mktBalSumArray.push(val);
		 			    }
		 			    
		 			   var flagArray = mktBalSumArray.filter(function(elem){
		 			    	if(elem > 0){
		 			    		return true;
		 			    	}
		 			    	return false;
		 			    });
		 			    
		 			    if(flagArray.length != 0){
		 			    //graph js
		 			    document.getElementById("containerQuality").innerHTML = "";
		 			   var data = [{
		 			        values: mktBalSumArray,
		 			        labels: gradeArray,
		 			        marker: {
		 			            colors: [
		 			            	'#f2476a',
		 			            	'#add8e6',
		 			            	'#90ee90',
		 			            	'#eb2d3a',
		 			            	'#fb654e',
		 			            	'#FFCC99',
		 				              '#99FFCC',
		 				              '#CCCC99',
		 				              '#CCCCCC',
		 				              '#CCCCFF',
		 				              '#CCFF99',
		 				              '#CCFFCC',
		 				              '#CCFFFF',
		 				              '#FFCCCC',
		 				              '#FFFF99',
		 				              '#FFCCFF',
		 				              '#FFFFCC',
		 			            ]},
		 			      		        type: 'pie'
		 			    }];

		 			   var layout = {
		 					  height: 400,
				    		  
				    		  margin: {
				    			    l: 10,
				    			    r: 55,
				    			    b:50,
				    			    t: 70,
				    			    pad: 10
				    			  },
				    		  showlegend: true,
				    		  legend: {"orientation": "h"},
					        title: '<b>Quality Vs Marketing Balance</b>',
					        "titlefont": {
							    "size": 14,
							    textposition: 'auto',
							  }
					    }

		 			    Plotly.newPlot('containerQuality', data, layout, {
		 			        showSendToCloud: true ,
		 			       displayModeBar: false
		 			    });
		 			}else{
		 				$("#productTypeVsMktBal").html("<div class='reportTitle'>Product Type Vs Marketing Balance</div>");
						$("#qualityVsMktBal").html("<div class='reportTitle'>Quality Vs Marketing Balance</div>");
						$("#finishVsMktBal").html("<div class='reportTitle'>Finish Vs Marketing Balance</div>");
						$("#containerProduct").html('<div class="noGraphData"><marquee class="mrq_data">No data available for graphical representation</marquee></div>');
						$("#containerQuality").html('<div class="noGraphData"><marquee class="mrq_data">No data available for graphical representation</marquee></div>');
						$("#containerFinish").html('<div class="noGraphData"><marquee class="mrq_data">No data available for graphical representation</marquee></div>');
			    	}
		 		    
		    		
		    	}
		    	
		    }
		}
		
		function filterOnProductType(){
			var productSearchedVal = document.getElementById("productSel").value;	
			//alert("productSearchedVal: "+productSearchedVal);
		    var tab360 = document.getElementById("cust360PopTable"); 
		    var mrktBalSumTotal = 0;
			var gradeMap = new Map();
		    var grdeCells = document.getElementsByClassName("grde");
			var gradeArray = [];
			var mktBalSumArray = [];
			var marketBalCells = document.getElementsByClassName("mktBalSel");
			//alert("hello");
		    for(i = 0 ; i < tab360.rows.length-1 ; i++) 
		    {     
		    	var tr = document.getElementById("row_" + i);
		    	 //var finish = document.getElementById("finish_" + i).innerText; 
		    	var product = document.getElementById("productType_" + i).innerText; 
		    	//alert("product : "+product);
		    	
		    	if(product === productSearchedVal || productSearchedVal.length === 0){
		    		
		 		        mrktBalSumTotal = mrktBalSumTotal + parseFloat(document.getElementById("mktBal_" + i).innerText);

		 			        if (!gradeMap.has(grdeCells[i].innerText)) {
		 			            gradeMap.set(grdeCells[i].innerText, marketBalCells[i].innerText);

		 			        } else {
		 			            var old = gradeMap.get(grdeCells[i].innerText);
		 			            var newValue = parseFloat(old) + parseFloat(marketBalCells[i].innerText);
		 			            gradeMap.set(grdeCells[i].innerText, newValue.toString());

		 			        }
		 			   
		 			    for (var val of gradeMap.keys()) {

		 			        gradeArray.push(val);
		 			    }
		 			    for (var val of gradeMap.values()) {

		 			        mktBalSumArray.push(val);
		 			    }
		 			    
		 			   var flagArray = mktBalSumArray.filter(function(elem){
		 			    	if(elem > 0){
		 			    		return true;
		 			    	}
		 			    	return false;
		 			    });
		 			    
		 			    if(flagArray.length != 0){
		 			    //graph js
		 			    document.getElementById("containerProduct").innerHTML = "";
		 			   var data = [{
		 			        values: mktBalSumArray,
		 			        labels: gradeArray,
		 			        marker: {
		 			            colors: [
		 			            	'#f2476a',
		 			            	'#add8e6',
		 			            	'#90ee90',
		 			            	'#eb2d3a',
		 			            	'#fb654e',
		 			            	'#FFCC99',
		 				              '#99FFCC',
		 				              '#CCCC99',
		 				              '#CCCCCC',
		 				              '#CCCCFF',
		 				              '#CCFF99',
		 				              '#CCFFCC',
		 				              '#CCFFFF',
		 				              '#FFCCCC',
		 				              '#FFFF99',
		 				              '#FFCCFF',
		 				              '#FFFFCC',
		 			            ]},
		 			      		        type: 'pie'
		 			    }];
		 			    
		 			    

		 			   var layout = {
		 					  height: 400,
				    		  
				    		  margin: {
				    			    l: 10,
				    			    r: 55,
				    			    b:50,
				    			    t: 70,
				    			    pad: 10
				    			  },
				    		  showlegend: true,
				    		  legend: {"orientation": "h"},
					        title: '<b>Product Type Vs Marketing Balance</b>',
					        "titlefont": {
							    "size": 14
							  }
					    }

		 			    Plotly.newPlot('containerProduct', data, layout, {
		 			        showSendToCloud: true,
		 			       displayModeBar: false
		 			    });
		 			    }else{
		 			    	$("#productTypeVsMktBal").html("<div class='reportTitle'>Product Type Vs Marketing Balance</div>");
							$("#qualityVsMktBal").html("<div class='reportTitle'>Quality Vs Marketing Balance</div>");
							$("#finishVsMktBal").html("<div class='reportTitle'>Finish Vs Marketing Balance</div>");
							$("#containerProduct").html('<div class="noGraphData"><marquee class="mrq_data">No data available for graphical representation</marquee></div>');
							$("#containerQuality").html('<div class="noGraphData"><marquee class="mrq_data">No data available for graphical representation</marquee></div>');
							$("#containerFinish").html('<div class="noGraphData"><marquee class="mrq_data">No data available for graphical representation</marquee></div>');
				    	}
		    		
		    	}
		    	
		    }
		}
		
		window.onload = function() {
			$.ajax({
				type: "GET",
				url: ACC.config.encodedContextPath + "/reports/reportsJson?report=POPYARDREPORT&CSRFToken=" + ACC.config.CSRFToken,
				success: function(response){
					 $("#failureDataCheck").hide();
					createTable(response);
					postTableCreation();
				},
				error: function(response){
					 $("#successDataCheck").hide();
			 		 $('#loading-wrapper').hide();		  
			 		 $("#failureDataCheck").show();
				}
			});
		}
		
		function createTable(json){
			var table = document.getElementById("popTable");
			json.forEach(function(item, index){
				var row = table.insertRow(index);
				row.id = "row_" + index;
				var cell0 = row.insertCell(0);
				cell0.id = "so_" + index;
				cell0.innerText = item.salesOrderNumber;
				var cell1 = row.insertCell(1);
				cell1.id = "item_" + index;
				cell1.innerText = item.item;
				var cell2 = row.insertCell(2);
				cell2.id = "productType_" + index;
				cell2.innerText = item.productType;
				cell2.classList.add("prdType");
				var cell3 = row.insertCell(3);
				cell3.id = "soDate_" + index;
				cell3.innerText = item.salesOrderDate;
				var cell4 = row.insertCell(4);
				cell4.id = "grade_" + index;
				cell4.innerText = item.grade;
				cell4.classList.add("grde");
				var cell5 = row.insertCell(5);
				cell5.id = "thk_" + index;
				cell5.innerText = item.thickness;
				var cell6 = row.insertCell(6);
				cell6.id = "wdth_" + index;
				cell6.innerText = item.width;
				var cell7 = row.insertCell(7);
				cell7.id = "len_" + index;
				cell7.innerText = item.length;
				var cell8 = row.insertCell(8);
				cell8.id = "edge_" + index;
				cell8.innerText = item.edgeCondition;
				var cell9 = row.insertCell(9);
				cell9.id = "finish_" + index;
				cell9.innerText = item.finish;
				cell9.classList.add("finish");
				var cell10 = row.insertCell(10);
				cell10.id = "quality_" + index;
				cell10.innerText = item.quality;
				cell10.classList.add("quality");
				var cell11 = row.insertCell(11);
				cell11.id = "baseRate_" + index;
				cell11.innerText = item.basicPrice;
				var cell12 = row.insertCell(12);
				cell12.id = "ordQty_" + index;
				cell12.innerText = item.orderItemQuantity;
				var cell13 = row.insertCell(13);
				cell13.id = "despQty_" + index;
				cell13.innerText = item.dispatchQuantity;
				var cell14 = row.insertCell(14);
				cell14.id = "mktBal_" + index;
				cell14.innerText = item.marketBalance;
				cell14.classList.add("mktBalSel")
				var cell15 = row.insertCell(15);
				cell15.id = "allocQty_" + index;
				cell15.innerText = item.orderAmount;
				var cell16 = row.insertCell(16);
				cell16.id = "oaNo_" + index;
				cell16.innerText = item.orderAcceptanceNumber;
				var cell17 = row.insertCell(17);
				cell17.id = "cusReqDel_" + index;
				cell17.innerText = item.customerRequiredDeliveryDate;
				var cell18 = row.insertCell(18);
				cell18.id = "pwMin_" + index;
				cell18.innerText = item.partWtMin;
				var cell19 = row.insertCell(19);
				cell19.id = "pwMax_" + index;
				cell19.innerText = item.partWtMax;
			});
		}

		
		function postTableCreation() {
			$("#loading-wrapper").hide();
			var qualityOptions = new Set();
			var qualitySel = document.getElementById("qualitySel");
			
			var productOptions = new Set();
			var productSel = document.getElementById("productSel");
			 var productCells = document.getElementsByClassName("prdType");
			    for (var i = 0; i < productCells.length; i++) {

			        if (!productOptions.has(productCells[i].innerText)) {
			        	productOptions.add(productCells[i].innerText);
			        }
			    }
			    productOptions.forEach(function(elem) {
			    	productSel.options.add(new Option(elem, elem));
			    });
			    
			    
		    var qualityCells = document.getElementsByClassName("quality");
		    for (var i = 0; i < qualityCells.length; i++) {

		        if (!qualityOptions.has(qualityCells[i].innerText)) {
		            qualityOptions.add(qualityCells[i].innerText);
		        }
		    }
		    qualityOptions.forEach(function(elem) {
		        qualitySel.options.add(new Option(elem, elem));
		    });

		    // for grade
		    var gradeMap = new Map();
		    var finishOptions = new Set();
		    var finishSel = document.getElementById("finishSel");

		    var finishCells = document.getElementsByClassName("finish");
		    var mktSum = 0;
		    for (var i = 0; i < finishCells.length; i++) {

		        if (!finishOptions.has(finishCells[i].innerText)) {
		            finishOptions.add(finishCells[i].innerText);
		            gradeMap.set(finishOptions, 'foo');
		        }
		    }

		    finishOptions.forEach(function(elem) {
		        finishSel.options.add(new Option(elem, elem));
		    });



		    var gradeArray = [];
		    var mktBalSumArray = [];
		    var gradeMap = new Map();
		    var grdeCells = document.getElementsByClassName("grde");
		    var marketBalCells = document.getElementsByClassName("mktBalSel");
		    for (var i = 0; i < grdeCells.length; i++) {

		        if (!gradeMap.has(grdeCells[i].innerText)) {
		            gradeMap.set(grdeCells[i].innerText, marketBalCells[i].innerText);

		        } else {
		            var old = gradeMap.get(grdeCells[i].innerText);
		            var newValue = parseFloat(old) + parseFloat(marketBalCells[i].innerText);
		            gradeMap.set(grdeCells[i].innerText, newValue.toString());

		        }
		    }
		    for (var val of gradeMap.keys()) {

		        gradeArray.push(val);
		    }
		    for (var val of gradeMap.values()) {

		        mktBalSumArray.push(val);
		    }

		    var mrktBalSumTotal = 0;
		    for (var i = 0; i < marketBalCells.length; i++) {
		        mrktBalSumTotal = mrktBalSumTotal + parseFloat(marketBalCells[i].innerText);
		    }

		    var flagArray = mktBalSumArray.filter(function(elem){
		    	if(elem > 0){
		    		return true;
		    	}
		    	return false;
		    });
		    
		    if(flagArray.length != 0){
			    //graph js
			    var data = [{
			        values: mktBalSumArray,
			        labels: gradeArray,
			        marker: {
			            colors: [
			            	'#f2476a',
 			            	'#add8e6',
 			            	'#90ee90',
 			            	'#eb2d3a',
 			            	'#fb654e',
 			            	'#FFCC99',
 				              '#99FFCC',
 				              '#CCCC99',
 				              '#CCCCCC',
 				              '#CCCCFF',
 				              '#CCFF99',
 				              '#CCFFCC',
 				              '#CCFFFF',
 				              '#FFCCCC',
 				              '#FFFF99',
 				              '#FFCCFF',
 				              '#FFFFCC',
			            ]},
			      		        type: 'pie'
			    }];
	
			  
				    		  var layoutQuality = {
							    		height: 400,
							    		  
							    		  margin: {
							    			    l: 10,
							    			    r: 55,
							    			    b:50,
							    			    t: 70,
							    			    pad: 10
							    			  },
							    		  showlegend: true,
							    		  legend: {"orientation": "h"},
				        title: '<b>Quality Vs Marketing Balance</b>',
				         "titlefont": {
						    "size": 14
						  },
					  textposition: 'center',
				    }
			    
			    var layoutFinish = {
				    				  height: 400,
						    		  
						    		  margin: {
						    			    l: 10,
						    			    r: 55,
						    			    b:50,
						    			    t: 70,
						    			    pad: 10
						    			  },
						    		  showlegend: true,
						    		  legend: {"orientation": "h"},
			        title: '<b>Finish Vs Marketing Balance</b>',
			        "titlefont": {
					    "size": 14
					  },
			    }
			    
			    var layoutProduct = {
				    				  height: 400,
						    		  
						    		  margin: {
						    			    l: 10,
						    			    r: 55,
						    			    b:50,
						    			    t: 70,
						    			    pad: 10
						    			  },
						    		  showlegend: true,
						    		  legend: {"orientation": "h"},
			        title: '<b>Product Type Vs Marketing Balance</b>',
			        "titlefont": {
					    "size": 14
					  }
			    }
			    
			    Plotly.newPlot('containerProduct', data, layoutProduct, {
			        showSendToCloud: true,
			        displayModeBar: false
			    });
			    
			    Plotly.newPlot('containerFinish', data, layoutFinish, {
			        showSendToCloud: true ,
			        displayModeBar: false
			    });
			    
			    Plotly.newPlot('containerQuality', data, layoutQuality, {
			        showSendToCloud: true ,
			        displayModeBar: false
			    });
		    }else{
		    	$("#productTypeVsMktBal").html("<div class='reportTitle'>Product Type Vs Marketing Balance</div>");
				$("#qualityVsMktBal").html("<div class='reportTitle'>Quality Vs Marketing Balance</div>");
				$("#finishVsMktBal").html("<div class='reportTitle'>Finish Vs Marketing Balance</div>");
				$("#containerProduct").html('<div class="noGraphData"><marquee class="mrq_data">No data available for graphical representation</marquee></div>');
				$("#containerQuality").html('<div class="noGraphData"><marquee class="mrq_data">No data available for graphical representation</marquee></div>');
				$("#containerFinish").html('<div class="noGraphData"><marquee class="mrq_data">No data available for graphical representation</marquee></div>');
		    }
		    

		}
		
		function resetProductFilter(){
			document.getElementById("productSel").value = "";
			filterOnProductType();
			
		}
		function resetFinishFilter(){
			 document.getElementById("finishSel").value = "";
			filterOnFinish();
		}

		function resetQualityFilter(){
			document.getElementById("qualitySel").value = "";
			filterOnQuality();
		}

		/* anychart.onDocumentReady(function() {

		  // set the data
		  var data = [
		      {x: "304", value: 223553265},
		      {x: "316", value: 38929319},
		      {x: "321", value: 2932248},
		      {x: "JT", value: 14674252},
		      {x: "441", value: 540013},
		      {x: "304L", value: 19107368},
		      {x: "316L", value: 9009073}
		  ];

		  // create the chart
		  var chart = anychart.pie();

		  // set the chart title
		  chart.title("POP Account Report");

		  // add the data
		  chart.data(data);

		  // display the chart in the container
		  chart.container('container');

		  chart.draw();


		}); */
