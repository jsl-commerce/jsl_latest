var contextPath = $("#contextPath").val();
var seriesFlag = false;
var gradeFlag = false;
var selectedSeriesValue, selectedSeriesValuetemp;
var winCount = 0,
    bidsCount = 0;

function callCompletedAuction() {
	window.location.href=ACC.config.encodedContextPath +"/auction/completedAuction";
}

function toggleFilterDiv() {
	document.getElementById("calendar").style.display = "";
}

function toggleFilterDivForOnDate() {
	$("#onDateCalendar").show();
	$("#calendar").hide();
}

function toggleFilterDivForDateRange() {
	$("#onDateCalendar").hide();
	$("#calendar").show();
}

function downloadBidListForAdminByDateRange() {
	var sdate = new Date(document.getElementById("aStartDate").value);
    var edate = new Date(document.getElementById("endDate").value);
    sday = sdate.getDate();
    smonth = sdate.getMonth() + 1;
    syear = sdate.getFullYear();
    eday = edate.getDate();
    emonth = edate.getMonth() + 1;
    eyear = edate.getFullYear();
    
    var type = $("#excelTypeByDateRange").val();
    var auctionStartDate = syear+ "-" + smonth + "-" + sday;
	var auctionEndDate = eyear+ "-" + emonth + "-" + eday;
    window.location.href = window.location.origin+contextPath+"/custom/downloadExcelForAdmin/"+type+"?dateRange=true&auctionStartDate="+auctionStartDate+"&auctionEndDate="+auctionEndDate;
}

function downloadBidListForAdminByDate() {
	var sdate = new Date(document.getElementById("aDate").value);
	sday = sdate.getDate();
    smonth = sdate.getMonth() + 1;
    syear = sdate.getFullYear();
    
    var type = $("#excelTypeByDate").val();
    var auctionStartDate = syear+ "-" + smonth + "-" + sday;
    window.location.href = window.location.origin+contextPath+"/custom/downloadExcelForAdmin/"+type+"?dateRange=false&auctionStartDate="+auctionStartDate;
}

function selectSeriesFunction() {
    var checkBox = document.getElementById("serFlter");
    //var text = document.getElementById("seriesFilter");
    var selectedSeries = document.getElementById("seriesFilter");
    selectedSeriesValue = selectedSeries.options[selectedSeries.selectedIndex].text;
    // alert(selectedSeriesValue);
    if (checkBox.checked == true) {
        selectedSeries.style.display = "block";
        seriesFlag = true;
    } else {
        selectedSeries.text = "All";
        selectedSeries.style.display = "none";
        seriesFlag = false;
    }

    if (document.getElementById("serFlter").checked === false) {
        document.getElementById("all").selected = true;
    }
    if (document.getElementById("serFlter").checked === false && document.getElementById("grdFlter").checked === true) {
        document.getElementById("gradesListA").style.display = "none";
        document.getElementById("gradesListB").style.display = "none";
        document.getElementById("gradesListC").style.display = "none";
        document.getElementById("gradesListD").style.display = "none";
        document.getElementById("gradesListAll").style.display = "block";
    }
}

function displayGrades() {
    var selectedSeries = document.getElementById("seriesFilter");
    selectedSeriesValue = selectedSeries.options[selectedSeries.selectedIndex].text;
    // var checkBox = document.getElementById("grdFlter");
    selectGradeFunction();
}
var seriesListSelected;
var seriesIndexes = [],
    productDetailSeriesIndex = [],
    gradeIndexes = [];
var selectedGradeList = [];
var gradeIndexData = [];
var gradeListSelected = [];
$('section form div input ').change(function() {
    console.log($(this).val());
    gradeListSelected.length = 0;
    gradeIndexes.length = 0;
    $.each($("input:checked"), function(i) {
        if ($(this).val() !== "on" && gradeListSelected.includes($(this).val()) === false) {
            gradeListSelected.unshift($(this).val());
        }
    });
    console.log(gradeListSelected);
    var z = 0;

    for (var i = 0; i < lotDetails.bidEntries.length; i++) {
        gradeIndexData[i] = [];
        for (var j = 0; j < lotDetails.bidEntries[i].prodDetails.length; j++) {
            gradeIndexData[i][j] = [];
        }
    }
    for (var i = 0; i < lotDetails.bidEntries.length; i++) {
        for (var j = 0; j < gradeListSelected.length; j++) {
            for (var k = 0; k < lotDetails.bidEntries[i].prodDetails.length; k++) {
                if ((lotDetails.bidEntries[i].prodDetails[k].Grade).toLowerCase().trim().replace(" ", "").match(gradeListSelected[j].toLowerCase().trim().replace(" ", "")) || (gradeListSelected[j]).toLowerCase().trim().replace(" ", "").match((lotDetails.bidEntries[i].prodDetails[k].Grade).toLowerCase().trim().replace(" ", ""))) {
                    gradeIndexes.push(i);
                    gradeIndexData[i][k].push(gradeListSelected[j]);
                }
            }
        }

    }

    for (var i = 0; i < lotDetails.bidEntries.length; i++) {
        var tr = document.getElementById("row_" + i);
        for (var j = 0; j < gradeIndexData.length; j++) {
            var flag = false;
            for (var k = 0; k < gradeIndexData[j].length; k++) {
                if (gradeIndexData[j][k].length !== 0) {
                    if (selectedGradeList.includes(j) === false) {
                        selectedGradeList.push(j);
                    } else {
                        selectedGradeList.splice(j, 1)
                    }

                }
            }
        }

    }
});

$('form select').change(function() {
    console.log($(this).val());
    myFunction($(this));
    gradeIndexData.length = 0;
    gradeListSelected.length = 0;
    seriesIndexes = [];
    $.each($("div input:checked"), function(i) {
       document.getElementById('gradeBoxALL_' + i).checked = false;
    });
    $.each($("div input:checked"), function(i) {
        document.getElementById('gradeBoxA_' + i).checked = false;
    });
    $.each($("div input:checked"), function(i) {
        document.getElementById('gradeBoxB_' + i).checked = false;
    });
    $.each($("div input:checked"), function(i) {
        document.getElementById('gradeBoxC_' + i).checked = false;
    });
    $.each($("div input:checked"), function(i) {
        document.getElementById('gradeBoxD_' + i).checked = false;
    });

    var seriesListSelected = $(this).val();
    for (var i = 0; i < lotDetails.bidEntries.length; i++) {
        for (var j = 0; j < lotDetails.bidEntries[i].prodDetails.length; j++) {
            if (lotDetails.bidEntries[i].prodDetails[j].Series.toLowerCase().replace(" ", "") == seriesListSelected.toLowerCase().replace(" ", "")) {
                if (seriesIndexes.includes(i) === false) {
                    seriesIndexes.push(i);
                }
            } else {
                seriesIndexes.splice(i, 1);
            }
        }
    }

    if (document.getElementById("seriesFilter").checked === false || document.getElementById("all").selected === true) {
        for (var i = 0; i < lotDetails.bidEntries.length; i++) {
            document.getElementById("row_" + i).style.display = "";
            document.getElementById("accordion_" + i).style.display = "none";
        }
    }
});

// function exportTableToExcel(tableID, filename) {
//     var downloadLink;
//     var dataType = 'application/vnd.ms-excel';
//     var tableSelect = document.getElementById(tableID);
//     var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

//     // Specify file name
//     filename = filename ? filename + '.xls' : 'excel_data.xls';

//     // Create download link element
//     downloadLink = document.createElement("a");

//     document.body.appendChild(downloadLink);

//     if (navigator.msSaveOrOpenBlob) {
//         var blob = new Blob(['\ufeff', tableHTML], {
//             type: dataType
//         });
//         navigator.msSaveOrOpenBlob(blob, filename);
//     } else {
//         // Create a link to the file
//         downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

//         // Setting the file name
//         downloadLink.download = filename;

//         //triggering the function
//         downloadLink.click();
//     }
// }

function exportTableToExcel(tableId)
{
    var tab_text="<table border='2px'><tr bgcolor='#87AFC6'>";
    var textRange; var j=0;
    var tab = document.getElementById(tableId); // id of table

    for(j = 0 ; j < tab.rows.length ; j++) 
    {     
        tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
        //tab_text=tab_text+"</tr>";
    }

    tab_text=tab_text+"</table>";
    tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
    tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
    tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE "); 

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
    {
        txtArea1.document.open("txt/html","replace");
        txtArea1.document.write(tab_text);
        txtArea1.document.close();
        txtArea1.focus(); 
        var sa=txtArea1.document.execCommand("SaveAs",true,"Auction Data.xls");
    }  
    else                 //other browser not tested on IE 11
        sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));  

    return (sa);
}

function myFunction($elem) {
    var lastID;
    lastID = $elem.attr('id');
    $.data('lastID', lastID);
}

function filterTable() {
    event.preventDefault();

    if (document.getElementById("serFlter").checked === true) {
        for (var i = 0; i < lotDetails.bidEntries.length; i++) {
            if (seriesIndexes.includes(i)) {
                document.getElementById("row_" + i).style.display = "";
                document.getElementById("accordion_" + i).style.display = "none";

            } else {
                document.getElementById("row_" + i).style.display = "none";
                document.getElementById("accordion_" + i).style.display = "none";
            }
        }

        if (document.getElementById("all").selected === true) {
            for (var i = 0; i < lotDetails.bidEntries.length; i++) {
                document.getElementById("row_" + i).style.display = "";
                document.getElementById("accordion_" + i).style.display = "none";

            }
        }
    }

    if (document.getElementById("grdFlter").checked == true) {
        for (var i = 0; i < lotDetails.bidEntries.length; i++) {
            if (gradeIndexes.includes(i)) {
                document.getElementById("row_" + i).style.display = "";
                document.getElementById("accordion_" + i).style.display = "none";
            } else {
                document.getElementById("row_" + i).style.display = "none";
                document.getElementById("accordion_" + i).style.display = "none";
            }
        }
        if (document.querySelectorAll('input.gradeBox:checked').length === 0) {
            for (var i = 0; i < lotDetails.bidEntries.length; i++) {
                document.getElementById("row_" + i).style.display = "";
                document.getElementById("accordion_" + i).style.display = "none";
            }
        }
    }
    if (document.getElementById("serFlter").checked === true && document.getElementById("grdFlter").checked === true) {
        if (document.querySelectorAll('input.gradeBox:checked').length === 0) {
            for (var i = 0; i < lotDetails.bidEntries.length; i++) {
                if (seriesIndexes.includes(i)) {
                    document.getElementById("row_" + i).style.display = "";
                    document.getElementById("accordion_" + i).style.display = "none";
                } else {
                    document.getElementById("row_" + i).style.display = "none";
                    document.getElementById("accordion_" + i).style.display = "none";
                }
            }
        }
    }
    
     if (document.getElementById("grdFlter").checked == false  && document.getElementById("serFlter").checked == false) {
    	var snackbar = document.getElementById("snackBar");
    	snackbar.innerHTML = "Please select at least one Series or Grade Group";
        snackbar.style.backgroundColor = "red";
        snackbar.className = "show";
        setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
    }
    
    else{
    	var snackbar = document.getElementById("snackBar");
    	snackbar.innerHTML = "Auctions Filtered";
        snackbar.style.backgroundColor = "green";
        snackbar.className = "show";
        setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
    }
  
    if (document.getElementById("grdFlter").checked == true) {
    	if (document.querySelectorAll('input.gradeBox:checked').length === 0) {
        	var snackbar = document.getElementById("snackBar");
            snackbar.innerHTML = "Please Select atleast one Grade Group";
            snackbar.style.backgroundColor = "red";
            snackbar.className = "show";
            setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
            for (var i = 0; i < lotDetails.bidEntries.length; i++) {
            if (seriesIndexes.includes(i)) {
                document.getElementById("row_" + i).style.display = "";
                document.getElementById("accordion_" + i).style.display = "none";

            } else {
                document.getElementById("row_" + i).style.display = "none";
                document.getElementById("accordion_" + i).style.display = "none";
            }
        }
        }
    }
    
    if (document.getElementById("grdFlter").checked == true && document.getElementById("serFlter").checked === false) {
    	if (document.querySelectorAll('input.gradeBox:checked').length === 0) {
    		for (var i = 0; i < lotDetails.bidEntries.length; i++) {
                document.getElementById("row_" + i).style.display = "";
                document.getElementById("accordion_" + i).style.display = "none";
            }
    	}
    	}
}


function resetTable() {
	event.preventDefault();
	document.getElementById("all").selected = true;
	selectedSeriesValue = 'All';
    $.each($("div input"), function(i) {
        if(document.getElementById('gradeBoxA_' + i) !== null){
        	document.getElementById('gradeBoxA_' + i).checked = false;
    	}
    });
    $.each($("div input"), function(i) {
        if(document.getElementById('gradeBoxB_' + i) !== null){
        	document.getElementById('gradeBoxB_' + i).checked = false;
    	}
    });
    $.each($("div input"), function(i) {
        if(document.getElementById('gradeBoxC_' + i) !== null){
        	document.getElementById('gradeBoxC_' + i).checked = false;
    	}
    });
    $.each($("div input"), function(i) {
    	if(document.getElementById('gradeBoxD_' + i) !== null){
    		document.getElementById('gradeBoxD_' + i).checked = false;
    	}
    });
    $.each($("div input"), function(i) {
    	        if(document.getElementById('gradeBoxALL_' + i) !== null){
    	        	document.getElementById('gradeBoxALL_' + i).checked = false;
    	    	}
    	    });
   
   document.getElementById("seriesFilter").style.display="none";
    document.getElementById('serFlter').checked = false;
	document.getElementById('grdFlter').checked = false;
	document.getElementById("gradesListA").style.display = "none";
    document.getElementById("gradesListB").style.display = "none";
    document.getElementById("gradesListC").style.display = "none";
    document.getElementById("gradesListD").style.display = "none";
    document.getElementById("gradesListAll").style.display = "none";
        for (var i = 0; i < lotDetails.bidEntries.length; i++) {
            document.getElementById("row_" + i).style.display = "";
            document.getElementById("accordion_" + i).style.display="none";
        }
    
        var snackbar = document.getElementById("snackBar");
        snackbar.innerHTML = "Auctions Reset";
        snackbar.style.backgroundColor = "green";
        snackbar.className = "show";
        setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
}


function selectGradeFunction() {
    var checkBox = document.getElementById("grdFlter");
    // var selectedGrades = document.getElementById("gradesList");
    //selectedGradeValue = selectedGrades.options[selectedGrades.selectedIndex].text;
    //alert(selectedGradeValue); 400 Series
    if (document.getElementById("grdFlter").checked === false) {
        
        $.each($("div input:checked"), function(i) {
        if((document.getElementById('gradeBoxA_' + i) !== undefined || document.getElementById('gradeBoxA_' + i) !== null)&& (this).id !== "all"){
            document.getElementById('gradeBoxA_' + i).checked = false;
            }
        });
        $.each($("div input:checked"), function(i) {
        if((document.getElementById('gradeBoxB_' + i) !== undefined || document.getElementById('gradeBoxB_' + i) !== null)&& (this).id !== "all"){
            document.getElementById('gradeBoxB_' + i).checked = false;
            }
        });
        $.each($("div input:checked"), function(i) {
        if((document.getElementById('gradeBoxC_' + i) !== undefined || document.getElementById('gradeBoxC_' + i) !== null)&& (this).id !== "all"){
            document.getElementById('gradeBoxC_' + i).checked = false;
            }
        });
        $.each($("div input:checked"), function(i) {
        if((document.getElementById('gradeBoxD_' + i) !== undefined || document.getElementById('gradeBoxD_' + i) !== null)&& (this).id !== "all"){
            document.getElementById('gradeBoxD_' + i).checked = false;
            }
        });
        $.each($("div input:checked"), function(i) {
        if((document.getElementById('gradeBoxALL_' + i) !== undefined || document.getElementById('gradeBoxALL_' + i) !== null)&& (this).id !== "all"){
            document.getElementById('gradeBoxALL_' + i).checked = false;
            }
        });
        for (var i = 0; i < lotDetails.bidEntries.length; i++) {
            document.getElementById("row_" + i).style.display = "";

        }
    }
    if (seriesFlag === true) {
        console.log(selectedSeriesValue);
        if (selectedSeriesValue === '200 Series') {
            if (checkBox.checked == true) {
                document.getElementById("gradesListA").style.display = "block";
                document.getElementById("gradesListB").style.display = "none";
                document.getElementById("gradesListC").style.display = "none";
                document.getElementById("gradesListD").style.display = "none";
                document.getElementById("gradesListAll").style.display = "none";
                gradeFlag = true;
            } else {
                document.getElementById("gradesListA").style.display = "none";
                gradeFlag = false;
            }
        }
        if (selectedSeriesValue === '300 Series') {
            if (checkBox.checked == true) {
                document.getElementById("gradesListB").style.display = "block";
                document.getElementById("gradesListA").style.display = "none";
                document.getElementById("gradesListC").style.display = "none";
                document.getElementById("gradesListD").style.display = "none";
                document.getElementById("gradesListAll").style.display = "none";
                gradeFlag = true;
            } else {
                document.getElementById("gradesListB").style.display = "none";
                gradeFlag = false;
            }
        }
        if (selectedSeriesValue === '400 Series') {
            if (checkBox.checked == true) {
                document.getElementById("gradesListC").style.display = "block";
                document.getElementById("gradesListA").style.display = "none";
                document.getElementById("gradesListB").style.display = "none";
                document.getElementById("gradesListD").style.display = "none";
                document.getElementById("gradesListAll").style.display = "none";
                gradeFlag = true;
            } else {
                document.getElementById("gradesListC").style.display = "none";
                gradeFlag = false;
            }
        }
        if (selectedSeriesValue === 'Duplex') {
            if (checkBox.checked == true) {
                document.getElementById("gradesListD").style.display = "block";
                document.getElementById("gradesListA").style.display = "none";
                document.getElementById("gradesListB").style.display = "none";
                document.getElementById("gradesListC").style.display = "none";
                document.getElementById("gradesListAll").style.display = "none";
                gradeFlag = true;
            } else {
                document.getElementById("gradesListD").style.display = "none";
                gradeFlag = false;
            }
        }
        if (selectedSeriesValue === 'All') {
            if (checkBox.checked == true) {
                document.getElementById("gradesListAll").style.display = "block";
                document.getElementById("gradesListA").style.display = "none";
                document.getElementById("gradesListB").style.display = "none";
                document.getElementById("gradesListC").style.display = "none";
                document.getElementById("gradesListD").style.display = "none";
                gradeFlag = true;
            } else {
                document.getElementById("gradesListAll").style.display = "none";
                gradeFlag = false;
            }
        }

    }
    if (seriesFlag !== true) {
        if (checkBox.checked == true) {
            document.getElementById("gradesListAll").style.display = "block";
            gradeFlag = true;
        } else {
            document.getElementById("gradesListAll").style.display = "none";
            gradeFlag = false;
        }

    }
}

function filterTableData() {
    event.preventDefault();
    if (seriesFlag == true && gradeFlag == false) {
        console.log('only series selected');
        console.log(selectedSeriesValue);
    }
    if (seriesFlag == false && gradeFlag == true) {
        console.log('only grades selected');
    }
    if (seriesFlag == true && gradeFlag == true) {
        console.log('Both series and greades selected');
    }
}

function displayGradeGrpData(selectedSeriesValue) {
    //alert(selectedSeriesValue);
}

var sday, smonth, syear, eday, emonth, eyear;
var jslDates = [];
// $(document).ready(function() {
//     document.getElementById("startDate").valueAsDate = new Date();
//     document.getElementById("endDate").valueAsDate = new Date();
// });
$('#getAuctions').on('click', function() {
    var currentDate = new Date();
    var sdate = new Date($('#aStartDate').val());
    var edate = new Date($('#endDate').val());
    $("#CSRFToken").val(ACC.config.CSRFToken);
    sday = sdate.getDate();
    smonth = sdate.getMonth() + 1;
    syear = sdate.getFullYear();
    eday = edate.getDate();
    emonth = edate.getMonth() + 1;
    eyear = edate.getFullYear();
    var timeDiff = Math.abs(currentDate.getTime() - sdate.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    if (sdate == "Invalid Date") {
        var snackbar = document.getElementById("snackBar");
        snackbar.innerHTML = "Please enter the correct date.";
        snackbar.style.backgroundColor = "darkred";
        snackbar.className = "show";
        setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
    } else if (diffDays > 15) {
        $('#startDate').val('');
        sday = null;
        smonth = null;
        syear = null;
        var snackbar = document.getElementById("snackBar");
        snackbar.innerHTML = "Date can not be more than last 15 days";
        snackbar.style.backgroundColor = "darkred";
        snackbar.className = "show";
        setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
    } else if (diffDays <= 15) {
    	var completedLotsDates = {
                "auctionStartDate": sday + "/" + smonth + "/" + syear,
                "auctionEndDate": eday + "/" + emonth + "/" + eyear
            };
    	
    	$("#auctionStartDate").val(syear+ "-" + smonth + "-" + sday);
        $("#auctionEndDate").val(eyear+ "-" + emonth + "-" + eday);
        
    	jslDates.push(completedLotsDates);
        var lotDateDetails = {
            dates: jslDates
        };

        jslDates = [];

        $("#auctionDateRange").submit();
    }

});

function Lot(auctionDate, auctionId, lotNo, qty, startBidPrice, bidIncrement, h1Price) {
    this.auctionDate = auctionDate;
    this.auctionId = auctionId;
    this.lotNo = lotNo;
    this.qty = qty;
    this.startBidPrice = startBidPrice;
    this.bidIncrement = bidIncrement;
    this.h1Price = h1Price;

    this.createTable = function() {
        console.log("success");
    };

}
var table = document.getElementById("compAuctionTable");

function getLotData() {
    var lotJson = [];
    var prodJson = [];
    var mainTable = $(document).find("#completeTable");
    var $headers = mainTable.find("th");
    var lotRows = document.getElementsByClassName("completedRow");
    var prodTables = document.getElementsByClassName("prods");
    for (var i = 0; i < lotRows.length; i++) {
        var $cells = $(lotRows[i]).find("td");
        lotJson[i] = {};
        for (var k = 0; k < $cells.length; k++) {
            lotJson[i][$($headers[k]).html()] = $($cells[k]).text();
        }
        // $cells.each(function(cellIndex){
        //     lotJson[i][$($headers[cellIndex]).html()] = $(this).text(); 
        // });
        var prodDetails = [];
        var $prodRows = $(prodTables[i]).find(".prodRow");
        var $prodHeaders = $(prodTables[i]).find("th");
        for (var j = 0; j < $prodRows.length; j++) {
            var $prodCells = $($prodRows[j]).find("td");
            prodJson[j] = {};
            for (var l = 0; l < $prodCells.length; l++) {
                prodJson[j][$($prodHeaders[l]).html()] = $($prodCells[l]).text();
                prodDetails[j] = prodJson[j];
            }
            // $prodCells.each(function(cellIndex){
            //     prodJson[j][$($prodHeaders[cellIndex]).html()] = $(this).text();
            // });
        }
        lotJson[i].prodDetails = prodDetails;
    }
    var myObj = {};
    myObj.bidEntries = lotJson;
    
    return JSON.parse(JSON.stringify(myObj).replace(/\s/g, ""));
}

var lotDetails;
var lots = [];
$(document).ready(function() {
    $("#CSRFToken").val(ACC.config.CSRFToken);
});

window.onload = function(){
    lotDetails = getLotData();
    if (lotDetails.bidEntries.length >= 1) {
       makeTable(lotDetails);    
    }
    $("#loading-wrapper").hide();
    console.log(lotDetails);
}

function makeTable(lotDetails) {
    var auctionDate, auctionId, lotNo, qty, startBidPrice, bidIncrement, h1Price;

    for (var i = 0; i < lotDetails.bidEntries.length; i++) {
       // var row = table.insertRow(i);
        auctionDate = lotDetails.bidEntries[i].auctionDate;
        auctionId = lotDetails.bidEntries[i].auctionId;
        lotNo = lotDetails.bidEntries[i].lotNo;
        qty = lotDetails.bidEntries[i].qty;
        startBidPrice = lotDetails.bidEntries[i].startBidPrice;
        bidIncrement = lotDetails.bidEntries[i].bidIncrement;
        h1Price = lotDetails.bidEntries[i].h1Price;
        var lot = new Lot(auctionDate, auctionId, lotNo, qty, startBidPrice, bidIncrement, h1Price);
        lots.push(lot);
    }
}

var TableIDvalue = "completeTable";
var TableLastSortedColumn = -1;

function SortTable() {
	var sortColumn = parseInt(arguments[0]);
	var type = arguments.length > 1 ? arguments[1] : 'T';
	var table = document.getElementById(TableIDvalue);
	var tbody = table.getElementsByTagName("tbody")[0];
	var rowRecord = tbody.getElementsByTagName("tr");
	
	var rows = [];
	var accordionRows = [];
	var indexCount = 0;
	var accordionIndexCount = 0;
	for(var i=0, len=rowRecord.length; i<len; i++) {
		if(rowRecord[i].id.includes("row_")) {
			rows[indexCount++] = rowRecord[i];
		} else if(rowRecord[i].id.includes("accordion_")) {
			accordionRows[accordionIndexCount++] = rowRecord[i];
		}
	}
	
	var arrayOfRows = new Array();
	type = type.toUpperCase();
	for(var i=0, len=rows.length; i<len; i++) {
		arrayOfRows[i] = new Object;
		arrayOfRows[i].oldIndex = i;
		var celltext = rows[i].getElementsByTagName("td")[sortColumn].innerHTML.replace(/<[^>]*>/g,"");
		var re = type=="N" ? /[^\.\-\+\d]/g : /[^a-zA-Z0-9]/g;
		arrayOfRows[i].value = celltext.replace(re,"").substr(0,25).toLowerCase();
		}
	if (sortColumn == TableLastSortedColumn) {
		arrayOfRows.reverse(); 
	}else {
		TableLastSortedColumn = sortColumn;
		arrayOfRows.sort(CompareRowOfText);
		}
	var newTableBody = document.createElement("tbody");
	for(var i=0, len=arrayOfRows.length; i<len; i++) {
		newTableBody.appendChild(rows[arrayOfRows[i].oldIndex].cloneNode(true));
		}
	table.replaceChild(newTableBody,tbody);
	
	for(var i=0, len=rows.length; i<len; i++) {
		$("#"+rows[i].id).after(accordionRows[i]);
	}
}

function CompareRowOfText(a,b) {
	var aval = a.value;
	var bval = b.value;
	return( aval == bval ? 0 : (aval > bval ? 1 : -1) );
}

$('#getAuctionsForAdmin').on('click', function() {
    var currentDate = new Date();
    var sdate = new Date($('#aDate').val());
    var edate = new Date($('#endDate').val());
    $("#CSRFToken").val(ACC.config.CSRFToken);
    sday = sdate.getDate();
    smonth = sdate.getMonth() + 1;
    syear = sdate.getFullYear();
    eday = edate.getDate();
    emonth = edate.getMonth() + 1;
    eyear = edate.getFullYear();
    var timeDiff = Math.abs(currentDate.getTime() - sdate.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    if (sdate == "Invalid Date") {
        var snackbar = document.getElementById("snackBar");
        snackbar.innerHTML = "Please enter the correct date.";
        snackbar.style.backgroundColor = "darkred";
        snackbar.className = "show";
        setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
    }  else {
    	var completedLotsDates = {
                "auctionStartDate": sday + "/" + smonth + "/" + syear,
                "auctionEndDate": eday + "/" + emonth + "/" + eyear
            };
    	
    	$("#auctionDate").val(syear+ "-" + smonth + "-" + sday);
        $("#auctionEndDate").val(eyear+ "-" + emonth + "-" + eday);
        
    	jslDates.push(completedLotsDates);
        var lotDateDetails = {
            dates: jslDates
        };

        jslDates = [];

        $("#adminAuctionDateRange").submit();
    }

});
