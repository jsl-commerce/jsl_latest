var additionalTime = 0;
var timerDuration = 1800000;
var inc = 0;
var seriesFlag = false;
var gradeFlag = false;
var selectedSeriesValue, selectedSeriesValuetemp;
var winCount = 0, bidsCount = 0;
var h1Interval;
var auctionStatus;
var serverTime;
var user;
var auctionStartDateTime;
var timerFire;
var yetToStart;

$(document).ready(function() {
	var selectedAuctionID = $("#selectedAuctionID").val();
    if(selectedAuctionID != "" && selectedAuctionID != "All") {
    	$("#acceptedAuctionList").val($("#selectedAuctionID").val());
    }

	    /*code for websocket*/
		//openConnection();
		additionalTimeInterval = window.setInterval(getAdditionalTime,800);
		h1Interval = window.setInterval(getH1Price,1000);
		
	var table = document.getElementById("lotTable");
    var auctionId = getAuctionByUrl();
    
    if(localStorage.getItem("optionsAuction") != null && localStorage.getItem("optionsAuctionUp") != null){
    	var todaysOptions = JSON.parse(localStorage.getItem("optionsAuction"));
    	var upcomingOptions = JSON.parse(localStorage.getItem("optionsAuctionUp"));
    	var auctionSelected = mergeArray(todaysOptions, upcomingOptions);
    }else if(localStorage.getItem("optionsAuction") != null){
    	var auctionSelected = JSON.parse(localStorage.getItem("optionsAuction"));
    }else{
    	var auctionSelected = JSON.parse(localStorage.getItem("optionsAuctionUp"));
    }
    if (auctionSelected.length != 0) {
        var div = document.querySelector("#sel"),
            frag = document.createDocumentFragment(),
            select = document.createElement("select");
        select.onchange = function(){selectAuction();};
        select.id = "acceptedAuctionList";
        for (var i = 0; i < auctionSelected.length; i++) {
            if (auctionId == auctionSelected[i].trim()) {
                select.options.add(new Option(auctionSelected[i], auctionSelected[i], true, true));
            } else {
                select.options.add(new Option(auctionSelected[i], auctionSelected[i]));
            }
        }
        frag.appendChild(select);
        div.appendChild(frag);
        
        user = Number($("#userPK").val());
    }
    var auctionStartTime = $("#auctionStartTime").val();
    var auctionStartDate = $("#auctionStartDate").val();
    auctionStartDateTime = new Date(auctionStartTime+" "+auctionStartDate).getTime();
});

window.onload = function() {
	var table = document.getElementById("lotTable");
	lotDetails = getLotData();
	if (lotDetails.bidEntries.length >= 1) {
	    makeTable(lotDetails);
	}
	document.getElementById("tc").innerHTML = lotDetails.bidEntries.length;
    fireTimer();
    timerFire = window.setInterval(resetTimer, 30500);
}

function selectSeriesFunction() {
    var checkBox = document.getElementById("serFlter");
    var selectedSeries = document.getElementById("seriesFilter");
    selectedSeriesValue = selectedSeries.options[selectedSeries.selectedIndex].text;
    if (checkBox.checked == true) {
        selectedSeries.style.display = "block";
        seriesFlag = true;
    } else {
        selectedSeries.text = "All";
        selectedSeries.style.display = "none";
        seriesFlag = false;
    }

    if (document.getElementById("serFlter").checked === false) {
        document.getElementById("all").selected = true;
    }
    if (document.getElementById("serFlter").checked === false && document.getElementById("grdFlter").checked === true) {
        document.getElementById("gradesListA").style.display = "none";
        document.getElementById("gradesListB").style.display = "none";
        document.getElementById("gradesListC").style.display = "none";
        document.getElementById("gradesListD").style.display = "none";
        document.getElementById("gradesListAll").style.display = "block";
    }
}

function setH1OnLoad(){
    for(var i = 0; i < lots.length; i++){
        var myBid = Number($("#lastBid_" + i).html());
        if((lots[i].h1Price != "" && lots[i].h1Price != 0) && (myBid != 0 && myBid != "") && lots[i].h1Price == myBid){
            lots[i].lastBid = lots[i].h1Price;
        }
    }
}

function displayGrades() {
    var selectedSeries = document.getElementById("seriesFilter");
    selectedSeriesValue = selectedSeries.options[selectedSeries.selectedIndex].text;
    selectGradeFunction();
}
var seriesListSelected;
var seriesIndexes = [],
    productDetailSeriesIndex = [],
    gradeIndexes = [];
var selectedGradeList = [];
var gradeIndexData = [];
var gradeListSelected = [];

$('section form div input ').change(function() {
    console.log($(this).val());
    gradeListSelected.length = 0;
    gradeIndexes.length = 0;
    $.each($("input:checked"), function(i) {
        if ($(this).val() !== "on" && gradeListSelected.includes($(this).val()) === false) {
            gradeListSelected.unshift($(this).val());
        }
    });
    console.log(gradeListSelected);
    var z = 0;

    for (var i = 0; i < lotDetails.bidEntries.length; i++) {
        gradeIndexData[i] = [];
        for (var j = 0; j < lotDetails.bidEntries[i].prodDetails.length; j++) {
            gradeIndexData[i][j] = [];
        }
    }
    for (var i = 0; i < lotDetails.bidEntries.length; i++) {
        for (var j = 0; j < gradeListSelected.length; j++) {
            for (var k = 0; k < lotDetails.bidEntries[i].prodDetails.length; k++) {
                if ((lotDetails.bidEntries[i].prodDetails[k].Grade).toLowerCase().trim().replace(" ", "").match(gradeListSelected[j].toLowerCase().trim().replace(" ", "")) || (gradeListSelected[j]).toLowerCase().trim().replace(" ", "").match((lotDetails.bidEntries[i].prodDetails[k].Grade).toLowerCase().trim().replace(" ", ""))) {
                    gradeIndexes.push(i);
                    gradeIndexData[i][k].push(gradeListSelected[j]);
                }
            }
        }
    }
    for (var i = 0; i < lotDetails.bidEntries.length; i++) {
        var tr = document.getElementById("row_" + i);
        for (var j = 0; j < gradeIndexData.length; j++) {
            var flag = false;
            for (var k = 0; k < gradeIndexData[j].length; k++) {
                if (gradeIndexData[j][k].length !== 0) {
                    if (selectedGradeList.includes(j) === false) {
                        selectedGradeList.push(j);
                    } else {
                        selectedGradeList.splice(j, 1)
                    }

                }
            }
        }
    }
});

$('form select').change(function() {
    console.log($(this).val());
    myFunction($(this));
    gradeIndexData.length = 0;
    gradeListSelected.length = 0;
    seriesIndexes = [];
    for (var i = 0; i < lotDetails.bidEntries.length; i++) {
        document.getElementById("row_" + i).style.display = "";
        document.getElementById("accordion_" + i).style.display = "none";
    }
    $.each($("div input:checked"), function(i) {
        document.getElementById('gradeBoxALL_' + i).checked = false;
    });
    $.each($("div input:checked"), function(i) {
        document.getElementById('gradeBoxA_' + i).checked = false;
    });
    $.each($("div input:checked"), function(i) {
        document.getElementById('gradeBoxB_' + i).checked = false;
    });
    $.each($("div input:checked"), function(i) {
        document.getElementById('gradeBoxC_' + i).checked = false;
    });
    $.each($("div input:checked"), function(i) {
        document.getElementById('gradeBoxD_' + i).checked = false;
    });

    var seriesListSelected = $(this).val();
    for (var i = 0; i < lotDetails.bidEntries.length; i++) {
        for (var j = 0; j < lotDetails.bidEntries[i].prodDetails.length; j++) {
            if (lotDetails.bidEntries[i].prodDetails[j].Series.toLowerCase().replace(" ", "") == seriesListSelected.toLowerCase().replace(" ", "")) {
                if (seriesIndexes.includes(i) === false) {
                    seriesIndexes.push(i);
                }
            } else {
                seriesIndexes.splice(i, 1);
            }
        }
    }

    if (document.getElementById("seriesFilter").checked === false || document.getElementById("all").selected === true) {
        for (var i = 0; i < lotDetails.bidEntries.length; i++) {
            document.getElementById("row_" + i).style.display = "";
            document.getElementById("accordion_" + i).style.display = "none";
        }
    }
});

function myFunction($elem) {
    var lastID;
    lastID = $elem.attr('id');
    $.data('lastID', lastID);
}

function filterTable() {
    event.preventDefault();
    if (document.getElementById("serFlter").checked === true) {
        for (var i = 0; i < lotDetails.bidEntries.length; i++) {
            if (seriesIndexes.includes(i)) {
                document.getElementById("row_" + i).style.display = "";
                document.getElementById("accordion_" + i).style.display = "none";
            } else {
                document.getElementById("row_" + i).style.display = "none";
                document.getElementById("accordion_" + i).style.display = "none";
            }
        }
        if (document.getElementById("all").selected === true) {
            for (var i = 0; i < lotDetails.bidEntries.length; i++) {
                document.getElementById("row_" + i).style.display = "";
                document.getElementById("accordion_" + i).style.display = "none";

            }
        }
    }
    if (document.getElementById("grdFlter").checked == true) {
        for (var i = 0; i < lotDetails.bidEntries.length; i++) {
            if (gradeIndexes.includes(i)) {
                document.getElementById("row_" + i).style.display = "";
                document.getElementById("accordion_" + i).style.display = "none";
            } else {
                document.getElementById("row_" + i).style.display = "none";
                document.getElementById("accordion_" + i).style.display = "none";
            }
        }
    }
    if (document.getElementById("grdFlter").checked == false  && document.getElementById("serFlter").checked == false) {
    	var snackbar = document.getElementById("snackBar");
    	snackbar.innerHTML = "Please select at least one Series or Grade Group";
        snackbar.style.backgroundColor = "red";
        snackbar.className = "show";
        setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
    }
    else{
    	var snackbar = document.getElementById("snackBar");
    	snackbar.innerHTML = "Auctions Filtered";
        snackbar.style.backgroundColor = "green";
        snackbar.className = "show";
        setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
    }
    if (document.getElementById("grdFlter").checked == true) {
    	if (document.querySelectorAll('input.gradeBox:checked').length === 0) {
        	var snackbar = document.getElementById("snackBar");
            snackbar.innerHTML = "Please Select atleast one Grade Group";
            snackbar.style.backgroundColor = "red";
            snackbar.className = "show";
            setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
            for (var i = 0; i < lotDetails.bidEntries.length; i++) {
                if (seriesIndexes.includes(i)) {
                    document.getElementById("row_" + i).style.display = "";
                    document.getElementById("accordion_" + i).style.display = "none";
                } else {
                    document.getElementById("row_" + i).style.display = "none";
                    document.getElementById("accordion_" + i).style.display = "none";
                }
            }
        }
    	
    }
    if (document.getElementById("grdFlter").checked == true && document.getElementById("serFlter").checked === false) {
    	if (document.querySelectorAll('input.gradeBox:checked').length === 0) {
    		for (var i = 0; i < lotDetails.bidEntries.length; i++) {
                document.getElementById("row_" + i).style.display = "";
                document.getElementById("accordion_" + i).style.display = "none";
            }
    	}
    }
}

function resetTable() {
	event.preventDefault();
	document.getElementById("all").selected = true;
	selectedSeriesValue = 'All';
    $.each($("div input"), function(i) {
        if(document.getElementById('gradeBoxA_' + i) !== null){
        	document.getElementById('gradeBoxA_' + i).checked = false;
    	}
    });
    $.each($("div input"), function(i) {
        if(document.getElementById('gradeBoxB_' + i) !== null){
        	document.getElementById('gradeBoxB_' + i).checked = false;
    	}
    });
    $.each($("div input"), function(i) {
        if(document.getElementById('gradeBoxC_' + i) !== null){
        	document.getElementById('gradeBoxC_' + i).checked = false;
    	}
    });
    $.each($("div input"), function(i) {
    	if(document.getElementById('gradeBoxD_' + i) !== null){
    		document.getElementById('gradeBoxD_' + i).checked = false;
    	}
    });
    $.each($("div input"), function(i) {
        if(document.getElementById('gradeBoxALL_' + i) !== null){
        	document.getElementById('gradeBoxALL_' + i).checked = false;
    	}
    });
    document.getElementById("seriesFilter").style.display="none";
    document.getElementById('serFlter').checked = false;
	document.getElementById('grdFlter').checked = false;
	document.getElementById("gradesListA").style.display = "none";
    document.getElementById("gradesListB").style.display = "none";
    document.getElementById("gradesListC").style.display = "none";
    document.getElementById("gradesListD").style.display = "none";
    document.getElementById("gradesListAll").style.display = "none";
        for (var i = 0; i < lotDetails.bidEntries.length; i++) {
            document.getElementById("row_" + i).style.display = "";
            document.getElementById("accordion_" + i).style.display = "none";
        }
        var snackbar = document.getElementById("snackBar");
        snackbar.innerHTML = "Auctions Reset";
        snackbar.style.backgroundColor = "green";
        snackbar.className = "show";
        setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
}

function selectGradeFunction() {
    var checkBox = document.getElementById("grdFlter");
    if (document.getElementById("grdFlter").checked === false) {
    	 $.each($("div input:checked"), function(i) {
    	        if(document.getElementById('gradeBoxALL_' + i) !== null){
    	        	document.getElementById('gradeBoxALL_' + i).checked = false;
    	    	}
    	    });
    	    $.each($("div input:checked"), function(i) {
    	        if(document.getElementById('gradeBoxA_' + i) !== null){
    	        	document.getElementById('gradeBoxA_' + i).checked = false;
    	    	}
    	    });
    	    $.each($("div input:checked"), function(i) {
    	        if(document.getElementById('gradeBoxB_' + i) !== null){
    	        	document.getElementById('gradeBoxB_' + i).checked = false;
    	    	}
    	    });
    	    $.each($("div input:checked"), function(i) {
    	        if(document.getElementById('gradeBoxC_' + i) !== null){
    	        	document.getElementById('gradeBoxC_' + i).checked = false;
    	    	}
    	    });
    	    $.each($("div input:checked"), function(i) {
    	    	if(document.getElementById('gradeBoxD_' + i) !== null){
    	    		document.getElementById('gradeBoxD_' + i).checked = false;
    	    	}
    	    });
        for (var i = 0; i < lotDetails.bidEntries.length; i++) {
            document.getElementById("row_" + i).style.display = "";
        }
    }
    if (seriesFlag === true) {
        console.log(selectedSeriesValue);
        if (selectedSeriesValue === '200 Series') {
            if (checkBox.checked == true) {
                document.getElementById("gradesListA").style.display = "block";
                document.getElementById("gradesListB").style.display = "none";
                document.getElementById("gradesListC").style.display = "none";
                document.getElementById("gradesListD").style.display = "none";
                document.getElementById("gradesListAll").style.display = "none";
                gradeFlag = true;
            } else {
                document.getElementById("gradesListA").style.display = "none";
                gradeFlag = false;
            }
        }
        if (selectedSeriesValue === '300 Series') {
            if (checkBox.checked == true) {
                document.getElementById("gradesListB").style.display = "block";
                document.getElementById("gradesListA").style.display = "none";
                document.getElementById("gradesListC").style.display = "none";
                document.getElementById("gradesListD").style.display = "none";
                document.getElementById("gradesListAll").style.display = "none";
                gradeFlag = true;
            } else {
                document.getElementById("gradesListB").style.display = "none";
                gradeFlag = false;
            }
        }
        if (selectedSeriesValue === '400 Series') {
            if (checkBox.checked == true) {
                document.getElementById("gradesListC").style.display = "block";
                document.getElementById("gradesListA").style.display = "none";
                document.getElementById("gradesListB").style.display = "none";
                document.getElementById("gradesListD").style.display = "none";
                document.getElementById("gradesListAll").style.display = "none";
                gradeFlag = true;
            } else {
                document.getElementById("gradesListC").style.display = "none";
                gradeFlag = false;
            }
        }
        if (selectedSeriesValue === 'Duplex') {
            if (checkBox.checked == true) {
                document.getElementById("gradesListD").style.display = "block";
                document.getElementById("gradesListA").style.display = "none";
                document.getElementById("gradesListB").style.display = "none";
                document.getElementById("gradesListC").style.display = "none";
                document.getElementById("gradesListAll").style.display = "none";
                gradeFlag = true;
            } else {
                document.getElementById("gradesListD").style.display = "none";
                gradeFlag = false;
            }
        }
        if (selectedSeriesValue === 'All') {
            if (checkBox.checked == true) {
                document.getElementById("gradesListAll").style.display = "block";
                document.getElementById("gradesListA").style.display = "none";
                document.getElementById("gradesListB").style.display = "none";
                document.getElementById("gradesListC").style.display = "none";
                document.getElementById("gradesListD").style.display = "none";
                gradeFlag = true;
            } else {
                document.getElementById("gradesListAll").style.display = "none";
                gradeFlag = false;
            }
        }

    }
    if (seriesFlag !== true) {
        if (checkBox.checked == true) {
            document.getElementById("gradesListAll").style.display = "block";
            gradeFlag = true;
        } else {
            document.getElementById("gradesListAll").style.display = "none";
            gradeFlag = false;
        }
    }
}

function displayGradeGrpData(selectedSeriesValue) {
}

function Lot(matLoc, qty, startBidPrice, h1Price, myBid, proxyAmt, products, index) {
    this.auctionId = $("#selectedAuctionID").val().trim();
    this.lotNo = $("#bundleId_"+index).val();
    this.matLoc = matLoc;
//    this.startTime = startTime;
    this.qty = qty;
//    this.unit = "Rs/MT";
    this.startBidPrice = startBidPrice;
    this.h1Price = h1Price.replace(/[^0-9 ]/g, "");
    this.status = "";
    this.myBid = myBid;
    this.select = "";
    this.proxyAmt = proxyAmt;
    this.lastBid = 0;
    this.previousH1 = h1Price.replace(/[^0-9 ]/g, "");
    this.timer;
    this.timeValue;
//    this.incCount = 0;
    this.prodDetails = products;
    this.h1Bidder;
    this.inc;
    this.check;
}

function makeTable(lotDetails) {
    var matLoc, qty, startBidPrice, h1Price, myBid, proxyAmt;
    for (var i = 0; i < lotDetails.bidEntries.length; i++) {
        matLoc = lotDetails.bidEntries[i].MaterialLocation;
        qty = lotDetails.bidEntries[i].Qty;
        startBidPrice = lotDetails.bidEntries[i].StartBidPrice.replace(/\D/g, '');
        h1Price = lotDetails.bidEntries[i].H1Price.replace(/\D/g, '');
        if(lotDetails.bidEntries[i].MyBid != undefined){
        	myBid = lotDetails.bidEntries[i].MyBid.replace(/\D/g, '');
        }else{
        	myBid = 0;
        }
        if(lotDetails.bidEntries[i].ProxyAmt != undefined){
        	proxyAmt = lotDetails.bidEntries[i].ProxyAmt.replace(/\D/g, '').replace(/[^0-9 ]/g, "");
        }else {
        	proxyAmt = 0;
        }
        var productsData = {};
        var prodDataArray = [];
        for (var j = 0; j < lotDetails.bidEntries[i].prodDetails.length; j++) {
            productsData['quantity'] = lotDetails.bidEntries[i].prodDetails[j].Quantity;
            productsData['quality'] = lotDetails.bidEntries[i].prodDetails[j].Quality;
            productsData['grade'] = lotDetails.bidEntries[i].prodDetails[j].Grade;
            productsData['series'] = lotDetails.bidEntries[i].prodDetails[j].Series;
            productsData['width'] = lotDetails.bidEntries[i].prodDetails[j].Width;
            productsData['thickness'] = lotDetails.bidEntries[i].prodDetails[j].Thickness;
            productsData['length'] = lotDetails.bidEntries[i].prodDetails[j].Length;
            productsData['finish'] = lotDetails.bidEntries[i].prodDetails[j].Finish;
            productsData['edge'] = lotDetails.bidEntries[i].prodDetails[j].Edge;
            productsData['pvc'] = lotDetails.bidEntries[i].prodDetails[j].PVC;
            prodDataArray.push(productsData);
        }
        var lot = new Lot(matLoc, qty, startBidPrice, h1Price, myBid, proxyAmt, prodDataArray, i);
        lots.push(lot);
    }
    return true;
}

var table = document.getElementById("lotTable");
var selectedLots = [];

function selectCheckBox(a) {
    var check = document.getElementById("sel_" + a);
    if ($(check).is(':checked')) {
        lots[a].select = true;
        document.getElementById("incbtn_" + a).title = "";
        document.getElementById("decbtn_" + a).title = "";
    } else {
        lots[a].select = false;
    }
}

function checks(i){
	lots[i].check = setInterval(function() {
		if((auctionStatus === undefined) || (auctionStatus != undefined && auctionStatus != "CLOSED" && auctionStatus != "COMPLETED")){
	    		if(!yetToStart){
				proxyChecker(i);
		        if (lots[i].h1Bidder != undefined && lots[i].h1Bidder == user) {
		            lots[i].hammer = true;
		            document.getElementById("hmr_" + i).innerHTML = "<i class = \"fa fa-legal\">" + "</i>";
		            lots[i].status = "Bid Accepted!";
		            if(document.getElementById("status_" + i) != undefined){
		            	document.getElementById("status_" + i).innerHTML = "<b>Bid Accepted!</b>";
		            }
		            $("#row_" + i).addClass("bidPlaced");
		        } else{
		            lots[i].hammer = false;
		            document.getElementById("hmr_" + i).innerHTML = "";
		            lots[i].status = "";
		            if(document.getElementById("status_" + i) != undefined){
		            	document.getElementById("status_" + i).innerHTML = "<b>Bid In Progress</b>";
		            }
		            $("#row_" + i).removeClass("bidPlaced");
		        }
	    		}
	    	}else if(auctionStatus == "CLOSED"){
	    		document.getElementById("timer_" + i).innerHTML = "<b>00:00:00</b>";
	    		if(lots[i].h1Bidder != undefined && lots[i].h1Bidder == user){
	    			lots[i].status="Bid Won";
	    			document.getElementById("status_" + i).innerHTML = "<b>Bid Won</b>";
	    			lots[i].hammer = true;
	    			greyOut(i, "late");
		            document.getElementById("hmr_" + i).innerHTML = "<i class = \"fa fa-legal\">" + "</i>";
	    		}else{
	    			lots[i].status = "Auction Closed";
	                document.getElementById("status_" + i).innerHTML = "<b>Auction Closed</b>";
	                greyOut(i, "late");
	                displayHide(i);
	    		}
	    		$("#loading-wrapper").hide();
		}
	}, 1000);
}

function calculateTime(time, i, strtTime, duration) {
	var timer = true;
	clearInterval(lots[i].timer);
    lots[i].timer = setInterval(function() {
    	if((auctionStatus === undefined) || (auctionStatus != undefined && auctionStatus != "CLOSED" && auctionStatus != "COMPLETED")){
    		lots[i].timeValue = time;
    		var hours = Math.floor((time % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
	        hours < 10 ? hours = "0" + hours : hours;
	        var minutes = Math.floor((time % (1000 * 60 * 60)) / (1000 * 60));
	        minutes < 10 ? minutes = "0" + minutes : minutes;
	        var seconds = Math.floor((time % (1000 * 60)) / 1000);
	        seconds < 10 ? seconds = "0" + seconds : seconds;	
	     	if( document.getElementById("timer_" + i) != null && time > 1000){
	     		if(!yetToStart){
	     			activate(i, timer);
	     		}
	            document.getElementById("timer_" + i).innerHTML = "<b>" + hours + ":" + minutes + ":" + seconds + "</b>";
	        }else if(time <= 1000){
	        	greyOut(i, "early");
	        }
	     	
	        $("#loading-wrapper").hide();
	        if(lots[i].inc > 0){
	        	if($("#auctionPageName").val() === "auctionLivePage"){
	        		time = - serverTime + (strtTime + timerDuration + (i*60000) + Number($("#additionalTime").val()));
	        		lots[i].inc = 0;
	        	}else {
	        		var index = $("#lotIndex_" + i).val().split("-");
	        		time = - serverTime + (strtTime + timerDuration + (index[0]*60000) + Number($("#additionalTime").val()));
	        		lots[i].inc = 0;
	        	}
	        }else{
	        	time = time - 1000;
	        }
	        if (time <= -2000) {
	            clearInterval(lots[i].timer);
	            auctionTimer(strtTime, i, duration);
	        }      
    	}else{
    		clearInterval(lots[i].timer);
    	}
    }, 1000);
}

function fireTimer(){
	var x = setInterval(function(){
		if(serverTime != undefined){
			var startTime = $("#auctionStartTime").val();
		    var startDate = $("#auctionStartDate").val();
		    var strtTime = new Date(startDate+" "+startTime).getTime();
		    additionalTime = Number($("#additionalTime").val());
		    var duration = timerDuration + additionalTime;
		    if($("#auctionPageName").val() === "auctionLivePage"){
		        for (var i = 0; i < lots.length; i++) {
		            auctionTimer(strtTime, i, duration);
		            duration += 60000;
		        }
		    }else if($("#auctionPageName").val() === "auctionWatchListPage"){
		        for (var i = 0; i < lots.length; i++){
		            var index = $("#lotIndex_" + i).val().split("-");
		            auctionTimer(strtTime, i, duration + (index[0]*60000));
		        }
		    }
			clearInterval(x);
		}
		else{
			console.log("timer needs time!!")
		}
	}, 1000);
}

function activate(a, timer) {
	if(document.getElementById("setProxy") != undefined || document.getElementById("placeBid") != undefined){
		document.getElementById("setProxy").disabled = false;
	    document.getElementById("placeBid").disabled = false;
	} 
	
	if(document.getElementById("status_" + a) != undefined && !timer){
		document.getElementById("status_" + a).innerHTML = "<b>Bid In Progress</b>";
	}
	if(!timer){
		document.getElementById("accordion_"  +  a).style.display = "none";
	}
    if ($("#row_" + a).hasClass("early")) {
        $("#row_" + a).removeClass("early");
    }
    if (document.getElementById("hmr_" + a).disabled === true) {
        document.getElementById("hmr_" + a).disabled = false;
    }
    if (document.getElementById("lot_" + a).disabled === true) {
        document.getElementById("lot_" + a).disabled = false;
    }
    if (document.getElementById("timer_" + a).disabled === true) {
        document.getElementById("timer_" + a).disabled = false;
    }
    if (document.getElementById("status_" + a) != undefined && document.getElementById("status_" + a).disabled === true) {
        document.getElementById("status_" + a).disabled = false;
    }
    if (document.getElementById("h1Prc_" + a).disabled === true) {
        document.getElementById("h1Prc_" + a).disabled = false;
    }
    if (document.getElementById("decbtn_" + a) != undefined && document.getElementById("decbtn_" + a).disabled === true) {
        document.getElementById("decbtn_" + a).disabled = false;
    }
    if (document.getElementById("bidLabel_" + a) != undefined && document.getElementById("bidLabel_" + a).disabled === true) {
        document.getElementById("bidLabel_" + a).disabled = false;
    }
    if (document.getElementById("incbtn_" + a) != undefined && document.getElementById("incbtn_" + a).disabled === true) {
        document.getElementById("incbtn_" + a).disabled = false;
    }
    if (document.getElementById("sel_" + a) != undefined && document.getElementById("sel_" + a).disabled === true) {
        document.getElementById("sel_" + a).disabled = false;
    }
}

function greyOut(a, txt) {
    if ($("#sel_" + a).is(':checked')) {
        $("#sel_" + a).prop('checked', false);
        lots[a].select = false;
    }
    $("#row_" + a).addClass(txt);
    document.getElementById("accordion_"  +  a).style.display = "none";
    document.getElementById("hmr_" + a).disabled = true;
    document.getElementById("lot_" + a).disabled = true;
    document.getElementById("timer_" + a).disabled = true;
    if(document.getElementById("status_" + a) != undefined){
    	document.getElementById("status_" + a).disabled = true;
    }
    document.getElementById("h1Prc_" + a).disabled = true;
    if(document.getElementById("decbtn_" + a) != undefined){
    	document.getElementById("decbtn_" + a).disabled = true;
    	document.getElementById("bidLabel_" + a).disabled = true;
    	document.getElementById("incbtn_" + a).disabled = true;
    }
    if(document.getElementById("sel_" + a) != undefined){
    	document.getElementById("sel_" + a).disabled = true;
    }
}

function proxyChecker(a) {
    if (lots[a].proxyAmt != "" && lots[a].h1Price != "" && lots[a].h1Price > lots[a].proxyAmt) {
        document.getElementById("proxyVal_" + a).innerHTML = "Proxy Outbid";
        document.getElementById("proxyVal_" + a).parentNode.style.backgroundColor = "#f57921";
    }
}

function mergeArray(array1, array2) {
    var result_array = [];
    var arr = array1.concat(array2);
    var len = arr.length;
    var assoc = {};

    while(len--) {
        var item = arr[len];

        if(!assoc[item]) 
        { 
            result_array.unshift(item);
            assoc[item] = true;
        }
    }

    return result_array;
}


function incBid(a) {
    if (lots[a].select === true) {
        var my_bid = parseInt(lots[a].myBid, 10);
        my_bid += 500;
        lots[a].myBid = my_bid;
        document.getElementById("bidLabel_" + a).innerHTML = lots[a].myBid;
    }
    else {
        var snackbar = document.getElementById("snackBar");
        snackbar.innerHTML = "Please select the checkbox for "+ $(".lot_"+lots[a].lotNo).text();
        snackbar.style.backgroundColor = "darkred";
        snackbar.className = "show";
        setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
    }
}

function decBid(a) {
    if (lots[a].select === true) {
        if ((lots[a].h1Price === "" || lots[a].h1Price === "0" || lots[a].h1Price === "null") && (lots[a].proxyAmt === "" || lots[a].proxyAmt === "0" || lots[a].proxyAmt === "null")) {
            if (lots[a].myBid > lots[a].startBidPrice) {
                lots[a].myBid -= 500;
            }
        } else {
            if (lots[a].proxyAmt > lots[a].h1Price) {
                if (lots[a].myBid > lots[a].proxyAmt) {
                    lots[a].myBid -= 500;
                }
            } else {
                if (lots[a].myBid > Number(lots[a].h1Price)+500) {
                    lots[a].myBid -= 500;
                }
            }
        }
    }
    else {
        var snackbar = document.getElementById("snackBar");
        snackbar.innerHTML = "Please select the checkbox for "+ $(".lot_"+lots[a].lotNo).text();
        snackbar.style.backgroundColor = "darkred";
        snackbar.className = "show";
        setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
    }
    document.getElementById("bidLabel_" + a).innerHTML = lots[a].myBid;
}

function getLotData() {
    var lotJson = [];
    var prodJson = [];
    var mainTable = $(document).find("#myTable");
    var $headers = mainTable.find("th");
    var lotRows = document.getElementsByClassName("lotRow");
    if(lotRows.length===0){
    	$("#loading-wrapper").hide();
    }
    var prodTables = document.getElementsByClassName("prods");
    for (var i = 0; i < lotRows.length; i++) {
        var $cells = $(lotRows[i]).find("td");
        lotJson[i] = {};
        for (var k = 0; k < $cells.length; k++) {
            lotJson[i][$($headers[k]).html()] = $($cells[k]).text();
        }
        var prodDetails = [];
        var $prodRows = $(prodTables[i]).find(".prodRow");
        var $prodHeaders = $(prodTables[i]).find("th");
        for (var j = 0; j < $prodRows.length; j++) {
            var $prodCells = $($prodRows[j]).find("td");
            prodJson[j] = {};
            for (var l = 0; l < $prodCells.length; l++) {
                prodJson[j][$($prodHeaders[l]).html()] = $($prodCells[l]).text();
                prodDetails[j] = prodJson[j];
            }
        }
        lotJson[i].prodDetails = prodDetails;
    }
    var myObj = {};
    myObj.bidEntries = lotJson;
    console.log(JSON.parse(JSON.stringify(myObj).replace(/\s/g, "")));
    
    return JSON.parse(JSON.stringify(myObj).replace(/\s/g, ""));
}

var lotDetails;
var lots = [];

function getAuctionByUrl() {
    var url = window.location.href;
    var id = url.split("=");
    if (id[1] != undefined) {
        return id[1];
    }
}

function displayHide(a){
	    if ($("#sel_" + a).is(':checked')) {
	        $("#sel_" + a).prop("checked", false);
	        lots[a].select = false;
	    }
	    document.getElementById("row_" + a).style.display = "none";
	    document.getElementById("accordion_" + a).style.display = "none";
}

function auctionTimer(strtTime, a, duration) {
    var elapsedTime = serverTime - strtTime;
    if (elapsedTime < 0) {
    	yetToStart = true;
        lots[a].status = "<b>Yet To Start</b>";
        if(document.getElementById("status_" + a) != undefined){
        	document.getElementById("status_" + a).innerHTML = "<b>Yet To Start</b>";
        }
        greyOut(a, "early");
        if(document.getElementById("setProxy") != undefined && document.getElementById("placeBid") != undefined){
        	document.getElementById("sel_" + a).disabled = false;
        	document.getElementById("setProxy").disabled = true;
            document.getElementById("placeBid").disabled = true;
        }
        checks(a);
        calculateTime(Math.abs(elapsedTime), a, strtTime, duration);
    }
    if (elapsedTime >= 0 && elapsedTime < duration) {
    	if(yetToStart)
		{
			yetToStart = false;
	    	resetTimer();
		}
        var remTime = duration - elapsedTime;
        var timer = false;
        activate(a, timer);
        checks(a);
        calculateTime(remTime, a, strtTime, duration);
    }
    if (elapsedTime >= duration) {
        document.getElementById("timer_" + a).innerHTML = "<b>00:00:00</b>";
        if(document.getElementById("status_" + a) != undefined){
        	document.getElementById("status_" + a).innerHTML = "<b>Bid Completed</b>"
        }
        clearInterval(lots[a].check);
        if(auctionStatus == "CLOSED"){
    		document.getElementById("timer_" + a).innerHTML = "<b>00:00:00</b>";
    		if(lots[a].h1Bidder != undefined && lots[a].h1Bidder == user){
    			lots[a].status="Bid Won";
    			document.getElementById("status_" + a).innerHTML = "<b>Bid Won</b>";
    			lots[a].hammer = true;
	            document.getElementById("hmr_" + a).innerHTML = "<i class = \"fa fa-legal\">" + "</i>";
    		}else{
    			lots[a].status = "Auction Closed";
                document.getElementById("status_" + a).innerHTML = "<b>Auction Closed</b>";
                greyOut(a, "late");
                displayHide(a);
    		}
        }
        greyOut(a, "late");
        $("#loading-wrapper").hide();
        if (lots[a].h1Bidder != undefined && lots[a].h1Bidder == user) {
            winCount++;
            document.getElementById("wc").innerHTML = winCount;
            lots[a].hammer = true;
            document.getElementById("hmr_" + a).innerHTML = "<i class = \"fa fa-legal\">" + "</i>";
            lots[a].status = "Bid Won";
            document.getElementById("status_" + a).innerHTML = "<b>Bid Won</b>";
            $("#row_" + a).addClass("bidPlaced");
        }
        else {
        	displayHide(a);
        }
        if(a === lots.length-1){
        	clearInterval(additionalTimeInterval);
        	clearInterval(timerFire);
        }
    }
}

function getSelectedAuction(sel) {
    var opt;
    for (var i = 0, len = sel.options.length; i < len; i++) {
        opt = sel.options[i];
        if (opt.selected === true) {
            break;
        }
    }
    return opt.value;
}

$(".seriesCheckboxInner").hide();
$(".gradeCheckboxInner").hide();
$(".seriesGradeInner").hide();

/* websocket and ajax code */
var contextPath = $("#contextPath").val();
var websocket = null;

function openConnection() {
	var loc = window.location, uri;
	var error;
	if (loc.protocol === "https:") {
	    uri = "wss:";
	} else {
	    uri = "ws:";
	}
	uri += "//" + loc.host+"/jslstorefront/getH1Price";
	try {
		websocket = new WebSocket(uri);
		websocket.onopen = () => sendMessage();
		websocket.onerror = function (error) {
			    console.log("Error in socket connection",error);
		  };
	} catch(e) {
		console.log("Exceptions in socket connection",e);
	}
}

function closeConnection() {
	setTimeout(function() { 
	var lotsclosedLength = $(".late").length;
	var totalLotsLength = lots.length;
	if(lotsclosedLength == totalLotsLength) {
			//websocket.close();
			clearInterval(h1Interval);	
		}
	}, 2000);
}

function showRate() {
	websocket.onmessage = function(event) {
		var bidEvent = JSON.parse(event.data);
		console.log("websocket response: "+bidEvent);
	};
}

function sendMessage() {
	var jslLots = [];
	var auctionEvent = $("#acceptedAuctionList").val().trim();
	websocket.send(auctionEvent);
	showRate();
	closeConnection();
}

$("#placeBid").click(function() {
    var snackbar = document.getElementById("snackBar");
    var auctionID;
    var isLast30Sec = false;
    var lotIdOf30Sec;
    for (var i = 0; i < lots.length; i++) {
        if (lots[i].select === true) {
            if (lots[i].myBid > lots[i].h1Price) {
            	auctionID = lots[i].auctionId;
            	if(!auctionID) {
            		auctionID = $("#acceptedAuctionList").val().trim();
            	}
                var lotsData = {
                    "auctionEventId": auctionID.trim(),
                    "bidPrice": lots[i].myBid,
                    "bundleTemplateId": lots[i].lotNo
                };
                selectedLots.push(lotsData);
            } else {
                var bidfail = true;
            }
            if(1000 < lots[i].timeValue && lots[i].timeValue < 30000){
            	isLast30Sec = true;
            	lotIdOf30Sec = lots[i].lotNo;
            }
        }
    }
    if (bidfail === true) {
        snackbar.innerHTML = "Bid Failed! The bid can not be less than H1 Price";
        snackbar.style.backgroundColor = "darkred";
        snackbar.className = "show";
        setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
    }
    if (selectedLots.length === 0 && bidfail != true) {
        snackbar.innerHTML = "Please select at least one Lot!";
        snackbar.style.backgroundColor = "darkred";
        snackbar.className = "show";
        setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
    }
    var auctionData = {
    	"auctionEventId" : auctionID.trim(),
    	"lotIdOf30Sec" : lotIdOf30Sec,
    	"isLast30Sec" : isLast30Sec,
        "defaultBundleCommonDto": selectedLots
    };
    
    if (!$.isEmptyObject(selectedLots) && !bidfail) {
    	$.ajax({
			type:   "POST",
			url:   ACC.config.encodedContextPath +"/auction/placeBid?CSRFToken="+ACC.config.CSRFToken,
			data: JSON.stringify(auctionData),
			contentType:   "application/json; charset=utf-8",
			dataType:   "json",
			success:   function (response) {
				if(response.responseCode == "112") {
					var snackbar = document.getElementById("snackBar");
					snackbar.innerHTML = "Auction is closed";
					snackbar.style.backgroundColor = "darkred";
					snackbar.className = "show";
                    setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
				} else if(response.responseCode == "000") {
					var snackbar = document.getElementById("snackBar");
					var lotNumber = [];
					if (selectedLots.length == 1) {
						lotNumber.push($(".lot_"+selectedLots[0].bundleTemplateId).text());
					} else {
						for (var i = 0; i < selectedLots.length; i++) {
                            lotNumber.push($(".lot_"+selectedLots[i].bundleTemplateId).text());
						}
					} 
					selectedLots = [];
					snackbar.innerHTML = "Bid placed successfully for: "+lotNumber.join();
					snackbar.style.backgroundColor = "green";
					snackbar.className = "show";
                    setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
                    lotNumber = [];
                    for (var i = 0; i < lots.length; i++) {
                        if (lots[i].select === true) {
                            lots[i].select = false;
                            document.getElementById("sel_" + i).checked = false;
                            lots[i].lastBid = lots[i].myBid;
                        }
                    }
				} else if(response.responseCode == "108"){
					var snackbar = document.getElementById("snackBar");
					snackbar.innerHTML = "Bid price exceeded the maximum price limit";
					snackbar.style.backgroundColor = "darkred";
					snackbar.className = "show";
                    setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
				} else if(response.responseCode == "109"){
					var snackbar = document.getElementById("snackBar");
					snackbar.innerHTML = "Bid price cannot be less than current H1 price";
					snackbar.style.backgroundColor = "darkred";
					snackbar.className = "show";
                    setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
				} else if(response.responseCode == "110"){
					var snackbar = document.getElementById("snackBar");
					snackbar.innerHTML = "Bid price cannot be less than lot base bid price";
					snackbar.style.backgroundColor = "darkred";
					snackbar.className = "show";
                    setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
				} else if(response.responseCode == "114"){
					var snackbar = document.getElementById("snackBar");
					snackbar.innerHTML = "Bid Outbid";
					snackbar.style.backgroundColor = "darkorange";
					snackbar.className = "show";
                    setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
				} else {
					var snackbar = document.getElementById("snackBar");
					snackbar.innerHTML = "Failed to place bid";
					snackbar.style.backgroundColor = "darkred";
					snackbar.className = "show";
                    setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
				}
				selectedLots = [];
				console.log("additional time: "+response.additionalTime);
			},
			error: function (response) {
				snackbar = document.getElementById("snackBar");
				snackbar.innerHTML = "Failed to place bid";
				snackbar.style.backgroundColor = "darkred";
				snackbar.className = "show";
				setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
			}
		});
    }
});

$("#watchList").click(function() {
	var auctionID;
    for (var i = 0; i < lots.length; i++) {
        if (lots[i].select === true) {
        	auctionID = lots[i].auctionId;
        	if(!auctionID) {
        		auctionID = $("#acceptedAuctionList").val().trim();
        	}
            var lotsData = {
            		 "auctionEventId": auctionID.trim(),
                     "bundleTemplateId": lots[i].lotNo,
                     "lotIndex" : i
            };
            selectedLots.push(lotsData);
            lots[i].select = false;
            document.getElementById("sel_" + i).checked = false;
        }
    }
    if (selectedLots.length === 0) {
        var snackbar = document.getElementById("snackBar");
        snackbar.innerHTML = "Please select at least one Lot!";
        snackbar.style.backgroundColor = "darkred";
        snackbar.className = "show";
        setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
    }
    var watchList = {
        lotData: selectedLots
    };
   
    if (watchList != []) {
    	$.ajax({
    		type:   "POST",
    		url:    ACC.config.encodedContextPath + "/auction/addToWatchList?CSRFToken="+ACC.config.CSRFToken,
    		data: JSON.stringify(selectedLots),
    		contentType:   "application/json; charset=utf-8",
    		dataType:   "json",
    		success:   function (response) {
    			if(response == 112){
 					var snackbar = document.getElementById("snackBar");
 					snackbar.innerHTML = "Auction is closed";
 					snackbar.style.backgroundColor = "darkred";
 					snackbar.className = "show";
                     setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
 				} else if(response == 0) {
    				var snackbar = document.getElementById("snackBar");
    				snackbar.innerHTML = "Failed to add in watch list";
    				snackbar.style.backgroundColor = "darkred";
    				snackbar.className = "show";
    				setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
    			} else if(response == 111) {
    				var snackbar = document.getElementById("snackBar");
    				snackbar.innerHTML = "Already in watch list";
    				snackbar.style.backgroundColor = "darkorange";
    				snackbar.className = "show";
    				setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
    	            document.getElementById("watchlist_auction").href = ACC.config.encodedContextPath +"/auction/watchList?auctionID="+auctionID;
    			} else {
    				var snackbar = document.getElementById("snackBar");
    				var lotNumber = [];
					if (selectedLots.length == 1) {
						lotNumber.push($(".lot_"+selectedLots[0].bundleTemplateId).text());
					} else {
						for (var i = 0; i < selectedLots.length; i++) {
							lotNumber.push($(".lot_"+selectedLots[i].bundleTemplateId).text());
						}
					} 
    				snackbar.innerHTML = lotNumber.join()+ "succesfully added in watch list";
    				snackbar.style.backgroundColor = "darkgreen";
    				snackbar.className = "show";
    				setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
    	            document.getElementById("watchlist_auction").href = ACC.config.encodedContextPath +"/auction/watchList?auctionID="+auctionID;
    	            lotNumber = [];
    			}
    			selectedLots = [];
    		},
    		error: function (response) {
    			var snackbar = document.getElementById("snackBar");
    			snackbar.innerHTML = "Failed to add in watch list";
    			snackbar.style.backgroundColor = "darkred";
    			snackbar.className = "show";
    			setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
    			selectedLots = [];
    		}
    	});
    	 
    }
});

$("#setProxy").click(function() {
	var auctionID;
    var isLast30Sec = false;
    var lotIdOf30Sec;
    for (var i = 0; i < lots.length; i++) {
        if (lots[i].select === true) {
        	auctionID = lots[i].auctionId;
        	if(!auctionID) {
        		auctionID = $("#acceptedAuctionList").val().trim();
        	}
            var lotsData = {
            		 "auctionEventId": auctionID.trim(),
                     "bidPrice": lots[i].myBid,
                     "bundleTemplateId": lots[i].lotNo
            };
            selectedLots.push(lotsData);
            if(1000 < lots[i].timeValue && lots[i].timeValue < 30000){
            	isLast30Sec = true;
            	lotIdOf30Sec = lots[i].lotNo;
            }
        }
    }
    if (selectedLots.length === 0) {
        var snackbar = document.getElementById("snackBar");
        snackbar.innerHTML = "Please select at least one Lot!";
        snackbar.style.backgroundColor = "darkred";
        snackbar.className = "show";
        setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
    }
    var proxy = {
        lotData: selectedLots
    };
    var auctionData = {
        	"auctionEventId" : auctionID.trim(),
        	"lotIdOf30Sec" : lotIdOf30Sec,
        	"isLast30Sec" : isLast30Sec,
            "defaultBundleCommonDto": selectedLots
        };
    
    if (!$.isEmptyObject(selectedLots)) {
    	 $.ajax({
             type:   "POST",
             url:   ACC.config.encodedContextPath +"/auction/setProxy?CSRFToken="+ACC.config.CSRFToken,
             data: JSON.stringify(auctionData),
             contentType:   "application/json; charset=utf-8",
             dataType:   "json",
             success:   function (response) {
            	 if(response.responseCode == "112"){
 					var snackbar = document.getElementById("snackBar");
 					snackbar.innerHTML = "Auction is closed";
 					snackbar.style.backgroundColor = "darkred";
 					snackbar.className = "show";
                     setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
 				} else if(response.responseCode == "000") {
 					var snackbar = document.getElementById("snackBar");
					var proxySetLotNumber = [];
					if (selectedLots.length == 1) {
						proxySetLotNumber.push($(".lot_"+selectedLots[0].bundleTemplateId).text());
					} else {
						for (var i = 0; i < selectedLots.length; i++) {
							proxySetLotNumber.push($(".lot_"+selectedLots[i].bundleTemplateId).text());
						}
                    }
                    snackbar.innerHTML = "Proxy bid set successfully for: "+proxySetLotNumber.join();
 					snackbar.style.backgroundColor = "green";
 					snackbar.className = "show";
 					setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
                    for(var i = 0; i < lots.length; i++){
                        if (lots[i].select === true) {
                            lots[i].proxyAmt = lots[i].myBid;
                            document.getElementById("proxyVal_" + i).innerHTML = Number(lots[i].myBid).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                            document.getElementById("proxyVal_" + i).parentNode.style.backgroundColor = "";
                            document.getElementById("proxyVal_" + i).parentNode.classList.remove("blink");
                            lots[i].select = false;
                            document.getElementById("sel_" + i).checked = false;
                        }
                    }
 					proxySetLotNumber = [];
 				} else if(response.responseCode == "108"){
					var snackbar = document.getElementById("snackBar");
					snackbar.innerHTML = "Proxy Bid price exceeded the maximum price limit";
					snackbar.style.backgroundColor = "darkred";
					snackbar.className = "show";
                    setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
				} else if(response.responseCode == "109"){
					var snackbar = document.getElementById("snackBar");
					snackbar.innerHTML = "Proxy Bid price cannot be less than current H1 price";
					snackbar.style.backgroundColor = "darkred";
					snackbar.className = "show";
                    setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
				} else if(response.responseCode == "110"){
					var snackbar = document.getElementById("snackBar");
					snackbar.innerHTML = "Proxy Bid price cannot be less than lot base bid price";
					snackbar.style.backgroundColor = "darkred";
					snackbar.className = "show";
                    setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
				} else if(response.responseCode == "113"){
					var snackbar = document.getElementById("snackBar");
					snackbar.innerHTML = "Bid Outbid";
					snackbar.style.backgroundColor = "darkred";
					snackbar.className = "show";
                    setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
				} else if(response.responseCode == "115"){
					var snackbar = document.getElementById("snackBar");
					snackbar.innerHTML = "Bid Outbid";
					snackbar.style.backgroundColor = "darkred";
					snackbar.className = "show";
                    setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
				} else {
					var snackbar = document.getElementById("snackBar");
					snackbar.innerHTML = "Failed to set proxy bid";
					snackbar.style.backgroundColor = "darkred";
					snackbar.className = "show";
                    setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
				}
				selectedLots = [];
 			},
 			error: function (response) {
 				var snackbar = document.getElementById("snackBar");
 				snackbar.innerHTML = "Failed to set proxy bid";
 				snackbar.style.backgroundColor = "darkred";
 				snackbar.className = "show";
 				setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);

 				selectedLots = [];
 			}
         });
    }
});

$('#removeWatchList').click(function() {
	var auctionID;
	 for (var i = 0; i < lots.length; i++) {
	        if (lots[i].select === true) {
	        	auctionID = lots[i].auctionId;
	        	if(!auctionID) {
	        		auctionID = $("#acceptedAuctionList").val().trim();
	        	}
	            var lotsData = {
	        		 "auctionEventId": auctionID.trim(),
	                 "bundleTemplateId": lots[i].lotNo
	            };
	            selectedLots.push(lotsData);
	            lots[i].select = false;
	            document.getElementById("sel_" + i).checked = false;
	        }
	    }
	    if (selectedLots.length === 0) {
	        var snackbar = document.getElementById("snackBar");
	        snackbar.innerHTML = "Please select at least one Lot!";
	        snackbar.style.backgroundColor = "darkred";
	        snackbar.className = "show";
	        setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
	    }
	    var watchList = {
	        lotData: selectedLots
	    };
	   
		$.ajax({
			type:   "POST",
			url:   ACC.config.encodedContextPath +"/auction/removeWatchList?CSRFToken="+ACC.config.CSRFToken,
			data: JSON.stringify(selectedLots),
			contentType:   "application/json; charset=utf-8",
			dataType:   "json",
			success:   function (response) {
				if(response) {
					window.location.href = window.location.origin+contextPath+"/auction/watchList?removed=true&auctionID="+auctionID
				} else {
					var snackbar = document.getElementById("snackBar");
					snackbar.innerHTML = "Failed to remove from watch list";
					snackbar.style.backgroundColor = "darkred";
					snackbar.className = "show";
					setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
				}
			},
			error: function (response) {
				var snackbar = document.getElementById("snackBar");
				snackbar.innerHTML = "Failed to remove from watch list";
				snackbar.style.backgroundColor = "darkred";
				snackbar.className = "show";
				setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
			}
		});
});

$("#liveCustomers").click(function (){
	$.ajax({
		type:   "GET",
		cache : false,
		url:   ACC.config.encodedContextPath +"/auction/getLiveCustomers",
		success:   function (response) {
			var liveCustomers = response;
			var htmlCode = "";
			liveCustomers.forEach(function(customer){
				htmlCode = htmlCode + "<li><span>" + customer.name + "</span><br><span>Auction Id: " + customer.auctionId + "</span><br><span>User Group: " + customer.auctionUserGroup + "</span><br><span>Last Login Time: " + new Date(customer.lastLogin).toLocaleString() + "</span></li><br>";  
			});
			if(htmlCode == ""){
				htmlCode = "<h3>No Customers Logged In currently</h3>";
			}else{
				htmlCode = "<ul>" + htmlCode + "</ul>";
			}
			$("#liveCustomersDialogBody").html(htmlCode);
			$("#liveCustomersDialog").show();
		},
		error: function (response) {
			var liveCustomers = response;
		}
	});
});

function restorePageWithData() {
    console.log("page refreshed");
	var auctionId = $("#acceptedAuctionList").val().trim();
	JSON.stringify(auctionId);
	$.ajax({
		type:   "POST",
		url:   ACC.config.encodedContextPath +"/auction/restorePageWithData?CSRFToken="+ACC.config.CSRFToken,
		data: auctionId,
		contentType:   "application/json; charset=utf-8",
		success:   function (response) {
			if(response !== "") {
				var pageData = JSON.parse(response);
				if(pageData.maxBidForCurrentUser != null) {
					var maxBidForCurrentUser = JSON.parse(pageData.maxBidForCurrentUser);
					maxBidForCurrentUser.forEach(function (bidEvent) {
						$(".myBid_"+bidEvent.bundleTemplateId).text(Number(bidEvent.bidPrice));
					});
				}
				if(pageData.proxyBid != null) {
					var proxyBid = JSON.parse(pageData.proxyBid);
					proxyBid.forEach(function (proxyBidPrice) {
						$(".myProxyBid_"+proxyBidPrice.bundleTemplateId).text(Number(proxyBidPrice.bidPrice).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
					});
				}
			}
		},
		error: function (response) {
		}
	});
}

function showLotDetails(id) {
	$(id).toggle();
}

function selectAuction() {
	var auctionID = $("#acceptedAuctionList").val().trim();
	var pageName = $("#auctionPageName").val();
	if(pageName == "auctionLivePage") {
		window.location.href = window.location.origin+contextPath+"/auction/liveAuction?auctionID="+auctionID;
	} else {
		window.location.href = window.location.origin+contextPath+"/auction/watchList?auctionID="+auctionID;
	}
}

function getH1Price() {
	var auctionId = $("#acceptedAuctionList").val().trim();
	JSON.stringify(auctionId);
	$.ajax({
		type:   "POST",
		url:   ACC.config.encodedContextPath +"/auction/getH1Price?CSRFToken="+ACC.config.CSRFToken,
		data: auctionId,
		contentType:   "application/json",
		success:   function (response) {
			var bidEvent = JSON.parse(response);
			if(!$.isEmptyObject(bidEvent.defaultBundleCommonDto)) {
				bidEvent.defaultBundleCommonDto.forEach(function(data) {
					if(data != null || data !== "") {
						if(data.bidPrice != null || data.bidPrice !== "") {
							$(".h1Prc_"+data.bundleTemplateId).text(Number(data.bidPrice).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
						} else {
							$(".h1Prc_"+data.bundleTemplateId).text(0);
						}
						var h1Id = $(".h1Prc_"+data.bundleTemplateId).prop('id');
						if(h1Id !== undefined) {
							h1Id = h1Id.split("_")[1];
							lots[h1Id].h1Price = Number(data.bidPrice);
							lots[h1Id].h1Bidder = Number(data.h1Bidder);
							var proxyBidSet = $("#proxyVal_"+h1Id).text().trim();
							if(Number(lots[h1Id].h1Price) >= Number(lots[h1Id].myBid)) {
								lots[h1Id].myBid = Number(parseInt(data.bidPrice)+parseInt(500)).toFixed(0);
								$(".myBid_"+data.bundleTemplateId).text(lots[h1Id].myBid);
							}
							if(lots[h1Id].h1Bidder != undefined && lots[h1Id].h1Bidder == user){
								$("#row_" + h1Id).addClass("bidPlaced");
							} else {
								$("#row_" + h1Id).removeClass("bidPlaced");
							}
						}
					}
				});
			} else {
				$(".h1Prc").text(0);
			}
			serverTime = bidEvent.currentTime;
			auctionStatus = bidEvent.auctionStatus;
			closeConnection();
		},
		error: function (response) {
		}
	});
}

function showLotBidDetails(a) {
	var lotsData = {
   		 "auctionEventId": $("#acceptedAuctionList").val().trim(),
   		 "bundleTemplateId": $("#bundleId_"+a).val()
       };
    JSON.stringify(lotsData);
	$.ajax({
		type:   "POST",
		url:   ACC.config.encodedContextPath +"/auction/getLotBidDetails?CSRFToken="+ACC.config.CSRFToken,
		data: JSON.stringify(lotsData),
		contentType:   "application/json",
		success:   function (response) {
			var bidEventDetails = JSON.parse(response);
			var listCode = "<ul>";
			for (var p in bidEventDetails) {
			  	if(p !== "totalBidCount" && p !== "h1Bidder") {
			  		listCode = listCode + "<li>" + p + "</li>";
			  	}
			}
			listCode = listCode + "</ul>";
			document.getElementById("detailsBox").innerHTML = listCode;
			document.getElementById("h1Bidder").innerHTML = bidEventDetails['h1Bidder'];
			document.getElementById("totalBidCount").innerHTML = bidEventDetails['totalBidCount'];
			showModal("detailsModal");
	  		console.log("h1Bidder: "+bidEventDetails['h1Bidder']);
	  		console.log("Total Bid counts: "+bidEventDetails['totalBidCount']);
		},
		error: function (response) {
		}
	});
}

function showModal(name) {
    document.getElementById(name).style.display = "block";
}

function hideModal(name) {
    document.getElementById(name).style.display = "none";
}

function addAdditionalTime() {
	var auctionId = $("#acceptedAuctionList").val().trim();
	JSON.stringify(auctionId);
	$.ajax({
		type:   "POST",
		url:   ACC.config.encodedContextPath +"/auction/addAdditionalTime?CSRFToken="+ACC.config.CSRFToken,
		data: auctionId,
		contentType:   "application/json",
		success:   function (response) {
            return response;
		},
		error: function (response) {
		}
	});
}

function getAdditionalTime() {
	var auctionId = $("#acceptedAuctionList").val().trim();
	$.ajax({
		type:   "POST",
		url:   ACC.config.encodedContextPath +"/auction/getAuctionEventData?CSRFToken="+ACC.config.CSRFToken,
		data: JSON.stringify(auctionId),
		contentType:   "application/json",
		success:   function (response) {
			var previousTime = $("#additionalTime").val();
			var auctionEvent = JSON.parse(response);
			if(Number(auctionEvent.additionalTime) > Number($("#additionalTime").val())) {
				$("#additionalTime").val(auctionEvent.additionalTime);
				for(var i = 0; i < lots.length; i++){
					lots[i].inc = auctionEvent.additionalTime - previousTime;
				}				
			}
		},
		error: function (response) {
		}
	});
}

function resetTimer() {
	winCount = 0;
	var time = getLatestTimer();
	for (var i = 0; i < lots.length; i++) {
		clearInterval(lots[i].timer);
		additionalTime = Number($("#additionalTime").val());
	    var duration = timerDuration + additionalTime;
	    if($("#auctionPageName").val() === "auctionLivePage"){
		    if(yetToStart){
		    	calculateTime(time-timerDuration, i, auctionStartDateTime, duration);
		    }else{
		    	calculateTime(time+(i*60000), i, auctionStartDateTime, duration);
		    }
	    }else if($("#auctionPageName").val() === "auctionWatchListPage"){
	    	if(yetToStart){
		    	calculateTime(time-timerDuration, i, auctionStartDateTime, duration);
		    }else{
		    	var index = $("#lotIndex_" + i).val().split("-");
		    	calculateTime(time+(index[0]*60000), i, auctionStartDateTime, duration);
		    }
	    }
	}
}

function getLatestTimer() {
	return - serverTime + (auctionStartDateTime + timerDuration + Number($("#additionalTime").val()));
}
