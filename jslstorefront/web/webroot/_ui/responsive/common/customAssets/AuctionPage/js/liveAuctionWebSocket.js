var stompClient = null;

$(document).ready(function() {
	connect();
});

function setConnected(connected) {
	alert("connected "+connected);
}

function connect() {
	var socket = new SockJS("/jslstorefront/getCurrentH1Price");
	stompClient = Stomp.over(socket);
	stompClient.connect({}, function(frame) {
		setConnected(true);
		console.log('Connected: ' + frame);
		stompClient.subscribe('/topic/showH1Price', function(messageOutput) {
			showMessageOutput(JSON.parse(messageOutput));
		});
	});
}

function disconnect() {
	if (stompClient != null) {
		stompClient.disconnect();
	}
	setConnected(false);
	console.log("Disconnected");
}

function sendMessage() {
	stompClient.send("/liveAuctionApp/getCurrentH1Price", {}, JSON.stringify({
		'auctionEventId' : "4001",
	}));
}

function showMessageOutput(messageOutput) {
	console.log("message from server: "+messageOutput);
}