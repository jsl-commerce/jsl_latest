window.onload = function(){
    $("#loading-wrapper").hide();
}

function getFreightCharges(str) {
	$.ajax({
		method: "GET",
		url: ACC.config.encodedContextPath + '/custom/downloadFreightPdf/' + str,
		xhrFields: {
			responseType: 'blob'
		},
		success: function(blob) {
			 /*window.open(data);*/
			console.log(blob.size);
      		var link=document.createElement('a');
    	  	link.href=window.URL.createObjectURL(blob);
	      	link.download="FreightCharge_" + str + ".pdf";
	      	link.click();
			
		},
		error: function() {
			
		}
	});

}
