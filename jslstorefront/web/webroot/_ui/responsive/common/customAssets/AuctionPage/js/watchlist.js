var isRemoved = $("#removedFromWatchlist").val();
if(isRemoved == "true") {
	var snackbar = document.getElementById("snackBar");
	 snackbar.innerHTML = "Removed from watch list";
     snackbar.style.backgroundColor = "green";
     snackbar.className = "show";
     setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
}
