function setDate() {
	const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
	    "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"
	];
	if($("#auctionPageName").val() !== 'auctionUpcomingPage' &&  $("#auctionPageName").val() !== 'auctionUserManual') {
		var d = new Date();
		document.getElementById("auction_date").innerHTML = d.getDate();
		document.getElementById("auction_month").innerHTML = monthNames[d.getMonth()];
		document.getElementById("auction_year").innerHTML = d.getFullYear();
	} else if($("#auctionPageName").val() === 'auctionUpcomingPage') {
		var tomorrow = new Date();
		tomorrow.setDate(tomorrow.getDate() + 1);
		document.getElementById("auction_date").innerHTML = tomorrow.getDate();
		document.getElementById("auction_month").innerHTML = monthNames[tomorrow.getMonth()];
		document.getElementById("auction_year").innerHTML = tomorrow.getFullYear();
	}

}

function setCurrentLeftNavTab() {
	$(".leftNav").removeClass("current");
	var pageName = $("#auctionPageName").val();
	$("#"+pageName).addClass("current");
}

function enableLinks(){
	if(localStorage.getItem("optionsAuction") != null){
		var auctionID = JSON.parse(localStorage.getItem("optionsAuction"));
		if(!$.isEmptyObject(auctionID)){
			document.getElementById("live_auction").classList.remove("isDisabled");
			document.getElementById("watchlist_auction").classList.remove("isDisabled");
		}
	}else if(localStorage.getItem("optionsAuctionUp") != null){
		var auctionIDUp = JSON.parse(localStorage.getItem("optionsAuctionUp"));
		if(!$.isEmptyObject(auctionIDUp)){
			document.getElementById("watchlist_auction").classList.remove("isDisabled");
		}
	}
}

function getOtpStatus() {
	localStorage.removeItem("optionsAuction");
//	if($("#isAdminUser").val() === "true"){
//		var auctionId = [];
//		if(!$.isEmptyObject(auctions)){
//			for(var i = 0; i < auctions.length; i++){
//				auctionId.push(auctions[i].auctionNo);
//			}
//			localStorage.setItem("optionsAuction", JSON.stringify(auctionId));
//			return;
//		}
//		else{
//			return;
//		}
//	}
	var auctionId = [];
	var otpAcceptedForAuction = document.getElementsByClassName("otpAccepted");
	for (var index = 0; index < otpAcceptedForAuction.length; ++index) {
		auctionId.push(otpAcceptedForAuction[index].value);
	}
	if(!$.isEmptyObject(auctionId)) {
		localStorage.setItem("optionsAuction", JSON.stringify(auctionId));
	}
}

function setLeftNavUrl() {
	if(localStorage.getItem("optionsAuction") != null){
		var auctionID = JSON.parse(localStorage.getItem("optionsAuction"));
		if(!$.isEmptyObject(auctionID)) {
			document.getElementById("live_auction").href = ACC.config.encodedContextPath +"/auction/liveAuction?auctionID="+auctionID[auctionID.length-1];
			document.getElementById("watchlist_auction").href = ACC.config.encodedContextPath +"/auction/watchList?auctionID="+auctionID[auctionID.length-1];
			return;
		}
	}
	if(localStorage.getItem("optionsAuctionUp") != null){
		var auctionID = JSON.parse(localStorage.getItem("optionsAuctionUp"));
		if(!$.isEmptyObject(auctionID)) {
			document.getElementById("live_auction").href = ACC.config.encodedContextPath +"/auction/liveAuction?auctionID="+auctionID[auctionID.length-1];
			document.getElementById("watchlist_auction").href = ACC.config.encodedContextPath +"/auction/watchList?auctionID="+auctionID[auctionID.length-1];
		} else {
			$("#watchlist_auction").addClass("isDisabled");
		}
	}
}

function navigatePageToSelectedAuction() {
	var auctionID = localStorage.getItem("optionsAuction");
	var fromAuctionId = $("#acceptedAuctionList").val().trim();
	if(!$.isEmptyObject(auctionID) && $("#auctionPageName").val() === "auctionLivePage" || $("#auctionPageName").val() === "auctionWatchListPage" ) {
	    document.getElementById("live_auction").href = ACC.config.encodedContextPath +"/auction/liveAuction?auctionID="+fromAuctionId;
	    document.getElementById("watchlist_auction").href = ACC.config.encodedContextPath +"/auction/watchList?auctionID="+fromAuctionId;
	} else {
		$("#watchlist_auction").addClass("isDisabled");
	}
}

function checkIsAdminUser() {
	var isAdminUser = $("#isAdminUser").val();
	if(isAdminUser != "" && isAdminUser === "true") {
		$("#auctionWatchListPage").hide();
//		$("#auctionCompletedPage").hide();
//		$("#auctionUpcomingPage").hide();
		$("#auctionUserManual").show();
	} else {
		$("#auctionWatchListPage").show();
//		$("#auctionCompletedPage").show();
//		$("#auctionUpcomingPage").show();
		$("#auctionUserManual").show();
	}
}

function getAuctionStatus() {
	if($("#auctionPageName").val() === 'auctionHomePage') {
		var auctionId = [];
		var auctionStatus = document.getElementsByClassName("auctionStatus");
		console.log(auctionStatus);
		if(auctionStatus.length==0){
			 $("#loading-wrapper").hide();
		}
		for (var index = 0; index < auctionStatus.length; ++index) {
			if(auctionStatus[index].innerText.toUpperCase() !== "STARTED" && auctionStatus[index].innerText.toUpperCase() !== "YET TO START") {
				var loopIndex = auctionStatus[index].id.split("_")[1];
				$("#acptBtn_"+loopIndex).addClass("isDisabled");
				$("#acptBtn_"+loopIndex).removeClass("isEnabled");
				if(auctionStatus[index].innerText.toUpperCase() !== "COMPLETED" || ($("#isAdminUser").val() != "" && $("#isAdminUser").val() == "true")) {
					$("#auctionNum_"+loopIndex).addClass("isDisabled");
					$("#auctionNum_"+loopIndex).removeClass("isEnabled");
				}
			}
		}
	}
}

function navigatePageToCompletedAuction() {
	$("#fromLeftNavToCompleted").submit();
}

(function($) {

    var $window = $(window),
        $body = $('body'),
        $document = $(document);

    // Breakpoints.
    breakpoints({
        desktop: ['737px', null],
        wide: ['1201px', null],
        narrow: ['737px', '1200px'],
        narrower: ['737px', '1000px'],
        mobile: [null, '736px']
    });

    // Play initial animations on page load.
    $window.on('load', function() {
        //document.getElementById("loading").style.display = "none";
        window.setTimeout(function() {
            $body.removeClass('is-preload');
        }, 100);
    });

    // Title Bar.
    if($('#logo').html() != null && $('#logo').html() != undefined){
    $(
            '<div id="titleBar">' +
            '<a href="#sidebar" class="toggle"></a>' +
            '<span class="title">' + $('#logo').html() + '</span>' +
            '</div>'
        )
        .appendTo($body);
	}
    // Sidebar
    $('#sidebar')
        .panel({
            delay: 500,
            hideOnClick: true,
            hideOnSwipe: true,
            resetScroll: true,
            resetForms: true,
            side: 'left',
            target: $body,
            visibleClass: 'sidebar-visible'
        });

})(jQuery);

function showDetails() {
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.display === "block")
                panel.style.display = "none";
            else
                panel.style.display = "block";
        });
    }
}

function navigateLive() {
    if (acceptFlag === true)
        window.location = "./Live-Auction/live-auction.html";
    if (acceptFlag === false) {
        var snackbar = document.getElementById("snackBar");
        snackbar.innerHTML = "Kindly accept the Terms & Conditions";
        snackbar.style.backgroundColor = "red";
        snackbar.className = "show";
        setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
    }

}

function Auction(auctionNo, date, startTime, lotDetails, termsLink, termsFlag) {
    this.auctionNo = auctionNo;
    this.date = date;
    this.startTime = startTime;
    this.otp = "";
    this.lotDetails = lotDetails;
    this.termsLink = termsLink;
    this.termsFlag = termsFlag;
}

function getAuctionData() {
    var lotJson = [];
    var $headers = $("th");
    var $rows = $("tbody tr").each(function(index){
        var $cells = $(this).find("td");
        lotJson[index] = {};
        $cells.each(function(cellIndex){
            lotJson[index][$($headers[cellIndex]).html()] = $(this).text();
        });
    });
    var myObj = {};
    myObj.auctionList = lotJson;
    // console.log(JSON.parse(JSON.stringify(myObj).replace(/\s(?=\w+":)/g, "")));
    
    return JSON.parse(JSON.stringify(myObj).replace(/\s(?=\w+":)/g, ""));
}

var auctionData;
var auctions = [];
var table = document.getElementById("auctionTable");
var acceptFlag;

$(document).ready(function() {
    checkIsAdminUser();
    getAuctionStatus();
    
	setCurrentLeftNavTab();
	setDate();
	$("#CSRFToken").val(ACC.config.CSRFToken);
	document.getElementsByTagName("footer")[0].setAttribute("class", "auctionFooter");
	
    auctionData = getAuctionData();
    if (auctionData.auctionList.length >= 1 && $("#auctionPageName").val() === 'auctionHomePage') {
        loadTable(auctionData);
    }
//    acceptFlag = localStorage.getItem("JSLLocalStorage");
//    if (acceptFlag === null) {
//        console.log('I am null');
//    } else if (acceptFlag === "true") {
//        if (document.getElementById("live_auction") !== null) {
//            document.getElementById("live_auction").classList.add("isEnabled");
//            document.getElementById("live_auction").classList.remove("isDisabled");
//        }
//    } else {
//        if (document.getElementById("live_auction") !== null) {
//            document.getElementById("live_auction").classList.add("isDisabled");
//            document.getElementById("live_auction").classList.remove("isEnabled");
//        }
//    }
    /*for (var i = 0; i < auctionData.auctionList.length; i++) {
        if (document.getElementById("auctionNum_" + i) !== null) {
            document.getElementById("auctionNum_" + i).title = "Please Enter OTP and then accept Terms & Conditions";
            document.getElementById("auctionNum_" + i).href = "#";
        }
    }
    for (var i = 0; i < auctionData.auctionList.length; i++) {
        if (document.getElementById("view_catlgue_link_" + i) !== null) {
    		if($("#view_catlgue_link_" + i).hasClass("isEnabled")){
		        document.getElementById("view_catlgue_link_" + i).classList.remove("isDisabled");
		        document.getElementById("view_catlgue_link_" + i).classList.add("isEnabled");
    		} else {
    			document.getElementById("view_catlgue_link_" + i).title = "Please Enter OTP and then accept Terms & Conditions";
 		        document.getElementById("view_catlgue_link_" + i).classList.add("isDisabled");
 		        document.getElementById("view_catlgue_link_" + i).classList.remove("isEnabled");
    		}
        }
    }*/
    if($("#auctionPageName").val() === "auctionHomePage"){
        getOtpStatus();
    }
    enableLinks();
    console.log(auctions);
	setLeftNavUrl();
});

//var auctionIdArray = [];

function acceptLog(a) {
    hideModal();
    document.getElementById("auctionNum_" + a).title = "";
    var otpData = {
        auctionID: auctions[a].auctionNo,
        otp: document.getElementById("otp_" + a).value
    };
    var auctionID = auctions[a].auctionNo;
    var otp = document.getElementById("otp_" + a).value;
//    
//    if (localStorage.getItem("optionsAuction") != null) {
//        var auctionLocalData = JSON.parse(localStorage.getItem("optionsAuction"));
//        for (var i = 0; i < auctionLocalData.length; i++) {
//            if (auctionLocalData[i] !== auctions[a].auctionNo) {
//                if (auctionIdArray.includes(auctionLocalData[i]) === false) {
//                    auctionIdArray.push(auctionLocalData[i]);
//                }
//                if (auctionIdArray.includes(auctions[a].auctionNo) === false) {
//                    auctionIdArray.push(auctions[a].auctionNo);
//                }
//            }
//
//        }
//        localStorage.setItem("optionsAuction", JSON.stringify(auctionIdArray));
//    } else {
//        auctionIdArray.push(auctions[a].auctionNo);
//        localStorage.setItem("optionsAuction", JSON.stringify(auctionIdArray));
//    }
    $.ajax({
        type: "POST",
        url: ACC.config.encodedContextPath +"/auction/validateOTP?auctionId="+auctionID+"&otp="+otp+"&CSRFToken="+ACC.config.CSRFToken,
        success: function (response) {
        	var otpResponse = JSON.parse(response);
        	if(otpResponse.isValid == "true") {
	        	var snackbar = document.getElementById("snackBar");
	            snackbar.innerHTML = "Terms & Conditions Accepted";
	            snackbar.style.backgroundColor = "green";
	            snackbar.className = "show";
	            setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
	            auctions[a].termsFlag = true;
                document.getElementById("acptBtn_" + a).innerHTML = "Show T&C  <i class=\"fa fa-check-square\" style=\"font-size: 20px;color:green;\"></i>";
                document.getElementById("acptBtn_" + a).onclick = function(arg) { return function() { openModal(arg,'accepted'); } }(a);
                document.getElementById("auctionNum_" + a).title = "";
                document.getElementById("view_catlgue_link_" + a).title = "";
	            document.getElementById("auctionNum_" + a).href = ACC.config.encodedContextPath +"/auction/liveAuction?auctionID="+auctionID;
	            document.getElementById("live_auction").href = ACC.config.encodedContextPath +"/auction/liveAuction?auctionID="+auctionID;
//	            document.getElementById("live_auction").classList.add("isEnabled");
	            document.getElementById("live_auction").classList.remove("isDisabled");
	            document.getElementById("watchlist_auction").classList.remove("isDisabled");
//	            document.getElementById("view_catlgue_link_" + a).classList.add("isEnabled");
	            document.getElementById("view_catlgue_link_" + a).classList.remove("isDisabled");
	            if (localStorage.getItem("optionsAuction") != null) {
	              var auctionLocalData = JSON.parse(localStorage.getItem("optionsAuction"));
	              auctionLocalData.push(auctions[a].auctionNo);
	              localStorage.setItem("optionsAuction", JSON.stringify(auctionLocalData));
	            }else{
	            	var auctionLocalData = [];
	            	auctionLocalData.push(auctions[a].auctionNo);
	            	localStorage.setItem("optionsAuction", JSON.stringify(auctionLocalData));
	            }
	            
        	} else if(otpResponse.isValid == "false" && otpResponse.reason =="expired") {
        		var snackbar = document.getElementById("snackBar");
	            snackbar.innerHTML = "OTP expired";
	            snackbar.style.backgroundColor = "red";
	            snackbar.className = "show";
	            setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
        	} else if(otpResponse.isValid == "false"  && otpResponse.reason =="notfound") {
        		var snackbar = document.getElementById("snackBar");
	            snackbar.innerHTML = "OTP not generated";
	            snackbar.style.backgroundColor = "darkred";
	            snackbar.className = "show";
	            setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
        	} else if(otpResponse.isValid == "false"  && otpResponse.reason =="invalid") {
        		var snackbar = document.getElementById("snackBar");
	            snackbar.innerHTML = "Please enter valid OTP";
	            snackbar.style.backgroundColor = "darkred";
	            snackbar.className = "show";
	            setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
        	}
            
        },
        error: function(response) {
        	var snackbar = document.getElementById("snackBar");
            snackbar.innerHTML = "Sorry something went wrong";
            snackbar.style.backgroundColor = "darkred";
            snackbar.className = "show";
            setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
        }
    });
   // localStorage.setItem("JSLLocalStorage", true);
}

function createModal(a, otpStatus) {
    document.getElementById("modalAuctionNo0").innerHTML = auctions[a].auctionNo;
    if(otpStatus === "pending") {
    	$("#otpStausPending").show();
    	$("#tncPending").show();
    	$("#tnc_accept").show();
    	$("#tncAccepted").hide();
    	
	    var checker = document.getElementById("tnc_check");
	    checker.checked = false;
	    var accept_btn = document.getElementById("tnc_accept");
	    accept_btn.disabled = true;
	    accept_btn.onclick = function(arg) { return function() { acceptLog(arg); } }(a);
	    checker.onchange = function() {
	        accept_btn.disabled = !this.checked;
	    }
    } else {
    	$("#otpStausPending").hide();
    	$("#tncPending").hide();
    	$("#tnc_accept").hide();
    	$("#tncAccepted").show();
    }
}

function showModal() {
    document.getElementById("myModal").style.display = "block";
}

function hideModal() {
    document.getElementById("myModal").style.display = "none";
}

function openModal(a,otpStatus) {
    if (document.getElementById("otp_" + a).value === "" && otpStatus === "pending") {
        var snackbar = document.getElementById("snackBar");
        snackbar.innerHTML = "Please enter OTP sent on your registered email";
        snackbar.style.backgroundColor = "darkred";
        snackbar.className = "show";
        setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
    } else {
        createModal(a,otpStatus);
        showModal();
    }
}

function loadTable(auctionData) {
    var auctionNo, date, startTime, lotDetails, termsLink, termsFlag;

    for (var i = 0; i < auctionData.auctionList.length; i++) {
        //var row = table.insertRow(i);
        auctionNo = auctionData.auctionList[i].AuctionNumber;
        date = auctionData.auctionList[i].Date;
        startTime = auctionData.auctionList[i].StartTime;
        lotDetails = auctionData.auctionList[i].Catalogue;
        termsLink = auctionData.auctionList[i].termsLink;
        termsFlag = auctionData.auctionList[i].termsFlag;
        var auction = new Auction(auctionNo, date, startTime, lotDetails, termsLink, termsFlag);
        auctions.push(auction);
    }
    
    $("#loading-wrapper").hide();
}

// function exportTableToExcel(tableID, filename) {
//     var downloadLink;
//     var dataType = 'application/vnd.ms-excel';
//     var tableSelect = document.getElementById(tableID);
//     var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

//     // Specify file name
//     filename = filename ? filename + '.xls' : 'excel_data.xls';

//     // Create download link element
//     downloadLink = document.createElement("a");

//     document.body.appendChild(downloadLink);

//     if (navigator.msSaveOrOpenBlob) {
//         var blob = new Blob(['\ufeff', tableHTML], {
//             type: dataType
//         });
//         navigator.msSaveOrOpenBlob(blob, filename);
//     } else {
//         // Create a link to the file
//         downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

//         // Setting the file name
//         downloadLink.download = filename;

//         //triggering the function
//         downloadLink.click();
//     }
// }


