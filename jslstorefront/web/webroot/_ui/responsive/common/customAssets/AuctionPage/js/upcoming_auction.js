var selectedLots = [];
var seriesFlag = false;
var gradeFlag = false;
function showLotDetails(id) {
	$(id).toggle();
}

function selectToggle(a) {
    var checkBox = document.getElementById("sel_" + a);
    if ($(checkBox).is(":checked")) {
        selectedLots.push(a);
    } else {
        for (var i = 0; i < selectedLots.length; i++) {
            if (selectedLots[i] === a) {
                selectedLots.splice(i, 1);
            }
        }
    }

}

function getLotData() {
    var auctionJson = [];
    var mainTable = $(document).find("#myTable");
    var $headers = mainTable.find("th");
    var auctionRows = document.getElementsByClassName("auctionRow");
    var lotTables = document.getElementsByClassName("lotsTable");
    for(var x = 0; x < auctionRows.length; x++){ 
    	var $auctionCells = $(auctionRows[x]).find("td");
    	auctionJson[x] = {};
    	for(var y = 0; y < $auctionCells.length; y++){
    		auctionJson[x][$($headers[y]).html()] = $($auctionCells[y]).text();
    	}
    	var lotData = [];
    	var $lotRows = $(lotTables[x]).find(".lotRow");
    	var $lotHeaders = $(lotTables[x]).find("th");
    	var prodTables = $(lotTables[x]).find(".prods");
	    for (var i = 0; i < $lotRows.length; i++) {
	        var $cells = $($lotRows[i]).find("td");
	        lotData[i] = {};
	        for (var k = 0; k < $cells.length; k++) {
	            lotData[i][$($lotHeaders[k]).html()] = $($cells[k]).text();
	        }
	        var prodDetails = [];
	        var $prodRows = $(prodTables[i]).find(".prodRow");
	        var $prodHeaders = $(prodTables[i]).find("th");
	        for (var j = 0; j < $prodRows.length; j++) {
	            var $prodCells = $($prodRows[j]).find("td");
	            prodDetails[j] = {};
	            for (var l = 0; l < $prodCells.length; l++) {
	                prodDetails[j][$($prodHeaders[l]).html()] = $($prodCells[l]).text();
	            }
	        }
	        lotData[i].prodDetails = prodDetails;
	    }
	    auctionJson[x].lotData = lotData;
	}
    var myObj = {};
    myObj.bidEntries = auctionJson;
    console.log(JSON.parse(JSON.stringify(myObj).replace(/\s/g, "")));
    
    return JSON.parse(JSON.stringify(myObj).replace(/\s/g, ""));
}

function selectSeriesFunction() {
    var checkBox = document.getElementById("serFlter");
    //var text = document.getElementById("seriesFilter");
    var selectedSeries = document.getElementById("seriesFilter");
    selectedSeriesValue = selectedSeries.options[selectedSeries.selectedIndex].text;
    // alert(selectedSeriesValue);
    if (checkBox.checked == true) {
        selectedSeries.style.display = "block";
        seriesFlag = true;
    } else {
        selectedSeries.text = "All";
        selectedSeries.style.display = "none";
        seriesFlag = false;
    }

    if (document.getElementById("serFlter").checked === false) {
        document.getElementById("all").selected = true;
    }
    if (document.getElementById("serFlter").checked === false && document.getElementById("grdFlter").checked === true) {
        document.getElementById("gradesListA").style.display = "none";
        document.getElementById("gradesListB").style.display = "none";
        document.getElementById("gradesListC").style.display = "none";
        document.getElementById("gradesListD").style.display = "none";
        document.getElementById("gradesListAll").style.display = "block";
    }
}

function displayGrades() {
    var selectedSeries = document.getElementById("seriesFilter");
    selectedSeriesValue = selectedSeries.options[selectedSeries.selectedIndex].text;
    // var checkBox = document.getElementById("grdFlter");
    selectGradeFunction();
}
var seriesListSelected;
var seriesIndexes = [],
    productDetailSeriesIndex = [],
    seriesHiddenIndexes=[];
var selectedGradeList = [];
var gradeIndexData = [];
var gradeListSelected = [];
var gradeIndexAll =[];
var seriesFlag =false;
$('section form div input ').change(function() {
    console.log($(this).val());
    gradeListSelected.length = 0;
    $.each($("input:checked"), function(i) {
        if ($(this).val() !== "on" && $(this).val() !== "all" && gradeListSelected.includes($(this).val()) === false) {
            gradeListSelected.unshift($(this).val());
        }
    });
    console.log(gradeListSelected);
  
    
    for (var i = 0; i < lotDetails.bidEntries.length; i++) {
       
        for (var z = 0; z < lotDetails.bidEntries[i].lotData.length; z++) {
        	lotDetails.bidEntries[i].lotData[z].display =[];
        	gradeIndexAll[i] = new Array(z);
        }
        
    }
    for (var i = 0; i < lotDetails.bidEntries.length; i++) {
    	
        
        	for (var z = 0; z < lotDetails.bidEntries[i].lotData.length; z++) {
        		for (var j = 0; j < gradeListSelected.length; j++) {
            for (var k = 0; k < lotDetails.bidEntries[i].lotData[z].prodDetails.length; k++) {
            	
                if ((lotDetails.bidEntries[i].lotData[z].prodDetails[k].Grade).toLowerCase().trim().replace(" ", "").match(gradeListSelected[j].toLowerCase().trim().replace(" ", "")) || (gradeListSelected[j]).toLowerCase().trim().replace(" ", "").match((lotDetails.bidEntries[i].lotData[z].prodDetails[k].Grade).toLowerCase().trim().replace(" ", ""))) {;
                
                gradeIndexAll[i][z] ="show";
                lotDetails.bidEntries[i].lotData[z].display[j] ="show";
                 
                }
                /*else{
                	gradeIndexAll[i][z] ="hide";
                	lotDetails.bidEntries[i].lotData[z].display[i] ="hide";
                
                	
                }*/
            }
            
        	}
        }

    }

//    for (var i = 0; i < lotDetails.bidEntries.length; i++) {
//        var tr = document.getElementById("row_" + i);
//        for (var j = 0; j < gradeIndexData.length; j++) {
//            var flag = false;
//            for (var k = 0; k < gradeIndexData[j].length; k++) {
//                if (gradeIndexData[j][k].length !== 0) {
//                    if (selectedGradeList.includes(j) === false) {
//                        selectedGradeList.push(j);
//                    } else {
//                        selectedGradeList.splice(j, 1)
//                    }
//
//                }
//            }
//        }
//
//    }
});

$('form select').change(function() {
    console.log($(this).val());
    gradeIndexData.length = 0;
    gradeListSelected.length = 0;
    seriesIndexes = [];
    $.each($("div input:checked"), function(i) {
        document.getElementById('gradeBoxALL_' + i).checked = false;
    });
    $.each($("div input:checked"), function(i) {
        document.getElementById('gradeBoxA_' + i).checked = false;
    });
    $.each($("div input:checked"), function(i) {
        document.getElementById('gradeBoxB_' + i).checked = false;
    });
    $.each($("div input:checked"), function(i) {
        document.getElementById('gradeBoxC_' + i).checked = false;
    });
    $.each($("div input:checked"), function(i) {
        document.getElementById('gradeBoxD_' + i).checked = false;
    });

    for (var i = 0; i < lotDetails.bidEntries.length; i++) {
        
        for (var z = 0; z < lotDetails.bidEntries[i].lotData.length; z++) {
        	lotDetails.bidEntries[i].lotData[z].displaySeries ="";
        }
        
    }
    var seriesListSelected = $(this).val();
    for (var i = 0; i < lotDetails.bidEntries.length; i++) {
    	for (var z = 0; z < lotDetails.bidEntries[i].lotData.length; z++) {
        for (var j = 0; j < lotDetails.bidEntries[i].lotData[z].prodDetails.length; j++) {
            if (lotDetails.bidEntries[i].lotData[z].prodDetails[j].Series.toLowerCase().replace(" ", "") == seriesListSelected.toLowerCase().replace(" ", "")) {
            	seriesIndexes.push([i,z]);
            	lotDetails.bidEntries[i].lotData[z].displaySeries ="show";
            	break;
        }
            else{
            	seriesHiddenIndexes.push([i,z]);
            	lotDetails.bidEntries[i].lotData[z].displaySeries ="hide";
            	break;
            }
    	}
    }
    }

    /*if (document.getElementById("seriesFilter").checked === false || document.getElementById("all").selected === true) {
        for (var i = 0; i < lotDetails.bidEntries.length; i++) {
            document.getElementById("row_" + i).style.display = "";
            document.getElementById("accordion_" + i).className = "collapse";
        }
    }*/
});


function filterTable() {
    event.preventDefault();
    if (document.getElementById("serFlter").checked === true) {
    	for (var i = 0; i < lotDetails.bidEntries.length; i++) {
        	for (var z = 0; z < lotDetails.bidEntries[i].lotData.length; z++) {
       		if (lotDetails.bidEntries[i].lotData[z].displaySeries == "hide"  ) {
                    document.getElementById("row_" + i + z).style.display = "none";
                    if(document.getElementById("accordion_" + i + z) != null){
                    	document.getElementById("accordion_" + i + z).style.display = "none";
                    }
                   /* if(document.getElementById("accordion_" + i) != null){
                    	document.getElementById("accordion_" + i).style.display = "none";
                    }*/                   
                } 
                else if(lotDetails.bidEntries[i].lotData[z].displaySeries == "show"){
                	document.getElementById("row_" +i + z).style.display = "";
                	 if(document.getElementById("accordion_" + i + z) != null){
                     	document.getElementById("accordion_" + i + z).style.display = "none";
                     }
                    /* if(document.getElementById("accordion_" + i) != null){
                     	document.getElementById("accordion_" + i).style.display = "none";
                     } */
                }
        }
        }
    	/*for (var i = 0; i < lotDetails.bidEntries.length; i++) {
    		for (var si = 0; si < seriesIndexes.length; si++) {
    			 if (seriesHiddenIndexes[si][0] == i && seriesHiddenIndexes[si][1] == null){
    				 document.getElementById("auctionRow_" + i).style.display = "none";
    			 }
    		}
    		
    		
    	}
        for (var i = 0; i < lotDetails.bidEntries.length; i++) {
        	for (var si = 0; si < seriesIndexes.length; si++) {
        	for (var z = 0; z < lotDetails.bidEntries[i].lotData.length; z++) {
            if ( seriesHiddenIndexes[si][1] == z ) {
                document.getElementById("row_" + i + z).style.display = "none";
                document.getElementById("accordion_0" + i).className = "collapse";
            } 
        }
        }
        }*/
        /*if (document.getElementById("all").selected === true) {
            for (var i = 0; i < lotDetails.bidEntries.length; i++) {
                document.getElementById("row_" + i).style.display = "";
                document.getElementById("accordion_" + i).className = "collapse";

            }
        }*/
    }
    if (document.getElementById("grdFlter").checked == true) {
//    	for (var i = 0; i < lotDetails.bidEntries.length; i++) {
//    		for (var gi = 0; gi < gradeHiddenIndexes.length; gi++) {
//    			 if (gradeHiddenIndexes[gi][0] == i && gradeHiddenIndexes[gi][1] == null){
//    				 document.getElementById("auctionRow_" + i).style.display = "none";
//    			 }
//    		}
//    		
//    		
//    	}
//    	if (document.querySelectorAll('input.gradeBox:checked').length === 0) {
//        	var snackbar = document.getElementById("snackBar");
//            snackbar.innerHTML = "Please Select atleast one grade";
//            snackbar.style.backgroundColor = "red";
//            snackbar.className = "show";
//            setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
//        }
    	
    	for (var i = 0; i < lotDetails.bidEntries.length; i++) {
        	for (var z = 0; z < lotDetails.bidEntries[i].lotData.length; z++) {
        		
        		if(gradeIndexAll[i][z] == "show"){
                	document.getElementById("row_" +i + z).style.display = "";
                	if(document.getElementById("accordion_" + i + z) != null){
                     	document.getElementById("accordion_" + i + z).style.display = "none";
                     }
//                     if(document.getElementById("accordion_" + i) != null){
//                     	document.getElementById("accordion_" + i).style.display = "none";
//                     } 
                }
        		else{
        			document.getElementById("row_" + i + z).style.display = "none";
                    if(document.getElementById("accordion_" + i + z) != null){
                     	document.getElementById("accordion_" + i + z).style.display = "none";
                     }
//                     if(document.getElementById("accordion_" + i) != null){
//                     	document.getElementById("accordion_" + i).style.display = "none";
//                     } 
        		}
       		/*if (lotDetails.bidEntries[i].lotData[z].display.contains("")) {
                    document.getElementById("row_" + i + z).style.display = "none";
                    if(document.getElementById("accordion_" + i + z) != null){
                     	document.getElementById("accordion_" + i + z).style.display = "none";
                     }
                     if(document.getElementById("accordion_" + i) != null){
                     	document.getElementById("accordion_" + i).style.display = "none";
                     } 
                    
                } 
                 if(lotDetails.bidEntries[i].lotData[z].display.contains("show")){
                	document.getElementById("row_" +i + z).style.display = "";
                	if(document.getElementById("accordion_" + i + z) != null){
                     	document.getElementById("accordion_" + i + z).style.display = "none";
                     }
                     if(document.getElementById("accordion_" + i) != null){
                     	document.getElementById("accordion_" + i).style.display = "none";
                     } 
                }*/
        }
        }
//        if (document.querySelectorAll('input.gradeBox:checked').length === 0) {
//            for (var i = 0; i < lotDetails.bidEntries.length; i++) {
//                document.getElementById("row_" + i).style.display = "";
//                document.getElementById("accordion_" + i).className = "collapse";
//            }
//        }
    }
//        for (var i = 0; i < lotDetails.bidEntries.length; i++) {
//        	for (var z = 0; z < lotDetails.bidEntries[i].lotData.length; z++) {
//        	for (var gi = 0; gi < gradeAllIndexes.length; gi++) {
//        	
//        		
////            if ( gradeHiddenIndexes[gi][0]== i && gradeHiddenIndexes[gi][1] == z ) {
////            	console.log(i + "true" + z );
////                document.getElementById("row_" + z).style.display = "none";
////               
//////                document.getElementById("accordion_0" + i).className = "collapse";
////            } 
//        		
//        		if (gradeAllIndexes[gi][0] == i && gradeAllIndexes[gi][2] == "hide" ) {
//                    document.getElementById("row_" + z).style.display = "none";
//                   
//                   
////                    document.getElementById("accordion_0" + i).className = "collapse";
//                } 
//                else if(gradeAllIndexes[gi][0] == i && gradeAllIndexes[gi][2] == "show"){
//                	document.getElementById("row_" + z).style.display = "";
//                }
////        		if ( gradeHiddenIndexes[gi][2] == "hide" ) {
////                    document.getElementById("row_" + z).style.display = "none";
////                   
//////                    document.getElementById("accordion_0" + i).className = "collapse";
////                } 
////                else {
////                	document.getElementById("row_" + z).style.display = "";
////                	break;
////                }
////            if ( gradeHiddenIndexes[gi][2] == "hide" ) {
////                document.getElementById("row_" + z).style.display = "none";
////               
//////                document.getElementById("accordion_0" + i).className = "collapse";
////            } 
////            else {
////            	document.getElementById("row_" + z).style.display = "";
////            	break;
////            }
//           
//        }
//        }
//        }
////        if (document.querySelectorAll('input.gradeBox:checked').length === 0) {
////            for (var i = 0; i < lotDetails.bidEntries.length; i++) {
////                document.getElementById("row_" + i).style.display = "";
////                document.getElementById("accordion_" + i).className = "collapse";
////            }
////        }
//    }
    
    if (document.getElementById("grdFlter").checked == false  && document.getElementById("serFlter").checked == false) {
    	var snackbar = document.getElementById("snackBar");
    	snackbar.innerHTML = "Please select at least one Series or Grade Group";
        snackbar.style.backgroundColor = "red";
        snackbar.className = "show";
        setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
    }
    
    else{
    	var snackbar = document.getElementById("snackBar");
    	snackbar.innerHTML = "Auctions Filtered";
        snackbar.style.backgroundColor = "green";
        snackbar.className = "show";
        setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
    }
    
    if (document.getElementById("grdFlter").checked == true) {
    	if (document.querySelectorAll('input.gradeBox:checked').length === 0) {
        	var snackbar = document.getElementById("snackBar");
            snackbar.innerHTML = "Please Select atleast one Grade Group";
            snackbar.style.backgroundColor = "red";
            snackbar.className = "show";
            setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
            for (var i = 0; i < lotDetails.bidEntries.length; i++) {
            	for (var z = 0; z < lotDetails.bidEntries[i].lotData.length; z++) {
           		if (lotDetails.bidEntries[i].lotData[z].displaySeries == "hide"  ) {
                        document.getElementById("row_" + i + z).style.display = "none";
                        if(document.getElementById("accordion_" + i + z) != null){
                        	document.getElementById("accordion_" + i + z).style.display = "none";
                        }
                       /* if(document.getElementById("accordion_" + i) != null){
                        	document.getElementById("accordion_" + i).style.display = "none";
                        }*/                   
                    } 
                    else if(lotDetails.bidEntries[i].lotData[z].displaySeries == "show"){
                    	document.getElementById("row_" +i + z).style.display = "";
                    	 if(document.getElementById("accordion_" + i + z) != null){
                         	document.getElementById("accordion_" + i + z).style.display = "none";
                         }
                        /* if(document.getElementById("accordion_" + i) != null){
                         	document.getElementById("accordion_" + i).style.display = "none";
                         } */
                    }
            }
            }
        }
    }
    if (document.getElementById("grdFlter").checked == true && document.getElementById("serFlter").checked === false) {
    	if (document.querySelectorAll('input.gradeBox:checked').length === 0) {
    		for (var i = 0; i < lotDetails.bidEntries.length; i++) {
    	    	for (var z = 0; z < lotDetails.bidEntries[i].lotData.length; z++) {
    	                document.getElementById("row_" + i + z).style.display = "";
    	                if(document.getElementById("accordion_" + i + z) != null){
    	                 	document.getElementById("accordion_" + i + z).style.display = "none";
    	                 }
    	                 if(document.getElementById("accordion_" + i) != null){
    	                 	document.getElementById("accordion_" + i).style.display = "none";
    	                 } 
    	                 $("productDetails_" + i + z).css("display", "none");
    	            } 
    		}
    	}
    	}
}

function resetTable() {
	event.preventDefault();
	selectedSeriesValue = 'All';
	for (var i = 0; i < lotDetails.bidEntries.length; i++) {
    	for (var z = 0; z < lotDetails.bidEntries[i].lotData.length; z++) {
                document.getElementById("row_" + i + z).style.display = "";
                if(document.getElementById("accordion_" + i + z) != null){
                 	document.getElementById("accordion_" + i + z).style.display = "none";
                 }
                 if(document.getElementById("accordion_" + i) != null){
                 	document.getElementById("accordion_" + i).style.display = "none";
                 } 
                 $("productDetails_" + i + z).css("display", "none");
            } 
	}
	$.each($("div input:checked"), function(i) {
        
        if(document.getElementById('gradeBoxALL_' + i) !== null){
        	document.getElementById('gradeBoxALL_' + i).checked = false;
    	}
    });
    $.each($("div input:checked"), function(i) {
        if(document.getElementById('gradeBoxA_' + i) !== null){
        	document.getElementById('gradeBoxA_' + i).checked = false;
    	}
    });
    $.each($("div input:checked"), function(i) {
        if(document.getElementById('gradeBoxB_' + i) !== null){
        	document.getElementById('gradeBoxB_' + i).checked = false;
    	}
    });
    $.each($("div input:checked"), function(i) {
        if(document.getElementById('gradeBoxC_' + i) !== null){
        	document.getElementById('gradeBoxC_' + i).checked = false;
    	}
    });
    $.each($("div input:checked"), function(i) {
    	if(document.getElementById('gradeBoxD_' + i) !== null){
    		document.getElementById('gradeBoxD_' + i).checked = false;
    	}
    });
	 document.getElementById("seriesFilter").style.display="none";
	    document.getElementById('serFlter').checked = false;
		document.getElementById('grdFlter').checked = false;
		document.getElementById("gradesListA").style.display = "none";
	    document.getElementById("gradesListB").style.display = "none";
	    document.getElementById("gradesListC").style.display = "none";
	    document.getElementById("gradesListD").style.display = "none";
	    document.getElementById("gradesListAll").style.display = "none";
    document.getElementById("all").selected = true;
    var snackbar = document.getElementById("snackBar");
    snackbar.innerHTML = "Auctions Reset";
    snackbar.style.backgroundColor = "green";
    snackbar.className = "show";
    setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
}


function selectGradeFunction() {
    var checkBox = document.getElementById("grdFlter");
    // var selectedGrades = document.getElementById("gradesList");
    //selectedGradeValue = selectedGrades.options[selectedGrades.selectedIndex].text;
    //alert(selectedGradeValue); 400 Series
    if (document.getElementById("grdFlter").checked === false) {
    	 
    	    $.each($("div input"), function(i) {
    	        if(document.getElementById('gradeBoxA_' + i) !== null){
    	        	document.getElementById('gradeBoxA_' + i).checked = false;
    	    	}
    	    });
    	    $.each($("div input"), function(i) {
    	        if(document.getElementById('gradeBoxB_' + i) !== null){
    	        	document.getElementById('gradeBoxB_' + i).checked = false;
    	    	}
    	    });
    	    $.each($("div input"), function(i) {
    	        if(document.getElementById('gradeBoxC_' + i) !== null){
    	        	document.getElementById('gradeBoxC_' + i).checked = false;
    	    	}
    	    });
    	    $.each($("div input"), function(i) {
    	    	if(document.getElementById('gradeBoxD_' + i) !== null){
    	    		document.getElementById('gradeBoxD_' + i).checked = false;
    	    	}
    	    });
    	    $.each($("div input"), function(i) {
    	        if(document.getElementById('gradeBoxALL_' + i) !== null){
    	        	document.getElementById('gradeBoxALL_' + i).checked = false;
    	    	}
    	    });
//        for (var i = 0; i < lotDetails.bidEntries.length; i++) {
//            document.getElementById("row_" + i).style.display = "";
//
//        }
    }
    if (seriesFlag === true) {
        console.log(selectedSeriesValue);
        if (selectedSeriesValue === '200 Series') {
            if (checkBox.checked == true) {
                document.getElementById("gradesListA").style.display = "block";
                document.getElementById("gradesListB").style.display = "none";
                document.getElementById("gradesListC").style.display = "none";
                document.getElementById("gradesListD").style.display = "none";
                document.getElementById("gradesListAll").style.display = "none";
                gradeFlag = true;
            } else {
                document.getElementById("gradesListA").style.display = "none";
                gradeFlag = false;
            }
        }
        if (selectedSeriesValue === '300 Series') {
            if (checkBox.checked == true) {
                document.getElementById("gradesListB").style.display = "block";
                document.getElementById("gradesListA").style.display = "none";
                document.getElementById("gradesListC").style.display = "none";
                document.getElementById("gradesListD").style.display = "none";
                document.getElementById("gradesListAll").style.display = "none";
                gradeFlag = true;
            } else {
                document.getElementById("gradesListB").style.display = "none";
                gradeFlag = false;
            }
        }
        if (selectedSeriesValue === '400 Series') {
            if (checkBox.checked == true) {
                document.getElementById("gradesListC").style.display = "block";
                document.getElementById("gradesListA").style.display = "none";
                document.getElementById("gradesListB").style.display = "none";
                document.getElementById("gradesListD").style.display = "none";
                document.getElementById("gradesListAll").style.display = "none";
                gradeFlag = true;
            } else {
                document.getElementById("gradesListC").style.display = "none";
                gradeFlag = false;
            }
        }
        if (selectedSeriesValue === 'Duplex') {
            if (checkBox.checked == true) {
                document.getElementById("gradesListD").style.display = "block";
                document.getElementById("gradesListA").style.display = "none";
                document.getElementById("gradesListB").style.display = "none";
                document.getElementById("gradesListC").style.display = "none";
                document.getElementById("gradesListAll").style.display = "none";
                gradeFlag = true;
            } else {
                document.getElementById("gradesListD").style.display = "none";
                gradeFlag = false;
            }
        }
        if (selectedSeriesValue === 'All') {
            if (checkBox.checked == true) {
                document.getElementById("gradesListAll").style.display = "block";
                document.getElementById("gradesListA").style.display = "none";
                document.getElementById("gradesListB").style.display = "none";
                document.getElementById("gradesListC").style.display = "none";
                document.getElementById("gradesListD").style.display = "none";
                gradeFlag = true;
            } else {
                document.getElementById("gradesListAll").style.display = "none";
                gradeFlag = false;
            }
        }

    }
    if (seriesFlag !== true) {
        if (checkBox.checked == true) {
            document.getElementById("gradesListAll").style.display = "block";
            gradeFlag = true;
        } else {
            document.getElementById("gradesListAll").style.display = "none";
            gradeFlag = false;
        }

    }
}


$('#watchList').click(function() {
    var jslLots = [];
    if (selectedLots.length === 0) {
        var snackbar = document.getElementById("snackBar");
        snackbar.innerHTML = "Please select atleast one lot";
        snackbar.style.backgroundColor = "darkred";
        snackbar.className = "show";
        setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
    } else {
        for (var j = 0; j < selectedLots.length; j++) {
            var addedLots = {
            	"bundleTemplateId": document.getElementById("lotNo_" + selectedLots[j].toString()).value,
            	"auctionEventId": (document.getElementById("auctionId_" + selectedLots[j].toString().slice(0, 1)).innerHTML).trim()
            };
            jslLots.push(addedLots);
        }
        $.ajax({
    		type:   "POST",
    		url:   ACC.config.encodedContextPath +"/auction/addToWatchList?CSRFToken="+ACC.config.CSRFToken,
    		data: JSON.stringify(jslLots),
    		contentType:   "application/json; charset=utf-8",
    		dataType:   "json",
    		success:   function (response) {
    			if(response == 112){
 					var snackbar = document.getElementById("snackBar");
 					snackbar.innerHTML = "Auction is closed";
 					snackbar.style.backgroundColor = "darkred";
 					snackbar.className = "show";
                     setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
 				} else if(response == 0) {
    				var snackbar = document.getElementById("snackBar");
    				snackbar.innerHTML = "Failed to add in watch list";
    				snackbar.style.backgroundColor = "darkred";
    				snackbar.className = "show";
    				setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
    			} else if(response == 111) {
    				var snackbar = document.getElementById("snackBar");
    				snackbar.innerHTML = "Already in watch list";
    				snackbar.style.backgroundColor = "darkorange";
    				snackbar.className = "show";
    				setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
    				$("#watchlist_auction").removeClass("isDisabled");
    	            document.getElementById("watchlist_auction").href = ACC.config.encodedContextPath +"/auction/watchList?auctionID="+jslLots[0].auctionEventId;
    			} else {
    				var snackbar = document.getElementById("snackBar");
    				var lotNumber = [];
					if (jslLots.length == 1) {
						lotNumber.push($(".lot_"+jslLots[0].bundleTemplateId).text());
					} else {
						for (var i = 0; i < selectedLots.length; i++) {
							lotNumber.push($(".lot_"+jslLots[i].bundleTemplateId).text());
						}
					} 
    				snackbar.innerHTML = lotNumber.join()+ "succesfully added in watch list";
    				snackbar.style.backgroundColor = "darkgreen";
    				snackbar.className = "show";
    				setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
    				$("#watchlist_auction").removeClass("isDisabled");
    	            document.getElementById("watchlist_auction").href = ACC.config.encodedContextPath +"/auction/watchList?auctionID="+jslLots[0].auctionEventId;
    	            lotNumber = [];
    			}
    			jslLots = [];
    		},
    		error: function (response) {
    			var snackbar = document.getElementById("snackBar");
    			snackbar.innerHTML = "Failed to add in watch list";
    			snackbar.style.backgroundColor = "darkred";
    			snackbar.className = "show";
    			setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
    		}
    	});
    }
});

$(document).ready(function() {
	lotDetails = getLotData();
	getOtpStatus();
	$("#loading-wrapper").hide();
});

function getOtpStatus(){
	localStorage.removeItem("optionsAuctionUp");
	var auctionId = [];
	var otpAcceptedForAuction = document.getElementsByClassName("otpAccepted");
	for (var index = 0; index < otpAcceptedForAuction.length; ++index) {
		auctionId.push(otpAcceptedForAuction[index].value);
	}
	if(!$.isEmptyObject(auctionId)) {
		localStorage.setItem("optionsAuctionUp", JSON.stringify(auctionId));
	}
}

function openModalUpcoming(a,otpStatus) {
    if (document.getElementById("otp_" + a).value === "" && otpStatus === "pending") {
        var snackbar = document.getElementById("snackBar");
        snackbar.innerHTML = "Please enter OTP sent on your registered email";
        snackbar.style.backgroundColor = "darkred";
        snackbar.className = "show";
        setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
    } else {
    	createModalUpcoming(a,otpStatus);
        showModal();
    }
}

function createModalUpcoming(a, otpStatus) {
    document.getElementById("modalAuctionNo0").innerHTML = document.getElementById("auctionId_" + a).innerHTML;
    if(otpStatus === "pending") {
    	$("#otpStausPending").show();
    	$("#tncPending").show();
    	$("#tnc_accept").show();
    	$("#tncAccepted").hide();
    	
	    var checker = document.getElementById("tnc_check");
	    checker.checked = false;
	    var accept_btn = document.getElementById("tnc_accept");
	    accept_btn.disabled = true;
	    accept_btn.onclick = function(arg) { return function() { acceptLogUpcoming(arg); } }(a);
	    checker.onchange = function() {
	        accept_btn.disabled = !this.checked;
	    }
    } else {
    	$("#otpStausPending").hide();
    	$("#tncPending").hide();
    	$("#tnc_accept").hide();
    	$("#tncAccepted").show();
    }
}

function acceptLogUpcoming(a) {
	hideModalUpcoming();
    document.getElementById("auctionId_" + a).title = "";
    var otpData = {
        auctionID: document.getElementById("auctionId_" + a).innerHTML,
        otp: document.getElementById("otp_" + a).value
    };
    var auctionID = document.getElementById("auctionId_" + a).innerHTML;
    var otp = document.getElementById("otp_" + a).value;
    $.ajax({
        type: "POST",
        url: ACC.config.encodedContextPath +"/auction/validateOTP?auctionId="+auctionID+"&otp="+otp+"&CSRFToken="+ACC.config.CSRFToken,
       /* data: otpData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",*/
        success: function (response) {
        	var otpResponse = JSON.parse(response);
        	if(otpResponse.isValid == "true") {
	        	var snackbar = document.getElementById("snackBar");
	            snackbar.innerHTML = "Terms & Conditions Accepted";
	            snackbar.style.backgroundColor = "green";
	            snackbar.className = "show";
	            setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
//	            auctions[a].termsFlag = true;
	            document.getElementById("acptBtn_" + a).innerHTML = "Show T&C  <i class=\"fa fa-check-square\" style=\"font-size: 20px;color:green;\"></i>";
	            document.getElementById("acptBtn_" + a).onclick = function(arg) { return function() { openModalUpcoming(arg,'accepted'); } }(a);
//	            document.getElementById("auctionNum_" + a).classList.remove("tooltip");
//	            document.getElementById("auctionNum_" + a).href = ACC.config.encodedContextPath +"/auction/liveAuction?auctionID="+auctionID;
//	            document.getElementById("auctionId_" + a).classList.add("isEnabled");
	            document.getElementById("auctionId_" + a).classList.remove("isDisabled");
	            
//	            document.getElementById("view_catlgue_link_" + a).classList.add("isEnabled");
	            document.getElementById("view_catlgue_link_" + a).classList.remove("isDisabled");
	            if(localStorage.getItem("optionsAuctionUp") != null){
	            	var auctionsLocalData = localStorage.getItem("optionsAuctionUp");
	            	auctionsLocalData.push(JSON.stringify(Number(lotDetails.bidEntries[a].AuctionNumber.replace(/\D/g,''))));
	            	localStorage.setItem("optionsAuctionUp",JSON.stringify(auctionsLocalData));
	            }else{
	            	var auctionsLocalData = [];
	            	auctionsLocalData.push(JSON.stringify(Number(lotDetails.bidEntries[a].AuctionNumber.replace(/\D/g,''))));
	            	localStorage.setItem("optionsAuctionUp",JSON.stringify(auctionsLocalData));
	            }
	            document.getElementById("watchlist_auction").classList.remove("isDisabled");
	            
//	            localStorage.setItem("JSLLocalStorage", true);

        	} else if(otpResponse.isValid == "false" && otpResponse.reason =="expired") {
        		var snackbar = document.getElementById("snackBar");
	            snackbar.innerHTML = "OTP expired";
	            snackbar.style.backgroundColor = "red";
	            snackbar.className = "show";
	            setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
        	} else if(otpResponse.isValid == "false"  && otpResponse.reason =="notfound") {
        		var snackbar = document.getElementById("snackBar");
	            snackbar.innerHTML = "Please enter valid OTP";
	            snackbar.style.backgroundColor = "red";
	            snackbar.className = "show";
	            setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
        	}else if(otpResponse.isValid == "false"  && otpResponse.reason =="invalid") {
        		var snackbar = document.getElementById("snackBar");
	            snackbar.innerHTML = "Please enter valid OTP";
	            snackbar.style.backgroundColor = "darkred";
	            snackbar.className = "show";
	            setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
        	}
            
        },
        error: function(response) {
        	var snackbar = document.getElementById("snackBar");
            snackbar.innerHTML = "Sorry something went wrong";
            snackbar.style.backgroundColor = "red";
            snackbar.className = "show";
            setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
        }
    });

    //localStorage.setItem("JSLLocalStorage", true);
}

function showModalUpcoming() {
    document.getElementById("myModal").style.display = "block";
}

function hideModalUpcoming() {
    document.getElementById("myModal").style.display = "none";
}



