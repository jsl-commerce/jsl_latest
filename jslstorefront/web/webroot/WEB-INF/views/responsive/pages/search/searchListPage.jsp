<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/responsive/storepickup" %>

<template:page pageTitle="${pageTitle}">

<div id="loading-wrapper" style="display: none;">
    <div class="spinner">
  <div class="rect1"></div>
  <div class="rect2"></div>
  <div class="rect3"></div>
  <div class="rect4"></div>
  <div class="rect5"></div>
  <div class="rect6"></div>

  </div>
</div>

	
		<div class="searchLeftDiv">
			<cms:pageSlot position="ProductLeftRefinements" var="feature" element="div" class="search-list-page-left-refinements-slot">
				<cms:component component="${feature}" element="div" class="search-list-page-left-refinements-component"/>
			</cms:pageSlot>
		</div>
		<div class="searchDetailsDiv">
			<cms:pageSlot position="SearchResultsListSlot" var="feature" element="div" class="search-list-page-right-result-list-slot">
				<cms:component component="${feature}" element="div" class="search-list-page-right-result-list-component"/>
			</cms:pageSlot>
        </div>
	</div>
	<div id="plant_warehouse_message" class="alert alert-success"></div>
	<div id="updatedMessageCart" class="alert alert-success"></div>
	<div id="showbuyErrorModel" class="showErrorMessage">

	<storepickup:pickupStorePopup />

</template:page>