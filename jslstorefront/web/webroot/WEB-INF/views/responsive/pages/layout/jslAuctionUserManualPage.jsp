<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
-->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="${commonResourcePath}/customAssets/AuctionPage/css/user-manual.css"/>

<div id="loading-wrapper">
     <div class="spinner">
				  <div class="rect1"></div>
				  <div class="rect2"></div>
				  <div class="rect3"></div>
				  <div class="rect4"></div>
				  <div class="rect5"></div>
				  <div class="rect6"></div>
					
		  </div>
</div>
<span class="sidebarclose"><i class="lni-construction-hammer"></i></span>
     <div class="inner">
        <input type="hidden" id="contextPath" name="contextPath" value="${pageContext.request.contextPath}">
        <input type="hidden" id="auctionPageName" name="auctionPageName" value="auctionUserManual">
        <input type="hidden" id="CSRFToken"	name="CSRFToken" value /> 
        
            <article class="box post post-excerpt">
            
            <div class="container-fluid" id ="downloadVideos">
            	<h1>
                        <i class="fa fa fa-play-circle"> Help Videos</i><br><br>
                        <b>Auction Process :</b>  <a href="${commonResourcePath}/customAssets/AuctionPage/docs/Auction Bidding Process Manual.mp4" target="_blank">Play Video</a>
                         <a href="${commonResourcePath}/customAssets/AuctionPage/docs/Auction Bidding Process Manual.mp4" download="Auction_Process"><i class="fa fa-download"></i> Download</a><br>
                        <b> Change & Reset Passwords :</b>  <a href="${commonResourcePath}/customAssets/AuctionPage/docs/Login_Change_and_Reset Password.mp4" target="_blank">Play Video</a>
                         <a href="${commonResourcePath}/customAssets/AuctionPage/docs/Login_Change_and_Reset Password.mp4" download="Login_Change_and_Reset_Password"><i class="fa fa-download"></i> Download</a>
                </h1>
                
            </div>
                <div class="container-fluid" id="downloadPdf">
                    <h1>
                        <i class="fa fa-file-pdf-o"> JSL Auction-Terms & Conditions</i>
                    </h1>
            
                       <b>User Manual :</b> <a href="${commonResourcePath}/customAssets/AuctionPage/docs/CustomerGuide.pdf" target="_blank">View</a>
                        <a href="${commonResourcePath}/customAssets/AuctionPage/docs/CustomerGuide.pdf" download="UserManual"><i class="fa fa-download"></i> Download</a><br>
                        
                        <b>Freight Charges for Jajpur :</b><a style="cursor:pointer;" onclick="getFreightCharges('JSL')"><i class="fa fa-download"></i> Download </a><br>
                        <b>Freight Charges for Hisar :</b><a style="cursor:pointer;" onclick="getFreightCharges('JSHL')"><i class="fa fa-download"></i> Download </a><br>
           
                        <b>Non Prime Quality Complaint SOP - Jajpur :</b>  <a href="${commonResourcePath}/customAssets/AuctionPage/docs/SOP- Prime-2-Quality standards-Jajpur.pdf" target="_blank">View</a>

                         <a href="${commonResourcePath}/customAssets/AuctionPage/docs/SOP- Prime-2-Quality standards-Jajpur.pdf" download="Non_Prime_Quality_Complaint_SOP_Jajpur"><i class="fa fa-download"></i> Download</a><br>

                        <b>Non Prime Quality Complaint SOP - Hisar :</b>  <a href="${commonResourcePath}/customAssets/AuctionPage/docs/SOP- Prime-2-Quality standards-Hisar.pdf" target="_blank">View</a>

                         <a href="${commonResourcePath}/customAssets/AuctionPage/docs/SOP- Prime-2-Quality standards-Hisar.pdf" download="Non_Prime_Quality_Complaint_SOP_Hisar"><i class="fa fa-download"></i> Download</a><br>
                </div>
            </article>
        </div>
<script	type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/user-manual.js"></script>
<script	type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/jquery.min.js"></script>
<script type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/browser.min.js"></script>
<script	type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/breakpoints.min.js"></script>
<script type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/util.js"></script>
<script type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/main.js"></script>
<script	type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/jsCalendar.js"></script>

<script>
$(".filterSection").hide();
</script>        
<%--<script>
window.onload = function() { 
document.getElementById("loading").style.display = "none" }
</script>--%>