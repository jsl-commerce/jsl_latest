<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>


            <link rel="stylesheet" type="text/css" media="all" href="${commonResourcePath}/customAssets/AuctionPage/css/live-auction.css" />

<div id="loading-wrapper">
     <div class="spinner">
				  <div class="rect1"></div>
				  <div class="rect2"></div>
				  <div class="rect3"></div>
				  <div class="rect4"></div>
				  <div class="rect5"></div>
				  <div class="rect6"></div>
					
		  </div>
</div> 
<span class="sidebarclose"><i class="lni-construction-hammer"></i></span>
            <div class="inner">
                <input type="hidden" id="auctionPageName" name="auctionPageName" value="auctionLivePage" />
                <input type="hidden" id="currentDateTime" name="currentDateTime" value="${currentDateTime}" />
                <input type="hidden" id="contextPath" name="contextPath" value="${pageContext.request.contextPath}" />
                <input type="hidden" id="CSRFToken" name="CSRFToken" />
                <input type="hidden" id="isAdminUser" name="isAdminUser" value="${isAdminUser}" />
                <input type="hidden" id="additionalTime" name="additionalTime" value="${additionalTime}" />
                <input type="hidden" id="auctionStartDate" name="auctionStartDate" value="<fmt:formatDate type="date" dateStyle="medium" timeStyle="medium"
                                                   value="${bundleTemplateList[0].auctionEventData.startDate}" />">
                <input type="hidden" id="auctionStartTime" name="auctionStartTime" value="<fmt:formatDate type="time" dateStyle="medium" timeStyle="medium" 
                                            value="${bundleTemplateList[0].auctionEventData.startDate}" />">
                <input type="hidden" id="selectedAuctionID" name="selectedAuctionID" value="${auctionID}" />
                <input type="hidden" id="userPK" name="userPK" value="${userPK}">
                
                <article class="box post post-excerpt">
                    <div>
                        <!-- <p>
                            <marquee style="color: #142939; font-size:140%;"><b>Auction Won, Dreams Go On</b></marquee>
                        </p> -->
                        <cms:pageSlot position="TopBannerSlot" var="component">
                                              <p class="marqueeTitle">
                                                     <spring:htmlEscape defaultHtmlEscape="true">${component.content}</spring:htmlEscape>
                                              </p>
                                       </cms:pageSlot>
                    </div>
                    <div class="info">
                        <span class="date"><span class="month" id="auction_month"></span>
                        <span class="day" id="auction_date"></span><span id="auction_year">
                                       </span>
                        </span>
                        <ul class="stats">
                            <li><a href="#" id="tc" class="icon fa fa-check" title="No. of lots in the current auction">0</a></li>
                            <li><a href="#" id="wc" class="icon fa fa-legal" title="No. of lots won in the current auction">0</a></li>
                        </ul>

                    </div>              

                                              <!--   </div>
                                                    </div> -->
                                                    
                                 <div class="top_level">
                                <div class="col-sm-6 col-md-6">
                                    <form>
                                        <div class="auction_dropdown">
                                           <div class="col-md-3"> <span class="auction_no">Auction No:</span></div>
                                             <div class="col-md-3"> <div id="sel"></div></div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-sm-3"></div>
                                <div class="col-sm-0"></div>
                                <div class="col-sm-6 top_btns">
                                                    <c:choose>
                                                           <c:when test="${isAdminUser}">
                                                           	<button id="liveCustomers"
                                                                        class="btn btn-primary bid_page_btn">
                                                                        <i class=""></i>Live Customers
                                                                  </button>
                                                           </c:when>
                                                           <c:otherwise>
                                                                  <button id="placeBid"
                                                                        class="btn btn-primary bid_page_btn">
                                                                        <i class="lni-construction-hammer"></i> Place Bid
                                                                  </button>
                                                                  <button id="watchList"
                                                                        class="btn btn-primary bid_page_btn">
                                                                        <i class="lni-star"></i> Add to Watchlist
                                                                  </button>
                                                                  <button id="setProxy"
                                                                        class="btn btn-primary proxy_btn">
                                                                        <i class="lni-timer"></i> Set Proxy
                                                                  </button>
                                                           </c:otherwise>
                                                    </c:choose>
                                </div>
                                </div>
                            <c:choose>
                                              <c:when test="${isAdminUser}">
                                              <div style="clear: both; width: 100%"></div>
                                                    <div class="main-tableDiv sticky-table" style = "overflow: auto;">
                                <input type="hidden" id="proxyBidList" name="proxyBidList" value="">
                                <table id="table_1" class="table table-bordered table_auctionData">
                                    <thead id="myTable">
                                    <tr class="sticky-header">
                                        <th>&nbsp;</th>
                                        <th title="Click On Lot No for Lot Details">Lot Number</th>
                                        <th>Material Location</th>
                                        <th>Time Left</th>
                                        <th>Qty</th>
                                        <th>Unit</th>
                                        <th>Start Bid Price</th>
                                        <th>H1 Price</th>
                                        <th>Other Details</th>
                                       </tr> 
                                    </thead>
                                    <tbody id="lotTable" class="tableFix">
                                        <c:forEach items="${bundleTemplateList}" var="bundleTemplate" varStatus="loop">
                                            <tr id="row_${loop.index}" class="lotRow">
                                                <input type="hidden" id="bundleId_${loop.index}" name="bundleId_${loop.index}" value="${bundleTemplate.bundleId}">
                                                <td><i id="hmr_${loop.index}"></i></td>
                                                <td><a id="lot_${loop.index}"
                                                       data-toggle="collapse" data-target="#accordion_${loop.index}" class="clickable lot_${bundleTemplate.bundleId}" onclick="showLotDetails('#accordion_${loop.index}');">${bundleTemplate.bundleName}</a></td>
                                                <td>${bundleTemplate.productData[0].location}</td>
                                                <td><span id="timer_${loop.index}"><b></b></span></td>
                                                <td><fmt:formatNumber type = "number" 
         												maxFractionDigits = "2" value = "${bundleTemplate.quantity}" /></td>
                                                <td>&#x20b9;/MT</td>
                                                <td>&#x20b9;<span id="startBidPrc_${loop.index}">
                                                                                     <fmt:formatNumber type="number" maxFractionDigits="2"
                                                                                                   value="${bundleTemplate.baseBidPrice}" />
                                                                               </span>
                                                </td>
                                                <td><b>
                                                                                     <span id="h1Prc_${loop.index}" class="h1Prc h1Prc_${bundleTemplate.bundleId}" style="color: black;" onchange="hammerShow()">
                                                                                     <fmt:formatNumber type="number" maxFractionDigits="0"
                                                                                                  groupingUsed="true" value="${bundleTemplate.h1Price}" />
                                                                                     </span>
                                                                               </b>
                                                </td>
                                                <td><button class="btn btn-primary btnHome" onclick="showLotBidDetails(${loop.index});">
                                                           Show Details</button>
                                            </tr>
                                            <tr id="accordion_${loop.index}" class="collapse">
                                                <td colspan="13">
                                                    <div class="pr_table_show">
                                                        <h3>Lot Details<i class="lni-close" onclick="showLotDetails('#accordion_${loop.index}');"></i></h3>
                                                        <table class = "table table-striped prods">
                                                            <thead>
                                                                <tr>
                                                                    <th>Grade</th>
                                                                    <th>Thickness</th>
                                                                    <th>Width</th>
                                                                    <th>Length</th>
                                                                    <th>Finish</th>
                                                                   <!--  <th>Edge</th> -->
                                                                    <th>PVC</th>
                                                                    <th>Quality</th>
                                                                    <th>Quantity</th>
                                                                    <th>Series</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <c:forEach items="${bundleTemplate.productData}" var="products">
                                                                    <tr class="prodRow">
                                                                        <td id="item_grade_${loop.index}">${products.gradeGroup}</td>
                                                                        <td id="item_thickness_${loop.index}">${products.thickness}</td>
                                                                        <td id="item_width_${loop.index}">${products.width}</td>
                                                                        <td id="item_length_${loop.index}">${products.length}</td>
                                                                        <td id="item_finish_${loop.index}">${products.finish}</td>
                                                                        <%-- <td id="item_edge_${loop.index}">${products.edgeCondition}</td> --%>
                                                                        <td id="item_PVC_${loop.index}">${products.pvc}</td>
                                                                        <td id="item_quality_${loop.index}">${products.quality}</td>
                                                                        <td id="item_quantity_${loop.index}"><fmt:formatNumber type = "number" 
         																										maxFractionDigits = "2" value = "${products.quantity}" /></td>
                                                                        <td id="item_series_${loop.index}">${products.series}</td>
                                                                    </tr>
                                                                </c:forEach>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                            <div id="detailsModal" role="dialog" draggable="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h2 class="modal-title">BIDDING DETAILS<i class = "lni-close" style="float:right" onclick="hideModal('detailsModal')"></i></h2>
                            </div>
                            <div class="modal-body" id="detailsModalBody">
                                <div>
                                    <label>Total Bid Count :</label><div id="totalBidCount"> </div></br></br>
                                    <label>Bidders :</label><br><div id = "detailsBox"></div></br>
                                    <label>H1 Bidder :</label></br><div id = "h1Bidder"></div>
                                </div><br>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-primary" type="submit" onclick="hideModal('detailsModal')">Close</button>
                            </div>
                        </div>
                    </div>
                </div> 
                                              </c:when>
                                              <c:otherwise>
                                              <div style="clear: both; width: 100%"></div>
                                                    <div class="main-tableDiv sticky-table" style= "overflow: auto;">
                                <input type="hidden" id="proxyBidList" name="proxyBidList" value="">
                                <table id="table_1" class="table table-bordered table_auctionData">
                                    <thead id="myTable">
                                    <tr class="sticky-header">
                                        <th>&nbsp;</th>
                                        <th title="Click On Lot No for Lot Details">Lot Number</th>
                                        <th>Material Location</th>
                                        <th>Time Left</th>
                                        <th>Qty</th>
                                        <th>Unit</th>
                                        <th>Start Bid Price</th>
                                        <th>H1 Price</th>
                                        <th>Status</th>
                                        <th>My Bid</th>
                                        <th class="s_width">Select</th>
                                        <th>Proxy Amt</th>
                                         <th class="th_blank">&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody class="tableFix" id="lotTable">
                                        <c:forEach items="${bundleTemplateList}" var="bundleTemplate" varStatus="loop">
                                            <tr id="row_${loop.index}" class="lotRow">
                                                <input type="hidden" id="bundleId_${loop.index}" name="bundleId_${loop.index}" value="${bundleTemplate.bundleId}">
                                                <td><i id="hmr_${loop.index}"></i></td>
                                                <td><a id="lot_${loop.index}"
                                                       data-toggle="collapse" data-target="#accordion_${loop.index}" class="clickable lot_${bundleTemplate.bundleId}" onclick="showLotDetails('#accordion_${loop.index}');">${bundleTemplate.bundleName}</a></td>
                                                <td>${bundleTemplate.productData[0].location}</td>
                                                <td><span id="timer_${loop.index}"><b></b></span></td>
                                                <td><fmt:formatNumber type = "number" 
         												maxFractionDigits = "2" value = "${bundleTemplate.quantity}" /></td>
                                                <td>&#x20b9;/MT</td>
                                                <td>&#x20b9;<span id="startBidPrc_${loop.index}">
                                                                                     <fmt:formatNumber type="number" maxFractionDigits="2"
                                                                                                   value="${bundleTemplate.baseBidPrice}" />
                                                                               </span>
                                                </td>
                                                <td><b>
                                                                                     <span id="h1Prc_${loop.index}" class="h1Prc h1Prc_${bundleTemplate.bundleId}" style="color: black;" onchange="hammerShow()">
                                                                                     <fmt:formatNumber type="number" maxFractionDigits="0"
                                                                                                  groupingUsed="true" value="${bundleTemplate.h1Price}" />
                                                                                     </span>
                                                                               </b>
                                                </td>
                                                <td class="min-width-150"><span id="status_${loop.index}"></span></td>
                                                <td class="min-width-150">
                                                    <input id="decbtn_${loop.index}" class="btn minus-data" type="button" value="-" onclick="decBid(${loop.index});">
                                                    <label id="bidLabel_${loop.index}" class="myBid_${bundleTemplate.bundleId} bid-data">
                                                                                            <fmt:formatNumber type="number" maxFractionDigits="0"
                                                                                                  groupingUsed="false" value="${bundleTemplate.myBidEvent}" />
                                                                                     </label>
                                                    <input id="incbtn_${loop.index}" class="btn plus-data" type="button" value="+" onclick="incBid(${loop.index});">
                                                </td>
                                                <td class="s_width"><input type="checkbox" id="sel_${loop.index}" value="0" onclick="selectCheckBox(${loop.index});"></td>
                                                <td><span id="proxyVal_${loop.index}"
                                                                                      class="myProxyBid_${bundleTemplate.bundleId}">
                                                                                     <fmt:formatNumber type="number" maxFractionDigits="0"
                                                                                                  groupingUsed="true" value="${bundleTemplate.proxyBidEvent}" />
                                                                                     </span></td>
                                                                               <td style="display:none;"><span id="lastBid_${loop.index}">${bundleTemplate.myLastBid}</span></td>
                                            </tr>
                                            <tr id="accordion_${loop.index}" class="collapse">
                                                <td colspan="13">
                                                    <div class="pr_table_show">
                                                        <h3>Lot Details<i class="lni-close" onclick="showLotDetails('#accordion_${loop.index}');" ></i></h3>
                                                        <table class ="table table-striped prods">
                                                            <thead>
                                                                <tr>
                                                                    <th scope="col">Grade</th>
                                                                    <th scope="col">Thickness</th>
                                                                    <th scope="col">Width</th>
                                                                    <th scope="col">Length</th>
                                                                    <th scope="col">Finish</th>
                                                                    <!-- <th>Edge</th> -->
                                                                    <th scope="col">PVC</th>
                                                                    <th scope="col">Quality</th>
                                                                    <th scope="col">Quantity</th>
                                                                   <th scope="col">Series</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <c:forEach items="${bundleTemplate.productData}" var="products">
                                                                    <tr class="prodRow">
                                                                        <td data-label="Grade" id="item_grade_${loop.index}">${products.gradeGroup}</td>
                                                                        <td data-label="Thickness" id="item_thickness_${loop.index}">${products.thickness}</td>
                                                                        <td data-label="Width" id="item_width_${loop.index}">${products.width}</td>
                                                                        <td data-label="Length" id="item_length_${loop.index}">${products.length}</td>
                                                                        <td data-label="Finish" id="item_finish_${loop.index}">${products.finish}</td>
                                                                        <%-- <td id="item_edge_${loop.index}">${products.edgeCondition}</td> --%>
                                                                        <td data-label="PVC" id="item_PVC_${loop.index}">${products.pvc}</td>
                                                                        <td data-label="Quality" id="item_quality_${loop.index}">${products.quality}</td>
                                                                        <td data-label="Quantity" id="item_quantity_${loop.index}"><fmt:formatNumber type = "number" 
         																										maxFractionDigits = "2" value = "${products.quantity}" /></td>
                                                                        <td data-label="Series" id="item_series_${loop.index}">${products.series}</td>
                                                                    </tr>
                                                                </c:forEach>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                                              </c:otherwise>
                                              </c:choose>
                        
                        </article>
                    </div>
                 <!--  </div> -->
                    
                
            <!-- </div> -->
            <div id="snackBar"></div>
            
            
            <div id="myModal" role="dialog">
                    <div class="modal-dialog modal-dialog-centered">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h2 class="modal-title"><span id="tncAccepted">Terms & Conditions accepted </span><span id="tncPending">Accept Terms & Conditions</span> for Auction:&nbsp;<span id="modalAuctionNo0"></span></h2>
                            </div>
                            <div class="modal-body">
                                <div id="tncBox">
                                    <embed src="${commonResourcePath}/customAssets/AuctionPage/docs/E-Auction Terms and Conditions.pdf" />
                                </div><br>
                            </div>
                            <div class="modal-footer">
                                <div id="otpStausPending" style="float:left;"><input type="checkbox" id="tnc_check">I Agree</div>
                                <button class="btn btn-primary" id="tnc_accept">Accept</button>
                                <button class="btn btn-primary" type="submit" onclick="hideModal()">Close</button>
                            </div>
                        </div>
                    </div>
                </div>     
              <div id="liveCustomersDialog" role="dialog">
                    <div class="modal-dialog modal-dialog-centered">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h2 class="modal-title">Live Customers<i class = "lni-close" style="float:right" onclick="hideModal('liveCustomersDialog')"></i></h2>
                            </div>
                            <div class="modal-body">
                                <div id="liveCustomersDialogBody"></div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-primary" type="submit" onclick="hideModal('liveCustomersDialog')">Close</button>
                            </div>
                        </div>
                    </div>
                </div> 
                
<script type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/jquery.min.js"></script>
<script type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/browser.min.js"></script>
<script type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/breakpoints.min.js"></script>
<script type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/util.js"></script>
<script type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/main.js"></script>
<script type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/jsCalendar.js"></script>
<script type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/live-auction.js"></script>
<script type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/sockjs.js"></script>
<script type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/stomp.js"></script>
<script type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/liveAuctionWebSocket.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
<script async src="https://urldefense.proofpoint.com/v2/url?u=https-3A__www.googletagmanager.com_gtag_js-3Fid-3DUA-2D142673878-2D1&d=DwIFaQ&c=eIGjsITfXP_y-DLLX0uEHXJvU8nOHrUK8IrwNKOtkVU&r=-VkCbqDiz42qmVWnhStcNFiFiSaMrjEMChe3msrmMb0&m=wFfO8iJ0NPJpowq-NrtUQBs0wnGgGhq5wFpZ64QXfi0&s=tr6o1-SBp1BqKNjTXjbBoaLPghUtW292ysr8mc2R0x8&e= "></script> 

<script type="text/javascript" >

$(document).ready(function(){
       var headerheight = $("header").height();
       var maincontantheight = $(window).height() - headerheight;
       $(".main__inner-wrapper").height(maincontantheight);
       $(".main-tableDiv").height(maincontantheight - 120);
       $("#sidebar").height(maincontantheight);
       
       window.dataLayer = window.dataLayer || [];     
});



 $('#aStartDate').datepicker({
      format: 'mm/dd/yyyy',
      autoclose:true
      
 });
     
     $("#startDateIcon").on('click', function(){
             $("#aStartDate").show().focus();
             
       });


$('#endDate').datepicker({
      format: 'mm/dd/yyyy',
      autoclose:true
});

 $("#endDateIcon").on('click', function(){
       $("#endDate").show().focus();
       
   });

 
 $('#aDate').datepicker({
             format: 'mm/dd/yyyy',
             autoclose:true
       });
       
 $("#aDateIcon").on('click', function(){
              $("#aDate").show().focus();
              
       });

</script>


