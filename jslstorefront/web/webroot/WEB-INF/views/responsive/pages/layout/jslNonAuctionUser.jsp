<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>


      <link rel="stylesheet" type="text/css" media="all" href="${commonResourcePath}/customAssets/AuctionPage/css/bootstrap.css" />
      <link rel="stylesheet" type="text/css" media="all" href="${commonResourcePath}/customAssets/AuctionPage/css/bootstrap-theme.css" />
      <link rel="stylesheet" type="text/css" media="all" href="${commonResourcePath}/customAssets/AuctionPage/css/font-awesome.css" />
      <link rel="stylesheet" type="text/css" media="all" href="${commonResourcePath}/customAssets/AuctionPage/css/nonAuctionCustom.css" />
              
<body>

<div id="loading-wrapper">
     <div class="loader"></div>
</div> 

  <div id="wrapper">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <!-- <h1>WeBuild</h1> -->
          <h1 class="subtitle">Thanks for visting us!</h1>
		  <h2 class="subtitle">You are not a Registered Customer. Please Register to participate in Auction</h2>
          <div id="countdown" style="line-height:1.1em;margin:40px 0 60px"></div>
          <form class="form-inline signup" role="form">
           <!--  <div class="form-group">
              <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter your email address">
            </div> -->
            <button type="submit" class="btn btn-theme">Get notified!</button>
          </form>

       <!--   <div class="social" style="margin-top:20px;">
            <a href="https://www.facebook.com/JindalStainlessSolutions/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
            <a href="https://twitter.com/MagicofSS" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
            <a href="https://www.youtube.com/channel/UCi8c6I3UfAJl0HacYcxyhqA/videos" target="_blank" ><i class="fa fa-youtube" aria-hidden="true"></i></a>
            <a href="https://www.linkedin.com/company/jindal-stainlees-steel-jsl-stainless-ltd--?trk=vsrp_companies_res_name&trkInfo=VSRPsearchId%3A4448043831442550905936%2CVSRPtargetId%3A1241509%2CVSRPcmpt%3Aprimary" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
          </div>  -->
        </div>
      </div>


    </div>
  </div>
  
    
<script type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/jquery.countdown.min.js"></script>
<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>

</body>