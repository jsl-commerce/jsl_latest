<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="pagination"
	tagdir="/WEB-INF/tags/responsive/nav/pagination"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" media="all"
	href="${commonResourcePath}/customAssets/AuctionPage/css/watchlist.css" />

<div id="loading-wrapper">
     <div class="spinner">
				  <div class="rect1"></div>
				  <div class="rect2"></div>
				  <div class="rect3"></div>
				  <div class="rect4"></div>
				  <div class="rect5"></div>
				  <div class="rect6"></div>
					
		  </div>
</div>
<span class="sidebarclose"><i class="lni-construction-hammer"></i></span>
<div class="inner">
	<input type="hidden" id="auctionPageName" name="auctionPageName" value="auctionWatchListPage">
	<input type="hidden" id="currentDateTime" name="currentDateTime" value="${currentDateTime}">
	<input type="hidden" id="contextPath" name="contextPath" value="${pageContext.request.contextPath}"> 
	<input type="hidden" id="CSRFToken" name="CSRFToken" value /> 
	<input type="hidden" id="auctionStartDate" name="auctionStartDate"
		value="<fmt:formatDate type="date" dateStyle="medium" timeStyle="medium"
								value="${watchListDetailsList[0].bundleTemplateData.auctionEventData.startDate}" />">
	<input type="hidden" id="auctionStartTime" name="auctionStartTime"
		value="<fmt:formatDate type="time" dateStyle="medium" timeStyle="medium" 
								value="${watchListDetailsList[0].bundleTemplateData.auctionEventData.startDate}" />">
	<input type="hidden" id="selectedAuctionID" name="selectedAuctionID" value="${auctionID}">
    <input type="hidden" id="additionalTime" name="additionalTime" value="${additionalTime}">
    <input type="hidden" id="userPK" name="userPK" value="${userPK}">
    
	<article class="box post post-excerpt">
	<div>
                        <!-- <p>
                            <marquee style="color: #142939; font-size:140%;"><b>Auction Won, Dreams Go On</b></marquee>
                        </p> -->
                        <cms:pageSlot position="TopBannerSlot" var="component">
							<p>
								<spring:htmlEscape defaultHtmlEscape="true">${component.content}</spring:htmlEscape>
							</p>
						</cms:pageSlot>
                    </div>
		<div class="info">
			<span class="date"><span class="month" id="auction_month"></span>
				<span class="day" id="auction_date"></span><span id="auction_year"></span> </span>
			<ul class="stats">
				<li><a href="#" id="tc" class="icon fa fa-check"
					title="No. of lots in the current auction">0</a></li>
				<li><a href="#" id="wc" class="icon fa fa-legal"
					title="No. of lots won in the current auction">0</a></li>
			</ul>
		</div>

		<div class="auction_main_panel">
			<div class="panel">
				<div class="top_level">
					<div class="col-sm-4 col-md-4">
						<form>
							 <div class="auction_dropdown">
								 <div class="col-md-3"> <span class="auction_no">Auction No:</span></div>
                                  <div class="col-md-3"> <div id="sel"></div></div>
							</div>
						</form>
					</div>
					 <div class="col-sm-3"></div>
                     <div class="col-sm-0"></div>
                     <div class="col-sm-8 col-md-8">
						<button id="placeBid"
							class="btn bid_page_btn btn-primary">
							<i class="lni-construction-hammer"></i> Place Bid
						</button>
						<button id="removeWatchList"
							class="btn bid_page_btn btn-primary">
							<i class="lni-star"></i> Remove from Watchlist
						</button>
						<button id="setProxy"
							class="btn bid_page_btn btn-primary proxy_btn">
							<i class="lni-timer"></i> Set Proxy
						</button>
					</div>
					</div>
				
				<div style="clear: both; width: 100%"></div>
				<div class="main-tableDiv sticky-table" style = "overflow: auto;">
					<input type="hidden" id="proxyBidList" name="proxyBidList" value="">
					<input type="hidden" id="removedFromWatchlist"
						name="removedFromWatchlist" value="${removed}">
					<table id="table_1" class="table table-bordered table_auctionData">
						<thead id="myTable">
                                       <tr class="sticky-header">
                                        <th>&nbsp;</th>
                                        <th title="Click On Lot No for Lot Details">Lot Number</th>
                                        <th>Material Location</th>
                                        <th>Time Left</th>
                                        <th>Qty</th>
                                        <th>Unit</th>
                                        <th>Start Bid Price</th>
                                        <th>H1 Price</th>
                                        <th>Status</th>
                                        <th>My Bid</th>
                                        <th class="s_width">Select</th>
                                        <th>Proxy Amt</th>
                                         <th class="th_blank">&nbsp;</th>
                                       </tr>
                                       </thead>
                                       <tbody class="tableFix" id="lotTable">
                                              <c:forEach items="${watchListDetailsList}" var="watchList"
                                                     varStatus="loop">
                                                    <tr id="row_${loop.index}" class="lotRow">
                                                           <input type="hidden" id="lotIndex_${loop.index}"
                                                                  name="lotIndex_${loop.index}" value="${watchList.watchListId}">
                                                           <input type="hidden" id="bundleId_${loop.index}"
                                                                  name="bundleId_${loop.index}"
                                                                  value="${watchList.bundleTemplateData.bundleId}">
                                                           <td><i id="hmr_${loop.index}"></i></td>
                                                            <td><a
                                                                  style="text-decoration: underline !important; cursor: pointer;"
                                                                  onclick="showLotDetails('#accordion_${loop.index}');"
                                                                  class="item_details lot_${watchList.bundleTemplateData.bundleId}" id="lot_${loop.index}">${watchList.bundleTemplateData.bundleName}</a></td>
                                                           <td>${watchList.bundleTemplateData.productData[0].location}</td>
                                                           <td><span
                                                                  id="timer_${loop.index}"><b></b></span></td>
                                                           <td><fmt:formatNumber type = "number" 
    																maxFractionDigits = "2" value = "${watchList.bundleTemplateData.quantity}" /></td>
                                                           <td>&#x20b9;/MT</td>
                                                           <td>&#x20b9; <span
                                                                  id="startBidPrc_${loop.index}"> <fmt:formatNumber
                                                                               type="number" maxFractionDigits="2"
                                                                               value="${watchList.bundleTemplateData.baseBidPrice}" />
                                                           </span>
                                                           </td>
                                                          <td><b> <span
                                                                        id="h1Prc_${loop.index}"
                                                                        class="h1Prc h1Prc_${watchList.bundleTemplateData.bundleId}"
                                                                        onchange="hammerShow()"> <fmt:formatNumber
                                                                                     type="number" maxFractionDigits="0" groupingUsed="true"
                                                                                      value="${watchList.bundleTemplateData.h1Price}" />
                                                                  </span>
                                                           </b></td>
                                                           <td><span
                                                                  id="status_${loop.index}"><b></b></span></td>
                                                           <td class="min-width-150"><input
                                                                  id="decbtn_${loop.index}" class="btn minus-data"
                                                                  type="button" value="-" onclick="decBid(${loop.index});">
                                                                  <span id="bidLabel_${loop.index}"
                                                                  class="myBid_${watchList.bundleTemplateData.bundleId}">
                                                                        <fmt:formatNumber type="number" maxFractionDigits="0"
                                                                               groupingUsed="false"
                                                                               value="${watchList.bundleTemplateData.myBidEvent}" />
                                                           </span> <input id="incbtn_${loop.index}"
                                                                  class="btn plus-data"
                                                                  
                                                                  type="button" value="+" onclick="incBid(${loop.index});">

                                                           </td>
                                                           <td class="s_width"><input type="checkbox" id="sel_${loop.index}"
                                                                  value="0" onclick="selectCheckBox(${loop.index});"></td>
                                                            <td><span
                                                                  id="proxyVal_${loop.index}"
                                                               class="myProxyBid_${watchList.bundleTemplateData.bundleId}">
                                                                        <fmt:formatNumber type="number" maxFractionDigits="0"
                                                                               groupingUsed="true"
                                                                               value="${watchList.bundleTemplateData.proxyBidEvent}" />
                                                           </span></td>
                                                           <td style="display: none;"><span
                                                           id="lastBid_${loop.index}">${watchList.bundleTemplateData.myLastBid}</span></td>
                                                    </tr>

								<tr id="accordion_${loop.index}" class="collapse">
									<td colspan="13">
									<div class="pr_table_show">
											<h3>Lot Details<i class="lni-close" onclick="showLotDetails('#accordion_${loop.index}');"></i></h3>
											<table class ="table table-striped prods">
												<thead>
													<tr>
														<th scope="col">Grade</th>
														<th scope="col">Thickness</th>
														<th scope="col">Width</th>
														<th scope="col">Length</th>
														<th scope="col">Finish</th>
														<!-- <th>Edge</th> -->
														<th scope="col">PVC</th>
														<th scope="col">Quality</th>
														<th scope="col">Quantity</th>
														<th scope="col">Series</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach
														items="${watchList.bundleTemplateData.productData}"
														var="products">
														<tr class="prodRow">
															<td data-label="Grade" id="grade_${loop.index}">${products.gradeGroup}</td>
															<td data-label="Thickness" id="thickness_${loop.index}">${products.thickness}</td>
															<td data-label="Width" id="width_${loop.index}">${products.width}</td>
															<td data-label="Length" id="length_${loop.index}">${products.length}</td>
															<td data-label="Finish" id="finish_${loop.index}">${products.finish}</td>
															<%-- <td id="edge_${loop.index}">${products.edgeCondition}</td> --%>
															<td data-label="PVC" id="PVC_${loop.index}">${products.pvc}</td>
															<td data-label="Quality" id="quality_${loop.index}">${products.quality}</td>
															<td data-label="Quantity" id="quantity_${loop.index}"><fmt:formatNumber type = "number" 
    																							maxFractionDigits = "2" value = "${products.quantity}" /></td>
															<td data-label="Series" id="series_${loop.index}">${products.series}</td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</div>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</article>
</div>
<div id="snackBar"></div>
<script type="text/javascript"
	src="${commonResourcePath}/customAssets/AuctionPage/js/jquery.min.js"></script>
<script type="text/javascript"
	src="${commonResourcePath}/customAssets/AuctionPage/js/browser.min.js"></script>
<script type="text/javascript"
	src="${commonResourcePath}/customAssets/AuctionPage/js/breakpoints.min.js"></script>
<script type="text/javascript"
	src="${commonResourcePath}/customAssets/AuctionPage/js/util.js"></script>
<script type="text/javascript"
	src="${commonResourcePath}/customAssets/AuctionPage/js/main.js"></script>
<script type="text/javascript"
	src="${commonResourcePath}/customAssets/AuctionPage/js/jsCalendar.js"></script>

<script type="text/javascript"
	src="${commonResourcePath}/customAssets/AuctionPage/js/live-auction.js"></script>
<script type="text/javascript"
	src="${commonResourcePath}/customAssets/AuctionPage/js/watchlist.js"></script>

	
<script>
  $(document).ready(function(){
		var headerheight = $("header").height();
		var maincontantheight = $(window).height() - headerheight;
		console.log(maincontantheight);
		$(".main__inner-wrapper").height(maincontantheight);
		$(".main-tableDiv").height(maincontantheight - 120);
		$("#sidebar").height(maincontantheight);
	});
  </script>