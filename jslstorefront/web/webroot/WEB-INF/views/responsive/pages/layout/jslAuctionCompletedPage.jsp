<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
<spring:url value="/auction/completedAuction" var="completedAuctionUrl" />
<spring:url value="/auction/getCompletedAuctionDetailsBasedOnDateRange" var="completedAuctionDateRangeUrl" />
<spring:url value="/custom/downloadCompletedLotExcel?all=true" var="downloadCatalogUrlAll" />
<spring:url value="/custom/downloadCompletedLotExcel?all=false" var="downloadCatalogUrl" />
<div id="loading-wrapper">
  <div class="spinner">
    <div class="rect1"></div>
    <div class="rect2"></div>
    <div class="rect3"></div>
    <div class="rect4"></div>
    <div class="rect5"></div>
    <div class="rect6"></div>
  </div>
</div>

<span class="sidebarclose"><i class="lni-construction-hammer"></i></span>
<div class="inner">
  <input type="hidden" id="auctionPageName" name="auctionPageName" value="auctionCompletedPage">
  <input type="hidden" id="contextPath" name="contextPath" value="${pageContext.request.contextPath}">
  <input type="hidden" id="isAdminUser" name="isAdminUser" value="${isAdminUser}">
  <article class="box post post-excerpt">
    <div class="info">
      <span class="date"><span class="month" id="auction_month" ></span>
      <span class="day" id="auction_date"></span><span id="auction_year">
      </span> </span>
    </div>
    <div class="col-sm-12 completed-main-panel">
      <h4>Completed Auction Details :</h4>
      <!-- <button class="btn btn-primary right-float" onclick="exportTableToExcel('completeTable')">Export to Excel</button> -->
      <%-- <a class="btn exporttoexcel_btn right-float" id="view_catlgue_link" href="${downloadCatalogUrl}">
        <i class="fa fa-download"></i> Export to Excel
        </a> --%>
      <%-- <c:if test="${dateRange}">
        </c:if> --%>
      <c:choose>
        <c:when test="${isAdminUser}">
        </c:when>
        <c:otherwise>
          <c:choose>
            <c:when test="${dateRange}">
              <a class="btn btn-primary exporttoexcel_btn right-float" id="view_catlgue_link" 
                href="${downloadCatalogUrl}&auctionStartDate=${auctionStartDate}&auctionEndDate=${auctionEndDate}">
              <i class="fa fa-download"></i> Export to Excel</a>
              <label class="auction_range_details">Showing Auction on ${auctionStartDate} (YYYY-MM-DD)</label>
            </c:when>
            <c:otherwise>
              <a class="btn btn-primary exporttoexcel_btn right-float" id="view_catlgue_link" href="${downloadCatalogUrlAll}">
              <i class="fa fa-download"></i> Export to Excel</a>
            </c:otherwise>
          </c:choose>
        </c:otherwise>
      </c:choose>
      <div>
        <c:choose>
          <c:when test="${isAdminUser}">
            <div>
              <div class="completed_top_panel">
                <div class="container-fluid">
                  <h5>Filter Data:</h5>
                  <div class="col-md-3">
                    <form class="">
                      <input type="radio" name="filter" value="OnDate" id="OnDate" onclick="toggleFilterDivForOnDate();"> OnDate<br>
                      <input type="radio" name="filter" value="dateRange" id="dateRange" onclick="toggleFilterDivForDateRange();"> Date Range
                    </form>
                  </div>
                  <div class="col-md-9" id="calendar" style="display: none;">
                    <!-- <input type="hidden" id="auctionStartDate" name="auctionStartDate"  /> 
                      <input type="hidden" id="auctionEndDate" name="auctionEndDate"  /> -->
                    <input type="hidden" id="auctionStartDate" name="auctionStartDate" value /> 
                    <input type="hidden" id="auctionEndDate" name="auctionEndDate" value />
                    <input type="hidden" id="dateRange" name="dateRange" value="true" /> 
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Start Date</label>
                        <div class="input-group">
                          <input type="text" id="aStartDate" autocomplete="off" placeholder="mm/dd/yyyy" class="form-control hasDatepicker"/> 
                          <span class="input-group-addon" id="startDateIcon"><span class="fa fa-calendar"></span>
                          </span>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>End Date</label>
                        <div class="input-group">
                          <input type="text" id="endDate"       autocomplete="off" placeholder="mm/dd/yyyy" class="form-control hasDatepicker" value="${auctionEndDate}" />
                          <span class="input-group-addon" id="endDateIcon"> 
                          <span class="fa fa-calendar" ></span>
                          </span>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label class="blank">Type</label>
                        <select class="form-control" id="excelTypeByDateRange">
                          <option value="completed" selected>Completed</option>
                          <option value="detailed">Detailed</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group auction_details_btn">
                        <label class="blank"></label>
                        <button id="getAuctionsAdmin" type="button" class="btn date_btn" onclick="downloadBidListForAdminByDateRange();">Export Excel</button>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-9" id="onDateCalendar" style="display: none;">
                  <form id="adminAuctionDateRange" action="${completedAuctionUrl}" method="POST" modelAttribute="auctionDateRange">
                    <input type="hidden" id="auctionDate" name="auctionStartDate"  /> 
                     <input type="hidden" id="CSRFToken" name="CSRFToken" value />
                     <input type="hidden" id="dateRange" name="dateRange" value="true" />
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Date</label>
                        <div class="input-group">
                          <input type="text" id="aDate" autocomplete="off" placeholder="mm/dd/yyyy" class="form-control hasDatepicker"  /> 
                          <span class="input-group-addon" id="aDateIcon"><span class="fa fa-calendar"></span>
                          </span>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label class="blank">Type</label>
                        <select class="form-control" id="excelTypeByDate">
                          <option value="completed" selected>Completed</option>
                          <option value="detailed">Detailed</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group auction_details_btn">
                        <label class="blank"></label>
                        <button id="getAuctionsAdmin" type="button" class="btn date_btn" onclick="downloadBidListForAdminByDate();">Export Excel</button1>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group auction_details_btn">
                        <label class="blank"></label>
                        <button id="getAuctionsForAdmin" type="button" class="btn date_btn">Show Auction</button1>
                      </div>
                    </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </c:when>
          <c:otherwise>
            <div class="completed_top_panel">
              <div class="container-fluid">
                <div class="col-sm-3">
                  <form class="">
                    <c:choose>
                      <c:when test="${dateRange}">
                        <!-- <input type="radio" name="filter" value="all" id="all" onclick="navigatePageToCompletedAuction();"> All<br> -->
                        <input type="radio" name="filter" value="dateRange" id="dateRange" onclick="toggleFilterDiv();" checked> Date
                      </c:when>
                      <c:otherwise>
                        <!--  <input type="radio" name="filter" value="all" id="all" onclick="navigatePageToCompletedAuction();" checked> All<br> -->
                        <input type="radio" name="filter" value="dateRange" id="dateRange" onclick="toggleFilterDiv();"> Date
                      </c:otherwise>
                    </c:choose>
                  </form>
                </div>
                <div class="col-sm-0"></div>
                <c:choose>
                  <c:when test="${dateRange}">
                    <div class="col-sm-9" id="calendar" style="display: block;">
                      <form id="auctionDateRange" action="${completedAuctionUrl}" method="POST" modelAttribute="auctionDateRange">
                        <input type="hidden" id="CSRFToken" name="CSRFToken" value />
                        <input type="hidden" id="auctionStartDate" name="auctionStartDate" value /> 
                        <input type="hidden" id="auctionEndDate" name="auctionEndDate" value />
                        <input type="hidden" id="dateRange" name="dateRange" value="true" /> 
                        <div class="col-sm-4">
                          <div class="form-group">
                            <label>Date</label> 
                            <div class="input-group">
                              <input type="text" id="aStartDate" class="form-control hasDatepicker"/>
                              <span class="input-group-addon" id="startDateIcon">
                              <span class="fa fa-calendar">
                              </span>
                              </span>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <!-- <div class="form-group">
                            <label>End Date</label>
                            <div class="input-group">
                              <input type="text" id="endDate" class="form-control hasDatepicker"/>
                            <span class="input-group-addon" id="endDateIcon">
                            <span class="fa fa-calendar"></span>
                            </span></div>
                            
                            </div> -->
                        </div>
                        <div class="col-sm-4">
                          <div class="form-group auction_details_btn">
                            <label class="blank"></label>
                            <button id="getAuctions" type="button" class="btn auction_details_btn date_btn">Get
                            Auction Details</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </c:when>
                  <c:otherwise>
                    <div class="col-sm-9" id="calendar" style="display: none;">
                      <form id="auctionDateRange" action="${completedAuctionUrl}" method="POST" modelAttribute="auctionDateRange">
                        <input type="hidden" id="CSRFToken" name="CSRFToken" value />
                        <input type="hidden" id="auctionStartDate" name="auctionStartDate" value />
                        <input type="hidden" id="auctionEndDate" name="auctionEndDate" value />
                        <input type="hidden" id="dateRange" name="dateRange" value="true" /> 
                        <div class="col-sm-4">
                          <div class="form-group">
                            <label>Date</label> 
                            <div class="input-group">
                              <input type="text" id="aStartDate" autocomplete="off" placeholder="mm/dd/yyyy" class="form-control hasDatepicker" />
                              <span class="input-group-addon" id="startDateIcon">
                              <span class="fa fa-calendar"></span>
                              </span>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <!-- <div class="form-group">
                            <label>End Date</label>
                             <div class="input-group">
                             <input type="text" id="endDate" autocomplete="off" placeholder="mm/dd/yyyy" class="form-control hasDatepicker" />
                            <span class="input-group-addon" id="endDateIcon">
                            <span class="fa fa-calendar"></span>
                            </span>
                            </div>
                            </div> -->
                        </div>
                        <div class="col-sm-4">
                          <div class="form-group auction_details_btn">                                                                
                            <label class="blank"></label>
                            <button id="getAuctions" type="button" class="btn auction_details_btn date_btn">Get
                            Auction Details</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </c:otherwise>
                </c:choose>
              </div>
            </div>
          </c:otherwise>
        </c:choose>
        <div>
          <div style="clear: both; width: 100%"></div>
          <div class="main-tableDiv sticky-table" style = "overflow : auto;">
            <table class="table table-bordered table_auctionData" id="completeTable">
              <thead>
                <c:choose>
                  <c:when test="${isAdminUser}">
                    <tr class="sticky-header">
                      <th width="15%">Auction Date</th>
                      <th width="10%" onclick="SortTable(1,'T');">Auction Id <i class="fa fa-fw fa-sort"></i></th>
                      <th width="10%" >lot No</th>
                      <th width="10%">Qty</th>
                      <th width="10%">Start Bid Price</th>
                      <th width="10%">H1 Price</th>
                      <th width="15%" onclick="SortTable(8,'T');">Approvals Status <i class="fa fa-fw fa-sort"></i></th>
                      <th width="15%">Won By</th>
                    </tr>
                  </c:when>
                  <c:otherwise>
                    <th width="15%">Auction Date</th>
                    <th width="10%" onclick="SortTable(1,'T');">Auction Id <i class="fa fa-fw fa-sort"></i></th>
                    <th width="10%" >lot No</th>
                    <th width="10%">Qty</th>
                    <th width="10%">Start Bid Price</th>
                    <th width="10%">Your bid Price</th>
                    <th width="10%">H1 Price</th>
                    <th width="10%" onclick="SortTable(7,'T');" id="bidStatus">Bid Status <i class="fa fa-fw fa-sort"></i></th>
                    <th width="15%" onclick="SortTable(8,'T');">Approvals Status <i class="fa fa-fw fa-sort"></i></th>
                  </c:otherwise>
                </c:choose>
              </thead>
              <tbody id="compAuctionTable">
                <c:forEach items="${completedBundleList}" var="bundleTemplate" varStatus="loop">
                  <tr class = "completedRow" id="row_${loop.index}">
                    <td width="15%">
                      <label id="auctionDate_${loop.index}">
                        <fmt:formatDate
                          type="date" dateStyle="medium" timeStyle="medium"
                          value="${bundleTemplate.auctionStartDate}" />
                      </label>
                    </td>
                    <td width="10%"><label id="auctionId_${loop.index}">${bundleTemplate.auctionID}</label></td>
                    <td width="10%"><a class="item_details" id="lot_${loop.index}" onclick="showLotDetails('#accordion_${loop.index}');">${bundleTemplate.bundleName}</a></td>
                    <td width="10%">
                      <label id="qty_${loop.index}">
                        <fmt:formatNumber type = "number" 
                          maxFractionDigits = "2" value = "${bundleTemplate.bundleQty}" />
                      </label>
                    </td>
                    <td width="10%">
                      <label id="startBidPrice_${loop.index}">
                        &#x20b9;
                        <fmt:formatNumber type="number" maxFractionDigits="2"
                          value="${bundleTemplate.startBidPrice}" />
                      </label>
                    </td>
                    <c:if test="${not isAdminUser}">
                      <td width="10%">
                        <label id="urBidPrc">
                          &#x20b9; 
                          <fmt:formatNumber type="number" maxFractionDigits="2"
                            value="${bundleTemplate.userLastBidPrice}" />
                        </label>
                      </td>
                    </c:if>
                    <td width="10%">
                      <label id="h1Prc">
                        &#x20b9; 
                        <fmt:formatNumber
                          type="number" maxFractionDigits="2"
                          value="${bundleTemplate.h1Price}" />
                      </label>
                    </td>
                    <c:if test="${not isAdminUser}">
                      <td width="10%"><label>${bundleTemplate.bidStatus}</label></td>
                    </c:if>
                    <td width="15%"><label>${bundleTemplate.orderStatus}</label></td>
                    <c:if test="${isAdminUser}">
                      <td width="15%"><label>${bundleTemplate.bidWonBy}</label></td>
                    </c:if>
                  </tr>
                  <tr width="100%" id="accordion_${loop.index}" class="collapse">
                    <td colspan="13">
                      <div class="pr_table_show">
                        <h3>Lot Details<i class="fa fa-close" onclick="showLotDetails('#accordion_${loop.index}');"></i></h3>
                        <table class = "table table-striped prods">
                          <thead>
                            <tr>
                              <th>Grade</th>
                              <th>Thickness</th>
                              <th>Width</th>
                              <th>Length</th>
                              <th>Finish</th>
                              <!-- <th>Edge</th> -->
                              <th>PVC</th>
                              <th>Quality</th>
                              <th>Quantity</th>
                              <th>Series</th>
                            </tr>
                          </thead>
                          <tbody>
                            <c:forEach items="${bundleTemplate.productData}" var="products">
                              <tr class="prodRow">
                                <td id="item_grade_${loop.index}">${products.gradeGroup}</td>
                                <td id="item_thickness_${loop.index}">${products.thickness}</td>
                                <td id="item_width_${loop.index}">${products.width}</td>
                                <td id="item_length_${loop.index}">${products.length}</td>
                                <td id="item_finish_${loop.index}">${products.finish}</td>
                                <td id="item_edge_${loop.index}">${products.edgeCondition}</td>
                                <td id="item_PVC_${loop.index}">${products.pvc}</td>
                                <td id="item_quality_${loop.index}">${products.quality}</td>
                                <td id="item_quantity_${loop.index}">
                                  <fmt:formatNumber type = "number" 
                                    maxFractionDigits = "2" value = "${products.quantity}" />
                                </td>
                                <td id="item_series_${loop.index}">${products.series}</td>
                              </tr>
                            </c:forEach>
                          </tbody>
                        </table>
                      </div>
                    </td>
                  </tr>
                </c:forEach>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </article>
</div>
<div id="snackBar"></div>
<script type="text/javascript">
  function showLotDetails(id) {
      $(id).toggle();
  }
  
   $('#aStartDate').datepicker({
  	format: 'mm/dd/yyyy',
  	autoclose:true
  	
  });
      
      $("#startDateIcon").on('click', function(){
        	$("#aStartDate").show().focus();
        	
        });
  
  
  $('#endDate').datepicker({
  	format: 'mm/dd/yyyy',
  	autoclose:true
  });
  
  $("#endDateIcon").on('click', function(){
    	$("#endDate").show().focus();
    	
    });
  
  $("#aDate").datepicker({
   format: "mm/dd/yyyy",
   autoclose:true
  });
  
  $("#aDateIcon").on('click', function(){
    	$("#aDate").show().focus();
    	
    });
  
  $(document).ready(function(){
  var headerheight = $("header").height();
  console.log(headerheight);
  var maincontantheight = $(window).height() - headerheight;
  console.log(maincontantheight);
  $(".main__inner-wrapper").height(maincontantheight);
  $(".main-tableDiv").height(maincontantheight - 120);
  $("#sidebar").height(maincontantheight);
  });
  
</script>
<script type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/completed_auction.js"></script>
<script type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/jquery.min.js"></script>
<script type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/browser.min.js"></script>
<script type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/breakpoints.min.js"></script>
<script type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/util.js"></script>
<script type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/main.js"></script>
<script type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/jsCalendar.js"></script>