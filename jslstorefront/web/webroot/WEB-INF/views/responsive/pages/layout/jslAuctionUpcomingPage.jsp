<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<link rel="stylesheet" type="text/css" media="all" href="${commonResourcePath}/customAssets/AuctionPage/css/upcoming_auction.css"/>
<spring:url value="/custom/downloadExcel?auctionID=" var="downloadCatalogUrl" />

<div id="loading-wrapper">
     <div class="spinner">
				  <div class="rect1"></div>
				  <div class="rect2"></div>
				  <div class="rect3"></div>
				  <div class="rect4"></div>
				  <div class="rect5"></div>
				  <div class="rect6"></div>
					
		  </div>
</div>
<span class="sidebarclose"><i class="lni-construction-hammer"></i></span>
<div class="inner">
<input type="hidden" id="contextPath" name="contextPath" value="${pageContext.request.contextPath}">
<input type="hidden" id="auctionPageName" name="auctionPageName" value="auctionUpcomingPage">
<input type="hidden" id="CSRFToken"	name="CSRFToken" value />
<input type="hidden" id="isAdminUser" value="${isAdminUser}" /> 

	<article class="box post post-excerpt">
		<div class="info">
				<span class="date"><span class="month" id="auction_month"></span>
				<span class="day" id="auction_date"></span><span id="auction_year">
				</span>
			</span>
		</div>
		 <div class="col-sm-12 upcoming_main_panel">
            <div class="col-sm-12">
                  <h4>Upcoming Auction Details:</h4>
                  <c:choose>
                  	<c:when test="${!isAdminUser}">
                  		<button class="btn btn-primary right-float" id="watchList">Add To WatchList</button>	
                  	</c:when>
                  </c:choose>
                  
                  <!-- <i class="fa fa-chevron-down" style="float: right" onclick="showDetails()"></i> -->
            </div>

        <input type="hidden" id="testotpAcceptedList"	name="testotpAcceptedList" value="${otpAcceptedAuctionList}" /> 
            <input type="hidden" id="testAuctionList"	name="testAuctionList" value="${auctionEventList}}" /> 
            			 <div class="col-sm-12">
               <div style="clear: both; width: 100%"></div>
				<div class="main-tableDiv sticky-table" style = "overflow : auto;">
					 <table class="table table-bordered table_auctionData">
                          <thead class="" id="myTable">
							<th width="20%">Auction Number</th>
							<th width="20%">Auction Group</th>
							<th width="10%">Date</th>
							<th width="20%">Start Time</th>
							<th width="20%">OTP</th>
							<th width="10%">Catalog</th>
							<th width="20%">Terms and Conditions</th>
						</thead>
						<tbody id="upcomingAuctionTable">
						<c:choose>
							<c:when test="${isAdminUser}">
								<c:forEach items="${auctionEventList}" var="auctionEvent"
								varStatus="loop">
								<tr class="auctionRow" id="auctionRow_${loop.index}">
                                   <input class="otpAccepted" type="hidden" id="otpAccepted_${loop.index}" value="${auctionEvent.auctionId}">
									<td width="20%"><span
										id="auctionId_${loop.index}" data-toggle="collapse"
										data-target="#accordion_${loop.index}" class="clickable"
										onclick="showLotDetails('#accordion_${loop.index}');">
											${auctionEvent.auctionId} </span></td>
									<td width="20%"><span id="auction_group_${loop.index}">${auctionEvent.auctionGroup}</span></td>
									<td width="10%"><span id="auctionDate_${loop.index}"><fmt:formatDate
												type="date" dateStyle="medium" timeStyle="medium"
												value="${auctionEvent.startDate}" /></span></td>
									<td width="20%"><span id="startTime_${loop.index}"><fmt:formatDate
												type="time" dateStyle="medium" timeStyle="medium"
												value="${auctionEvent.startDate}" /></span></td>
									<td width="20%"><input class="" type="text"
										id="otp_${loop.index}" readonly></td>
									<td width="10%"><a id="view_catlgue_link_${loop.index}"
										href="${downloadCatalogUrl}${auctionEvent.auctionId}"
										class="fa fa-download isEnabled" style="cursor: pointer;">Download</a></td>
									<td width="20%"><button class="btn btnHome" id="acptBtn_${loop.index}"
											onclick="openModalUpcoming(${loop.index},'accepted');">
											Show T&C <i class="fa fa-check-square"></i>
										</button></td>
								</tr>
								<tr id="accordion_${loop.index}" class="collapse">
									<td colspan="7">
										<div class="pr_table_show">
											<h3>Auction Details<i class="fa fa-close" onclick="showLotDetails('#accordion_${loop.index}');" ></i></h3>
											<table class="lotsTable table table-striped ">
												<thead>
													<tr>
														<th scope="col" title="Click On Lot No for Lot Details">Lot
															Number</th>
														<th scope="col">Material Location</th>
														<th scope="col">Qty</th>
														<th scope="col">Unit</th>
														<th scope="col">Start Bid Price</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${BundleMap}" var="bundle">
														<c:if test="${bundle.key eq auctionEvent.auctionId}">
															<c:forEach items="${bundle.value}" var="bundleTemplate"
																varStatus="lotloop">
																<tr id="row_${loop.index}${lotloop.index}" class="lotRow">
																	<input type="hidden"
																		id="lotNo_${loop.index}${lotloop.index}" name=""
																		value="${bundleTemplate.bundleId}">
																	<td><a
																		style="text-decoration: underline !important; cursor: pointer;"
																		onclick="showLotDetails('.productDetails_${loop.index}${lotloop.index}');"
																		id="lotName_${loop.index}${lotloop.index}"
																		data-toggle="collapse"
																		data-target="#accordion_0${lotloop.index}"
																		class="clickable lot_${bundleTemplate.bundleId}">${bundleTemplate.bundleName}</a></td>
																	<td><span id="matLoc_1${lotloop.index}">${bundleTemplate.productData[0].location}</span></td>
																	<td><span id="qty_1${lotloop.index}">${bundleTemplate.quantity}</span></td>
																	<td><span id="unit_1${lotloop.index}">&#x20b9;/MT</span></td>
																	<td>&#x20b9;<span id="startBidPrc_1${lotloop.index}"><fmt:formatNumber type="number" maxFractionDigits="2"
																								value="${bundleTemplate.baseBidPrice}" /></span>
																	</td>
																</tr>
																<tr id="accordion_${loop.index}${lotloop.index}"
																	class="collapse productDetails_${loop.index}${lotloop.index}">
																	<td colspan="14">
																		<div>
																			<h3>
																				Lot Details<i class="fa fa-close"
																					onclick="showLotDetails('#accordion_${loop.index}${lotloop.index}');"
																					></i>
																			</h3>
																			<table class="table table-striped prods">
																				<thead>
																					<tr>
																						<th>Grade</th>
																						<th>Thickness</th>
																						<th>Width</th>
																						<th>Length</th>
																						<th>Finish</th>
																						<!-- <th>Edge</th> -->
																						<th>PVC</th>
																						<th>Quality</th>
																						<th>Quantity</th>
																						<th>Series</th>
																					</tr>
																				</thead>
																				<tbody>
																					<c:forEach items="${bundleTemplate.productData}"
																						var="products">
																						<tr class="prodRow">
																							<td id="item_grade_${loop.index}">${products.gradeGroup}</td>
																							<td id="item_thickness_${loop.index}">${products.thickness}</td>
																							<td id="item_width_${loop.index}">${products.width}</td>
																							<td id="item_length_${loop.index}">${products.length}</td>
																							<td id="item_finish_${loop.index}">${products.finish}</td>
																							<%-- <td id="item_edge_${loop.index}">${products.edgeCondition}</td> --%>
																							<td id="item_PVC_${loop.index}">${products.pvc}</td>
																							<td id="item_quality_${loop.index}">${products.quality}</td>
																							<td id="item_quantity_${loop.index}">${products.quantity}</td>
																							<td id="item_series_${loop.index}">${products.series}</td>
																						</tr>
																					</c:forEach>
																				</tbody>
																			</table>
																		</div>
																	</td>
																</tr>
															</c:forEach>
														</c:if>
													</c:forEach>
												</tbody>
											</table>
										</div>
									</td>
								</tr>
							</c:forEach>
							</c:when>
							<c:otherwise>
								<c:forEach items="${otpAcceptedAuctionList}" var="auctionEvent"
								varStatus="loop">
								<tr class="auctionRow" id="auctionRow_${loop.index}">
                                   <input class="otpAccepted" type="hidden" id="otpAccepted_${loop.index}" value="${auctionEvent.auctionId}">
									<td width="20%"><span
										id="auctionId_${loop.index}" data-toggle="collapse"
										data-target="#accordion_${loop.index}" class="clickable"
										onclick="showLotDetails('#accordion_${loop.index}');">
											${auctionEvent.auctionId} </span></td>
									<td width="20%"><span id="auction_group_${loop.index}">${auctionEvent.auctionGroup}</span></td>
									<td width="10%"><span id="auctionDate_${loop.index}"><fmt:formatDate
												type="date" dateStyle="medium" timeStyle="medium"
												value="${auctionEvent.startDate}" /></span></td>
									<td width="20%"><span id="startTime_${loop.index}"><fmt:formatDate
												type="time" dateStyle="medium" timeStyle="medium"
												value="${auctionEvent.startDate}" /></span></td>
									<td width="20%"><input class="" type="text"
										id="otp_${loop.index}" readonly></td>
									<td width="10%"><a id="view_catlgue_link_${loop.index}"
										href="${downloadCatalogUrl}${auctionEvent.auctionId}"
										class="fa fa-download isEnabled" style="cursor: pointer;">Download</a></td>
									<td width="20%"><button class="btn btnHome" id="acptBtn_${loop.index}"
											onclick="openModalUpcoming(${loop.index},'accepted');">
											Show T&C <i class="fa fa-check-square"></i>
										</button></td>
								</tr>
								<tr id="accordion_${loop.index}" class="collapse">
									<td colspan="7">
										<div class="pr_table_show">
											<h3>Auction Details<i class="fa fa-close" onclick="showLotDetails('#accordion_${loop.index}');" ></i></h3>
											<table class="lotsTable table table-striped ">
												<thead>
													<tr>
														<th scope="col" title="Click On Lot No for Lot Details">Lot
															Number</th>
														<th scope="col">Material Location</th>
														<th scope="col">Qty</th>
														<th scope="col">Unit</th>
														<th scope="col">Start Bid Price</th>
														<th scope="col">Select</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${BundleMap}" var="bundle">
														<c:if test="${bundle.key eq auctionEvent.auctionId}">
															<c:forEach items="${bundle.value}" var="bundleTemplate"
																varStatus="lotloop">
																<tr id="row_${loop.index}${lotloop.index}" class="lotRow">
																	<input type="hidden"
																		id="lotNo_${loop.index}${lotloop.index}" name=""
																		value="${bundleTemplate.bundleId}">
																	<td><a
																		style="text-decoration: underline !important; cursor: pointer;"
																		onclick="showLotDetails('.productDetails_${loop.index}${lotloop.index}');"
																		id="lotName_${loop.index}${lotloop.index}"
																		data-toggle="collapse"
																		data-target="#accordion_0${lotloop.index}"
																		class="clickable lot_${bundleTemplate.bundleId}">${bundleTemplate.bundleName}</a></td>
																	<td><span id="matLoc_1${lotloop.index}">${bundleTemplate.productData[0].location}</span></td>
																	<td><span id="qty_1${lotloop.index}"><fmt:formatNumber type = "number" 
         																										maxFractionDigits = "2" value = "${bundleTemplate.quantity}" /></span></td>
																	<td><span id="unit_1${lotloop.index}">&#x20b9;/MT</span></td>
																	<td>&#x20b9;<span id="startBidPrc_1${lotloop.index}"><fmt:formatNumber type="number" maxFractionDigits="2"
																								value="${bundleTemplate.baseBidPrice}" /></span>
																	</td>
																	<td><input type="checkbox"
																		id="sel_${loop.index}${lotloop.index}"
																		onclick="selectToggle('${loop.index}${lotloop.index}')"></td>
																</tr>
																<tr id="accordion_${loop.index}${lotloop.index}"
																	class="collapse productDetails_${loop.index}${lotloop.index}">
																	<td colspan="14">
																		<div>
																			<h3>
																				Lot Details<i class="fa fa-close"
																					onclick="showLotDetails('#accordion_${loop.index}${lotloop.index}');"
																					></i>
																			</h3>
																			<table class="table table-striped prods">
																				<thead>
																					<tr>
																						<th>Grade</th>
																						<th>Thickness</th>
																						<th>Width</th>
																						<th>Length</th>
																						<th>Finish</th>
																						<!-- <th>Edge</th> -->
																						<th>PVC</th>
																						<th>Quality</th>
																						<th>Quantity</th>
																						<th>Series</th>
																					</tr>
																				</thead>
																				<tbody>
																					<c:forEach items="${bundleTemplate.productData}"
																						var="products">
																						<tr class="prodRow">
																							<td id="item_grade_${loop.index}">${products.gradeGroup}</td>
																							<td id="item_thickness_${loop.index}">${products.thickness}</td>
																							<td id="item_width_${loop.index}">${products.width}</td>
																							<td id="item_length_${loop.index}">${products.length}</td>
																							<td id="item_finish_${loop.index}">${products.finish}</td>
																							<%-- <td id="item_edge_${loop.index}">${products.edgeCondition}</td> --%>
																							<td id="item_PVC_${loop.index}">${products.pvc}</td>
																							<td id="item_quality_${loop.index}">${products.quality}</td>
																							<td id="item_quantity_${loop.index}"><fmt:formatNumber type = "number" 
         																																maxFractionDigits = "2" value = "${products.quantity}" /></td>
																							<td id="item_series_${loop.index}">${products.series}</td>
																						</tr>
																					</c:forEach>
																				</tbody>
																			</table>
																		</div>
																	</td>
																</tr>
															</c:forEach>
														</c:if>
													</c:forEach>
												</tbody>
											</table>
										</div>
									</td>
								</tr>
							</c:forEach>
							<c:forEach items="${auctionEventList}" var="auctionEvent"
								varStatus="loop">
								<tr class="auctionRow" id="auctionRow_${loop.index+fn:length(otpAcceptedAuctionList)}">
									<td width="20%"><span
										id="auctionId_${loop.index+fn:length(otpAcceptedAuctionList)}" data-toggle="collapse"
										data-target="#accordion_${loop.index+fn:length(otpAcceptedAuctionList)}" class="clickable isDisabled"
										onclick="showLotDetails('#accordion_${loop.index+fn:length(otpAcceptedAuctionList)}');">
											${auctionEvent.auctionId} </span></td>
									<td width="20%"><span id="auction_group_${loop.index}">${auctionEvent.auctionGroup}</span></td>
									<td width="10%"><span id="auctionDate_${loop.index+fn:length(otpAcceptedAuctionList)}"><fmt:formatDate
												type="date" dateStyle="medium" timeStyle="medium"
												value="${auctionEvent.startDate}" /></span></td>
									<td width="20%"><span id="startTime_${loop.index+fn:length(otpAcceptedAuctionList)}"><fmt:formatDate
												type="time" dateStyle="medium" timeStyle="medium"
												value="${auctionEvent.startDate}" /></span></td>
									<td width="20%"><input class="" type="text" id="otp_${loop.index+fn:length(otpAcceptedAuctionList)}"></td>
									<td width="10%"><a id="view_catlgue_link_${loop.index+fn:length(otpAcceptedAuctionList)}" href="${downloadCatalogUrl}${auctionEvent.auctionId}"
										class="fa fa-download isDisabled">Download</a></td>
									<td width="20%"><button class="btn btnHome" id="acptBtn_${loop.index+fn:length(otpAcceptedAuctionList)}"
											onclick="openModalUpcoming(${loop.index+fn:length(otpAcceptedAuctionList)},'pending');">Show
											T&C</button></td>
								</tr>
								<tr id="accordion_${loop.index+fn:length(otpAcceptedAuctionList)}" class="collapse">
									<td colspan="7">
										<div class="pr_table_show">
											<h3>Auction Details<i class="fa fa-close" onclick="showLotDetails('#accordion_${loop.index+fn:length(otpAcceptedAuctionList)}');" ></i></h3>
											<table class="lotsTable table table-striped ">
												<thead>
													<tr>
														<th scope="col" title="Click On Lot No for Lot Details">Lot
															Number</th>
														<th scope="col">Material Location</th>
														<th scope="col">Qty</th>
														<th scope="col">Unit</th>
														<th scope="col">Start Bid Price</th>
														<th scope="col">Select</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${BundleMap}" var="bundle">
														<c:if test="${bundle.key eq auctionEvent.auctionId}">
															<c:forEach items="${bundle.value}" var="bundleTemplate"
																varStatus="lotloop">
																<tr id="row_${loop.index+fn:length(otpAcceptedAuctionList)}${lotloop.index}" class="lotRow">
																	<input type="hidden" id="lotNo_${loop.index+fn:length(otpAcceptedAuctionList)}${lotloop.index}" name="" value="${bundleTemplate.bundleId}">
																	<td><a
																		style="text-decoration: underline !important; cursor: pointer;"
																		onclick="showLotDetails('.productDetails_${loop.index}${lotloop.index}');"
																		id="lotName_${loop.index+fn:length(otpAcceptedAuctionList)}${lotloop.index}"
																		data-toggle="collapse"
																		data-target="#accordion_0${lotloop.index}"
																		class="clickable lot_${bundleTemplate.bundleId}">${bundleTemplate.bundleName}</a></td>
																	<td><span id="matLoc_1${lotloop.index}">${bundleTemplate.productData[0].location}</span></td>
																	<td><span id="qty_1${lotloop.index}"><fmt:formatNumber type = "number" 
         																										maxFractionDigits = "2" value = "${bundleTemplate.quantity}" /></span></td>
																	<td><span id="unit_1${lotloop.index}">&#x20b9;/MT</span></td>
																	<td>&#x20b9;<span id="startBidPrc_1${lotloop.index}"><fmt:formatNumber type="number" maxFractionDigits="2"
																								value="${bundleTemplate.baseBidPrice}" /></span>
																	</td>
																	<td><input type="checkbox"
																		id="sel_${loop.index+fn:length(otpAcceptedAuctionList)}${lotloop.index}"
																		onclick="selectToggle('${loop.index+fn:length(otpAcceptedAuctionList)}${lotloop.index}')"></td>
																</tr>
																<tr id="accordion_${loop.index+fn:length(otpAcceptedAuctionList)}${lotloop.index}"
																	class="collapse productDetails_${loop.index}${lotloop.index}">
																	<td colspan="14">
																		<div>
																			<h3>
																				Lot Details<i class="fa fa-close"
																					onclick="showLotDetails('#accordion_${loop.index}${lotloop.index}');"
																					></i>
																			</h3>
																			<table class="table table-striped prods">
																				<thead>
																					<tr>
																						<th>Grade</th>
																						<th>Thickness</th>
																						<th>Width</th>
																						<th>Length</th>
																						<th>Finish</th>
																						<!-- <th>Edge</th> -->
																						<th>PVC</th>
																						<th>Quality</th>
																						<th>Quantity</th>
																						<th>Series</th>
																					</tr>
																				</thead>
																				<tbody>
																					<c:forEach items="${bundleTemplate.productData}"
																						var="products">
																						<tr class="prodRow">
																							<td id="item_grade_${loop.index+fn:length(otpAcceptedAuctionList)}">${products.gradeGroup}</td>
																							<td id="item_thickness_${loop.index+fn:length(otpAcceptedAuctionList)}">${products.thickness}</td>
																							<td id="item_width_${loop.index+fn:length(otpAcceptedAuctionList)}">${products.width}</td>
																							<td id="item_length_${loop.index+fn:length(otpAcceptedAuctionList)}">${products.length}</td>
																							<td id="item_finish_${loop.index+fn:length(otpAcceptedAuctionList)}">${products.finish}</td>
																							<%-- <td id="item_edge_${loop.index+fn:length(otpAcceptedAuctionList)}">${products.edgeCondition}</td> --%>
																							<td id="item_PVC_${loop.index+fn:length(otpAcceptedAuctionList)}">${products.pvc}</td>
																							<td id="item_quality_${loop.index+fn:length(otpAcceptedAuctionList)}">${products.quality}</td>
																							<td id="item_quantity_${loop.index+fn:length(otpAcceptedAuctionList)}"><fmt:formatNumber type = "number" 
         																																								maxFractionDigits = "2" value = "${products.quantity}" /></td>
																							<td id="item_series_${loop.index+fn:length(otpAcceptedAuctionList)}">${products.series}</td>
																						</tr>
																					</c:forEach>
																				</tbody>
																			</table>
																		</div>
																	</td>
																</tr>
															</c:forEach>
														</c:if>
													</c:forEach>
												</tbody>
											</table>
										</div>
									</td>
								</tr>
							</c:forEach>
							
							</c:otherwise>
						</c:choose>
							
						</tbody>
						
					</table>
				</div>
			</div>
		</div>
	</article>
</div>
<div id="snackBar"></div>

<!-- <div id="myModal" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            Modal content
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Accept Terms & Conditions</h2>
                </div>
                <div class="modal-body">
                    <label>Auction Number:&nbsp;</label><span id="modalAuctionNo0"></span><br>
                    <input type="checkbox" id="tnc_check"> I Accept the <strong>Terms & Conditions</strong> for <span id="modalAuctionNo1"></span>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" id="tnc_accept">Accept</button>
                    <button class="btn btn-primary" type="submit" onclick="hideModal()">Close</button>
                </div>
            </div>
        </div>
    </div> -->
    
    <div id="myModal" role="dialog" class="home-modal">
                    <div class="modal-dialog modal-dialog-centered">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h2 class="modal-title"><span id="tncAccepted">Terms & Conditions accepted </span><span id="tncPending">Accept Terms & Conditions</span> for Auction:&nbsp;<span id="modalAuctionNo0"></span></h2>
                            </div>
                            <div class="modal-body">
                                <div id="tncBox" style="overflow:scroll">
                                    <header>
									  <h2>
										GENERAL TERMS & CONDITIONS OF E-AUCTION SALE 
									 </h2>
									</header>
									
									<section align="justify">
									  
									  <article>
									    <h1></h1>
										
									<p>
											
									<h1>1.	INTRODUCTION </h1>
									<P align="justify">Welcome to Jindal Online Auction Portal website (hereinafter referred to as the &#34Website&#34).<br />
									M/s Jindal Stainless (Hisar) Ltd. (JSHL) and M/s Jindal Stainless Limited (JSL) (hereinafter referred to as &#34Sellers&#34 collectively and &#34Seller&#34 individually) hereby appoint M/s Jindal Stainless Corporate Management Services Pvt Ltd., Delhi, (JSCMS) to provide for the online display and additional hosting facilities, conducting and managing the e-auction of their respective products to the bidders through the e-auction market place, regular maintenance of e- auction market place and other related services.  </p>
									</P>
									
									<h1>2.	DEFINITIONS:</h1>
									<P align="justify">Unless the context otherwise requires under the terms and conditions of the auction sale, the following words shall have the meaning as assigned to them hereunder:<br /></p>
									
									a.	&#34Bid&#34:- is the basic price excluding applicable tax and freight which is quoted by the Bidder for the Lot. <br />
									b.	&#34Bidder&#34 shall mean such person or entity who has been registered on the Website by the Service Provider basis the norms and conditions finalized by the respective Sellers   and shall be deemed to mean and include his/her/their/its legal heirs, legal representatives and successors. A successful Bidder shall be one who has entered the highest Bid for the Lot in e-auction held on the Website.<br />
									c.	&#34Bidding&#34 shall mean the process by which the Bidder can participate in the e-auction and Bid for a particular auction or Lot. A Bidder shall have the option to place multiple Bids against each Lot during the stipulate time of the e-auction.<br />
									d.	&#34Contract&#34 shall mean the order acceptance sent by the respective Seller for approved Lot of the successful Bidder for supply of Products/Services in electronic form which includes scope of supply, delivery instructions and specifications, etc. together with any additional terms and conditions specific to an online auction event on the Website (which can be found through one or more links on the Website for the auction in question) constitute the Contract.<br />
									e.	Lot is a set of multiple batches of Product which will be provided for e-auction.<br />
									f.	&#34Products/Material&#34: shall mean stainless steel hot rolled or cold rolled flat products manufactured by the respective Seller at their respective facilities. <br />
									g.	 &#34Seller&#34 shall mean either Jindal Stainless (Hisar) Limited or Jindal Stainless Limited to sell its Products to the successful Bidders who are registered on the Website.<br />
									h.	&#34Services&#34 shall mean the service of transportation of the Products to the successful Bidder which will be provided at a cost over and above the successful Bidder Bid.<br />
									The scope of the services provided by the Website (the Services) may be amended by the Service Provider from time to time at its sole discretion and shall stand accepted by the Bidder.<br />
									i.	&#34Service Provider&#34 shall mean Jindal Stainless Corporate Management Services Pvt. Ltd. which is managing the e-auction sale of the Products of the Seller.<br />
									j.	Sale Notice means the each e-auction sale notice which shall amongst others provide the details of the Lot along with the base price thereof for inviting Bids from the Bidders and shall be accessible by the Bidder on the Website upon entering the one time password sent by the Service Provider to the Bidder on their registered e-mail id.<br />
									k.	&#34User&#34 - Any legal entity who uses the Website and/or the related Services provided by the Service Provider is considered as a User.<br />
									l.	&#34Website&#34 shall mean Jindal Online Auction Portal.<br />
									m.	&#34Working Day&#34 shall mean a day on which Bank are open for transactions in New Delhi.<br />
									n.	On accessing the Website or using the Services provided by the Service Provider or by registering as a Bidder/User you agree that you have read the General Terms and Conditions, including the terms and conditions specific to an online auction event.<br />
									o.	You are bound by these terms and conditions, including those specific to a particular online auction event.<br />
									
									</P>
									<h1>3.	MODIFICATIONS TO WEBSITE, SERVICES AND TERMS AND CONDITIONS</h1><br />
									<P align="justify">The Service Provider reserves the right to modify, amend, suspend or discontinue any aspect of the Services or the Website at any time without notice.</p><br />
									
									<P align="justify">The Service Provider reserves the right to amend any, or all of the terms and conditions without notice at any time at its sole discretion and announce the same on the Website. Accessing the Website and/or using the Services shall constitute acceptance of the terms and conditions including the amendments.</p>
									<br />
									<ol type="I">
									<h3>i.	USER ID, PASSWORD AND SECURITY</h3>
									<P align="justify">All Users will be allotted a unique user identification (User ID) and you will be prompted to enter a Password following due registration process defined on the Website. The password is a string of characters (combination of alpha, numeric & special characters) used to authenticate your identity and provide access to various resources on the Website. The User is solely responsible for all use and for maintaining and protecting the confidentiality of their User ID and password. User shall be solely and completely responsible for all transactions taking place on the Website using his/her User ID and Password and Website shall not be responsible in any manner. Furthermore, Users are entirely responsible for any and all activities which occur under their account. Users agree to immediately notify the Service Provider of any unauthorized use of their account or any other breach of security. Each User/Bidder is expected to observe a certain level of decorum in the usage of the Services.</p><br />
									
									<h3>ii.	FORGOT PASSWORD</h3>
									<P align="justify">In case you forget the log in password, the only way you can change your log in password is by clicking on the &#34Forgot Password&#34 link on the log-in page of the Website. You will receive a verification code on your registered e-mail address.. Using the verification code you can change your log in password.<p><br />
									
									<h3>iii.	TRANSACTION PASSWORD</h3>
									<P align="justify">To have an additional layer of security, Bidders participating in auctions must set up a transaction password. Participating Bidders must enter this transaction password to authenticate their entry into the virtual auction room and place their Bids. Bidders can set their transaction password and have to confirm the same with the OTP that will be sent to their registered e-mail ID. Transaction password is online auction specific.</p><br />
									
									<h3>iv.	TIME</h3>
									<P align="justify">Time wherever mentioned in any page of the Website refers to Indian Standard Time (IST). All the timings of the online auction shall be based on the time indicated by the Server hosting the auction engine. It shall be the endeavor of the Service Provider to ensure that the Server time reflects as closely as possible the Indian Standard Time (IST). However, in the event of any deviations between the Server Time and the Indian Standard Time, the functioning of the auction engine (including Start, Operation & closure of auctions) would be guided by the Server time. The exact time left for participation in the online auction shall be displayed on the log-in page of the Website.</p><br />
									
									<h3>v.	TERM OF AGREEMENT</h3>
									<P align="justify">This Agreement shall continue to be in full force and effect for so long as you are using the Website and its Services. The Service Provider provides the online auction platform wherein the Bidders can, bid and buy the Products listed on the Website pursuant to the terms and conditions set forth below.</p><br />
									</pre>
									</ol>
									<h1>4.	GENERAL TERMS AND CONDITIONS:</h1><br />
									1.	The bidding shall be completely online and the participation and sale shall be subject to the terms and conditions mentioned in succeeding paras:<br />
									
									2.	Material or Product Specifications: Bidders are advised that  the same are as per &#34as is where is &#34basis and no complaints regarding the same shall be entertained.  Bidders are advised to thoroughly study the specification of the Lot in the Sale Notice.<br />
									3.	The Service Provider does not give warranty or guarantee of the quality, quantity, measurement, condition and about its &#34End Use&#34 or fitness for a particular purpose.<br />
									4.	The highest/lowest Bidder does not get any right to demand acceptance of his bid/offer. Seller reserves the right to accept/reject/cancel any bid/offer, withdraw any portion of the Products/Lot at any stage from auction, even after acceptance of bid/offer, issue of order acceptance without assigning any reason thereof. In the event of such rejection, cancellation or withdrawal, the Service Provider shall not be responsible for any damages/loss to the successful Bidder.<br />
									<h1>5.	Earnest Money Deposit:</h1><br />
									
									a)	Bidders willing to participate in the e-auction   shall have to deposit interest-free earnest money deposit (EMD) of INR 5,00,000 (Rupees Five Lakhs) in favour of the Seller after registering on the Website by way of remittance of funds through NEFT/RTGS  or by way of Demand Draft/Pay Order.<br />
									b)	EMD submitted by the Bidder shall be forfeited, if the Bidder:<br />
									i.	Withdraws or modify or impairs or derogates from the bid in any respect within the period of validity of its bid; or<br />
									ii.	 If it comes to notice that the information / documents furnished in its Bid is false, misleading or forged.<br />
									c)	The EMD will not be adjusted along with the total price payable for a Lot of the Products. In order to lift the Products the successful Bidder shall have to make 100% payment of the confirmed Lot.<br />
									d)	Where the EMD is less than the amount as mentioned as above then such Bid/offer shall be treated as invalid.<br />
									e)	If the Bidder chooses not to participate in any future e-auction he/she can apply for refund of the EMD which shall be refunded after verifying his account details. <br />
									
									<h1>6.	Products for e -auction:</h1><br />
									
									6.1.	The Seller intends to auction the Products as provided in Lot wise list.<br />
									6.2.	A Bidder shall not be allowed to select partial quantity from a Lot of Products and 100% selection of a particular Lot of Products shall be compulsory. Any Bid which is conditional shall be out rightly rejected. The frequency and Lot size of the Products shall be decided by the respective Seller and shall be made available to the Bidder through the Sale Notice. <br />
									
									<h1>7.	Bidding Process:</h1><br />
									
									7.1.	The Bidders are requested to ensure correctness of the information provided by them before placing the Bid. All Bids placed are legally valid Bids and are to be considered as Bids from the Bidder himself/ herself. Once the Bid is placed, the Bidders cannot withdraw the Bid for whatever reason.
									For auction of a particular Lot, the decision regarding the successful Bidder shall be made available on the registered e-mail id of the Bidder or through the online portal. . The successful Bidder will also be contacted by the Service Provider through order acceptance and will be intimated the total value payable in respect of the successful Bid which will be inclusive of freight to be paid by the successful Bidder for lifting the Lot of Products for which his/her Bid was successful.<br />
									7.2.	The Bidder shall have to deposit the  total  value of the  Lot within 3 Working Days of intimation to the Bank Account of the respective Seller. The Products pertaining to the Lot shall be delivered to the successful Bidder within 10working days subject to logistics availability.<br />
									7.3.	If the successful Bidder fails to make payment of the total value within the time specified above, the EMD shall be liable to be forfeited. The decision of Seller shall be final and binding in this regard.<br />
									7.4.	Bidders  are  required   to  quote  the &#34basic&#34  price  per metric ton  excluding  of  the applicable taxes  & freight for every category of Product as per the Lot-wise list. <br />
									7.5.	Seller reserves the right to cancel the e-auction or reject any bid at any time without assigning any reason thereof and without any costs, risks or consequences.<br />
									7.6.	The Service Provider expressly reserves the right to terminate the use of, or to refuse to permit the use of, the Services and the Website by any person or entity, at the sole discretion of the Service Provider, for any reason and without notice.<br />
									
									
									<h1>8.	Delivery period and payment:</h1>
									
									8.1.	Seller shall arrange to deliver the Product on &#34as is where is&#34 directly to the successful Bidder subject to full truck load quantity from respective location at the time of dispatch within 10 (ten) working days from the date of all commercial clearances.<br />
									
									8.2.	In case the successful Bidder fails to pay the total value of the allotted LOT within 3(three) working days subsequent to being intimated as provided under clause 6.2 herein above, Seller shall have the right to de-allocate the Lot , at Seller&#39s sole discretion.<br />
									
									8.3.	Seller shall be responsible for delivery, logistics and transportation of the Product to the successful Bidder.<br />
									
									8.4.	Taxes at the prevailing rate will be applicable for sale within or beyond the territory of the state where the Product is located.<br />
									8.5.	It is mandatory to stamp and sign the delivery order while taking delivery at the specified location. Bidders are requested to keep the rubber stamp at the yard and instruct accordingly. Non-adherence to this condition shall be deemed as confirmation by the Bidder of delivery of Product and the Bidder shall have no right to raise any dispute in this regard.<br />
									</br>
									</br>
									<h2>GENERAL RULES AND REGUL ATION GOVERNING CONDUCT OF ONLINE-AUCTIONS</h2>
									
									<h1>INTRODUCTION:</h1>
									
									<P align="justify">This Online Forward Auction is being conducted for Jindal Stainless Hisar Limited and Jindal Stainless Limited (herein after referred as the &#34Client(s)&#39&#39) on the auction platform of JSCMS (hereinafter referred as &#34Service Provider&#34).</p><br />
									
									<P align="justify">The General Rules and Regulations (&#34Rules&#34) provided herein govern the conduct of Online Forward Auctions arranged by &#34Service Provider&#34 on its Auction Platform. These rules cover the roles and responsibilities of the parties in the Online Forward Auctions on the Auction Platform. Acceptance and strict adherence to these General Rules and Regulations governing conduct of online auctions and General Terms and Conditions for Sale of Materials from Client(s) is a pre-requisite for securing  participation in the online auctions.</p><br />
									
									<h1> ROLE OF SERVICE PROVIDER </h1>
									
									1. Service Provider is the agency (operator) primarily providing the platform and service of the online Forward 	auction to the Client(s).<br />
									2. Finalization of the auction items in consultation with the Seller.<br />
									3. Defining of Bidding rules for each auction in consultation with the Client(s).<br />
									4. Enhancing bidder awareness of and comfort with the auction mechanism. <br />
									5. Collection of EMD. from the Bidders and forwarding the same to the Client(s).<br />
									6. Providing access to the approved Bidders to participate in the auction.<br />
									7. Summarizing the auction proceedings and communicate the outcome to the Client(s).<br />
									
									<P align="justify">The responsibility of fulfilment of the Contract rests between the Bidders and the Client(s) and the Service Provider shall not be held responsible in this respect.</p><br />
									
									<h1>ROLE OF BIDDER:</h1>
									
									<P align="justify">The role of the Bidder is outlined below:</p>
									
									1.	The Bidders, before placing the order on the Website, should have the required approvals and sanctions of the competent authorities as per applicable laws.<br />
									2.	On award of the Contract(s), it would be construed that the Bidder has obtained all necessary sanctions of the competent authority and adequate funds are available with respect to the awarded Contract(s).<br />
									3.	The Bidders should not exercise any corrupt influence on any aspect of Contract and commit to take all measures necessary to prevent corruption maintaining complete transparency and fairness in all activities related to the Website.<br />
									4.	Further, Bidders are not allowed to place any off-line contract to the Seller directly based on the outcome of e-auction  conducted on Website.<br /> All such contracts shall be treated as null and void and Website reserves the right to delete all data related to such transactions from Website database besides taking suitable actions against such Bidders including suspension from Website.<br />
									
									5.	In the event of winning an allotment in the auction mechanism, the successful Bidder shall commit to fulfill outlined obligations under the Contract.<br />
									
									6.	The Bidders shall bid on the terms specified by the Client & place their bids in the auction engine in the manner specified by &#34Service Provider&#34 and shall not stipulate any conditions on and of their own. Bids entered with conditions attached shall be considered Conditional Bids & &#34Service Provider&#34 retains the right to reject these bids even without intimating the Client(s).<br />
									
									<h1>BIDDING RULES</h1>
									<P align="justify">The Bidding Rules refer to the information and terms defined specifically for a particular auction. The purpose of the
									Bidding rules is to provide approved bidders with the information and terms specific to the auction in which they are bidding. This would include:</p><br />
									<ul style=&#34list-style-type:square;&#34>
									  <li>Start Time and duration of the auction</li>
									  <li>Any extension of the duration of the auction in the event of bids being received towards the end of the pre -specified duration</li>
									  <li>Start Bid Price</li>
									  <li>Specified Unit for Bidding</li>
									 <li>Price Increments and any reduction in the price increment in the auction in the event of inactivity other attributes (informational/non-negotiable in nature)</li>
									</ul>
									
									<P align="justify">While it shall be the endeavor of &#34Service Provider&#34 to specify these rules at the earliest for each online auction, the &#34Service Provider&#34 shall retain the right to delay the announcement of these bidding rules or modify rules specified earlier at the time of the online bidding. These details would be available to the Bidders on th e Auction Engine at the time of Bidding. Participation in the auction process presumes complete awareness and understanding of the bidding rules.</p><br />
									
									
									
									<h1>CONDUCT OF THE AUCTION:</h1>
									<P align="justify">Only those bidders who have been approved by the &#34Client(s)&#34 and have submitted stamped and manually signed &#34General Rules and Regulations governing conduct of online auction along with Letter of Interest, required EMD amount and other necessary documents to the &#34Client(s)&#34/ &#34Service Provider&#34 within the specified time will be given &#34User ID&#34 and &#34PASSWORD&#34 to enable them to view and participate in online auction.</p><br />
									
									<P align="justify">The Auction shall be conducted on pre-specified date. The Key Terms pertaining to the conduct of Auction such as &#34START TIME&#34, &#34DURATION&#34, &#34END TIME&#34 AND &#34AUTO EXTENSION FACILITY&#34 Shall be specified separately for each Auction.</p><br />
									&#34Service provider&#34 retains the right to cancel or reschedule the auction, with the approval of the competent authority of the Client(s), 
									
										<P align="justify">The duration of auction may also vary from the pre-specified period of time either on account of termination of the auction by &#34Service Provider&#34 on the advice of the Client(s); or in case of situations where it is felt that continuance of the auction proceedings is prejudicial to the smooth conduct and / or the integrity of the auction process; or due to auto extension during the Auction.</p><br />
									
										<P align="justify">In the event of any problems being faced in the smooth conduct of the auction, &#34Service Provider&#34 with the approval 	of the Competent Authority of the Client(s), shall have the right to undertake one or more of the following steps:</p><br />
									Cancellation/ premature termination of the auction with/ without a subsequent rerun of the auction
									Cancellation of a bid
									Locking / deactivating a bidder&#39s account (suspension of operations in the account), etc.
									
									<h1>LIABILITY OF &#34SERVICE PROVIDER&#34 </h1>
									
									&#34Service Provider&#34 shall not be liable to the Bidders participating in the auction or any other person(s) for:<br />
									
									
									
									<ul style=&#34list-style-type:square;&#34>
									  <li>Any breach of contract by any of the parties in the fulfillment of the underlying contract.</li>
									  <li>Any delays in initiating the online auction or postponement / cancellation of the online auction proceedings due to any problem with the hardware / software / infrastructure facilities or any other shortcomings.</li>
									</ul>
									
									<P align="justify">While, reasonable care and diligence will be taken by &#34Service Provider&#34 in discharge of its responsibilities such as design of the online bid, communication of bid details and rules, guidance to Client(s)/ bidders in accessing the Auction Engine and placing bids, etc. the bidders shall specifically indemnify &#34Service Provider&#34 from all liabilities for any shortcomings on these aspects.</p>
									
									<P align="justify">It is clearly understood that these activities are undertaken by &#34Service Provider&#34 to assist the bidders in participation but the ultimate responsibility on all these counts lies totally with the bidders.</p>
									
									<h1>DISCLAIMER</h1>
									<P align="justify">The Service Provider will not be held responsible for any failure of power, network, server, hosting server, Internet connectivity, ISP or otherwise at Bidder&#39s end or at Service Provider directly or indirectly affecting online method of Bidding.</p>
									 	<P align="justify">Service Provider takes no responsibility of the quality, quantity, documentation details of Bidders. The Bidders agree to 	indemnify and hold harmless the Service Provider from any loss, damage, cost and expenses caused by any reason during 	the transactions on the Website. In no event shall Service Provider be liable for any loss for the business, revenues, profit, 	costs direct and incidental, consequential or punitive damages of any claim.</p>
									
									<h1>RIGHT OF THE CLIENT(S):</h1>
									
									<P align="justify">The Client(s) reserves the right to partially or totally accept or reject any / all bids placed in the Online Auction without assigning any reason whatsoever. The decision of the Client(s) would be final and binding on the bidder in any such case.</p>
									
									
									<h1>CONFIDENTIALITY CLAUSE:</h1>
									&#34Service Provider&#34 <P align="justify">Undertakes to handle any confidential information provided by the Client(s) or confirmed bidders for the auctions conducted with utmost trust and confidentiality.</p>
									
									<h1>GOVERNING LAW AND JURISDICTION</h1>
									
									1. This agreement is governed and construed in accordance with the laws of India. Users hereby irrevocably consent to the exclusive jurisdiction and venue of courts in New Delhi, India in all disputes arising out of or relating to the use of the Services and the Website.<br />
									2. Users agree to indemnify and hold Service Provider and its officials and employees harmless from any claim, demand or damage asserted by any third party due to and arising out of use of Services and the Website.<br />
									
									<h1>FORCE MAJEURE</h1>
									<P align="justify">Service Provider/Clients shall not be liable for any failure or delay in performance due to any cause beyond its control including fire, strike, go-slow, lock-out, closure, theft, dislocation of normal working conditions, internet or network non-availability, accident, war, riots, civil commotion, political upheaval, epidemics, break-down of machinery, any terrorist activity, any natural calamity, adverse weather or climatic conditions, or any other causes or conditions beyond the control of Service Provider/Clients, whether directly due to or in consequence of the aforesaid causes and the existence of such causes or consequences shall operate to extend the time of performance till the cause of delay shall have ceased to exist.</p><br />
									</p>
									
									
									
									
									
									  </article>
									</section>
                                </div><br>
                            </div>
                            <div class="modal-footer">
                                <div id="otpStausPending" class="left-float"><input type="checkbox" id="tnc_check">I Agree</div>
                                <button class="btn btn-primary" id="tnc_accept">Accept</button>
                                <button class="btn btn-primary" type="submit" onclick="hideModal()">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                
                 

<script	type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/jquery.min.js"></script>
<script type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/browser.min.js"></script>
<script	type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/breakpoints.min.js"></script>
<script type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/util.js"></script>
<script type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/main.js"></script>
<script	type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/jsCalendar.js"></script>
    
<script type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/upcoming_auction.js"></script>

 				<script>
						  $(document).ready(function(){
								var headerheight = $("header").height();
								console.log(headerheight);
								var maincontantheight = $(window).height() - headerheight;
								console.log(maincontantheight);
								$(".main__inner-wrapper").height(maincontantheight);
								$(".main-tableDiv").height(maincontantheight - 120);
								$("#sidebar").height(maincontantheight);
							});
 				 </script>
