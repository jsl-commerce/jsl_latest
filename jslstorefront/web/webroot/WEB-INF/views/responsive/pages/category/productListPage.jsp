<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>


<template:page pageTitle="${pageTitle}">

<div id="loading-wrapper" style="display: none;">
    <div class="spinner">
  <div class="rect1"></div>
  <div class="rect2"></div>
  <div class="rect3"></div>
  <div class="rect4"></div>
  <div class="rect5"></div>
  <div class="rect6"></div>

  </div>
</div>

<div id="plant_warehouse_message" class="alert alert-success"></div>
<div id="updatedMessageCart" class="alert alert-success"></div>
		<div class="yCmsContentSlot container-fluid"></div>
      <a id="skip-to-content"></a>
      <div class="main__inner-wrapper">
         <div class="product_details_container">
				<div>
					<cms:pageSlot position="ProductLeftRefinements" var="feature" element="div">
						<cms:component component="${feature}" element="div" />
					</cms:pageSlot>
				</div>
			</div>
			
		 <div id="content" class="plp_flat_product">		
			
			
	
			 <div class="product_content">	
			 <div class="row">
			 <div class="col-md-12">   		  
				   <h1>Flat Products</h1>
				   </div>	
				 
			</div>
				   
				   <div class="product_main_box">
				   
				 <div class="top_filter">		
				<select>
							  <option value="both">Both</option>
							  <option value="jsl">JSL</option>
							  <option value="jshl">JSHL</option>
							 
				</select>
			</div>
			
					<cms:pageSlot position="ProductListSlot" var="feature" element="div" >
						<cms:component component="${feature}" element="div" />
						
						
					</cms:pageSlot>
				</div>
				
		</div>
		</div>
		</div>
	
<div id="showbuyErrorModel" class="showErrorMessage">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <p>Please Choose minimum 3 Quantity..</p>
  </div>

</div>



</template:page>


