 <div class="faqDiv container">
<h2>Frequently Asked Questions (FAQ's)</h2>

<h3>Password</h3>
<h4>How To Change Password</h4>
<p>In My profile go to Password. Enter current password -> New Password -> Confirm New Password & update.</p>

<h4>Your Account is Blocked, Please Contact Your Administrator</h4>
<p>If you get the above message, Probably you might have entered wrong password several times. </p>

<p>In order to activate the account please contact the order services team. Contact: <u>order.services@jindalstainless.com</u>

</p>

<h3>DashBoard</h3>
<h4>Graphical Representation</h4>
<p>Dashboard has five reports which are real time information display from our ERP system.</p>

<p>This tile gives customer a snapshot of over dues, Yesterday & Month till date Billing, Yesterday & Month till date  Order booking, POP status.</p>


<h3>Account 360</h3>
<h4>Detailed Information</h4>
<p>There are five reports in the account 360 tab.</p>

<p>These reports can be downloaded in MS Excel format by clicking on the Export data tab.</p>


<h3>Place Order</h3> 
<h4>Place Order Flow</h4> 
<p>Place order allows the customer to book the order online. This certainly reduces the booking time in the ERP. </p>

<p>In "place order" there are supply Location based products display, So In case the customer does not finds the required product in Warehouse Location. He can select Plant Location

This feature gives an easy navigation for selecting the sizes by applying filters.</p>

<p>You can select the required product -> Input order quantity -> Add to Cart</p>

<h3>Cart</h3>
<h4>Plant & Warehouse Location</h4>
<p>Once the items in Warehouse location are added in cart, they are required to checked out from the cart in order to add new items from plant

</p>

<h4>Shipping Address</h4>
<p>In cart you can add shipping address, In case you want to add the a new shipping address - please contact the respective Key account Manager.<br>

Based on the Shipping address the local freight will be determined.</p>

<h4>Shipment Method</h4>

<p>You can select the shipping method from the drop down menu.</p>

<h3>Product Enquiry</h3>

<p>This feature enbles the customer to place the enquiry of non generic or customise sizes.
<br> 
After Submitting the enquiry, An enquiry ID will be generated for reference discussion with the Key account manager.<br>

This enquiry will directly get into our CRM tool & assigned to the respective key account manager for further understanding the requirement.</p>

<h3>Support</h3>
<p>Under this heading we provide the feature of lodging the complaints related to Service and Product.<br>

Customer will be issued a ticket for tracking the complaint.<br>

Customer can check the status of the Complaint in My profile -> Support Ticket
</p>

<h3>Make Payments</h3>
<p>This feature allows customer to pay online, the due invoices will be displayed under this heading.
<br> 
Customer can select the multiple invoices and pay online.</p>
</div>