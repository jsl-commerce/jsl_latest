<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
   <head>
      <title>JSL Site</title>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
      <link rel="stylesheet" type="text/css" media="all" href="./JSL Site_files/style.css">
      <link rel="stylesheet" type="text/css" media="all" href="${commonResourcePath}/customAssets/AuctionPage/css/customer360.css" />
      <link rel="stylesheet" type="text/css" media="all" href="./JSL Site_files/main.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
      <script src="https://cdn.anychart.com/js/8.0.1/anychart-core.min.js"></script>
      <script src="https://cdn.anychart.com/js/8.0.1/anychart-pie.min.js"></script>
      <script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
      <script type="text/javascript"
         src="${commonResourcePath}/customAssets/js/customerFeedbackAnalysis.js"></script>	
      <!-- Numeric JS -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/numeric/1.2.6/numeric.min.js"></script>
   </head>
   <body class="page-auctionHomePage pageType-ContentPage">
      <div class="yCmsContentSlot container-fluid"></div>
      <a id="skip-to-content"></a>
      <div class="main__inner-wrapper">
         <div id="loading-wrapper">
            <div class="spinner">
               <div class="rect1"></div>
               <div class="rect2"></div>
               <div class="rect3"></div>
               <div class="rect4"></div>
               <div class="rect5"></div>
               <div class="rect6"></div>
            </div>
            <div class = "loader_text">
               <blinkLoaderText>Retrieving live data...please wait</blinkLoaderText>
            </div>
         </div>
         <!-- <div class="product_details_place_order_container"> -->           
         <div id="content">
            <div>
               <select id="compny_data">
                  <option value="both">Both</option>
                  <option value="jsl">JSL</option>
                  <option value="jshl">JSHL</option>
               </select>
            </div>
            <div id="successDataCheck">
               <div class="product_content">
                  <h2>Customer Feedback Report </h2>
                  <div class="cust360_main_box">
                     <!--<div><button class="my_enq_btn">My Enquiry</button>
                        <button class="my_enq_btn">New Enquiry</button>
                        </div> -->
                     <!-- <div class="product__header">HPL-F05-N1-PRIME-400 series-IRSM44-97-409M Group-10-2500-790-0.953-NO1</div>   -->
                     <div class="product__body">
                        <div class ="row">
                           <div class="filter_box" >
                              <div class="col-md-4" id="displayAllId">
                                 <div class="p_attribute_item_title first">
                                    Filter By : 									
                                    <select id="categoryID" onchange="filterOnCategory()">
                                       <option value="all" selected ="">All</option>
                                       <option value="custId" >Customer ID</option>
                                       <option value="questn" >Questions</option>
                                       <option value="rating" >Rating</option>
                                    </select>
                                    <button class="cust360_report_btn" id="resetFilterBtn" onclick="resetProductFilter()">Reset Filter</button>
                                 </div>
                                 <!-- <div class="cust360_border">
                                    <div id="productTypeVsMktBal"></div>
                                    <div id="containerProduct"></div>
                                    </div> -->
                              </div>
                              <div class="col-md-8" style="display : none" id="displayCustId">
                                 <div class="p_attribute_item_title first">
                                    Filter By Customer ID : 									
                                    <select id="productSel" onchange="filterOnCustomerID()">
                                       <option value="allCustomerID" selected="" id="custIdAll" >All</option>
                                    </select>
                                    <button class="cust360_report_btn" id="resetFilterBtn" onclick="resetCustomerIdFilter()">Reset Filter</button>
                                 </div>
                                 <!-- <div class="cust360_border">
                                    <div id="productTypeVsMktBal"></div>
                                    <div id="containerProduct"></div>
                                    </div> -->
                              </div>
                              <div class="col-md-8"  style="display : none" id="displayQuestId">
                                 <div class="p_attribute_item_title second">
                                    Filter By Questions :										 						
                                    <select id="finishSel" onchange="filterOnQuestions()"  style = "overflow: scroll; height: auto; max-height: 100px;">
                                       <option value="allQuestion" selected="allQuestionsID">All</option>
                                       <option value="q1" >Q1</option>
                                       <option value="q2" >Q2</option>
                                       <option value="q3" >Q3</option>
                                       <option value="q4" >Q4</option>
                                       <option value="q5" >Q5</option>
                                       <option value="q6" >Q6</option>
                                       <option value="q7" >Q7</option>
                                       <option value="q8" >Q8</option>
                                       <option value="q9" >Q9</option>
                                       <option value="q10" >Q10</option>
                                       <option value="q11" >Q11</option>
                                       <option value="q12" >Q12</option>
                                       <option value="q13" >Q13</option>
                                    </select>
                                    <button class="cust360_report_btn" id="resetFilterBtn" onclick="resetQuestionFilter()">Reset Filter</button>		 
                                 </div>
                                 <!-- <div class="cust360_border">
                                    <div id="finishVsMktBal"></div>
                                    <div id="containerFinish"></div>
                                    </div>	 -->						
                              </div>
                              <div class="col-md-8"  style="display : none" id="displayRatingId">
                                 <div class="p_attribute_item_title last">
                                    Filter By Rating : 									
                                    <select id="qualitySel" onchange="filterOnRating()">
                                       <option value="allRatingVal" selected="allRatingID">All</option>
                                    </select>
                                    <button class="cust360_report_btn" id="resetFilterBtn" onclick="resetRatingFilter()">Reset Filter</button>
                                 </div>
                                 <!-- <div class="cust360_border">
                                    <div id="qualityVsMktBal"></div>
                                    <div id="containerQuality"></div>
                                    </div> -->
                              </div>
                           </div>
                        </div>
                        <div class="cust360_border">
                           <div id="productTypeVsMktBal"></div>
                           <div id="containerProduct"></div>
                        </div>
                        <br>
                        <div class = "row">
                           <div class ="disclaimer col-md-4">Q1 - Submission of clear Techno-Commercial Quotation - On Time</div>
                           <div class ="disclaimer col-md-4">Q2 - Order Acceptance Information</div>
                           <div class ="disclaimer col-md-4">Q3 - Feedback on status of order</div>
                        </div>
                        <div class = "row">
                           <div class ="disclaimer col-md-4">Q4 - On time delivery of the product</div>
                           <div class ="disclaimer col-md-4">Q5 - Marking/ Packaging/ Shipment/ Safe receipt of material</div>
                           <div class ="disclaimer col-md-4">Q6 - Product Quality with respect to competitor - Surface</div>
                        </div>
                        <div class = "row">
                           <div class ="disclaimer col-md-4">Q7 - Product Quality with respect to competitor - Dimensions & Tolerances</div>
                           <div class ="disclaimer col-md-4">Q8 - Product Quality with respect to competitor - Mechanical Properties</div>
                           <div class ="disclaimer col-md-4">Q9 -Documentation at the time of dispatch like INVOICE/ GR/TEST CERTIFICATE - Presentation</div>
                        </div>
                        <div class = "row">
                           <div class ="disclaimer col-md-4">Q10 - Order Acceptance Information</div>
                           <div class ="disclaimer col-md-4">Q11 - Feedback on status of order</div>
                           <div class ="disclaimer col-md-4">Q12 - On time delivery of the product</div>
                           <div class ="disclaimer col-md-3">Q13 - Marking/ Packaging/ Shipment/ Safe receipt of material</div>
                        </div>
                        <!-- Table started -->
                        <div class="main-tableDiv sticky-table">
                           <!-- <caption class="caption_add">Added Enquiries</caption><br>  -->
                           <div class="row">
                              <div class="col-md-9 cust360_text_data">
                                 <h3>Customer Feedback Data</h3>
                              </div>
                              <div class="col-md-3">
                                 <div><button class="cust360_export_btn btn btn-primary" onclick="excelComplaintAdminFeedbackExport()">Export Data</button>
                                 </div>
                              </div>
                           </div>
                           <div class="pr_table_show cust360_table_display" style = "overflow: scroll; height: auto; max-height: 400px">
                              <table class ="table table-striped table-bordered ordertable-striped order" id="customerFeedbackTableID" ">
                                 <thead>
                                    <tr class="sticky-header">
                                       <th>Customer ID</th>
                                       <th>Complaint Number</th>
                                       <th>Q1</th>
                                       <th>Q2</th>
                                       <th>Q3</th>
                                       <th>Q4</th>
                                       <th>Q5</th>
                                       <th>Q6</th>
                                       <th>Q7</th>
                                       <th>Q8</th>
                                       <th>Q9</th>
                                       <th>Q10</th>
                                       <th>Q11</th>
                                       <th>Q12</th>
                                       <th>Q13</th>
                                       <th>Comment</th>
                                       <th>FeedBack Date</th>
                                    </tr>
                                 </thead>
                                 <tbody id="customerFeedbackTableBody">
                                 </tbody>
                              </table>
                           </div>
                        </div>
                        <!-- <br>
                           <div class ="disclaimer">* Information provided in the site is indicative and for information only.In case of any difference/clarity, please refer to our Customer Support Team.</div>
                                             -->
                     </div>
                  </div>
               </div>
            </div>
            <div id="failureDataCheck">
               <div class="product_content">
                  <div class="no_data_cust360_border">
                     <span class = "noData_text">Oops! Something went wrong. Please retry</span>
                  </div>
               </div>
            </div>
         </div>
         <!-- </div> -->
      </div>
      </div>
      </main>
      </div>
      <script>
         function excelExport()
         {
         window.location.href="${request.contextPath}/reports/exportReport?report=POPPLANTREPORT";
         }
         
         
         function excelComplaintAdminFeedbackExport()
         {
         window.location.href="${request.contextPath}/my-account/customerFeedbackAnalysis";
         }
         
          
      </script>
   </body>
</html>