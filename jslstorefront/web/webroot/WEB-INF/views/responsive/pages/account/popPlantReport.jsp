<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html>
   <head>
      <title>JSL Site</title>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
      <link rel="stylesheet" type="text/css" media="all" href="./JSL Site_files/style.css">
        <link rel="stylesheet" type="text/css" media="all" href="${commonResourcePath}/customAssets/AuctionPage/css/customer360.css" />
      <link rel="stylesheet" type="text/css" media="all" href="./JSL Site_files/main.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
		  <script src="https://cdn.anychart.com/js/8.0.1/anychart-core.min.js"></script>
      <script src="https://cdn.anychart.com/js/8.0.1/anychart-pie.min.js"></script>
	  <script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
	  <script type="text/javascript"
		src="${commonResourcePath}/customAssets/js/popPlantReport.js"></script>	
	  
	
	  
  <!-- Numeric JS -->
			<script src="https://cdnjs.cloudflare.com/ajax/libs/numeric/1.2.6/numeric.min.js"></script>
   </head>
   <body class="page-auctionHomePage pageType-ContentPage">
      <div class="yCmsContentSlot container-fluid"></div>
      <a id="skip-to-content"></a>
      <div class="main__inner-wrapper">
     <div id="loading-wrapper">
		    <div class="spinner">
				  <div class="rect1"></div>
				  <div class="rect2"></div>
				  <div class="rect3"></div>
				  <div class="rect4"></div>
				  <div class="rect5"></div>
				  <div class="rect6"></div>
					
		  </div>
		  <div class = "loader_text"><blinkLoaderText>Retrieving live data...please wait</blinkLoaderText></div>
	</div>
         <!-- <div class="product_details_place_order_container"> -->           
            <div id="content">		
			<div>			
				<select id="compny_data">
							  <option value="both">Both</option>
							  <option value="jsl">JSL</option>
							  <option value="jshl">JSHL</option>							 
				</select>
			</div>
			<div id="successDataCheck">
               <div class="product_content">		   		  
				   <h2>POP Plant Report </h2>		 			  
                  <div class="cust360_main_box">
					<!--<div><button class="my_enq_btn">My Enquiry</button>
						<button class="my_enq_btn">New Enquiry</button>
					</div> -->
                    <!-- <div class="product__header">HPL-F05-N1-PRIME-400 series-IRSM44-97-409M Group-10-2500-790-0.953-NO1</div>   -->
                     <div class="product__body">
					 
						<div class ="row">  
							<div class="filter_box" >
							
							 <div class="col-md-4">					
										<div class="p_attribute_item_title first">Filter By Product : 									
										<select id="productSel" onchange="filterOnProductType()">
											<option value="" selected="" >All</option>
										</select>									 
										 <button class="cust360_report_btn" id="resetFilterBtn" onclick="resetProductFilter()">Reset Filter</button>
									</div>
									<div class="cust360_border">
										<div id="productTypeVsMktBal"></div>
										<div id="containerProduct"></div>
									</div>
						  </div>	
							
								<div class="col-md-4">
									 <div class="p_attribute_item_title second">Filter By Finish :										 						
										  <select id="finishSel" onchange="filterOnFinish()">
										 	<option value="" selected="">All</option>					 
										 </select>
										  <button class="cust360_report_btn" id="resetFilterBtn" onclick="resetFinishFilter()">Reset Filter</button>		 
									</div>
									
									<div class="cust360_border">
										<div id="finishVsMktBal"></div>
										<div id="containerFinish"></div>
									</div>							
								</div>
							
							<div class="col-md-4">
									<div class="p_attribute_item_title last">Filter By Quality : 									
										<select id="qualitySel" onchange="filterOnQuality()">
											<option value="" selected="">All</option>	
										</select>								 
    										 <button class="cust360_report_btn" id="resetFilterBtn" onclick="resetQualityFilter()">Reset Filter</button>
									</div>							
									<div class="cust360_border">
										<div id="qualityVsMktBal"></div>
										<div id="containerQuality"></div>
									</div>
						   </div>
						   
					</div>
               </div>
               <br>
					<div class ="disclaimer">* Information provided in the site is indicative and for information only.In case of any difference/clarity, please refer to our Customer Support Team.</div>
					 
					 <!-- Table started -->
					 
                     <div class="main-tableDiv sticky-table">
					 
					 <!-- <caption class="caption_add">Added Enquiries</caption><br>  -->
					 <div class="row">
						<div class="col-md-9 cust360_text_data">
							<h3>POP Plant Data</h3>
						</div>
						
						<div class="col-md-3">
							<div><button class="cust360_export_btn btn btn-primary" onclick="excelExport()">Export Data</button>
						</div>
						</div>
				
					 </div>
					  
					 <div class="pr_table_show cust360_table_display">
					  <table class ="table table-striped table-bordered ordertable-striped order" id="cust360PopTable">
					  
					   <thead>
						 <tr class="sticky-header">
						    <th>S.O</th>
							<th>Item</th>
							<th>Product Type</th>
							<th>So Date</th> 
							<th>Grade</th>
							<th>Thickness</th>
							<th>width</th> 
							<th>Length</th>
							<th>Edge Condition</th> 
							<th>Finish</th>
							<th>Quality</th>
							<th>Basic Rate</th> 
							<th>Ord_Qty</th>
							<th>Desp_Qty <span style ="font-size:10px;">(In Curr. Month)</span></th>
							<th>MKT_Bal</th> 
							<th>Alloc_Qty</th>
							<th>OA NO</th>
							<th>Customer Req delv</th> 
							<th>Hold Status</th>
							<th>PW Min</th>
							<th>PW Max</th>
						  </tr>
						  </thead>
						  <tbody id="popTable">
					  </tbody>
					</table>
					 </div>	
										 
					</div>
					
                     </div>
					 </div>
					 
					
					
                    
                  </div></div>
                  
                  	  <div id="failureDataCheck">
               	 <div class="product_content">
	               
	               <div class="no_data_cust360_border">
	              			<span class = "noData_text">Oops! Something went wrong. Please retry</span>
	              			
	               </div>
               </div>
               
               </div>
               
			    </div>
            <!-- </div> -->
         </div>
      </div>
      </main>
      </div>
	  
	  <script>
			
	  function excelExport()
		{
			window.location.href="${request.contextPath}/reports/exportReport?report=POPPLANTREPORT";
		}

    </script>
	  
   </body>
</html>