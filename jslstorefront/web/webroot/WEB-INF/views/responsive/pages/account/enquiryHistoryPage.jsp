<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<script >
	function toggleAccordion(id) {
		$(id).toggle();
	}
</script>  
	  <link rel="stylesheet" type="text/css" href="${commonResourcePath}/customAssets/AuctionPage/css/productEnquiry.css" />
   <div class="page-auctionHomePage pageType-ContentPage">     
         <div class="product_details_place_order_container">            
           
               <div class="product_content">
				  	 			  
                  <div class="cust360_main_box">
                     <div class="product__body enquiry_layout container">
					 
					
					 <!-- Table started -->
					 
                     <div class="main-tableDiv sticky-table">
					 
					 <!-- <caption class="caption_add">Added Enquiries</caption><br>  -->
					
						<div >
						    <h1>Enquiry History </h1>
						   
						</div>
				
					 </div>
					  
					 <div class="pr_table_show cust360_table_display">
					 <!-- start -->
					 
					   <table id="table_1" class="table table-bordered table_auctionData">
                                    <thead id="myTable">
                                    <tr class="sticky-header">
                                        <th title="Click On Enquiry Number for Product Details">Enquiry Number</th>
                                        <th>Created Date</th>
                                        <th>KAM Name</th>
                                        </tr>
                                    </thead>
                                    <tbody class="tableFix" id="productEnquiryTable">
                                    <c:choose>
	                                   	<c:when test="${productEnquiryHistory.size() != 0}">
	                                   		<c:forEach items="${productEnquiryHistory}" var="productEnquiry" varStatus="loop">
                                            <tr id="row_${loop.index}" class="lotRow">
                                                <td><a id="lot_${loop.index}"
                                                       data-toggle="collapse" data-target="#accordion_${loop.index}" class="clickable lot_${productEnquiry.enquiryId}" onclick="toggleAccordion('#accordion_${loop.index}');">${productEnquiry.enquiryId}</a></td>
                                                <td><span id="craetedDate_${loop.index}">
                                                	<fmt:formatDate value="${productEnquiry.createdDate}" dateStyle="medium" timeStyle="short" type="both" />
												</span></td>
												<td><span id="kamName_${loop.index}">${productEnquiry.kamName}</span>
                                             </tr>
                                       			<tr id="accordion_${loop.index}" class="collapse">
                                                <td colspan="13">
                                                    <div class="pr_table_show">
                                                        <h3>Product Details<i class="fa fa-close" onclick="toggleAccordion('#accordion_${loop.index}');" ></i></h3>
                                                        <table class ="table table-striped prods">
                                                            <thead>
                                                                <tr>
                                                                    <th>Product Type</th>
                                                                    <th>Series</th>
                                                                    <th>Grade</th>
                                                                    <th>Finish</th>
                                                                    <th>Thickness</th>
                                                                    <th>Width</th>
                                                                    <th>Length</th>
                                                                    <th>Edge</th>
                                                                    <th>ILP</th>
                                                                    <th>PVC</th>
                                                                    <th>Inspection</th>
                                                                    <th>Quantity(MT)</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <c:forEach items="${productEnquiry.enquiryEntries}" var="enquiryEntry">
                                                                    <tr class="prodRow">
                                                                        <td id="item_product_${loop.index}">${enquiryEntry.productType}</td>
                                                                        <td id="item_series_${loop.index}">${enquiryEntry.series}</td>
                                                                        <td id="item_grade_${loop.index}">${enquiryEntry.grade}</td>
                                                                        <td id="item_finish_${loop.index}">${enquiryEntry.finish}</td>
                                                                        <td id="item_thickness_${loop.index}">${enquiryEntry.thickness}</td>
                                                                        <td id="item_width_${loop.index}">${enquiryEntry.width}</td>
                                                                        <td id="item_length_${loop.index}">${enquiryEntry.length}</td>
                                                                        <c:choose>
                                                                        	<c:when test="${enquiryEntry.edgeCondition == 'ME'}">
                                                                        		<td id="item_edge_${loop.index}">Mill Edge</td>
                                                                        	</c:when>
                                                                        	<c:otherwise>
                                                                        		<td id="item_edge_${loop.index}">Trim Edge</td>
                                                                          	</c:otherwise>
                                                                        </c:choose>
                                                                        <c:choose>
                                                                        	<c:when test="${enquiryEntry.ilp eq 'YES'}">
                                                                        		<td id="item_ilp_${loop.index}">Required</td>
                                                                        	</c:when>
                                                                        	<c:otherwise>
                                                                        		<td id="item_ilp_${loop.index}">Not Required</td>
                                                                        	</c:otherwise>
                                                                        </c:choose>
                                                                        <c:choose>
                                                                        	<c:when test="${enquiryEntry.pvc eq 'Y'}">
                                                                        		<td id="item_pvc_${loop.index}">Required</td>
                                                                        	</c:when>
                                                                        	<c:otherwise>
                                                                        		<td id="item_pvc_${loop.index}">Not Required</td>
                                                                        	</c:otherwise>
                                                                        </c:choose>
                                                                        <c:choose>
                                                                        	<c:when test="${enquiryEntry.inspection eq 'true'}">
                                                                        		<td id="item_inspection_${loop.index}">Required</td>
                                                                        	</c:when>
                                                                        	<c:otherwise>
                                                                        		<td id="item_inspection_${loop.index}">Not Required</td>
                                                                        	</c:otherwise>
                                                                        </c:choose>
                                                                
                                                                        <td id="item_quantity_${loop.index}">${enquiryEntry.quantity}</td>
                                                                    </tr>
                                                                </c:forEach>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
    
                                       
                                             </c:forEach>
    
	                                   	</c:when>
	                                   	<c:otherwise>
	                                   		<tr>
	                                   			<td colspan=3><h2>No Enquiries Found</h2></td>
	                                   		</tr>
	                                   	</c:otherwise>
                                    </c:choose>
                                                            
                                </tbody>
                                </table>
					 
					 <!-- End -->
					 </div>	
										 
					</div>
					
                     </div>
					 </div>
					 
				
             </div>
</div>