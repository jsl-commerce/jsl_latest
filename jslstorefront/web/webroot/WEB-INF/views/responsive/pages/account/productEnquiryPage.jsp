<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>




<!DOCTYPE html>
<html>
   <head>
      <title>JSL Site</title>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
      <link rel="stylesheet" type="text/css" media="all" href="${commonResourcePath}/customAssets/css/style.css">
      <link rel="stylesheet" type="text/css" media="all" href="${commonResourcePath}/customAssets/css/productEnquiry.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <script type="text/javascript" src="${commonResourcePath}/customAssets/js/productEnquiry.js"></script>
    </head>
   <body class="page-auctionHomePage pageType-ContentPage">
      
      <div class="yCmsContentSlot container-fluid"></div>
      <a id="skip-to-content"></a>
      <div class="main__inner-wrapper">
      <div id="loading-wrapper">
		    <div class="spinner">
				  <div class="rect1"></div>
				  <div class="rect2"></div>
				  <div class="rect3"></div>
				  <div class="rect4"></div>
				  <div class="rect5"></div>
				  <div class="rect6"></div>
					
		  </div>
	</div>
      <div id="snackBar"></div>
      <div id="myModal" role="dialog" class="home-modal">
		<div class="modal-dialog modal-dialog-centered">
			<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h2 class="modal-title">Enquiry Created Successfully</h2>
					</div>
					<div class="modal-body">
						<div id="modalMessage"></div>
					</div>
					<div class="modal-footer">                  
					    <button class="btn btn-primary" type="submit" onclick="hideModal()">OK</button>
					</div>
				</div>
		</div>
	</div>
         <div class="product_details_place_order_container">           
            <div id="content">			
			<div>			
				<select class="compny_data">
							  <option value="both">Both</option>
							  <option value="jsl">JSL</option>
							  <option value="jshl">JSHL</option>							 
				</select>
			</div>
               <div class="product_content">		   		  
				   <h1>Flat Products </h1>		 			  
                  <div class="product_main_box">
					<!--<div><button class="my_enq_btn">My Enquiry</button>
						<button class="my_enq_btn">New Enquiry</button>
					</div> -->
                    <!-- <div class="product__header">HPL-F05-N1-PRIME-400 series-IRSM44-97-409M Group-10-2500-790-0.953-NO1</div>   -->
                     <div class="product__body">
					 <div class ="prd_border">
					 	<div class="product_attribute_list">
					 		<div class="product_attribute_item">
					 			<div class="p_attribute_item_title">
							  Product Type </div>
                              <select  class="dropdown_prd" id="prodTypeSel" onchange="populateNext(id)" autofocus="true">
									  <option value="" selected>Select</option>							  
							 </select>
					 		
					 		</div>
					 		
					 			<div class="product_attribute_item">
					 			<div class="p_attribute_item_title">
							  Series </div>
                              <select  class="dropdown_prd" id="seriesSel" onchange="populateNext(id)">
									  <option value="" selected>Select</option>						 
							 </select>
					 		</div>
					 		
					 		<div class="product_attribute_item">
					 			<div class="p_attribute_item_title">
							  Grade </div>
                              <select  class="dropdown_prd" id="gradeSel" onchange="populateNext(id)">
									  <option value="" selected>Select</option>							 
							 </select>
					 		</div>
					 		
					 		
					 			<div class="product_attribute_item smallWidth">
					 			<div class="p_attribute_item_title">
							  Finish </div>
                              <select  class="dropdown_prd" id="finishSel" onchange="populateNext(id)">
									  <option value="" selected>Select</option>							 
							 </select>
							 </div>
					 		
					 		
					 		<div class="product_attribute_item smallWidth">
					 			<div class="p_attribute_item_title">
							  Thickness</div>
	                             <select  class="dropdown_prd" id="thkSel" onchange="populateNext(id)">
							  			<option value="" selected>Select</option>							 
							 </select>
							 
							 <input type="number" class="customInput" id="thkInput" min="0"/>
							 
							 </div>
					 		
					 		
					 		<div class="product_attribute_item smallWidth">
					 			<div class="p_attribute_item_title">
							  Width</div>
                              <select  class="dropdown_prd" id="wdthSel" onchange="populateNext(id)">
							 			<option value="" selected>Select</option>							 
							 </select>
							 <input type="number" class="customInput" id="wdthInput" min="0"/>
							 
							 </div>
					 		
					 		
					 		<div class="product_attribute_item smallWidth">
					 			<div class="p_attribute_item_title">
							   Length</div>
                              <select  class="dropdown_prd" id="lenSel" onchange="populateNext(id)">
									  <option value="" selected>Select</option>							 
							 </select>
							 <input type="number" class="customInput" id="lenInput" min="0"/>
							 
							 </div>
					 		
					 		
					 		<div class="product_attribute_item">
					 			<div class="p_attribute_item_title">
							  Edge </div>
                              <select  class="dropdown_prd" id="edgeSel">
									  <option value="" selected>Select</option>
									  <option value="ME">Mill Edge</option>
									  <option value="TE">Trim Edge</option>							 
								</select></div>
					 		
					 		
					 		<div class="product_attribute_item">
					 			<div class="p_attribute_item_title">
							  ILP </div>
                              <select  class="dropdown_prd" id="ilpSel">
									  <option value="" selected>Select</option>
									  <option value="YES">Required</option>
									  <option value="NO">Not Required</option>							 
							 </select></div>
					 		
					 		
					 		<div class="product_attribute_item">
					 			<div class="p_attribute_item_title">
							  PVC </div>
                              <select  class="dropdown_prd" id="pvcSel">
                              			<option value="" selected>Select</option>
                              			<option value="Y">Required</option>
                              			<option value="N">Not Required</option>
							 </select></div>
							 
							 
					 		<div class="product_attribute_item">
					 			<div class="p_attribute_item_title">
							  Inspection </div>
                              <select  class="dropdown_prd" id="inspSel">
									  <option value="" selected>Select</option>
									  <option value="true">Required</option>
									  <option value="false">Not Required</option>						 
							 </select></div>
					 		
					 		
					 		<div class="product_attribute_item buymt_item">
					 			<div class="p_attribute_item_title">
							  Buy (MT) </div>
                             <div class="c-input-box">							 
                                    <input type="number" class="customInput c-input" id="qty" min="0"/>
                                 </div></div>
                                 
                              <div class="product_attribute_item">
                              <div class="p_attribute_item_title blank">&nbsp;</div>
					 			<button id="addProdBtn" class="btn cartbutton_prd_enq" onclick=addProduct()>
									<i class="fa fa-truck fa-2x"></i> Add to Cart
								</button>
                             </div>   
					 		
					 	</div>
                     
						 			
					</div>	
						<!--Fixed till here -->
						<!--    <div class="product_footer_cart">
                       
                        <div class="cart_item_value">                                              
                           
                           <div class="cart_item_value_txt">
                              <div class="item_add-to-cart">
							  <div class="p_attribute_item_title">
							  Buy (in MT) </div>
                                 <div class="c-input-box">
								 
                                    <input type="text" id="qty" placeholder="> 60.0" class="c-input">
                                 </div>
                                 <button class="c-btn add-to-cart-btn">Add Product</button>
                              </div>
                           </div>
                        </div>
                     </div>  -->
						
					 

					 
					 <!-- Table started -->
					 <div><br></div>
					 
                     <div class="main-tableDiv sticky-table">
					 
					 <!-- <caption class="caption_add">Added Enquiries</caption><br>  -->
					 <div class="row">
						<div class="col-md-12 text_data">
							Added Products
						</div>
					 </div>
					  
					 <div class="pr_table_show pr_table_display">
					  <table class ="table table-striped ordertable-striped order">
					  
					   <thead>
						 <tr class="sticky-header">
						    <th>Line ID</th>
							<th>Product Type</th>
							<th>Series</th> 
							<th>Grade</th>
							<th>Finish</th>
							<th>Thickness</th> 
							<th>Width</th>
							<th>Length</th>
							<th>Edge Condition</th> 
							<th>ILP</th>
							<th>PVC</th>
							<th>Inspection</th> 
							<th>Quantity</th>
							<th>Delete</th>
						  </tr>
						  </thead>
						  <tbody id="productTable">
						  	<tr>
						  		<td colspan="14"><span><center>No Products Yet</center></span></td>
						  	</tr>
						  </tbody>
					</table>
					 </div>	
										 
					</div>
					<!-- Table ends -->
					<br>
					<textarea class="txt_area" id="comments" placeholder="Enter Your Comment Here..."></textarea>
					<div class ="row">
					 <div class="col-md-12">
						<div><button class="order-Enq-btn add-to-cart-btn" onclick="sendList()">Submit Enquiry</button></div>
						
					</div>
					<!-- <div class="col-md-10">
						<div><button class="order-Enq-btn add-to-cart-btn">Enter Remarks</button></div>
						
					</div> -->
					
                     </div>
					 </div>
	 
                  </div>
			    </div>
            </div>
         </div>
      </div>

      </div>
   </body>
</html>