<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<spring:htmlEscape defaultHtmlEscape="true" />
<style>
   a, span {
   font-size: 13px;
   } 
   #myModal {
   display: none;
   position: fixed;
   height: auto;
   width: auto;
   top: 50%;
   left: 50%;
   transform: translate(-50%, -50%);
   z-index: 1000;
   }
   .modal-header{
   background-color: #f57921;
   color: white;
   }
   .modal-content{
   width: 100%;
   }
   .searchBtn {
   position: absolute;
   top: 0;
   right: 11px;
   background: #ddd;
   border: 0;
   color: #fff;
   padding: 7px 10px;
   }
   .invoicevalueDetails{
   display: none;
   }
   .bill_details {
   /*max-height: 307px;
   overflow-y: scroll;*/
   }
   #addedBatches {
   max-height: 307px;
   /*overflow-y: scroll;*/
   }
   #materialLocation {
   height: auto;
   min-height: 120px !important;
   }
</style>
<script type="text/javascript" src="${commonResourcePath}/customAssets/js/primaryDefects.json"></script>
<script type="text/javascript"
   src="${commonResourcePath}/customAssets/js/supportTicket.js"></script>
<input type="hidden" id="salesOffice"/>
<input type="hidden" id="invoiceCompanyCode"/>
<input type="hidden" id="salesGroup"/>
<input type="hidden" id="salesDistrict"/>
<input type="hidden" id="salesOrg"/>
<input type="hidden" id="invoiceKamName"/>
<input type="hidden" id="invoiceKamId"/>
<div id="global-alerts" class="global-alerts"></div>
<div id="loading-wrapper">
   <div class="spinner">
      <div class="rect1"></div>
      <div class="rect2"></div>
      <div class="rect3"></div>
      <div class="rect4"></div>
      <div class="rect5"></div>
      <div class="rect6"></div>
   </div>
</div>
<div id="myModal" role="dialog" class="home-modal">
   <div class="modal-dialog modal-dialog-centered">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <h2 class="modal-title">Support Ticket raised successfully</h2>
         </div>
         <div class="modal-body">
            <div id="modalMessage"></div>
         </div>
         <div class="modal-footer">                  
            <button class="btn btn-primary" type="submit" onclick="hideModal()">OK</button>
         </div>
      </div>
   </div>
</div>
<div class="back-link border">
   <div class="row">
      <div class="container-lg col-md-12">
         <span class="label" style="float:left">
            <spring:theme text="Complaint Tickets" />
         </span>
      </div>
   </div>
</div>
<div class="supportTicketsDiv">
   <div class="form-group row">
      <div class="type_Complaint">
         <label class="col-md-2">*Type of Complaint:</label>
         <div class="col-md-10">
            <select class="form-control selectType" id="compType">
               <option selected>Material</option>
               <option>Service</option>
            </select>
         </div>
      </div>
   </div>
   <div class="materialType">
      <div class="form-group row">
         <label class="col-md-2">*Present Material Condition:</label>
         <div class="col-md-10">
            <div class="form-check-inline">
               <label class="form-check-label">
               <input type="radio" class="form-check-input" name="optradio" value="As Supplied">As Supplied</label>
            </div>
            <div class="form-check-inline">
               <label class="form-check-label">
               <input type="radio" class="form-check-input" name="optradio" value="Semi Finished/ Processed">Semi Finished/ Processed</label>
            </div>
            <!-- <div class="form-check-inline">
               <label class="form-check-label">
                 <input type="radio" class="form-check-input" name="optradio" value="Not Available">Not Available</label>
               </div> -->
         </div>
      </div>
      <div class="form-group row">
         <div class="type_Complaint">
            <label class="col-md-2">*Nature of Complaint:</label>
            <div class="col-md-10">
               <select id="natureOfComplaint" class="form-control selectType" onchange="updatePrimDefect()">
                  <option value="">Select</option>
                  <option value="Surface Related">Surface Related</option>
                  <option value="Dimension Related">Dimension Related</option>
                  <option value="Weight Related">Weight Related</option>
                  <option value="Property Related">Property Related</option>
                  <option value="Transit Related">Transit Related</option>
                  <option value="Shape Related">Shape Related</option>
                  <!-- <option value="">Select</option>
                     <option value="Surface Defects">Surface Defects</option>
                     <option value="Wrong Allocation/Wrong Planning">Wrong Allocation/Wrong Planning</option>
                     <option value="Handling damage">Handling damage</option>
                     <option value="Mechanical Testing Failure">Mechanical Testing Failure</option>
                     <option value="Accidental Return">Accidental Return</option>
                     <option value="Wrong order booking">Wrong order booking</option>
                     <option value="Dimension related">Dimension related</option>
                     <option value="Shape related">Shape related</option>
                     <option value="Transit Damage">Transit Damage</option>
                     <option value="Packing Problem">Packing Problem</option>
                     <option value="Weight shortage">Weight shortage</option>
                     <option value="Metallurgical/Microstructure/Chemically fail">Metallurgical/Microstructure/Chemically fail</option> -->
               </select>
            </div>
            <!-- <div class="form-check-inline">
               <label class="form-check-label">
                 <input type="checkbox" class="form-check-input" value="Surface/ Shape Related" name="natureOfComplaint">Surface/ Shape Related</label>
               </div>
               
               <div class="form-check-inline">
               <label class="form-check-label">
                 <input type="checkbox" class="form-check-input" value="Wrong Material Supply" name="natureOfComplaint">Wrong Material Supply</label>
               </div>
               
               <div class="form-check-inline">
               <label class="form-check-label">
                 <input type="checkbox" class="form-check-input" value="Packing/Transit Related" name="natureOfComplaint">Packing/Transit Related</label>
               </div>
               
               <div class="form-check-inline">
               <label class="form-check-label">
                 <input type="checkbox" class="form-check-input" value="Metallurgical/Mechanical properties Related" name="natureOfComplaint">Metallurgical/Mechanical properties Related</label>
               </div>
               -->    
         </div>
      </div>
      <div class="form-group row">
         <label class="col-md-2">Search By SAP Bill No:</label>
         <div id="searchBox" class="col-md-10" style="position: relative">
            <input type="text" id="invoiceSearch" placeholder="Enter SAP Bill Number" class="form-control">
            <button onclick="searchInvoice()" class="searchBtn"><i class="fa fa-search"></i></button>
            <div class="invoicevalueDetails">
               <ul>
                  <li>
                     <span class="invoice_value" id="invoiceId">
                     <i class="lni-plus"></i>
                     </span>
                     <div class="bill_details">
                        <div id="searchBox" style="position: relative">
                           <input type="text" id="batchSearch" onkeyup="searchBatch()" placeholder="Search by Batch Number" class="form-control selectType">
                        </div>
                        <div class="main-tableDiv sticky-table" style = "max-height: 300px; overflow: auto;">
                        <table id="batchTable" class="table table-bordered">
                           <thead>
                              <tr class="sticky-header">
                                 <th>Add</th>
                                 <th>Batch Number</th>
                                 <!-- <th>Company Code</th> -->
                                 <th>Grade</th>
                                 <th>Thickness</th>
                                 <th>Width</th>
                                 <th>Length</th>
                                 <th>Finish</th>
                                 <th>Edge</th>
                                 <th>Primary Defect</th>
                                 <th>Supply Qty</th>
                                 <th width=10%>Complaint Qty</th>
                                 <th>Plant</th>
                                 <th>Invoice Date</th>
                                 <th>KAM Name</th>
                                 <th>SO No</th>
                              </tr>
                           </thead>
                           <tbody id="batchTableBody" class="tableFix">
                           </tbody>
                        </table>
                        </div>
                     </div>
                  </li>
               </ul>
            </div>
         </div>
      </div>
      <div class="form-group row">
         <label class="col-md-2">*Added Batches:</label>
         <div class="col-md-10" id="added_batchTable">
            <table class="table table-stripped table-bordered">
               <thead>
                  <tr>
                     <th>Batch Number</th>
                     <!-- <th>Company Code</th> -->
                     <th>Grade</th>
                     <th>Thickness</th>
                     <th>Width</th>
                     <th>Length</th>
                     <th>Finish</th>
                     <th>Edge</th>
                     <th>Primary Defect</th>
                     <th>Supply Qty</th>
                     <th width=10%>Complaint Qty</th>
                     <th>Plant</th>
                     <th>Invoice Date</th>
                     <th>KAM Name</th>
                     <th>SO No</th>
                     <th>Remove</th>
                  </tr>
               </thead>
               <tbody id="addedBatches">
               </tbody>
            </table>
         </div>
      </div>
      <div class="form-group row">
         <label class="col-md-2">*Photograph/Report:<br><span style="font-size: 11px; color: #0068b3;">(Total attachment Size 10 MB)</span><br><span style="font-size: 11px; color: #0068b3;">Valid formats - .png,.jpeg,.jpg,.bmp</span></label>
         <div class="col-md-10">
            <div class="col-md-3"><input type="file" class="form-control" id="attachment1"/><a onclick="$('#attachment1').val('')">Clear Selection</a></div>
            <div class="col-md-3"><input type="file" class="form-control" id="attachment2"/><a onclick="$('#attachment2').val('')">Clear Selection</a></div>
            <div class="col-md-3"><input type="file" class="form-control" id="attachment3"/><a onclick="$('#attachment3').val('')">Clear Selection</a></div>
            <div class="col-md-3"><input type="file" class="form-control" id="attachment4"/><a onclick="$('#attachment4').val('')">Clear Selection</a></div>
         </div>
      </div>
      <div class="form-group row">
         <label class="col-md-2">*Current Location of Material:</label>
         <div class="col-md-10">
            <textarea class="form-control" id="materialLocation"></textarea>
         </div>
      </div>
      <div class="form-group row">
         <label class="col-md-2">*Contact Person Name:</label>
         <div class="col-md-10">
            <input type="text" class="form-control" id="contactPersonName"/>
         </div>
      </div>
      <div class="form-group row">
         <label class="col-md-2">*Contact Number:</label>
         <div class="col-md-10">
            <input type="number" class="form-control" id="contactNo"/>
         </div>
      </div>
      <div class="form-group row">
         <label class="col-md-2">Complaint Description:</label>
         <div class="col-md-10">
            <textarea class="form-control com_desc" id="complaintDesc" maxlength="2000" placeholder="Enter Your Comment Here...Max limit 2000 characters" onfocus="this.placeholder=''" onblur="this.placeholder='Enter Your Comment Here...Max limit 2000 characters'"></textarea>
         </div>
      </div>
   </div>
   <div class="serviceType">
      <div class="form-group row">
         <label class="col-md-2">*Sub-Category:</label>
         <div class="col-md-10">
            <select class="form-control selectType" id="serviceSubCat">
               <option value="">Select</option>
               <option value="Delivery Related">Delivery Related</option>
               <option value="Logistic Related">Logistic Related</option>
               <option value="Sales Related">Sales Related</option>
               <option value="Plant Related">Plant Related</option>
               <option value="SAP/IT Related">SAP/IT Related</option>
               <option value="Others">Others</option>
            </select>
         </div>
      </div>
      <div class="form-group row">
         <label class="col-md-2">Complaint Description:</label>
         <div class="col-md-10">
            <textarea class="form-control" id="com_desc"></textarea>
         </div>
      </div>
   </div>
   <div class="btns">
      <button class="btn btn-primary" onclick="sendList()">Submit</button>
   </div>
</div>
<div id="snackBar"></div>
<div style="display: none">
   <span id="supporttickets-tryLater">
      <spring:theme code="text.account.supporttickets.tryLater"/>
   </span>
   <span id="attachment-file-max-size-exceeded-error-message">
      <spring:theme code="text.account.supporttickets.fileMaxSizeExceeded"/>
   </span>
   <span id="file-too-large-message">
      <spring:theme code="text.account.supporttickets.file.is.large.than" arguments="${maxUploadSizeMB}"/>
   </span>
</div>
<script>
$(".invoicevalueDetails li .invoice_value").click(function () {
	$(this).next(".bill_details").toggleClass("active");
	$(this).find(".lni-plus").toggleClass("lni-minus").toggleClass("lni-plus");

});
</script>
<common:globalMessagesTemplates/>