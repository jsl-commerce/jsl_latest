<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div id="loading-wrapper" style="position: absolute; display: none;">
   <div class="spinner" style="position: absolute;">
      <div class="rect1"></div>
      <div class="rect2"></div>
      <div class="rect3"></div>
      <div class="rect4"></div>
      <div class="rect5"></div>
      <div class="rect6"></div>
   </div>
</div>
<div class="InvoiceDetails">
   <div class="container">
      <div class = "payment_Type">
         <div class="levelTop" id = "paymentTypeID" style = "padding: 0px;">
            <div class="row">
               <div class="col-md-4">
                  <div class="radioOptions form-group">
                     <div class="form-group">
                        <label class= "label_heading">Select Company </label>
                     </div>
                     <select id="cmpnyCode"  onchange="checkpaymentTypeSelected()">
                        <option value="all" selected="">Select</option>
                        <option value="JS01">Jindal Stainless (Hisar) Limited</option>
                        <option value="JS03">Jindal Stainless Limited</option>
                     </select>
                  </div>
               </div>
               <!--             <div class="col-md-8" style = "display:none;" id="paymentOptionId">
                  <div class="radioOptions checkedType">
                     <div class="form-group">
                        <label class= "label_heading">Select Payment Type </label>
                     </div>
                     <div class="form-group">
                        <input type="radio" name="number11" id="pai" value="Pay against Invoice" disabled><label>Pay against Invoice</label>
                     </div>
                     <div class="form-group">
                        <input type="radio" name="number11" id= "pa" disabled><label>Pay Advance</label>
                     </div>
                  </div>
                   
                  </div> -->
            </div>
         </div>
      </div>
      <div class = "payInvoice" style = "display:none;" id ="showPaymentHistoryPageId">
         <!--          <div class="levelTop">
            <div class="row form-inline">
               <div class="col-md-3">
                  <div class="form-group">
                     <label>Total Due Amount</label>
                     <input type="text" class="form-control" id = "totalDueAmountInvoice" disabled>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="form-group">
                     <label style="color:brown;">Payable Amount</label>
                     <input type="text" class="form-control" id="totalPayableAmt" style="background-color: white !important" disabled>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="form-group">
                     <label>Balance Due</label>
                     <input type="text" class="form-control" id="totalBalanceDueAmt" disabled>
                  </div>
               </div>
               <div class="col-md-3 makePayment_btn">
                  <button class="btn btn-primary pull-left" onclick="submitPaymentData()">Submit Payment</button>
               </div>
            </div>
            </div> -->
         <div class = "payment_Filter">
            <div class="row">
               <div class = "searchByParameters">
                  <div class="col-md-1">
                     <div class="radioOptions allSearch">
                        <input type="radio" name="searchData" value="all" id= "serachInvoiceAllData" checked><label> All</label>
                     </div>
                  </div>
                  <div class="col-md-11">
                     <div class="radioOptions searchByDueDate">
                        <div class="pull-left">
                           <input type="radio" name="searchData" value="Search By Payment Date"><label>Search By Payment Date</label>
                        </div>
                        <div class="input-group">
                           <input type="text" id="aStartDate" autocomplete="off" placeholder="From" class="form-control hasDatepicker" disabled /> 
                           <span class="input-group-addon" id="startDateIcon"><span class="icon icon-calendar"></span>
                           </span>
                        </div>
                        <div class="input-group">
                           <input type="text" id="aEndDate" autocomplete="off" placeholder="To" class="form-control hasDatepicker" disabled/> 
                           <span class="input-group-addon" id="endDateIcon"><span class="icon icon-calendar"></span>
                           </span>
                        </div>
                        <button class="btn btn-primary" onclick ="searchByPayAgainstInvoiceDateWise()" id="dateButtonId" disabled><i class="icon icon-magnifier"></i></button>
                        <button class="btn btn-primary" onclick ="resetFilterDate()" id="resetFilterDateButtonId" disabled>Reset Filter</button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="serviceTypeComplaint main-tableDiv sticky-table" style = "max-height: 400px; overflow: auto;">
            <!-- <div class = "serviceTypeComplaint" style = 'overflow: scroll; height: auto; max-height: 500px;'> -->
            <table class="table table-bordered table_auctionData" id="paymentHistoryMainTable">
               <thead>
                  <tr class="sticky-header">
                     <th>JSL Transaction ID</th>
                     <th>SBI Transaction ID</th>
                     <th>JSL Ref Amount</th>
                     <th>Payment Date</th>
                     <th>Status</th>
                     <th style = "display : none;">Company Code</th>
                     <th>Pending Transaction Date</th>
                     <th>Re Initiate Payment</th>
                     <th style= "display:none;">Payment Mode</th>
                  </tr>
               </thead>
               <tbody class="tableFix" id = "paymentHistorytbodyID">
                  <c:choose>
                     <c:when test="${paymentDetailsHistoryList.size() != 0}">
                        <c:forEach items="${paymentDetailsHistoryList}" var="transactionData" varStatus="loop">
                           <tr id="row_${loop.index}" class="lotRow">
                              <td><a id="jslTranId_${loop.index}"
                                 data-toggle="collapse" data-target="#accordion_${loop.index}" class="clickable ticketID_${serviceComplaint.status}" onclick="toggleAccordion('#accordion_service_${loop.index}');">
                                 ${transactionData.jslRefNumber}</a>
                              </td>
                              <td><span id="sbiTransId_${loop.index}">${transactionData.sbiRefNumber}</span>
                              <td>
                                 <span id="jslRefAmount_${loop.index}">
                                    <fmt:formatNumber type = "number" minFractionDigits="2" maxFractionDigits = "2" groupingUsed="false" value = "${transactionData.jslRefAmount}" />
                                 </span>
                              <td><span id="paymentDate_${loop.index}">${transactionData.paymentDate}</span>
                              <td><span id="status_${loop.index}">${transactionData.paymentStatus}</span>
                              <td style= "display:none;"><span id="cmpnyCode_${loop.index}">${transactionData.companyCode}</span>												
                              <td><span id="pendingPaymentDate_${loop.index}">${transactionData.pendingPaymentDate}</span>
                              <td><span id="reInitiatePayment_${loop.index}" button class="btn btn-primary" onclick="submitPendingPaymentData(${loop.index})" style = "display:none">Re Initiate Payment</span>
                              <td style= "display:none;"><span id="paymentMode_${loop.index}">${transactionData.paymentMode}</span>																										
                           </tr>
                           <c:choose>
                              <c:when test="${transactionData.paymentTypeAdv == 'N'}">
                                 <tr id="accordion_service_${loop.index}" class="collapse">
                                    <td colspan="7">
                                       <div class="pr_table_show">
                                          <h3>Payment Description<i class="fa fa-close" onclick="toggleAccordion('#accordion_service_${loop.index}');" ></i></h3>
                                          <table class ="table table-striped prods">
                                             <thead>
                                                <tr>
                                                   <th>Invoice number</th>
                                                   <th>GST Invoice Number</th>
                                                   <th>Invoice Amount</th>
                                                </tr>
                                             </thead>
                                             <tbody>
                                                <c:forEach items="${transactionData.invoiceList}" var="invoiceData" varStatus="loop">
                                                   <tr class="prodRow">
                                                      <td id="sub_category">${invoiceData.invoiceNumber}</td>
                                                      <td id="gstInvNo">${invoiceData.gstInvoiceNumber}</td>
                                                      <td id="amountTotal">
                                                         <fmt:formatNumber type = "number" minFractionDigits="2" maxFractionDigits = "2" groupingUsed="false" value = "${invoiceData.grandTotal}" />
                                                      </td>
                                                   </tr>
                                                </c:forEach>
                                             </tbody>
                                          </table>
                                       </div>
                                    </td>
                                 </tr>
                              </c:when>
                           </c:choose>
                           <c:choose>
                              <c:when test="${transactionData.paymentTypeAdv == 'Y'}">
                                 <tr id="accordion_service_${loop.index}" class="collapse">
                                    <td colspan="7">
                                       <div class="pr_table_show">
                                          <h3>Payment Description<i class="fa fa-close" onclick="toggleAccordion('#accordion_service_${loop.index}');" ></i></h3>
                                          <table class ="table table-striped prods">
                                             <thead>
                                                <tr>
                                                   <th>Sales Order Number</th>
                                                   <th>Purchase Order Number</th>
                                                   <th>Order Amount</th>
                                                </tr>
                                             </thead>
                                             <tbody>
                                                <tr class="prodRow">
                                                   <td id="sub_category">${transactionData.saleOrder.saleOrderNumber}</td>
                                                   <td id="description">${transactionData.saleOrder.purchaseOrderNumber}</td>
                                                   <td id="description">
                                                      <fmt:formatNumber type = "number" minFractionDigits="2" maxFractionDigits = "2" groupingUsed="false" value = "${transactionData.saleOrder.saleOrderAmount}" />
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </div>
                                    </td>
                                 </tr>
                              </c:when>
                           </c:choose>
                        </c:forEach>
                     </c:when>
                     <c:otherwise>
                        <tr>
                           <td colspan=7>
                              <h2>No Data Found</h2>
                           </td>
                        </tr>
                     </c:otherwise>
                  </c:choose>
               </tbody>
            </table>
         </div>
      </div>
      <div id = "noInvoiceDataFound">
         <h2 class= "noPurchaseData">No Record found.</h2>
      </div>
   </div>
</div>
</div>
<script type="text/javascript"
   src="${commonResourcePath}/customAssets/js/paymentHistory.js"></script>