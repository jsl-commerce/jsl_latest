<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="InvoiceDetails">
   <div class="container">
      <div id="loading-wrapper" style="position: absolute;">
         <div class="spinner" style="position: absolute;">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
            <div class="rect6"></div>
         </div>
      </div>
      <div class = "payment_Type">
         <div class="levelTop" id = "paymentTypeID" style = "padding: 0px;">
            <div class="row">
               <div class="col-md-4">
                  <div class="radioOptions form-group">
                     <div class="form-group">
                        <label class= "label_heading">Select Company </label>
                     </div>
                     <select id="cmpnyCode"  onchange="checkpaymentTypeSelected()">
                        <option value="all" selected="">Select</option>
                        <option value="JS01">Jindal Stainless (Hisar) Limited</option>
                        <option value="JS03">Jindal Stainless Limited</option>
                     </select>
                  </div>
               </div>
               <div class="col-md-8" style = "display:none;" id="paymentOptionId">
                  <div class="radioOptions checkedType">
                     <div class="form-group">
                        <label class= "label_heading">Select Payment Type </label>
                     </div>
                     <div class="form-group">
                        <input type="radio" class = "paymentTypeId" name="number11" id="pai" value="Pay against Invoice" disabled><label>Pay against Invoice</label>
                     </div>
                     <div class="form-group">
                        <input type="radio" class = "paymentTypeId" name="number11" id= "pa" value = "Pay Advance" disabled><label>Pay Advance</label>
                     </div>
                  </div>
               </div>
               <!--   <div class="col-md-2" style = "float:right">
                  <select id="compny_data">
                     <option value="both">Both</option>
                     <option value="jsl">JSL</option>
                     <option value="jshl">JSHL</option>
                  </select>
                  </div> -->
            </div>
         </div>
      </div>
      <div class = "payInvoice">
         <div class="levelTop" style = "padding: 9px;">
            <div class="row form-inline">
               <div class="col-md-3">
                  <div class="form-group">
                     <label>Total Due Amount</label>
                     <input type="text" class="form-control" id = "totalDueAmountInvoice" disabled>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="form-group">
                     <label style="color:brown;">Payable Amount</label>
                     <input type="text" class="form-control" id="totalPayableAmt" style="background-color: white !important" disabled>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="form-group">
                     <label>Balance Due</label>
                     <input type="text" class="form-control" id="totalBalanceDueAmt" disabled>
                  </div>
               </div>
               <div class="col-md-3 makePayment_btn">
                  <button class="btn btn-primary pull-left" onclick="submitPaymentData()">Submit Payment</button>
               </div>
            </div>
         </div>
         <div class = "payment_Filter">
            <div class="row">
               <div class = "searchByParameters">
                  <div class="col-md-1">
                     <div class="radioOptions allSearch">
                        <input type="radio" name="searchData" value="all" id= "serachInvoiceAllData" checked><label> All</label>
                     </div>
                  </div>
                  <div class="col-md-5">
                     <div class="radioOptions searchByInvoice">
                        <input type="radio" name="searchData" value="Search By Invoice" />
                        <label>Search By GST Invoice</label>
                        <div class="input-group">
                           <input class="searchInvoice form-control" type= "text" id= "serachInvoiceData" disabled>
                           <div class="input-group-append">
                              <button class="btn btn-primary" onclick ="searchByPayAgainstInvoice()" id="invoiceButtonId" disabled><i class="icon icon-magnifier"></i></button>
                              <button class="btn btn-primary" onclick ="resetFilterInvoice()" id="resetFilterIvoiceButtonId" style= "margin-left : 2px;" disabled>Reset Filter</button>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="radioOptions searchByDueDate">
                        <div class="pull-left">
                           <input type="radio" name="searchData" value="Search By Due Date"><label> Search By Due Date</label>
                        </div>
                        <div class="input-group">
                           <input type="text" id="aStartDate" autocomplete="off" placeholder="From" class="form-control hasDatepicker" disabled /> 
                           <span class="input-group-addon" id="startDateIcon"><span class="icon icon-calendar"></span>
                           </span>
                        </div>
                        <div class="input-group">
                           <input type="text" id="aEndDate" autocomplete="off" placeholder="To" class="form-control hasDatepicker" disabled/> 
                           <span class="input-group-addon" id="endDateIcon"><span class="icon icon-calendar"></span>
                           </span>
                        </div>
                        <button class="btn btn-primary" onclick ="searchByPayAgainstInvoiceDateWise()" id="dateButtonId" disabled><i class="icon icon-magnifier"></i></button>
                        <button class="btn btn-primary" onclick ="resetFilterDate()" id="resetFilterDateButtonId" disabled>Reset Filter</button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="invoiceTableDetails main-tableDiv sticky-table" style = "max-height: 400px; overflow-x:auto;">
            <!-- <div class="invoiceTableDetails" style = "overflow: scroll; height: auto; max-height: 400px;"> -->
            <table class="table table-bordered table-striped" id="paymentInvoiceTable">
               <thead>
                  <tr class="sticky-header">
                     <th><span class="checkbox-field"><input type="checkbox" id="chkBoxAll" onclick="selectAllRowCheckBox()" style = "visibility : hidden"></span></th>
                     <th>GST Invoice No.</th>
                     <th>Sale Order No.</th>
                     <th>SAP Bill No.</th>
                     <th>Plant</th>
                     <th style="display:none;">Company Code</th>
                     <th>Invoice date</th>
                     <th>Due Date</th>
                     <th>Amount Due</th>
                     <th style="display:none;">Invoice Number</th>
                  </tr>
               </thead>
               <!--  <tr id="row_0">
                  <td><span class="checkbox-field"><input type="checkbox" id="chkBox_0" onclick="selectChkBoxInvoiceRow(id)"></span></td>
                  <td id="invNo_0">9999999</td>
                  <td id="saleOrdNo_0">12345678</td>
                  <td id="sapBillNo_0">500</td>
                  <td id="plant_0">Hisar</td>
                  <td id="cmpnyCode_0">JSL</td>
                  <td id="invDate_0">01/06/2019</td>
                  <td id="dueDate_0">06/02/2019</td>
                  <td id="amtDue_0">2050000</td>
                  </tr>
                  <tr id="row_1">
                  <td><span class="checkbox-field"><input type="checkbox" id="chkBox_1" onclick="selectChkBoxInvoiceRow(id)"></span></td>
                  <td id="invNo_1">8767887</td>
                  <td id="saleOrdNo_1">12345678</td>
                  <td id="sapBillNo_1">500</td>
                  <td id="plant_1">Hisar</td>
                  <td id="cmpnyCode_1">JSL</td>
                  <td id="invDate_1">05/06/2019</td>
                  <td id="dueDate_1">06/05/2019</td>
                  <td id="amtDue_1">2050000</td>
                  </tr> -->
               <tbody class="tableFix">
                  <c:forEach items="${paymentDetailsHistoryList}" var="paymentDetailsHistory" varStatus="loop">
                     <tr id="row_${loop.index}">
                        <td><span class="checkbox-field"><input type="checkbox" id="chkBox_${loop.index}" onclick="selectChkBoxInvoiceRow(id)"></span></td>
                        <td id="gstInvNo_${loop.index}">${paymentDetailsHistory.gstInvoiceNumber}</td>
                        <td id="saleOrdNo_${loop.index}">${paymentDetailsHistory.orderNumber}</td>
                        <td id="sapBillNo_${loop.index}">${paymentDetailsHistory.invoiceNumber}</td>
                        <td id="plant_${loop.index}">${paymentDetailsHistory.plant}</td>
                        <td id="cmpnyCode_${loop.index}" style="display:none;">${paymentDetailsHistory.invoiceCompanyCode}</td>
                        <td id="invDate_${loop.index}">${paymentDetailsHistory.invoiceDate}</td>
                        <td id="dueDate_${loop.index}">${paymentDetailsHistory.dueDate}</td>
                        <td id="amtDue_${loop.index}">
                           <fmt:formatNumber type = "number" minFractionDigits="2" maxFractionDigits = "2" groupingUsed="false" value="${paymentDetailsHistory.grandTotal}"/>
                        </td>
                        <td id="invNo_${loop.index}" style="display:none;">${paymentDetailsHistory.invoiceNumber}</td>
                     </tr>
                  </c:forEach>
               </tbody>
            </table>
         </div>
         <div id = "noInvoiceDataFound">
            <h2 class= "noPurchaseData">No Record found.</h2>
         </div>
         <br>
         <div class="levelBottom">
            <div class="row">
               <div class="col-md-12">
                  <button class="btn btn-primary pull-right" onclick="submitPaymentData()">Submit Payment</button>
               </div>
            </div>
         </div>
         <div id="paymentConfirmationModal" role="dialog" class="home-modal">
            <div class="modal-dialog modal-dialog-centered">
               <!-- Modal content-->
               <div class="modal-content">
                  <div class="modal-header">
                     <h2 class="modal-title">Payment Confirmation</h2>
                  </div>
                  <div class="modal-body">
                     <div id="paymentConfirmationModalMessage"></div>
                  </div>
                  <div class="modal-footer">                  
                     <button class="btn btn-primary" type="submit" onclick="submitPaymentConfirmationModal()">OK</button>
                     <button class="btn btn-primary" type="submit" onclick="hidePaymentConfirmationModal()">Cancel</button>
                  </div>
               </div>
            </div>
         </div>
         <div id="paymentInvoiceModal" role="dialog" class="home-modal">
            <div class="modal-dialog modal-dialog-centered">
               <!-- Modal content-->
               <div class="modal-content">
                  <div class="modal-header">
                     <h2 class="modal-title" id="paymentInvoiceTitle"></h2>
                  </div>
                  <div class="modal-body">
                     <div id="paymentModalMessage"></div>
                  </div>
                  <div class="modal-footer">                  
                     <button class="btn btn-primary" type="submit" onclick="hidePaymentModal()">OK</button>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class= "payAdvance">
         <div class="levelTop" style = "padding: 0px;">
            <!--  <div class="row form-inline">
               <div class="col-md-5">
                  <label class="pt-10">Enter Purchase Order No.</label>
                  <div class="input-group searchByInvoice">
                     <input type="text" class="form-control" id="invoiceText">
                     <div class="input-group-append">
                        <button class="input-group-text" onclick ="searchByInvoice()"><i class="icon icon-magnifier"></i></button>
                     </div>
                  </div>
               </div>
               </div> -->
            <div class = "payment_Filter">
               <div class="row">
                  <div class = "searchByAdvanceParameters">
                     <div class="col-md-5">
                        <div class="radioOptions searchByInvoice">
                           <input type="radio" name="searchData" value="Search By Sale Order No" />
                           <label>Search By Sale Order No.</label>
                           <div class="input-group">
                              <input class="searchInvoice form-control" type= "text" id= "serachInvoiceDataAdvance" disabled>
                              <div class="input-group-append">
                                 <button class="btn btn-primary" onclick ="searchByAdvanceInvoice()" id="searchAdvanceInvoiceId" disabled><i class="icon icon-magnifier"></i></button>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="radioOptions searchByInvoice">
                           <input type="radio" name="searchData" value="Search By Purchase Order No" />
                           <label>Search By Purchase Order No.</label>
                           <div class="input-group">
                              <input class="searchInvoice form-control" type= "text" id= "serachPOData" style= "width:250px;" disabled>
                              <div class="input-group-append">
                                 <button class="btn btn-primary" onclick ="searchByAdvancePO()" id = "searchAdvancePoId" disabled><i class="icon icon-magnifier"></i></button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div id="showTableAdvance">
            <div class="invoiceTableDetails main-tableDiv sticky-table" style = "max-height: 400px; overflow-x:auto;">
               <!-- <div class="invoiceTableDetails" style = "overflow: scroll; height: auto; max-height: 400px;"> -->
               <table class="table table-bordered table-striped" id="paymentAdvanceTable">
                  <thead>
                     <tr class="sticky-header">
                        <th><span class="checkbox-field"><input type="checkbox" id="chkBoxAdvanceAll" onclick="selectAdvanceAllRowCheckBox()" style= "margin-right:9px; visibility : hidden" ></span></th>
                        <th>Sale Order No.</th>
                        <th>Purchase Order No.</th>
                        <!-- <th>Invoice No.</th> -->
                        <th>Sale Order Date</th>
                        <!-- <th>Plant</th> -->
                        <!--   <th>Company Name</th> -->
                        <th>Order Value</th>
                        <!-- <th>Business Area</th> -->
                        <th>Advance Amount</th>
                        <th>Notes</th>
                     </tr>
                     <!--  <tr>
                        <td><span class="checkbox-field"><input type="checkbox"></span></td>
                          <td>12345678</td>
                          <td>30/06/2019</td>
                          <td>Hisar</td>
                          <td>JSL</td>
                          <td>Rs. 20000</td>
                          <td>Hisar</td>
                          <td><input type = "text"></td>
                          <td><input type = "text"></td>
                        </tr>
                        <tr>
                         <td><span class="checkbox-field"><input type="checkbox"></span></td>
                          <td>12345678</td>
                          <td>30/06/2019</td>
                          <td>Hisar</td>
                          <td>JSHL</td>
                          <td>Rs. 20000</td>
                          <td>Hisar</td>
                          <td><input type = "text"></td>
                          <td><input type = "text"></td>
                        </tr> -->
                  </thead>
                  <tbody id="advTable" class="tableFix">
                  </tbody>
               </table>
            </div>
            <br>
            <div class="levelBottom">
               <div class="row">
                  <div class="col-md-12">
                     <button class="btn btn-primary pull-right" onclick="submitPaymentAdvanceData()" id="submitAdvBtnId">Submit Payment</button>
                  </div>
               </div>
            </div>
         </div>
         <div>
            <h2 class= "noPurchaseData" id ="noDataFound"></h2>
         </div>
      </div>
      <div id="paymentAdvanceConfirmationModal" role="dialog" class="home-modal">
         <div class="modal-dialog modal-dialog-centered">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <h2 class="modal-title">Advance Payment Confirmation</h2>
               </div>
               <div class="modal-body">
                  <div id="paymentAdvanceConfirmationModalMessage"></div>
               </div>
               <div class="modal-footer">                  
                  <button class="btn btn-primary" type="submit" onclick="submitAdvancePaymentConfirmationModal()">OK</button>
                  <button class="btn btn-primary" type="submit" onclick="hideAdvancePaymentConfirmationModal()">Cancel</button>
               </div>
            </div>
         </div>
      </div>
      <div id="paymentAdvanceModal" role="dialog" class="home-modal">
         <div class="modal-dialog modal-dialog-centered">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <h2 class="modal-title" id="paymentAdvanceTitle"></h2>
               </div>
               <div class="modal-body">
                  <div id="advanceModalMessage"></div>
               </div>
               <div class="modal-footer">                  
                  <button class="btn btn-primary" type="submit" onclick="hideAdvancePaymentModal()">OK</button>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script>
   window.onload = function() {
       // ...
   	$("#loading-wrapper").hide();
   }
</script>
<script type="text/javascript"
   src="${commonResourcePath}/customAssets/js/paymentInvoiceAdvance.js"></script>