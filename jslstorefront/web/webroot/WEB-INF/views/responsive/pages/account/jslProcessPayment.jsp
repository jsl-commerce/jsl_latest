<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<br>
	<br>
	<br>
	<br>


	<table class="table table-striped prods">
		<thead>
			<tr>
				<th>Payment Status</th>
				<th>Status Description</th>
				<th>Transaction Number</th>
				<th>SBI Transaction Number</th>
				<th>Amount</th>
			</tr>
		</thead>
		<tbody>
			<tr class="prodRow" style = "font-size : 13px;">
				<td id="paymentStatus">${paymentResponse.status}</td>
				<td id="paymentStatusDescription">${paymentResponse.status_desc}</td>
				<td id="jslRefNumber">${paymentResponse.jsl_ref_no}</td>
				<td id="sbiRefNumber">${paymentResponse.sbi_ref_no}</td>
				<td id="jslRefAmount"><fmt:formatNumber type = "number" minFractionDigits="2" maxFractionDigits = "2" groupingUsed="false" value="${paymentResponse.jsl_ref_amt}" /></td>

			</tr>
		</tbody>
	</table>

	<br>
	<br>
	<br>
	<br>

</body>

</html>