<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="pagination" tagdir="/WEB-INF/tags/responsive/nav/pagination"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<spring:url value="/my-account/customer-feedback" var="customerFeedbackPageUrl" />
<script type="text/javascript"
   src="${commonResourcePath}/customAssets/js/supportTicketHistory.js"></script>
<link rel="stylesheet" type="text/css" href="${commonResourcePath}/customAssets/AuctionPage/css/supportPageHistory.css" />
<div id="loading-wrapper" style="position: absolute; display: none;">
   <div class="spinner" style="position: absolute;">
      <div class="rect1"></div>
      <div class="rect2"></div>
      <div class="rect3"></div>
      <div class="rect4"></div>
      <div class="rect5"></div>
      <div class="rect6"></div>
   </div>
</div>
<div class="page-auctionHomePage pageType-ContentPage">
   <div class="product_details_place_order_container">
      <div class="product_content">
         <div class="cust360_main_box">
            <div class="product__body enquiry_layout container">
               <!-- Table started -->
               <div class="main-tableDiv sticky-table">
                  <!-- <caption class="caption_add">Added Enquiries</caption><br>  -->
                  <div >
                     <h1>Complaint Ticket History </h1>
                     <br>
                  </div>
               </div>
               <div class="supportTicketsDiv">
                  <div class="form-group row">
                     <div class="type_Complaint">
                        <label class="col-md-2">Complaint Type</label>
                        <div class="col-md-10">
                           <select class="form-control selectType" id="complaintTypeHistory">
                              <option selected>Material</option>
                              <option>Service</option>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="pr_table_show cust360_table_display  sticky-table" style = 'overflow: scroll; height: auto; max-height: 400px;'>
                     <!-- start -->
                     <div class = "materialTypeComplaint main-tableDiv">
                        <table class="table table-bordered table_auctionData">
                           <thead>
                              <tr class="sticky-header">
                                 <th title="Click On Complaint Number for Batch Details">Complaint Number</th>
                                 <th>Invoice Number</th>
                                 <th>KAM Name</th>
                                 <th>Quality Person  Name</th>
                                 <th>Created Date</th>
                                 <th>Settlement Remark</th>
                                 <th>Internal Status</th>
                                 <th>Settlement Note Feedback</th>
                                 <th>Status</th>
                                 <th>Feedback & Rating</th>
                              </tr>
                           </thead>
                           <%--  ${productEnquiry.enquiryId}</a></td> --%>
                           <tbody class="tableFix">
                              <c:choose>
                                 <c:when test="${MaterialComplaintList.size() != 0}">
                                    <c:forEach items="${MaterialComplaintList}" var="materialComplaint" varStatus="loop">
                                       <tr id="row_${loop.index}" class="lotRow">
                                          <td><a id="ticketID_${loop.index}" data-toggle="collapse" data-target="#accordion_${loop.index}" class="clickable ticketID_${materialComplaint.status}"
                                             onclick="toggleAccordion('#accordion_${loop.index}');">
                                             ${materialComplaint.complaintNumber}</a>
                                          </td>
                                          <td><span id="invoiceNumber_${loop.index}">${materialComplaint.invoiceNumber}</span>
                                             <%--  <td><span id="craetedDate_${loop.index}">
                                                <fmt:formatDate value="${materialComplaint.status}" dateStyle="medium" timeStyle="short" type="both" />
                                                </span></td> --%>
                                          <td><span id="kamName_${loop.index}">${materialComplaint.kamName}</span>
                                          <td><span id="qualityPersonName_${loop.index}">${materialComplaint.qualityPersonName}</span>
                                          <td><span id="createdDate_${loop.index}">${fn:replace(materialComplaint.createdOn,'IST','')}</span>
                                          <td><span id="remark_${loop.index}"><a onclick="viewSettlementRemark('complaintNumber_${loop.index}','${fn:escapeXml(materialComplaint.settlementRemarks)}','materialTypeRemark')">View</a></span>
                                          <td>
                                             <span id="internalStatus_${loop.index}">${materialComplaint.internalStatus}</span>
                                             <c:choose>
                                                <c:when test="${materialComplaint.status == 'Open'}">
                                          <td><span id="internalFeedback_${loop.index}"><a class ="isDisabled">Share Your Feedback</a></span>
                                          </c:when>
                                          <c:when test="${materialComplaint.status == 'In Process' && (materialComplaint.internalStatus == 'Pending for appointment/ Visit date from Customer' || materialComplaint.internalStatus == 'Visit date confirmed' || materialComplaint.internalStatus == 'Sample awaited Required/Requested' || materialComplaint.internalStatus == 'Sample received' || materialComplaint.internalStatus == 'Sample analysis completed & shared with Customer' || materialComplaint.internalStatus == 'Developmental/Trials to be conducted waiting for confirmation dates' || materialComplaint.internalStatus == 'Visit date confirmed' || materialComplaint.internalStatus == 'Pending for Settlement note') && materialComplaint.satisfactionSubmitted == false}">
                                          <td><span id="internalFeedback_${loop.index}"><a class ="isDisabled">Share Your Feedback</a></span>
                                          </c:when>
                                          <c:when test="${materialComplaint.status == 'In Process' && (materialComplaint.internalStatus == 'Pending for appointment/ Visit date from Customer' || materialComplaint.internalStatus == 'Visit date confirmed' || materialComplaint.internalStatus == 'Sample awaited Required/Requested' || materialComplaint.internalStatus == 'Sample received' || materialComplaint.internalStatus == 'Sample analysis completed & shared with Customer' || materialComplaint.internalStatus == 'Developmental/Trials to be conducted waiting for confirmation dates' || materialComplaint.internalStatus == 'Visit date confirmed' || materialComplaint.internalStatus == 'Pending for Settlement note') && materialComplaint.satisfactionSubmitted == true && materialComplaint.customerSatisfaction=='Accepted'}">
                                          <td><span id="internalFeedback_${loop.index}" class ="isDisabled">FeedBack Submitted</span>
                                          </c:when>
                                          <c:when test="${materialComplaint.status == 'In Process' && (materialComplaint.internalStatus == 'Pending for appointment/ Visit date from Customer' || materialComplaint.internalStatus == 'Visit date confirmed' || materialComplaint.internalStatus == 'Sample awaited Required/Requested' || materialComplaint.internalStatus == 'Sample received' || materialComplaint.internalStatus == 'Sample analysis completed & shared with Customer' || materialComplaint.internalStatus == 'Developmental/Trials to be conducted waiting for confirmation dates' || materialComplaint.internalStatus == 'Visit date confirmed' || materialComplaint.internalStatus == 'Pending for Settlement note') && materialComplaint.satisfactionSubmitted == true && materialComplaint.customerSatisfaction=='Rejected'}">
                                          <td><span id="internalFeedback_${loop.index}"><a class ="isDisabled">Share Your Feedback</a></span>
                                          </c:when>													
                                          <c:when test="${materialComplaint.status == 'In Process' && (materialComplaint.internalStatus == 'Settlement note sent to Customer & awaiting response' || materialComplaint.internalStatus == 'Revised settlement note issued' || materialComplaint.internalStatus == 'Settlement note approved') && materialComplaint.satisfactionSubmitted == false}">
                                          <td><span id="internalFeedback_${loop.index}"><a onclick="shareInternalStatus('complaintNumber_${loop.index}')">Share
                                          Your Feedback</a></span>
                                          </c:when>
                                          <c:when test="${materialComplaint.status == 'In Process' && (materialComplaint.internalStatus == 'Settlement note sent to Customer & awaiting response' || materialComplaint.internalStatus == 'Revised settlement note issued' || materialComplaint.internalStatus == 'Settlement note approved') && materialComplaint.satisfactionSubmitted == true && materialComplaint.customerSatisfaction=='Accepted'}">
                                          <td><span id="internalFeedback_${loop.index}" class ="isDisabled">FeedBack Submitted</span>
                                          </c:when>
                                          <c:when test="${materialComplaint.status == 'In Process' && (materialComplaint.internalStatus == 'Settlement note sent to Customer & awaiting response' || materialComplaint.internalStatus == 'Revised settlement note issued' || materialComplaint.internalStatus == 'Settlement note approved') && materialComplaint.satisfactionSubmitted == true  && materialComplaint.customerSatisfaction=='Rejected'}">
                                          <td><span id="internalFeedback_${loop.index}"><a onclick="shareInternalStatus('complaintNumber_${loop.index}')">Share
                                          Your Feedback</a></span>
                                          </c:when>
                                          <c:when test="${materialComplaint.status == 'Technically Resolved' && materialComplaint.satisfactionSubmitted == false}">
                                          <td><span id="internalFeedback_${loop.index}"><a onclick="shareInternalStatus('complaintNumber_${loop.index}')">Share
                                          Your Feedback</a></span>
                                          </c:when>
                                          <c:when test="${materialComplaint.status == 'Technically Resolved' && materialComplaint.satisfactionSubmitted == true && materialComplaint.customerSatisfaction=='Accepted'}">
                                          <td><span id="internalFeedback_${loop.index}" class ="isDisabled">FeedBack Submitted</span>
                                          </c:when>
                                          <c:when test="${materialComplaint.status == 'Technically Resolved' && materialComplaint.satisfactionSubmitted == true && materialComplaint.customerSatisfaction=='Rejected'}">
                                          <td><span id="internalFeedback_${loop.index}"><a onclick="shareInternalStatus('complaintNumber_${loop.index}')">Share
                                          Your Feedback</a></span>
                                          </c:when>	
                                          <c:when test="${materialComplaint.status == 'Resolved' && materialComplaint.satisfactionSubmitted == false}">
                                          <td><span id="internalFeedback_${loop.index}"><a onclick="shareInternalStatus('complaintNumber_${loop.index}')">Share
                                          Your Feedback</a></span>
                                          </c:when>
                                          <c:when test="${materialComplaint.status == 'Resolved' && materialComplaint.satisfactionSubmitted == true && materialComplaint.customerSatisfaction=='Accepted'}">
                                          <td><span id="internalFeedback_${loop.index}" class ="isDisabled">FeedBack Submitted</span>
                                          </c:when>
                                          <c:when test="${materialComplaint.status == 'Resolved' && materialComplaint.satisfactionSubmitted == true && materialComplaint.customerSatisfaction=='Rejected'}">
                                          <td><span id="internalFeedback_${loop.index}"><a onclick="shareInternalStatus('complaintNumber_${loop.index}')">Share
                                          Your Feedback</a></span>
                                          </c:when>	
                                          <c:when test="${materialComplaint.status == 'Closed' && materialComplaint.satisfactionSubmitted == false}">
                                          <td><span id="internalFeedback_${loop.index}"><a class ="isDisabled">Share Your Feedback</a></span>
                                          </c:when>
                                          <c:when test="${materialComplaint.status == 'Closed' && materialComplaint.satisfactionSubmitted == true}">
                                          <td><span id="internalFeedback_${loop.index}" class ="isDisabled">FeedBack Submitted</span>
                                          </c:when>
                                          </c:choose>
                                          <td>
                                             <span id="status_${loop.index}">${materialComplaint.status}</span>
                                             <c:choose>
                                                <c:when test="${materialComplaint.status == 'Resolved' && materialComplaint.feedbackSubmitted == false}">
                                          <td><span id="rateFeedback_${loop.index}"><a href="${customerFeedbackPageUrl}" onclick="setComplaintNumber('complaintNumber_${loop.index}')">Share
                                          Your Feedback</a></span>
                                          </c:when>
                                          <c:when test="${materialComplaint.status == 'Resolved' && materialComplaint.feedbackSubmitted == true}">
                                          <td><span id="rateFeedback_${loop.index}" class ="isDisabled">FeedBack Submitted</span>
                                          </c:when>
                                          <c:when test="${materialComplaint.status == 'Closed' && materialComplaint.feedbackSubmitted == false}">
                                          <td><span id="rateFeedback_${loop.index}"><a class ="isDisabled">Share Your Feedback</a></span>
                                          </c:when>
                                          <c:when test="${materialComplaint.status == 'Closed' && materialComplaint.feedbackSubmitted == true}">
                                          <td><span id="rateFeedback_${loop.index}" class ="isDisabled">FeedBack Submitted</span>
                                          </c:when>
                                          <c:when test="${materialComplaint.status != 'Resolved' && materialComplaint.status != 'Closed'}">
                                          <td><span id="rateFeedback_${loop.index}"><a class ="isDisabled">Share Your Feedback</a></span>
                                          </c:when>
                                          </c:choose>
                                       </tr>
                                       <tr id="accordion_${loop.index}" class="collapse">
                                          <td colspan="15">
                                             <div class="pr_table_show">
                                                <h3>Batch Details<i class="fa fa-close" onclick="toggleAccordion('#accordion_${loop.index}');" ></i></h3>
                                                <table class ="table table-striped prods">
                                                   <thead>
                                                      <tr>
                                                         <th>Batch Number</th>
                                                         <!-- <th>Company Code</th> -->
                                                         <th>Grade</th>
                                                         <th>Thickness</th>
                                                         <th>Width</th>
                                                         <th>Length</th>
                                                         <th>Finish</th>
                                                         <th>Edge</th>
                                                         <th>Primary Defect</th>
                                                         <th>SO No</th>
                                                         <th>Supply Qty</th>
                                                         <th>Complaint Qty</th>
                                                         <th>Plant</th>
                                                         <th>Invoice Date</th>
                                                         <th>KAM Name</th>
                                                      </tr>
                                                   </thead>
                                                   <tbody>
                                                      <c:forEach items="${materialComplaint.invoiceDetails}" var="invoiceDetail" varStatus="loop1">
                                                         <tr class="prodRow">
                                                            <td id="item_product_${loop1.index}">${invoiceDetail.batchNumber}</td>
                                                            <%-- <td id="item_series_${loop1.index}">${invoiceDetail.companyCode}</td> --%>
                                                            <td id="item_grade_${loop1.index}">${invoiceDetail.grade}</td>
                                                            <td id="item_finish_${loop1.index}">${invoiceDetail.thickness}</td>
                                                            <td id="item_thickness_${loop1.index}">${invoiceDetail.width}</td>
                                                            <td id="item_width_${loop1.index}">${invoiceDetail.length}</td>
                                                            <td id="item_length_${loop1.index}">${invoiceDetail.finish}</td>
                                                            <td id="item_edge_${loop1.index}">${invoiceDetail.edge}</td>
                                                            <td id="item_ilp_${loop1.index}">${invoiceDetail.primaryDefect}</td>
                                                            <td id="item_pvc_${loop1.index}">${invoiceDetail.soNumber}</td>
                                                            <td id="item_inspection_${loop1.index}">${invoiceDetail.supplyQuantity}</td>
                                                            <td id="item_quantity_${loop1.index}">${invoiceDetail.complaintQuantity}</td>
                                                            <td id="item_pvc_${loop1.index}">${invoiceDetail.plant}</td>
                                                            <td id="item_inspection_${loop1.index}">${invoiceDetail.invoiceDate}</td>
                                                            <td id="item_quantity_${loop1.index}">${invoiceDetail.kamName}</td>
                                                         </tr>
                                                      </c:forEach>
                                                   </tbody>
                                                </table>
                                             </div>
                                          </td>
                                       </tr>
                                    </c:forEach>
                                 </c:when>
                                 <c:otherwise>
                                    <tr>
                                       <td colspan=10>
                                          <h2>No Enquiries Found</h2>
                                       </td>
                                    </tr>
                                 </c:otherwise>
                              </c:choose>
                           </tbody>
                        </table>
                     </div>
                     <div class = "serviceTypeComplaint main-tableDiv">
                        <table class="table table-bordered table_auctionData">
                           <thead>
                              <tr class="sticky-header">
                                 <th title="Click On Complaint Number for Batch Details">Complaint Number</th>
                                 <th>KAM Name</th>
                                 <th style="display : none;">Quality Person  Name</th>
                                 <th>Created Date</th>
                                 <th>Settlement Remark</th>
                                 <th>Status</th>
                                 <th>Feedback & Rating</th>
                              </tr>
                           </thead>
                           <%--  ${productEnquiry.enquiryId}</a></td> --%>
                           <tbody class="tableFix">
                              <c:choose>
                                 <c:when test="${ServiceComplaintList.size() != 0}">
                                    <c:forEach items="${ServiceComplaintList}" var="serviceComplaint" varStatus="loop">
                                       <tr id="row_${loop.index}" class="lotRow">
                                          <td><a id="serviceticketID_${loop.index}"
                                             data-toggle="collapse" data-target="#accordion_${loop.index}" class="clickable serviceticketID_${serviceComplaint.status}" onclick="toggleAccordion('#accordion_service_${loop.index}');">
                                             ${serviceComplaint.complaintNumber}</a>
                                          </td>
                                          <td><span id="kamName_${loop.index}">${serviceComplaint.kamName}</span>
                                          <td style="display : none;"><span id="qualityPersonName_${loop.index}">${serviceComplaint.qualityPersonName}</span>
                                          <td><span id="createdDate_${loop.index}">${fn:replace(serviceComplaint.createdOn,'IST','')}</span>
                                             <%-- <td><span id="remark_${loop.index}">${serviceComplaint.settlementRemarks}</span> --%>
                                          <td><span id="remark_${loop.index}"><a onclick="viewSettlementRemark('complaintNumber_${loop.index}','${fn:escapeXml(serviceComplaint.settlementRemarks)}','serviceTypeRemark')">View</a></span>
                                          <td>
                                             <span id="status_${loop.index}">${serviceComplaint.status}</span>									
                                             <c:choose>
                                                <c:when test="${serviceComplaint.status == 'Resolved' && (serviceComplaint.customerFeedBackRating == null || serviceComplaint.customerFeedBackRating == 'null')}">
                                          <td><span id="rateFeedback_${loop.index}"><a onclick="rateFeedBack('rateFeedback_${loop.index}')">Share
                                          Your Feedback</a></span>
                                          </c:when>
                                          <c:when test="${serviceComplaint.status == 'Resolved' && (serviceComplaint.customerFeedBackRating != null || serviceComplaint.customerFeedBackRating != 'null')}">
                                          <td><span id="rateFeedback_${loop.index}" class ="isDisabled">FeedBack Submitted</span>
                                          </c:when>
                                          <c:when test="${serviceComplaint.status != 'Resolved'}">
                                          <td><span id="rateFeedback_${loop.index}"><a class ="isDisabled">Share Your Feedback</a></span>
                                          </c:when>														
                                          </c:choose>
                                       </tr>
                                       <tr id="accordion_service_${loop.index}" class="collapse">
                                          <td colspan="7">
                                             <div class="pr_table_show">
                                                <h3>Complaint Description<i class="fa fa-close" onclick="toggleAccordion('#accordion_service_${loop.index}');" ></i></h3>
                                                <table class ="table table-striped prods">
                                                   <thead>
                                                      <tr>
                                                         <th>Sub Category</th>
                                                         <th>Description</th>
                                                      </tr>
                                                   </thead>
                                                   <tbody>
                                                      <tr class="prodRow">
                                                         <td id="sub_category">${serviceComplaint.natureOfComplaints}</td>
                                                         <td id="description">${serviceComplaint.complaintDescription}</td>
                                                      </tr>
                                                      <%--                                                            <c:forEach items="${materialComplaint.invoiceDetails}" var="invoiceDetail" varStatus="loop1">
                                                         <tr class="prodRow">
                                                                    <td id="item_product_${loop1.index}">${invoiceDetail.batchNumber}</td>
                                                                    <td id="item_series_${loop1.index}">${invoiceDetail.companyCode}</td>
                                                         </tr>
                                                         </c:forEach> --%>
                                                   </tbody>
                                                </table>
                                             </div>
                                          </td>
                                       </tr>
                                    </c:forEach>
                                 </c:when>
                                 <c:otherwise>
                                    <tr>
                                       <td colspan=7>
                                          <h2>No Enquiries Found</h2>
                                       </td>
                                    </tr>
                                 </c:otherwise>
                              </c:choose>
                           </tbody>
                        </table>
                     </div>
                     <div id="feedbackServiceSubmitModal" role="dialog" class="home-modal">
                        <div class="modal-dialog modal-dialog-centered">
                           <!-- Modal content-->
                           <div class="modal-content">
                              <div class="modal-header">
                                 <h2 class="modal-title" id="feedBackServiceTitle"></h2>
                              </div>
                              <div class="modal-body">
                                 <div id="feedBackServiceModalMessage"></div>
                              </div>
                              <div class="modal-footer">                  
                                 <button class="btn btn-primary" type="submit" onclick="hidefeedbackServiceModal()">OK</a></button>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div id="internalFeedbackSubmitModal" role="dialog" class="home-modal">
                        <div class="modal-dialog modal-dialog-centered">
                           <!-- Modal content-->
                           <div class="modal-content">
                              <div class="modal-header">
                                 <h2 class="modal-title" id="internalFeedBackTitle">Settlement Feedback </h2>
                              </div>
                              <div class="modal-body">
                                 <div id="internalFeedBackModalMessage">
                                    <div class="checkedType" onchange="feedbackSelection()">
                                       <div class="form-check-inline">
                                          <label class="form-check-label" style="color:black; font-size:13px;">
                                          <input type="radio" class="form-check-input" name="optradio" value="Accepted">Accepted</label>
                                       </div>
                                       <div class="form-check-inline">
                                          <label class="form-check-label" style="color:black; font-size:13px;">
                                          <input type="radio" class="form-check-input" name="optradio" value="Rejected">Rejected</label>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="modal-footer">                  
                                 <button class="btn btn-primary" type="submit" onclick="submitIntrnalFeedbackStatus()" id = "submitBtnInternalFeedback" disabled>Submit</a></button>
                                 <button class="btn btn-primary" type="submit" onclick="hideIntrnalFeedbackModal()">Cancel</a></button>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div id="settlementRemarkModal" role="dialog" class="home-modal">
                        <div class="modal-dialog modal-dialog-centered">
                           <!-- Modal content-->
                           <div class="modal-content">
                              <div class="modal-header">
                                 <h2 class="modal-title" id="settlementRemarkModalTitle"></h2>
                              </div>
                              <div class="modal-body" style = "word-wrap: break-word; overflow: scroll; height: auto; max-height: 400px;">
                                 <div id="settlementRemarkModalMessage"></div>
                              </div>
                              <div class="modal-footer">                  
                                 <button class="btn btn-primary" type="submit" onclick="hidesettlementRemarkModal()">OK</a></button>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- End -->
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div id="feedBackModal">
         <div class="innerfeedBackModal">
            <div class="top_level">
               <input type="hidden" id="ratingModal">
               <span class="model_close"><i class="fa fa-close" onclick = "closeModal()"></i></span>
            </div>
            <div class="body_panel">
               <div class="form-group" id="starChecked">
                  <span class="fa fa-star-o" onclick = "starClicked(id)" id='firstStar'></span>
                  <span class="fa fa-star-o" onclick = "starClicked(id)" id='twoStar'></span>
                  <span class="fa fa-star-o" onclick = "starClicked(id)" id='threetStar'></span>
                  <span class="fa fa-star-o" onclick = "starClicked(id)" id='fourStar'></span>
                  <span class="fa fa-star-o" onclick = "starClicked(id)" id='fiveStar'></span>
               </div>
               <div class="form-group comments-field">
                  <label for="comment">Comment:</label>
                  <textarea class="form-control" id="comment"></textarea>
               </div>
            </div>
            <div class="modal_btn"><button type="button" class="btn btn-primary" onclick="submitFeedback()">Submit</button></div>
         </div>
      </div>
      <div id="snackBar"></div>
   </div>
</div>
<script>
   function formatDate(date) {
   	date = date.replace("IST", "");
       var d = new Date(date),
           month = '' + (d.getMonth() + 1),
           day = '' + d.getDate(),
           year = d.getFullYear();
   
       if (month.length < 2) month = '0' + month;
       if (day.length < 2) day = '0' + day;
   
       return [month, day , year].join('/');
   }
</script>