<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />


<div class="OrderExport">
<h1>Order Export</h1>
<div class="OrderExportDiv form-inline">

<div class="col-md-4">
<div class="form-group">
    <label>Start Date</label>
    <input type="date" class="form-control" id ="inp_StartDate" placeholder="mm/dd/yyyy">
    <!-- <i class="icon icon-calendar"></i> -->
  </div>
</div>


<div class="col-md-4">

  <div class="form-group">
    <label>End Date</label>
    <input type="date" class="form-control" id ="inp_EndDate" placeholder="mm/dd/yyyy">
   <!--  <i class="icon icon-calendar"></i> -->
  </div>
</div>


<div class="col-md-4">
<div class="orderExportbtn form-group">
<label class="blank">&nbsp;</label>
<input type ="button" class="btn btn-primary" id="button_Orderexport" onclick="exportData()" value ="Export Data">

</div>
</div>



  


</div>

</div>

<script>
function exportData(){
	window.location.href=ACC.config.encodedContextPath + "/my-account/ExportData?startDate=" + $("#inp_StartDate").val() +"&endDate="+ $("#inp_EndDate").val();
}

/* $(document).ready(function(){
	
	 $('#inp_StartDate').datepicker({
	      format: 'mm/dd/yyyy',
	      autoclose:true
	      
	 });
	 
	 $('#inp_EndDate').datepicker({
	      format: 'mm/dd/yyyy',
	      autoclose:true
	      
	 });
	
}); */


</script>


