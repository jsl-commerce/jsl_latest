<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html>
<head>
<title>JSL Site</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, user-scalable=no">
<link rel="stylesheet" type="text/css" media="all"
	href="./JSL Site_files/style.css">
<link rel="stylesheet" type="text/css" media="all"
	href="./JSL Site_files/main.css">
<link rel="stylesheet" type="text/css" media="all" href="${commonResourcePath}/customAssets/AuctionPage/css/customer360.css" />	
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
<script type="text/javascript"
	src="${commonResourcePath}/customAssets/js/accountStatementReport.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
 
<script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
<%-- <script type="text/javascript"
	src="${commonResourcePath}/customAssets/js/orderBookingReport.js"></script> --%>	
</head>
<body class="page-auctionHomePage pageType-ContentPage">
	<div class="yCmsContentSlot container-fluid"></div>
	<a id="skip-to-content"></a>
	<div class="main__inner-wrapper">
	<div id="loading-wrapper">
		    <div class="spinner">
				  <div class="rect1"></div>
				  <div class="rect2"></div>
				  <div class="rect3"></div>
				  <div class="rect4"></div>
				  <div class="rect5"></div>
				  <div class="rect6"></div>
					
		  </div>
		  <div class = "loader_text"><blinkLoaderText>Retrieving live data...please wait</blinkLoaderText></div>
	</div>
		<!-- <div class="product_details_place_order_container"> -->
		<div id="content">
		<div id="successDataCheck">
			<div class="product_content">
				<h2><center>Account Statement (Customer)  <span class = "daysText">(Last 90 Days Data)</span></center></h2>
				<select id="compny_data" style="float:right" onchange="filterData('drop')">
					<option value="both">Both</option>
					<option value="jsl">JSL</option>
					<option value="jshl">JSHL</option>
				</select>
				<div class="cust360_main_box">
					<!--<div><button class="my_enq_btn">My Enquiry</button>
						<button class="my_enq_btn">New Enquiry</button>
					</div> -->
					<!-- <div class="product__header">HPL-F05-N1-PRIME-400 series-IRSM44-97-409M Group-10-2500-790-0.953-NO1</div>   -->
					<div class="product__body">  
							<div class="row">
								<div class="col-md-6 cust360_text_data">
									<h3>Account Statement Data</h3>
								</div>
							</div>
							
					<br>
							<div class= "custDetailsBox">	<span id="customerId"></span>			<span id="customerName"></span></div>
						
						<!-- Table started -->

						<div class="main-tableDiv sticky-table">

							<!-- <caption class="caption_add">Added Enquiries</caption><br>  -->								
							<div class="row">
								<div class="col-md-2">
                                	<div class="form-group">
                                   		<div class="input-group">
                                         	<input type="text" id="aStartDate" autocomplete="off" placeholder="FROM" class="form-control hasDatepicker"/> 
                                         	<span class="input-group-addon" id="startDateIcon"><span class="fa fa-calendar"></span>
                                         	</span>
                                      	</div>
                                   </div>
           	                  	</div>
								
								<div class="col-md-2">
                                	<div class="form-group">
                                   		<div class="input-group">
                                         	<input type="text" id="aEndDate" autocomplete="off" placeholder="TO" class="form-control hasDatepicker"/> 
                                         	<span class="input-group-addon" id="endDateIcon"><span class="fa fa-calendar"></span>
                                         	</span>
                                      	</div>
                                   </div>
           	                  	</div>
           	                  	<div class="col-md-1">
									<button class="cust360_export_btn btn btn-primary" id="filterBtn" onclick="filterData('button')">Filter</button>
								</div>
								<div class="col-md-1">
									<button class="cust360_export_btn btn btn-primary" id="filterBtn" onclick="resetData()">Reset</button>
								</div>
								<div class="col-md-6">
									<div>
										<button class="cust360_export_btn btn btn-primary" onclick="excelExport()">Export Data</button>
									</div>
								</div>

							</div>

							<div class="pr_table_show cust360_table_display">
								<table class="table table-striped table-bordered ordertable-striped order">
									<thead>
										<tr class="sticky-header">
											<th>Company code</th>
											<th>Doc. Date</th>
											<th>Document No</th>
											<th>Document Desc.</th>
											<th>Ref / InstNo</th>
											<th>Reference</th>
											<th>Spl</th>
											<th>Item Description</th>
											<th>Due Date</th>
											<th>Amount</th>
											<th>Cur</th>
											<th>Debit Amount</th>
											<th>Credit Amount</th>
										</tr>
									</thead>
									<tbody id="tableBody">
										<tr>
											<td colspan=11>GRAND TOTAL <span style = "font-size : 10px;">(<span style = "color:red;">*</span>  <span>This total includes EMD and security deposits)</span></span></td>
											<td id="totalDebit"></td>
											<td id="totalCredit"></td>
										</tr>
									</tbody>
								</table>
							</div>

						</div>
							<br>
					<div class ="disclaimer" style="float : left">* Information provided in the site is indicative and for information only.In case of any difference/clarity, please refer to our Customer Support Team.</div>
					</div>
				</div>




			</div>
			
			
			
			</div>
			
			 <div id="failureDataCheck">
               	 <div class="product_content">
	               
	               <div class="no_data_cust360_border">
	              			<span class = "noData_text">Oops! Something went wrong. Please retry</span>
	              			
	               </div>
               </div>
               
               </div>
               
		</div>
		<!-- </div> -->
	</div>
	</div>
	</main>
	</div>
	<script>
	var today = new Date();
	var threeMonthsAgo = new Date(new Date().setDate(today.getDate() - 90));
	$('#aStartDate').datepicker({
		format: 'mm/dd/yyyy',
		autoclose:true,
		startDate: threeMonthsAgo,
		endDate: today
	});
	    
	    $("#startDateIcon").on('click', function(){
	      	$("#aStartDate").show().focus();
	      });


	$('#aEndDate').datepicker({
		format: 'mm/dd/yyyy',
		autoclose:true,
		startDate: threeMonthsAgo,
		endDate: today
	});

	$("#endDateIcon").on('click', function(){
	  	$("#aEndDate").show().focus(); 	
	  });
	
	function excelExport()
	{   
	  var frm = $('#aStartDate').val();
	  var to = $('#aEndDate').val();
	  var slectedCompany = $('#compny_data').val();
		if($('#aStartDate').val() != "" && $('#aEndDate').val() != "")
		{
			  if(frm > to){
				  alert("start date can not be greater than end date");
				  $('#aStartDate').val("");
				  $('#aEndDate').val("");
			  }
			  else
				  {
				  window.location.href="${request.contextPath}/reports/exportReport?report=ACCOUNTSTATEMENTREPORT&fromDate="+frm+"&toDate="+to+"&compny_data="+slectedCompany;
				  }
		}
		 else
		  {
		  window.location.href="${request.contextPath}/reports/exportReport?report=ACCOUNTSTATEMENTREPORT&fromDate="+frm+"&toDate="+to+"&compny_data="+slectedCompany;
		  }
	}
	</script>