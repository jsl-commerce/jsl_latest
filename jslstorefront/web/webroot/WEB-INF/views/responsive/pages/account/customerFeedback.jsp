<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<spring:url value="/my-account/support-ticket-history" var="accountSupportHistoryPageUrl" />
<div class="feedbackDetails">
   <div class="container">
      <h3>Customer Feedback Form</h3>
      <div class="radioOptions">
         <label>Mandatory Feedback</label>
         <div class = "row">
            <div class="form-group col-md-6">
               <label class= "label_heading">Response to current complaint *</label>
            </div>
            <div class = "col-md-6 Q1">
               <div class="form-group">
                  <input type="radio" value ="Excellent" name="optradio1"><label>Excellent</label>
               </div>
               <div class="form-group">
                  <input type="radio" value="Good" name="optradio1"><label>Good</label>
               </div>
               <div class="form-group">
                  <input type="radio" value="Average" name="optradio1"><label>Average</label>
               </div>
               <div class="form-group">
                  <input type="radio" value="Fair" name="optradio1"><label>Fair</label>
               </div>
               <div class="form-group">
                  <input type="radio" value="Poor" name="optradio1"><label>Poor</label>
               </div>
            </div>
         </div>
         <br>     
         <div class="checkbox">
            <label><input type="checkbox" value="" onclick="showAllOptionalFeedBack()" id="optionalChxBox" style = "margin-top: 0px;">Optional Feedback</label>
         </div>
         <div class="radioOptions" id="optionalFeedBackId" style = "display:none">
            <div class = "row">
               <div class="form-group col-md-6">
                  <label class= "label_heading">Order Acceptance Information</label>
               </div>
               <div class = "col-md-6 Q2">
                  <div class="form-group">
                     <input type="radio" value ="Excellent" name="optradio2"><label>Excellent</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Good" name="optradio2"><label>Good</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Average" name="optradio2"><label>Average</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Fair" name="optradio2"><label>Fair</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Poor" name="optradio2"><label>Poor</label>
                  </div>
               </div>
            </div>
            <div class = "row">
               <div class="form-group col-md-6">
                  <label class= "label_heading">Feedback on status of order</label>
               </div>
               <div class = "col-md-6 Q3">
                  <div class="form-group">
                     <input type="radio" value ="Excellent" name="optradio3"><label>Excellent</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Good" name="optradio3"><label>Good</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Average" name="optradio3"><label>Average</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Fair" name="optradio3"><label>Fair</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Poor" name="optradio3"><label>Poor</label>
                  </div>
               </div>
            </div>
            <div class = "row">
               <div class="form-group col-md-6">
                  <label class= "label_heading">On time delivery of the product</label>
               </div>
               <div class = "col-md-6 Q4">
                  <div class="form-group">
                     <input type="radio" value ="Excellent" name="optradio4"><label>Excellent</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Good" name="optradio4"><label>Good</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Average" name="optradio4"><label>Average</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Fair" name="optradio4"><label>Fair</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Poor" name="optradio4"><label>Poor</label>
                  </div>
               </div>
            </div>
            <div class = "row">
               <div class="form-group col-md-6">
                  <label class= "label_heading">Marking/ Packaging/ Shipment/ Safe receipt of material</label>
               </div>
               <div class = "col-md-6 Q5">
                  <div class="form-group">
                     <input type="radio" value ="Excellent" name="optradio5"><label>Excellent</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Good" name="optradio5"><label>Good</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Average" name="optradio5"><label>Average</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Fair" name="optradio5"><label>Fair</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Poor" name="optradio5"><label>Poor</label>
                  </div>
               </div>
            </div>
            <div class = "row">
               <div class="form-group col-md-6">
                  <label class= "label_heading">Product Quality with respect to Sales Order - Surface</label>
               </div>
               <div class = "col-md-6 Q6">
                  <div class="form-group">
                     <input type="radio" value ="Excellent" name="optradio6"><label>Excellent</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Good" name="optradio6"><label>Good</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Average" name="optradio6"><label>Average</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Fair" name="optradio6"><label>Fair</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Poor" name="optradio6"><label>Poor</label>
                  </div>
               </div>
            </div>
            <div class = "row">
               <div class="form-group col-md-6">
                  <label class= "label_heading">Product Quality with respect to Sales Order - Dimensions & Tolerances</label>
               </div>
               <div class = "col-md-6 Q7">
                  <div class="form-group">
                     <input type="radio" value ="Excellent" name="optradio7"><label>Excellent</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Good" name="optradio7"><label>Good</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Average" name="optradio7"><label>Average</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Fair" name="optradio7"><label>Fair</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Poor" name="optradio7"><label>Poor</label>
                  </div>
               </div>
            </div>
            <div class = "row">
               <div class="form-group col-md-6">
                  <label class= "label_heading">Product Quality with respect to Sales Order - Mechanical Properties</label>
               </div>
               <div class = "col-md-6 Q8">
                  <div class="form-group">
                     <input type="radio" value ="Excellent" name="optradio8"><label>Excellent</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Good" name="optradio8"><label>Good</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Average" name="optradio8"><label>Average</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Fair" name="optradio8"><label>Fair</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Poor" name="optradio8"><label>Poor</label>
                  </div>
               </div>
            </div>
            <div class = "row">
               <div class="form-group col-md-6">
                  <label class= "label_heading">Documentation at the time of dispatch like INVOICE/ GR/TEST CERTIFICATE - Presentation</label>
               </div>
               <div class = "col-md-6 Q9">
                  <div class="form-group">
                     <input type="radio" value ="Excellent" name="optradio9"><label>Excellent</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Good" name="optradio9"><label>Good</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Average" name="optradio9"><label>Average</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Fair" name="optradio9"><label>Fair</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Poor" name="optradio9"><label>Poor</label>
                  </div>
               </div>
            </div>
            <div class = "row">
               <div class="form-group col-md-6">
                  <label class= "label_heading">Documentation at the time of dispatch like INVOICE/ GR/TEST CERTIFICATE - Adequacy of Information</label>
               </div>
               <div class = "col-md-6 Q10">
                  <div class="form-group">
                     <input type="radio" value ="Excellent" name="optradio10"><label>Excellent</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Good" name="optradio10"><label>Good</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Average" name="optradio10"><label>Average</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Fair" name="optradio10"><label>Fair</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Poor" name="optradio10"><label>Poor</label>
                  </div>
               </div>
            </div>
            <div class = "row">
               <div class="form-group col-md-6">
                  <label class= "label_heading">Documentation at the time of dispatch like INVOICE/ GR/TEST CERTIFICATE - Correctness of content</label>
               </div>
               <div class = "col-md-6 Q11">
                  <div class="form-group">
                     <input type="radio" value ="Excellent" name="optradio11"><label>Excellent</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Good" name="optradio11"><label>Good</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Average" name="optradio11"><label>Average</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Fair" name="optradio11"><label>Fair</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Poor" name="optradio11"><label>Poor</label>
                  </div>
               </div>
            </div>
            <div class = "row">
               <div class="form-group col-md-6">
                  <label class= "label_heading">Submission of clear Techno-Commercial Quotation - On Time</label>
               </div>
               <div class = "col-md-6 Q12">
                  <div class="form-group">
                     <input type="radio" value ="Excellent" name="optradio12"><label>Excellent</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Good" name="optradio12"><label>Good</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Average" name="optradio12"><label>Average</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Fair" name="optradio12"><label>Fair</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Poor" name="optradio12"><label>Poor</label>
                  </div>
               </div>
            </div>
            <div class = "row">
               <div class="form-group col-md-6">
                  <label class= "label_heading">Overall performance</label>
               </div>
               <div class = "col-md-6 Q13">
                  <div class="form-group">
                     <input type="radio" value ="Excellent" name="optradio13"><label>Excellent</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Good" name="optradio13"><label>Good</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Average" name="optradio13"><label>Average</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Fair" name="optradio13"><label>Fair</label>
                  </div>
                  <div class="form-group">
                     <input type="radio" value="Poor" name="optradio13"><label>Poor</label>
                  </div>
               </div>
            </div>
            <br> 
         </div>
         <div id= "feedbackSubmitModal" role="dialog" class="home-modal">
            <div class="modal-dialog modal-dialog-centered">
               <!-- Modal content-->
               <div class="modal-content">
                  <div class="modal-header">
                     <h2 class="modal-title" id="feedBackTitle"></h2>
                  </div>
                  <div class="modal-body">
                     <div id="feedBackModalMessage"></div>
                  </div>
                  <div class="modal-footer">                  
                     <button class="btn btn-primary" type="submit" onclick="hidefeedbackModal()"><a href="${accountSupportHistoryPageUrl}" style= "color: white">OK</a></button>
                  </div>
               </div>
            </div>
         </div>
         <div class = "row">
            <div class= "feedBackcomments">
               <label>Your Valued Comments/Suggestions For Further Improvement:</label>
            </div>
            <br>
            <div>
               <textarea class="form-control" id="custFeedback" placeholder="Enter Your Comment Here..."></textarea>
            </div>
         </div>
         <div class = "row">
            <button class="cust-feedback-btn" onclick="submitFeedbackData()">Submit Feedback</button>
         </div>
         <br>
      </div>
   </div>
</div>
<script type="text/javascript"
   src="${commonResourcePath}/customAssets/js/customerFeedback.js"></script>