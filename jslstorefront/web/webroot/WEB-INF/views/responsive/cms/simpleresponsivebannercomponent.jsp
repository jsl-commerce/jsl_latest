<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<spring:url value="/reports/AccountStatementReport" var="reportAccountSummaryUrl" />
<spring:url value="/reports/SalesBillingReport" var="reportBillingSummaryUrl" />
<spring:url value="/reports/OrderBookingReport" var="reportOrderBookingUrl" />
<spring:url value="/reports/PopPlantReport" var="reportPOPplantUrl" />
<spring:url value="/reports/PopYardReport" var="reportPOPYardUrl" />
<spring:url value="/reports/ShipmentTrackingReport" var="vehicleTrackingReportUrl" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<spring:url value="/my-account/customer-feedback-analysis" var="customerFeedbackAdminPageUrl" />
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<!DOCTYPE html>
<html>
   <head>
      <title>JSL Site</title>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
      <link rel="stylesheet" type="text/css" media="all" href="./JSL Site_files/style.css">
      <link rel="stylesheet" type="text/css" media="all" href="${commonResourcePath}/customAssets/AuctionPage/css/customerDashBoard.css" />
      <link rel="stylesheet" type="text/css" media="all" href="./JSL Site_files/main.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
      <script src="https://cdn.anychart.com/js/8.0.1/anychart-core.min.js"></script>
      <script src="https://cdn.anychart.com/js/8.0.1/anychart-pie.min.js"></script>
      <script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
      <script async src="https://urldefense.proofpoint.com/v2/url?u=https-3A__www.googletagmanager.com_gtag_js-3Fid-3DUA-2D142673878-2D1&d=DwIFaQ&c=eIGjsITfXP_y-DLLX0uEHXJvU8nOHrUK8IrwNKOtkVU&r=-VkCbqDiz42qmVWnhStcNFiFiSaMrjEMChe3msrmMb0&m=wFfO8iJ0NPJpowq-NrtUQBs0wnGgGhq5wFpZ64QXfi0&s=tr6o1-SBp1BqKNjTXjbBoaLPghUtW292ysr8mc2R0x8&e= "></script> 
      <!-- Numeric JS -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/numeric/1.2.6/numeric.min.js"></script>
      <script src="${commonResourcePath}/customAssets/js/customer360Dashboard.js"></script>
      <script src="${commonResourcePath}/customAssets/js/jquery.cookie.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
   </head>
   <body class="page-auctionHomePage pageType-ContentPage">
      <div class="yCmsContentSlot container-fluid"></div>
      <a id="skip-to-content"></a>
      <div class="main__inner-wrapper">
         <div id="loading-wrapper">
            <div class="spinner">
               <div class="rect1"></div>
               <div class="rect2"></div>
               <div class="rect3"></div>
               <div class="rect4"></div>
               <div class="rect5"></div>
               <div class="rect6"></div>
            </div>
            <div class = "loader_text">
               <blinkLoaderText>Retrieving live data...please wait</blinkLoaderText>
            </div>
         </div>
         <!-- <div class="product_details_place_order_container"> --> 
         <div id="normalUser">
            <div id="content">
               <div>
                  <select id="compny_data">
                     <option value="both">Both</option>
                     <option value="jsl">JSL</option>
                     <option value="jshl">JSHL</option>
                  </select>
               </div>
               <div id="successDataCheck">
                  <div class="product_content">
                     <h3>Customer Dashboard</h3>
                     <div class="cust360_main_box">
                        <!--<div><button class="my_enq_btn">My Enquiry</button>
                           <button class="my_enq_btn">New Enquiry</button>
                           </div> -->
                        <!-- <div class="product__header">HPL-F05-N1-PRIME-400 series-IRSM44-97-409M Group-10-2500-790-0.953-NO1</div>   -->
                        <div class="product__body">
                           <div class ="row">
                              <div class="col-md-4">
                                 <div class="cust360_border">
                                    <a href="${reportAccountSummaryUrl}" class= "linksReports">
                                       <blink>Detailed Report</blink>
                                    </a>
                                    <div class= "graphValues"><b>* Ageing Vs Overdue Amount (Fig in INR)</b></div>
                                    <div id="reportTitleAccountSummary"></div>
                                    <div id="containerAccountSummary"></div>
                                    <div class="main-tableDiv sticky-table">
                                       <div class="pr_table_show cust360_table_display">
                                          <table class ="table table-striped table-bordered ordertable-striped order" id="cust360AccountTable">
                                             <thead>
                                                <tr class="sticky-header">
                                                   <!-- <th>O/S</th>
                                                      <th>Cr. Limit</th>
                                                      <th>Overdue</th>
                                                      <th>Aeging <30</th>
                                                      <th>Aeging 30-60</th>
                                                      <th>Aeging >60</th> -->
                                                      <th rowspan=2 style = "vertical-align: middle;">Company</th>
                                                   <th rowspan=2 style = "vertical-align: middle;">Outstanding</th>
                                                   <!-- <th rowspan=2 style = "vertical-align: middle;">Cr. Limit</th> -->
                                                   <th rowspan=2  style = "vertical-align: middle;">Overdue</th>
                                                   <th colspan=3>Overdue Ageing</th>
                                                <tr>
                                                   <th>0-30</th>
                                                   <th>30-60</th>
                                                   <th>>60</th>
                                                </tr>
                                                </tr>
                                             </thead>
                                             <tbody id="accountTableBody">
                                             </tbody>
                                          </table>
                                       </div>
                                    </div>
                                    <!-- <a href="" class= "linksReports">Visit our HTML tutorial</a> -->
                                 </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="cust360_border">
                                    <a href="${reportBillingSummaryUrl}" class= "linksReports">
                                       <blink>Detailed Report</blink>
                                    </a>
                                    <div class= "graphValues">* Series Vs MTD (Fig in MT)</div>
                                    <div id="reportTitleBillingSummary"></div>
                                    <div id="containerBillingSummary"></div>
                                    <div class="main-tableDiv sticky-table">
                                       <div class="pr_table_show cust360_table_display">
                                          <table class ="table table-striped table-bordered ordertable-striped order" id="cust360BillingSummaryTable">
                                             <thead>
                                                <tr class="sticky-header">
                                                   <th>Series</th>
                                                   <th>Yesterday Sale</th>
                                                   <th>MTD (Month till Date)</th>
                                                   <!-- <th>YTD</th> -->
                                                   
                                                </tr>
                                             </thead>
                                             <tbody id="billingSummaryTableBody">                                          
                                             </tbody>
                                             <tr style="font-weight: bold">
											   <td>Total</td>
											   <td id="totalYesrterDaySale"></td>
											   <td id="totalMTDSale"></td>
											 </tr>
                                          </table>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="cust360_border">
                                    <a href="${reportOrderBookingUrl}" class= "linksReports">
                                       <blink>Detailed Report</blink>
                                    </a>
                                    <div class= "graphValues">* Series Vs MTD (Fig in MT)</div>
                                    <div id="reportTitleOrderBookingSummary"></div>
                                    <div id="containerOrderBooking"></div>
                                    <div class="main-tableDiv sticky-table">
                                       <div class="pr_table_show cust360_table_display">
                                          <table class ="table table-striped table-bordered ordertable-striped order" id="cust360OrderBookingTable">
                                             <thead>
                                                <tr class="sticky-header">
                                                   <th>Series</th>
                                                   <th>Yesterday Booking</th>
                                                   <th>MTD (Month till Date)</th>
                                                   <!-- <th>YTD</th> -->
                                                </tr>
                                             </thead>
                                             <tbody id="orderBookingTableBody">
                                             </tbody>
                                              	<tr style="font-weight: bold">
											   <td>Total</td>
											   <td id="totalYesrterBookingSale"></td>
											   <td id="totalMTDBookingSale"></td>
											 </tr>
                                          </table>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class ="row">
                              <div class="col-md-6">
                                 <div class="cust360_border">
                                    <a href="${reportPOPplantUrl}" class= "linksReports">
                                       <blink>Detailed Report</blink>
                                    </a>
                                    <div class= "graphValues">* Series Vs Product Volume</div>
                                    <div id="reportTitlePopPlantSummary"></div>
                                    <div id="containerPopPlant"></div>
                                    <div class="main-tableDiv sticky-table">
                                       <div class="pr_table_show cust360_table_display">
                                          <table class ="table table-striped table-bordered ordertable-striped order" id="cust360PopPlantTable">
                                             <thead>
                                                <tr class="sticky-header">
                                                   <th>Series</th>
                                                   <th>CRAP</th>
                                                   <th>HRAP</th>
                                                   <th>Plate</th>
                                                   <th>HR Black</th>
                                                   <th>Total</th>
                                                </tr>
                                             </thead>
                                             <tbody id="popPlantTableBody">
                                             </tbody>
                                             <tr style="font-weight: bold">
											   <td>Total</td>
											   <td id="totalPlantCRAPSale"></td>
											   <td id="totalPlantHRAPSale"></td>
											    <td id="totalPlantPlateSale"></td>
											   <td id="totalPlantHRBLACKSale"></td>
											   <td id="totalAllPlantRowsSale"></td>
											 </tr>
                                          </table>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="cust360_border">
                                    <a href="${reportPOPYardUrl}" class= "linksReports">
                                       <blink>Detailed Report</blink>
                                    </a>
                                    <div class= "graphValues">* Series Vs Product Volume</div>
                                    <div id="reportTitlePopYardSummary"></div>
                                    <div id="containerPopYard"></div>
                                    <div class="main-tableDiv sticky-table">
                                       <div class="pr_table_show cust360_table_display">
                                          <table class ="table table-striped table-bordered ordertable-striped order" id="cust360PopYardTable">
                                             <thead>
                                                <tr class="sticky-header">
                                                   <th>Series</th>
                                                   <th>CRAP</th>
                                                   <th>HRAP</th>
                                                   <th>Plate</th>
                                                   <th>HR Black</th>
                                                   <th>Total</th>
                                                </tr>
                                             </thead>
                                             <tbody id="popYardTableBody">
                                             </tbody>
                                             <tr style="font-weight: bold">
											   <td>Total</td>
											   <td id="totalYardCRAPSale"></td>
											   <td id="totalYardHRAPSale"></td>
											    <td id="totalYardPlateSale"></td>
											   <td id="totalYardHRBLACKSale"></td>
											   <td id="totalAllYardRowsSale"></td>
											 </tr>
                                          </table>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              
<!--                               <div class="col-md-4"> -->
<!--                                  <div class="cust360_border"> -->
<%--                                     <a href="${vehicleTrackingReportUrl}" class= "linksReports"> --%>
<!--                                        <blink>Detailed Report</blink> -->
<!--                                     </a> -->
<!-- <!--                                     <div class= "graphValues">* Series Vs Product Volume</div> -->
<!--                                     <div id="reportTitleGpsTrackingSummary"></div> -->
<!--                                     <div id="containerGpsTrackingSummary"></div> -->
<!--                                     <div class="main-tableDiv sticky-table"> -->
<!--                                        <div class="pr_table_show cust360_table_display"> -->
<!-- <!--                                           <table class ="table table-striped table-bordered ordertable-striped order" id="cust360PopYardTable"> --> 
<!-- <!--                                              <thead> --> 
<!-- <!--                                                 <tr class="sticky-header"> --> 
<!-- <!--                                                    <th>Series</th> --> 
<!-- <!--                                                    <th>CRAP</th> --> 
<!-- <!--                                                    <th>HRAP</th> --> 
<!-- <!--                                                    <th>Plate</th> -->
<!-- <!--                                                    <th>HR Black</th> -->
<!-- <!--                                                    <th>Total</th> --> 
<!-- <!--                                                 </tr> --> 
<!-- <!--                                              </thead> --> 
<!-- <!--                                              <tbody id="popYardTableBody"> --> 
<!-- <!--                                              </tbody> --> 
<!-- <!--                                              <tr style="font-weight: bold"> --> 
<!-- <!-- 											   <td>Total</td> --> 
<!-- <!-- 											   <td id="totalYardCRAPSale"></td> -->
<!-- <!-- 											   <td id="totalYardHRAPSale"></td> --> 
<!-- <!-- 											    <td id="totalYardPlateSale"></td> --> 
<!-- <!-- 											   <td id="totalYardHRBLACKSale"></td> --> 
<!-- <!-- 											   <td id="totalAllYardRowsSale"></td> --> 
<!-- <!-- 											 </tr> --> 
<!-- <!--                                           </table> --> 
<!--                                        </div> -->
<!--                                     </div> -->
<!--                                  </div> -->
<!--                               </div> -->
                              
                              <!--   <div class="col-md-4">
                                 </div> -->
                              <div class ="disclaimer">* Information provided in the site is indicative and for information only.In case of any difference/clarity, please refer to our Customer Support Team.</div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div id="failureDataCheck">
                  <div class="product_content">
                     <div class="no_data_cust360_border">
                        <span class = "noData_text">Oops! Something went wrong. Please retry</span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div id="adminUser">
            <c:forEach items="${medias}" var="media">
               <c:choose>
                  <c:when test="${empty imagerData}">
                     <c:set var="imagerData">"${ycommerce:encodeJSON(media.width)}":"${ycommerce:encodeJSON(media.url)}"</c:set>
                  </c:when>
                  <c:otherwise>
                     <c:set var="imagerData">${imagerData},"${ycommerce:encodeJSON(media.width)}":"${ycommerce:encodeJSON(media.url)}"</c:set>
                  </c:otherwise>
               </c:choose>
               <c:if test="${empty altText}">
                  <c:set var="altTextHtml" value="${fn:escapeXml(media.altText)}"/>
               </c:if>
            </c:forEach>
            <c:url value="${urlLink}" var="simpleResponsiveBannerUrl" />
            <%-- <div class="simple-banner banner__component--responsive">
               <c:set var="imagerDataJson" value="{${imagerData}}"/>
               
               <c:choose>
               
               
                      <c:when test="${empty simpleResponsiveBannerUrl || simpleResponsiveBannerUrl eq '#'}">
               
                             <img src="${commonResourcePath}/customAssets/images/JslBanner_1400x480_BigSplash_EN_01_1400W.jpg">       
                             <img class="js-responsive-image" data-media='${fn:escapeXml(imagerDataJson)}' alt='${altTextHtml}' title='${altTextHtml}' style="">
                             
                      </c:when>
                      <c:otherwise>
                      
                             <img src="${commonResourcePath}/customAssets/images/JslBanner_1400x480_BigSplash_EN_01_1400W.jpg">       
                             <a href="${fn:escapeXml(simpleResponsiveBannerUrl)}">
                                   <img class="js-responsive-image" data-media='${fn:escapeXml(imagerDataJson)}' title='${altTextHtml}' alt='${altTextHtml}' style="">
                             </a>
                      </c:otherwise>
               </c:choose>
               </div> --%>
            <div class="slideshow-container">
               <div class="mySlides">
                  <img src="${commonResourcePath}/customAssets/images/banner55.jpg" style="width:100%">
               </div>
               <%-- <div class="mySlides">
                  <img src="${commonResourcePath}/customAssets/images/banner44.jpg" style="width:100%">
                  </div>
                  
                  <div class="mySlides">
                  <img src="${commonResourcePath}/customAssets/images/banner99.jpg" style="width:100%">  
                  </div> --%>
            </div>
            <br>
            <div style="text-align:center">
               <span class="dot"></span> 
               <span class="dot"></span> 
               <span class="dot"></span> 
            </div>
         </div>
         <div id="complaintAdminUser">
            <div class = "row">
               <div class= "col-md-9"><span style = "margin-left : 43px; font-weight : 500; margin-top: 17px;">Customer Complaint</span></div>
               <div class ="col-md-3">
                  <h2 style="float :left"><a href="${customerFeedbackAdminPageUrl}" class = "complaint_underline">Feedback Analysis Dashboard</a></h2>
               </div>
            </div>
            <div id="content">
               <div>
                  <select id="compny_data">
                     <option value="both">Both</option>
                     <option value="jsl">JSL</option>
                     <option value="jshl">JSHL</option>
                  </select>
               </div>
               <div id="successDataCheck">
                  <div class="product_content">
                     <h2>
                        Complaint Admin Dashboard <!-- <span class = "daysText">(Last 30 Days Data)</span> -->
                     </h2>
                     <div class="complaint_main_box">
                        <div class="product__body">
                           <div class ="row">
                              <div class="filter_box">
                                 <div>
                                    <div class= "p_attribute_item_title col-md-4">
                                       Filter By Complaint Type : 									
                                       <select id="cmplaintType" onchange="filterOnComplaintTypeData()">
                                          <option value="all" selected="">All</option>
                                          <option value="materialType">Material</option>
                                          <option value="serviceType">Service</option>
                                       </select>
                                       <button class="cust360_report_btn" id="resetFilterBtn" onclick="resetcomplaintData()">Reset Filter</button>
                                    </div>
                                    <div class="p_attribute_item_title col-md-4">
                                       Filter By Status : 									
                                       <select id="statusType" onchange="filterOnStatusData()">
                                          <option value="all" selected="">All</option>
                                          <option value="open">Open</option>
                                          <option value="inprocess">In Process</option>
                                          <option value="created">Created</option>
                                          <option value="resolved">Resolved</option>
                                          <option value="closed">Closed</option>
                                       </select>
                                       <button class="cust360_report_btn" id="resetFilterBtn" onclick="resetcomplaintData()">Reset Filter</button>
                                    </div>
                                    <div class="p_attribute_item_title col-md-4">
                                       Filter By Ageing : 									
                                       <select id="ageingType" onchange="filterOnAgeingData()">
                                          <option value="all" selected="">All</option>
                                          <option value="less30">< 30 Days</option>
                                          <option value="betw30to60">Between 30-60 Days</option>
                                          <option value="greater60">> 60 Days</option>
                                       </select>
                                       <button class="cust360_report_btn" id="resetFilterBtn" onclick="resetcomplaintData()">Reset Filter</button>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="customerComplaint_border">
                              <div id="containerComplaint"></div>
                           </div>
                           <br>
                           <!-- <div class ="customerComplaintDisclaimer">* Information provided in the site is indicative and for information only.In case of any difference/clarity, please refer to our Customer Support Team.</div> -->
                           <!-- Table started -->
                           <div class="cust_main-tableDiv cust_sticky-table">
                              <!-- <caption class="caption_add">Added Enquiries</caption><br>  -->
                              <div class="row">
                                 <div class="col-md-9 cust360_text_data">
                                    <h3>Complaint Ticket Data</h3>
                                 </div>
                                 <div class="col-md-3">
                                    <div>
                                       <button class="complaint_export_btn btn btn-primary" onclick="excelComplaintAdminExport()">Export Data</button>
                                    </div>
                                 </div>
                              </div>
                              <div class="pr_table_show cust360_table_display sticky-table" style = "overflow: scroll; height: auto; max-height: 400px">
                                 <table class="table table-striped table-bordered ordertable-striped order">
                                    <thead>
                                       <tr class="sticky-header">
                                          <th>complaint Number</th>
                                          <th>Complaint Type</th>
                                          <th>Invoice Number</th>
                                          <th>Nature Of Complaint</th>
                                          <th>Created Date</th>
                                          <th>Status</th>
                                       </tr>
                                    </thead>
                                    <tbody id="complaintTableBody" class="tableFix">
                                    </tbody>
                                 </table>
                                 <div class="pr_table_show" style ="display:none;" id="materialTable">
                                    <%--  <h3>Batch Details<i class="fa fa-close" onclick="toggleAccordion('#accordion_${loop.index}');" ></i></h3> --%>
                                    <table class ="table table-striped prods">
                                       <thead>
                                          <tr>
                                             <th>Batch Number</th>
                                             <!-- <th>Company Code</th> -->
                                             <th>Grade</th>
                                             <th>Thickness</th>
                                             <th>Width</th>
                                             <th>Length</th>
                                             <th>Finish</th>
                                             <th>Edge</th>
                                             <th>Primary Defect</th>
                                             <th>SO No</th>
                                             <th>Supply Qty</th>
                                             <th>Complaint Qty</th>
                                             <th>Plant</th>
                                             <th>Invoice Date</th>
                                             <th>KAM Name</th>
                                          </tr>
                                       </thead>
                                       <tbody id="complaintSubMaterialTableBody">
                                       </tbody>
                                    </table>
                                 </div>
                                 <div class="pr_table_show" style ="display:none;" id="serviceTable">
                                    <%-- <h3>Batch Details<i class="fa fa-close" onclick="toggleAccordion('#accordion_${loop.index}');" ></i></h3> --%>
                                    <table class ="table table-striped prods">
                                       <thead>
                                          <tr>
                                             <th>Sub Category</th>
                                             <th>Description</th>
                                          </tr>
                                       </thead>
                                       <tbody id="complaintServiceTableBody">
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div id="failureDataCheck">
                  <div class="product_content">
                     <div class="no_data_cust360_border">
                        <span class = "noData_text">Oops! Something went wrong. Please retry</span>
                     </div>
                  </div>
               </div>
            </div>
            <div>
            </div>
            <!-- </div> -->
         </div>
      </div>
      </main>
      </div>
      <div id="cyberCrimeIntimationMessageWrapper" style="display:none;">
	    <div id="cyberCrimeIntimationMessage">
      		<common:homePageWelcomePopUp/>
      	</div>
      </div>
      
   </body>
   <script>
      var slideIndex = 0;
      showSlides();
      
      function showSlides() {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("dot");
        for (i = 0; i < slides.length; i++) {
          slides[i].style.display = "none";  
        }
        slideIndex++;
        if (slideIndex > slides.length) {slideIndex = 1}    
        for (i = 0; i < dots.length; i++) {
          dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex-1].style.display = "block";  
        dots[slideIndex-1].className += " active";
        setTimeout(showSlides, 1500); // Change image every 2 seconds
      }
   </script>
   <script>
      var today = new Date();
      
      $('#aStartDate').datepicker({
      	format: 'mm/dd/yyyy',
      	autoclose: true,
      
      });
      
      $('#aEndDate').datepicker({
      	format: 'mm/dd/yyyy',
      	autoclose: true,
      
      });
      
      $("#endDateIcon").on('click', function () {
      	$("#aEndDate").show().focus();
      });
      
      $("#startDateIcon").on('click', function () {
      	$("#aStartDate").show().focus();
      });
      
      function excelComplaintAdminExport()
      {
      window.location.href="${request.contextPath}/my-account/exportCustomerComplaintreport";
      }
      
   </script>
</html>