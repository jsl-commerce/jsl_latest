<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="pagination" tagdir="/WEB-INF/tags/responsive/nav/pagination"%>
<spring:url value="/auction" var="homeAuctionUrl" />
<spring:url value="/auction/liveAuction?auctionID=All" var="liveAuctionUrl" />
<spring:url value="/auction/completedAuction" var="completedAuctionUrl" />
<spring:url value="/auction/futureAuction" var="upcomingAuctionUrl" />
<spring:url value="/auction/watchList" var="watchListUrl" />
<spring:url value="/auction/userManual" var="userManualUrl" />

        <!-- Logo -->
        <h1 class="auctionlogo">
            <a><img src="${commonResourcePath}/customAssets/AuctionPage/images/logo.png"></a>
        </h1>
<div class="tollfree_no">Help Line Nos: <br><i class="lni-phone"></i>  +91-124 449 4876<br><i class="lni-phone-handset"></i>  +91-882 650 3315</div>
        <!-- Nav -->
        <nav id="nav">
            <ul>
                <li id="auctionHomePage" class="leftNav"><a href="${homeAuctionUrl}"><i class="icon icon-home"></i> Today's Auction</a></li>
                <li id="auctionLivePage" class="leftNav"><a onclick="navigatePageToSelectedAuction();" class="isDisabled" id="live_auction"><i class="lni-construction-hammer"></i> Live Auction</a></li>
                <li id="auctionWatchListPage" class="leftNav"><a onclick="navigatePageToSelectedAuction();" class="isDisabled" id="watchlist_auction"><i class="lni-star"></i> Watch List</a></li>
                <li id="auctionCompletedPage" class="leftNav">
                	<form id="fromLeftNavToCompleted" action="${completedAuctionUrl}" method="GET">
	                    <input type="hidden" id="dateRange" name="dateRange" value="false" /> 
						<!-- <input type="hidden" id="CSRFToken" name="CSRFToken" value /> -->
                		<a onclick="navigatePageToCompletedAuction();"><i class="lni-lock"></i> Completed Auction</a>
                	</form>	
                </li>
                <li id="auctionUpcomingPage" class="leftNav"><a href="${upcomingAuctionUrl}"><i class="fa fa-calendar"></i> Upcoming Auction</a></li>
                <li id="auctionUserManual" class="leftNav"><a href="${userManualUrl}"><i class="lni-lock"></i> User Manual</a></li>
            </ul>
        </nav>

  <section class="box recent-comments filterSection">
            <header>
                <h2 class="filterHeading"><i class="lni-funnel"></i> Filter</h2>
            </header>

            <form>
		<div class="form-group">
                <input type="checkbox" id="serFlter" onclick="selectSeriesFunction()"> Series
                </div>
                <!-- onchange="displayGrades()" -->
                <select id="seriesFilter" style="display:none" onchange="displayGrades()">
                            <option value="all" id="all" selected>All</option>  
                            <option value="200 Series" id="a" >200 Series</option>
                            <option value="300 Series" id="b">300 Series</option>
                            <option value="400 Series" id="c">400 Series</option>
                            <option value="Duplex" id="d">Duplex</option>
                </select>
<div class="form-group">
                <input type="checkbox" id="grdFlter" onclick="selectGradeFunction()"> Grade Group
</div>
                <div id="gradesListAll" style="display:none; height: 150px; overflow-y: scroll;background-color: white;padding-left:10px;">
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_0" value="202 Group" />202<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_1" value="204CU Group" />204CU<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_2" value="253 MA Group" />253 MA<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_3" value="J4 Group" />J4<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_4" value="JSLAUS Group" />JSLAUS<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_5" value="JSLU DD Group" />JSLU DD<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_6" value="201 Group" />201<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_7" value="JT Group" />JT<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_8" value="301 Group" />301<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_9" value="304 Group" />304<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_10" value="304L Group" />304L<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_11" value="309 Group" />309<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_12" value="310 Group" />310<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_13" value="316 Ti Group" />316 Ti<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_14" value="316L Group" />316L<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_15" value="317 Group" />317<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_16" value="321 Group" />321<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_17" value="347 Group" />347<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_18" value="EN 1.4529 Group" />EN 1.4529<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_19" value="SUPER AUSTENITINC GROUP" />SUPER AUSTENITINC<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_20" value="405 Group" />405<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_21" value="409 Group" />409<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_22" value="409M Group" />409M<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_23" value="410 Group" />410<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_24" value="415 Group" />415<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_25" value="420 Group" />420<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_26" value="430 Group" />430<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_27" value="432 Group" />432<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_28" value="436 Group" />436<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_29" value="439 Group" />439<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_30" value="439 Group" />439<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_31" value="441 Group" />441<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_32" value="Duplex Group" />Duplex<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxALL_33" value="SUPER DUPLEX GROUP" />SUPER DUPLEX<br>
                </div>
                <div id="gradesListA" style="display:none; height: 150px; overflow-y: scroll;background-color: white;padding-left:10px;">
                    <input type="checkbox" class="gradeBox" id="gradeBoxA_0" value="201 Group" />201<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxA_1" value="202 Group" />202<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxA_2" value="204CU Group" />204CU<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxA_3" value="253 MA Group" />253 MA<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxA_4" value="J4 Group" />J4<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxA_5" value="JSLAUS Group" />JSLAUS<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxA_6" value="JSLU DD Group" />JSLU DD<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxA_7" value="JT Group" />JT<br>
                </div>
                <div id="gradesListB" style="display:none; height: 150px; overflow-y: scroll;background-color: white;padding-left:10px;">
                    <input type="checkbox" class="gradeBox" id="gradeBoxB_0" value="301 Group" />301<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxB_1" value="304 Group" />304<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxB_2" value="304L Group" />304L<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxB_3" value="309 Group" />309<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxB_4" value="310 Group" />310<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxB_5" value="316 Ti Group" />316 Ti<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxB_6" value="316L Group" />316L<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxB_7" value="317 Group" />317<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxB_8" value="321 Group" />321<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxB_9" value="347 Group" />347<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxB_10" value="EN 1.4529 Group" />EN 1.4529<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxB_11" value="SUPER AUSTENITINC GROUP" />SUPER AUSTENITINC<br>
                </div>
                <div id="gradesListC" style="display:none; height: 150px; overflow-y: scroll;background-color: white;padding-left:10px;">
                    <input type="checkbox" class="gradeBox" id="gradeBoxC_0" value="405 Group" />405<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxC_1" value="409 Group" />409<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxC_2" value="409M Group" />409M<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxC_3" value="410 Group" />410<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxC_4" value="415 Group" />415<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxC_5" value="420 Group" />420<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxC_6" value="430 Group" />430<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxC_7" value="432 Group" />432<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxC_8" value="436 Group" />436<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxC_9" value="439 Group" />439<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxC_10" value="439 Group" />439<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxC_11" value="441 Group" />441<br>
                </div>
                <div id="gradesListD" style="display:none; height: 150px; overflow-y: scroll;background-color: white;padding-left:10px;">
                    <input type="checkbox" class="gradeBox" id="gradeBoxD_0" value="Duplex Group" />Duplex<br>
                    <input type="checkbox" class="gradeBox" id="gradeBoxD_1" value="SUPER DUPLEX GROUP" />SUPER DUPLEX<br>
                </div>
                <div class="filterGroup_btn">
                <button class="btn filter_btn" onclick="filterTable()">Filter</button>
                <button class="btn reset_btn" onclick="resetTable()">Reset</button>
				</div>
            </form>
        </section>
          
       