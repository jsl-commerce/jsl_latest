<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>

<div id="product-facet" class="hidden-sm hidden-xs product__facet js-product-facet">
		<div id="sidebar">
		<span class="facet_close"><i class="lni-close"></i></span>
		<div class="b-page__item b-page__item--filter">
		                  <div class="b-filter-new js-filter-new">
		                     <div class="b-tabs-new">
		                       
		                        <div class="b-tabs-new__bodies">
							        <div class="b-tabs-new__body">
									    <nav:facetNavAppliedFilters pageData="${searchPageData}"/>
									    <nav:facetNavRefinements pageData="${searchPageData}"/>
									</div>
								</div>
							</div>
							</div>
							</div>
			</div>
</div>


<script>
$(".facet_close").click(function(){
	$(this).parents("#sidebar").toggleClass("hide_sidebar");
	$(".facet_close .lni-close").toggleClass("lni-menu");
	$(".plp_flat_product").toggleClass("full_veiw_content");
	
});

$(".b-filter-new__facet h4").click(function(){	
$(this).toggleClass("non_active");
$(this).next(".facet-field").toggleClass("hide");
	
});

</script>