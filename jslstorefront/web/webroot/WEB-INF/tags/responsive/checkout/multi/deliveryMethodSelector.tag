<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="deliveryMethods" required="true"
	type="java.util.List"%>
<%@ attribute name="selectedDeliveryMethodId" required="false"
	type="java.lang.String"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="multi-checkout"
	tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ attribute name="cost" required="true" type="java.lang.Integer"%>
<%@ attribute name="supplyLocation" required="true"
	type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<select id="delivery_method" name="delivery_method" class="form-control">
	<c:choose>
		<c:when test="${supplyLocation eq 'PLANT'}">
			<c:forEach items="${deliveryMethods}" var="deliveryMethod">
				<c:choose>
					<c:when test="${deliveryMethod.code eq 'free-road'}">
						<multi-checkout:deliveryMethodDetails
							deliveryMethod="${deliveryMethod}"
							isSelected="${deliveryMethod.code}" />
					</c:when>
					<c:otherwise>
					</c:otherwise>
				</c:choose>

			</c:forEach>
		</c:when>
		<c:otherwise>
			<c:choose>
				<c:when test="${Integer(deliveryCost)>0 }">
					<c:forEach items="${deliveryMethods}" var="deliveryMethod">
						<c:choose>
							<c:when test="${deliveryMethod.code ne 'free-road'}">
								<multi-checkout:deliveryMethodDetails
									deliveryMethod="${deliveryMethod}"
									isSelected="${deliveryMethod.code eq 'standard-gross'}" />
							</c:when>
							<c:otherwise>
							</c:otherwise>
						</c:choose>

					</c:forEach>
				</c:when>
				<c:otherwise>
					<c:forEach items="${deliveryMethods}" var="deliveryMethod">
						<c:choose>
							<c:when test="${deliveryMethod.code eq 'customer-gross'}">
								<multi-checkout:deliveryMethodDetails
									deliveryMethod="${deliveryMethod}"
									isSelected="${deliveryMethod.code}" />
							</c:when>
							<c:otherwise>
							</c:otherwise>
						</c:choose>

					</c:forEach>
				</c:otherwise>
			</c:choose>

		</c:otherwise>
	</c:choose>
</select>
