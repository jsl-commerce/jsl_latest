<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="searchUrl" required="true" %>
<%@ attribute name="searchPageData" required="true"
              type="de.hybris.platform.commerceservices.search.pagedata.SearchPageData" %>
<%@ attribute name="top" required="true" type="java.lang.Boolean" %>
<%@ attribute name="showTopTotals" required="false" type="java.lang.Boolean" %>
<%@ attribute name="supportShowAll" required="true" type="java.lang.Boolean" %>
<%@ attribute name="supportShowPaged" required="true" type="java.lang.Boolean" %>
<%@ attribute name="additionalParams" required="false" type="java.util.HashMap" %>
<%@ attribute name="msgKey" required="false" %>
<%@ attribute name="showCurrentPageInfo" required="false" type="java.lang.Boolean" %>
<%@ attribute name="hideRefineButton" required="false" type="java.lang.Boolean" %>
<%@ attribute name="numberPagesShown" required="false" type="java.lang.Integer" %>
<%@ attribute name="startDate" required="false" type="java.lang.String" %>
<%@ attribute name="endDate" required="false" type="java.lang.String" %>
<%@ attribute name="orderStatus" required="false" type="java.lang.String" %>
<%@ taglib prefix="pagination" tagdir="/WEB-INF/tags/responsive/nav/pagination" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/jsCalendar.js"></script>
<script type="text/javascript" src="${commonResourcePath}/customAssets/AuctionPage/js/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
                                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />

<spring:htmlEscape defaultHtmlEscape="true" />

<c:set var="themeMsgKey" value="${not empty msgKey ? msgKey : 'search.page'}"/>
<c:set var="showCurrPage" value="${not empty showCurrentPageInfo ? showCurrentPageInfo : false}"/>
<c:set var="hideRefBtn" value="${hideRefineButton ? true : false}"/>
<c:set var="showTotals" value="${empty showTopTotals ? true : showTopTotals}"/>

<c:if test="${searchPageData.pagination.totalNumberOfResults == 0 && top && showTotals}">
    <div class="paginationBar top clearfix">
         <div id="snackBar"></div>
                    <div class="row">
                        <div class="col-sm-2">
                                            <div class="form-group">
                                            <label>Start Date</label> 
                                          <div class="input-group">
                                             <input type="text" id="aStartDate"  value="${startDate}" class="form-control hasDatepicker"/>
                                        <span class="input-group-addon" id="startDateIcon">
    <span class="fa fa-calendar">
    </span>
</span></div>                                                  
                                        </div>
                                        </div>
                                        <div class="col-sm-2">
                                        <div class="form-group">
                                            <label>End Date</label>
                                            <div class="input-group">
                                              <input type="text" id="endDate" value="${endDate}" class="form-control hasDatepicker"/>
                                        <span class="input-group-addon" id="endDateIcon">
       <span class="fa fa-calendar"></span>
    </span></div>

                                        </div>
                                        </div>
                                        <div class="col-sm-2">
                                        <label>Order Status</label>
                                        	<select class="form-control" id="status" select="${orderStatus}"> 
                                        		<option value="">Select</option>
                                        		<option value="pending">Pending</option>
                                        		<option value="approved">Approved</option>
                                        		<option value="rejected">Rejected</option>
                                        	</select>
                                        </div>
                                           <div class="col-sm-1">
                                        <label>Auction Id</label>
                                        	<select class="form-control" id="auctionIdS" > 
                                        		<option value="">Select</option>
                                        		<c:forEach items="${auctionIdList}" var="item">
                                        			<option value="${item}">${item}</option>
                                        		</c:forEach>
                                        	</select>
                                        </div>
                                        <div class="col-sm-1" style= "margin-top: 16px;">
                                        	<button class="btn btn-primary" onclick="filter()">Filter</button>
                                        </div>
										
										<div class="col-sm-1" style= "margin-top: 16px;">
                                        	<button class="btn btn-primary" onclick="reset()">Reset</button>
                                        </div>
										
                        <div class="col-sm-4 pagination-wrap">
                            <pagination:pageSelectionPagination searchUrl="${searchUrl}" searchPageData="${searchPageData}"
                                                                numberPagesShown="${numberPagesShown}"
                                                                themeMsgKey="${themeMsgKey}"/>
                        </div>

                        <c:if test="${not hideRefBtn}">
                            <div class="col-xs-12 col-sm-2 col-md-4 hidden-md hidden-lg">
                                <ycommerce:testId code="searchResults_refine_button">
                                    <product:productRefineButton styleClass="btn btn-default js-show-facets"/>
                                </ycommerce:testId>
                            </div>
                        </c:if>
                    </div>
                </div>
    </div>
</c:if>

<c:if test="${searchPageData.pagination.totalNumberOfResults > 0}">
    <div class="pagination-bar ${(top)?"top":"bottom"}">
        <div class="pagination-toolbar">
            <c:if test="${not empty searchPageData.sorts}">
                <div class="helper clearfix hidden-md hidden-lg"></div>
                <div class="sort-refine-bar">
                    <div id="snackBar"></div>
                    <div class="row">
                        <div class="col-sm-2">
                                            <div class="form-group">
                                            <label>Start Date</label> 
                                          <div class="input-group">
                                             <input type="text" id="aStartDate"  value="${startDate}" class="form-control hasDatepicker"/>
                                        <span class="input-group-addon" id="startDateIcon">
    <span class="fa fa-calendar">
    </span>
</span></div>                                                  
                                        </div>
                                        </div>
                                        <div class="col-sm-2">
                                        <div class="form-group">
                                            <label>End Date</label>
                                            <div class="input-group">
                                              <input type="text" id="endDate" value="${endDate}" class="form-control hasDatepicker"/>
                                        <span class="input-group-addon" id="endDateIcon">
       <span class="fa fa-calendar"></span>
    </span></div>

                                        </div>
                                        </div>
                                        <div class="col-sm-2">
                                        <label>Order Status</label>
                                        	<select class="form-control" id="status" select="${orderStatus}"> 
                                        		<option value="">Select</option>
                                        		<option value="pending">Pending</option>
                                        		<option value="approved">Approved</option>
                                        		<option value="rejected">Rejected</option>
                                        	</select>
                                        </div>
                                        <div class="col-sm-1">
                                        <label>Auction Id</label>
                                        	<select class="form-control" id="auctionIdS" > 
                                        		<option value="">Select</option>
                                        		<c:forEach items="${auctionIdList}" var="item">
                                        			<option value="${item}">${item}</option>
                                        		</c:forEach>
                                        	</select>
                                        </div>
                                        <div class="col-sm-1" style= "margin-top: 16px;">
                                        	<button class="btn btn-primary" onclick="filter()">Filter</button>
                                        </div>
										
										<div class="col-sm-1" style= "margin-top: 16px;">
                                        	<button class="btn btn-primary" onclick="reset()">Reset</button>
                                        </div>
										
                        <div class="col-sm-4 pagination-wrap">
                            <pagination:pageSelectionPagination searchUrl="${searchUrl}" searchPageData="${searchPageData}"
                                                                numberPagesShown="${numberPagesShown}"
                                                                themeMsgKey="${themeMsgKey}"/>
                        </div>

                        <c:if test="${not hideRefBtn}">
                            <div class="col-xs-12 col-sm-2 col-md-4 hidden-md hidden-lg">
                                <ycommerce:testId code="searchResults_refine_button">
                                    <product:productRefineButton styleClass="btn btn-default js-show-facets"/>
                                </ycommerce:testId>
                            </div>
                        </c:if>
                    </div>
                </div>
            </c:if>
        </div>
        <c:if test="${top && showTotals}">
            <div class="row">
                <div class="col-xs-12">
                    <div class="pagination-bar-results">
                        <ycommerce:testId code="searchResults_productsFound_label">
                            <c:choose>
                                <c:when test="${showCurrPage}">
                                    <c:choose>
                                        <c:when test="${searchPageData.pagination.totalNumberOfResults == 1}">
                                            <spring:theme code="${themeMsgKey}.totalResultsSingleOrder"/>
                                        </c:when>
                                        <c:when test="${searchPageData.pagination.numberOfPages <= 1}">
                                            <spring:theme code="${themeMsgKey}.totalResultsSinglePag"
                                                          arguments="${searchPageData.pagination.totalNumberOfResults}"/>
                                        </c:when>
                                        <c:otherwise>
                                            <c:set var="currentPageItems"
                                                   value="${(searchPageData.pagination.currentPage + 1) * searchPageData.pagination.pageSize}"/>
                                            <c:set var="upTo"
                                                   value="${(currentPageItems > searchPageData.pagination.totalNumberOfResults ? searchPageData.pagination.totalNumberOfResults : currentPageItems)}"/>
                                            <c:set var="currentPage"
                                                   value="${searchPageData.pagination.currentPage * searchPageData.pagination.pageSize + 1} - ${upTo}"/>
                                            <spring:theme code="${themeMsgKey}.totalResultsCurrPage"
                                                          arguments="${currentPage},${searchPageData.pagination.totalNumberOfResults}"/>
                                        </c:otherwise>
                                    </c:choose>
                                </c:when>
                                <c:otherwise>
                                    <spring:theme code="${themeMsgKey}.totalResults"
                                                  arguments="${searchPageData.pagination.totalNumberOfResults}"/>
                                </c:otherwise>
                            </c:choose>
                        </ycommerce:testId>
                    </div>
                </div>
            </div>
        </c:if>
    </div>
</c:if>
<script>
$("#status").val("${orderStatus}");
$("#auctionIdS").val("${auctionId}");
$('#aStartDate').datepicker({
	format: 'mm/dd/yyyy',
	autoclose:true
	
});
    
    $("#startDateIcon").on('click', function(){
      	$("#aStartDate").show().focus();
      	
      });


$('#endDate').datepicker({
	format: 'mm/dd/yyyy',
	autoclose:true
});

$("#endDateIcon").on('click', function(){
  	$("#endDate").show().focus();
  	
  });
function filter(){
	  var status = $("#status").val();
	  var startDate = $("#aStartDate").val();
	  var endDate = $("#endDate").val();
	  var auctionId =$("#auctionIdS").val();
	  validate(status,startDate,endDate,auctionId);
	  
	 
}

function snackBarError(str){
	var snackbar = document.getElementById("snackBar");
	snackbar.innerHTML = str;
    snackbar.style.backgroundColor = "red";
    snackbar.className = "show";
    setTimeout(function() { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
}
function validate(status,startDate,endDate,auctionId)
{
	
	
	if(status=="" && startDate=="" && endDate=="" && auctionId=="")
		{
		snackBarError('Please select some filter parameter');
		}
	
	else if(startDate!=""&&endDate=="")
		{
		snackBarError('Please enter end date');
		}
	else if(endDate!=""&&startDate=="")
		{
		snackBarError('Please enter start date');
		}
	else if(startDate!=""&&endDate!="")
		{
		var sDate = new Date(startDate);
		var eDate = new Date(endDate);
		if(sDate.getTime() > eDate.getTime())
		{
			snackBarError('Start date cannot be grater than end date');
		}
		else{
			window.location.href='../my-account/approval-dashboard?orderStatus='+status+'&startDate='+startDate+'&endDate='+endDate+'&auctionId='+auctionId;
		}
		}
	
	else
		{
		 window.location.href='../my-account/approval-dashboard?orderStatus='+status+'&startDate='+startDate+'&endDate='+endDate+'&auctionId='+auctionId;
		}
	
}

function reset(){
	  window.location.href='../my-account/approval-dashboard';
}
</script>