<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
* {
  box-sizing: border-box;
}

.paragraphtext {
  font-family: Century Gothic;
  justify-content: justify;

}

/* Style the header */
.heading {
  background-color: #D35400;
  padding: 1px;
  text-align: center;
  font-size: 25px;
  color: white;
  //background-image: url("http://www.jslstainless.com/assets/images/logo.png");
  background-repeat: no-repeat;
  background-position: right top;
}

/* Container for flexboxes */
.sectionclass {
  display: -webkit-flex;
  display: flex;
  justify-content: justify;
}


/* Style the content */
article {
  -webkit-flex: 3;
  -ms-flex: 3;
  flex: 3;
  background-color: #f1f1f1;
  padding: 10px;
  justify-content: justify;
}

/* Style the footer */
/* footer {
  background-color: #777;
  padding: 10px;
  text-align: center;
  color: white;
} */

/* Responsive layout - makes the menu and the content (inside the section) sit on top of each other instead of next to each other */
@media (max-width: 600px) {
  .sectionclass {
    -webkit-flex-direction: column;
    flex-direction: column;
    justify-content: justify;
  }
}
</style>
<span class="heading">
  <h2>
	BE AWARE AND DON'T BECOME VICTIM OF CYBERCRIME! 
 </h2>
</span>
<div class="paragraphtext">
<div class="sectionclass" align="justify">
  
  <article>
    <h1></h1>
	
<p>
<!-- <h1>2.	DEFINITIONS:</h1>
<P align="justify">Unless the context otherwise requires under the terms and conditions of the auction sale, the following words shall have the meaning as assigned to them hereunder:<br /></p> -->

Cybercrime a major challenge for all corporations globally. More and more often, fraudsters use fake bank details to direct payments to false accounts. For this purpose, they pretend to be official representatives of the business partner. Often, they even copy the names and contact addresses of trusted contacts. To do this, the fraudsters use a variety of sources and entrance doors to obtain information about the business transaction.


<P align="justify">Against this background and for your protection, we kindly highlight to you our procedure for financially relevant changes to master data:<br /></p>

1.	We do not authorize third parties (like agents) to collect receivables on behalf of JSL.<br /><br />
2.	We never inform you on short notice regarding any changes in bank account details (in particular not via e-mail), but will contact you at least 1 month in advance.<br /><br />
3.	If bank details change, we inform our customer in writing through a letter signed by authorized officers at JSL.<br /><br /><br />

If you are contacted by us by e-mail regarding payment instructions such as changes of account numbers, please always carefully check the e-mail address of the sender. Unfortunately, forged addresses are not always easy to identify, and the devil is often in the details. 
Please do not hesitate to contact your direct contact at JSL to alert us to suspicious activities so that we can take legal and technical action.


<P align="justify">And most importantly: Please always double-check any master data change requests via a telephone call with your well-known contact persons at JSL.<br /></p>

  </article>
</div>

</div>


