<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="hideHeaderLinks" required="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>

<spring:url value="/my-account/faq" var="FAQURL" />

<spring:htmlEscape defaultHtmlEscape="true" />

<cms:pageSlot position="TopHeaderSlot" var="component" element="div" >
	<cms:component component="${component}" />
</cms:pageSlot>

<header class="js-mainHeader">
	<nav class="navigation navigation--top">
		<div class="row">
			
			
				<%--  <div class="nav__left js-site-logo">
					<cms:pageSlot position="SiteLogo" var="logo" limit="1">
						<cms:component component="${logo}" element="div" class="yComponentWrapper"/>
					</cms:pageSlot>
				</div>  --%>
			
			<div class="col-sm-12 col-md-12">
				<div class="nav__right">
					<ul class="nav__links nav__links--account">
						<c:if test="${empty hideHeaderLinks}">
							<c:if test="${uiExperienceOverride}">
								<li class="backToMobileLink">
									<c:url value="/_s/ui-experience?level=" var="backToMobileStoreUrl" />
									<a href="${fn:escapeXml(backToMobileStoreUrl)}">
										<spring:theme code="text.backToMobileStore" />
									</a>
								</li>
							</c:if>

							<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
								<c:set var="maxNumberChars" value="25" />
								<c:if test="${fn:length(user.firstName) gt maxNumberChars}">
									<c:set target="${user}" property="firstName"
										value="${fn:substring(user.firstName, 0, maxNumberChars)}..." />
								</c:if>

								<li class="logged_in js-logged_in"><i class="icon icon-user"></i>
									<ycommerce:testId code="header_LoggedUser">
										<spring:theme code="header.welcome" arguments="${user.firstName},${user.lastName}" />
									</ycommerce:testId>
									<span class="line">|</span>
								</li>
							</sec:authorize>

						
							 <cms:pageSlot position="HeaderLinks" var="link">
								 <cms:component component="${link}" element="li" />
							 </cms:pageSlot>

							<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')" >
							
								<li class="liOffcanvas">
									<ycommerce:testId code="header_Login_link">
										<c:url value="/login" var="loginUrl" />
										<a href="${fn:escapeXml(loginUrl)}">
											<spring:theme code="header.link.login" />
										</a>
									</ycommerce:testId>
								</li>
							</sec:authorize>

							<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')" >
							<span class="line">|</span>
								<li class="liOffcanvas">
									<ycommerce:testId code="header_signOut">
										<c:url value="/logout" var="logoutUrl"/>
										<a class="logout_link" href="${fn:escapeXml(logoutUrl)}">
										
											<spring:theme code="header.link.logout" />
										</a>
									</ycommerce:testId>
								</li>
							</sec:authorize>

						</c:if>
					</ul>
				</div>
			</div>
		</div>
	</nav>
	<%-- a hook for the my account links in desktop/wide desktop--%>
	<div class="js-secondaryNavAccount collapse myProfileDropdown" id="accNavComponentDesktopOne">
		<ul class="nav__links">

		</ul>
	</div>
	<div class="hidden-xs hidden-sm js-secondaryNavCompany collapse" id="accNavComponentDesktopTwo">
		<ul class="nav__links js-nav__links">

		</ul>
	</div>
	<nav class="navigation navigation--middle js-navigation--middle">
		<div class="container-fluid">
			<div class="col-sm-12 mobile_view">
				<div class="mobile__nav__row mobile__nav__row--table">
					<div class="mobile__nav__row--table-group">
						<div class="mobile__nav__row--table-row">
							<div class="mobile__nav__row--table-cell visible-xs hidden-sm">
								<button class="mobile__nav__row--btn btn mobile__nav__row--btn-menu js-toggle-sm-navigation"
										type="button">
									<span class="glyphicon glyphicon-align-justify"></span>
								</button>
							</div>

							<div class="visible-xs mobile__nav__row--seperator hidden-xs hidden-sm">
								<ycommerce:testId code="header_search_activation_button">
									<button	class="mobile__nav__row--btn btn mobile__nav__row--btn-search js-toggle-xs-search hidden-sm hidden-md hidden-lg" type="button">
										<span class="glyphicon glyphicon-search"></span>
									</button>
								</ycommerce:testId>
							</div>

							 <c:if test="${empty hideHeaderLinks}">
								<ycommerce:testId code="header_StoreFinder_link">
									<div class="mobile__nav__row--table-cell hidden-sm hidden-md hidden-lg mobile__nav__row--seperator">
										<c:url value="/store-finder" var="storeFinderUrl"/>
										<a href="${fn:escapeXml(storeFinderUrl)}" class="mobile__nav__row--btn mobile__nav__row--btn-location btn">
											<span class="lni-map-marker"></span>
										</a>
									</div>
								</ycommerce:testId>
							</c:if> 

							<cms:pageSlot position="MiniCart" var="cart" element="div" class="miniCartSlot componentContainer hidden-md hidden-lg hidden-xs hidden-sm">
								<cms:component component="${cart}" element="div" class="mobile__nav__row--table-cell" />
							</cms:pageSlot>

						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-12 desktop__nav">
				<!-- <div class="nav__left col-xs-12 col-sm-6">
					<div class="row">
						<div class="col-sm-2 hidden-xs visible-sm mobile-menu">
							<button class="btn js-toggle-sm-navigation" type="button">
								<span class="glyphicon glyphicon-align-justify"></span>
							</button>
						</div>
						
					</div>
				</div> -->
				
			<div class="col-sm-12 col-md-2 mobile-logo">
			
			 <div class="nav__left js-site-logo">
			 <%--  <img src="${commonResourcePath}/customAssets/images/logo.jpg" alt="Jindal"> --%>
					 <cms:pageSlot position="SiteLogo" var="logo" limit="1">
						<cms:component component="${logo}" element="div" class="yComponentWrapper"/>
					</cms:pageSlot> 
				</div> 
			
			
			<%-- <div class="nav__left js-site-logo">
				<a href="#"><img src="${commonResourcePath}/customAssets/images/logo.jpg"
				alt="${fn:escapeXml(media.altText)}" src="${fn:escapeXml(media.url)}"></a>
			</div> --%>
			</div>
				<div class="nav__right col-md-10 col-xs-12 pull-right mobile-middle-header">
				
							<div class="site-search">
<!-- Disable SearchBox for 1st Sprint  -->							
								<cms:pageSlot position="SearchBox" var="component">
									<cms:component component="${component}" element="div"/>
								</cms:pageSlot> 
							</div>
						
					<ul class="nav__links nav__links--shop_info">
					<li class="nav-icon"><a href="javascript:void(0)" class="btn"><i class="lni-menu"></i></a></li>
					<li class="search-icon"><a href="javascript:void(0)" class="btn"><i class="icon icon-magnifier"></i></a></li>
					<li class="home"><a href="javascript:void(0)" class="btn"><i class="icon icon-home"></i></a></li>
					<!-- <li class="home"><a href="#" class="btn"><i class="icon icon-home" title= "Dashboard"></i></a></li> -->
						<li class="faq"><a href="${FAQURL}" class="btn"><i class="lni-question-circle" title= "FAQ"></i></a></li>
						<%-- <li>
							<c:if test="${empty hideHeaderLinks}">
								<ycommerce:testId code="header_StoreFinder_link">
									<div class="nav-location hidden-xs">
										<c:url value="/store-finder" var="storeFinderUrl"/>
										<a href="${fn:escapeXml(storeFinderUrl)}" class="btn">
											<span class="lni-map-marker"></span>
										</a>
									</div>
								</ycommerce:testId>
							</c:if>
						</li>  --%>
						<li class="wdt-50">
<!-- Disable MiniCart and QuickLink for 1st Sprint  -->						
 							<cms:pageSlot position="MiniCart" var="cart" element="div" class="componentContainer">
								<cms:component component="${cart}" element="div"/>
							
							</cms:pageSlot> 
						</li>
					</ul>
				</div>
			</div>
		</div>
	</nav>	
	
	<a id="skiptonavigation"></a>
	<nav:topNavigation />
	
	<div class="marquee_title">
	<cms:pageSlot position="MarqueeBannerSlot" var="component">
		
			<spring:htmlEscape defaultHtmlEscape="true">${component.content}</spring:htmlEscape>
		
     </cms:pageSlot> 
	
	</div>
	
</header>

<cms:pageSlot position="NewFeatureBanner" var="component" element="div"	class="container-fluid">
	<cms:component component="${component}" />
</cms:pageSlot>

<cms:pageSlot position="BottomHeaderSlot" var="component" element="div"	class="container-fluid">
	<cms:component component="${component}" />
</cms:pageSlot>

		<cms:pageSlot position="Section3" var="component">
			<h4>
				<spring:htmlEscape defaultHtmlEscape="true">.....${component.content}</spring:htmlEscape>
			</h4>
		</cms:pageSlot>
	
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
		
<script>

$(window).scroll(function() {    
    var scroll = $(window).scrollTop();
	if (scroll >= 50) {
        $(".navigation--middle, .menu-header").addClass("top-head-fixed fadeInDown animated");		
	}
	
	else {
         $(".navigation--middle, .menu-header").removeClass("top-head-fixed fadeInDown animated");
    }
});

$(document).ready(function(){
	$(".nav-order-tools").hover(function(){
		$(".nav-order-tools__child-wrap").show();		
	},
	
	function(){
		$(".nav-order-tools__child-wrap").hide();
	}
	);
	
	/* $(".invoicevalueDetails li .invoice_value").click(function(){
		$(this).find(".lni-plus").toggleClass("lni-minus");
		$(this).next(".bill_details").slideToggle();	
	}); */
	
	
var logolink = $(".js-site-logo a").attr("href");
$("li.home a").attr("href", logolink);
$(".nav__links li a[title='Auction']").addClass("Auction");
$(".Auction").prepend("<i class='lni-construction-hammer'></i>");

$(".nav__links li a[title='Product Enquiry']").addClass("ProductEnquiry");
$(".ProductEnquiry").prepend("<i class='lni-question-circle'></i>");

	
 $(".nav__links li a[title='Place Order']").addClass("placeOrder");
 $(".placeOrder").prepend("<i class='lni-delivery'></i>");
 
 $(".search-icon").click(function(){
	 $(this).toggleClass("active");
	 $(this).parents(".mobile-middle-header").find(".site-search").toggleClass("active");
	 
 });
 
 $(".nav-icon").click(function(){
	 $(".navigation--bottom").toggleClass("open");
	 $("body").addClass("fixed");
	 
 });
 
 $(".navigation").on("click", ".close-nav", function(){
	 $(this).parents(".navigation--bottom").removeClass("open");
	 $("body").removeClass("fixed");
	 
 });
	
});
	
</script>		
