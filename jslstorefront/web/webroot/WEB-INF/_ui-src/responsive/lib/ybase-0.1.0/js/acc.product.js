ACC.product = {

    _autoload: [
        "bindToAddToCartForm",
        "enableStorePickupButton",
        "enableVariantSelectors",
        "bindFacets"
    ],


    bindFacets: function () {
        $(document).on("click", ".js-show-facets", function (e) {
        	
            e.preventDefault();
            var selectRefinementsTitle = $(this).data("selectRefinementsTitle");
            var colorBoxTitleHtml = ACC.common.encodeHtml(selectRefinementsTitle);
            ACC.colorbox.open(colorBoxTitleHtml, {
                href: ".js-product-facet",
                inline: true,
                width: "480px",
                onComplete: function () {
                    $(document).on("click", ".js-product-facet .js-facet-name", function (e) {
                        e.preventDefault();
                        $(".js-product-facet  .js-facet").removeClass("active");
                        $(this).parents(".js-facet").addClass("active");
                        $.colorbox.resize()
                    })
                },
                onClosed: function () {
                    $(document).off("click", ".js-product-facet .js-facet-name");
                }
            });
        });
        enquire.register("screen and (min-width:" + screenSmMax + ")", function () {
            $("#cboxClose").click();
        });
    },


    enableAddToCartButton: function () {
        $('.js-enable-btn').each(function () {
            if (!($(this).hasClass('outOfStock') || $(this).hasClass('out-of-stock'))) {
                $(this).prop("disabled", false);
            }
        });
    },

    enableVariantSelectors: function () {
        $('.variant-select').prop("disabled", false);
    },

    bindToAddToCartForm: function () {
        var addToCartForm = $('.add_to_cart_form');
        addToCartForm.ajaxForm({
        	beforeSubmit:ACC.product.showRequest,
        	success: ACC.product.displayAddToCartPopup
         });    
        setTimeout(function(){
        	$ajaxCallEvent  = true;
         }, 2000);
     },
     showRequest: function(arr, $form, options) {  
      $("#loading-wrapper").addClass("showloading");
    	 if($ajaxCallEvent)
    		{
    		 $ajaxCallEvent = false;
    		 return true;
    		}   	
    	 return false;
 
    },

    bindToAddToCartStorePickUpForm: function () {
    	 
        var addToCartStorePickUpForm = $('#colorbox #add_to_cart_storepickup_form');
        addToCartStorePickUpForm.ajaxForm({success: ACC.product.displayAddToCartPopup});
    },

    enableStorePickupButton: function () {
        $('.js-pickup-in-store-button').prop("disabled", false);
    },

    displayAddToCartPopup: function (cartResult, statusText, xhr, formElement) {
    	$ajaxCallEvent=true;
        $('#addToCartLayer').remove();
        
        if (typeof ACC.minicart.updateMiniCartDisplay == 'function') {
            ACC.minicart.updateMiniCartDisplay();
        }
        
        
        
        
        $("#updatedMessageCart").append(cartResult.addToCartLayer);
        
        $('#addToCartLayer').fadeIn(function(){
        	 $("#loading-wrapper").removeClass("showloading");
        	$('#updatedMessageCart').addClass("show");
        	
        	$(".loaderDiv").hide();
               $.colorbox.close();
               if (typeof timeoutId != 'undefined')
               {
                     clearTimeout(timeoutId);
                   
               }
             timeoutId = setTimeout(function ()
               {
                     $('#addToCartLayer').fadeOut(function(){
                        $('#addToCartLayer').remove();
                        $('#updatedMessageCart').removeClass("show");
                   
                            
                     });
               }, 2000);
               
        });
        
        
        
        
        var titleHeader = $('#addToCartTitle').html();

//        ACC.colorbox.open(titleHeader, {
//            html: cartResult.addToCartLayer,
//            width: "460px"
//        });

        var productCode = $('[name=productCodePost]', formElement).val();
        var quantityField = $('[name=qty]', formElement).val();

        var quantity = 1;
        if (quantityField != undefined) {
            quantity = quantityField;
        }

        var cartAnalyticsData = cartResult.cartAnalyticsData;

        var cartData = {
            "cartCode": cartAnalyticsData.cartCode,
            "productCode": productCode, "quantity": quantity,
            "productPrice": cartAnalyticsData.productPostPrice,
            "productName": cartAnalyticsData.productName
        };
        ACC.track.trackAddToCart(productCode, quantity, cartData);
    }
};

$(document).ready(function () {
	$ajaxCallEvent = true;
    ACC.product.enableAddToCartButton();
});

var supplyArryList = [];
$(".tab-facet .facet__list.js-facet-list .supply_lxt").each(function(){
supplyArryList.push($(this).text());
//alert(supplyArryListExist);
})	
var supplyArryListExist = Array.isArray(supplyArryList);
console.log(supplyArryList);




if ($(".b-filter-new__facet h4").text().length == 0) {
	sessionStorage.removeItem("supplyLocation");
	sessionStorage.setItem("addClass", "active");
}

$(".cartbutton").click(function(e){
	
	 if (sessionStorage.getItem("addClass") === 'active' && sessionStorage.getItem("supplyFlagValue") == 'true') {
	
	}
	 
	 else if (sessionStorage.getItem("addClass") === 'active' && sessionStorage.getItem("supplyFlagValue") == 'false') {
			$("#plant_warehouse_message").addClass("show");
			$("#plant_warehouse_message").html("Please select any one option from Supply Location");
			setTimeout(function() {
			$("#plant_warehouse_message").removeClass("show")}, 5000);
			e.preventDefault();
	
		}
	
		else if(supplyArryList.length == 0 && (sessionStorage.getItem("supplyFlagValue") == 'false')){
		$("#plant_warehouse_message").addClass("show");
		$("#plant_warehouse_message").html("Please select any one option from Supply Location");
		setTimeout(function() {
		$("#plant_warehouse_message").removeClass("show")}, 5000);
		e.preventDefault();	
	}
	
		else if(supplyArryList.length == 0 && (sessionStorage.getItem("supplyFlagValue") == 'true')){}
	
	
	

});


$(".facet__list.facet_list_text a").click(function(){
if(supplyArryListExist) {
window.sessionStorage.removeItem('addClass');
}
});