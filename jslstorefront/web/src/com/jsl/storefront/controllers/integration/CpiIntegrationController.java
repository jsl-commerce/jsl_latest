/**
 *
 */
package com.jsl.storefront.controllers.integration;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.SOAPException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jsl.core.dto.reports.customer.dashboard.CustomerDashboardDto;
import com.jsl.core.enums.Customer360Reports;
import com.jsl.facades.paymentdetailshistory.dto.PaymentDetailsDto;
import com.jsl.facades.reports.service.JslReportServiceFacade;
import com.jsl.facades.supportticket.dto.InvoiceWithMaterialDto;



/**
 * @author himanshu.sial
 *
 */
@Controller
@RequestMapping(value = "/reports")
public class CpiIntegrationController extends AbstractPageController
{

	private static final Logger LOG = Logger.getLogger(CpiIntegrationController.class);

	private static final String BREADCRUMBS_ATTR = "breadcrumbs";

	private static final String REPORT_ERROR_CMS_PAGE = "ErrorReport";
	private static final String TEXT_REPORT_ERROR_PAGE = "text.ErrorReport";


	private static final String REPORT_POP_YARD_CMS_PAGE = "PopYardReport";
	private static final String TEXT_REPORT_POP_YARD_PAGE = "text.PopYardReport";

	private static final String REPORT_POP_PLANT_CMS_PAGE = "PopPlantReport";
	private static final String TEXT_REPORT_POP_PLANT_PAGE = "text.PopPlantReport";

	private static final String REPORT_ORDER_BOOKING_CMS_PAGE = "OrderBookingReport";
	private static final String TEXT_REPORT_ORDER_BOOKING_PAGE = "text.OrderBookingReport";

	private static final String REPORT_SALES_BILLING_CMS_PAGE = "SalesOrderReport";
	private static final String TEXT_SALES_BILLING_PAGE = "text.SalesOrderReport";

	private static final String REPORT_ACCOUNT_STATEMENT_CMS_PAGE = "AccountStatementReport";
	private static final String TEXT_ACCOUNT_STATEMENT_PAGE = "text.AccountStatementReport";

	private static final String REPORT_SHIPMENT_DETAILS_CMS_PAGE = "GpsTrackingReport";
	private static final String TEXT_ACCOUNT_SHIPMENT_DETAILS_PAGE = "text.GpsDeatilsReport";


	@Resource(name = "accountBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

	@Resource(name = "jslReportServiceFacade")
	private JslReportServiceFacade jslReportServiceFacade;

	/*
	 * @RequestMapping(value = "/getPopReport", method = RequestMethod.GET) public void getPopReports() throws
	 * SOAPException { System.out.println("here pop report");
	 * System.out.println(cpiReportsService.getPopReportResponse()); System.out.println("end here"); }
	 *
	 * @RequestMapping(value = "/getSalesReport", method = RequestMethod.GET) public void getSalesReports() throws
	 * SOAPException { System.out.println("here sales report");
	 * System.out.println(cpiReportsService.getSalesReportResponse()); System.out.println("end here"); }
	 *
	 * @RequestMapping(value = "/getOrderBookingReport", method = RequestMethod.GET) public void getOrderBookingReports()
	 * throws SOAPException { System.out.println("here orderbooking");
	 * System.out.println(cpiReportsService.getOrderBookingReportResponse()); System.out.println("end here"); }
	 */
	@RequestMapping(value = "/PopYardReport", method = RequestMethod.GET)
	public String getPopYardReport(final Model model) throws CMSItemNotFoundException
	{

		try
		{
			//model.addAttribute("popReportList", jslReportServiceFacade.getPopYardReport());

			storeCmsPageInModel(model, getContentPageForLabelOrId(REPORT_POP_YARD_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(REPORT_POP_YARD_CMS_PAGE));
			model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_REPORT_POP_YARD_PAGE));
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

		}
		catch (final Exception e)
		{
			storeCmsPageInModel(model, getContentPageForLabelOrId(REPORT_ERROR_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(REPORT_ERROR_CMS_PAGE));
			model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_REPORT_ERROR_PAGE));
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
			LOG.error("Error in Pop Yard Report : " + e);
		}
		return getViewForPage(model);
	}

	@RequestMapping(value = "/PopPlantReport", method = RequestMethod.GET)
	public String getPopPlantReport(final Model model) throws CMSItemNotFoundException
	{
		try
		{
			//model.addAttribute("popReportList", jslReportServiceFacade.getPopPlantReport());
			storeCmsPageInModel(model, getContentPageForLabelOrId(REPORT_POP_PLANT_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(REPORT_POP_PLANT_CMS_PAGE));
			model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_REPORT_POP_PLANT_PAGE));
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

		}
		catch (final Exception e)
		{
			storeCmsPageInModel(model, getContentPageForLabelOrId(REPORT_ERROR_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(REPORT_ERROR_CMS_PAGE));
			model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_REPORT_ERROR_PAGE));
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
			LOG.error("Error in Pop Plant Report : " + e);
		}
		return getViewForPage(model);

	}

	@RequestMapping(value = "/OrderBookingReport", method = RequestMethod.GET)
	public String getOrderBookingReport(final Model model) throws CMSItemNotFoundException
	{

		try
		{
			//model.addAttribute("orderBookingReportList", jslReportServiceFacade.getOrderBookingReport());
			storeCmsPageInModel(model, getContentPageForLabelOrId(REPORT_ORDER_BOOKING_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(REPORT_ORDER_BOOKING_CMS_PAGE));
			model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_REPORT_ORDER_BOOKING_PAGE));
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		}

		catch (final Exception e)
		{
			storeCmsPageInModel(model, getContentPageForLabelOrId(REPORT_ERROR_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(REPORT_ERROR_CMS_PAGE));
			model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_REPORT_ERROR_PAGE));
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
			LOG.error("Error in Order Booking Report : " + e);
		}
		return getViewForPage(model);

	}

	@RequestMapping(value = "/SalesBillingReport", method = RequestMethod.GET)
	public String getSalesBillingReport(final Model model) throws CMSItemNotFoundException
	{

		try
		{
			//model.addAttribute("salesReportList", jslReportServiceFacade.getSalesReport());
			storeCmsPageInModel(model, getContentPageForLabelOrId(REPORT_SALES_BILLING_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(REPORT_SALES_BILLING_CMS_PAGE));
			model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_SALES_BILLING_PAGE));
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		}
		catch (final Exception e)
		{
			storeCmsPageInModel(model, getContentPageForLabelOrId(REPORT_ERROR_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(REPORT_ERROR_CMS_PAGE));
			model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_REPORT_ERROR_PAGE));
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
			LOG.error("Error in Sales Billing Report : " + e);
		}
		return getViewForPage(model);
	}

	@RequestMapping(value = "/AccountStatementReport", method = RequestMethod.GET)
	public String getAccountLeisureReport(final Model model) throws CMSItemNotFoundException
	{
		LOG.info("in method for Account Statement Report ");
		try
		{
			//model.addAttribute("accountStatementDto", jslReportServiceFacade.getAccountStatementReport());
			storeCmsPageInModel(model, getContentPageForLabelOrId(REPORT_ACCOUNT_STATEMENT_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(REPORT_ACCOUNT_STATEMENT_CMS_PAGE));
			model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_STATEMENT_PAGE));
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		}
		catch (final Exception e)
		{
			storeCmsPageInModel(model, getContentPageForLabelOrId(REPORT_ERROR_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(REPORT_ERROR_CMS_PAGE));
			model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_REPORT_ERROR_PAGE));
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
			LOG.error("Error in Account Statement Report : " + e);
		}
		return getViewForPage(model);
	}

	@RequestMapping(value = "/ShipmentTrackingReport", method = RequestMethod.GET)
	public String getShipmentTrackingReport(final Model model) throws CMSItemNotFoundException
	{
		LOG.info("in method for Shipment tracking Report ");
		try
		{
			//model.addAttribute("accountStatementDto", jslReportServiceFacade.getAccountStatementReport());
			storeCmsPageInModel(model, getContentPageForLabelOrId(REPORT_SHIPMENT_DETAILS_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(REPORT_SHIPMENT_DETAILS_CMS_PAGE));
			model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_SHIPMENT_DETAILS_PAGE));
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		}
		catch (final Exception e)
		{
			storeCmsPageInModel(model, getContentPageForLabelOrId(REPORT_ERROR_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(REPORT_ERROR_CMS_PAGE));
			model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_REPORT_ERROR_PAGE));
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
			LOG.error("Error in Shipment Details  Report : " + e);
		}
		return getViewForPage(model);
	}

	@RequestMapping(value = "/customerDashboard", method = RequestMethod.GET)
	public @ResponseBody CustomerDashboardDto getCustomerDashboardReport(final HttpServletRequest request,
			final HttpServletResponse response) throws IOException, SOAPException

	{
		return jslReportServiceFacade.getCustomerDashboard();
	}


	@RequestMapping(value = "/reportsJson", method = RequestMethod.GET)
	public @ResponseBody Object getReportsJson(@RequestParam(value = "report")
	final Customer360Reports report, final HttpServletRequest request, final HttpServletResponse response)
			throws IOException, SOAPException
	{

		return jslReportServiceFacade.getReportsAsJson(report);
	}



	@RequestMapping(value = "/exportReport", method = RequestMethod.GET, produces = "text/comma-separated-values;charset=UTF-8")
	public void exportSalesBillingReport(@RequestParam(value = "report")
	final Customer360Reports report, @RequestParam(required = false, value = "toDate")
	final String toDate, @RequestParam(required = false, value = "fromDate")
	final String fromDate, @RequestParam(required = false, value = "compny_data")
	final String compny_data, final HttpServletRequest request, final HttpServletResponse response) throws IOException

	{
		if ((StringUtils.isNotBlank(toDate)) || (StringUtils.isNotBlank(fromDate)))
		{
			jslReportServiceFacade.exportReport(report, toDate, fromDate, compny_data, response);
		}
		else
		{
			jslReportServiceFacade.exportReport(report, compny_data, response);
		}
	}

	@RequestMapping(value = "/fetchInvoiceDetails", method = RequestMethod.POST)
	public @ResponseBody List<InvoiceWithMaterialDto> getInvoiceDetailsByInvoiceNumber(@RequestBody
	final PaymentDetailsDto paymentDetails, final HttpServletRequest request, final HttpServletResponse response)
			throws SOAPException

	{
		return jslReportServiceFacade.getInvoiceDetailsByInvoiceNumber(paymentDetails.getSearchValue());
	}


}
