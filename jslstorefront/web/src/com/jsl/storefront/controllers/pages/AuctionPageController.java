/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.jsl.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.services.B2BCustomerService;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jsl.core.dto.AuctionEventData;
import com.jsl.core.dto.BundleTemplateData;
import com.jsl.core.dto.DefaultAuctionCommonDto;
import com.jsl.core.dto.DefaultBundleCommonDto;
import com.jsl.core.dto.JslCompletedAuctionDto;
import com.jsl.core.dto.JslOTPValidationDto;
import com.jsl.core.dto.WatchListData;
import com.jsl.core.enums.OTPGeneratedForEnum;
import com.jsl.core.enums.OTPUserIdentificationType;
import com.jsl.core.exceptions.JslAuctionException;
import com.jsl.core.model.AuctionEventModel;
import com.jsl.facades.auction.JslAuctionFacade;
import com.jsl.facades.csv.JslBidEventDataExportExcelFacade;
import com.jsl.facades.customer.JslCustomerFacade;
import com.jsl.facades.otp.JslUserOTPFacade;


/**
 * Auction Controller. Handles Auction flow.
 */
@Controller
@RequestMapping(value = "auction", method =
{ RequestMethod.GET, RequestMethod.POST })
public class AuctionPageController extends AbstractPageController
{
	private static final Logger LOG = Logger.getLogger(AuctionPageController.class);

	private static final String BREADCRUMBS_ATTR = "breadcrumbs";

	private static final String AUCTION_HOME_CMS_PAGE = "auctionHomePage";
	private static final String TEXT_AUCTION_HOME = "text.auctionHomePage";

	private static final String TEXT_LIVE_AUCTION = "text.liveAuctionPage";
	private static final String LIVE_AUCTION_CMS_PAGE = "liveAuctionPage";

	private static final String TEXT_WATCH_LIST = "text.watchListPage";
	private static final String WATCH_LIST_CMS_PAGE = "watchListPage";

	private static final String TEXT_COMPLETE_AUCTION = "text.completeAuctionPage";
	private static final String COMPLETE_AUCTION_CMS_PAGE = "completedAuctionPage";

	private static final String UPCOMING_AUCTION_CMS_PAGE = "upcomingAuctionPage";
	private static final String TEXT_UPCOMING_AUCTION = "text.upcomingAuctionPage";

	private static final String USER_MANUAL_CMS_PAGE = "userManualPage";
	private static final String TEXT_USER_MANUAL = "text.userManualPage";


	private static final String NON_AUCTION_CMS_PAGE = "nonAuctionPage";
	private static final String NON_TEXT_USER_MANUAL = "text.nonActionPage";

	@Resource(name = "jslAuctionFacade")
	private JslAuctionFacade auctionFacade;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "b2bCustomerService")
	private B2BCustomerService b2bCustomerService;

	@Resource(name = "jslUserOTPFacade")
	private JslUserOTPFacade userOtpFacade;

	@Resource(name = "jslBidEventDataExportExcelFacade")
	private JslBidEventDataExportExcelFacade JslBidEventDataExportExcelFacade;

	@Resource(name = "accountBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

	@Resource(name = "customerFacade")
	private JslCustomerFacade jslCustomerFacade;

	private HttpSessionRequestCache httpSessionRequestCache;

	@Resource(name = "httpSessionRequestCache")
	public void setHttpSessionRequestCache(final HttpSessionRequestCache accHttpSessionRequestCache)
	{
		this.httpSessionRequestCache = accHttpSessionRequestCache;
	}

	@RequestMapping(method = RequestMethod.POST)
	public String doShowAuctionPage(@RequestParam(value = "error", defaultValue = "false")
	final boolean auctionError, final Model model, final HttpServletRequest request, final HttpServletResponse response,
			final HttpSession session) throws CMSItemNotFoundException
	{
		final String identityValue = getUserService().getCurrentUser().getUid();
		final Map<String, Boolean> otpAcceptedForAuctionRecord = new HashMap<>();
		storeCmsPageInModel(model, getContentPageForLabelOrId(AUCTION_HOME_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(AUCTION_HOME_CMS_PAGE));

		final List<AuctionEventData> auctionEventList = getAuctionFacade().getAllAuctionList();
		final boolean isAdminUser = getAuctionFacade()
				.isAdminObserverUser(((B2BCustomerModel) getUserService().getCurrentUser()).getOriginalUid());

		final List<AuctionEventData> otpAcceptedAuctionList = getAuctionFacade().checkOtpStatus(identityValue, auctionEventList);
		if (!isAdminUser)
		{
			auctionEventList.removeAll(otpAcceptedAuctionList);
		}
		model.addAttribute("currentDateTime", System.currentTimeMillis());
		model.addAttribute("auctionEventList", auctionEventList);
		model.addAttribute("otpAcceptedAuctionList", otpAcceptedAuctionList);
		model.addAttribute("isAdminUser", isAdminUser);
		model.addAttribute("error", auctionError);
		model.addAttribute("userPK", getUserService().getCurrentUser().getPk());
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_AUCTION_HOME));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

		return getViewForPage(model);
	}

	@RequestMapping(value = "/liveAuction", method = RequestMethod.GET)
	public String getOngoingAuctionDetails(final Model model, final HttpServletRequest request, final HttpServletResponse response,
			final HttpSession session) throws CMSItemNotFoundException
	{
		long additionalTime = 0;
		storeCmsPageInModel(model, getContentPageForLabelOrId(LIVE_AUCTION_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(LIVE_AUCTION_CMS_PAGE));
		final String auctionID = request.getParameter("auctionID");
		final List<BundleTemplateData> bundleTemplateList = getAuctionFacade().getOngoingAuctionDetails(auctionID);
		final boolean isAdminUser = getAuctionFacade()
				.isAdminObserverUser(((B2BCustomerModel) getUserService().getCurrentUser()).getOriginalUid());
		if (!CollectionUtils.isEmpty(bundleTemplateList)
				&& bundleTemplateList.get(0).getAuctionEventData().getAdditionalTime() != null)
		{
			additionalTime = bundleTemplateList.get(0).getAuctionEventData().getAdditionalTime();
		}
		model.addAttribute("bundleTemplateList", bundleTemplateList);
		model.addAttribute("currentDateTime", System.currentTimeMillis());
		model.addAttribute("auctionID", auctionID);
		model.addAttribute("isAdminUser", isAdminUser);
		model.addAttribute("additionalTime", additionalTime);
		model.addAttribute("userPK", getUserService().getCurrentUser().getPk());
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_LIVE_AUCTION));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

		return getViewForPage(model);
	}

	@RequestMapping(value = "/completedAuction", method = RequestMethod.POST)
	public String completedAuction(final Model model, @ModelAttribute("auctionDateRange")
	final DefaultBundleCommonDto auctionDateRange, final HttpServletRequest request, final HttpServletResponse response,
			final HttpSession session) throws CMSItemNotFoundException
	{
		Map<String, Object> responseObject = new HashMap<>();
		final Map<String, Object> requestObject = new HashMap<>();

		storeCmsPageInModel(model, getContentPageForLabelOrId(COMPLETE_AUCTION_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(COMPLETE_AUCTION_CMS_PAGE));

		final boolean isAdminUser = getAuctionFacade()
				.isAdminObserverUser(((B2BCustomerModel) getUserService().getCurrentUser()).getOriginalUid());
		requestObject.put("auctionDateRange", auctionDateRange);
		requestObject.put("isAdminUser", isAdminUser);

		responseObject = getAuctionFacade().getCompletedAuctionDetails(requestObject, responseObject);
		final List<JslCompletedAuctionDto> completedBundleList = (List<JslCompletedAuctionDto>) responseObject
				.get("completedAuctionList");
		final boolean dateRange = (Boolean) responseObject.get("dateRange");

		model.addAttribute("completedBundleList", completedBundleList);
		model.addAttribute("isAdminUser", isAdminUser);
		model.addAttribute("dateRange", dateRange);
		model.addAttribute("auctionStartDate", auctionDateRange.getAuctionStartDate());
		model.addAttribute("auctionEndDate", auctionDateRange.getAuctionEndDate());
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_COMPLETE_AUCTION));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

		return getViewForPage(model);
	}

	@RequestMapping(value = "/futureAuction", method = RequestMethod.GET)
	public String getUpcomingAuctionEvents(final Model model, final HttpServletRequest request, final HttpServletResponse response,
			final HttpSession session) throws CMSItemNotFoundException
	{
		final String identityValue = getUserService().getCurrentUser().getUid();
		final boolean isAdminUser = getAuctionFacade()
				.isAdminObserverUser(((B2BCustomerModel) getUserService().getCurrentUser()).getOriginalUid());
		final Map<String, Boolean> otpAcceptedForAuctionRecord = new HashMap<>();
		final Map<String, List<BundleTemplateData>> bundleMap = new HashMap<>();
		List<BundleTemplateData> bundleTemplateList = null;
		storeCmsPageInModel(model, getContentPageForLabelOrId(UPCOMING_AUCTION_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(UPCOMING_AUCTION_CMS_PAGE));

		final List<AuctionEventData> auctionEventList = getAuctionFacade().getUpcomingAuctionDetails();
		for (final AuctionEventData auctionEvent : auctionEventList)
		{
			bundleTemplateList = getAuctionFacade().getBundleListByAuctionId(String.valueOf(auctionEvent.getAuctionId()));
			bundleMap.put(String.valueOf(auctionEvent.getAuctionId()), bundleTemplateList);
		}
		final List<AuctionEventData> otpAcceptedAuctionList = getAuctionFacade().checkOtpStatus(identityValue, auctionEventList);
		if (!isAdminUser)
		{
			auctionEventList.removeAll(otpAcceptedAuctionList);
		}
		model.addAttribute("BundleMap", bundleMap);
		model.addAttribute("auctionEventList", auctionEventList);
		model.addAttribute("isAdminUser", isAdminUser);
		model.addAttribute("otpAcceptedAuctionList", otpAcceptedAuctionList);
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_UPCOMING_AUCTION));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

		return getViewForPage(model);
	}

	@RequestMapping(value = "/watchList", method = RequestMethod.GET)
	public String watchList(@RequestParam(value = "removed", defaultValue = "false")
	final boolean watchListError, final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		long additionalTime = 0;
		final String auctionID = request.getParameter("auctionID");
		storeCmsPageInModel(model, getContentPageForLabelOrId(WATCH_LIST_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(WATCH_LIST_CMS_PAGE));

		final List<WatchListData> watchListDetailsList = getAuctionFacade().getWatchListDetails(auctionID);
		if (!CollectionUtils.isEmpty(watchListDetailsList)
				&& watchListDetailsList.get(0).getAuctionEventData().getAdditionalTime() != null)
		{
			additionalTime = watchListDetailsList.get(0).getAuctionEventData().getAdditionalTime();
		}
		model.addAttribute("watchListDetailsList", watchListDetailsList);
		model.addAttribute("currentDateTime", System.currentTimeMillis());
		model.addAttribute("auctionID", auctionID);
		model.addAttribute("removed", watchListError);
		model.addAttribute("additionalTime", additionalTime);
		model.addAttribute("userPK", getUserService().getCurrentUser().getPk());
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_WATCH_LIST));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

		return getViewForPage(model);
	}

	@RequestMapping(value = "/addToWatchList", method = RequestMethod.POST)
	public @ResponseBody int addToWatchList(@RequestBody
	final List<DefaultBundleCommonDto> addToWatchList, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		try
		{
			return getAuctionFacade().addToWatchList(addToWatchList);
		}
		catch (final JslAuctionException exception)
		{
			return Integer.valueOf(exception.getCode());
		}
	}

	@RequestMapping(value = "/removeWatchList", method = RequestMethod.POST)
	public @ResponseBody boolean removeWatchList(@RequestBody
	final List<DefaultBundleCommonDto> toRemovefromWatchList, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		return getAuctionFacade().removeFromWatchList(toRemovefromWatchList);
	}

	@RequestMapping(value = "/userManual", method = RequestMethod.GET)
	public String userManual(final Model model, final HttpServletRequest request, final HttpServletResponse response,
			final HttpSession session) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(USER_MANUAL_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(USER_MANUAL_CMS_PAGE));

		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_USER_MANUAL));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

		return getViewForPage(model);
	}

	@RequestMapping(value = "/placeBid", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> placeBid(@RequestBody
	final DefaultAuctionCommonDto bidEvent, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		Map<String, Object> placeBidResponse = new HashMap<>();
		if (bidEvent.isLast30Sec())
		{
			final AuctionEventModel auctionEvent = getAuctionFacade().getAuctionEventForAuctionId(bidEvent.getAuctionEventId());
			if (null != auctionEvent && checkAuctionAdditionalTime(auctionEvent))
			{
				final long additionalTime = getAuctionFacade().updateAuctionEventWithAdditionalTime(auctionEvent);
				placeBidResponse.put("additionalTime", additionalTime);
			}
		}
		if (!CollectionUtils.isEmpty(bidEvent.getDefaultBundleCommonDto()))
		{
			placeBidResponse = getAuctionFacade().placeBid(bidEvent, placeBidResponse);
		}

		return placeBidResponse;
	}

	@RequestMapping(value = "/setProxy", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> setProxy(@RequestBody
	final DefaultAuctionCommonDto proxyBidPriceList, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		Map<String, Object> setProxyResponse = new HashMap<>();
		final boolean isProxySet = false;
		if (proxyBidPriceList.isLast30Sec())
		{
			final AuctionEventModel auctionEvent = getAuctionFacade()
					.getAuctionEventForAuctionId(proxyBidPriceList.getAuctionEventId());
			if (null != auctionEvent && checkAuctionAdditionalTime(auctionEvent))
			{
				final long additionalTime = getAuctionFacade().updateAuctionEventWithAdditionalTime(auctionEvent);
				setProxyResponse.put("additionalTime", additionalTime);
			}
		}
		if (!CollectionUtils.isEmpty(proxyBidPriceList.getDefaultBundleCommonDto()))
		{
			setProxyResponse = getAuctionFacade().setProxy(proxyBidPriceList.getDefaultBundleCommonDto(), setProxyResponse);
		}

		return setProxyResponse;
	}

	@RequestMapping(value = "/restorePageWithData", method = RequestMethod.POST)
	public @ResponseBody String restorePageWithData(@RequestBody
	final String auctionId, final HttpServletRequest request, final HttpServletResponse response) throws CMSItemNotFoundException
	{
		final ObjectMapper mapper = new ObjectMapper();
		final Map<String, String> pageDataMap = new HashMap<>();
		String maxBidForCurrentUser = null;
		String pageData = null;
		String proxyBid = null;
		try
		{
			final List<DefaultBundleCommonDto> bidEventList = getAuctionFacade().getMaxBidForCurrentUser(auctionId);
			maxBidForCurrentUser = mapper.writeValueAsString(bidEventList);
			pageDataMap.put("maxBidForCurrentUser", maxBidForCurrentUser);
			pageData = mapper.writeValueAsString(pageDataMap);
		}
		catch (final Exception ex)
		{
			LOG.error("Excepion in restorePageWithData() for getMaxBidForCurrentUser()", ex);
		}
		try
		{
			final List<DefaultBundleCommonDto> proxyBidList = getAuctionFacade().getProxyBidForCurrentUser(auctionId);
			proxyBid = mapper.writeValueAsString(proxyBidList);
			pageDataMap.put("proxyBid", proxyBid);
			pageData = mapper.writeValueAsString(pageDataMap);
		}
		catch (final Exception ex)
		{
			LOG.error("Excepion in restorePageWithData() for getProxyBidForCurrentUser()", ex);
		}

		return pageData;
	}

	@RequestMapping(value = "/validateOTP", method = RequestMethod.POST)
	public @ResponseBody String validateOTPForCustomer(final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final HttpSession session) throws CMSItemNotFoundException
	{
		final ObjectMapper mapper = new ObjectMapper();
		String responseJson = "";
		final String otp = request.getParameter("otp");
		final String auctionId = request.getParameter("auctionId");
		final String identityValue = getUserService().getCurrentUser().getUid();
		final long enteredOTPTime = System.currentTimeMillis();
		final JslOTPValidationDto jslOTPValidationDto = new JslOTPValidationDto();
		jslOTPValidationDto.setAuctionId(auctionId);
		try
		{
			final Map<String, String> validationResponse = userOtpFacade.validateOTP(otp, OTPUserIdentificationType.EMAIL_ID,
					identityValue, OTPGeneratedForEnum.AUCTION, enteredOTPTime, jslOTPValidationDto);
			responseJson = mapper.writeValueAsString(validationResponse);

		}
		catch (final JsonProcessingException ex)
		{
			LOG.error("Excepion in validateOTPForCustomer() during map to string", ex);
		}

		return responseJson;
	}

	@RequestMapping(value = "/getH1Price", method = RequestMethod.POST)
	public @ResponseBody String getH1Price(@RequestBody
	final String auctionId, final HttpServletRequest request, final HttpServletResponse response)
	{
		String json = null;
		final ObjectMapper mapper = new ObjectMapper();
		if (StringUtils.isEmpty(auctionId))
		{
			return json;
		}
		try
		{
			final DefaultAuctionCommonDto currentMaxBid = getAllMaxBidPrice(auctionId);
			json = mapper.writeValueAsString(currentMaxBid);
			LOG.debug("Message from getH1Price() with list of Max Bid Price : " + json);
		}
		catch (final Exception ex)
		{
			LOG.error("Excepion in getH1Price() ajax", ex);
		}

		return json;
	}


	@RequestMapping(value = "/addAdditionalTime", method = RequestMethod.POST)
	public @ResponseBody long updateAuctionEventWithAdditionalTime(@RequestBody
	final String auctionId, final HttpServletRequest request, final HttpServletResponse response)
	{
		final AuctionEventModel auctionEvent = getAuctionFacade().getAuctionEventForAuctionId(auctionId);
		return getAuctionFacade().updateAuctionEventWithAdditionalTime(auctionEvent);
	}

	@RequestMapping(value = "/getLotBidDetails", method = RequestMethod.POST)
	public @ResponseBody String getLotBidDetails(@RequestBody
	final DefaultBundleCommonDto lotDetails, final HttpServletRequest request, final HttpServletResponse response)
	{
		final ObjectMapper mapper = new ObjectMapper();
		String responseJson = null;
		try
		{
			final Map<String, Object> lotDetailsMap = getAuctionFacade().getAllBidHistoryForLot(lotDetails);
			responseJson = mapper.writeValueAsString(lotDetailsMap);
		}
		catch (final Exception e)
		{
			LOG.error("Excepion in getLotBidDetails() ajax", e);
		}

		return responseJson;
	}

	@RequestMapping(value = "/getAuctionEventData", method = RequestMethod.POST)
	public @ResponseBody String getAuctionEventData(@RequestBody
	final String auctionId, final HttpServletRequest request, final HttpServletResponse response)
	{
		final ObjectMapper mapper = new ObjectMapper();
		String auctionEventJson = null;
		try
		{
			final AuctionEventData auctionEvent = getAuctionFacade().getAuctionEventDataForAuctionId(auctionId);
			auctionEventJson = mapper.writeValueAsString(auctionEvent);
		}
		catch (final Exception e)
		{
			LOG.error("Excepion in getAuctionEventData() ", e);
		}
		return auctionEventJson;
	}

	@RequestMapping(value = "/getLiveCustomers", method = RequestMethod.GET)
	@RequireHardLogIn
	public @ResponseBody List<CustomerData> getLiveCustomers(final HttpServletRequest request, final HttpServletResponse response)
	{
		return jslCustomerFacade.getAuctionLiveCustomers();
	}

	private boolean checkAuctionAdditionalTime(final AuctionEventModel auctionEvent)
	{
		boolean isValid = false;
		final long currentTimeinMillis = System.currentTimeMillis();
		final Long lastModifiedTimeInMillis = auctionEvent.getUpdatedOn();
		if (null != lastModifiedTimeInMillis)
		{
			final long timeDiff = currentTimeinMillis - lastModifiedTimeInMillis.longValue();
			if (timeDiff >= 2000)
			{
				isValid = true;
			}
		}
		else
		{
			isValid = true;
		}

		return isValid;
	}


	@RequestMapping(value = "/nAction", method = RequestMethod.GET)
	public String nonAuctionUser(final Model model) throws CMSItemNotFoundException
	{

		storeCmsPageInModel(model, getContentPageForLabelOrId(NON_AUCTION_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(NON_AUCTION_CMS_PAGE));
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(NON_TEXT_USER_MANUAL));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

		return getViewForPage(model);
	}


	private DefaultAuctionCommonDto getAllMaxBidPrice(final String auctionEventId)
	{
		return getAuctionFacade().getAllMaxBidPrice(auctionEventId);
	}

	public JslAuctionFacade getAuctionFacade()
	{
		return auctionFacade;
	}

	public void setAuctionFacade(final JslAuctionFacade auctionFacade)
	{
		this.auctionFacade = auctionFacade;
	}

	public UserService getUserService()
	{
		return userService;
	}

	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	public B2BCustomerService getB2bCustomerService()
	{
		return b2bCustomerService;
	}

	public void setB2bCustomerService(final B2BCustomerService b2bCustomerService)
	{
		this.b2bCustomerService = b2bCustomerService;
	}

	public JslBidEventDataExportExcelFacade getJslBidEventDataExportExcelFacade()
	{
		return JslBidEventDataExportExcelFacade;
	}

	public void setJslBidEventDataExportExcelFacade(final JslBidEventDataExportExcelFacade jslBidEventDataExportExcelFacade)
	{
		JslBidEventDataExportExcelFacade = jslBidEventDataExportExcelFacade;
	}

	@RequestMapping(value = "/isAdminDashBoardUser", method = RequestMethod.GET)
	public @ResponseBody boolean isAdminDashBoardUser(final HttpServletRequest request, final HttpServletResponse response)
	{
		return getAuctionFacade().isAdminObserverUser(((B2BCustomerModel) getUserService().getCurrentUser()).getOriginalUid());
	}


}
