/**
 *
 */
package com.jsl.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.jsl.facades.customersupport.CustomerSupportFacade;
import com.jsl.facades.reports.service.JslExcelExportService;
import com.jsl.facades.supportticket.dto.CustomerComplaintDto;
import com.jsl.facades.supportticket.dto.JslCustomerFeedbackDto;



/**
 * @author himanshu.sial
 *
 */
@Controller
public class SupportTicketPageController extends AbstractPageController
{
	private static final Logger LOG = Logger.getLogger(SupportTicketPageController.class);

	private static final String BREADCRUMBS_ATTR = "breadcrumbs";

	private static final String SUPPORT_TICKET_CMS_PAGE = "JslSupportTicket";
	private static final String TEXT_SUPPORT_TICKET_PAGE = "text.ErrorReport";

	private static final String SUPPORT_TICKET_FORM = "supportTicketForm";

	private static final String SUPPORT_TICKET_HISTORY_CMS_PAGE = "jslSupportTicketHistory";
	private static final String TEXT_SUPPORT_TICKET_HISTORY_PAGE = "text.ErrorReport";

	private static final String ADMIN_SUPPORT_TICKET_HISTORY_CMS_PAGE = "jslSupportTicketHistory";
	private static final String ADMIN_TEXT_SUPPORT_TICKET_HISTORY_PAGE = "text.ErrorReport";

	@Resource(name = "accountBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

	@Autowired
	private CustomerSupportFacade csFacade;

	@Autowired
	JslExcelExportService jslExcelExportService;

	@RequestMapping(value = "/my-account/complaint", method = RequestMethod.GET)
	public String getComplaintPage(final Model model) throws CMSItemNotFoundException
	{

		storeCmsPageInModel(model, getContentPageForLabelOrId(SUPPORT_TICKET_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SUPPORT_TICKET_CMS_PAGE));
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_SUPPORT_TICKET_PAGE));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

		return getViewForPage(model);
	}

	@PostMapping(value = "/my-account/register-complaint", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public @ResponseBody Map<String, String> submitComplaint(
			@RequestPart(value = "file", required = false) final MultipartFile[] file,
			@RequestPart(value = "customerComplaintDto", required = false) final CustomerComplaintDto requestDto,
			final HttpServletRequest request) throws Exception
	{
		requestDto.setAttachment(Arrays.asList(file));
		return getCsFacade().submitComplaint(requestDto);
	}

	@RequestMapping(value = "/my-account/support-ticket-history", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getSupportTicketHistory(final Model model) throws CMSItemNotFoundException
	{

		final Map<String, List<CustomerComplaintDto>> response = csFacade.getSupportTicketHistory();
		LOG.info("ServiceComplaintList: " + response.get("ServiceComplaintList"));
		LOG.info("MaterialComplaintList: " + response.get("MaterialComplaintList"));

		model.addAttribute("ServiceComplaintList", response.get("ServiceComplaintList"));
		model.addAttribute("MaterialComplaintList", response.get("MaterialComplaintList"));

		storeCmsPageInModel(model, getContentPageForLabelOrId(SUPPORT_TICKET_HISTORY_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SUPPORT_TICKET_HISTORY_CMS_PAGE));
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_SUPPORT_TICKET_HISTORY_PAGE));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

		return getViewForPage(model);
	}

	@GetMapping(value = "/observer/complaints")
	public @ResponseBody Map getSupportTicketList(final Model model) throws CMSItemNotFoundException
	{
		final Map<String, Object> map = new HashMap<>();
		final List<CustomerComplaintDto> complaintList = getCsFacade().getAllComplaintListForAdmin();
		LOG.info("Admin supports ticket list: " + complaintList);
		map.put("complaintList", complaintList);

		final boolean isSupportTicketAdminUser = getCsFacade().isSupportTicketAdminUser();
		map.put("isSupportTicketAdminUser", isSupportTicketAdminUser);
		return map;
	}

	@RequestMapping(value = "/isSupportTicketAdminUser", method = RequestMethod.GET)
	public @ResponseBody boolean isAdminDashBoardUser(final HttpServletRequest request, final HttpServletResponse response)
	{
		return getCsFacade().isSupportTicketAdminUser();
	}

	@RequestMapping(value = "/my-account/submitFeedBack", method = RequestMethod.POST)
	public @ResponseBody Boolean submitCustomerFeedBack(@RequestBody final CustomerComplaintDto complaintDto,
			final HttpServletRequest request)
	{
		return getCsFacade().submitCustomerFeedBack(complaintDto);
	}

	public CustomerSupportFacade getCsFacade()
	{
		return csFacade;
	}

	@RequestMapping(value = "/my-account/exportInvoiceReport", method = RequestMethod.GET)
	public void exportInvoiceData(final HttpServletRequest request, final HttpServletResponse response) throws Exception
	{
		jslExcelExportService.exportInvoices(response);
	}

	@RequestMapping(value = "/my-account/exportCustomerComplaintreport", method = RequestMethod.GET, produces = "application/octet-stream;charset=UTF-8")
	public void exportCustomerComplaintreport(final HttpServletRequest request, final HttpServletResponse response,
			final Model model) throws Exception
	{
		FileInputStream fileInputStream = null;
		OutputStream outputStream = null;
		File file = null;
		final String csvFileName = "CustomerComplaint.xls";
		final String headerKey = "Content-Disposition";
		final String headerValue = String.format("attachment; filename=\"%s\"", csvFileName);
		try
		{
			response.setHeader(headerKey, headerValue);
			response.setContentType("application/octet-stream;charset=UTF-8");
			final URL resource = SupportTicketPageController.class.getResource("/storeFrontWorkBook.xls");
			file = Paths.get(resource.toURI()).toFile();
			fileInputStream = new FileInputStream(file);
			if (jslExcelExportService.exportCustomerComplaintreport(response, file))
			{
				response.setHeader("charset", "iso-8859-1");
				response.setContentLength((int) file.length());
				response.setStatus(HttpServletResponse.SC_OK);
				final byte[] data = new byte[(int) file.length()];
				fileInputStream.read(data);
				outputStream = response.getOutputStream();
				outputStream.write(data, 0, (int) file.length());
				outputStream.flush();
				response.flushBuffer();
			}
		}
		catch (final Exception e)
		{
			LOG.info("Exception in JslExcelExportService.exportCustomerComplaintreport : {}", e);
		}

		finally
		{
			fileInputStream.close();

			if (outputStream != null)
			{
				outputStream.close();
			}

		}
	}


	@RequestMapping(value = "/my-account/submitFeedBackForm", method = RequestMethod.POST)
	public @ResponseBody Boolean submitCustomerFeedBackForm(@RequestBody final JslCustomerFeedbackDto jslCustomerFeedbackDto,
			final HttpServletRequest request) throws Exception
	{
		return getCsFacade().submitCustomerFeedBackForm(jslCustomerFeedbackDto);
	}

	@RequestMapping(value = "/my-account/submitCustomerSatisfaction", method = RequestMethod.POST)
	public @ResponseBody Boolean submitCustomerSatisfaction(@RequestBody final CustomerComplaintDto complaintDto,
			final HttpServletRequest request) throws Exception
	{
		//return getCsFacade().submitCustomerFeedBackForm(jslCustomerFeedbackDto);
		return getCsFacade().submitCustomerSatisfaction(complaintDto);
	}


	@RequestMapping(value = "/support/getFeedbackData", method = RequestMethod.GET)
	public @ResponseBody List<JslCustomerFeedbackDto> getCustomerFeedBackForm() throws Exception
	{
		return getCsFacade().getAllCustomerFeedBackFormData();
	}

	@RequestMapping(value = "/my-account/customerFeedbackAnalysis", method = RequestMethod.GET)
	public void customerFeedbackAnalysisExport(final HttpServletRequest request, final HttpServletResponse response)
			throws Exception
	{
		jslExcelExportService.customerFeedbackAnalysis(response);
	}

	@RequestMapping(value = "/my-account/isBatchAlreadyAdded", method = RequestMethod.GET)
	public @ResponseBody CustomerComplaintDto isBatchAlreadyAdded(final HttpServletRequest request,
			final HttpServletResponse response, @RequestParam final String invoiceNumber, @RequestParam final String batchNumber)
			throws Exception
	{
		return getCsFacade().isBatchAlreadyAdded(invoiceNumber, batchNumber);
	}

}
