/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.jsl.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jsl.core.dto.ProductEnquiry;
import com.jsl.facades.product.enquiry.JslProductEnquiryFacades;


/**
 * Controller for home page
 */
@Controller
@RequestMapping(value = "/enquiry", method =
{ RequestMethod.GET, RequestMethod.POST })
public class ProductEnquiryPageController extends AbstractPageController
{

	// CMS Pages

	private static final String BREADCRUMBS_ATTR = "breadcrumbs";

	private static final String TEXT_PRODUCT_ENQUIRY = "text.productEnquiryPage";
	private static final String PRODUCT_ENQUIRY_CMS_PAGE = "productEnquiryPage";

	@Resource(name = "jslProductEnquiryFacades")
	private JslProductEnquiryFacades jslProductEnquiryFacades;

	private static final Logger LOG = Logger.getLogger(ProductEnquiryPageController.class);

	@Resource(name = "accountBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "customerFacade")
	private CustomerFacade customerFacade;

	@RequestMapping(value = "/product-enquiry", method = RequestMethod.POST)
	public String ProductEnquiryForm(final Model model, final HttpServletRequest request, final HttpServletResponse response,
			final HttpSession session) throws CMSItemNotFoundException
	{

		model.addAttribute("productList", jslProductEnquiryFacades.getProducts());
		storeCmsPageInModel(model, getContentPageForLabelOrId(PRODUCT_ENQUIRY_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(PRODUCT_ENQUIRY_CMS_PAGE));

		LOG.info("....successfull....");

		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_PRODUCT_ENQUIRY));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

		return getViewForPage(model);
	}

	@RequestMapping(value = "/getProductsJson", method = RequestMethod.GET)
	public @ResponseBody String getProductsJson(final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		return jslProductEnquiryFacades.getProducts();
	}

	@RequestMapping(value = "/sendProductsList", method = RequestMethod.POST)
	public @ResponseBody Map<String, String> sendProductsList(@RequestBody
	final ProductEnquiry data, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		LOG.info("list Saved");
		return jslProductEnquiryFacades.productEnquiryCreate(data);
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

}
