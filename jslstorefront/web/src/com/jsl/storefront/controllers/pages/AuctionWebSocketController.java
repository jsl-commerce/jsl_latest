package com.jsl.storefront.controllers.pages;

import javax.annotation.Resource;
import javax.websocket.server.ServerEndpoint;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.server.standard.TomcatRequestUpgradeStrategy;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jsl.core.dto.DefaultAuctionCommonDto;
import com.jsl.facades.auction.JslAuctionFacade;


@Configuration
@EnableWebSocket
@EnableWebSocketMessageBroker
@Controller
@ServerEndpoint("/getCurrentH1Price")
public class AuctionWebSocketController extends AbstractWebSocketMessageBrokerConfigurer
{
	private static final Logger LOG = Logger.getLogger(AuctionWebSocketController.class);

	@Resource(name = "jslAuctionFacade")
	private JslAuctionFacade auctionFacade;

	@Override
	public void configureMessageBroker(final MessageBrokerRegistry config)
	{
		System.out.println("in configureMessageBroker ");
		config.enableSimpleBroker("/topic");
		config.setApplicationDestinationPrefixes("/liveAuctionApp");
	}

	@Override
	public void registerStompEndpoints(final StompEndpointRegistry endpointRegistry)
	{
		System.out.println("in registerStompEndpoints ");
		endpointRegistry.addEndpoint("/getCurrentH1Price").setAllowedOrigins("*");
		endpointRegistry.addEndpoint("/getCurrentH1Price")
				.setHandshakeHandler(new DefaultHandshakeHandler(new TomcatRequestUpgradeStrategy()))
				.withSockJS();
	}

	@MessageMapping("/getCurrentH1Price")
	@SendTo("/topic/showH1Price")
	public @ResponseBody String getH1Price(final DefaultAuctionCommonDto auctionEvent)
	{
		String json = null;
		final ObjectMapper mapper = new ObjectMapper();
		if (StringUtils.isEmpty(auctionEvent.getAuctionEventId()))
		{
			return json;
		}
		try
		{
			final DefaultAuctionCommonDto currentMaxBid = getAllMaxBidPrice(auctionEvent.getAuctionEventId());
			json = mapper.writeValueAsString(currentMaxBid);
			LOG.debug("Message from getH1Price() with list of Max Bid Price : " + json);
		}
		catch (final Exception ex)
		{
			LOG.error("Excepion in getH1Price() ajax", ex);
		}

		return json;
	}

	private DefaultAuctionCommonDto getAllMaxBidPrice(final String auctionEventId)
	{
		return getAuctionFacade().getAllMaxBidPrice(auctionEventId);
	}

	public JslAuctionFacade getAuctionFacade()
	{
		return auctionFacade;
	}

	public void setAuctionFacade(final JslAuctionFacade auctionFacade)
	{
		this.auctionFacade = auctionFacade;
	}

}
