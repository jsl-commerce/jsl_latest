/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.jsl.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.jsl.configurablebundleservice.service.impl.JslBundleTemplateExportExcelServiceImpl;
import com.jsl.core.dto.BidEventDto;
import com.jsl.core.dto.BundleTemplateDto;
import com.jsl.core.dto.DefaultBundleCommonDto;
import com.jsl.core.pdf.generator.service.JslFreightPdf;
import com.jsl.facades.auction.JslAuctionFacade;
import com.jsl.facades.csv.JslBidEventDataExportExcelFacade;
import com.jsl.facades.csv.impl.JslBundleTemplateExportExcelFacadeImpl;


/**
 * Controller for home page
 */
@Controller
@RequestMapping("/custom")
public class CustomPageController extends AbstractSearchPageController
{
	private static final Logger LOG = Logger.getLogger(CustomPageController.class);

	private static final String BREADCRUMBS_ATTR = "breadcrumbs";


	private static final String PLACE_ORDER_CMS_PAGE = "placeOrder";
	private static final String TEXT_PLACE_ORDER = "text.placeOrder";

	private static final String PRODUCT_ENQUIRY_CMS_PAGE = "productEnquiry";
	private static final String TEXT_PRODUCT_ENQUIRY = "text.productEnquiry";

	private static final String SUPPORT_TICKETS_CMS_PAGE = "supportTickets";
	private static final String TEXT_SUPPORT_TICKETS = "text.supportTickets";

	private static final String ORDER_HISTORY_CMS_PAGE = "orderHistory";
	private static final String TEXT_ORDER_HISTORY = "text.orderHistory";

	private static final String PAYMENT_DETAILS_CMS_PAGE = "paymentDetails";
	private static final String TEXT_PAYMENT_DETAILS = "text.paymentDetails";

	@Resource(name = "accountBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

	@Resource(name = "jslBundleTemplateExportExcelServiceImpl")
	private JslBundleTemplateExportExcelServiceImpl jslBundleTemplateExportExcelServiceImpl;


	@Resource(name = "jslBundleTemplateExportExcelFacadeImpl")
	private JslBundleTemplateExportExcelFacadeImpl jslBundleTemplateExportExcelFacadeImpl;

	@Resource(name = "jslBidEventDataExportExcelFacade")
	private JslBidEventDataExportExcelFacade JslBidEventDataExportExcelFacade;

	@Resource(name = "jslAuctionFacade")
	private JslAuctionFacade auctionFacade;

	@Resource(name = "jslFreightPdf")
	private JslFreightPdf jslFreightPdf;

	@RequestMapping(value = "/placeOrder", method = RequestMethod.GET)
	@RequireHardLogIn
	public String placeOrder(final Model model) throws CMSItemNotFoundException
	{

		storeCmsPageInModel(model, getContentPageForLabelOrId(PLACE_ORDER_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(PLACE_ORDER_CMS_PAGE));

		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_PLACE_ORDER));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/productEnquiry", method = RequestMethod.GET)
	@RequireHardLogIn
	public String productEnquiry(final Model model) throws CMSItemNotFoundException
	{

		storeCmsPageInModel(model, getContentPageForLabelOrId(PRODUCT_ENQUIRY_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(PRODUCT_ENQUIRY_CMS_PAGE));

		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_PRODUCT_ENQUIRY));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/supportTickets", method = RequestMethod.GET)
	@RequireHardLogIn
	public String supportTickets(final Model model) throws CMSItemNotFoundException
	{

		storeCmsPageInModel(model, getContentPageForLabelOrId(SUPPORT_TICKETS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SUPPORT_TICKETS_CMS_PAGE));

		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_SUPPORT_TICKETS));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/orderHistory", method = RequestMethod.GET)
	@RequireHardLogIn
	public String orderHistory(final Model model) throws CMSItemNotFoundException
	{

		storeCmsPageInModel(model, getContentPageForLabelOrId(ORDER_HISTORY_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORDER_HISTORY_CMS_PAGE));

		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ORDER_HISTORY));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/paymentDetails", method = RequestMethod.GET)
	@RequireHardLogIn
	public String paymentDetails(final Model model) throws CMSItemNotFoundException
	{

		storeCmsPageInModel(model, getContentPageForLabelOrId(PAYMENT_DETAILS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(PAYMENT_DETAILS_CMS_PAGE));

		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_PAYMENT_DETAILS));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}


	@RequestMapping(value = "/downloadExcel", method = RequestMethod.GET, produces = "text/comma-separated-values;charset=UTF-8")
	public void exportBundleDatatoExcel(final HttpServletRequest request, final HttpServletResponse response) throws IOException
	{
		ICsvBeanWriter csvWriter = null;
		final String auctionID = request.getParameter("auctionID");
		final String csvFileName = auctionID + "-LotCatalog.csv";
		final String headerKey = "Content-Disposition";
		final String headerValue = String.format("attachment; filename=\"%s\"", csvFileName);
		response.setHeader(headerKey, headerValue);
		response.setContentType("text/comma-separated-values;charset=UTF-8");
		try
		{
			final List<BundleTemplateDto> bundleTemplateList = jslBundleTemplateExportExcelFacadeImpl
					.getBundleTemplatesForAuctionId(auctionID);
			csvWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);
			final String[] header =
			{ "AuctionId", "LotID", "Location", "Grade", "Thickness", "Width", "Length", "Finish", "Edge", "PVC", "Quality",
					"Quantity", "BaseBidPrice" };

			csvWriter.writeHeader(header);
			for (final BundleTemplateDto bundleTemplate : bundleTemplateList)
			{
				csvWriter.write(bundleTemplate, header);
			}
		}
		catch (final Exception e)
		{
			LOG.error("Exception in file Download for auctionId: " + auctionID, e);
		}
		finally
		{
			if (csvWriter != null)
			{
				csvWriter.close();
			}
		}
	}

	@RequestMapping(value = "/downloadCompletedLotExcel", method = RequestMethod.GET, produces = "text/comma-separated-values;charset=UTF-8")
	public void exportCompletedBundleDatatoExcel(final HttpServletRequest request, final HttpServletResponse response)
			throws IOException
	{
		Map<String, Object> responseObject = new HashMap<>();
		final Map<String, Object> requestObject = new HashMap<>();
		ICsvBeanWriter csvWriter = null;
		final String csvFileName = new SimpleDateFormat("yyyy-MM-dd_HH:mm").format(new Date(System.currentTimeMillis()))
				+ "-LotCatalog.csv";
		final String headerKey = "Content-Disposition";
		final String headerValue = String.format("attachment; filename=\"%s\"", csvFileName);
		response.setHeader(headerKey, headerValue);
		response.setContentType("text/comma-separated-values;charset=UTF-8");
		try
		{
			final boolean allCompletedAuction = Boolean.parseBoolean(request.getParameter("all"));
			final String auctionStartDate = request.getParameter("auctionStartDate");
			final String auctionEndDate = request.getParameter("auctionEndDate");
			final DefaultBundleCommonDto auctionDateRange = new DefaultBundleCommonDto();
			auctionDateRange.setAuctionStartDate(auctionStartDate);
			auctionDateRange.setAuctionEndDate(auctionEndDate);
			requestObject.put("allCompletedAuction", allCompletedAuction);
			requestObject.put("auctionDateRange", auctionDateRange);

			responseObject = jslBundleTemplateExportExcelFacadeImpl.getCompletedBundleData(requestObject, responseObject);

			final List<BundleTemplateDto> wonBundleTemplateList = (List<BundleTemplateDto>) responseObject.get("bundleTemplateList");
			final List<BundleTemplateDto> lostBundleTemplateList = (List<BundleTemplateDto>) responseObject
					.get("lostbundleTemplateList");
			csvWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);
			final String[] header =
			{ "AuctionId", "LotID", "Location", "Grade", "Thickness", "Width", "Length", "Finish", "Edge", "PVC", "Quality",
					"Quantity", "YourBidPrice", "BidWonPrice", "BidStatus", "ApprovalsStatus" };
			csvWriter.writeHeader(header);

			for (final BundleTemplateDto bundleTemplate : wonBundleTemplateList)
			{
				csvWriter.write(bundleTemplate, header);
			}
			for (final BundleTemplateDto bundleTemplate : lostBundleTemplateList)
			{
				csvWriter.write(bundleTemplate, header);
			}
		}
		catch (final Exception e)
		{
			LOG.error("Exception in file Download CustomPageController.exportCompletedBundleDatatoExcel() ", e);
		}
		finally
		{
			if (csvWriter != null)
			{
				csvWriter.close();
			}
		}
	}

	@RequestMapping(value = "/downloadExcelForAdmin/completed", method = RequestMethod.GET, produces = "text/comma-separated-values;charset=UTF-8")
	public void exportCompleteLotDetailsDatatoExcel(final HttpServletRequest request, final HttpServletResponse response)
			throws IOException
	{
		final Map<String, Object> responseObject = new HashMap<>();
		final Map<String, Object> requestObject = new HashMap<>();
		ICsvBeanWriter csvWriter = null;
		final String auctionID = request.getParameter("auctionID");
		final String csvFileName = "CompletedLotBidDetails.csv";
		final String headerKey = "Content-Disposition";
		final String headerValue = String.format("attachment; filename=\"%s\"", csvFileName);
		response.setHeader(headerKey, headerValue);
		response.setContentType("text/comma-separated-values;charset=UTF-8");
		final String auctionStartDate = request.getParameter("auctionStartDate");
		final String auctionEndDate = request.getParameter("auctionEndDate");
		List<BidEventDto> bidEventDataList = null;
		try
		{
			csvWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);
			final String[] header =
			{ "AuctionId", "SalesOrderNumber", "AuctionDate", "LotID", "Location", "MaterialNum", "BatchId", "GradeGroup", "Grade",
					"Thickness", "Width", "Length", "Finish", "Edge", "PVC", "Quality", "Quantity", "Age", "Plant", "StoLoc",
					"DescStoLoc", "SBP", "H1Bidder", "H1BidPricePerMT", "NoOfBids" };

			final boolean dateRange = Boolean.parseBoolean(request.getParameter("dateRange"));
			final DefaultBundleCommonDto auctionDateRange = new DefaultBundleCommonDto();
			auctionDateRange.setAuctionStartDate(auctionStartDate);
			auctionDateRange.setAuctionEndDate(auctionEndDate);
			requestObject.put("dateRange", dateRange);
			requestObject.put("auctionDateRange", auctionDateRange);

			bidEventDataList = JslBidEventDataExportExcelFacade.getCompletedReport(requestObject);

			JslBidEventDataExportExcelFacade.addSalesOrderNumber(bidEventDataList);

			csvWriter.writeHeader(header);

			for (final BidEventDto bidEvent : bidEventDataList)
			{
				csvWriter.write(bidEvent, header);
			}
		}
		catch (final Exception e)
		{
			LOG.error("Exception in file Download for auctionId: " + auctionID, e);
		}
		finally
		{
			if (csvWriter != null)
			{
				csvWriter.close();
			}
		}
	}

	@RequestMapping(value = "/downloadExcelForAdmin/detailed", method = RequestMethod.GET, produces = "text/comma-separated-values;charset=UTF-8")
	public void exportDetailedBidEventDatatoExcel(final HttpServletRequest request, final HttpServletResponse response)
			throws IOException
	{
		final Map<String, Object> responseObject = new HashMap<>();
		final Map<String, Object> requestObject = new HashMap<>();
		ICsvBeanWriter csvWriter = null;
		final String auctionID = request.getParameter("auctionID");
		final String csvFileName = "DetailedLotBidDetails.csv";
		final String headerKey = "Content-Disposition";
		final String headerValue = String.format("attachment; filename=\"%s\"", csvFileName);
		response.setHeader(headerKey, headerValue);
		response.setContentType("text/comma-separated-values;charset=UTF-8");
		final String auctionStartDate = request.getParameter("auctionStartDate");
		final String auctionEndDate = request.getParameter("auctionEndDate");
		List<BidEventDto> bidEventDataList = null;
		try
		{
			csvWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);
			final String[] header =
			{ "AuctionId", "SalesOrderNumber", "AuctionDate", "LotID", "SBP", "H1Bidder", "H1BidderId", "BidPlacedDateTime",
					"BidBasis", "BidPricePerMT", "Quantity", "StatusBidType" };

			final boolean dateRange = Boolean.parseBoolean(request.getParameter("dateRange"));
			final DefaultBundleCommonDto auctionDateRange = new DefaultBundleCommonDto();
			auctionDateRange.setAuctionStartDate(auctionStartDate);
			auctionDateRange.setAuctionEndDate(auctionEndDate);
			requestObject.put("dateRange", dateRange);
			requestObject.put("auctionDateRange", auctionDateRange);

			bidEventDataList = JslBidEventDataExportExcelFacade.getDetailedReport(requestObject);

			JslBidEventDataExportExcelFacade.addSalesOrderNumber(bidEventDataList);

			csvWriter.writeHeader(header);

			for (final BidEventDto bidEvent : bidEventDataList)
			{
				csvWriter.write(bidEvent, header);
			}
		}
		catch (final Exception e)
		{
			LOG.error("Exception in file Download for auctionId: " + auctionID, e);
		}
		finally
		{
			if (csvWriter != null)
			{
				csvWriter.close();
			}
		}
	}

	@RequestMapping(value = "/downloadFreightPdf/{location}", method = RequestMethod.GET)
	public void exportFrieghtChargesPdf(@PathVariable("location")
	final String location, final HttpServletRequest request, final HttpServletResponse response) throws Exception
	{

		final ByteArrayOutputStream baos = new ByteArrayOutputStream(4096);
		response.setContentType(MediaType.APPLICATION_PDF_VALUE);
		jslFreightPdf.generatePdf(location, baos);
		response.setContentLength(baos.size());
		response.setHeader("Content-Disposition", "attachment; filename=Charges.pdf");
		// Flush byte array to servlet output stream.
		final ServletOutputStream out = response.getOutputStream();
		baos.writeTo(out);
		out.flush();
		//response.flushBuffer();
	}

}

