/**
 *
 */
package com.jsl.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jsl.core.payment.dto.JslPaymentRequestDto;
import com.jsl.core.payment.dto.JslPaymentResponse;
import com.jsl.facades.payment.dto.SaleOrderDetailDto;
import com.jsl.facades.payment.dto.TransactionMasterDto;
import com.jsl.facades.payment.service.PaymentProcessFacade;
import com.jsl.facades.paymentdetailshistory.PaymentDetailsHistoryFacade;
import com.jsl.facades.paymentdetailshistory.dto.PaymentDetailsDto;
import com.jsl.facades.paymentdetailshistory.dto.PaymentDetailsHistoryDto;


/**
 * @author s.bhambhani
 *
 */
@Controller
@RequestMapping(value = "/my-account")
public class PaymentPageController extends AbstractSearchPageController
{
	private static final Logger LOG = Logger.getLogger(PaymentPageController.class);
	private static final String BREADCRUMBS_ATTR = "breadcrumbs";


	private static final String PAYMENT_INVOICE_PAGE = "paymentInvoicePage";
	private static final String TEXT_PAYMENT_INVOICE = "text.account.paymentInvoicePage";

	private static final String PAYMENT_HISTORY_PAGE = "paymentHistoryPage";
	private static final String TEXT_PAYMENT_HISTORY = "text.account.paymentHistoryPage";

	private static final String JSL_MAKE_PAYMENT_CMS_PAGE = "jslMakePayment";
	private static final String TEXT_JSL_MAKE_PAYMENT = "text.account.jslMakePayment";

	private static final String JSL_PROCESS_PAYMENT_CMS_PAGE = "jslProcessPayment";
	private static final String TEXT_JSL_PROCESS_PAYMENT = "text.account.jslProcessPayment";

	private static final String JSL_MAKE_PAYMENT_HISTORY_CMS_PAGE = "jslMakePaymentHistory";
	private static final String TEXT_JSL_MAKE_PAYMENT_HISTORY = "text.account.jslMakePaymentHistory";

	@Autowired
	private PaymentDetailsHistoryFacade paymentDetailsHistoryFacade;

	@Resource(name = "paymentProcessFacade")
	private PaymentProcessFacade paymentProcessFacade;

	@Resource(name = "accountBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

	@RequestMapping("/paymentInvoice")
	@RequireHardLogIn
	public String paymentInvoice(final HttpServletRequest request, final HttpServletResponse response, final Model model)
			throws CMSItemNotFoundException

	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(PAYMENT_INVOICE_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(PAYMENT_INVOICE_PAGE));
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_PAYMENT_INVOICE));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/make-payment", method = RequestMethod.GET)
	@RequireHardLogIn
	public String makePaymentPage(final Model model) throws Exception
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(JSL_MAKE_PAYMENT_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(JSL_MAKE_PAYMENT_CMS_PAGE));
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_JSL_MAKE_PAYMENT));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		final List<PaymentDetailsHistoryDto> paymentDetailsHistoryList = paymentDetailsHistoryFacade
				.getPendingPaymentDetailsForCurrentUser();
		model.addAttribute("paymentDetailsHistoryList", paymentDetailsHistoryList);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/make-adv-payment", method = RequestMethod.POST)
	@RequireHardLogIn
	public @ResponseBody List<SaleOrderDetailDto> makeAdvPayment(@RequestBody
	final PaymentDetailsDto paymentDetails) throws Exception
	{
		final List<SaleOrderDetailDto> paymentDetailsHistoryList = paymentDetailsHistoryFacade.getsaleOrderDetails(paymentDetails);
		return paymentDetailsHistoryList;
	}

	@RequestMapping(value = "/make-payment-history", method = RequestMethod.GET)
	@RequireHardLogIn
	public String makePaymentHistory(final Model model) throws Exception
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(JSL_MAKE_PAYMENT_HISTORY_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(JSL_MAKE_PAYMENT_HISTORY_CMS_PAGE));
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_JSL_MAKE_PAYMENT_HISTORY));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		final List<TransactionMasterDto> paymentDetailsHistoryList = paymentDetailsHistoryFacade
				.getTransactionDetailsForCurrentUser();
		model.addAttribute("paymentDetailsHistoryList", paymentDetailsHistoryList);

		return getViewForPage(model);
	}

	@RequestMapping(value = "/submit-make-payment", method = RequestMethod.POST)
	@RequireHardLogIn
	public @ResponseBody JslPaymentRequestDto submitMakePaymentPage(final Model model, @RequestBody
	final PaymentDetailsDto paymentDetails) throws Exception
	{
		LOG.info("Payment submitted : " + paymentDetails.getPaymentdetailshistory());
		return paymentProcessFacade.processPayment(paymentDetails);
	}

	@RequestMapping(value = "/submit-pending-payment", method = RequestMethod.POST)
	@RequireHardLogIn
	public @ResponseBody JslPaymentRequestDto submitPendingPaymentPage(final Model model, @RequestBody
	final TransactionMasterDto transactionMasterDto) throws Exception
	{
		return paymentProcessFacade.processPendingPayment(transactionMasterDto);//paymentProcessFacade.processPayment(paymentDetails);
	}

	@RequireHardLogIn
	@RequestMapping(value = "/process-payment", method = RequestMethod.GET)
	public String processPaymentPage(final HttpServletRequest request, final HttpServletResponse response,
			final JslPaymentResponse paymentResponse, final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(JSL_PROCESS_PAYMENT_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(JSL_PROCESS_PAYMENT_CMS_PAGE));
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_JSL_PROCESS_PAYMENT));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		model.addAttribute("paymentResponse", paymentResponse);
		return getViewForPage(model);
	}

	@RequestMapping("/paymentHistory")
	@RequireHardLogIn
	public String paymentHistory(final HttpServletRequest request, final HttpServletResponse response, final Model model)
			throws Exception
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(PAYMENT_HISTORY_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(PAYMENT_HISTORY_PAGE));
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_PAYMENT_HISTORY));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

}
