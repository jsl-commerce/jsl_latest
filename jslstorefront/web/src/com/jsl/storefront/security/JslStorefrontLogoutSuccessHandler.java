/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.jsl.storefront.security;

import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.security.GUIDCookieStrategy;
import de.hybris.platform.servicelayer.session.SessionService;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;

import com.jsl.facades.customer.JslCustomerFacade;


public class JslStorefrontLogoutSuccessHandler extends SimpleUrlLogoutSuccessHandler
{
	/**
	 *
	 */
	private static final String LOGGED_IN_USER = "loggedInUser";
	private GUIDCookieStrategy guidCookieStrategy;
	private List<String> restrictedPages;
	private SessionService sessionService;
	private JslCustomerFacade jslCustomerFacade;



	/**
	 * @return the jslCustomerFacade
	 */
	public JslCustomerFacade getJslCustomerFacade()
	{
		return jslCustomerFacade;
	}

	/**
	 * @param jslCustomerFacade
	 *           the jslCustomerFacade to set
	 */
	@Required
	public void setJslCustomerFacade(final JslCustomerFacade jslCustomerFacade)
	{
		this.jslCustomerFacade = jslCustomerFacade;
	}

	protected GUIDCookieStrategy getGuidCookieStrategy()
	{
		return guidCookieStrategy;
	}

	@Required
	public void setGuidCookieStrategy(final GUIDCookieStrategy guidCookieStrategy)
	{
		this.guidCookieStrategy = guidCookieStrategy;
	}

	protected List<String> getRestrictedPages()
	{
		return restrictedPages;
	}

	public void setRestrictedPages(final List<String> restrictedPages)
	{
		this.restrictedPages = restrictedPages;
	}

	@Override
	public void onLogoutSuccess(final HttpServletRequest request, final HttpServletResponse response,
			final Authentication authentication) throws IOException, ServletException
	{
		getGuidCookieStrategy().deleteCookie(request, response);
		getSessionService().removeAttribute(WebConstants.USER_CONSENTS);

		//Removing cybercrime pop up cookie
		deleteCookie(LOGGED_IN_USER, response);

		getJslCustomerFacade().setLogin(authentication.getPrincipal().toString(), false);

		// Delegate to default redirect behaviour
		super.onLogoutSuccess(request, response, authentication);
	}

	@Override
	protected String determineTargetUrl(final HttpServletRequest request, final HttpServletResponse response)
	{
		String targetUrl = super.determineTargetUrl(request, response);

		for (final String restrictedPage : getRestrictedPages())
		{
			// When logging out from a restricted page, return user to homepage.
			if (targetUrl.contains(restrictedPage))
			{
				targetUrl = super.getDefaultTargetUrl();
			}
		}

		return targetUrl;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	public void deleteCookie(final String key, final HttpServletResponse response)
	{
		final Cookie loggedInUser = new Cookie(key, "");
		loggedInUser.setMaxAge(0);
		response.addCookie(loggedInUser);
	}
}
