/**
 *
 */
package com.jsl.storefront.security;


import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.security.StorefrontAuthenticationSuccessHandler;
import de.hybris.platform.core.Constants;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;

import com.jsl.facades.customer.JslCustomerFacade;


/**
 * @author manav.magoo
 *
 */
public class JslStorefrontAuthenticationSuccessHandler extends StorefrontAuthenticationSuccessHandler
{

	JslCustomerFacade JslCustomerFacade;
	private static final Logger LOG = Logger.getLogger(JslStorefrontAuthenticationSuccessHandler.class);

	@Override
	public void onAuthenticationSuccess(final HttpServletRequest request, final HttpServletResponse response,
			final Authentication authentication) throws IOException, ServletException
	{
		//if redirected from some specific url, need to remove the cachedRequest to force use defaultTargetUrl
		final RequestCache requestCache = new HttpSessionRequestCache();
		final SavedRequest savedRequest = requestCache.getRequest(request, response);

		if (savedRequest != null)
		{
			for (final String redirectUrlForceDefaultTarget : getListRedirectUrlsForceDefaultTarget())
			{
				if (savedRequest.getRedirectUrl().contains(redirectUrlForceDefaultTarget))
				{
					requestCache.removeRequest(request, response);
					break;
				}
			}
		}

		getJslCustomerFacade().setloginActive(true);
		getCustomerFacade().loginSuccess();
		request.setAttribute(WebConstants.CART_MERGED, Boolean.FALSE);

		// Check if the user is in role admingroup
		if (!isAdminAuthority(authentication))
		{
			getCartRestorationStrategy().restoreCart(request);
			getBruteForceAttackCounter().resetUserCounter(getCustomerFacade().getCurrentCustomerUid());
			getCustomerConsentDataStrategy().populateCustomerConsentDataInSession();
			super.onAuthenticationSuccess(request, response, authentication);
		}
		else
		{
			LOG.warn("Invalidating session for user in the " + Constants.USER.ADMIN_USERGROUP + " group");
			invalidateSession(request, response);
		}
	}

	/**
	 * @return the jslCustomerFacade
	 */
	public JslCustomerFacade getJslCustomerFacade()
	{
		return JslCustomerFacade;
	}

	/**
	 * @param jslCustomerFacade
	 *           the jslCustomerFacade to set
	 */
	public void setJslCustomerFacade(final JslCustomerFacade jslCustomerFacade)
	{
		JslCustomerFacade = jslCustomerFacade;
	}

}
