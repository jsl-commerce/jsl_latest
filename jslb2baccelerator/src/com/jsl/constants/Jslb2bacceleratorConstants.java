/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.jsl.constants;

/**
 * Global class for all Jslb2baccelerator constants. You can add global constants for your extension into this class.
 */
public final class Jslb2bacceleratorConstants extends GeneratedJslb2bacceleratorConstants
{
	public static final String EXTENSIONNAME = "jslb2baccelerator";
	public static final String ADDON_PREFIX = "addon:/jslb2baccelerator";

	private Jslb2bacceleratorConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
