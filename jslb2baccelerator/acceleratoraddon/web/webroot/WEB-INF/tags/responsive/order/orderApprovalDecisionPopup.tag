<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="orderApprovalData" required="true" type="de.hybris.platform.b2bacceleratorfacades.order.data.B2BOrderApprovalData"%>
<%@ attribute name="orderApprovalDecisionForm" required="true" type="com.jsl.forms.OrderApprovalDecisionForm"%>
<%@ attribute name="orderApprovalDecisionURL" required="true" type="java.lang.String"%>
<%@ attribute name="decision" required="true" type="java.lang.String"%>
<%@ attribute name="actionButtonLabel" required="true" type="java.lang.String"%>
<%@ attribute name="actionButtonClass" required="true" type="java.lang.String"%>
<%@ attribute name="modalPopupClass" required="true" type="java.lang.String"%>
<%@ attribute name="commentLabel" required="true" type="java.lang.String"%>
<%@	attribute name="loopIndex" required="true" type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<c:set var="commentMaxChars" value="255" />

<div>
    <form:form method="post" cssClass="orderApprovalDecisionForm" commandName="orderApprovalDecisionForm"
        action="${orderApprovalDecisionURL}">
        <input type="hidden" id ="CSRFToken" name="CSRFToken" value="${csrf}" />
        <c:choose>
            <c:when test="${orderApprovalData.approvalDecisionRequired}">
                <div>
                    <form:input type="hidden" name="workflowActionCode" path="workFlowActionCode" value="${orderApprovalData.workflowActionModelCode}" />
                    <form:input type="hidden" name="approverSelectedDecision" path="approverSelectedDecision" id="approverSelectedDecision" />

                    <button class="${fn:escapeXml(actionButtonClass)}" type="button" onclick="resetTitleSize()">
                        <spring:theme code="${actionButtonLabel}" />
                    </button>
                </div>
                <div style="display: none">
                    <div class="${fn:escapeXml(modalPopupClass)} comment-modal">
                    	<c:choose>
	                    	<c:when test="${decision == 'REJECT'}">
	                    		<form:select path="comments" id = "rejectionReasons_${loopIndex}" onchange="showCommentBox(id)">
	                    			<form:option value = "" label = "Select" />
		                     		<form:option value = "Reject On approval" label = "Reject On approval" />
		                     		<form:option value = "Customer Reject" label = "Customer Reject" />
		                     		<form:option value = "Reject Already Allocated/Partial Allocated" label = "Reject Already Allocated/Partial Allocated" />
		                     		<form:option value = "Reject due to Logistics" label = "Reject due to Logistics" />
		                     		<form:option value = "Specification Mismatch" label = "Specification Mismatch" />
		                     		<form:option value = "Others" label = "Others" />
	                  			</form:select>
	                  			<br><br><br><br>
	                  			<div id = "commentBox_${loopIndex}" style="display: none">
	                  				<div class="headline">
	                            		<spring:theme code="${commentLabel}" />
	                        		</div>
	                  				<form:textarea path="comments" maxlength="255" id="comments_${loopIndex}"></form:textarea>
	                  				<div class="help-block">
                                		<spring:theme code="responsive.checkout.summary.requestApproval.maxchars" arguments="${commentMaxChars}" />
                            		</div>
	                  			</div>
	                    	</c:when>
	                    	<c:otherwise>
	                    		<div class="headline">
	                            	<spring:theme code="${commentLabel}" />
	                        	</div>
	                  			<form:textarea path="comments" maxlength="255"></form:textarea>
	                  			<div class="help-block">
                                	<spring:theme code="responsive.checkout.summary.requestApproval.maxchars" arguments="${commentMaxChars}" />
                            	</div>
	                    	</c:otherwise>
	                  	</c:choose>
                        <div class="form-actions clearfix" style="clear: both;">
	                        <button type="button" class="btn btn-primary approverDecisionButton col-sm-4" data-decision="${fn:escapeXml(decision)}">
	                            <spring:theme code="${actionButtonLabel}" />
	                        </button>
	                        <div class="col-sm-4"></div>
	                        <button type="button" class="btn btn-primary cancelOrderApprovalCommentButton col-sm-4 pull-right">
	                            <spring:theme code="checkout.summary.requestApproval.cancel" />
	                        </button>
                        </div>
                        </div>
                    </div>
                </div>
            </c:when>
        </c:choose>
    </form:form>
</div>
<script>
function showCommentBox(id){
	var index = id.split("_");
	if(document.getElementById(id).value === "Others"){
		document.getElementById("commentBox_" + index[1]).style.display = "block";
		document.getElementById(id).name = "";
		document.getElementById("comments_" + index[1]).name = "comments";
	}
	else{
		document.getElementById("commentBox_" + index[1]).style.display = "none";
		document.getElementById("comments_" + index[1]).name = "";
		document.getElementById(id).name = "comments";
	}
}
</script>