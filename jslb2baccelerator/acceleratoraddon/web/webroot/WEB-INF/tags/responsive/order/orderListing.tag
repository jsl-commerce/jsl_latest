<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="searchUrl" required="true" type="String" %>
<%@ attribute name="messageKey" required="true" type="String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true"/>

<c:if test="${empty searchPageData.results}">
    <div class="row">
        <div class="col-md-6 col-md-push-3">
            <div class="account-section-content content-empty">
                <ycommerce:testId code="orderHistory_noOrders_label">
                    <spring:theme code="text.account.orderHistory.noOrders"/>
                </ycommerce:testId>
            </div>
        </div>
    </div>
</c:if>

<c:if test="${not empty searchPageData.results}">
    <div class="account-section-content">
        <div class="account-orderhistory">
            <div class="account-orderhistory-pagination">
                <nav:pagination top="true" msgKey="${messageKey}" showCurrentPageInfo="true" hideRefineButton="true"
                                supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}"
                                searchPageData="${searchPageData}" searchUrl="${searchUrl}"
                                numberPagesShown="${numberPagesShown}"/>
            </div>
            <div class="responsive-table order_history_details">
                <table class="responsive-table">
                    <thead>
                    <tr class="responsive-table-head hidden-xs">
                        <th id="header1">P.O. Number</th>
                        <th id="header3"><spring:theme code="text.account.orderHistory.orderStatus"/></th>
                        <th id="header4"><spring:theme code="text.account.orderHistory.datePlaced"/></th>
                        <th id="header5">Base Bid Price</th>
                        <th id="header6">H1 Price</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${searchPageData.results}" var="order" varStatus="loop">
                        <tr class="responsive-table-item">
                            <td class="hidden-sm hidden-md hidden-lg"><spring:theme code="text.account.orderHistory.orderNumber"/></td>
                            <td headers="header1" class="responsive-table-cell">
                                <ycommerce:testId code="orderHistoryItem_orderDetails_link">
                                    <spring:url value="/my-account/order/{/orderCode}" var="orderDetailsUrl" htmlEscape="false">
                                        <spring:param name="orderCode" value="${order.code}" />
                                    </spring:url>
                                    <a href="javascript:void(0)" class="responsive-table-link" onclick="showLotDetails('#accordion_${loop.index}');">
                                    	${fn:escapeXml(order.purchaseOrderNumber)}
                                    </a>
                                </ycommerce:testId>
                            </td>
                            <td class="hidden-sm hidden-md hidden-lg"><spring:theme code="text.account.orderHistory.purchaseOrderNumber"/></td>
                           
                            <td class="hidden-sm hidden-md hidden-lg"><spring:theme code="text.account.orderHistory.orderStatus"/></td>
                            <td headers="header3" class="responsive-table-cell">
                                <spring:theme code="text.account.order.status.display.${order.statusDisplay}"/>
                            </td>
                            <td class="hidden-sm hidden-md hidden-lg"><spring:theme
                                    code="text.account.orderHistory.datePlaced"/></td>
                            <td headers="header4" class="responsive-table-cell">
                                <fmt:formatDate value="${order.placed}" dateStyle="medium" timeStyle="short" type="both"/>
                            </td>
                            
                            	<c:forEach items="${orderDetailData}" var="orderData">
                                <c:if test="${orderData.key eq order.code}">
                            		 <td headers="header5" class="responsive-table-cell responsive-table-cell-bold">
                                		<div><fmt:formatNumber value="${orderData.value.winBidData.minBidPrice}" minFractionDigits="0"/></div>
                            		 </td>
                            		 <td headers="header6" class="responsive-table-cell responsive-table-cell-bold">
                                		<div><fmt:formatNumber value="${orderData.value.winBidData.h1Price}" minFractionDigits="0"/></div>
                            		 </td>                                	
                                </c:if>
                                </c:forEach>
                                                                					
                            
                        </tr>
                        <tr width=100% id="accordion_${loop.index}" class="collapse">
                                                                        <td colspan="5">
                                                                            <div class="pr_table_show">
                                                                                <h3>Order Details<i class="fa fa-close" onclick="showLotDetails('#accordion_${loop.index}');"></i></h3>
                                                                                <table class = "table table-striped prods">
                                                                                    <thead>
                                                                                        <tr>
						                                                                    <th>Grade</th>
						                                                                    <th>Thickness</th>
						                                                                    <th>Width</th>
						                                                                    <th>Length</th>
						                                                                    <th>Finish</th>
						                                                                    <th>PVC</th>
						                                                                    <th>Quality</th>
						                                                                    <th>Quantity</th>
						                                                                   <th>Series</th>
						                                                                </tr>    
                                                                					</thead>
                                                                					<tbody>
                                                                					<c:forEach items="${orderDetailData}" var="orderData">
                                                                						<c:if test="${orderData.key eq order.code}">
                                                                							<c:forEach items="${orderData.value.entries}" var="orderDetails" varStatus="lotloop">
	                                                                					<tr>
						                                                                        <td id="item_grade_${loop.index}">${orderDetails.product.gradeGroup}</td>
						                                                                        <td id="item_thickness_${loop.index}">${orderDetails.product.thickness}</td>
						                                                                        <td id="item_width_${loop.index}">${orderDetails.product.width}</td>
						                                                                        <td id="item_length_${loop.index}">${orderDetails.product.length}</td>
						                                                                        <td id="item_finish_${loop.index}">${orderDetails.product.finish}</td>
						                                                                        <td id="item_PVC_${loop.index}">${orderDetails.product.pvc}</td>
						                                                                        <td id="item_quality_${loop.index}">${orderDetails.product.quality}</td>
						                                                                        <td id="item_quantity_${loop.index}">${orderDetails.product.quantity}</td>
						                                                                        <td id="item_series_${loop.index}">${orderDetails.product.series}</td>
						                                                                 </tr>
	                                                                					</c:forEach>
                                                                					</c:if>
                                                                					</c:forEach>                                                         						
                                                                					</tbody>
                                                   
                                                                						</table>
                                                                						</div>
                                                                						</td>
                                                                						</tr>
                        
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="account-orderhistory-pagination">
            <nav:pagination top="false" msgKey="${messageKey}" showCurrentPageInfo="true" hideRefineButton="true"
                            supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}"
                            searchPageData="${searchPageData}" searchUrl="${searchUrl}"
                            numberPagesShown="${numberPagesShown}"/>
        </div>
    </div>
</c:if>


<script>
function showLotDetails(id) {
	$(id).toggle();
}
</script>