<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="b2b-order"
	tagdir="/WEB-INF/tags/addons/jslb2baccelerator/responsive/order"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<script type="text/javascript">
	function toggleAccordion(id) {
		$(id).toggle();
	}
</script>
<div id="loading-wrapper-app">
     
</div> 
<spring:htmlEscape defaultHtmlEscape="true" />
<spring:url
	value="/my-account/order/approvalDecision?page=${searchPageData.pagination.currentPage}&startDate=${startDate}&endDate=${endDate}&orderStatus=${ycommerce:encodeUrl(orderStatus)}&auctionId=${auctionId}"
	var="orderApprovalDecisionURL" htmlEscape="false" />
<c:set var="searchUrl"
	value="/my-account/approval-dashboard?sort=${ycommerce:encodeUrl(searchPageData.pagination.sort)}&startDate=${startDate}&endDate=${endDate}&orderStatus=${ycommerce:encodeUrl(orderStatus)}&auctionId=${auctionId}" />

<div class="account-section-header">
	<spring:theme code="text.account.orderApprovalDashboard" />
</div>
<input type="hidden" id="orderList" value="${listApprovalData}" />
<c:if test="${empty listApprovalData}">
	<div class="row">
		<div class="account-orderhistory-pagination">
			<nav:paginationOrderApproval top="true"
				msgKey="text.account.orderHistory.page" showCurrentPageInfo="true"
				hideRefineButton="true" supportShowPaged="${isShowPageAllowed}"
				supportShowAll="${isShowAllAllowed}"
				searchPageData="${searchPageData}" searchUrl="${searchUrl}"
				numberPagesShown="${numberPagesShown}" startDate="${startDate}"
				endDate="${endDate}" orderStatus="${orderStatus}" />
			<div class="account-section-content content-empty">
				<ycommerce:testId code="orderHistory_noOrders_label">
					<spring:theme code="text.account.orderHistory.noOrders" />
				</ycommerce:testId>
			</div>
		</div>
	</div>
</c:if>

<c:if test="${not empty listApprovalData}">
	<div class="account-section-content	">
		<div class="account-orderhistory">
			<div class="account-orderhistory-pagination">
				<nav:paginationOrderApproval top="true"
					msgKey="text.account.orderHistory.page" showCurrentPageInfo="true"
					hideRefineButton="true" supportShowPaged="${isShowPageAllowed}"
					supportShowAll="${isShowAllAllowed}"
					searchPageData="${searchPageData}" searchUrl="${searchUrl}"
					numberPagesShown="${numberPagesShown}" startDate="${startDate}"
					endDate="${endDate}" orderStatus="${orderStatus}" />
			</div>

			<div class="responsive-table" style="overflow-x : auto;">
				<table id="order_history" class="responsive-table">
					<thead>
						<tr class="responsive-table-head">
							<th><spring:theme
									code="text.account.orderApprovalDashBoard.orderNumber" /></th>
							<th><spring:theme
									code="text.account.orderApprovalDashBoard.purchaseOrderNumber" /></th>
							<th><spring:theme
									code="text.account.orderApprovalDetails.orderPlacedBy" /></th>
							<th><spring:theme
									code="text.account.orderHistory.datePlaced" /></th>
							<th>Base Bid Price</th>
							<th>H1 Price</th>
							<th><spring:theme
									code="text.account.orderApprovalDashBoard.orderStatus" /></th>
							<th>Comments</th>
							<th></th>
							<th>
								<button onclick="sendList()">Approve</button>
							</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${listApprovalData}" var="order"
							varStatus="loop">
							<tr class="responsive-table-item">
								<td class="hidden-sm hidden-md hidden-lg"><spring:theme
										code='text.account.orderApprovalDashBoard.orderNumber' /></td>
								<td class="responsive-table-cell"><ycommerce:testId
										code="orderApprovalDashboard_orderNumber_link">
										<spring:url
											value="/my-account/orderApprovalDetails/{/workflowActionModelCode}"
											var="orderApprovalDetailsUrl" htmlEscape="false">
											<spring:param name="workflowActionModelCode"
												value="${order.workflowActionModelCode}" />
										</spring:url>
										<a href=# class="responsive-table-link"
											onclick="toggleAccordion('#accordion_${loop.index}');">${fn:escapeXml(order.b2bOrderData.code)}</a>
									</ycommerce:testId></td>
								<td class="hidden-sm hidden-md hidden-lg"><spring:theme
										code='text.account.orderApprovalDashBoard.purchaseOrderNumber' /></td>
								<td class="responsive-table-cell"><ycommerce:testId
										code="orderApprovalDashboard_purchaseOrderNumber_label">
										<c:choose>
											<c:when
												test="${empty order.b2bOrderData.purchaseOrderNumber}">
											&nbsp;
										</c:when>
											<c:otherwise>
											${fn:escapeXml(order.b2bOrderData.purchaseOrderNumber)}
										</c:otherwise>
										</c:choose>
									</ycommerce:testId></td>
								<td class="hidden-sm hidden-md hidden-lg"><spring:theme
										code="text.account.orderApprovalDetails.orderPlacedBy" /></td>
								<td class="responsive-table-cell"><ycommerce:testId
										code="orderApprovalDashboard_orderpurchaser_link">
									${fn:escapeXml(order.b2bOrderData.winBidData.customerName)}
								</ycommerce:testId></td>
								<td class="hidden-sm hidden-md hidden-lg"><spring:theme
										code='text.account.orderHistory.datePlaced' /></td>
								<td class="responsive-table-cell"><fmt:formatDate
										value="${order.b2bOrderData.created}" dateStyle="medium"
										timeStyle="short" type="both" /></td>
								<td
									class="hidden-sm hidden-md hidden-lg responsive-table-cell-bold"><spring:theme
										code='text.account.order.total' /></td>
								<td class="responsive-table-cell responsive-table-cell-bold">
									<fmt:formatNumber
										value="${order.b2bOrderData.winBidData.minBidPrice}"
										minFractionDigits="0" />
								</td>
								<td class="responsive-table-cell responsive-table-cell-bold">
									<fmt:formatNumber
										value="${order.b2bOrderData.winBidData.h1Price}"
										minFractionDigits="0" />
								</td>
								<td class="hidden-sm hidden-md hidden-lg"><spring:theme
										code='text.account.orderApprovalDashBoard.orderStatus' /></td>
								<td class="status"><ycommerce:testId
										code="orderApprovalDashboard_orderStatus_label">
										<spring:theme
											code="text.account.order.status.display.${order.b2bOrderData.statusDisplay}" />
									</ycommerce:testId></td>
								<td>
									<div>${fn:escapeXml(order.b2bOrderData.b2bPermissionResult[0].approverNotes)}</div>
								</td>
								<td><span class="col-sm-6"> <b2b-order:orderApprovalDecisionPopup
											orderApprovalDecisionForm="${orderApprovalDecisionForm}"
											orderApprovalData="${order}"
											orderApprovalDecisionURL="${orderApprovalDecisionURL}"
											loopIndex="${loop.index}" decision="REJECT"
											actionButtonLabel="Reject"
											actionButtonClass="approverDecisionRejectButton"
											modalPopupClass="orderApprovalRejectCommentModal"
											commentLabel="text.account.orderApproval.reject.reason" />
								</span></td>
								<td><c:choose>
										<c:when
											test="${order.b2bOrderData.statusDisplay == 'pending.approval'}">
											<input type="checkbox" id="chbx_${loop.index}"
												value="${order.workflowActionModelCode}"
												onclick="addToApprovalList(id)">
										</c:when>
									</c:choose></td>
							</tr>
							<tr id="accordion_${loop.index}" class="collapse">
								<td colspan="13">
									<div class="pr_table_show">
										<h3>
											Lot Details<i class="fa fa-close"
												onclick="toggleAccordion('#accordion_${loop.index}');"></i>
										</h3>
										<table class="table table-striped prods">
											<thead>
												<tr>
													<th>Grade</th>
													<th>Thickness</th>
													<th>Width</th>
													<th>Length</th>
													<th>Finish</th>
													<!-- <th>Edge</th> -->
													<th>PVC</th>
													<th>Quality</th>
													<th>Quantity</th>
													<th>Series</th>
													<th>Ageing</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${order.b2bOrderData.unconsignedEntries}"
													var="products">
													<tr class="prodRow">
														<td id="item_grade_${loop.index}">${products.product.gradeGroup}</td>
														<td id="item_thickness_${loop.index}">${products.product.thickness}</td>
														<td id="item_width_${loop.index}">${products.product.width}</td>
														<td id="item_length_${loop.index}">${products.product.length}</td>
														<td id="item_finish_${loop.index}">${products.product.finish}</td>
														<%-- <td id="item_edge_${loop.index}">${products.edgeCondition}</td> --%>
														<td id="item_PVC_${loop.index}">${products.product.pvc}</td>
														<td id="item_quality_${loop.index}">${products.product.quality}</td>
														<td id="item_quantity_${loop.index}">${products.product.quantity}</td>
														<td id="item_series_${loop.index}">${products.product.series}</td>
														<td id="item_series_${loop.index}">${products.product.ageingOfManufacturing}</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</c:if>
<script>
	var approvedOrderList = [];
	function resetTitleSize() {
		if (document.getElementById("cboxTitle") != null) {
			document.getElementById("cboxTitle").style.height = "auto";
		}
	}

	function addToApprovalList(id) {
		var workflowCode = document.getElementById(id).value;
		if (document.getElementById(id).checked) {
			approvedOrderList.push(workflowCode);
		} else {
			approvedOrderList
					.splice(approvedOrderList.indexOf(workflowCode), 1);
		}
	}

	function sendList() {
		var url = ACC.config.encodedContextPath
				+ "/my-account/order/approvedDecision?CSRFToken=${csrf}";

		var returnUrl = ACC.config.encodedContextPath
				+ "/my-account/approval-dashboard?page=${searchPageData.pagination.currentPage}&startDate=${startDate}&endDate=${endDate}&orderStatus=${ycommerce:encodeUrl(orderStatus)}&auctionId=${auctionId}";
		var workflowList = approvedOrderList.join();

		if (!$.isEmptyObject(approvedOrderList)) {

			var div = document.getElementById("loading-wrapper-app");
			div.innerHTML = "<div id='loading-wrapper'><div class='loader'></div></div>";
			$.ajax({
				type : "POST",
				url : url,
				data : {workflowList : workflowList},
				success : function(response) {
					window.location.href = returnUrl;
				},
				error : function(response) {
					//window.location.href = returnUrl;
				}
			});
		} else
			alert("Please Select atleast 1 order ID");
	}
</script>