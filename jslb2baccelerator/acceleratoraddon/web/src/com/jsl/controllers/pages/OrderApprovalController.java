/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.jsl.controllers.pages;


/**
 * Controller for order approval dashboard.
 */

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BOrderApprovalData;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.workflow.enums.WorkflowActionType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.jsl.facades.order.service.JslB2BOrderFacade;
import com.jsl.forms.OrderApprovalDecisionForm;

@Controller
@RequestMapping("/my-account")
public class OrderApprovalController extends AbstractSearchPageController
{
	private static final String REDIRECT_MY_ACCOUNT = REDIRECT_PREFIX + "/my-account";
	private static final String ORDER_APPROVAL_DASHBOARD_CMS_PAGE = "order-approval-dashboard";
	private static final String WORKFLOW_ACTION_CODE_PATH_VARIABLE_PATTERN = "{workflowActionCode:.*}";
	private static final String ORDER_APPROVAL_DETAIL_CMS_PAGE = "order-approval-details";

	private static final Logger LOG = Logger.getLogger(OrderApprovalController.class);

	@Resource(name = "b2bOrderFacade")
	private JslB2BOrderFacade orderFacade;

	@Resource(name = "accountBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

	@RequestMapping(value = "/approval-dashboard", method = RequestMethod.GET)
	@RequireHardLogIn
	public String orderApprovalDashboard(@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final AbstractSearchPageController.ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode,
			@RequestParam(value = "startDate", required = false) final String startDate,
			@RequestParam(value = "endDate", required = false) final String endDate,
			@RequestParam(value = "orderStatus", required = false) final String orderStatus,
			@RequestParam(value = "errorMessage" ,required = false) final String error,
			@RequestParam(value = "auctionId", required = false)
			final String auctionId,
			final Model model, final HttpServletRequest request) throws CMSItemNotFoundException
	{
		final PageableData pageableData = createPageableData(page, 25, "byDate", showMode);
		final SearchPageData<? extends B2BOrderApprovalData> searchPageData = orderFacade.getFilteredPagedOrdersForApproval((new WorkflowActionType[]
				{ WorkflowActionType.START }), pageableData, startDate, endDate, orderStatus, auctionId);

		final CsrfToken token = new HttpSessionCsrfTokenRepository().loadToken(request);

		if("error".equalsIgnoreCase(error))
		{
			GlobalMessages.addErrorMessage(model, "text.account.orderApproval.addApproverComments");
		}
		model.addAttribute("auctionIdList", orderFacade.getAuctionIds());
		model.addAttribute("startDate", startDate);
		model.addAttribute("endDate", endDate);
		model.addAttribute("orderStatus", orderStatus);
		model.addAttribute("auctionId", auctionId);
		model.addAttribute("csrf", token.getToken());
		final List<B2BOrderApprovalData> listApprovalData = new ArrayList<B2BOrderApprovalData>();
		searchPageData.getResults().removeAll(Collections.singleton(null));
		searchPageData.getResults().forEach(s -> {
			listApprovalData.add(orderFacade.getOrderApprovalDetailsForCode(s.getWorkflowActionModelCode()));
		});
		if (!model.containsAttribute("orderApprovalDecisionForm"))
		{
			model.addAttribute("orderApprovalDecisionForm", new OrderApprovalDecisionForm());
		}

		listApprovalData.removeAll(Collections.singleton(null));

		populateModel(model, searchPageData, showMode);

		model.addAttribute("listApprovalData", listApprovalData);
		model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.orderApprovalDashboard"));
		storeCmsPageInModel(model, getContentPageForLabelOrId(ORDER_APPROVAL_DASHBOARD_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORDER_APPROVAL_DASHBOARD_CMS_PAGE));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}


	@RequestMapping(value = "/orderApprovalDetails/" + WORKFLOW_ACTION_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	@RequireHardLogIn
	public String orderApprovalDetails(@PathVariable("workflowActionCode") final String workflowActionCode, final Model model)
			throws CMSItemNotFoundException
	{
		try
		{
			final B2BOrderApprovalData orderApprovalDetails = orderFacade.getOrderApprovalDetailsForCode(workflowActionCode);
			model.addAttribute("orderApprovalData", orderApprovalDetails);
			model.addAttribute("orderData", orderApprovalDetails.getB2bOrderData());

			if (!model.containsAttribute("orderApprovalDecisionForm"))
			{
				model.addAttribute("orderApprovalDecisionForm", new OrderApprovalDecisionForm());
			}

			final List<Breadcrumb> breadcrumbs = accountBreadcrumbBuilder.getBreadcrumbs(null);
			breadcrumbs.add(new Breadcrumb("/my-account/approval-dashboard",
					getMessageSource().getMessage("text.account.orderApprovalDashboard", null, getI18nService().getCurrentLocale()),
					null));
			breadcrumbs.add(new Breadcrumb("#", getMessageSource().getMessage("text.account.order.orderBreadcrumb", new Object[]
			{ orderApprovalDetails.getB2bOrderData().getCode() }, "Order {0}", getI18nService().getCurrentLocale()), null));

			model.addAttribute("breadcrumbs", breadcrumbs);

		}
		catch (final UnknownIdentifierException e)
		{
			LOG.warn("Attempted to load a order that does not exist or is not visible", e);
			return REDIRECT_MY_ACCOUNT;
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(ORDER_APPROVAL_DETAIL_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORDER_APPROVAL_DETAIL_CMS_PAGE));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/order/approvalDecision", method = RequestMethod.POST)
	@RequireHardLogIn
	public String orderApprovalDecision(
			@ModelAttribute("orderApprovalDecisionForm") final OrderApprovalDecisionForm orderApprovalDecisionForm,
			@RequestParam(value = "page", required = false) final int page,
			@RequestParam(value = "startDate", required = false) final String startDate,
			@RequestParam(value = "endDate", required = false) final String endDate,
			@RequestParam(value = "orderStatus", required = false) final String orderStatus,
			@RequestParam(value = "auctionId", required = false)
			final String auctionId,
			@RequestBody final String workflowList,
			final Model model, final HttpServletRequest request) throws CMSItemNotFoundException
	{
		final String paginationUrl = "?page=" + page + "&startDate=" + startDate + "&endDate=" + endDate + "&orderStatus="
				+ orderStatus + "&auctionId=" + auctionId;

		try
		{
			if ("REJECT".contains(orderApprovalDecisionForm.getApproverSelectedDecision())
					&& StringUtils.isEmpty(orderApprovalDecisionForm.getComments()))
			{
				GlobalMessages.addErrorMessage(model, "text.account.orderApproval.addApproverComments");
				model.addAttribute("orderApprovalDecisionForm", orderApprovalDecisionForm);
				return REDIRECT_MY_ACCOUNT + "/approval-dashboard"+paginationUrl+"&errorMessage=error";
			}

			if ("REJECT".contains(orderApprovalDecisionForm.getApproverSelectedDecision())
					&& !StringUtils.isEmpty(orderApprovalDecisionForm.getComments())
					&& ",".equalsIgnoreCase(orderApprovalDecisionForm.getComments().trim()))
			{
				GlobalMessages.addErrorMessage(model, "text.account.orderApproval.addApproverComments");
				model.addAttribute("orderApprovalDecisionForm", orderApprovalDecisionForm);
				return REDIRECT_MY_ACCOUNT + "/approval-dashboard"+paginationUrl+"&errorMessage=error";
			}

			final B2BOrderApprovalData b2bOrderApprovalData = new B2BOrderApprovalData();
			b2bOrderApprovalData.setSelectedDecision(orderApprovalDecisionForm.getApproverSelectedDecision());
			b2bOrderApprovalData.setApprovalComments(orderApprovalDecisionForm.getComments());
			b2bOrderApprovalData.setWorkflowActionModelCode(orderApprovalDecisionForm.getWorkFlowActionCode());

			orderFacade.setOrderApprovalDecision(b2bOrderApprovalData);

		}
		catch (final UnknownIdentifierException e)
		{
			LOG.warn("Attempted to load a order that does not exist or is not visible", e);
			return REDIRECT_MY_ACCOUNT;
		}

		return REDIRECT_MY_ACCOUNT + "/approval-dashboard"+paginationUrl;
	}
	@RequestMapping(value = "/order/approvedDecision", method = RequestMethod.POST)
	@RequireHardLogIn
	public String orderApprovalDecision(
			@RequestParam(value = "workflowList") final String workFlowList,
			final Model model, final HttpServletRequest request) throws CMSItemNotFoundException
	{
		final String[] WorkFlowCode = workFlowList.split(",");
		for(int i = 0; i < WorkFlowCode.length; i++)
		{
			final B2BOrderApprovalData b2bOrderApprovalData = new B2BOrderApprovalData();
			b2bOrderApprovalData.setSelectedDecision("APPROVE");
			b2bOrderApprovalData.setWorkflowActionModelCode(WorkFlowCode[i]);

			orderFacade.setOrderApprovalDecision(b2bOrderApprovalData);

		}
		return REDIRECT_MY_ACCOUNT + "/approval-dashboard";
}
}