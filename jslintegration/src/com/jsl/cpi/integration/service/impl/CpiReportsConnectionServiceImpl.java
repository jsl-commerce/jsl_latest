/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.cpi.integration.service.impl;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;

import org.apache.log4j.Logger;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;
import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;

import com.jsl.cpi.integration.service.CpiReportsConnectionService;
import com.jsl.cpi.report.dto.CPIRequest;
import com.jsl.cpi.report.dto.CPIResponse;
import com.jsl.cpi.util.JslWebServiceMessageCallback;


/**
 *
 */
public class CpiReportsConnectionServiceImpl implements CpiReportsConnectionService
{

	private Jaxb2Marshaller marshaller;
	private WebServiceTemplate webServiceTemplate;

	private static MessageFactory msgFactory;
	private static SaajSoapMessageFactory saajSoapMessageFactory;

	private JslWebServiceMessageCallback jslWebServiceMessageCallback;

	private ClientInterceptor[] clientInterceptors;
	private static final Logger LOG = Logger.getLogger(CpiReportsConnectionServiceImpl.class);

	public CPIResponse getResponse(final String url, final CPIRequest request) throws SOAPException
	{
		this.createWebServiceTemplate();
		LOG.info("Calling soap webservice with url : " + url);
		return (CPIResponse) this.getWebServiceTemplate().marshalSendAndReceive(url, request, jslWebServiceMessageCallback);
	}

	private void createWebServiceTemplate() throws SOAPException
	{
		LOG.info("Creating workservice template");
		final WebServiceTemplate template = getWebServiceTemplate();
		template.setMarshaller(marshaller);
		template.setUnmarshaller(marshaller);
		template.setMessageFactory(getMessageFactory());
		template.setInterceptors(getClientInterceptors());
	}

	/**
	 * @return the marshaller
	 */
	public Jaxb2Marshaller getMarshaller()
	{
		return marshaller;
	}


	/**
	 * @param marshaller
	 *           the marshaller to set
	 */
	public void setMarshaller(final Jaxb2Marshaller marshaller)
	{
		this.marshaller = marshaller;
	}


	/**
	 * @return the webServiceTemplate
	 */
	public WebServiceTemplate getWebServiceTemplate()
	{
		return webServiceTemplate;
	}


	private SaajSoapMessageFactory getMessageFactory() throws SOAPException
	{
		if (this.msgFactory == null)
		{
			this.msgFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
		}
		if (this.saajSoapMessageFactory == null)
		{
			this.saajSoapMessageFactory = new SaajSoapMessageFactory(msgFactory);
		}
		return saajSoapMessageFactory;

	}


	/**
	 * @param webServiceTemplate
	 *           the webServiceTemplate to set
	 */
	public void setWebServiceTemplate(final WebServiceTemplate webServiceTemplate)
	{
		this.webServiceTemplate = webServiceTemplate;
	}


	/**
	 * @return the jslWebServiceMessageCallback
	 */
	public JslWebServiceMessageCallback getJslWebServiceMessageCallback()
	{
		return jslWebServiceMessageCallback;
	}


	/**
	 * @param jslWebServiceMessageCallback
	 *           the jslWebServiceMessageCallback to set
	 */
	public void setJslWebServiceMessageCallback(final JslWebServiceMessageCallback jslWebServiceMessageCallback)
	{
		this.jslWebServiceMessageCallback = jslWebServiceMessageCallback;
	}


	/**
	 * @return the clientInterceptors
	 */
	public ClientInterceptor[] getClientInterceptors()
	{
		return clientInterceptors;
	}


	/**
	 * @param clientInterceptors
	 *           the clientInterceptors to set
	 */
	public void setClientInterceptors(final ClientInterceptor[] clientInterceptors)
	{
		this.clientInterceptors = clientInterceptors;
	}

}
