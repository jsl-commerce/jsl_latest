/*
 * [y] hybris Platform
 * 
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of SAP 
 * Hybris ("Confidential Information"). You shall not disclose such 
 * Confidential Information and shall use it only in accordance with the 
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.cpi.integration.service;

import javax.xml.soap.SOAPException;

import com.jsl.cpi.report.dto.CPIRequest;
import com.jsl.cpi.report.dto.CPIResponse;


/**
 *
 */
public interface CpiReportsConnectionService
{

	public CPIResponse getResponse(final String url, final CPIRequest request) throws SOAPException;
}
