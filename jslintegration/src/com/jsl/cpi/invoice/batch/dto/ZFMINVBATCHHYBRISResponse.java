
package com.jsl.cpi.invoice.batch.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.jsl.cpi.report.dto.CPIResponse;


/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IT_CHAR_FIELD" type="{urn:sap-com:document:sap:rfc:functions}ZTT_HYB_BATCH_CHAR"/>
 *         &lt;element name="IT_OUTPUT" type="{urn:sap-com:document:sap:rfc:functions}ZTT_BATCH_HYBRIS"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder =
{ "itcharfield", "itoutput" })
@XmlRootElement(name = "ZFM_INV_BATCH_HYBRISResponse")
public class ZFMINVBATCHHYBRISResponse implements CPIResponse
{

	@XmlElement(name = "IT_CHAR_FIELD", required = true)
	protected ZTTHYBBATCHCHAR itcharfield;
	@XmlElement(name = "IT_OUTPUT", required = true)
	protected ZTTBATCHHYBRIS itoutput;

	/**
	 * Gets the value of the itcharfield property.
	 * 
	 * @return possible object is {@link ZTTHYBBATCHCHAR }
	 * 
	 */
	public ZTTHYBBATCHCHAR getITCHARFIELD()
	{
		return itcharfield;
	}

	/**
	 * Sets the value of the itcharfield property.
	 * 
	 * @param value
	 *           allowed object is {@link ZTTHYBBATCHCHAR }
	 * 
	 */
	public void setITCHARFIELD(ZTTHYBBATCHCHAR value)
	{
		this.itcharfield = value;
	}

	/**
	 * Gets the value of the itoutput property.
	 * 
	 * @return possible object is {@link ZTTBATCHHYBRIS }
	 * 
	 */
	public ZTTBATCHHYBRIS getITOUTPUT()
	{
		return itoutput;
	}

	/**
	 * Sets the value of the itoutput property.
	 * 
	 * @param value
	 *           allowed object is {@link ZTTBATCHHYBRIS }
	 * 
	 */
	public void setITOUTPUT(ZTTBATCHHYBRIS value)
	{
		this.itoutput = value;
	}

}
