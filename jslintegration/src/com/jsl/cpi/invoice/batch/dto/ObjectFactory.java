
package com.jsl.cpi.invoice.batch.dto;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each Java content interface and Java element interface generated in the
 * cpiwsdl package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the Java representation for XML content.
 * The Java representation of XML content can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory methods for each of these are provided in
 * this class.
 * 
 */
@XmlRegistry
public class ObjectFactory
{


	/**
	 * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cpiwsdl
	 * 
	 */
	public ObjectFactory()
	{
	}

	/**
	 * Create an instance of {@link ZFMINVBATCHHYBRIS }
	 * 
	 */
	public ZFMINVBATCHHYBRIS createZFMINVBATCHHYBRIS()
	{
		return new ZFMINVBATCHHYBRIS();
	}

	/**
	 * Create an instance of {@link ZTTHYBBATCHCHAR }
	 * 
	 */
	public ZTTHYBBATCHCHAR createZTTHYBBATCHCHAR()
	{
		return new ZTTHYBBATCHCHAR();
	}

	/**
	 * Create an instance of {@link ZTTBATCHHYBRIS }
	 * 
	 */
	public ZTTBATCHHYBRIS createZTTBATCHHYBRIS()
	{
		return new ZTTBATCHHYBRIS();
	}

	/**
	 * Create an instance of {@link ZFMINVBATCHHYBRISResponse }
	 * 
	 */
	public ZFMINVBATCHHYBRISResponse createZFMINVBATCHHYBRISResponse()
	{
		return new ZFMINVBATCHHYBRISResponse();
	}

	/**
	 * Create an instance of {@link ZHYBBATCHCHAR }
	 * 
	 */
	public ZHYBBATCHCHAR createZHYBBATCHCHAR()
	{
		return new ZHYBBATCHCHAR();
	}

	/**
	 * Create an instance of {@link ZBATCHHYBRIS }
	 * 
	 */
	public ZBATCHHYBRIS createZBATCHHYBRIS()
	{
		return new ZBATCHHYBRIS();
	}

}
