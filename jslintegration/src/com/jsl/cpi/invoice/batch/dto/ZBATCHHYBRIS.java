
package com.jsl.cpi.invoice.batch.dto;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>
 * Java class for ZBATCH_HYBRIS complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ZBATCH_HYBRIS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MANDT" type="{urn:sap-com:document:sap:rfc:functions}clnt3"/>
 *         &lt;element name="VBELN" type="{urn:sap-com:document:sap:rfc:functions}char10"/>
 *         &lt;element name="POSNR" type="{urn:sap-com:document:sap:rfc:functions}numeric6"/>
 *         &lt;element name="VBELV" type="{urn:sap-com:document:sap:rfc:functions}char10"/>
 *         &lt;element name="POSNV" type="{urn:sap-com:document:sap:rfc:functions}numeric6"/>
 *         &lt;element name="KUNNR" type="{urn:sap-com:document:sap:rfc:functions}char10"/>
 *         &lt;element name="KUNAG" type="{urn:sap-com:document:sap:rfc:functions}char10"/>
 *         &lt;element name="MATNR" type="{urn:sap-com:document:sap:rfc:functions}char18"/>
 *         &lt;element name="WERKS" type="{urn:sap-com:document:sap:rfc:functions}char4"/>
 *         &lt;element name="LGORT" type="{urn:sap-com:document:sap:rfc:functions}char4"/>
 *         &lt;element name="CHARG" type="{urn:sap-com:document:sap:rfc:functions}char10"/>
 *         &lt;element name="LFIMG" type="{urn:sap-com:document:sap:rfc:functions}quantum13.3"/>
 *         &lt;element name="VGBEL" type="{urn:sap-com:document:sap:rfc:functions}char10"/>
 *         &lt;element name="VGPOS" type="{urn:sap-com:document:sap:rfc:functions}numeric6"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ZBATCH_HYBRIS", propOrder =
{ "mandt", "vbeln", "posnr", "vbelv", "posnv", "kunnr", "kunag", "matnr", "werks", "lgort", "charg", "lfimg", "vgbel", "vgpos" })
public class ZBATCHHYBRIS
{

	@XmlElement(name = "MANDT", required = true)
	protected String mandt;
	@XmlElement(name = "VBELN", required = true)
	protected String vbeln;
	@XmlElement(name = "POSNR", required = true)
	protected String posnr;
	@XmlElement(name = "VBELV", required = true)
	protected String vbelv;
	@XmlElement(name = "POSNV", required = true)
	protected String posnv;
	@XmlElement(name = "KUNNR", required = true)
	protected String kunnr;
	@XmlElement(name = "KUNAG", required = true)
	protected String kunag;
	@XmlElement(name = "MATNR", required = true)
	protected String matnr;
	@XmlElement(name = "WERKS", required = true)
	protected String werks;
	@XmlElement(name = "LGORT", required = true)
	protected String lgort;
	@XmlElement(name = "CHARG", required = true)
	protected String charg;
	@XmlElement(name = "LFIMG", required = true)
	protected BigDecimal lfimg;
	@XmlElement(name = "VGBEL", required = true)
	protected String vgbel;
	@XmlElement(name = "VGPOS", required = true)
	protected String vgpos;

	/**
	 * Gets the value of the mandt property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMANDT()
	{
		return mandt;
	}

	/**
	 * Sets the value of the mandt property.
	 * 
	 * @param value
	 *           allowed object is {@link String }
	 * 
	 */
	public void setMANDT(String value)
	{
		this.mandt = value;
	}

	/**
	 * Gets the value of the vbeln property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getVBELN()
	{
		return vbeln;
	}

	/**
	 * Sets the value of the vbeln property.
	 * 
	 * @param value
	 *           allowed object is {@link String }
	 * 
	 */
	public void setVBELN(String value)
	{
		this.vbeln = value;
	}

	/**
	 * Gets the value of the posnr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPOSNR()
	{
		return posnr;
	}

	/**
	 * Sets the value of the posnr property.
	 * 
	 * @param value
	 *           allowed object is {@link String }
	 * 
	 */
	public void setPOSNR(String value)
	{
		this.posnr = value;
	}

	/**
	 * Gets the value of the vbelv property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getVBELV()
	{
		return vbelv;
	}

	/**
	 * Sets the value of the vbelv property.
	 * 
	 * @param value
	 *           allowed object is {@link String }
	 * 
	 */
	public void setVBELV(String value)
	{
		this.vbelv = value;
	}

	/**
	 * Gets the value of the posnv property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPOSNV()
	{
		return posnv;
	}

	/**
	 * Sets the value of the posnv property.
	 * 
	 * @param value
	 *           allowed object is {@link String }
	 * 
	 */
	public void setPOSNV(String value)
	{
		this.posnv = value;
	}

	/**
	 * Gets the value of the kunnr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getKUNNR()
	{
		return kunnr;
	}

	/**
	 * Sets the value of the kunnr property.
	 * 
	 * @param value
	 *           allowed object is {@link String }
	 * 
	 */
	public void setKUNNR(String value)
	{
		this.kunnr = value;
	}

	/**
	 * Gets the value of the kunag property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getKUNAG()
	{
		return kunag;
	}

	/**
	 * Sets the value of the kunag property.
	 * 
	 * @param value
	 *           allowed object is {@link String }
	 * 
	 */
	public void setKUNAG(String value)
	{
		this.kunag = value;
	}

	/**
	 * Gets the value of the matnr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMATNR()
	{
		return matnr;
	}

	/**
	 * Sets the value of the matnr property.
	 * 
	 * @param value
	 *           allowed object is {@link String }
	 * 
	 */
	public void setMATNR(String value)
	{
		this.matnr = value;
	}

	/**
	 * Gets the value of the werks property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWERKS()
	{
		return werks;
	}

	/**
	 * Sets the value of the werks property.
	 * 
	 * @param value
	 *           allowed object is {@link String }
	 * 
	 */
	public void setWERKS(String value)
	{
		this.werks = value;
	}

	/**
	 * Gets the value of the lgort property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getLGORT()
	{
		return lgort;
	}

	/**
	 * Sets the value of the lgort property.
	 * 
	 * @param value
	 *           allowed object is {@link String }
	 * 
	 */
	public void setLGORT(String value)
	{
		this.lgort = value;
	}

	/**
	 * Gets the value of the charg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCHARG()
	{
		return charg;
	}

	/**
	 * Sets the value of the charg property.
	 * 
	 * @param value
	 *           allowed object is {@link String }
	 * 
	 */
	public void setCHARG(String value)
	{
		this.charg = value;
	}

	/**
	 * Gets the value of the lfimg property.
	 * 
	 * @return possible object is {@link BigDecimal }
	 * 
	 */
	public BigDecimal getLFIMG()
	{
		return lfimg;
	}

	/**
	 * Sets the value of the lfimg property.
	 * 
	 * @param value
	 *           allowed object is {@link BigDecimal }
	 * 
	 */
	public void setLFIMG(BigDecimal value)
	{
		this.lfimg = value;
	}

	/**
	 * Gets the value of the vgbel property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getVGBEL()
	{
		return vgbel;
	}

	/**
	 * Sets the value of the vgbel property.
	 * 
	 * @param value
	 *           allowed object is {@link String }
	 * 
	 */
	public void setVGBEL(String value)
	{
		this.vgbel = value;
	}

	/**
	 * Gets the value of the vgpos property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getVGPOS()
	{
		return vgpos;
	}

	/**
	 * Sets the value of the vgpos property.
	 * 
	 * @param value
	 *           allowed object is {@link String }
	 * 
	 */
	public void setVGPOS(String value)
	{
		this.vgpos = value;
	}

}
