
package com.jsl.cpi.invoice.batch.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>
 * Java class for ZHYB_BATCH_CHAR complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ZHYB_BATCH_CHAR">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CHARG" type="{urn:sap-com:document:sap:rfc:functions}char10"/>
 *         &lt;element name="ATNAM" type="{urn:sap-com:document:sap:rfc:functions}char30"/>
 *         &lt;element name="ATWTB" type="{urn:sap-com:document:sap:rfc:functions}char30"/>
 *         &lt;element name="MATNR" type="{urn:sap-com:document:sap:rfc:functions}char18"/>
 *         &lt;element name="VBELN" type="{urn:sap-com:document:sap:rfc:functions}char10"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ZHYB_BATCH_CHAR", propOrder =
{ "charg", "atnam", "atwtb", "matnr", "vbeln" })
public class ZHYBBATCHCHAR
{

	@XmlElement(name = "CHARG", required = true)
	protected String charg;
	@XmlElement(name = "ATNAM", required = true)
	protected String atnam;
	@XmlElement(name = "ATWTB", required = true)
	protected String atwtb;
	@XmlElement(name = "MATNR", required = true)
	protected String matnr;
	@XmlElement(name = "VBELN", required = true)
	protected String vbeln;

	/**
	 * Gets the value of the charg property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCHARG()
	{
		return charg;
	}

	/**
	 * Sets the value of the charg property.
	 * 
	 * @param value
	 *           allowed object is {@link String }
	 * 
	 */
	public void setCHARG(String value)
	{
		this.charg = value;
	}

	/**
	 * Gets the value of the atnam property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getATNAM()
	{
		return atnam;
	}

	/**
	 * Sets the value of the atnam property.
	 * 
	 * @param value
	 *           allowed object is {@link String }
	 * 
	 */
	public void setATNAM(String value)
	{
		this.atnam = value;
	}

	/**
	 * Gets the value of the atwtb property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getATWTB()
	{
		return atwtb;
	}

	/**
	 * Sets the value of the atwtb property.
	 * 
	 * @param value
	 *           allowed object is {@link String }
	 * 
	 */
	public void setATWTB(String value)
	{
		this.atwtb = value;
	}

	/**
	 * Gets the value of the matnr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMATNR()
	{
		return matnr;
	}

	/**
	 * Sets the value of the matnr property.
	 * 
	 * @param value
	 *           allowed object is {@link String }
	 * 
	 */
	public void setMATNR(String value)
	{
		this.matnr = value;
	}

	/**
	 * Gets the value of the vbeln property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getVBELN()
	{
		return vbeln;
	}

	/**
	 * Sets the value of the vbeln property.
	 * 
	 * @param value
	 *           allowed object is {@link String }
	 * 
	 */
	public void setVBELN(String value)
	{
		this.vbeln = value;
	}

}
