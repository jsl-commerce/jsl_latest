//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.05.10 at 06:37:01 PM IST 
//


package com.jsl.cpi.pop.mou.dto;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the pop.pojo package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: pop.pojo
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ZSDPOPVALUEFORCARTHYBRIS }
     * 
     */
    public ZSDPOPVALUEFORCARTHYBRIS createZSDPOPVALUEFORCARTHYBRIS() {
        return new ZSDPOPVALUEFORCARTHYBRIS();
    }

    /**
     * Create an instance of {@link ZSDPOPVALUEFORCARTHYBRISResponse }
     * 
     */
    public ZSDPOPVALUEFORCARTHYBRISResponse createZSDPOPVALUEFORCARTHYBRISResponse() {
        return new ZSDPOPVALUEFORCARTHYBRISResponse();
    }

}
