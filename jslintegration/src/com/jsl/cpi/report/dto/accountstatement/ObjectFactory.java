//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.05.04 at 06:28:43 PM IST 
//


package com.jsl.cpi.report.dto.accountstatement;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the accountstatementreport.pojo package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: accountstatementreport.pojo
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ZFI137HYBRIS }
     * 
     */
    public ZFI137HYBRIS createZFI137HYBRIS() {
        return new ZFI137HYBRIS();
    }

    /**
     * Create an instance of {@link ZTTFI37HYBRIS }
     * 
     */
    public ZTTFI37HYBRIS createZTTFI37HYBRIS() {
        return new ZTTFI37HYBRIS();
    }

    /**
     * Create an instance of {@link ZFI137HYBRISResponse }
     * 
     */
    public ZFI137HYBRISResponse createZFI137HYBRISResponse() {
        return new ZFI137HYBRISResponse();
    }

    /**
     * Create an instance of {@link ZTFI37HYBRIS }
     * 
     */
    public ZTFI37HYBRIS createZTFI37HYBRIS() {
        return new ZTFI37HYBRIS();
    }

}
