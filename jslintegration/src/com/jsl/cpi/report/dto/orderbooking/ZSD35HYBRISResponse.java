//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.04.15 at 01:06:20 PM IST 
//


package com.jsl.cpi.report.dto.orderbooking;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.jsl.cpi.report.dto.CPIResponse;


/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ZSD35_HYBRIS" type="{urn:sap-com:document:sap:rfc:functions}ZTT_ORDER_BOOKING_HYBRIS" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder =
{ "zsd35HYBRIS" })
@XmlRootElement(name = "ZSD35_HYBRISResponse")
public class ZSD35HYBRISResponse implements CPIResponse
{

	@XmlElement(name = "ZSD35_HYBRIS")
	protected ZTTORDERBOOKINGHYBRIS zsd35HYBRIS;

	/**
	 * Gets the value of the zsd35HYBRIS property.
	 * 
	 * @return possible object is {@link ZTTORDERBOOKINGHYBRIS }
	 * 
	 */
	public ZTTORDERBOOKINGHYBRIS getZSD35HYBRIS()
	{
		return zsd35HYBRIS;
	}

	/**
	 * Sets the value of the zsd35HYBRIS property.
	 * 
	 * @param value
	 *           allowed object is {@link ZTTORDERBOOKINGHYBRIS }
	 * 
	 */
	public void setZSD35HYBRIS(ZTTORDERBOOKINGHYBRIS value)
	{
		this.zsd35HYBRIS = value;
	}

}
