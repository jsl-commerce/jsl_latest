//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.04.15 at 01:07:50 PM IST 
//


package com.jsl.cpi.report.dto.sales;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.jsl.cpi.report.dto.CPIRequest;


/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CUST_C" type="{urn:sap-com:document:sap:rfc:functions}char10" minOccurs="0"/&gt;
 *         &lt;element name="ZSD34_HYBRIS" type="{urn:sap-com:document:sap:rfc:functions}ZTT_SALES_ACCOUNT_HYBRIS" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder =
{ "custc", "zsd34HYBRIS" })
@XmlRootElement(name = "ZSD34_HYBRIS")
public class ZSD34HYBRIS implements CPIRequest
{

	@XmlElement(name = "CUST_C")
	protected String custc;
	@XmlElement(name = "ZSD34_HYBRIS")
	protected ZTTSALESACCOUNTHYBRIS zsd34HYBRIS;

	/**
	 * Gets the value of the custc property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCUSTC()
	{
		return custc;
	}

	/**
	 * Sets the value of the custc property.
	 * 
	 * @param value
	 *           allowed object is {@link String }
	 * 
	 */
	public void setCUSTC(String value)
	{
		this.custc = value;
	}

	/**
	 * Gets the value of the zsd34HYBRIS property.
	 * 
	 * @return possible object is {@link ZTTSALESACCOUNTHYBRIS }
	 * 
	 */
	public ZTTSALESACCOUNTHYBRIS getZSD34HYBRIS()
	{
		return zsd34HYBRIS;
	}

	/**
	 * Sets the value of the zsd34HYBRIS property.
	 * 
	 * @param value
	 *           allowed object is {@link ZTTSALESACCOUNTHYBRIS }
	 * 
	 */
	public void setZSD34HYBRIS(ZTTSALESACCOUNTHYBRIS value)
	{
		this.zsd34HYBRIS = value;
	}

}
