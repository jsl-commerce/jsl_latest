//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-147 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.02.25 at 04:03:12 PM IST 
//


package com.jsl.cpi.report.dto.vehicle.tracking;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.jsl.cpi.report.dto.CPIResponse;


/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IT_VEH_TACK" type="{urn:sap-com:document:sap:rfc:functions}ZSD_VEH_TRACK_TT"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder =
{ "itvehtack" })
@XmlRootElement(name = "ZSD_TRACK_DEL_HYBRISResponse")
public class ZSDTRACKDELHYBRISResponse implements CPIResponse
{

	@XmlElement(name = "IT_VEH_TACK", required = true)
	protected ZSDVEHTRACKTT itvehtack;

	/**
	 * Gets the value of the itvehtack property.
	 * 
	 * @return possible object is {@link ZSDVEHTRACKTT }
	 * 
	 */
	public ZSDVEHTRACKTT getITVEHTACK()
	{
		return itvehtack;
	}

	/**
	 * Sets the value of the itvehtack property.
	 * 
	 * @param value
	 *           allowed object is {@link ZSDVEHTRACKTT }
	 * 
	 */
	public void setITVEHTACK(ZSDVEHTRACKTT value)
	{
		this.itvehtack = value;
	}

}
