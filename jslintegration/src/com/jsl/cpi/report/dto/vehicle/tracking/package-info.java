/*
 * [y] hybris Platform
 * 
 * Copyright (c) 2000-2020 SAP SE
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of SAP 
 * Hybris ("Confidential Information"). You shall not disclose such 
 * Confidential Information and shall use it only in accordance with the 
 * terms of the license agreement you entered into with SAP Hybris.
 */
/**
 *
 */
@javax.xml.bind.annotation.XmlSchema(namespace = "urn:sap-com:document:sap:rfc:functions")
package com.jsl.cpi.report.dto.vehicle.tracking;