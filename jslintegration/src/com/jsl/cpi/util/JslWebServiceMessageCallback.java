/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.cpi.util;

import de.hybris.platform.util.Config;

import java.io.IOException;
import java.util.Base64;

import javax.xml.transform.TransformerException;

import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.transport.context.TransportContext;
import org.springframework.ws.transport.context.TransportContextHolder;
import org.springframework.ws.transport.http.HttpUrlConnection;


/**
 *
 */
public class JslWebServiceMessageCallback implements WebServiceMessageCallback
{

	private static String AUTHORIZATION = "Authorization";
	private static String REQUEST_USER_KEY = "soap.request.user";
	private static String REQUEST_PASSWORD_KEY = "soap.request.password";

	@Override
	public void doWithMessage(final WebServiceMessage arg0) throws IOException, TransformerException
	{
		// XXX Auto-generated method stub
		final TransportContext context = TransportContextHolder.getTransportContext();
		final HttpUrlConnection connection = (HttpUrlConnection) context.getConnection();
		connection.getConnection().addRequestProperty(AUTHORIZATION,
				generateBasicAutenticationHeader(Config.getParameter(REQUEST_USER_KEY), Config.getParameter(REQUEST_PASSWORD_KEY)));

	}

	private String generateBasicAutenticationHeader(final String userName, final String userPassword)
	{
		final byte[] encodedBytes = Base64.getEncoder().encode((userName + ":" + userPassword).getBytes());
		return "Basic " + new String(encodedBytes);
	}


}
