/*
 * [y] hybris Platform
 * 
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of SAP 
 * Hybris ("Confidential Information"). You shall not disclose such 
 * Confidential Information and shall use it only in accordance with the 
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.payment.dto;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 *
 */
public class SecurityTokenResponseDto
{

	@JsonProperty("access_token")
	private String accessToken;

	@JsonProperty("token_type")
	private String tokenType;

	@JsonProperty("refresh_token")
	private String refreshToken;

	@JsonProperty("expires_in")
	private String expiresIn;

	@JsonProperty("scope")
	private String scope;

	/**
	 * @return the accessToken
	 */
	@JsonProperty("access_token")
	public String getAccessToken()
	{
		return accessToken;
	}

	/**
	 * @param accessToken
	 *           the accessToken to set
	 */
	@JsonProperty("access_token")
	public void setAccessToken(String accessToken)
	{
		this.accessToken = accessToken;
	}

	/**
	 * @return the tokenType
	 */
	public String getTokenType()
	{
		return tokenType;
	}

	/**
	 * @param tokenType
	 *           the tokenType to set
	 */
	public void setTokenType(String tokenType)
	{
		this.tokenType = tokenType;
	}

	/**
	 * @return the refreshToken
	 */
	public String getRefreshToken()
	{
		return refreshToken;
	}

	/**
	 * @param refreshToken
	 *           the refreshToken to set
	 */
	public void setRefreshToken(String refreshToken)
	{
		this.refreshToken = refreshToken;
	}

	/**
	 * @return the expiresIn
	 */
	public String getExpiresIn()
	{
		return expiresIn;
	}

	/**
	 * @param expiresIn
	 *           the expiresIn to set
	 */
	public void setExpiresIn(String expiresIn)
	{
		this.expiresIn = expiresIn;
	}

	/**
	 * @return the scope
	 */
	public String getScope()
	{
		return scope;
	}

	/**
	 * @param scope
	 *           the scope to set
	 */
	public void setScope(String scope)
	{
		this.scope = scope;
	}

}
