/*
 * [y] hybris Platform
 * 
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of SAP 
 * Hybris ("Confidential Information"). You shall not disclose such 
 * Confidential Information and shall use it only in accordance with the 
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.jslintegration.service.impl;

import de.hybris.platform.sap.sapinvoiceaddon.model.SapB2BDocumentModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.Config;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;

import com.jsl.core.enums.JslPaymentStatus;
import com.jsl.core.model.JslPaymentTransactionModel;
import com.jsl.core.model.PaymentSaleOrderModel;
import com.jsl.facades.payment.dto.TransactionMasterDto;
import com.jsl.facades.paymentdetailshistory.dto.PaymentDetailsHistoryDto;
import com.jsl.jslintegration.dao.JslPaymentDao;
import com.jsl.jslintegration.payment.dto.JslEccPaymentHybrisRequest;
import com.jsl.jslintegration.payment.dto.JslEccPaymentItemRequest;
import com.jsl.jslintegration.payment.dto.JslEccPaymentRequest;
import com.jsl.jslintegration.payment.dto.JslEccPaymentResponse;
import com.jsl.jslintegration.payment.dto.JslPaymentResponse;
import com.jsl.jslintegration.service.JslPaymentResponseService;
import com.jsl.rest.service.JslRestService;


/**
 *
 */
public class JslPaymentResponseServiceImpl implements JslPaymentResponseService
{
	private static final Logger LOG = Logger.getLogger(JslPaymentResponseServiceImpl.class);
	private JslPaymentDao jslPaymentDao;

	private ModelService modelService;

	private static final String FAIL = "FAILURE";

	private static final String SUCCESS = "SUCCESS";

	private static final String INV = "INV";

	private static final String ADV = "ADV";

	private static final String COLUN = ":";

	private static final String ECC_PAYMENT_URL_KEY = "payment.ecc.url";

	private static DateTimeFormatter invoicePostingDateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	private static DateTimeFormatter saleOrderPostingDateFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");

	private static DateTimeFormatter postingDateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	private static DateTimeFormatter paymentDateFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");

	private static DateTimeFormatter requiredDateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

	private JslRestService jslRestService;

	private Converter<JslPaymentTransactionModel, TransactionMasterDto> transactionDetailsConverter;

	@Override
	public void processPaymentReponse(JslPaymentResponse jslPaymentResponse) throws Exception
	{
		// XXX Auto-generated method stub
		if (jslPaymentResponse.getJsl_ref_no().startsWith(INV))
		{
			this.saveInvoicePaymentStatus(jslPaymentResponse);
		}
		else
		{
			this.saveAdvancePaymentStatus(jslPaymentResponse);
		}
		this.saveTransactionStatus(jslPaymentResponse);

	}

	private boolean sendTransactionDataToEcc(JslPaymentTransactionModel jslPaymentTransactionModel)
	{
		boolean result = false;
		TransactionMasterDto transactionMasterDto = transactionDetailsConverter.convert(jslPaymentTransactionModel);

		JslEccPaymentRequest request = createEccPaymentRequest(transactionMasterDto);

		try
		{
			JslEccPaymentResponse response = jslRestService.postForObjectCommand(Config.getParameter(ECC_PAYMENT_URL_KEY), request,
					JslEccPaymentResponse.class, transactionMasterDto.getJslRefNumber(), transactionMasterDto.getCustomerNumber(),
					MediaType.APPLICATION_ATOM_XML);

			if (response != null)
			{
				result = true;
			}
		}
		catch (Exception e)
		{
			LOG.error("Error in posting transaction to ECC : " + e);
		}

		return result;
	}

	private JslEccPaymentRequest createEccPaymentRequest(TransactionMasterDto transactionMasterDto)
	{
		JslEccPaymentRequest jslEccPaymentRequest = new JslEccPaymentRequest();
		JslEccPaymentHybrisRequest jslEccPaymentHybrisRequest = new JslEccPaymentHybrisRequest();
		List<JslEccPaymentItemRequest> jslEccPaymentItemRequestList = new ArrayList<JslEccPaymentItemRequest>();
		jslEccPaymentItemRequestList.addAll(createJslPaymentItemRequest(transactionMasterDto));
		jslEccPaymentHybrisRequest.setJslEccPaymentItemRequest(jslEccPaymentItemRequestList);
		jslEccPaymentRequest.setJslEccPaymentHybrisRequest(jslEccPaymentHybrisRequest);
		return jslEccPaymentRequest;
	}

	private List<JslEccPaymentItemRequest> createJslPaymentItemRequest(TransactionMasterDto transactionMasterDto)
	{

		List<JslEccPaymentItemRequest> list = new ArrayList<JslEccPaymentItemRequest>();

		if ("N".equalsIgnoreCase(transactionMasterDto.getPaymentTypeAdv()))
		{
			for (PaymentDetailsHistoryDto invoice : transactionMasterDto.getInvoiceList())
			{
				JslEccPaymentItemRequest jslEccPaymentItemRequest = new JslEccPaymentItemRequest();
				jslEccPaymentItemRequest.setBusArea(invoice.getBusArea());
				jslEccPaymentItemRequest.setFiDocumentNumber(invoice.getDocumentNumber());
				jslEccPaymentItemRequest.setFiscalYear(invoice.getFiscalYear());
				jslEccPaymentItemRequest.setGstInvoiceNumber(invoice.getGstInvoiceNumber());
				jslEccPaymentItemRequest.setInvoiceAmount(String.format("%.2f", Double.parseDouble(invoice.getGrandTotal())));
				jslEccPaymentItemRequest.setInvoiceNumbers(invoice.getInvoiceNumber());
				jslEccPaymentItemRequest.setSaleOrderNumber(invoice.getOrderNumber());
				jslEccPaymentItemRequest.setPoNumber(invoice.getOurTaxNumber());

				jslEccPaymentItemRequest.setPstngDate((!StringUtils.isEmpty(invoice.getInvoiceDate())
						? postingDateFormatter.format(invoicePostingDateFormatter.parse(invoice.getInvoiceDate()))
						: StringUtils.EMPTY));
				list.add(jslEccPaymentItemRequest);

			}
		}
		else
		{
			JslEccPaymentItemRequest jslEccPaymentItemRequest = new JslEccPaymentItemRequest();

			jslEccPaymentItemRequest.setBusArea(StringUtils.EMPTY);
			jslEccPaymentItemRequest.setFiDocumentNumber(StringUtils.EMPTY);
			jslEccPaymentItemRequest.setFiscalYear(StringUtils.EMPTY);
			jslEccPaymentItemRequest.setGstInvoiceNumber(StringUtils.EMPTY);
			jslEccPaymentItemRequest.setInvoiceAmount(StringUtils.EMPTY);
			jslEccPaymentItemRequest.setInvoiceNumbers(StringUtils.EMPTY);
			jslEccPaymentItemRequest.setSaleOrderNumber(transactionMasterDto.getSaleOrder().getSaleOrderNumber());
			jslEccPaymentItemRequest.setPoNumber(transactionMasterDto.getSaleOrder().getPurchaseOrderNumber());
			jslEccPaymentItemRequest.setPstngDate((!StringUtils.isEmpty(transactionMasterDto.getSaleOrder().getSaleOrderDate())
					? postingDateFormatter
							.format(saleOrderPostingDateFormatter.parse(transactionMasterDto.getSaleOrder().getSaleOrderDate()))
					: StringUtils.EMPTY));
			list.add(jslEccPaymentItemRequest);

		}


		for (JslEccPaymentItemRequest jslEccPaymentItemRequest : list)
		{
			jslEccPaymentItemRequest.setCompanyCode(transactionMasterDto.getCompanyCode());
			jslEccPaymentItemRequest.setCustomerCode(transactionMasterDto.getCustomerNumber());
			jslEccPaymentItemRequest
					.setJslRefAmount(String.format("%.2f", Double.parseDouble(transactionMasterDto.getJslRefAmount())));
			jslEccPaymentItemRequest.setJslRefNumber(transactionMasterDto.getJslRefNumber());

			jslEccPaymentItemRequest.setPaymentMode(transactionMasterDto.getPaymentMode());
			jslEccPaymentItemRequest.setPaymentSource(transactionMasterDto.getPaymentSource());
			jslEccPaymentItemRequest.setPaymentTypeAdv(transactionMasterDto.getPaymentTypeAdv());

			jslEccPaymentItemRequest.setBankAccountNumber(transactionMasterDto.getBankAccNumber());

			jslEccPaymentItemRequest.setSbiRefNumber(transactionMasterDto.getSbiRefNumber());
			jslEccPaymentItemRequest.setPaymentMode(StringUtils.isEmpty(transactionMasterDto.getPaymentMode()) ? StringUtils.EMPTY
					: transactionMasterDto.getPaymentMode());

			jslEccPaymentItemRequest
					.setPaymentDate(requiredDateFormatter.format(paymentDateFormatter.parse(transactionMasterDto.getPaymentDate())));

			//Start: Always Constant and blank
			jslEccPaymentItemRequest.setMandt("366");
			jslEccPaymentItemRequest.setName1(StringUtils.EMPTY);
			jslEccPaymentItemRequest.setClear(StringUtils.EMPTY);
			jslEccPaymentItemRequest.setShkzg(StringUtils.EMPTY);
			jslEccPaymentItemRequest.setPaymentNo(StringUtils.EMPTY);
			jslEccPaymentItemRequest.setPostDate(StringUtils.EMPTY);
		}
		return list;
	}

	private void saveTransactionStatus(JslPaymentResponse jslPaymentResponse) throws Exception
	{
		List<JslPaymentTransactionModel> documentList = jslPaymentDao.getTransactionModels(jslPaymentResponse.getJsl_ref_no());

		for (JslPaymentTransactionModel document : documentList)
		{
			document.setSbiRefNumber(jslPaymentResponse.getSbi_ref_no());
			document.setPaymentStatus(jslPaymentResponse.getStatus());
			document.setPaymentStatusDescription(jslPaymentResponse.getStatus_desc());
			document.setPaymentMode(jslPaymentResponse.getPay_mode());

			if (FAIL.equalsIgnoreCase(jslPaymentResponse.getStatus()))
			{
				document.setHybrisPaymentStatus(JslPaymentStatus.FAILED);
			}
			else if (SUCCESS.equalsIgnoreCase(jslPaymentResponse.getStatus()))
			{
				document.setHybrisPaymentStatus(JslPaymentStatus.SUCCESS);
				if (sendTransactionDataToEcc(document))
				{
					document.setPostedToEcc(true);
				}
			}
			else
			{
				document.setHybrisPaymentStatus(JslPaymentStatus.PENDING);
				if (document.getPendingStatusCounter() == 0)
				{
					document.setPendingPaymentDate(Calendar.getInstance().getTime());
				}
				document.setPendingStatusCounter(document.getPendingStatusCounter() + 1);
			}
		}
		modelService.saveAll(documentList);
	}

	private void saveAdvancePaymentStatus(JslPaymentResponse jslPaymentResponse) throws Exception
	{
		List<PaymentSaleOrderModel> documentList = jslPaymentDao
				.getSaleOrderPaymentDetailsFromJslRefNumber(jslPaymentResponse.getJsl_ref_no());

		for (PaymentSaleOrderModel document : documentList)
		{
			document.setSbiRefNumber(jslPaymentResponse.getSbi_ref_no());
			document.setPaymentStatus(jslPaymentResponse.getStatus());
			document.setPaymentStatusDescription(jslPaymentResponse.getStatus_desc());

			if (FAIL.equalsIgnoreCase(jslPaymentResponse.getStatus()))
			{
				document.setHybrisPaymentStatus(JslPaymentStatus.FAILED);
			}
			else if (SUCCESS.equalsIgnoreCase(jslPaymentResponse.getStatus()))
			{
				document.setHybrisPaymentStatus(JslPaymentStatus.SUCCESS);
			}
			else
			{
				document.setHybrisPaymentStatus(JslPaymentStatus.PENDING);
			}
		}
		modelService.saveAll(documentList);
	}

	private void saveInvoicePaymentStatus(JslPaymentResponse jslPaymentResponse) throws Exception
	{
		List<SapB2BDocumentModel> documentList = jslPaymentDao
				.getInvoicePaymentDetailsFromJslRefNumber(jslPaymentResponse.getJsl_ref_no());

		for (SapB2BDocumentModel document : documentList)
		{
			document.setSbiReferenceNumber(jslPaymentResponse.getSbi_ref_no());
			document.setPaymentStatus(jslPaymentResponse.getStatus());
			document.setPaymentStatusDescription(jslPaymentResponse.getStatus_desc());

			if (FAIL.equalsIgnoreCase(jslPaymentResponse.getStatus()))
			{
				document.setHybrisPaymentStatus(JslPaymentStatus.FAILED);
			}
			else if (SUCCESS.equalsIgnoreCase(jslPaymentResponse.getStatus()))
			{
				document.setHybrisPaymentStatus(JslPaymentStatus.SUCCESS);
			}
			else
			{
				document.setHybrisPaymentStatus(JslPaymentStatus.PENDING);
			}
		}
		modelService.saveAll(documentList);
	}

	/**
	 * @return the jslPaymentDao
	 */
	public JslPaymentDao getJslPaymentDao()
	{
		return jslPaymentDao;
	}

	/**
	 * @param jslPaymentDao
	 *           the jslPaymentDao to set
	 */
	public void setJslPaymentDao(JslPaymentDao jslPaymentDao)
	{
		this.jslPaymentDao = jslPaymentDao;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * @return the transactionDetailsConverter
	 */
	public Converter<JslPaymentTransactionModel, TransactionMasterDto> getTransactionDetailsConverter()
	{
		return transactionDetailsConverter;
	}

	/**
	 * @param transactionDetailsConverter
	 *           the transactionDetailsConverter to set
	 */
	public void setTransactionDetailsConverter(
			Converter<JslPaymentTransactionModel, TransactionMasterDto> transactionDetailsConverter)
	{
		this.transactionDetailsConverter = transactionDetailsConverter;
	}

	/**
	 * @return the jslRestService
	 */
	public JslRestService getJslRestService()
	{
		return jslRestService;
	}

	/**
	 * @param jslRestService
	 *           the jslRestService to set
	 */
	public void setJslRestService(JslRestService jslRestService)
	{
		this.jslRestService = jslRestService;
	}

}
