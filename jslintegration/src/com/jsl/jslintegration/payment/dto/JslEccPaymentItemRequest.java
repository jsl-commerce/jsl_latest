/*
 * [y] hybris Platform
 * 
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of SAP 
 * Hybris ("Confidential Information"). You shall not disclose such 
 * Confidential Information and shall use it only in accordance with the 
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.jslintegration.payment.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 *
 */
@XmlRootElement(name = "item")
@XmlAccessorType(XmlAccessType.FIELD)
public class JslEccPaymentItemRequest
{

	@XmlElement(name = "MANDT") //Always fixed to 366
	private String mandt;
	@XmlElement(name = "BUKRS")
	private String companyCode;
	@XmlElement(name = "KUNNR")
	private String customerCode;
	@XmlElement(name = "BELNR")
	private String invoiceNumbers;
	@XmlElement(name = "NETWR")
	private String jslRefAmount;
	@XmlElement(name = "VBELN")
	private String saleOrderNumber;
	@XmlElement(name = "SHKZG") //Always Fixed S for invoice
	private String shkzg;
	@XmlElement(name = "REMARK")
	private String jslRefNumber;
	@XmlElement(name = "NAME1")
	private String name1;
	@XmlElement(name = "TRANSID")
	private String paymentSource;
	@XmlElement(name = "UTRNO")
	private String sbiRefNumber;
	@XmlElement(name = "BANK_DATE")
	private String paymentDate;
	@XmlElement(name = "BANK_ACCNO")
	private String bankAccountNumber;
	@XmlElement(name = "CLEAR")
	private String clear;
	@XmlElement(name = "PAYMENT_NO")
	private String paymentNo;
	@XmlElement(name = "POST_DATE")
	private String postDate;
	@XmlElement(name = "PAYMENT_TYPE")
	private String paymentMode;
	@XmlElement(name = "GST_INV_NUMBER")
	private String gstInvoiceNumber;
	@XmlElement(name = "FI_DOC_NUMBER")
	private String fiDocumentNumber;
	@XmlElement(name = "PO_NO")
	private String poNumber;
	@XmlElement(name = "INVOICE_AMOUNT")
	private String invoiceAmount;
	@XmlElement(name = "GJAHR")
	private String fiscalYear;
	@XmlElement(name = "PMT_TYPE_ADV")
	private String paymentTypeAdv;
	@XmlElement(name = "PSTNG_DATE")
	private String pstngDate;
	@XmlElement(name = "BUS_AREA")
	private String busArea;

	/**
	 * @return the mandt
	 */
	public String getMandt()
	{
		return mandt;
	}

	/**
	 * @param mandt
	 *           the mandt to set
	 */
	public void setMandt(String mandt)
	{
		this.mandt = mandt;
	}

	/**
	 * @return the companyCode
	 */
	public String getCompanyCode()
	{
		return companyCode;
	}

	/**
	 * @param companyCode
	 *           the companyCode to set
	 */
	public void setCompanyCode(String companyCode)
	{
		this.companyCode = companyCode;
	}

	/**
	 * @return the customerCode
	 */
	public String getCustomerCode()
	{
		return customerCode;
	}

	/**
	 * @param customerCode
	 *           the customerCode to set
	 */
	public void setCustomerCode(String customerCode)
	{
		this.customerCode = customerCode;
	}

	/**
	 * @return the invoiceNumbers
	 */
	public String getInvoiceNumbers()
	{
		return invoiceNumbers;
	}

	/**
	 * @param invoiceNumbers
	 *           the invoiceNumbers to set
	 */
	public void setInvoiceNumbers(String invoiceNumbers)
	{
		this.invoiceNumbers = invoiceNumbers;
	}

	/**
	 * @return the jslRefAmount
	 */
	public String getJslRefAmount()
	{
		return jslRefAmount;
	}

	/**
	 * @param jslRefAmount
	 *           the jslRefAmount to set
	 */
	public void setJslRefAmount(String jslRefAmount)
	{
		this.jslRefAmount = jslRefAmount;
	}

	/**
	 * @return the saleOrderNumber
	 */
	public String getSaleOrderNumber()
	{
		return saleOrderNumber;
	}

	/**
	 * @param saleOrderNumber
	 *           the saleOrderNumber to set
	 */
	public void setSaleOrderNumber(String saleOrderNumber)
	{
		this.saleOrderNumber = saleOrderNumber;
	}

	/**
	 * @return the shkzg
	 */
	public String getShkzg()
	{
		return shkzg;
	}

	/**
	 * @param shkzg
	 *           the shkzg to set
	 */
	public void setShkzg(String shkzg)
	{
		this.shkzg = shkzg;
	}

	/**
	 * @return the jslRefNumber
	 */
	public String getJslRefNumber()
	{
		return jslRefNumber;
	}

	/**
	 * @param jslRefNumber
	 *           the jslRefNumber to set
	 */
	public void setJslRefNumber(String jslRefNumber)
	{
		this.jslRefNumber = jslRefNumber;
	}

	/**
	 * @return the name1
	 */
	public String getName1()
	{
		return name1;
	}

	/**
	 * @param name1
	 *           the name1 to set
	 */
	public void setName1(String name1)
	{
		this.name1 = name1;
	}

	/**
	 * @return the paymentSource
	 */
	public String getPaymentSource()
	{
		return paymentSource;
	}

	/**
	 * @param paymentSource
	 *           the paymentSource to set
	 */
	public void setPaymentSource(String paymentSource)
	{
		this.paymentSource = paymentSource;
	}

	/**
	 * @return the sbiRefNumber
	 */
	public String getSbiRefNumber()
	{
		return sbiRefNumber;
	}

	/**
	 * @param sbiRefNumber
	 *           the sbiRefNumber to set
	 */
	public void setSbiRefNumber(String sbiRefNumber)
	{
		this.sbiRefNumber = sbiRefNumber;
	}

	/**
	 * @return the paymentDate
	 */
	public String getPaymentDate()
	{
		return paymentDate;
	}

	/**
	 * @param paymentDate
	 *           the paymentDate to set
	 */
	public void setPaymentDate(String paymentDate)
	{
		this.paymentDate = paymentDate;
	}

	/**
	 * @return the bankAccountNumber
	 */
	public String getBankAccountNumber()
	{
		return bankAccountNumber;
	}

	/**
	 * @param bankAccountNumber
	 *           the bankAccountNumber to set
	 */
	public void setBankAccountNumber(String bankAccountNumber)
	{
		this.bankAccountNumber = bankAccountNumber;
	}

	/**
	 * @return the clear
	 */
	public String getClear()
	{
		return clear;
	}

	/**
	 * @param clear
	 *           the clear to set
	 */
	public void setClear(String clear)
	{
		this.clear = clear;
	}

	/**
	 * @return the paymentNo
	 */
	public String getPaymentNo()
	{
		return paymentNo;
	}

	/**
	 * @param paymentNo
	 *           the paymentNo to set
	 */
	public void setPaymentNo(String paymentNo)
	{
		this.paymentNo = paymentNo;
	}

	/**
	 * @return the postDate
	 */
	public String getPostDate()
	{
		return postDate;
	}

	/**
	 * @param postDate
	 *           the postDate to set
	 */
	public void setPostDate(String postDate)
	{
		this.postDate = postDate;
	}

	/**
	 * @return the paymentMode
	 */
	public String getPaymentMode()
	{
		return paymentMode;
	}

	/**
	 * @param paymentMode
	 *           the paymentMode to set
	 */
	public void setPaymentMode(String paymentMode)
	{
		this.paymentMode = paymentMode;
	}

	/**
	 * @return the gstInvoiceNumber
	 */
	public String getGstInvoiceNumber()
	{
		return gstInvoiceNumber;
	}

	/**
	 * @param gstInvoiceNumber
	 *           the gstInvoiceNumber to set
	 */
	public void setGstInvoiceNumber(String gstInvoiceNumber)
	{
		this.gstInvoiceNumber = gstInvoiceNumber;
	}

	/**
	 * @return the fiDocumentNumber
	 */
	public String getFiDocumentNumber()
	{
		return fiDocumentNumber;
	}

	/**
	 * @param fiDocumentNumber
	 *           the fiDocumentNumber to set
	 */
	public void setFiDocumentNumber(String fiDocumentNumber)
	{
		this.fiDocumentNumber = fiDocumentNumber;
	}

	/**
	 * @return the poNumber
	 */
	public String getPoNumber()
	{
		return poNumber;
	}

	/**
	 * @param poNumber
	 *           the poNumber to set
	 */
	public void setPoNumber(String poNumber)
	{
		this.poNumber = poNumber;
	}

	/**
	 * @return the invoiceAmount
	 */
	public String getInvoiceAmount()
	{
		return invoiceAmount;
	}

	/**
	 * @param invoiceAmount
	 *           the invoiceAmount to set
	 */
	public void setInvoiceAmount(String invoiceAmount)
	{
		this.invoiceAmount = invoiceAmount;
	}

	/**
	 * @return the fiscalYear
	 */
	public String getFiscalYear()
	{
		return fiscalYear;
	}

	/**
	 * @param fiscalYear
	 *           the fiscalYear to set
	 */
	public void setFiscalYear(String fiscalYear)
	{
		this.fiscalYear = fiscalYear;
	}

	/**
	 * @return the paymentTypeAdv
	 */
	public String getPaymentTypeAdv()
	{
		return paymentTypeAdv;
	}

	/**
	 * @param paymentTypeAdv
	 *           the paymentTypeAdv to set
	 */
	public void setPaymentTypeAdv(String paymentTypeAdv)
	{
		this.paymentTypeAdv = paymentTypeAdv;
	}

	/**
	 * @return the pstngDate
	 */
	public String getPstngDate()
	{
		return pstngDate;
	}

	/**
	 * @param pstngDate
	 *           the pstngDate to set
	 */
	public void setPstngDate(String pstngDate)
	{
		this.pstngDate = pstngDate;
	}

	/**
	 * @return the busArea
	 */
	public String getBusArea()
	{
		return busArea;
	}

	/**
	 * @param busArea
	 *           the busArea to set
	 */
	public void setBusArea(String busArea)
	{
		this.busArea = busArea;
	}


}
