/*
 * [y] hybris Platform
 * 
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of SAP 
 * Hybris ("Confidential Information"). You shall not disclose such 
 * Confidential Information and shall use it only in accordance with the 
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.jslintegration.payment.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 *
 */
@XmlRootElement(name = "IT_HYBRIS")
@XmlAccessorType(XmlAccessType.FIELD)
public class JslEccPaymentHybrisRequest
{

	@XmlElement(name = "item")
	private List<JslEccPaymentItemRequest> jslEccPaymentItemRequest;

	/**
	 * @return the jslEccPaymentItemRequest
	 */
	public List<JslEccPaymentItemRequest> getJslEccPaymentItemRequest()
	{
		return jslEccPaymentItemRequest;
	}

	/**
	 * @param jslEccPaymentItemRequest
	 *           the jslEccPaymentItemRequest to set
	 */
	public void setJslEccPaymentItemRequest(List<JslEccPaymentItemRequest> jslEccPaymentItemRequest)
	{
		this.jslEccPaymentItemRequest = jslEccPaymentItemRequest;
	}

}
