/*
 * [y] hybris Platform
 * 
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of SAP 
 * Hybris ("Confidential Information"). You shall not disclose such 
 * Confidential Information and shall use it only in accordance with the 
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.jslintegration.payment.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 *
 */
@XmlRootElement(name = "ZFI_HYBRIS_PAYMENT")
@XmlAccessorType(XmlAccessType.FIELD)
public class JslEccPaymentRequest
{

	@XmlElement(name = "IT_HYBRIS")
	private JslEccPaymentHybrisRequest jslEccPaymentHybrisRequest;

	/**
	 * @return the jslEccPaymentHybrisRequest
	 */
	public JslEccPaymentHybrisRequest getJslEccPaymentHybrisRequest()
	{
		return jslEccPaymentHybrisRequest;
	}

	/**
	 * @param jslEccPaymentHybrisRequest
	 *           the jslEccPaymentHybrisRequest to set
	 */
	public void setJslEccPaymentHybrisRequest(JslEccPaymentHybrisRequest jslEccPaymentHybrisRequest)
	{
		this.jslEccPaymentHybrisRequest = jslEccPaymentHybrisRequest;
	}

}
