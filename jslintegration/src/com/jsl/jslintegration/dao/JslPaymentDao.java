/*
 * [y] hybris Platform
 * 
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of SAP 
 * Hybris ("Confidential Information"). You shall not disclose such 
 * Confidential Information and shall use it only in accordance with the 
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.jslintegration.dao;

import de.hybris.platform.sap.sapinvoiceaddon.model.SapB2BDocumentModel;

import java.util.List;

import com.jsl.core.model.JslPaymentTransactionModel;
import com.jsl.core.model.PaymentSaleOrderModel;


/**
 *
 */
public interface JslPaymentDao
{
	public List<SapB2BDocumentModel> getInvoicePaymentDetailsFromJslRefNumber(String jslRefNumber) throws Exception;

	public List<PaymentSaleOrderModel> getSaleOrderPaymentDetailsFromJslRefNumber(String jslRefNumber) throws Exception;

	public List<JslPaymentTransactionModel> getTransactionModels(String jslRefNumber) throws Exception;
}
