/*
 * [y] hybris Platform
 * 
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of SAP 
 * Hybris ("Confidential Information"). You shall not disclose such 
 * Confidential Information and shall use it only in accordance with the 
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.jslintegration.dao.impl;

import de.hybris.platform.sap.sapinvoiceaddon.model.SapB2BDocumentModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.List;

import com.jsl.core.model.JslPaymentTransactionModel;
import com.jsl.core.model.PaymentSaleOrderModel;
import com.jsl.jslintegration.dao.JslPaymentDao;


/**
 *
 */
public class JslPaymentDaoImpl implements JslPaymentDao
{

	private FlexibleSearchService flexibleSearchService;
	private SessionService sessionService;
	private UserService userService;

	private static final String GET_PAYMENT_DETAILS_FROM_JSL_REF_NUMBER = "SELECT {" + SapB2BDocumentModel.PK + "} FROM {"
			+ SapB2BDocumentModel._TYPECODE + "} WHERE {" + SapB2BDocumentModel.JSLREFERENCENUMBER + "} = ?jslrefnumber";

	private static final String GET_SALE_ORDER_PAYMENT_DETAILS_FROM_JSL_REF_NUMBER = "SELECT {" + PaymentSaleOrderModel.PK
			+ "} FROM {" + PaymentSaleOrderModel._TYPECODE + "} WHERE {" + PaymentSaleOrderModel.JSLREFNUMBER + "} = ?jslrefnumber";

	private static final String GET_TRANSACTION_DETAILS_FROM_JSL_REF_NUMBER = "SELECT {" + JslPaymentTransactionModel.PK
			+ "} FROM {" + JslPaymentTransactionModel._TYPECODE + "} WHERE {" + JslPaymentTransactionModel.JSLREFNUMBER
			+ "} = ?jslrefnumber";


	@Override
	public List<JslPaymentTransactionModel> getTransactionModels(String jslRefNumber) throws Exception
	{
		// XXX Auto-generated method stub
		return sessionService.executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public Object execute()
			{
				final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_TRANSACTION_DETAILS_FROM_JSL_REF_NUMBER);
				query.addQueryParameter("jslrefnumber", jslRefNumber);
				final SearchResult<JslPaymentTransactionModel> result = getFlexibleSearchService().search(query);

				return result.getResult();
			}
		}, userService.getAdminUser());
	}

	@Override
	public List<PaymentSaleOrderModel> getSaleOrderPaymentDetailsFromJslRefNumber(String jslRefNumber) throws Exception
	{
		// XXX Auto-generated method stub
		return sessionService.executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public Object execute()
			{
				final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_SALE_ORDER_PAYMENT_DETAILS_FROM_JSL_REF_NUMBER);
				query.addQueryParameter("jslrefnumber", jslRefNumber);
				final SearchResult<PaymentSaleOrderModel> result = getFlexibleSearchService().search(query);

				return result.getResult();
			}
		}, userService.getAdminUser());
	}

	@Override
	public List<SapB2BDocumentModel> getInvoicePaymentDetailsFromJslRefNumber(String jslRefNumber) throws Exception
	{
		// XXX Auto-generated method stub
		return sessionService.executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public Object execute()
			{
				final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_PAYMENT_DETAILS_FROM_JSL_REF_NUMBER);
				query.addQueryParameter("jslrefnumber", jslRefNumber);
				final SearchResult<SapB2BDocumentModel> result = getFlexibleSearchService().search(query);

				return result.getResult();
			}
		}, userService.getAdminUser());
	}

	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	/**
	 * @param flexibleSearchService
	 *           the flexibleSearchService to set
	 */
	public void setFlexibleSearchService(FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

	/**
	 * @return the sessionService
	 */
	public SessionService getSessionService()
	{
		return sessionService;
	}

	/**
	 * @param sessionService
	 *           the sessionService to set
	 */
	public void setSessionService(SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(UserService userService)
	{
		this.userService = userService;
	}

}
