/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.jsl.setup;

import static com.jsl.constants.JslintegrationConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import com.jsl.constants.JslintegrationConstants;
import com.jsl.service.JslintegrationService;


@SystemSetup(extension = JslintegrationConstants.EXTENSIONNAME)
public class JslintegrationSystemSetup
{
	private final JslintegrationService jslintegrationService;

	public JslintegrationSystemSetup(final JslintegrationService jslintegrationService)
	{
		this.jslintegrationService = jslintegrationService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		jslintegrationService.createLogo(PLATFORM_LOGO_CODE);
	}

	private InputStream getImageStream()
	{
		return JslintegrationSystemSetup.class.getResourceAsStream("/jslintegration/sap-hybris-platform.png");
	}
}
