/*
 * [y] hybris Platform
 * 
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of SAP 
 * Hybris ("Confidential Information"). You shall not disclose such 
 * Confidential Information and shall use it only in accordance with the 
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.rest.service.impl;

import de.hybris.platform.util.Config;

import java.io.IOException;
import java.net.URI;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.net.ssl.SSLContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.jsl.constants.JslintegrationConstants;
import com.jsl.rest.service.JslRestService;



/**
 *
 */
public class JslPaymentRestServiceImpl implements JslRestService
{

	private static final Logger LOG = Logger.getLogger(JslPaymentRestServiceImpl.class);

	private RestTemplate restTemplate;

	private StringHttpMessageConverter jslStringHttpMessageConverter;


	private CredentialsProvider credentialProvider;

	@PostConstruct
	private void init()
	{
		jslStringHttpMessageConverter.setSupportedMediaTypes(Arrays.asList(MediaType.ALL));
		// XXX Auto-generated constructor stub
		/*
		 * restTemplate.getMessageConverters().stream().filter(converter -> converter instanceof
		 * Jaxb2RootElementHttpMessageConverter) .forEach(c -> { ((Jaxb2RootElementHttpMessageConverter)
		 * c).setSupportedMediaTypes(Arrays.asList(MediaType.ALL)); });
		 */
		//	restTemplate.getMessageConverters().forEach(converter -> {
		//	converter.getSupportedMediaTypes().add(MediaType.ALL);
		//});
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.amway.lynxcore.services.LynxRestService#lynxGetForObjectCommand(java.lang.String, java.lang.Class,
	 * java.lang.Class)
	 */
	@Override
	public <T> T getForObjectCommand(final String url, final Class<T> responseObj, final String transactionKey,
			final String customerId)
	{
		T response = null;
		try
		{
			final HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders.set("transactionKey", getTimeStampedTransactionKey(transactionKey, customerId));
			final HttpEntity<?> httpEntity = new HttpEntity<Object>(requestHeaders);
			final ResponseEntity<T> responseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, responseObj);
			response = responseEntity.getBody();
			printResponse(response);
		}
		catch (final Exception e)
		{
			LOG.error(String.format("%s : %s : %s", transactionKey, customerId, url), e);
		}
		return response;
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see com.amway.lynxcore.services.LynxRestService#lynxGetForObjectCommand(java.lang.String, java.lang.Class,
	 * java.lang.Class)
	 */
	@Override
	public <T> T getForObjectCommand(final URI uri, final Class<T> responseObj, final String transactionKey,
			final String customerId)
	{
		T response = null;
		try
		{
			final HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders.set("transactionKey", getTimeStampedTransactionKey(transactionKey, customerId));
			final HttpEntity<?> httpEntity = new HttpEntity<Object>(requestHeaders);
			final ResponseEntity<T> responseEntity = restTemplate.exchange(uri, HttpMethod.GET, httpEntity, responseObj);
			response = responseEntity.getBody();
			printResponse(response);
		}
		catch (final Exception e)
		{
			LOG.error(String.format("%s : %s : %s", transactionKey, customerId, uri.getPath()), e);
		}
		return response;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.amway.lynxcore.services.LynxRestService#lynxPostForObjectCommand(java.lang.String, java.lang.Class,
	 * java.lang.Class)
	 */
	@Override
	public <K, T> T postForObjectCommand(final String url, final K requestObj, final Class<T> responseObj,
			final String transactionKey, final String customerId)
	{
		T response = null;
		try
		{
			final HttpComponentsClientHttpRequestFactory requestFactory = establishConnection();
			restTemplate.setRequestFactory(requestFactory);
			final HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders.set("transactionKey", getTimeStampedTransactionKey(transactionKey, customerId));
			final HttpEntity<K> request = new HttpEntity<>(requestObj, requestHeaders);
			response = restTemplate.postForObject(url, request, responseObj);
		}
		catch (final Exception e)
		{
			LOG.error("Exception in JslRestServiceImpl.postForObjectCommand(): "
					+ String.format("%s : %s : %s", transactionKey, customerId, url), e);
		}
		return response;
	}

	@Override
	public <K, T> T postForObjectCommand(String url, K requestObj, Class<T> responseObj, String transactionKey, String customerId,
			MediaType mediaType)
	{
		T response = null;
		try
		{

			final HttpComponentsClientHttpRequestFactory requestFactory = establishConnection();
			restTemplate.setRequestFactory(requestFactory);
			final HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders.setContentType(mediaType);
			requestHeaders.set("transactionKey", getTimeStampedTransactionKey(transactionKey, customerId));
			final HttpEntity<K> request = new HttpEntity<>(requestObj, requestHeaders);
			response = restTemplate.postForObject(url, request, responseObj);
		}
		catch (final Exception e)
		{
			LOG.error("Exception in JslRestServiceImpl.postForObjectCommand(): "
					+ String.format("%s : %s : %s", transactionKey, customerId, url), e);
		}
		return response;
	}

	@Override
	public <K, T> T postForObjectCommandWithParam(final String url, final K requestObj, final Class<T> responseObj,
			final Map<String, Object> uriParam, final String transactionKey, final String customerId)
	{
		T response = null;
		try
		{
			final HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders.set("transactionKey", getTimeStampedTransactionKey(transactionKey, customerId));
			final HttpEntity<K> request = new HttpEntity<>(requestObj, requestHeaders);
			response = restTemplate.postForObject(url, request, responseObj, uriParam);
		}
		catch (final Exception e)
		{
			LOG.error(String.format("%s : %s : %s", transactionKey, customerId, url), e);
		}
		return response;
	}

	@Override
	public <T> T getForObjectCommandWithParam(final String url, final Class<T> responseObj, final Map<String, Object> uriParam,
			final String transactionKey, final String customerId)
	{
		T response = null;
		try
		{
			final HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders.set("transactionKey", getTimeStampedTransactionKey(transactionKey, customerId));
			final HttpEntity<?> httpEntity = new HttpEntity<Object>(requestHeaders);
			final ResponseEntity<T> responseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, responseObj, uriParam);
			response = responseEntity.getBody();
			printResponse(response);
		}
		catch (final Exception e)
		{
			LOG.error(String.format("%s : %s : %s", transactionKey, customerId, url), e);
		}
		return response;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T> T deleteForObjectCommand(final String url, final Class<T> responseObj, final String transactionKey,
			final String customerId)
	{
		ResponseEntity<T> responseEntity = null;
		try
		{
			final HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders.set("transactionKey", getTimeStampedTransactionKey(transactionKey, customerId));
			final HttpEntity<?> httpEntity = new HttpEntity<Object>(requestHeaders);
			responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, httpEntity, responseObj);
			if (null != responseEntity)
			{
				LOG.info("Response get for URL is : " + responseEntity.getClass());
				return responseEntity.getBody();
			}
		}
		catch (final Exception e)
		{
			LOG.error(String.format("%s : %s : %s", transactionKey, customerId, url), e);
		}
		return null;
	}

	@Override
	public <T> T deleteForObjectCommand(final String url, final Class<T> responseObj, final Map<String, Object> uriParam,
			final String transactionKey, final String customerId)
	{
		ResponseEntity<T> responseEntity = null;
		try
		{
			final HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders.set("transactionKey", getTimeStampedTransactionKey(transactionKey, customerId));
			final HttpEntity<?> httpEntity = new HttpEntity<Object>(requestHeaders);
			responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, httpEntity, responseObj, uriParam);
			if (null != responseEntity)
			{
				LOG.info("Response get for URL is : " + responseEntity.getClass());
				return responseEntity.getBody();
			}
		}
		catch (final Exception e)
		{
			LOG.error(String.format("%s : %s : %s", transactionKey, customerId, url), e);
		}
		return null;
	}

	/**
	 * @return HttpComponentsClientHttpRequestFactory
	 * @throws KeyStoreException
	 * @throws NoSuchAlgorithmException
	 * @throws KeyManagementException
	 */
	private HttpComponentsClientHttpRequestFactory establishConnection()
			throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException
	{
		final String timeout = Config.getParameter(JslintegrationConstants.Rest.SENDER_CONNECTION_READ_TIMEOUT);
		final HttpClientBuilder builder = HttpClientBuilder.create();
		builder.setDefaultCredentialsProvider(this.credentialProvider);
		final CloseableHttpClient client = builder.setSSLSocketFactory(getSSLConnectionSocketFactory()).build();
		final HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		requestFactory.setHttpClient(client);
		requestFactory.setConnectTimeout(Integer.parseInt(timeout));
		return requestFactory;
	}

	private SSLConnectionSocketFactory getSSLConnectionSocketFactory()
			throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException
	{
		TrustStrategy acceptingTrustStrategy = new TrustStrategy()
		{

			@Override
			public boolean isTrusted(final X509Certificate[] chain, String authType) throws CertificateException
			{
				// Oh, I am easy...
				return true;
			}

		};

		SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();

		//SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);

		SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext, new String[]
		{ "TLSv1.2" }, null, new NoopHostnameVerifier());

		return csf;
	}

	private <T> void printResponse(final T t)
	{
		if (LOG.isDebugEnabled() && null != t)
		{
			final ObjectMapper obj = new ObjectMapper();
			try
			{
				LOG.debug(obj.writeValueAsString(t));
			}
			catch (final JsonGenerationException e)
			{
				e.printStackTrace();
			}
			catch (final JsonMappingException e)
			{
				e.printStackTrace();
			}
			catch (final IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	private String getTimeStampedTransactionKey(final String transactionKey, final String customerId)
	{
		final StringBuilder transactionKeyBuilder = new StringBuilder();
		transactionKeyBuilder.append(transactionKey).append("_");
		if (StringUtils.isNotEmpty(customerId))
		{
			transactionKeyBuilder.append(customerId).append("_");
		}
		transactionKeyBuilder.append(new Date());
		return transactionKeyBuilder.toString();
	}

	/**
	 * @return the restTemplate
	 */
	public RestTemplate getRestTemplate()
	{
		return restTemplate;
	}

	/**
	 * @param restTemplate
	 *           the restTemplate to set
	 */
	public void setRestTemplate(final RestTemplate restTemplate)
	{
		this.restTemplate = restTemplate;
	}

	/**
	 * @return the jslStringHttpMessageConverter
	 */
	public StringHttpMessageConverter getJslStringHttpMessageConverter()
	{
		return jslStringHttpMessageConverter;
	}


	/**
	 * @param jslStringHttpMessageConverter
	 *           the jslStringHttpMessageConverter to set
	 */
	public void setJslStringHttpMessageConverter(StringHttpMessageConverter jslStringHttpMessageConverter)
	{
		this.jslStringHttpMessageConverter = jslStringHttpMessageConverter;
	}

	/**
	 * @return the credentialProvider
	 */
	public CredentialsProvider getCredentialProvider()
	{
		return credentialProvider;
	}

	/**
	 * @param credentialProvider
	 *           the credentialProvider to set
	 */
	public void setCredentialProvider(CredentialsProvider credentialProvider)
	{
		this.credentialProvider = credentialProvider;
	}
}
