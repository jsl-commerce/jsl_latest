/**
 *
 */
package com.jsl.rest.service;

import java.net.URI;
import java.util.Map;

import org.springframework.http.MediaType;


/**
 * @author manav.magoo
 *
 */
public interface JslRestService
{
	/**
	 * To fire the rest call for GET
	 *
	 * @param url
	 *           : URL is the end point of Rest
	 *
	 * @param responseObj
	 *           : Response object
	 * @param transactionKey
	 *           , transaction key
	 * @return : Object of Type T
	 */
	<T> T getForObjectCommand(final String url, final Class<T> responseObj, final String transactionKey, final String customerId);

	/**
	 * @param uri
	 * @param responseObj
	 *           : Response object
	 * @param transactionKey
	 *           , transaction key
	 * @return: Object of Type T
	 */
	<T> T getForObjectCommand(final URI uri, final Class<T> responseObj, final String transactionKey, final String customerId);

	/**
	 * To fire the rest call for POST
	 *
	 * @param url
	 *           : URL is the end point of Rest
	 *
	 * @param responseObj
	 *           : Response object
	 * @param requestObj
	 *           : Request object
	 * @param transactionKey
	 *           , transaction key
	 * @return : Object of Type T
	 */
	<K, T> T postForObjectCommand(final String url, final K requestObj, Class<T> responseObj, final String transactionKey,
			final String customerId, MediaType mediaType);

	/**
	 * To fire the rest call for POST
	 *
	 * @param url
	 *           : URL is the end point of Rest
	 *
	 * @param responseObj
	 *           : Response object
	 * @param requestObj
	 *           : Request object
	 * @param transactionKey
	 *           , transaction key
	 * @return : Object of Type T
	 */
	<K, T> T postForObjectCommand(final String url, final K requestObj, Class<T> responseObj, final String transactionKey,
			final String customerId);

	/**
	 * To fire the rest call for POST
	 *
	 * @param url
	 *           : URL is the end point of Rest
	 *
	 * @param responseObj
	 *           : Response object
	 * @param requestObj
	 *           : Request object
	 * @param transactionKey
	 *           , transaction key
	 * @return : Object of Type T
	 */
	<K, T> T postForObjectCommandWithParam(final String url, final K requestObj, Class<T> responseObj,
			final Map<String, Object> uriParam, final String transactionKey, final String customerId);

	/**
	 * @param url
	 * @param responseObj
	 * @param uriParam
	 * @param transactionKey
	 *           , transaction key
	 * @return Object of Type T
	 */
	<T> T getForObjectCommandWithParam(final String url, Class<T> responseObj, Map<String, Object> uriParam,
			final String transactionKey, final String customerId);

	/**
	 * @param url
	 * @param responseObj
	 * @param transactionKey
	 *           , transaction key
	 * @return Object of Type T
	 */
	<T> T deleteForObjectCommand(final String url, final Class<T> responseObj, final String transactionKey,
			final String customerId);

	/**
	 * @param url
	 * @param responseObj
	 * @param uriParam
	 * @param transactionKey
	 * @return
	 */
	<T> T deleteForObjectCommand(final String url, final Class<T> responseObj, Map<String, Object> uriParam,
			final String transactionKey, final String customerId);


}
