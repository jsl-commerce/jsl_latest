/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.jsl.constants;

/**
 * Global class for all Jslintegration constants. You can add global constants for your extension into this class.
 */
public final class JslintegrationConstants extends GeneratedJslintegrationConstants
{
	public static final String EXTENSIONNAME = "jslintegration";

	private JslintegrationConstants()
	{
		//empty to avoid instantiating this constant class
	}

	public class Rest
	{
		public static final String BASIC_USERNAME = "rest.basic.credentials.username";

		public static final String BASIC_PASSWORD = "rest.basic.credentials.password";

		public static final String SENDER_CONNECTION_READ_TIMEOUT = "jsl.rest.connection.read.timeout";

	}
	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "jslintegrationPlatformLogo";

	public static final String GRANT_TYPE = "grant_type";
	public static final String USERNAME = "username";
	public static final String PASSWORD = "password";
	public static final String CLIENT_ID = "client_id";
	public static final String PAYMENT_BASIC_USERNAME_KEY = "rest.payment.basic.credentials.username";
}
