/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.interceptors;

import java.io.IOException;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.apache.log4j.Logger;
import org.springframework.ws.client.WebServiceClientException;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.soap.saaj.SaajSoapMessage;

import com.jsl.cpi.util.ByteTransport;



/**
 *
 */
public class JslWebServiceInterceptor implements ClientInterceptor
{
	private static final String PREFERRED_PREFIX = "soap";
	private static final Logger LOG = Logger.getLogger(JslWebServiceInterceptor.class);

	@Override
	public boolean handleFault(MessageContext messageContext) throws WebServiceClientException
	{
		// No-op
		return true;
	}

	@Override
	public boolean handleRequest(MessageContext messageContext) throws WebServiceClientException
	{

		final SaajSoapMessage soapResponse = (SaajSoapMessage) messageContext.getRequest();
		alterSoapEnvelope(soapResponse);
		final ByteTransport byteArrayTransportOutputStream = new ByteTransport();
		try
		{
			messageContext.getRequest().writeTo(byteArrayTransportOutputStream);
		}
		catch (IOException e)
		{
			LOG.error("Unable to log Http request : " + e.getMessage());
		}
		final String httpMessage = new String(byteArrayTransportOutputStream.toByteArray());
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Client Request Message: " + httpMessage);
		}
		return true;
	}

	@Override
	public boolean handleResponse(MessageContext messageContext) throws WebServiceClientException
	{
		final ByteTransport byteArrayTransportOutputStream = new ByteTransport();
		try
		{
			messageContext.getResponse().writeTo(byteArrayTransportOutputStream);
		}
		catch (IOException e)
		{
			LOG.error("Unable  to log Http response : " + e.getMessage());
		}
		final String httpMessage = new String(byteArrayTransportOutputStream.toByteArray());
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Client Response Message: " + httpMessage);
		}
		return true;
	}

	private void alterSoapEnvelope(SaajSoapMessage soapResponse)
	{
		try
		{
			final SOAPMessage soapMessage = soapResponse.getSaajMessage();
			final SOAPPart soapPart = soapMessage.getSOAPPart();
			final SOAPEnvelope envelope = soapPart.getEnvelope();
			final SOAPHeader header = soapMessage.getSOAPHeader();
			final SOAPBody body = soapMessage.getSOAPBody();
			final SOAPFault fault = body.getFault();
			envelope.removeNamespaceDeclaration(envelope.getPrefix());
			envelope.setPrefix(PREFERRED_PREFIX);
			header.setPrefix(PREFERRED_PREFIX);
			body.setPrefix(PREFERRED_PREFIX);
			if (fault != null)
			{
				fault.setPrefix(PREFERRED_PREFIX);
			}
		}
		catch (final SOAPException e)
		{
			LOG.error("Error in creating soap request: " + e);
		}
	}


	public void afterCompletion(MessageContext messageContext, Exception ex)
	{
		// no-op
	}
}
