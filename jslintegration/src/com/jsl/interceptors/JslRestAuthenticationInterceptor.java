/**
 *
 */
package com.jsl.interceptors;

import java.io.IOException;
import java.nio.charset.Charset;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.support.HttpRequestWrapper;


public class JslRestAuthenticationInterceptor implements ClientHttpRequestInterceptor
{

	private static final String SEPARATOR = ":";

	private static final String BASIC = "Basic ";

	private static final String AUTHORIZATION = "Authorization";

	private static final String US_ASCII = "US-ASCII";

	private String username;

	private String password;

	/** Logger instance for this class */
	private final Logger LOG = Logger.getLogger(JslRestAuthenticationInterceptor.class);

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.http.client.ClientHttpRequestInterceptor#intercept (org.springframework.http.HttpRequest,
	 * byte[], org.springframework.http.client.ClientHttpRequestExecution)
	 */
	@Override
	public ClientHttpResponse intercept(final HttpRequest request, final byte[] body, final ClientHttpRequestExecution execution)
			throws IOException
	{
		final String userCredentials = username + SEPARATOR + password;
		final byte[] encodedAuth = Base64.encodeBase64(userCredentials.getBytes(Charset.forName(US_ASCII)));
		final String authHeader = BASIC + new String(encodedAuth);
		final HttpRequestWrapper requestWrapper = new HttpRequestWrapper(request);
		requestWrapper.getHeaders().set(AUTHORIZATION, authHeader);
		return execution.execute(request, body);

	}

	/**
	 * @param username
	 *           the username to set
	 */
	public void setUsername(final String username)
	{
		this.username = username;
	}

	/**
	 * @param password
	 *           the password to set
	 */
	public void setPassword(final String password)
	{
		this.password = password;
	}

}
