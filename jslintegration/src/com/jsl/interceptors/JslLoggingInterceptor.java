/**
 *
 */
package com.jsl.interceptors;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;



public class JslLoggingInterceptor implements ClientHttpRequestInterceptor
{

	/** Logger instance for this class */
	private final Logger LOG = Logger.getLogger(JslLoggingInterceptor.class);

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.http.client.ClientHttpRequestInterceptor#intercept (org.springframework.http.HttpRequest,
	 * byte[], org.springframework.http.client.ClientHttpRequestExecution)
	 *
	 * This method is used to log request and response for calls based on spring RestTemplate
	 */
	@Override
	public ClientHttpResponse intercept(final HttpRequest request, final byte[] body, final ClientHttpRequestExecution execution)
			throws IOException
	{
		traceRequest(request, body);
		final ClientHttpResponse response = execution.execute(request, body);
		traceResponse(response);
		return response;
	}

	private void traceRequest(final HttpRequest request, final byte[] body) throws IOException
	{

		if (LOG.isDebugEnabled())
		{
			LOG.info("<===========================Request Logging begin================================================>");
			LOG.info("URI : " + request.getURI());
			LOG.info("Method : " + request.getMethod());
			LOG.info("Headers : " + request.getHeaders());
			LOG.info("Request Body : " + new String(body, "UTF-8"));
			LOG.info("<===========================Request Logging end==================================================>");
		}

	}

	private void traceResponse(final ClientHttpResponse response) throws IOException
	{

		if (!(StringUtils.equals("200", response.getStatusCode().toString())
				|| StringUtils.equals("201", response.getStatusCode().toString())))
		{
			LOG.info("<============================Response Logging begin==========================================>");
			LOG.info("status code: " + response.getStatusCode());
			LOG.info("status text: " + response.getStatusText());
			String res;
			final BufferedReader br = new BufferedReader(new InputStreamReader(response.getBody()));
			while ((res = br.readLine()) != null)
			{
				LOG.info("Response Body : " + res);
			}
			LOG.info("<=======================Response Logging end=================================================>");
		}
	}
}
