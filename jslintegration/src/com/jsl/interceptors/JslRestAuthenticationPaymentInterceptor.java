/*
 * [y] hybris Platform
 * 
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of SAP 
 * Hybris ("Confidential Information"). You shall not disclose such 
 * Confidential Information and shall use it only in accordance with the 
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.interceptors;

import de.hybris.platform.util.Config;

import java.io.IOException;

import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.support.HttpRequestWrapper;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.jsl.constants.JslintegrationConstants;
import com.jsl.payment.dto.SecurityTokenResponseDto;


/**
 *
 */
public class JslRestAuthenticationPaymentInterceptor implements ClientHttpRequestInterceptor
{

	private static final String SEPARATOR = ":";

	private static final String BEARER = "Bearer ";

	private static final String AUTHORIZATION = "Authorization";

	private static final String PAYMENT_OAUTH_TOKEN_URL_KEY = "payment.oauth.token.url";

	private static final String PAYMENT_USERNAME_KEY = "rest.payment.user.credentials.username";
	private static final String PAYMENT_PASSWORD_KEY = "rest.payment.user.credentials.password";

	private RestTemplate restTemplate;

	private CredentialsProvider credentialProvider;

	/** Logger instance for this class */
	private final Logger LOG = Logger.getLogger(JslRestAuthenticationPaymentInterceptor.class);

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.http.client.ClientHttpRequestInterceptor#intercept (org.springframework.http.HttpRequest,
	 * byte[], org.springframework.http.client.ClientHttpRequestExecution)
	 */
	@Override
	public ClientHttpResponse intercept(final HttpRequest request, final byte[] body, final ClientHttpRequestExecution execution)
			throws IOException
	{

		final String authHeader = BEARER + new String(fetchToken());
		final HttpRequestWrapper requestWrapper = new HttpRequestWrapper(request);
		requestWrapper.getHeaders().set(AUTHORIZATION, authHeader);
		return execution.execute(request, body);
	}

	private String fetchToken()
	{

		SecurityTokenResponseDto response = null;
		String url = Config.getParameter(PAYMENT_OAUTH_TOKEN_URL_KEY);
		try
		{
			final HttpComponentsClientHttpRequestFactory requestFactory = establishConnection();
			restTemplate.setRequestFactory(requestFactory);
			final HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
			map.add(JslintegrationConstants.GRANT_TYPE, "password");
			map.add(JslintegrationConstants.USERNAME, Config.getParameter(PAYMENT_USERNAME_KEY));
			map.add(JslintegrationConstants.PASSWORD, Config.getParameter(PAYMENT_PASSWORD_KEY));
			map.add(JslintegrationConstants.CLIENT_ID, Config.getParameter(JslintegrationConstants.PAYMENT_BASIC_USERNAME_KEY));
			final HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, requestHeaders);
			response = restTemplate.postForObject(url, request, SecurityTokenResponseDto.class);
		}
		catch (final Exception e)
		{
			LOG.error("Exception in JslRestServiceImpl.postForObjectCommand(): " + String.format("%s ", url), e);
		}
		return response.getAccessToken();
	}

	/**
	 * @return HttpComponentsClientHttpRequestFactory
	 */
	private HttpComponentsClientHttpRequestFactory establishConnection()
	{
		final String timeout = Config.getParameter(JslintegrationConstants.Rest.SENDER_CONNECTION_READ_TIMEOUT);
		final HttpClientBuilder builder = HttpClientBuilder.create();
		builder.setDefaultCredentialsProvider(this.credentialProvider);
		final CloseableHttpClient client = builder.build();
		final HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		requestFactory.setHttpClient(client);
		requestFactory.setConnectTimeout(Integer.parseInt(timeout));
		return requestFactory;
	}

	/**
	 * @return the restTemplate
	 */
	public RestTemplate getRestTemplate()
	{
		return restTemplate;
	}

	/**
	 * @param restTemplate
	 *           the restTemplate to set
	 */
	public void setRestTemplate(RestTemplate restTemplate)
	{
		this.restTemplate = restTemplate;
	}

	/**
	 * @return the credentialProvider
	 */
	public CredentialsProvider getCredentialProvider()
	{
		return credentialProvider;
	}

	/**
	 * @param credentialProvider
	 *           the credentialProvider to set
	 */
	public void setCredentialProvider(CredentialsProvider credentialProvider)
	{
		this.credentialProvider = credentialProvider;
	}

}