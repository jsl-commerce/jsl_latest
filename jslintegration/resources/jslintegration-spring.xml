<?xml version="1.0" encoding="UTF-8"?>
<!-- [y] hybris Platform Copyright (c) 2018 SAP SE or an SAP affiliate company. 
	All rights reserved. This software is the confidential and proprietary information 
	of SAP ("Confidential Information"). You shall not disclose such Confidential 
	Information and shall use it only in accordance with the terms of the license 
	agreement you entered into with SAP. -->
<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:aop="http://www.springframework.org/schema/aop"
	xsi:schemaLocation="http://www.springframework.org/schema/beans
           http://www.springframework.org/schema/beans/spring-beans.xsd
           http://www.springframework.org/schema/aop
           http://www.springframework.org/schema/aop/spring-aop.xsd">

	<!-- The hybris platform provides one single Spring application context 
		for beans located at the jalo layer. Each extension can add beans to this 
		context by specifying them at this configuration file. You can use and/or 
		redefine beans defined at configuration files of other extensions as well 
		as adding new one. If you want to locate this file at a different location 
		or want to use several files for better, structuring, please use the '<yourextension>.application-context' 
		property at your project.properties file. There you can specify a comma separated 
		list of files where first is tried to find a file per classpath and then 
		per file system. If you want to configure beans for your web module please 
		have a look at your web/webroot/WEB-INF/<extname>-web-spring.xml. -->

	<!-- Example how to override a manager -->
	<!-- <bean id="core.productManager" class="com.jsl.jalo.JslintegrationProductManager" 
		init-method="init" destroy-method="destroy" /> -->


	<!-- Example how to configure the class to use for jalo session creation -->
	<!-- <bean id="jalosession" class="de.hybris.platform.jalo.JaloSessionFactory"> 
		<property name="targetClassName" value="com.jsl.jalo.JslintegrationJaloSession"/> 
		</bean> -->


	<!-- Example how to override an item -->
	<!-- <bean id="de.hybris.platform.jalo.product.Product" class="com.jsl.jalo.JslintegrationProduct" 
		scope="prototype" /> -->

	<!-- Example how to register and use an AOP aspect Also see http://static.springframework.org/spring/docs/2.5.x/reference/aop.html -->

	<!-- <bean id="jslintegrationProfBean" class="com.jsl.aop.JslintegrationProfilingAspect"/> 
		<aop:config proxy-target-class="true"> <aop:aspect id="jslintegrationProfAspect" 
		ref="jslintegrationProfBean" > <aop:pointcut id="profiledMethods" expression="execution(* 
		getModificationTime(..))" /> <aop:around pointcut-ref="profiledMethods" method="profile" 
		/> </aop:aspect> </aop:config> -->


	<!-- some other examples of a pointcut that matches everything: <aop:pointcut 
		id="profiledMethods" expression="bean(de.hybris.platform.jalo.user.Customer) 
		&amp;&amp; !execution(* getPK(..))" /> <aop:pointcut id="profiledMethods" 
		expression="execution(* *(..))" /> -->

	<bean id="jslintegrationSystemSetup"
		class="com.jsl.setup.JslintegrationSystemSetup">
		<!-- constructor arg injection example -->
		<constructor-arg ref="jslintegrationService" />
	</bean>

	<bean id="jslintegrationService"
		class="com.jsl.service.impl.DefaultJslintegrationService">
		<!-- setter injection example -->
		<property name="modelService" ref="modelService" />
		<property name="mediaService" ref="mediaService" />
		<property name="flexibleSearchService"
			ref="flexibleSearchService" />
	</bean>

	<bean id="jslClientReportConnectionService"
		class="com.jsl.cpi.integration.service.impl.CpiReportsConnectionServiceImpl">
		<!-- setter injection example -->
		<property name="marshaller" ref="marshaller" />
		<property name="webServiceTemplate" ref="webServiceTemplate" />
		<property name="jslWebServiceMessageCallback"
			ref="jslWebServiceMessageCallback" />
		<property name="clientInterceptors">
			<list>
				<ref bean="jslWebServiceInterceptor" />
			</list>
		</property>
	</bean>

	<bean id="jslWebServiceInterceptor"
		class="com.jsl.interceptors.JslWebServiceInterceptor" />

	<bean id="marshaller"
		class="org.springframework.oxm.jaxb.Jaxb2Marshaller">
		<property name="contextPaths">
			<list>
				<!-- Please remove line breaks with whitespace at the end of the package 
					names that may occure after autoformat -->
				<value>com.jsl.cpi.report.dto.pop.plant</value>
				<value>com.jsl.cpi.report.dto.sales</value>
				<value>com.jsl.cpi.report.dto.orderbooking</value>
				<value>com.jsl.cpi.report.dto.pop.yard</value>
				<value>com.jsl.cpi.report.dto.customer.dashboard</value>
				<value>com.jsl.cpi.report.dto.accountstatement</value>
				<value>com.jsl.cpi.pop.mou.dto</value>
				<value>com.jsl.cpi.invoice.batch.dto</value>
				<value>com.jsl.cpi.report.dto.vehicle.tracking</value>
			</list>
		</property>
	</bean>

	<bean id="webServiceTemplate"
		class="org.springframework.ws.client.core.WebServiceTemplate" />

	<bean id="jslWebServiceMessageCallback"
		class="com.jsl.cpi.util.JslWebServiceMessageCallback" />

	<!-- Rest Template -->
	<bean id="loggingInterceptor"
		class="com.jsl.interceptors.JslLoggingInterceptor" />


	<bean id="authenticationInterceptor"
		class="com.jsl.interceptors.JslRestAuthenticationInterceptor">
		<property name="username"
			value="${rest.basic.credentials.username}" />
		<property name="password"
			value="${rest.basic.credentials.password}" />
	</bean>

	<bean id="credentialProvider"
		class="org.apache.http.impl.client.BasicCredentialsProvider" />

	<!-- Authorization scope for accessing restful service. Since we want this 
		template to be used for everything, we are setting up it with defaults -->
	<bean id="authScope" class="org.apache.http.auth.AuthScope">
		<constructor-arg name="host">
			<null />
		</constructor-arg>
		<constructor-arg>
			<value>-1</value>
		</constructor-arg>
		<constructor-arg>
			<null />
		</constructor-arg>
		<constructor-arg>
			<null />
		</constructor-arg>
	</bean>

	<!-- Username and Password Credentials to access restful service -->
	<bean id="JslDefaultCredentials"
		class="org.apache.http.auth.UsernamePasswordCredentials">
		<constructor-arg
			value="${rest.basic.credentials.username}" />
		<constructor-arg
			value="${rest.basic.credentials.password}" />
	</bean>


	<!-- Used to invoke a method in BasicCredentialsProvider. This has to be 
		done this way as BasicCredentialsProvider does not take provider and credentials 
		in constructor or setter method. It has to set by invoking setCredentials() 
		method and passing two arguments -->
	<bean id="methodInvoke"
		class="org.springframework.beans.factory.config.MethodInvokingFactoryBean">
		<property name="targetObject">
			<ref bean="credentialProvider" />
		</property>
		<property name="targetMethod" value="setCredentials">
		</property>
		<property name="arguments">
			<list>
				<ref bean="authScope" />
				<ref bean="JslDefaultCredentials" />
			</list>
		</property>
	</bean>


	<bean id="JslBaseRestTemplate"
		class="org.springframework.web.client.RestTemplate">
		<property name="messageConverters">
			<list>
				<bean
					class="org.springframework.http.converter.json.MappingJackson2HttpMessageConverter" />
				<bean
					class="org.springframework.http.converter.StringHttpMessageConverter" />
				<ref bean="jslRestXmlMessageConverter" />
			</list>
		</property>
		<property name="interceptors">
			<list>
				<ref bean="authenticationInterceptor" />
				<ref bean="loggingInterceptor" />
			</list>
		</property>
	</bean>

	<bean id="jslRestXmlMessageConverter"
		class="org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter" />

	<bean id="jslRestServiceImpl"
		class="com.jsl.rest.service.impl.JslRestServiceImpl">
		<property name="restTemplate" ref="JslBaseRestTemplate" />
		<property name="credentialProvider" ref="credentialProvider" />
		<property name="jaxb2RootElementHttpMessageConverter"
			ref="jslRestXmlMessageConverter" />
	</bean>
	<!--<bean id="jslXmlMarsheller" class = "org.springframework.oxm.xstream.XStreamMarshaller"/> 
		<bean id="jslXmlMessageConverter" class="org.springframework.http.converter.xml.MarshallingHttpMessageConverter"> 
		<constructor-arg ref="jslXmlMarsheller" /> </bean> <bean id="jslRestObjectMapper" 
		class="com.fasterxml.jackson.databind.ObjectMapper" > <property name="serializationInclusion" 
		value="NON_NULL"/> </bean> <bean id="jslRestXmlMessageConverter" class="org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter"> 
		<constructor-arg ref="jslRestObjectMapper" /> </bean> -->


	<!-- Jsl Payment Config -->

	<bean id="jslCredentialProvider"
		class="org.apache.http.impl.client.BasicCredentialsProvider" />
	<bean id="jslPaymentMethodInvoke"
		class="org.springframework.beans.factory.config.MethodInvokingFactoryBean">
		<property name="targetObject">
			<ref bean="jslCredentialProvider" />
		</property>
		<property name="targetMethod" value="setCredentials">
		</property>
		<property name="arguments">
			<list>
				<ref bean="authScope" />
				<ref bean="JsPaymentlDefaultCredentials" />
			</list>
		</property>
	</bean>
	<bean id="jslPaymentTokenAuthenticationInterceptor"
		class="com.jsl.interceptors.JslRestAuthenticationInterceptor">
		<property name="username"
			value="${rest.payment.basic.credentials.username}" />
		<property name="password"
			value="${rest.payment.basic.credentials.password}" />
	</bean>
	
	<bean id="jslPaymentAuthenticationInterceptor"
		class="com.jsl.interceptors.JslRestAuthenticationPaymentInterceptor">
		<property name="restTemplate"
			ref="JslPaymentTokenBaseRestTemplate" />
		<property name="credentialProvider"
			ref="jslCredentialProvider" />
	</bean>
	
	<!-- Username and Password Credentials to access restful service -->
	<bean id="JsPaymentlDefaultCredentials"
		class="org.apache.http.auth.UsernamePasswordCredentials">
		<constructor-arg
			value="${rest.payment.basic.credentials.username}" />
		<constructor-arg
			value="${rest.payment.basic.credentials.password}" />
	</bean>


	<bean id="JslPaymentTokenBaseRestTemplate"
		class="org.springframework.web.client.RestTemplate">
		<property name="messageConverters">
			<list>
				<bean
					class="org.springframework.http.converter.json.MappingJackson2HttpMessageConverter" />
				<bean
					class="org.springframework.http.converter.StringHttpMessageConverter" />
				<bean
					class="org.springframework.http.converter.FormHttpMessageConverter" />
					
			</list>
		</property>
		<property name="interceptors">
			<list>
				<ref bean="jslPaymentTokenAuthenticationInterceptor" />
				<ref bean="loggingInterceptor" />
			</list>
		</property>
	</bean>
	
	<bean id="JslPaymentBaseRestTemplate"
		class="org.springframework.web.client.RestTemplate">
		<property name="messageConverters">
			<list>
				<bean
					class="org.springframework.http.converter.json.MappingJackson2HttpMessageConverter" />
				<ref bean ="jslStringHttpMessageConverter"/>
				<bean
					class="org.springframework.http.converter.FormHttpMessageConverter" />
					
			</list>
		</property>
		<property name="interceptors">
			<list>
				<ref bean="loggingInterceptor" />
				<ref bean="jslPaymentAuthenticationInterceptor"/>
			</list>
		</property>
	</bean>
	
	<bean id = "jslStringHttpMessageConverter" class="org.springframework.http.converter.StringHttpMessageConverter" />

	<bean id="jslPaymentRestServiceImpl"
		class="com.jsl.rest.service.impl.JslPaymentRestServiceImpl">
		<property name="restTemplate" ref="JslPaymentTokenBaseRestTemplate" />
		<property name="jslStringHttpMessageConverter" ref ="jslStringHttpMessageConverter"/>
		<property name="credentialProvider" ref="jslCredentialProvider" />
	</bean>
	
	
	<!-- Payment Response Processing -->
	<alias name="jslPaymentIntegrationDao" alias="jslPaymentIntegrationDao"/>
	<bean id="jslPaymentIntegrationDao" class="com.jsl.jslintegration.dao.impl.JslPaymentDaoImpl">
		<property name="flexibleSearchService" ref="flexibleSearchService" />
		<property name="sessionService" ref="sessionService" />
		<property name="userService" ref="userService" />
	</bean>
	
	<alias name="jslIntegrationPaymentService" alias="jslIntegrationPaymentService"/>
	<bean id="jslIntegrationPaymentService" class="com.jsl.jslintegration.service.impl.JslPaymentResponseServiceImpl">
        <property name="modelService" ref="modelService"/>
        <property name="jslPaymentDao" ref="jslPaymentIntegrationDao"/>
        <property name="transactionDetailsConverter" ref="jslTransactionDetailsConverter"/>
        <property name="jslRestService" ref="jslRestServiceImpl"/>
	</bean>
</beans>
