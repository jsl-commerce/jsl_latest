/*
 * [y] hybris Platform
 * 
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of SAP 
 * Hybris ("Confidential Information"). You shall not disclose such 
 * Confidential Information and shall use it only in accordance with the 
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jsl.jslintegration.payment.dto.JslPaymentResponse;
import com.jsl.jslintegration.service.JslPaymentResponseService;


/**
 *
 */
@Controller
@RequestMapping(value = "/payment")
public class JslPaymentResponseController
{
	@Resource(name = "jslIntegrationPaymentService")
	private JslPaymentResponseService jslPaymentResponseService;

	@RequestMapping(value = "/process-payment", method = RequestMethod.POST)
	public @ResponseBody boolean processPaymentPage(@RequestBody
	final JslPaymentResponse paymentResponse, final Model model) throws Exception
	{
		jslPaymentResponseService.processPaymentReponse(paymentResponse);
		return true;
	}

	/**
	 * @return the jslPaymentResponseService
	 */
	public JslPaymentResponseService getJslPaymentResponseService()
	{
		return jslPaymentResponseService;
	}

	/**
	 * @param jslPaymentResponseService
	 *           the jslPaymentResponseService to set
	 */
	public void setJslPaymentResponseService(JslPaymentResponseService jslPaymentResponseService)
	{
		this.jslPaymentResponseService = jslPaymentResponseService;
	}

}
