package com.jsl.jslConfigurableBundleService.constants;

@SuppressWarnings({"deprecation","PMD","squid:CallToDeprecatedMethod"})
public class JslConfigurableBundleServiceConstants extends GeneratedJslConfigurableBundleServiceConstants
{
	public static final String EXTENSIONNAME = "jslConfigurableBundleService";
	
	private JslConfigurableBundleServiceConstants()
	{
		//empty
	}
	
	
}
