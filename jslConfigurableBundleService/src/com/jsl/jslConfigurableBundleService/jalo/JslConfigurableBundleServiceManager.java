package com.jsl.jslConfigurableBundleService.jalo;

import com.jsl.jslConfigurableBundleService.constants.JslConfigurableBundleServiceConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

@SuppressWarnings("PMD")
public class JslConfigurableBundleServiceManager extends GeneratedJslConfigurableBundleServiceManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( JslConfigurableBundleServiceManager.class.getName() );
	
	public static final JslConfigurableBundleServiceManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (JslConfigurableBundleServiceManager) em.getExtension(JslConfigurableBundleServiceConstants.EXTENSIONNAME);
	}
	
}
