/**
 *
 */
package com.jsl.configurablebundleservices.strategies.impl;

import de.hybris.platform.configurablebundleservices.bundle.BundleTemplateService;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.order.EntryGroup;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.jalo.order.delivery.DeliveryMode;
import de.hybris.platform.order.strategies.calculation.impl.DefaultFindDeliveryCostStrategy;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.util.PriceValue;

import org.apache.log4j.Logger;

import com.jsl.core.enums.JslCartType;
import com.jsl.core.services.JslAuctionDeliveryService;

import reactor.util.CollectionUtils;


/**
 * @author manav.magoo
 *
 */
public class JslFindDeliveryCostStrategy extends DefaultFindDeliveryCostStrategy
{

	private static final Logger LOG = Logger.getLogger(JslFindDeliveryCostStrategy.class);
	ModelService modelService;
	BundleTemplateService bundleTemplateService;
	JslAuctionDeliveryService jslAuctionDeliveryService;


	private boolean checkValidBundleEntry(final EntryGroup entryGroup)
	{
		if (!entryGroup.getChildren().isEmpty())
		{
			final BundleTemplateModel bundle = getBundle(entryGroup.getChildren().get(0).getExternalReferenceId());
			if (bundle != null & !CollectionUtils.isEmpty(bundle.getProducts()))
			{
				return true;
			}
		}
		return false;
	}

	private BundleTemplateModel getBundle(final String bundleTemplateId)
	{
		return getBundleTemplateService().getBundleTemplateForCode(bundleTemplateId);
	}

	@Override
	public PriceValue getDeliveryCost(final AbstractOrderModel order)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("order", order);
		try
		{
			if (JslCartType.AUCTION.equals(order.getType()))
			{
				if (CollectionUtils.isEmpty(order.getEntryGroups()))
				{
					return new PriceValue(order.getCurrency().getIsocode(), 0.0, order.getNet().booleanValue());
				}

				if (order.getDeliveryAddress() == null)
				{
					return new PriceValue(order.getCurrency().getIsocode(), 0.0, order.getNet().booleanValue());
				}

				final EntryGroup entryGroup = order.getEntryGroups().get(0);
				if (checkValidBundleEntry(entryGroup))
				{
					final BundleTemplateModel bundleTemplate = getBundle(entryGroup.getChildren().get(0).getExternalReferenceId());
					final String location = bundleTemplate.getProducts().get(0).getLocation();

					if (order.getDeliveryAddress() == null)
					{
						return new PriceValue(order.getCurrency().getIsocode(), 0.0, order.getNet().booleanValue());
					}

					final double deliveryCostPerTon = findDeliveryCost(location, order.getDeliveryAddress().getPostalcode());

					final double totalDeliveryCost = (bundleTemplate.getQuantity() * deliveryCostPerTon);

					return new PriceValue(order.getCurrency().getIsocode(), totalDeliveryCost, order.getNet().booleanValue());
				}
				else
				{
					return new PriceValue(order.getCurrency().getIsocode(), 0.0, order.getNet().booleanValue());
				}

			}
			else
			{
				final DeliveryModeModel deliveryMode = order.getDeliveryMode();
				if (deliveryMode != null && deliveryMode.getCode().equals("standard-gross") && order.getDeliveryAddress() != null)
				{
					final double deliveryCostPerTon = findDeliveryCost(order.getWarehouse().getCode(),
							order.getDeliveryAddress().getPostalcode());
					if (deliveryCostPerTon != 0)
					{

						double totalQuantity = 0;
						for (final AbstractOrderEntryModel ao : order.getEntries())
						{
							totalQuantity = totalQuantity + ao.getQuantity();
						}
						final double totalCost = totalQuantity * deliveryCostPerTon;
						return new PriceValue(order.getCurrency().getIsocode(), totalCost, order.getNet().booleanValue());
					}
					else
					{
						order.setDeliveryMode(getJslAuctionDeliveryService().getDeliveryMode("customer-gross"));
						getModelService().save(order);
						final AbstractOrder orderItem = getModelService().getSource(order);
						final DeliveryMode dModeJalo = getModelService().getSource(deliveryMode);
						return dModeJalo.getCost(orderItem);
					}
				}
				else
				{
					return new PriceValue(order.getCurrency().getIsocode(), 0.0, order.getNet().booleanValue());
				}
			}


		}
		catch (final Exception e)
		{
			LOG.warn("Could not find deliveryCost for order [" + order.getCode() + "] due to : " + e + "... skipping!");
			return new PriceValue(order.getCurrency().getIsocode(), 0.0, order.getNet().booleanValue());
		}
	}


	private double findDeliveryCost(final String location, final String postalCode)
	{
		return getJslAuctionDeliveryService().getDeliveryCharge(location, postalCode);
	}

	/**
	 * @return the modelService
	 */
	@Override
	public ModelService getModelService()
	{
		return modelService;
	}


	/**
	 * @param modelService
	 *           the modelService to set
	 */
	@Override
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}


	/**
	 * @return the bundleTemplateService
	 */
	public BundleTemplateService getBundleTemplateService()
	{
		return bundleTemplateService;
	}


	/**
	 * @param bundleTemplateService
	 *           the bundleTemplateService to set
	 */
	public void setBundleTemplateService(final BundleTemplateService bundleTemplateService)
	{
		this.bundleTemplateService = bundleTemplateService;
	}

	/**
	 * @return the jslAuctionDeliveryService
	 */
	public JslAuctionDeliveryService getJslAuctionDeliveryService()
	{
		return jslAuctionDeliveryService;
	}

	/**
	 * @param jslAuctionDeliveryService
	 *           the jslAuctionDeliveryService to set
	 */
	public void setJslAuctionDeliveryService(final JslAuctionDeliveryService jslAuctionDeliveryService)
	{
		this.jslAuctionDeliveryService = jslAuctionDeliveryService;
	}
}
