/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.configurablebundleservices.strategies.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.jsl.core.constants.JslCoreConstants;
import com.jsl.core.util.JslB2bUnitUtil;
import com.sap.hybris.sapcustomerb2b.inbound.DefaultSAPB2BDeliveryAddressesLookupStrategy;


/**
 *
 */
public class JslB2BDeliveryAddressesLookupStrategy extends DefaultSAPB2BDeliveryAddressesLookupStrategy
{
	private static final String UNDERSCORE = "_";
	private static final String AUCTION_UNIT_CODE = "auction.b2bunit.code";
	@Autowired
	SessionService sessionService;

	private static final Logger LOG = Logger.getLogger(JslB2BDeliveryAddressesLookupStrategy.class);

	@Autowired
	UserService userService;

	@Override
	public List<AddressModel> getDeliveryAddressesForOrder(final AbstractOrderModel abstractOrder,
			final boolean visibleAddressesOnly)
	{
		List<AddressModel> deliveryAddresses = new ArrayList<AddressModel>();
		// retrieve default delivery addresses for order
		deliveryAddresses = super.getDeliveryAddressesForOrder(abstractOrder, visibleAddressesOnly);
		// retrieve B2B customer of order
		final B2BCustomerModel b2bCustomer = (B2BCustomerModel) abstractOrder.getUser();
		// retrieve B2B unit of B2B customer

		final B2BUnitModel b2bUnit = JslB2bUnitUtil.getParentB2bUnit(b2bCustomer);

		final HashMap<String, B2BUnitModel> childUnits = sessionService.executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public Object execute()
			{
				final B2BUnitModel childb2bUnit1000 = getB2bUnitService().getUnitForUid(b2bUnit.getUid() + UNDERSCORE
						+ JslCoreConstants.SALESORGANIZATION_1000 + UNDERSCORE + Config.getParameter(AUCTION_UNIT_CODE));

				final B2BUnitModel childb2bUnit1010 = getB2bUnitService().getUnitForUid(b2bUnit.getUid() + UNDERSCORE
						+ JslCoreConstants.SALESORGANIZATION_1010 + UNDERSCORE + Config.getParameter(AUCTION_UNIT_CODE));

				final HashMap<String, B2BUnitModel> childB2BUnits = new HashMap<>(2);
				if (childb2bUnit1000 != null)
				{
					childB2BUnits.put(JslCoreConstants.SALESORGANIZATION_1000, childb2bUnit1000);
				}
				if (childb2bUnit1010 != null)
				{
					childB2BUnits.put(JslCoreConstants.SALESORGANIZATION_1010, childb2bUnit1010);
				}
				return childB2BUnits;
			}
			// set because of LynxTradeInRestriction
		}, userService.getAdminUser());

		LOG.info("JSL child units size" + childUnits.size());

		// retrieve delivery addresses for B2B unit
		final List<AddressModel> deliveryAddressesForB2BUnits = new ArrayList<>();

		childUnits.forEach((code, childUnit) -> {
			deliveryAddressesForB2BUnits.addAll(getDeliveryAddressesForB2BUnit(childUnit, visibleAddressesOnly));
		});

		// merge delivery addresses for order and for B2B unit
		if (deliveryAddresses != null && !deliveryAddresses.isEmpty())
		{
			if (deliveryAddressesForB2BUnits != null && !deliveryAddressesForB2BUnits.isEmpty())
			{
				deliveryAddresses.addAll(deliveryAddressesForB2BUnits);
			}
		}
		else
		{
			if (deliveryAddressesForB2BUnits != null && !deliveryAddressesForB2BUnits.isEmpty())
			{
				return deliveryAddressesForB2BUnits;

			}
			else
			{
				return Collections.emptyList();
			}
		}
		return deliveryAddresses;

	}

	@Override
	protected List<AddressModel> getDeliveryAddressesForB2BUnit(final B2BUnitModel b2bUnit, final boolean visibleAddressesOnly)
	{
		if (visibleAddressesOnly)
		{
			return getVisibleDeliveryAddressesForB2BUnit(b2bUnit);
		}
		return getAllDeliveryAddressesForB2BUnit(b2bUnit);
	}

	@Override
	protected List<AddressModel> getVisibleDeliveryAddressesForB2BUnit(final B2BUnitModel b2bUnit)
	{
		ServicesUtil.validateParameterNotNull(b2bUnit, "B2BUnit cannot be null");
		final List<AddressModel> addresses = new ArrayList<AddressModel>();
		for (final AddressModel address : b2bUnit.getAddresses())
		{
			// check whether B2B unit address is a shipping address and visible
			// in address book
			if (Boolean.TRUE.equals(address.getShippingAddress()) && Boolean.TRUE.equals(address.getVisibleInAddressBook()))
			{
				LOG.info("JSL getVisibleDeliveryAddressesForB2BUnit address in condition" + address.getSapCustomerID());
				addresses.add(address);
			}
		}
		return addresses;
	}

	@Override
	protected List<AddressModel> getAllDeliveryAddressesForB2BUnit(final B2BUnitModel b2bUnit)
	{

		ServicesUtil.validateParameterNotNull(b2bUnit, "B2BUnit cannot be null");
		final List<AddressModel> addresses = new ArrayList<AddressModel>();
		for (final AddressModel address : b2bUnit.getAddresses())
		{
			// check whether B2B unit address is a shipping address
			if (Boolean.TRUE.equals(address.getShippingAddress()))
			{
				addresses.add(address);
			}
		}
		return addresses;
	}
}
