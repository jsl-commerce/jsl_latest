/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.configurablebundleservice.cart.service.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.strategies.NetGrossStrategy;
import de.hybris.platform.configurablebundleservices.bundle.BundleTemplateService;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.order.OrderManager;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.jsl.configurablebundleservice.cart.service.JslBundleCommerceCartService;
import com.jsl.core.enums.JslCartType;


/**
 *
 */
public class JslBundleCommerceCartServiceImpl implements JslBundleCommerceCartService
{

	private KeyGenerator keyGenerator;
	private ModelService modelService;
	private CommonI18NService commonI18NService;
	private BundleTemplateService bundleTemplateService;
	private Converter<CommerceCartModification, CartModificationData> cartModificationConverter;
	private CommerceCartService commerceCartService;

	private I18NService i18nService;
	private B2BUnitService<B2BUnitModel, B2BCustomerModel> b2bUnitService;

	private NetGrossStrategy netGrossStrategy;
	private BaseSiteService baseSiteService;
	private BaseStoreService baseStoreService;
	private KeyGenerator guidKeyGenerator;


	public static final Logger LOG = Logger.getLogger(JslBundleCommerceCartServiceImpl.class);


	@Override
	public List<CartModificationData> addBundle(final String bundleTemplateId, final CartModel cartModel)
			throws CommerceCartModificationException
	{

		LOG.info("Adding bundle to cart bundleId: " + bundleTemplateId);
		final List<CartModificationData> cartModifications = new ArrayList<>();
		final CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setEnableHooks(true);
		parameter.setCart(cartModel);
		parameter.setEntryGroupNumbers(Collections.emptySet());
		parameter.setBundleTemplate(getBundleTemplateService().getBundleTemplateForCode(bundleTemplateId));
		final BundleTemplateModel bt = getBundleTemplateService().getBundleTemplateForCode(bundleTemplateId);
		for (final ProductModel p : bt.getProducts())
		{
			parameter.setProduct(p);
			parameter.setQuantity(1);
			cartModifications.add(getCartModificationConverter().convert(getCommerceCartService().addToCart(parameter)));
		}

		return cartModifications;
	}


	private CartModel createAuctionCart(final UserModel user)
	{
		// XXX Auto-generated method stub
		final CartModel cartModel = createCartInternal(user);
		postProcessCart(cartModel);
		return cartModel;
	}

	@Override
	public CartModel getAuctionCart(final UserModel user)
	{
		final Collection<CartModel> carts = user.getCarts();
		if (!carts.isEmpty())
		{
			for (final CartModel cart : carts)
			{
				if (JslCartType.AUCTION.equals(cart.getType()))
				{
					return cart;
				}
			}
		}

		return createAuctionCart(user);
	}


	protected CartModel createCartInternal(final UserModel customer) throws ModelSavingException
	{

		final BaseStoreModel baseStore = getBaseStoreService().getBaseStoreForUid("jsl");

		final String cartModelTypeCode = Config.getString(JaloSession.CART_TYPE, "Cart");

		final CartModel cart = modelService.create(cartModelTypeCode);
		cart.setCode(String.valueOf(keyGenerator.generate()));
		cart.setUser(customer);
		cart.setStore(baseStore);
		cart.setSite(getBaseSiteService().getBaseSiteForUID("jsl"));
		cart.setCurrency(baseStore.getDefaultCurrency());
		cart.setDate(new Date());
		cart.setNet(isNetUser(customer));
		cart.setType(JslCartType.AUCTION);
		cart.setNet(Boolean.valueOf(getNetGrossStrategy().isNet()));
		cart.setGuid(getGuidKeyGenerator().generate().toString());

		modelService.save(cart);
		return cart;
	}


	protected void postProcessCart(final CartModel cart)
	{
		cart.setLocale(getI18nService().getCurrentLocale().toString());
		cart.setStatus(OrderStatus.CREATED);
		if (isB2BCart(cart))
		{
			final B2BUnitModel unit = getB2bUnitService().getParent((B2BCustomerModel) cart.getUser());
			Assert.notNull(unit,
					String.format("No B2BUnit associated to cart %s created by %s", cart.getCode(), cart.getUser().getUid()));
			cart.setUnit(unit);

			if (LOG.isDebugEnabled())
			{
				LOG.debug(
						String.format("Setting B2BUnit %s on Cart %s created by %s", unit.getUid(), cart.getCode(), cart.getUser()));
			}


		}
		getModelService().save(cart);
	}

	public boolean isB2BCart(final CartModel cart)
	{
		return cart.getUser() instanceof B2BCustomerModel;
	}



	private Boolean isNetUser(final UserModel user)
	{
		final User userItem = modelService.getSource(user);
		final boolean result = OrderManager.getInstance().getPriceFactory().isNetUser(userItem);
		return Boolean.valueOf(result);
	}

	/**
	 * @return the keyGenerator
	 */
	public KeyGenerator getKeyGenerator()
	{
		return keyGenerator;
	}

	/**
	 * @param keyGenerator
	 *           the keyGenerator to set
	 */
	public void setKeyGenerator(final KeyGenerator keyGenerator)
	{
		this.keyGenerator = keyGenerator;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * @return the commonI18NService
	 */
	public CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	/**
	 * @param commonI18NService
	 *           the commonI18NService to set
	 */
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}



	/**
	 * @return the bundleTemplateService
	 */
	public BundleTemplateService getBundleTemplateService()
	{
		return bundleTemplateService;
	}


	/**
	 * @param bundleTemplateService
	 *           the bundleTemplateService to set
	 */
	@Required
	public void setBundleTemplateService(final BundleTemplateService bundleTemplateService)
	{
		this.bundleTemplateService = bundleTemplateService;
	}


	/**
	 * @return the cartModificationConverter
	 */
	public Converter<CommerceCartModification, CartModificationData> getCartModificationConverter()
	{
		return cartModificationConverter;
	}


	/**
	 * @param cartModificationConverter
	 *           the cartModificationConverter to set
	 */
	@Required
	public void setCartModificationConverter(
			final Converter<CommerceCartModification, CartModificationData> cartModificationConverter)
	{
		this.cartModificationConverter = cartModificationConverter;
	}


	/**
	 * @return the commerceCartService
	 */
	public CommerceCartService getCommerceCartService()
	{
		return commerceCartService;
	}


	/**
	 * @param commerceCartService
	 *           the commerceCartService to set
	 */
	@Required
	public void setCommerceCartService(final CommerceCartService commerceCartService)
	{
		this.commerceCartService = commerceCartService;
	}


	/**
	 * @return the b2bUnitService
	 */
	public B2BUnitService<B2BUnitModel, B2BCustomerModel> getB2bUnitService()
	{
		return b2bUnitService;
	}


	/**
	 * @param b2bUnitService
	 *           the b2bUnitService to set
	 */
	public void setB2bUnitService(final B2BUnitService<B2BUnitModel, B2BCustomerModel> b2bUnitService)
	{
		this.b2bUnitService = b2bUnitService;
	}


	/**
	 * @return the i18nService
	 */
	public I18NService getI18nService()
	{
		return i18nService;
	}


	/**
	 * @param i18nService
	 *           the i18nService to set
	 */
	public void setI18nService(final I18NService i18nService)
	{
		this.i18nService = i18nService;
	}


	/**
	 * @return the netGrossStrategy
	 */
	public NetGrossStrategy getNetGrossStrategy()
	{
		return netGrossStrategy;
	}


	/**
	 * @param netGrossStrategy
	 *           the netGrossStrategy to set
	 */
	public void setNetGrossStrategy(final NetGrossStrategy netGrossStrategy)
	{
		this.netGrossStrategy = netGrossStrategy;
	}


	/**
	 * @return the baseSiteService
	 */
	public BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}


	/**
	 * @param baseSiteService
	 *           the baseSiteService to set
	 */
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}


	/**
	 * @return the baseStoreService
	 */
	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}


	/**
	 * @param baseStoreService
	 *           the baseStoreService to set
	 */
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}


	/**
	 * @return the guidKeyGenerator
	 */
	public KeyGenerator getGuidKeyGenerator()
	{
		return guidKeyGenerator;
	}


	/**
	 * @param guidKeyGenerator
	 *           the guidKeyGenerator to set
	 */
	public void setGuidKeyGenerator(final KeyGenerator guidKeyGenerator)
	{
		this.guidKeyGenerator = guidKeyGenerator;
	}



}
