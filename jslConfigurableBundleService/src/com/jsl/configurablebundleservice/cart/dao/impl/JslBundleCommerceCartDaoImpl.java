/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.configurablebundleservice.cart.dao.impl;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;

import java.util.List;

import com.jsl.configurablebundleservice.cart.dao.JslBundleCommerceCartDao;




/**
 *
 */
public class JslBundleCommerceCartDaoImpl extends AbstractItemDao implements JslBundleCommerceCartDao
{

	@Override
	public List<CartModel> getAuctionCart(final UserModel user)
	{
		// XXX Auto-generated method stub
		return null;
	}

}
