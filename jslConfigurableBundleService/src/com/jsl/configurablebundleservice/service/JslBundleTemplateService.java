/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.configurablebundleservice.service;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.configurablebundleservices.bundle.BundleTemplateService;
import de.hybris.platform.configurablebundleservices.model.BundleSelectionCriteriaModel;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateStatusModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import javax.annotation.Nonnull;

import com.jsl.jslConfigurableBundleService.model.WatchListModel;


/**
 *
 */
public interface JslBundleTemplateService extends BundleTemplateService
{

	List<BundleTemplateModel> getBundleTemplatesForAuctionId(@Nonnull	final String auctionId);

	BundleTemplateModel createBundleTemplate(String name, List<ProductModel> productsList, CatalogVersionModel catalogVersion);

	BundleSelectionCriteriaModel getBundleSelectionCriteriaForCode(String id, CatalogVersionModel catlogVersion);

	BundleTemplateStatusModel getBundleTemplateStatusForCode(String id, CatalogVersionModel catlogVersion);

	List<BundleTemplateModel> getAuctionList();

	List<BundleTemplateModel> getOngoingAuctionDetails(String auctionId);

	List<BundleTemplateModel> getCompletedAuctionDetails(String flowName);

	List<BundleTemplateModel> getUpcomingAuctionDetails();

	BundleTemplateModel getBundleTemplateForBundleId(String bundleId, String auctionId);

	List<WatchListModel> getWatchListDetails(String auctionID);

	boolean removeFromWatchList(BundleTemplateModel bundleTemplate);

	int addToWatchList(BundleTemplateModel bundleTemplate, String lotIndex);

	List<ProductModel> getProductListByBundleId(String bundleId);

	void releaseStockofUnsoldBundle(String auctionId);

	void resetCounter();

	List<BundleTemplateModel> getCompletedAuctionDetailsByDateRange(String startDate, String endDate);

	List<OrderModel> getOrderByAuctionId(String auctionId);

	String getOrderApprovalStatusForBundle(BundleTemplateModel bundleTemplate);

	List<BundleTemplateModel> getCompletedAuctionDetailsForAdmin();

	List<BundleTemplateModel> getCompletedAuctionDetailsForAdminByDateRange(String startDate, String endDate);

	List<BundleTemplateModel> getCompletedAuctionLotWithoutBid();

	List<BundleTemplateModel> getCompletedAuctionLotWithoutBidByDateRange(String startDate, String endDate);

}
