/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.configurablebundleservice.service.impl;

import de.hybris.platform.configurablebundlecockpits.servicelayer.services.impl.DefaultBundleNavigationService;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.jsl.core.model.AuctionProductModel;


/**
 *
 */
public class JslBundleNavigationServiceImpl extends DefaultBundleNavigationService
{
	@Override
	public void add(final BundleTemplateModel bundleTemplateModel, final Collection<ProductModel> productModels)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("bundleTemplateModel", bundleTemplateModel);
		ServicesUtil.validateParameterNotNullStandardMessage("productModels", productModels);
		final List<ProductModel> existingProducts = new ArrayList(bundleTemplateModel.getProducts());
		final Iterator var5 = productModels.iterator();

		while (var5.hasNext())
		{
			final ProductModel productToAdd = (ProductModel) var5.next();
			if (!existingProducts.contains(productToAdd) && (productToAdd instanceof AuctionProductModel)
					&& !((AuctionProductModel) productToAdd).getBundled())
			{
				existingProducts.add(productToAdd);
				((AuctionProductModel) productToAdd).setBundled(true);
				bundleTemplateModel.setProducts(existingProducts);
				bundleTemplateModel.setQuantity(getTotalQuantity(existingProducts));
				this.getModelService().save(productToAdd);
				this.getModelService().save(bundleTemplateModel);
			}
		}
	}

	private Double getTotalQuantity(final List<ProductModel> productsList)
	{
		Double quantity = 0.0;
		for (final ProductModel product : productsList)
		{
			quantity += product.getQuantity();
		}
		return quantity;
	}


	@Override
	public void remove(final BundleTemplateModel bundleTemplateModel, final ProductModel productModel)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("bundleTemplateModel", bundleTemplateModel);
		ServicesUtil.validateParameterNotNullStandardMessage("productModel", productModel);
		final List<ProductModel> items = new ArrayList(bundleTemplateModel.getProducts());
		items.remove(productModel);
		bundleTemplateModel.setProducts(items);
		bundleTemplateModel.setQuantity(getTotalQuantity(items));
		this.getModelService().save(bundleTemplateModel);
		if (productModel instanceof AuctionProductModel)
		{
			((AuctionProductModel) productModel).setBundled(false);
			this.getModelService().save(productModel);
		}

	}


}
