/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.configurablebundleservice.service.impl;

import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;

import java.util.List;

import javax.annotation.Resource;

import com.jsl.configurablebundleservice.daos.impl.JslBundleTemplateExportExcelDaoImpl;
import com.jsl.configurablebundleservice.service.JslBundleTemplateExportExcelService;


/**
 *
 */
public class JslBundleTemplateExportExcelServiceImpl implements JslBundleTemplateExportExcelService
{


	@Resource(name = "jslBundleTemplateExportExcelDaoImpl")
	private JslBundleTemplateExportExcelDaoImpl jslBundleTemplateExportExcelDaoImpl;

	@Override
	public List<BundleTemplateModel> getAllBundleTemplates()
	{
		return jslBundleTemplateExportExcelDaoImpl.getAllBundleTemplates();
	}

}
