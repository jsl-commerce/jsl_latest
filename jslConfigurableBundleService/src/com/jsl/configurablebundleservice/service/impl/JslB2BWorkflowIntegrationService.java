/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.configurablebundleservice.service.impl;

import de.hybris.platform.b2b.enums.WorkflowTemplateType;
import de.hybris.platform.b2b.services.impl.DefaultB2BWorkflowIntegrationService;
import de.hybris.platform.b2b.strategies.WorkflowTemplateStrategy;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.workflow.model.WorkflowTemplateModel;

import java.util.List;

import org.springframework.util.Assert;


/**
 *
 */
public class JslB2BWorkflowIntegrationService extends DefaultB2BWorkflowIntegrationService
{


	@Override
	public WorkflowTemplateModel createWorkflowTemplate(final List<? extends UserModel> users, final String code,
			final String description, final WorkflowTemplateType templateType)
	{
		WorkflowTemplateModel workflowTemplateModel = getWorkflowTemplateForCode(code);
		if (workflowTemplateModel == null)
		{
			synchronized (this)
			{
				workflowTemplateModel = getWorkflowTemplateForCode(code);
				if (workflowTemplateModel == null)
				{
					final WorkflowTemplateStrategy strategy = getWorkflowTempateStrategy(templateType);
					Assert.notNull(strategy, String.format("Expected to find a strategy of type %s for with type %s",
							WorkflowTemplateStrategy.class, templateType));
					workflowTemplateModel = strategy.createWorkflowTemplate(users, code, description);
				}
			}
		}
		return workflowTemplateModel;
	}
}
