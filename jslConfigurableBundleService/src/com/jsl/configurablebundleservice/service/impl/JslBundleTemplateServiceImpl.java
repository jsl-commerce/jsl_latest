/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.configurablebundleservice.service.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.configurablebundleservices.bundle.impl.DefaultBundleTemplateService;
import de.hybris.platform.configurablebundleservices.enums.BundleTemplateStatusEnum;
import de.hybris.platform.configurablebundleservices.model.BundleSelectionCriteriaModel;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateStatusModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.user.UserService;

import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.WordUtils;
import org.apache.log4j.Logger;
import org.assertj.core.util.Strings;
import org.springframework.beans.factory.annotation.Required;

import com.jsl.configurablebundleservice.constants.JslConfigurableBundleServiceConstants;
import com.jsl.configurablebundleservice.daos.JslBundleTemplateDao;
import com.jsl.configurablebundleservice.exception.JslBundleException;
import com.jsl.configurablebundleservice.service.JslBundleTemplateService;
import com.jsl.core.constants.JslAuctionEnum;
import com.jsl.core.enums.AuctionStatus;
import com.jsl.core.exceptions.JslAuctionException;
import com.jsl.core.model.AuctionEventModel;
import com.jsl.core.model.AuctionProductModel;
import com.jsl.core.services.JslAuctionService;
import com.jsl.jslConfigurableBundleService.model.WatchListModel;



/**
 *
 */
public class JslBundleTemplateServiceImpl extends DefaultBundleTemplateService implements JslBundleTemplateService
{

	private KeyGenerator bundleKeyGenerator;
	private JslBundleTemplateDao jslBundleTemplateDao;
	private JslAuctionService jslAuctionService;
	private UserService userService;
	private final List<Integer> auctionIDList = new ArrayList<>();


	private static final Logger LOG = Logger.getLogger(JslBundleTemplateServiceImpl.class);

	@Override
	public void resetCounter()
	{
		auctionIDList.clear();
	}

	@Override
	public BundleTemplateModel createBundleTemplate(final String name, final List<ProductModel> productsList,
			final CatalogVersionModel catalogVersion)
	{
		AuctionEventModel auction = null;
		try
		{
			if (!productsList.isEmpty())
			{
				final String location = productsList.get(0).getLocation(); // All products of same location
				auction = getActiveAuction(location, catalogVersion);
				if (auction != null)
				{
					auctionIDList.add(auction.getAuctionId());
					final BundleTemplateModel rootTemplate = getModelService().create(BundleTemplateModel.class);
					rootTemplate.setId(getBundleUniqueId());
					rootTemplate.setName("Lot-" + Collections.frequency(auctionIDList, auction.getAuctionId()));
					rootTemplate.setCatalogVersion(catalogVersion);
					rootTemplate.setVersion(JslConfigurableBundleServiceConstants.JSL_BUNDLE_VERSION);
					final BundleTemplateStatusModel status = getModelService().create(BundleTemplateStatusModel.class);
					status.setId(rootTemplate.getId());
					status.setCatalogVersion(catalogVersion);
					status.setStatus(BundleTemplateStatusEnum.APPROVED);
					rootTemplate.setStatus(status);
					rootTemplate.setAuctionEvent(auction);

					final BundleTemplateModel childComponent = getModelService().create(BundleTemplateModel.class);
					childComponent.setId(getBundleUniqueId());
					childComponent.setName("Lot-" + Collections.frequency(auctionIDList, auction.getAuctionId()));
					childComponent.setCatalogVersion(catalogVersion);
					childComponent.setVersion(JslConfigurableBundleServiceConstants.JSL_BUNDLE_VERSION);
					childComponent.setStatus(status);
					childComponent.setParentTemplate(rootTemplate);
					childComponent.setBundleSelectionCriteria(getBundleSelectionCriteriaForCode(
							JslConfigurableBundleServiceConstants.JSL_BUNDLESELECTIONCRITERIA_ID, catalogVersion));
					childComponent.setProducts(productsList);
					childComponent.setAuctionEvent(auction);
					childComponent.setQuantity(getTotalQuantity(productsList));

					getModelService().save(status);
					getModelService().save(rootTemplate);
					getModelService().save(childComponent);

					setBundledTrue(productsList);
					return childComponent;
				}
				else
				{
					LOG.info("No Active auction found for location : " + location);
					return null;
				}
			}
		}
		catch (final Exception ex)
		{
			throw new JslBundleException(
					MessageFormat.format("Exception occure while creating bundle for auction : {0} and exception is {1} ",
							auction.getAuctionId() != null ? auction.getAuctionId() : "null", ex));
		}
		return null;
	}

	/**
	 *
	 */
	private Double getTotalQuantity(final List<ProductModel> productsList)
	{
		Double quantity = 0.0;
		for (final ProductModel product : productsList)
		{
			quantity += product.getQuantity();
		}
		return quantity;
	}

	private AuctionEventModel getActiveAuction(final String location, final CatalogVersionModel catalogVersion)
	{

		List<AuctionEventModel> auctions = getJslAuctionService().getAuctions(AuctionStatus.READYFORBUNDLE);
		if (auctions == null || auctions.isEmpty())
		{
			auctions = getJslAuctionService().getAuctions(AuctionStatus.READYFOROTP);
		}
		for (final AuctionEventModel auction : auctions)
		{
			if (auction.getAuctionGroup().getValidPhysicalLocations().contains(location)
					&& auction.getCatalogVersion().equals(catalogVersion))
			{
				return auction;
			}
		}
		return null;
	}


	private void setBundledTrue(final List<ProductModel> productsList)
	{
		for (final ProductModel p : productsList)
		{
			if (p instanceof AuctionProductModel)
			{
				((AuctionProductModel) p).setBundled(true);
				getModelService().save(p);
			}
		}
	}


	@Override
	public BundleSelectionCriteriaModel getBundleSelectionCriteriaForCode(final String id, final CatalogVersionModel catlogVersion)
	{
		return getJslBundleTemplateDao().findBundleSelectionCriteriaById(id, catlogVersion);
	}


	@Override
	public BundleTemplateStatusModel getBundleTemplateStatusForCode(final String id, final CatalogVersionModel catlogVersion)
	{
		return getJslBundleTemplateDao().findBundleTemplateStatusById(id, catlogVersion);
	}


	@Override
	public List<BundleTemplateModel> getBundleTemplatesForAuctionId(final String auctionId)
	{
		final List<BundleTemplateModel> approvedBundleTemplateWithProdList = new ArrayList<>();
		final List<BundleTemplateModel> bundleTemplateList = getJslBundleTemplateDao().getBundleTemplatesForAuctionId(auctionId);
		for (final BundleTemplateModel bundleTemplate : bundleTemplateList)
		{
			if (String.valueOf(bundleTemplate.getStatus().getStatus()).equalsIgnoreCase("Approved"))
			{
				final List<ProductModel> pList = getProductListByBundleId(bundleTemplate.getId());
				bundleTemplate.setProducts(pList);
				approvedBundleTemplateWithProdList.add(bundleTemplate);
			}
		}

		return approvedBundleTemplateWithProdList;
	}



	@Override
	public List<BundleTemplateModel> getAuctionList()
	{
		final Timestamp curDatetime = new Timestamp(System.currentTimeMillis());
		return getJslBundleTemplateDao().getAuctionList(curDatetime.toString());
	}


	@Override
	public List<BundleTemplateModel> getCompletedAuctionDetails(final String flowName)
	{
		List<BundleTemplateModel> bundleTemplateList = null;
		final List<BundleTemplateModel> bundleWonByUserList = new ArrayList<>();
		final Timestamp curDatetime = new Timestamp(System.currentTimeMillis());
		if (flowName.equalsIgnoreCase("completedPage"))
		{
			bundleTemplateList = getJslBundleTemplateDao().getCompletedAuctionDetailsForCurrentDate(curDatetime.toString());
		}
		else
		{
			bundleTemplateList = getJslBundleTemplateDao().getCompletedAuctionDetails();
		}
		for (final BundleTemplateModel bundleTemplate : bundleTemplateList)
		{
			getCompletedAuctionForCurrentUser(bundleWonByUserList, bundleTemplate);
		}

		return bundleWonByUserList;
	}

	@Override
	public List<BundleTemplateModel> getCompletedAuctionDetailsByDateRange(final String startDate, final String endDate)
	{
		List<BundleTemplateModel> bundleTemplateList = null;
		final List<BundleTemplateModel> bundleWonByUserList = new ArrayList<>();
		//	bundleTemplateList = getJslBundleTemplateDao().getCompletedAuctionDetailsByDateRange(startDate, endDate);
		bundleTemplateList = getJslBundleTemplateDao().getCompletedAuctionDetailsByDateForUser(startDate);
		for (final BundleTemplateModel bundleTemplate : bundleTemplateList)
		{
			getCompletedAuctionForCurrentUser(bundleWonByUserList, bundleTemplate);
		}

		return bundleWonByUserList;
	}

	@Override
	public List<BundleTemplateModel> getUpcomingAuctionDetails()
	{
		final Timestamp curDatetime = new Timestamp(System.currentTimeMillis());
		return getJslBundleTemplateDao().getUpcomingAuctionDetails(curDatetime.toString());
	}


	@Override
	public List<BundleTemplateModel> getOngoingAuctionDetails(final String auctionId)
	{
		List<BundleTemplateModel> bundleTemplateList = null;
		final List<BundleTemplateModel> approvedbundleTemplateList = new ArrayList<>();
		final Timestamp curDatetime = new Timestamp(System.currentTimeMillis());

		bundleTemplateList = getJslBundleTemplateDao().getOngoingAuctionDetails(curDatetime.toString(), auctionId);
		for (final BundleTemplateModel bundleTemplate : bundleTemplateList)
		{
			if (String.valueOf(bundleTemplate.getStatus().getStatus()).equalsIgnoreCase("Approved"))
			{
				final List<ProductModel> pList = getProductListByBundleId(bundleTemplate.getId());
				bundleTemplate.setProducts(pList);
				approvedbundleTemplateList.add(bundleTemplate);
			}
		}
		LOG.debug("JslBundleTemplateServiceImpl.class getOngoingAuctionDetails() bundleTemplateDataList size: "
				+ approvedbundleTemplateList.size());

		return approvedbundleTemplateList;
	}

	@Override
	public BundleTemplateModel getBundleTemplateForBundleId(final String bundleId, final String auctionId)
	{
		return getJslBundleTemplateDao().getBundleTemplateForBundleId(bundleId, auctionId);
	}

	@Override
	public List<WatchListModel> getWatchListDetails(final String auctionID)
	{
		final Timestamp curDatetime = new Timestamp(System.currentTimeMillis());
		final List<WatchListModel> watchListWithBundleProd = new ArrayList<>();
		final List<WatchListModel> watchList = getJslBundleTemplateDao().getWatchListDetails(getUserService().getCurrentUser(),
				curDatetime.toString(), auctionID);
		for (final WatchListModel watchListModel : watchList)
		{
			final List<ProductModel> pList = getProductListByBundleId(watchListModel.getBundleTemplate().getId());
			watchListModel.getBundleTemplate().setProducts(pList);
			watchListWithBundleProd.add(watchListModel);
		}
		return watchListWithBundleProd;
	}

	@Override
	public int addToWatchList(final BundleTemplateModel bundleTemplate, final String lotIndex)
	{
		int isAdded = 0;
		if (!bundleTemplate.getAuctionEvent().getAuctionStatus().getCode().equalsIgnoreCase("STARTED"))
		{
			LOG.error(MessageFormat.format(
					"Cannot add to watchlist lot ID: {0} auction id: {1} as auction status is {2} for userid: {3}",
					bundleTemplate.getId(), bundleTemplate.getAuctionEvent().getAuctionId(),
					bundleTemplate.getAuctionEvent().getAuctionStatus().getCode(), getUserService().getCurrentUser().getUid()));

			throw new JslAuctionException(JslAuctionEnum.AUCTION_NOT_IN_STARTED_STATE.getResponseCode(),
					JslAuctionEnum.AUCTION_NOT_IN_STARTED_STATE.getResponseDescription(), "addToWatchList", bundleTemplate);
		}
		final WatchListModel watchListModel = getJslBundleTemplateDao().getWatchListDetailsToAddOrRemove(bundleTemplate,
				getUserService().getCurrentUser());
		if (null == watchListModel || watchListModel.getBundleTemplate().getId() != bundleTemplate.getId())
		{
			final WatchListModel watchList = getModelService().create(WatchListModel.class);
			watchList.setWatchListId(lotIndex + "-" + getWatchListUniqueId());
			watchList.setAuctionEvent(bundleTemplate.getAuctionEvent());
			watchList.setBundleTemplate(bundleTemplate);
			watchList.setCreatedBy(getUserService().getCurrentUser());

			getModelService().save(watchList);
			isAdded = 1;
		}
		else
		{
			/* already exists */
			isAdded = 2;
			throw new JslAuctionException(JslAuctionEnum.BUNDLE_ALREADY_PRESENT_IN_WATCHLIST_ERROR.getResponseCode(),
					JslAuctionEnum.BUNDLE_ALREADY_PRESENT_IN_WATCHLIST_ERROR.getResponseDescription(), "addToWatchList",
					bundleTemplate);
		}

		return isAdded;
	}

	@Override
	public boolean removeFromWatchList(final BundleTemplateModel bundleTemplate)
	{
		boolean isRemoved = false;
		final WatchListModel watchList = getJslBundleTemplateDao().getWatchListDetailsToAddOrRemove(bundleTemplate,
				getUserService().getCurrentUser());
		if (null != watchList)
		{
			getModelService().remove(watchList);
			isRemoved = true;
		}

		return isRemoved;
	}

	@Override
	public List<ProductModel> getProductListByBundleId(final String bundleId)
	{
		return getJslBundleTemplateDao().getProductListByBundleId(bundleId);
	}

	@Override
	public void releaseStockofUnsoldBundle(final String auctionId)
	{
		List<ProductModel> productList = null;
		final List<BundleTemplateModel> unsoldBundleList = getJslBundleTemplateDao().getUnsoldBundle(auctionId);
		for (final BundleTemplateModel bundleTemplate : unsoldBundleList)
		{
			productList = bundleTemplate.getProducts();
			for (final ProductModel product : productList)
			{
				if (product instanceof AuctionProductModel)
				{
					((AuctionProductModel) product).setBundled(false);
					getModelService().save(product);
				}
			}
		}
	}

	@Override
	public List<OrderModel> getOrderByAuctionId(final String auctionId)
	{
		return getJslBundleTemplateDao().getOrderByAuctionId(auctionId);
	}

	@Override
	public String getOrderApprovalStatusForBundle(final BundleTemplateModel bundleTemplate)
	{
		String status = null;
		try
		{
			final AuctionEventModel auctionEvent = bundleTemplate.getAuctionEvent();
			if (auctionEvent.getAuctionStatus().getCode().equalsIgnoreCase(AuctionStatus.COMPLETED.getCode()))
			{
				final List<OrderModel> orderList = getOrderByAuctionId(String.valueOf(auctionEvent.getAuctionId()));

				for (final OrderModel order : orderList)
				{
					if (order.getEntryGroups().get(0).getChildren().get(0).getExternalReferenceId()
							.equalsIgnoreCase(bundleTemplate.getId()) && Strings.isNullOrEmpty(order.getVersionID()))
					{
						status = WordUtils.capitalize(order.getStatusDisplay().replaceAll("\\.", " "));
					}
				}
			}
		}
		catch (final Exception e)
		{
			LOG.error("Exception in JslBundleTemplatePopulator.setOrderStatusForBundle()", e);
		}

		return status;
	}

	@Override
	public List<BundleTemplateModel> getCompletedAuctionDetailsForAdmin()
	{
		List<BundleTemplateModel> bundleTemplateList = null;
		final List<BundleTemplateModel> bundleWonByUserList = new ArrayList<>();
		final Timestamp curDatetime = new Timestamp(System.currentTimeMillis());
		bundleTemplateList = getJslBundleTemplateDao().getCompletedAuctionDetailsForAdmin();
		for (final BundleTemplateModel bundleTemplate : bundleTemplateList)
		{
			getCompletedAuctionForAdminUser(bundleWonByUserList, bundleTemplate);
		}

		return bundleWonByUserList;
	}

	@Override
	public List<BundleTemplateModel> getCompletedAuctionDetailsForAdminByDateRange(final String startDate, final String endDate)
	{
		List<BundleTemplateModel> bundleTemplateList = null;
		final List<BundleTemplateModel> bundleWonByUserList = new ArrayList<>();
		//bundleTemplateList = getJslBundleTemplateDao().getCompletedAuctionDetailsByDateRange(startDate, endDate);
		bundleTemplateList = getJslBundleTemplateDao().getCompletedAuctionDetailsByDateForUser(startDate);
		for (final BundleTemplateModel bundleTemplate : bundleTemplateList)
		{
			getCompletedAuctionForAdminUser(bundleWonByUserList, bundleTemplate);
		}

		return bundleWonByUserList;
	}

	@Override
	public List<BundleTemplateModel> getCompletedAuctionLotWithoutBid()
	{
		List<BundleTemplateModel> bundleTemplateList = null;
		final List<BundleTemplateModel> bundleWithoutBidList = new ArrayList<>();
		bundleTemplateList = getJslBundleTemplateDao().getCompletedAuctionLotWithoutBid();
		for (final BundleTemplateModel bundleTemplate : bundleTemplateList)
		{
			getCompletedAuctionLotWithoutBid(bundleWithoutBidList, bundleTemplate);
		}
		return bundleWithoutBidList;
	}

	@Override
	public List<BundleTemplateModel> getCompletedAuctionLotWithoutBidByDateRange(final String startDate, final String endDate)
	{
		List<BundleTemplateModel> bundleTemplateList = null;
		final List<BundleTemplateModel> bundleWithoutBidList = new ArrayList<>();
		bundleTemplateList = getJslBundleTemplateDao().getCompletedAuctionLotWithoutBidByDateRange(startDate, endDate);
		for (final BundleTemplateModel bundleTemplate : bundleTemplateList)
		{
			getCompletedAuctionLotWithoutBid(bundleWithoutBidList, bundleTemplate);
		}
		return bundleWithoutBidList;
	}

	private void getCompletedAuctionForCurrentUser(final List<BundleTemplateModel> bundleWonByUserList,
			final BundleTemplateModel bundleTemplate)
	{
		try
		{
			if (String.valueOf(bundleTemplate.getStatus().getStatus()).equalsIgnoreCase("Approved"))
			{
				if (null != bundleTemplate.getBidEvent() && null != bundleTemplate.getBidEvent().getCreatedBy())
				{
					final B2BCustomerModel createdBy = (B2BCustomerModel) bundleTemplate.getBidEvent().getCreatedBy();
					if (createdBy.getOriginalUid()
							.equalsIgnoreCase(((B2BCustomerModel) getUserService().getCurrentUser()).getOriginalUid()))
					{
						final List<ProductModel> pList = getProductListByBundleId(bundleTemplate.getId());
						bundleTemplate.setProducts(pList);
						bundleWonByUserList.add(bundleTemplate);
					}
				}
			}
		}
		catch (final Exception e)
		{
			LOG.error("Exception in JslBundleTemplateService.getCompletedAuctionForCurrentUser()", e);
		}
	}


	private void getCompletedAuctionForAdminUser(final List<BundleTemplateModel> bundleWonByUserList,
			final BundleTemplateModel bundleTemplate)
	{
		try
		{
			if (String.valueOf(bundleTemplate.getStatus().getStatus()).equalsIgnoreCase("Approved"))
			{
				if (null != bundleTemplate.getBidEvent())
				{
					//final List<ProductModel> pList = getProductListByBundleId(bundleTemplate.getId());
					//bundleTemplate.setProducts(pList);
					bundleWonByUserList.add(bundleTemplate);
				}
			}
		}
		catch (final Exception e)
		{
			LOG.error("Exception in JslBundleTemplateService.getCompletedAuctionForAdminUser()", e);
		}
	}

	private void getCompletedAuctionLotWithoutBid(final List<BundleTemplateModel> bundleWithoutBidList,
			final BundleTemplateModel bundleTemplate)
	{
		try
		{
			if (String.valueOf(bundleTemplate.getStatus().getStatus()).equalsIgnoreCase("Approved"))
			{
				final List<ProductModel> pList = getProductListByBundleId(bundleTemplate.getId());
				bundleTemplate.setProducts(pList);
				bundleWithoutBidList.add(bundleTemplate);
			}
		}
		catch (final Exception e)
		{
			LOG.error("Exception in JslBundleTemplateService.getCompletedAuctionLotWithoutBid()", e);
		}
	}

	private String getWatchListUniqueId()
	{
		return (String) getBundleKeyGenerator().generate();
	}


	private synchronized String getBundleUniqueId()
	{
		return (String) getBundleKeyGenerator().generate();
	}


	public KeyGenerator getBundleKeyGenerator()
	{
		return bundleKeyGenerator;
	}

	@Required
	public void setBundleKeyGenerator(final KeyGenerator bundleKeyGenerator)
	{
		this.bundleKeyGenerator = bundleKeyGenerator;
	}

	/**
	 * @return the jslBundleTemplateDao
	 */
	public JslBundleTemplateDao getJslBundleTemplateDao()
	{
		return jslBundleTemplateDao;
	}

	/**
	 * @param jslBundleTemplateDao
	 *           the jslBundleTemplateDao to set
	 */
	@Required
	public void setJslBundleTemplateDao(final JslBundleTemplateDao jslBundleTemplateDao)
	{
		this.jslBundleTemplateDao = jslBundleTemplateDao;
	}

	/**
	 * @return the jslAuctionService
	 */

	public JslAuctionService getJslAuctionService()
	{
		return jslAuctionService;
	}

	/**
	 * @param jslAuctionService
	 *           the jslAuctionService to set
	 */
	@Required
	public void setJslAuctionService(final JslAuctionService jslAuctionService)
	{
		this.jslAuctionService = jslAuctionService;
	}


	public UserService getUserService()
	{
		return userService;
	}


	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

}
