/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.jsl.configurablebundleservice.constants;

import com.jsl.jslConfigurableBundleService.constants.GeneratedJslConfigurableBundleServiceConstants;


/**
 * Global class for all JslConfigurableBundleService constants. You can add global constants for your extension into
 * this class.
 */
public final class JslConfigurableBundleServiceConstants extends GeneratedJslConfigurableBundleServiceConstants
{
	public static final String EXTENSIONNAME = "jslConfigurableBundleService";

	private JslConfigurableBundleServiceConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "jslConfigurableBundleServicePlatformLogo";

	public static final String JSL_BUNDLE_VERSION = "1.0";
	public static final String JSL_BUNDLESELECTIONCRITERIA_ID = "CustomPackage_Pick1Engraving3";
	public static final String JSL_BUNDLETEMPLATESTATUS_ID = "Status_TOOLKIT";




	//Order constants
	public static final String DEFAULTCURRENCY = "INR";
	public static final String TRANSACTIONTYPE = "ZSLD";
	public static final String SALESORGANIZATION_1000 = "1000";
	public static final String SALESORGANIZATION_1010 = "1010";
	public static final String DISTRIBUTIONCHANNEL = "10";
	public static final String DIVISION = "00";
	public static final String PARTIALSHIPMENT = "Allowed";
	public static final String CLUBBING = "ALLOWED";
	public static final String ZDOCCLUBBING = "Disallowed";
	public static final String ORDERTYPE_PLANT = "ZORD";
	public static final String ORDERTYPE_YARD = "ZDOC";
	public static final String INCOTERM1 = "EXW";
	public static final String LOTSIZE = "20";
	public static final String COILID = "ANY";
	public static final String MATERIALGROUP = "NO";
	public static final String TOP = "TOP";
	public static final String MARKINGONPRODUCT = "YES";
	public static final String ROUTE = "Z00001";
	public static final String MARKINGONSTICKER = "YES";
	public static final String USAGE = "25";
	public static final String STANDARDDELIVERY = "EXW";
	public static final String CUSTOMERDELIVERY = "FHT";
	public static final String FREEONROAD = "FOR";
	public static final String ORDERREASON = "ZHY";
}
