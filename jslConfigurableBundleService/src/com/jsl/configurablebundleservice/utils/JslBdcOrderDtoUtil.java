/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.configurablebundleservice.utils;

import de.hybris.platform.core.model.order.OrderModel;

import java.util.List;

import com.jsl.core.dto.JslOrderExportDto;
import com.jsl.core.dto.JslZdocOrderExportDto;


/**
 *
 */
public interface JslBdcOrderDtoUtil
{

	void createAuctionOrderDtos(final List<OrderModel> orders, final List<JslOrderExportDto> orderDtoList);

	void createZdocOrderDtos(final List<OrderModel> orders, final List<JslZdocOrderExportDto> zdocOrderDtoList);
}
