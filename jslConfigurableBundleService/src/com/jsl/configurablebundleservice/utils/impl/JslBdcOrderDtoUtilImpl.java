/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.configurablebundleservice.utils.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.configurablebundleservices.bundle.BundleTemplateService;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.util.Config;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.slf4j.helpers.MessageFormatter;

import com.jsl.configurablebundleservice.constants.JslConfigurableBundleServiceConstants;
import com.jsl.configurablebundleservice.utils.JslBdcOrderDtoUtil;
import com.jsl.core.dto.JslOrderExportDto;
import com.jsl.core.dto.JslOrderItemExportDto;
import com.jsl.core.dto.JslZdocOrderExportDto;
import com.jsl.core.dto.JslZdocOrderItemExportDto;
import com.jsl.core.model.AuctionEventModel;
import com.jsl.core.services.JslAuctionDeliveryService;
import com.jsl.core.services.JslAuctionService;
import com.jsl.core.util.JslB2bUnitUtil;

import reactor.util.CollectionUtils;


/**
 *
 */
public class JslBdcOrderDtoUtilImpl implements JslBdcOrderDtoUtil
{
	private static final String EMPTY_STRING = "";
	private static final String AUCTION_UNIT_CODE = "auction.b2bunit.code";
	private static final String DEFAULT_B2BUNIT_CODE = "default.b2b.unit.codes";
	private static final String UOM = "MT";
	private static final String N = "N";
	private static final String Y = "Y";
	private static final String SPLITTER = "-";
	private static final String AUCTION = "Auction";
	private static final String ONE = "1";
	private static final String UNDERSCORE = "_";
	private static final String SALES_OFFICE_IN04 = "IN04";
	private int entryNumber = 0;
	private final int zdocEntryNumber = 1;

	B2BUnitService b2bUnitService;
	BundleTemplateService bundleTemplateService;
	JslAuctionService jslAuctionService;
	JslB2bUnitUtil jslB2bUnitUtil;
	JslAuctionDeliveryService jslAuctionDeliveryService;


	private static final SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
	private static final Logger LOG = Logger.getLogger(JslBdcOrderDtoUtilImpl.class);

	@Override
	public void createAuctionOrderDtos(final List<OrderModel> orders, final List<JslOrderExportDto> orderDtoList)
	{
		if (!CollectionUtils.isEmpty(orders))
		{
			orders.forEach(order -> {

				final JslOrderExportDto orderDto = new JslOrderExportDto();
				orderDto.setRefNo(String.valueOf(orders.indexOf(order) + 1));
				orderDto.setOrderCode(order.getCode());
				orderDto.setTransactionType(JslConfigurableBundleServiceConstants.TRANSACTIONTYPE);
				orderDto.setDistributionChannel(JslConfigurableBundleServiceConstants.DISTRIBUTIONCHANNEL);
				orderDto.setDivision(JslConfigurableBundleServiceConstants.DIVISION);
				orderDto.setPartialShipment(JslConfigurableBundleServiceConstants.PARTIALSHIPMENT);
				orderDto.setClubbing(JslConfigurableBundleServiceConstants.CLUBBING);
				orderDto.setOrderReason(JslConfigurableBundleServiceConstants.ORDERREASON);
				final B2BCustomerModel customer = (B2BCustomerModel) order.getUser();

				final String plantPrefix = order.getEntries().size() == 0 ? EMPTY_STRING
						: order.getEntries().get(0).getProduct().getPlant().substring(0, 1);
				if (plantPrefix.equalsIgnoreCase("1") || plantPrefix.equalsIgnoreCase("6"))
				{
					orderDto.setSalesOrganization(JslConfigurableBundleServiceConstants.SALESORGANIZATION_1000);
				}
				else if (plantPrefix.equalsIgnoreCase("5") || plantPrefix.equalsIgnoreCase("7"))
				{
					orderDto.setSalesOrganization(JslConfigurableBundleServiceConstants.SALESORGANIZATION_1010);
				}

				final B2BUnitModel parentb2bUnit = JslB2bUnitUtil.getParentB2bUnit(customer);

				if (parentb2bUnit != null)
				{
					orderDto.setSoldToPartyCode(parentb2bUnit.getUid());

					final B2BUnitModel childb2bUnit = (B2BUnitModel) getB2bUnitService().getUnitForUid(parentb2bUnit.getUid()
							+ UNDERSCORE + orderDto.getSalesOrganization() + UNDERSCORE + Config.getParameter(AUCTION_UNIT_CODE));


					if (childb2bUnit != null)
					{
						orderDto.setSalesOffice(childb2bUnit.getSalesOffice());
						orderDto.setIncoTerm1(childb2bUnit.getIncoterm1());
						orderDto.setIncoTerm2(childb2bUnit.getIncoterm2());
						orderDto.setCamID(childb2bUnit.getKamID());
						orderDto.setPaymentTerm(childb2bUnit.getIlnID());
					}
					else
					{
						LOG.warn(MessageFormatter.format("No B2b Unit found!! for Key {0)", parentb2bUnit.getUid() + UNDERSCORE
								+ orderDto.getSalesOrganization() + UNDERSCORE + Config.getParameter(AUCTION_UNIT_CODE)));
					}

				}
				if (null != order.getDeliveryAddress())
				{
					orderDto.setShipToPartyCode(order.getDeliveryAddress().getSapCustomerID());
				}

				orderDto.setCreationDate(EMPTY_STRING);

				final String lotSize = order.getLotsQuantity() == null ? EMPTY_STRING
						: String.format("%.2f", order.getLotsQuantity());
				orderDto.setLotsSize(lotSize);
				orderDto.setNoOfLots(String.valueOf(order.getEntryGroups().size()));

				orderDto.setSpecialRemark(EMPTY_STRING);
				orderDto.setReference(order.getCode());
				orderDto.setAuctionId(order.getAuctionId());
				final AuctionEventModel auction = getJslAuctionService().getAuctionEventForAuctionId(order.getAuctionId());
				orderDto.setPoDate(formatter.format(auction.getEndDate()));
				final BundleTemplateModel bundleTemplate = getBundleTemplateService()
						.getBundleTemplateForCode(order.getEntryGroups().get(0).getChildren().get(0).getExternalReferenceId());
				orderDto.setLotName(bundleTemplate.getName());
				//	bundleTemplate.getProducts().get(0).getPlant()
				final List<JslOrderItemExportDto> entries = orderDto.getOrderItems();
				order.getEntries().forEach(entry -> {
					final JslOrderItemExportDto orderItemDto = new JslOrderItemExportDto();
					final ProductModel product = entry.getProduct();

					orderItemDto.setRefNo(orderDto.getRefNo());
					orderItemDto.setOrderCode(order.getCode());
					orderItemDto.setItemNo(product.getCode()); // entry number
					orderItemDto.setPlant(product.getPlant());
					orderItemDto.setMaterialCode(product.getMaterialNum());
					orderItemDto.setBatch(product.getCode());
					orderItemDto.setQtyMT(String.valueOf(product.getQuantity()));
					orderItemDto.setUoM(UOM);
					orderItemDto.setRoute(product.getRoute());
					orderItemDto.setStandard(String.valueOf(product.getStandard()));
					orderItemDto.setGradeGroup(product.getGradeGroup());
					orderItemDto.setGrade(product.getGrade());
					orderItemDto.setThk(String.valueOf(product.getThickness()));
					orderItemDto.setWidth(String.valueOf(product.getWidth()));
					orderItemDto.setLength(
							String.valueOf(product.getLength()) == "null" ? EMPTY_STRING : String.valueOf(product.getLength()));
					orderItemDto.setFinish(product.getFinish());
					orderItemDto.setEdgeCon(product.getEdgeCondition());
					orderItemDto.setQuality(product.getQuality());
					orderItemDto.setCoilId(EMPTY_STRING);
					orderItemDto.setUsage(product.getUsageIndicator());
					orderItemDto.setNoOfSheets(product.getNumOfSheets());
					orderItemDto.setPartWtMaximum(ONE);
					orderItemDto.setPartWtMinimum(ONE);
					orderItemDto.setILP(product.getIlp());
					orderItemDto.setPVC(product.getPvc());
					orderItemDto.setiNSP_3RD_PARTY(N);
					orderItemDto.setMarkingOnProduct(String.valueOf(product.getMarkingOnProd()));
					orderItemDto.setMarkSTck(String.valueOf(product.getMarkingOnSticker()));
					orderItemDto.setUtensilUsage(product.getUtensilUsage());
					orderItemDto.setDesired_Date1(product.getDesiredDate()); //Auction Start Date
					final String zprcValue = order.getTotalPrice() == null ? EMPTY_STRING
							: String.format("%.2f", (order.getTotalPrice() - order.getDeliveryCost()) / order.getLotsQuantity());
					orderItemDto.setZPRC(zprcValue); // bid won price
					orderItemDto.setZPRCCurr(JslConfigurableBundleServiceConstants.DEFAULTCURRENCY);
					final String zfrbValue = order.getDeliveryCost() == null ? EMPTY_STRING
							: String.format("%.2f", order.getDeliveryCost() / order.getLotsQuantity());
					orderItemDto.setZFRB(zfrbValue);
					orderItemDto.setZFRBCurr(JslConfigurableBundleServiceConstants.DEFAULTCURRENCY);
					entries.add(orderItemDto);
				});



				orderDtoList.add(orderDto);
			});


		}

	}

	@Override
	public void createZdocOrderDtos(final List<OrderModel> orders, final List<JslZdocOrderExportDto> zdocOrderDtoList)
	{
		entryNumber = 0;
		final List<String> yardList = new ArrayList<>();

		if (!CollectionUtils.isEmpty(orders))
		{
			Integer index = 1;

			for (final OrderModel order : orders)
			{

				if (order.getEntries().get(0).getProduct().getPlant().startsWith("6")
						|| order.getEntries().get(0).getProduct().getPlant().startsWith("7"))
				{

					final List<AbstractOrderEntryModel> orderEntriesPlant6 = order.getEntries().stream()
							.filter(o -> o.getProduct().getPlant().startsWith("6")).collect(Collectors.toList());
					final List<AbstractOrderEntryModel> orderEntriesPlant7 = order.getEntries().stream()
							.filter(o -> o.getProduct().getPlant().startsWith("7")).collect(Collectors.toList());

					if (!orderEntriesPlant6.isEmpty())
					{
						zdocOrderDtoList.add(getDto(order, orderEntriesPlant6, index));
						index++;
					}
					if (!orderEntriesPlant7.isEmpty())
					{
						zdocOrderDtoList.add(getDto(order, orderEntriesPlant7, index));
						index++;
					}

				}
				else
				{
					final List<AbstractOrderEntryModel> orderEntriesYard1 = order.getEntries().stream()
							.filter(o -> o.getProduct().getPlant().startsWith("1")).collect(Collectors.toList());
					final List<AbstractOrderEntryModel> orderEntriesYard5 = order.getEntries().stream()
							.filter(o -> o.getProduct().getPlant().startsWith("5")).collect(Collectors.toList());
					if (!orderEntriesYard1.isEmpty())
					{
						zdocOrderDtoList.add(getDto(order, orderEntriesYard1, index));
						index++;
					}
					if (!orderEntriesYard5.isEmpty())
					{
						zdocOrderDtoList.add(getDto(order, orderEntriesYard5, index));
						index++;
					}

				}
			}

		}
	}

	private JslZdocOrderExportDto getDto(final OrderModel order, final List<AbstractOrderEntryModel> orderEntries,
			final Integer index)
	{

		// orders.indexOf(order) send from above

		//USE a global value for indexing from above method and increase the index when ever calling this method.
		final JslZdocOrderExportDto orderDto = new JslZdocOrderExportDto();

		if (orderEntries.get(0).getProduct().getPlant().startsWith("1")
				|| orderEntries.get(0).getProduct().getPlant().startsWith("5"))
		{
			orderDto.setOrderType(JslConfigurableBundleServiceConstants.ORDERTYPE_PLANT);
		}
		else
		{
			orderDto.setOrderType(JslConfigurableBundleServiceConstants.ORDERTYPE_YARD);
		}

		orderDto.setRefNo(String.valueOf(index));

		orderDto.setOrderCode(order.getCode());
		orderDto.setDistributionChannel(JslConfigurableBundleServiceConstants.DISTRIBUTIONCHANNEL);
		orderDto.setDivision(JslConfigurableBundleServiceConstants.DIVISION);
		orderDto.setPartialShipment(JslConfigurableBundleServiceConstants.PARTIALSHIPMENT);
		orderDto.setClubbing(JslConfigurableBundleServiceConstants.ZDOCCLUBBING);
		orderDto.setLotsSize(JslConfigurableBundleServiceConstants.LOTSIZE);
		orderDto.setOrderReason(JslConfigurableBundleServiceConstants.ORDERREASON);

		final UserModel customer = order.getUser();

		final String plantPrefix = orderEntries.size() == 0 ? EMPTY_STRING
				: orderEntries.get(0).getProduct().getPlant().substring(0, 1);
		if (plantPrefix.equalsIgnoreCase("1") || plantPrefix.equalsIgnoreCase("6"))
		{
			orderDto.setSalesOrganization(JslConfigurableBundleServiceConstants.SALESORGANIZATION_1000);
		}
		else if (plantPrefix.equalsIgnoreCase("5") || plantPrefix.equalsIgnoreCase("7"))
		{
			orderDto.setSalesOrganization(JslConfigurableBundleServiceConstants.SALESORGANIZATION_1010);
		}
		final B2BUnitModel parentb2bUnit = JslB2bUnitUtil.getParentB2bUnit(customer);
		Map<String, B2BUnitModel> childUnits = new HashMap<>(2);
		if (null != parentb2bUnit)
		{
			try
			{
				childUnits = getJslB2bUnitUtil().getChildB2BUnits(parentb2bUnit);
				orderDto.setSoldToPartyCode(parentb2bUnit.getUid());
			}
			catch (final Exception e)
			{
				LOG.error("Exception while getting child b2bunit from parent b2bunit: {}", e);
			}
		}
		childUnits.forEach((code, unit) -> {
			orderDto.setSalesOffice(unit.getSalesOffice());
			orderDto.setCamID(unit.getKamID());
		});

		if (null != order.getDeliveryAddress())
		{
			orderDto.setShipToPartyCode(order.getDeliveryAddress().getSapCustomerID());
		}
		orderDto.setPoDate(formatter.format(order.getDate()));
		orderDto.setPoNumber(Integer.parseInt(order.getCode()) + "_" + order.getPurchaseOrderNumber());

		if ("standard-gross".equalsIgnoreCase(order.getDeliveryMode().getCode()))
		{
			orderDto.setIncoTerm1(JslConfigurableBundleServiceConstants.STANDARDDELIVERY);
		}
		else if ("customer-gross".equalsIgnoreCase(order.getDeliveryMode().getCode()))
		{
			orderDto.setIncoTerm1(JslConfigurableBundleServiceConstants.CUSTOMERDELIVERY);
		}
		else if ("free-road".equalsIgnoreCase(order.getDeliveryMode().getCode()))
		{
			orderDto.setIncoTerm1(JslConfigurableBundleServiceConstants.FREEONROAD);
		}

		final String plantLocation = order.getEntries().size() == 0 ? EMPTY_STRING
				: order.getEntries().get(0).getProduct().getLocation();
		orderDto.setIncoTerm2(plantLocation);
		final Optional<Long> sum = order.getEntries().stream().map(entry -> entry.getQuantity()).reduce(Long::sum);
		if (sum.isPresent())
		{
			final long lotquantity = (sum.get() / 20) > 0 ? (sum.get() / 20) : 1;
			orderDto.setNoOfLots(String.valueOf(Math.round(lotquantity)));

		}
		orderDto.setTemplateRecordingDate(orderDto.getPoDate());

		if (SALES_OFFICE_IN04.equalsIgnoreCase(orderDto.getSalesOffice()))
		{
			final List<String> gradeList = orderEntries.stream().map(entry -> {
				return entry.getProduct().getGrade();
			}).collect(Collectors.toList());

			final boolean grade304L = gradeList.contains("304L");
			final boolean grade316L = gradeList.contains("316L");


			if (grade304L && grade316L)
			{
				orderDto.setSpecialRemark("Required dual cert/marking for 304L as 304/304L and 316L as 316/316L");
			}
			else if (grade304L)
			{
				orderDto.setSpecialRemark("Required dual cert/marking for 304L as 304/304L");
			}
			else if (grade316L)
			{
				orderDto.setSpecialRemark("Required dual cert/marking  for 316L as 316/316L");
			}
			else
			{
				orderDto.setSpecialRemark(EMPTY_STRING);
			}

		}
		else
		{
			orderDto.setSpecialRemark(EMPTY_STRING);
		}
		orderDto.setReference(EMPTY_STRING);

		final List<JslZdocOrderItemExportDto> entries = orderDto.getOrderItems();

		int zdocEntryNumber = 1;
		for (final AbstractOrderEntryModel entry : orderEntries)
		{
			final JslZdocOrderItemExportDto orderItemDto = new JslZdocOrderItemExportDto();
			final ProductModel product = entry.getProduct();
			entry.getQuantity();
			orderItemDto.setRefNo(orderDto.getRefNo());
			orderItemDto.setItemNo(String.valueOf(zdocEntryNumber++));
			orderItemDto.setOrderCode(order.getCode());
			orderItemDto.setPlant(product.getPlant());
			orderItemDto.setMaterialCode(product.getMaterialNum());
			orderItemDto.setQtyMT(String.valueOf(entry.getQuantity()));
			orderItemDto.setRoute(JslConfigurableBundleServiceConstants.ROUTE);
			orderItemDto.setStandard(String.valueOf(product.getStandard()));
			orderItemDto.setGradeGroup(product.getGradeGroup());
			orderItemDto.setGrade(product.getGrade());
			orderItemDto.setThk(String.valueOf(product.getThickness()));
			orderItemDto.setWidth(String.valueOf(product.getWidth()));
			orderItemDto.setLength(String.valueOf(product.getLength()));
			orderItemDto.setFinish(product.getFinish());
			orderItemDto.setEdgeCon(product.getEdgeCondition());


			orderItemDto.setQuality(product.getQuality());
			orderItemDto.setMaterialGroup(JslConfigurableBundleServiceConstants.MATERIALGROUP);
			orderItemDto.setUsage(JslConfigurableBundleServiceConstants.USAGE);
			orderItemDto.setPartWtMaximum(String.valueOf(product.getMaxPartWt()));
			orderItemDto.setPartWtMinimum(String.valueOf(product.getMinPartWt()));

			orderItemDto.setiNSP_3RD_PARTY(N);
			orderItemDto.setMarkingOnProduct(JslConfigurableBundleServiceConstants.MARKINGONPRODUCT);
			orderItemDto.setMarkSTck(JslConfigurableBundleServiceConstants.MARKINGONSTICKER);
			orderItemDto.setUtensilUsage(N);


			final String zprcValue = entry.getTotalPrice() == null ? EMPTY_STRING
					: String.format("%.2f", (entry.getTotalPrice() / entry.getQuantity()));
			orderItemDto.setZPRC(zprcValue);

			if ("standard-gross".equalsIgnoreCase(order.getDeliveryMode().getCode()))
			{
				final double deliveryCostPerTon = findDeliveryCost(order.getWarehouse().getCode(),
						order.getDeliveryAddress().getPostalcode());

				final String zfrbValue = order.getDeliveryCost() == null ? EMPTY_STRING : String.format("%.2f", deliveryCostPerTon);
				orderItemDto.setZFRB(zfrbValue);

			}
			else if ("free-road".equalsIgnoreCase(order.getDeliveryMode().getCode())
					|| "customer-gross".equalsIgnoreCase(order.getDeliveryMode().getCode()))
			{
				orderItemDto.setZFRB(EMPTY_STRING);
			}



			orderItemDto.setZPRCCurr(JslConfigurableBundleServiceConstants.DEFAULTCURRENCY);
			orderItemDto.setZFRBCurr(JslConfigurableBundleServiceConstants.DEFAULTCURRENCY);
			entries.add(orderItemDto);



			if (product.getLength() == 0)

			{
				orderItemDto.setCoilId(JslConfigurableBundleServiceConstants.COILID);
			}
			else
			{
				orderItemDto.setCoilId(EMPTY_STRING);
			}
			if (orderItemDto.getFinish().equalsIgnoreCase("N1"))
			{
				orderItemDto.setPVC(N);
				orderItemDto.setILP(N);
			}
			else if (orderItemDto.getFinish().equalsIgnoreCase("2B"))
			{

				orderItemDto.setPVC(N);
				orderItemDto.setILP(Y);
			}
			else if (orderItemDto.getFinish().equalsIgnoreCase("N4P"))
			{
				orderItemDto.setPVC(JslConfigurableBundleServiceConstants.TOP);
				orderItemDto.setILP(N);
			}

			if ("JT".equalsIgnoreCase(orderItemDto.getGrade()) || "JSLUSD".equalsIgnoreCase(orderItemDto.getGrade())
					|| "JSLUDD".equalsIgnoreCase(orderItemDto.getGrade()))
			{
				orderItemDto.setStandard("2");
			}
			else if ("316L".equalsIgnoreCase(orderItemDto.getGrade()) || "304".equalsIgnoreCase(orderItemDto.getGrade()))
			{
				orderItemDto.setStandard("42");
			}
			else if ("X5CRNI1810".equalsIgnoreCase(orderItemDto.getGrade()) || "X2CRNI12".equalsIgnoreCase(orderItemDto.getGrade()))
			{
				orderItemDto.setStandard("16");
			}
			else
			{
				orderItemDto.setStandard("2");
			}

			orderDto.setRequiredDeliveryDate(formatter.format(DateUtils.addDays(order.getDate(), 30)));
			orderItemDto.setDesired_Date1(orderDto.getRequiredDeliveryDate());

		}
		orderDto.setOrderItems(entries);

		return orderDto;
	}



	/**
	 * @return the b2bUnitService
	 */
	public B2BUnitService getB2bUnitService()
	{
		return b2bUnitService;
	}

	/**
	 * @param b2bUnitService
	 *           the b2bUnitService to set
	 */
	public void setB2bUnitService(final B2BUnitService b2bUnitService)
	{
		this.b2bUnitService = b2bUnitService;
	}

	/**
	 * @return the bundleTemplateService
	 */
	public BundleTemplateService getBundleTemplateService()
	{
		return bundleTemplateService;
	}

	/**
	 * @param bundleTemplateService
	 *           the bundleTemplateService to set
	 */
	public void setBundleTemplateService(final BundleTemplateService bundleTemplateService)
	{
		this.bundleTemplateService = bundleTemplateService;
	}

	/**
	 * @return the jslAuctionService
	 */
	public JslAuctionService getJslAuctionService()
	{
		return jslAuctionService;
	}

	/**
	 * @param jslAuctionService
	 *           the jslAuctionService to set
	 */
	public void setJslAuctionService(final JslAuctionService jslAuctionService)
	{
		this.jslAuctionService = jslAuctionService;
	}

	/**
	 * @return the jslB2bUnitUtil
	 */
	public JslB2bUnitUtil getJslB2bUnitUtil()
	{
		return jslB2bUnitUtil;
	}

	/**
	 * @param jslB2bUnitUtil
	 *           the jslB2bUnitUtil to set
	 */
	public void setJslB2bUnitUtil(final JslB2bUnitUtil jslB2bUnitUtil)
	{
		this.jslB2bUnitUtil = jslB2bUnitUtil;
	}


	private double findDeliveryCost(final String location, final String postalCode)
	{
		return getJslAuctionDeliveryService().getDeliveryCharge(location, postalCode);
	}

	/**
	 * @return the jslAuctionDeliveryService
	 */
	public JslAuctionDeliveryService getJslAuctionDeliveryService()
	{
		return jslAuctionDeliveryService;
	}

	/**
	 * @param jslAuctionDeliveryService
	 *           the jslAuctionDeliveryService to set
	 */
	public void setJslAuctionDeliveryService(final JslAuctionDeliveryService jslAuctionDeliveryService)
	{
		this.jslAuctionDeliveryService = jslAuctionDeliveryService;
	}
}
