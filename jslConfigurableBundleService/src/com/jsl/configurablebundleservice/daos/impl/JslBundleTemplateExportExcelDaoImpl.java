/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.configurablebundleservice.daos.impl;

import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.jsl.configurablebundleservice.daos.JslBundleTemplateExportExcelDao;


/**
 *
 */
public class JslBundleTemplateExportExcelDaoImpl implements JslBundleTemplateExportExcelDao
{

	private static final Logger LOG = Logger.getLogger(JslBundleTemplateExportExcelDaoImpl.class);
	@Autowired
	private FlexibleSearchService flexibleSearchService;

	@Override
	public List<BundleTemplateModel> getAllBundleTemplates()
	{
		final String queryString = "SELECT {p:" + BundleTemplateModel.PK + "} " + "FROM {" + BundleTemplateModel._TYPECODE
				+ " AS p} Where {parenttemplate} is not null";

		final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
		final List<BundleTemplateModel> bundleTemplateModel = flexibleSearchService.<BundleTemplateModel> search(query).getResult();

		return bundleTemplateModel;
	}

}
