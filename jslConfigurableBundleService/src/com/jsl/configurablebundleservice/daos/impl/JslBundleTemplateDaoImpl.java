/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.configurablebundleservice.daos.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.configurablebundleservices.daos.impl.DefaultBundleTemplateDao;
import de.hybris.platform.configurablebundleservices.model.BundleSelectionCriteriaModel;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateStatusModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Nonnull;

import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import com.jsl.configurablebundleservice.daos.JslBundleTemplateDao;
import com.jsl.core.enums.AuctionStatus;
import com.jsl.core.model.AuctionEventModel;
import com.jsl.jslConfigurableBundleService.model.WatchListModel;

public class JslBundleTemplateDaoImpl extends DefaultBundleTemplateDao implements JslBundleTemplateDao
{
	private static final Logger LOG = Logger.getLogger(JslBundleTemplateDaoImpl.class);

	private FlexibleSearchService flexibleSearchService;

	private static final String RESTRICTION_ONLY_APPROVED = " AND {status} IN ( "
			+ " {{ SELECT {bts.pk} FROM { BundleTemplateStatus AS bts JOIN EnumerationValue AS ev ON {bts.status} = {ev.pk} } WHERE {ev.Code} = 'approved' }} "
			+ ")";

	private static final String FIND_BUNDLESELECTIONTEMPLATE_QUERY = "SELECT {" + BundleSelectionCriteriaModel.PK + "} FROM {"
			+ BundleSelectionCriteriaModel._TYPECODE + "} where {" + BundleSelectionCriteriaModel.ID + "}= ?uid and {"
			+ BundleSelectionCriteriaModel.CATALOGVERSION + "} =?catalogVersion";

	private static final String FIND_BUNDLETEMPLATESTATUS_QUERY = "SELECT {" + BundleTemplateStatusModel.PK + "} FROM {"
			+ BundleTemplateStatusModel._TYPECODE + "} where {" + BundleTemplateStatusModel.ID + "}= ?uid and {"
			+ BundleTemplateStatusModel.CATALOGVERSION + "} =?catalogVersion";

	private static final String FIND_BUNDLETEMPLATEMODEL_QUERY = "SELECT {" + BundleTemplateModel.PK + "} FROM {"
			+ BundleTemplateModel._TYPECODE
			+ " AS bt JOIN AuctionEvent AS ae ON {bt.auctionEvent} = {ae.pk}} where {ae.auctionId}= ?auctionId and {bt."
			+ BundleTemplateModel.PARENTTEMPLATE + "} IS NOT NULL";

	private static final String FIND_BUNDLETEMPLATEMODEL_FROM_BUNDLEID_QUERY = "SELECT {" + BundleTemplateModel.PK + "} FROM {"
			+ BundleTemplateModel._TYPECODE + " AS bt JOIN AuctionEvent AS ae ON {bt.auctionEvent} = {ae.pk}}"
			+ " where {ae.auctionId}= ?auctionId AND {bt.id}= ?bundleId";

	private static final String GET_ALL_AUCTION_LIST_QUERY = "SELECT {b." + BundleTemplateModel.PK + "} FROM {"
			+ AuctionEventModel._TYPECODE + " as a JOIN " + BundleTemplateModel._TYPECODE + " as b"
			+ " ON {a.pk} = {b.auctionEvent} } " + "where {" + AuctionEventModel.STARTDATE + " } = ?curDateTime";

	private static final String GET_ALL_COMPLETED_AUCTION_LIST_QUERY = "SELECT {b." + BundleTemplateModel.PK + "} FROM {"
			+ AuctionEventModel._TYPECODE + " as a JOIN " + BundleTemplateModel._TYPECODE + " as b"
			+ " ON {a.pk} = {b.auctionEvent} JOIN auctionStatus as aes on {a.auctionStatus} = {aes.pk}} "
			+ "where {aes.code}= ?auctionStatus order by {a." + AuctionEventModel.STARTDATE + "} desc";

	private static final String GET_ALL_COMPLETED_AUCTION_LIST_DATE_RANGE_QUERY = "SELECT {b." + BundleTemplateModel.PK
			+ "} FROM {" + AuctionEventModel._TYPECODE + " as a JOIN " + BundleTemplateModel._TYPECODE + " as b"
			+ " ON {a.pk} = {b.auctionEvent} JOIN auctionStatus as aes on {a.auctionStatus} = {aes.pk}} "
			+ "where {aes.code}= ?auctionStatus AND CAST({a.startdate} as DATE) between ?startdate AND ?endDate order by {a."
			+ AuctionEventModel.STARTDATE + "} desc";

	private static final String GET_ALL_UPCOMING_AUCTION_LIST_QUERY = "SELECT {b." + BundleTemplateModel.PK + "} FROM {"
			+ AuctionEventModel._TYPECODE + " as a JOIN " + BundleTemplateModel._TYPECODE + " as b"
			+ " ON {a.pk} = {b.auctionEvent}} " + "where {" + AuctionEventModel.STARTDATE + " } > ?curDateTime";

	private static final String GET_ALL_LIVE_AUCTION_LIST_BY_AUCTIONID_QUERY = "SELECT {b." + BundleTemplateModel.PK + "} FROM {"
			+ BundleTemplateModel._TYPECODE + " as b JOIN " + AuctionEventModel._TYPECODE + " as a"
			+ " ON {b.auctionEvent}  = {a.pk} JOIN auctionStatus as aes on {a.auctionStatus} = {aes.pk}} "
			+ "where CAST({a."+ AuctionEventModel.STARTDATE + " } as DATE) <= ?curDateTime"
			+ " AND {b." + BundleTemplateModel.PARENTTEMPLATE + "} IS NOT NULL" + " AND {a." + AuctionEventModel.AUCTIONID
			+ " } = ?auctionId AND {aes.code} IN( ?auctionStatusStarted , ?auctionStatusClosed) ";

	private static final String GET_ALL_LIVE_AUCTION_LIST_QUERY = "SELECT {b." + BundleTemplateModel.PK + "} FROM {"
			+ BundleTemplateModel._TYPECODE + " as b JOIN " + AuctionEventModel._TYPECODE + " as a"
			+ " ON {b.auctionEvent}  = {a.pk} } " + "where {a." + AuctionEventModel.STARTDATE + " } <= ?curDateTime" + " AND {a."
			+ AuctionEventModel.ENDDATE + "} > ?curDateTime and {b." + BundleTemplateModel.PARENTTEMPLATE + "} IS NOT NULL";

	private static final String GET_ALL_WATCHLIST_AUCTION_BY_AUCTIONID_QUERY = "SELECT {w." + WatchListModel.PK + "} FROM {"
			+ WatchListModel._TYPECODE + " as w JOIN " + AuctionEventModel._TYPECODE + " as a" + " ON {a.pk} = {w.auctionEvent}"
			+ "JOIN auctionStatus as aes on {a.auctionStatus} = {aes.pk} } "
			+ "where {w." + WatchListModel.CREATEDBY + "}= ?createdBy AND {a." + AuctionEventModel.AUCTIONID
			+ " } = ?auctionId AND {aes.code} IN( ?auctionStatusStarted , ?auctionStatusClosed)";

	private static final String GET_ALL_WATCHLIST_AUCTION_LIST_QUERY = "SELECT {w." + WatchListModel.PK + "} FROM {"
			+ WatchListModel._TYPECODE + " as w JOIN " + AuctionEventModel._TYPECODE + " as a" + " ON {a.pk} = {w.auctionEvent} } "
			+ "where {a." + AuctionEventModel.ENDDATE + " } > ?curDateTime AND {w." + WatchListModel.CREATEDBY+ "}= ?createdBy";

	private static final String GET_ALL_WATCHLIST_AUCTION_LIST_TO_ADD_REMOVE_QUERY = "SELECT {w." + WatchListModel.PK + "} FROM {"
			+ WatchListModel._TYPECODE + " as w } " + "where {w." + WatchListModel.AUCTIONEVENT + " } = ?auctionEvent AND {w."
			+ WatchListModel.CREATEDBY + "}= ?createdBy AND {w." + WatchListModel.BUNDLETEMPLATE + "}= ?bundleTemplate";

	private static final String GET_PRODUCT_LIST_BY_BUNDLE_ID = "SELECT {prodRel.source} FROM {" + BundleTemplateModel._TYPECODE
			+ " as b JOIN ProductsBundleTemplatesRelation AS prodRel ON {prodRel:target}={b:pk} AND {b:parentTemplate} IS NOT NULL} where {b.id}=?bundleId "
			+ RESTRICTION_ONLY_APPROVED;

	private static final String GET_UNSOLD_BUNDLE = "SELECT {b." + BundleTemplateModel.PK + "} FROM {"
			+ BundleTemplateModel._TYPECODE + " as b JOIN " + AuctionEventModel._TYPECODE + " as a"
			+ " ON {b.auctionEvent} = {a.pk} JOIN AuctionStatus as aes ON {a.auctionStatus} = {aes.pk}} "
			+ "where {aes.code} = ?auctionStatus"
			+ " AND {b." + BundleTemplateModel.BIDEVENT + "} IS NULL AND {a." + AuctionEventModel.AUCTIONID + "} = ?auctionId";

	private static final String GET_ORDER_LIST_BY_AUCTION_ID = "SELECT {o.pk} FROM {" + OrderModel._TYPECODE + " as o } where {o."
			+ OrderModel.AUCTIONID + "}= ?auctionId";

	private static final String GET_ALL_COMPLETED_AUCTION_LOT_WITHOUT_BID_LIST_QUERY = "SELECT {b." + BundleTemplateModel.PK
			+ "} FROM {" + AuctionEventModel._TYPECODE + " as a JOIN " + BundleTemplateModel._TYPECODE + " as b"
			+ " ON {a.pk} = {b.auctionEvent} JOIN auctionStatus as aes on {a.auctionStatus} = {aes.pk}} "
			+ "where {aes.code}= ?auctionStatus AND {b.bidEvent} is NUll AND {b.parenttemplate} IS NOT NULL " + "AND CAST({a."
			+ AuctionEventModel.STARTDATE + " } as DATE) = ?curDateTime " + "ORDER BY {a." + AuctionEventModel.STARTDATE + "} desc";

	private static final String GET_ALL_COMPLETED_AUCTION_LOT_WITHOUT_BID_LIST_BY_DATE_RANGE_QUERY = "SELECT {b."
			+ BundleTemplateModel.PK + "} FROM {" + AuctionEventModel._TYPECODE + " as a JOIN " + BundleTemplateModel._TYPECODE
			+ " as b" + " ON {a.pk} = {b.auctionEvent} JOIN auctionStatus as aes on {a.auctionStatus} = {aes.pk}} "
			+ "where {aes.code}= ?auctionStatus AND CAST({a.startdate} as DATE) = ?startdate "
			+ "AND {b.bidEvent} is NUll AND {b.parenttemplate} IS NOT NULL ORDER BY {a."
			+ AuctionEventModel.STARTDATE + "} desc";

	private static final String GET_ALL_COMPLETED_AUCTION_LIST_FOR_GIVEN_DATE_QUERY = "SELECT {b." + BundleTemplateModel.PK
			+ "} FROM {" + AuctionEventModel._TYPECODE + " as a JOIN " + BundleTemplateModel._TYPECODE + " as b"
			+ " ON {a.pk} = {b.auctionEvent} JOIN auctionStatus as aes on {a.auctionStatus} = {aes.pk}} "
			+ "where {aes.code}= ?auctionStatus AND CAST({a.startdate} as DATE) = ?startdate order by {a."
			+ AuctionEventModel.STARTDATE + "} desc, {b." + BundleTemplateModel.ID + "}";

	private static final String GET_ALL_COMPLETED_AUCTION_LIST_FOR_CURRENT_DATE_QUERY = "SELECT {b." + BundleTemplateModel.PK
			+ "} FROM {" + AuctionEventModel._TYPECODE + " as a JOIN " + BundleTemplateModel._TYPECODE + " as b"
			+ " ON {a.pk} = {b.auctionEvent} JOIN auctionStatus as aes on {a.auctionStatus} = {aes.pk}} "
			+ "where {aes.code}= ?auctionStatus AND CAST({a." + AuctionEventModel.STARTDATE + " } as DATE) = ?curDateTime "
			+ "order by {a." + AuctionEventModel.STARTDATE + "} desc, {b." + BundleTemplateModel.ID + "}";

	@Override
	@Nonnull
	public BundleSelectionCriteriaModel findBundleSelectionCriteriaById(final String id, final CatalogVersionModel catalogVersion)
	{
		validateParameterNotNullStandardMessage("id", id);

		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_BUNDLESELECTIONTEMPLATE_QUERY);
		query.addQueryParameter("uid", id);
		query.addQueryParameter("catalogVersion", catalogVersion);

		return getFlexibleSearchService().searchUnique(query);
	}


	@Override
	public BundleTemplateStatusModel findBundleTemplateStatusById(final String id, final CatalogVersionModel catalogVersion)
	{
		validateParameterNotNullStandardMessage("id", id);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_BUNDLETEMPLATESTATUS_QUERY);
		query.addQueryParameter("uid", id);
		query.addQueryParameter("catalogVersion", catalogVersion);

		return getFlexibleSearchService().searchUnique(query);
	}


	@Override
	public List<BundleTemplateModel> getBundleTemplatesForAuctionId(final String auctionId)
	{
		validateParameterNotNullStandardMessage("id", auctionId);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_BUNDLETEMPLATEMODEL_QUERY);
		query.addQueryParameter("auctionId", auctionId);

		return getFlexibleSearchService().<BundleTemplateModel> search(query).getResult();
	}

	@Override
	public List<BundleTemplateModel> getAuctionList(final String curDateTime)
	{
		List<BundleTemplateModel> bundleTemplateList = null;
		SearchResult<BundleTemplateModel> result = null;
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ALL_AUCTION_LIST_QUERY);
		query.addQueryParameter("curDateTime", curDateTime);
		result = getFlexibleSearchService().search(query);
		bundleTemplateList = result.getResult();

		return bundleTemplateList;
	}

	@Override
	public List<BundleTemplateModel> getOngoingAuctionDetails(final String curDateTime, final String auctionId)
	{
		List<BundleTemplateModel> bundleTemplateList = null;
		SearchResult<BundleTemplateModel> result = null;
		FlexibleSearchQuery query = null;
		if (StringUtils.hasText(auctionId) && auctionId.equalsIgnoreCase("All"))
		{
			query = new FlexibleSearchQuery(GET_ALL_LIVE_AUCTION_LIST_QUERY);
		}
		else
		{
			query = new FlexibleSearchQuery(GET_ALL_LIVE_AUCTION_LIST_BY_AUCTIONID_QUERY);
			query.addQueryParameter("auctionId", auctionId);
		}
		query.addQueryParameter("curDateTime", curDateTime);
		query.addQueryParameter("auctionStatusStarted", AuctionStatus.STARTED.getCode());
		query.addQueryParameter("auctionStatusClosed", AuctionStatus.CLOSED.getCode());
		result = getFlexibleSearchService().search(query);
		bundleTemplateList = result.getResult();
		LOG.debug("JslBundleTemplateDaoImpl.class getOngoingAuctionDetails() bundleTemplateDataList size: "
				+ bundleTemplateList.size());

		return bundleTemplateList;
	}

	@Override
	public List<ProductModel> getProductListByBundleId(final String bundleId)
	{
		List<ProductModel> productList = null;
		SearchResult<ProductModel> result = null;
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_PRODUCT_LIST_BY_BUNDLE_ID);
		query.addQueryParameter("bundleId", bundleId);
		result = getFlexibleSearchService().search(query);
		productList = result.getResult();

		return productList;
	}

	@Override
	public List<BundleTemplateModel> getCompletedAuctionDetails()
	{
		List<BundleTemplateModel> bundleTemplateList = null;
		SearchResult<BundleTemplateModel> result = null;
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ALL_COMPLETED_AUCTION_LIST_QUERY);
		query.addQueryParameter("auctionStatus", AuctionStatus.COMPLETED.getCode());
		result = getFlexibleSearchService().search(query);
		bundleTemplateList = result.getResult();

		return bundleTemplateList;
	}

	@Override
	public List<BundleTemplateModel> getCompletedAuctionDetailsForAdmin()
	{
		final Timestamp curDateTime = new Timestamp(System.currentTimeMillis());
		List<BundleTemplateModel> bundleTemplateList = null;
		SearchResult<BundleTemplateModel> result = null;
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ALL_COMPLETED_AUCTION_LIST_FOR_CURRENT_DATE_QUERY);
		query.addQueryParameter("auctionStatus", AuctionStatus.COMPLETED.getCode());
		query.addQueryParameter("curDateTime", curDateTime.toString());
		result = getFlexibleSearchService().search(query);
		bundleTemplateList = result.getResult();

		return bundleTemplateList;
	}

	@Override
	public List<BundleTemplateModel> getCompletedAuctionDetailsForCurrentDate(final String curDateTime)
	{
		List<BundleTemplateModel> bundleTemplateList = null;
		SearchResult<BundleTemplateModel> result = null;
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ALL_COMPLETED_AUCTION_LIST_FOR_CURRENT_DATE_QUERY);
		query.addQueryParameter("auctionStatus", AuctionStatus.COMPLETED.getCode());
		query.addQueryParameter("curDateTime", curDateTime);
		result = getFlexibleSearchService().search(query);
		bundleTemplateList = result.getResult();

		return bundleTemplateList;
	}

	@Override
	public List<BundleTemplateModel> getCompletedAuctionDetailsByDateRange(final String startdate, final String endDate)
	{
		List<BundleTemplateModel> bundleTemplateList = null;
		SearchResult<BundleTemplateModel> result = null;
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ALL_COMPLETED_AUCTION_LIST_DATE_RANGE_QUERY);
		query.addQueryParameter("auctionStatus", AuctionStatus.COMPLETED.getCode());
		query.addQueryParameter("startdate", startdate);
		query.addQueryParameter("endDate", endDate);
		result = getFlexibleSearchService().search(query);
		bundleTemplateList = result.getResult();

		return bundleTemplateList;
	}

	@Override
	public List<BundleTemplateModel> getCompletedAuctionDetailsByDateForUser(final String startdate)
	{
		List<BundleTemplateModel> bundleTemplateList = null;
		SearchResult<BundleTemplateModel> result = null;
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ALL_COMPLETED_AUCTION_LIST_FOR_GIVEN_DATE_QUERY);
		query.addQueryParameter("auctionStatus", AuctionStatus.COMPLETED.getCode());
		query.addQueryParameter("startdate", startdate);
		result = getFlexibleSearchService().search(query);
		bundleTemplateList = result.getResult();

		return bundleTemplateList;
	}

	@Override
	public List<BundleTemplateModel> getUpcomingAuctionDetails(final String curDateTime)
	{
		List<BundleTemplateModel> bundleTemplateList = null;
		SearchResult<BundleTemplateModel> result = null;
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ALL_UPCOMING_AUCTION_LIST_QUERY);
		query.addQueryParameter("curDateTime", curDateTime);
		result = getFlexibleSearchService().search(query);
		bundleTemplateList = result.getResult();

		return bundleTemplateList;
	}


	@Override
	public BundleTemplateModel getBundleTemplateForBundleId(final String bundleId, final String auctionId)
	{
		validateParameterNotNullStandardMessage("id", bundleId);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_BUNDLETEMPLATEMODEL_FROM_BUNDLEID_QUERY);
		query.addQueryParameter("auctionId", auctionId);
		query.addQueryParameter("bundleId", bundleId);
		query.setDisableCaching(true);

		return getFlexibleSearchService().searchUnique(query);
	}

	@Override
	public List<WatchListModel> getWatchListDetails(final UserModel user, final String curDateTime, final String auctionID)
	{
		List<WatchListModel> watchList = null;
		SearchResult<WatchListModel> result = null;
		FlexibleSearchQuery query = null;
		if (StringUtils.hasText(auctionID))
		{
			query = new FlexibleSearchQuery(GET_ALL_WATCHLIST_AUCTION_BY_AUCTIONID_QUERY);
			query.addQueryParameter("auctionId", auctionID);
		} else {
			query = new FlexibleSearchQuery(GET_ALL_WATCHLIST_AUCTION_LIST_QUERY);
		}
		//query.addQueryParameter("curDateTime", curDateTime);
		query.addQueryParameter("createdBy", user);
		query.addQueryParameter("auctionStatusStarted", AuctionStatus.STARTED.getCode());
		query.addQueryParameter("auctionStatusClosed", AuctionStatus.CLOSED.getCode());
		query.setDisableCaching(true);
		result = getFlexibleSearchService().search(query);
		watchList = result.getResult();

		return watchList;
	}

	@Override
	public WatchListModel getWatchListDetailsToAddOrRemove(final BundleTemplateModel bundleTemplate, final UserModel user)
	{
		try
		{
			final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ALL_WATCHLIST_AUCTION_LIST_TO_ADD_REMOVE_QUERY);
			query.addQueryParameter("auctionEvent", bundleTemplate.getAuctionEvent());
			query.addQueryParameter("bundleTemplate", bundleTemplate);
			query.addQueryParameter("createdBy", user);
			query.setDisableCaching(true);

			return getFlexibleSearchService().searchUnique(query);
		}
		catch (final Exception e)
		{
			return null;
		}
	}

	@Override
	public List<BundleTemplateModel> getUnsoldBundle(final String auctionId)
	{
		List<BundleTemplateModel> unsoldBundleList = null;
		SearchResult<BundleTemplateModel> result = null;
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_UNSOLD_BUNDLE);
		query.addQueryParameter("auctionStatus", AuctionStatus.COMPLETED.getCode().toString());
		query.addQueryParameter("auctionId", auctionId);
		result = getFlexibleSearchService().search(query);
		unsoldBundleList = result.getResult();

		return unsoldBundleList;
	}

	@Override
	public List<OrderModel> getOrderByAuctionId(final String auctionId)
	{
		List<OrderModel> orderList = null;
		SearchResult<OrderModel> result = null;
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ORDER_LIST_BY_AUCTION_ID);
		query.addQueryParameter("auctionId", auctionId);
		result = getFlexibleSearchService().search(query);
		orderList = result.getResult();

		return orderList;
	}

	@Override
	public List<BundleTemplateModel> getCompletedAuctionLotWithoutBid()
	{
		final Timestamp curDatetime = new Timestamp(System.currentTimeMillis());
		List<BundleTemplateModel> bundleTemplateList = null;
		SearchResult<BundleTemplateModel> result = null;
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ALL_COMPLETED_AUCTION_LOT_WITHOUT_BID_LIST_QUERY);
		query.addQueryParameter("auctionStatus", AuctionStatus.COMPLETED.getCode());
		query.addQueryParameter("curDateTime", curDatetime.toString());
		result = getFlexibleSearchService().search(query);
		bundleTemplateList = result.getResult();

		return bundleTemplateList;
	}

	@Override
	public List<BundleTemplateModel> getCompletedAuctionLotWithoutBidByDateRange(final String startdate, final String endDate)
	{
		List<BundleTemplateModel> bundleTemplateList = null;
		SearchResult<BundleTemplateModel> result = null;
		final FlexibleSearchQuery query = new FlexibleSearchQuery(
				GET_ALL_COMPLETED_AUCTION_LOT_WITHOUT_BID_LIST_BY_DATE_RANGE_QUERY);
		query.addQueryParameter("auctionStatus", AuctionStatus.COMPLETED.getCode());
		query.addQueryParameter("startdate", startdate);
		query.addQueryParameter("endDate", endDate);
		result = getFlexibleSearchService().search(query);
		bundleTemplateList = result.getResult();

		return bundleTemplateList;
	}

	@Override
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}


	@Override
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

}
