/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.configurablebundleservice.daos;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.configurablebundleservices.daos.BundleTemplateDao;
import de.hybris.platform.configurablebundleservices.model.BundleSelectionCriteriaModel;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateStatusModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.List;

import com.jsl.jslConfigurableBundleService.model.WatchListModel;


/**
 *
 */
public interface JslBundleTemplateDao extends BundleTemplateDao
{

	List<BundleTemplateModel> getBundleTemplatesForAuctionId(String auctionId);

	BundleSelectionCriteriaModel findBundleSelectionCriteriaById(String id, CatalogVersionModel catalogVersion);

	BundleTemplateStatusModel findBundleTemplateStatusById(String id, CatalogVersionModel catalogVersion);

	List<BundleTemplateModel> getAuctionList(String curDateTime);

	List<BundleTemplateModel> getOngoingAuctionDetails(String curDateTime, String auctionId);

	List<BundleTemplateModel> getCompletedAuctionDetails();

	List<BundleTemplateModel> getUpcomingAuctionDetails(String curDateTime);

	BundleTemplateModel getBundleTemplateForBundleId(String bundleId, String auctionId);

	List<WatchListModel> getWatchListDetails(UserModel user, String curDateTime, String auctionID);

	WatchListModel getWatchListDetailsToAddOrRemove(BundleTemplateModel bundleTemplate, UserModel user);

	List<ProductModel> getProductListByBundleId(String bundleId);

	List<BundleTemplateModel> getUnsoldBundle(String auctionId);

	List<BundleTemplateModel> getCompletedAuctionDetailsByDateRange(String startdate, String endDate);

	List<OrderModel> getOrderByAuctionId(String auctionId);

	List<BundleTemplateModel> getCompletedAuctionLotWithoutBid();

	List<BundleTemplateModel> getCompletedAuctionLotWithoutBidByDateRange(String startdate, String endDate);

	List<BundleTemplateModel> getCompletedAuctionDetailsByDateForUser(String startdate);

	List<BundleTemplateModel> getCompletedAuctionDetailsForCurrentDate(String curDateTime);

	List<BundleTemplateModel> getCompletedAuctionDetailsForAdmin();

}
