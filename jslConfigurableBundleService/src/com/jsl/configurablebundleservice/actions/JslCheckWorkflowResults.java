/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.configurablebundleservice.actions;

import de.hybris.platform.b2b.process.approval.actions.AbstractSimpleB2BApproveOrderDecisionAction;
import de.hybris.platform.b2b.process.approval.actions.CheckWorkflowResults;
import de.hybris.platform.b2b.process.approval.model.B2BApprovalProcessModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commerceservices.impersonation.ImpersonationContext;
import de.hybris.platform.commerceservices.impersonation.ImpersonationService;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.task.RetryLaterException;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jsl.configurablebundleservice.service.JslBundleTemplateService;
import com.jsl.core.model.AuctionProductModel;


/**
 *
 */
public class JslCheckWorkflowResults extends AbstractSimpleB2BApproveOrderDecisionAction
{
	private static final Logger LOG = Logger.getLogger(CheckWorkflowResults.class);

	private JslBundleTemplateService jslBundleTemplateService;

	private ImpersonationService impersonationService;

	private CatalogVersionService catalogVersionService;

	private BaseSiteService baseSiteService;

	private SessionService sessionService;

	private UserService userService;


	@Override
	public Transition executeAction(final B2BApprovalProcessModel process) throws RetryLaterException
	{
		OrderModel order = null;
		try
		{
			order = process.getOrder();
			if (order.getStatus().equals(OrderStatus.REJECTED))
			{
				// create order history and exit process.
				unbundleProduct(order);
				return Transition.NOK;

			}
			else
			{
				// if order was approved delegate to PerformMerchantCheck action
				order.setStatus(OrderStatus.APPROVED);
				this.modelService.save(order);
				return Transition.OK;
			}
		}
		catch (final Exception e)
		{
			this.handleError(order, e);
			return Transition.NOK;
		}
	}

	protected void handleError(final OrderModel order, final Exception e)
	{
		if (order != null)
		{
			this.setOrderStatus(order, OrderStatus.B2B_PROCESSING_ERROR);
		}
		LOG.error(e.getMessage(), e);
	}

	private void unbundleProduct(final OrderModel order)
	{

		final String bundleId = order.getEntryGroups().get(0).getChildren().get(0).getExternalReferenceId();
		LOG.info("Unbundling products for rejected  order: " + bundleId);

		final List<ProductModel> productModelList = getProductsForContext(bundleId);

		productModelList.forEach(p -> {
			if (p instanceof AuctionProductModel)
			{
			((AuctionProductModel) p).setBundled(false);
			}
		});

		this.modelService.saveAll(productModelList);
	}

	private List<ProductModel> getProductsForContext(final String bundleId)
	{
		final ImpersonationContext impersonationContext = new ImpersonationContext();
		impersonationContext.setSite(baseSiteService.getBaseSiteForUID("jsl"));
		final List<CatalogVersionModel> catalogVersionModels = new ArrayList<CatalogVersionModel>();
		catalogVersionModels.add(catalogVersionService.getCatalogVersion("JslAuctionProductCatalog ", "Online"));
		impersonationContext.setCatalogVersions(catalogVersionModels);

		return sessionService.executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public Object execute()
			{

				return getBundleTemplateService().getProductListByBundleId(bundleId);
			}
		}, userService.getAdminUser());
	}


	/**
	 * @return the bundleTemplateService
	 */
	public JslBundleTemplateService getBundleTemplateService()
	{
		return jslBundleTemplateService;
	}

	/**
	 * @param bundleTemplateService
	 *           the bundleTemplateService to set
	 */
	public void setJslBundleTemplateService(final JslBundleTemplateService jslBundleTemplateService)
	{
		this.jslBundleTemplateService = jslBundleTemplateService;
	}

	/**
	 * @return the impersonationService
	 */
	public ImpersonationService getImpersonationService()
	{
		return impersonationService;
	}

	/**
	 * @param impersonationService
	 *           the impersonationService to set
	 */
	public void setImpersonationService(final ImpersonationService impersonationService)
	{
		this.impersonationService = impersonationService;
	}

	/**
	 * @return the catalogVersionService
	 */
	public CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	/**
	 * @param catalogVersionService the catalogVersionService to set
	 */
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

	/**
	 * @return the baseSiteService
	 */
	public BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	/**
	 * @param baseSiteService the baseSiteService to set
	 */
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}

	/**
	 * @return the sessionService
	 */
	public SessionService getSessionService()
	{
		return sessionService;
	}

	/**
	 * @param sessionService
	 *           the sessionService to set
	 */
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}



}
