/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.configurablebundleservice.job;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.jsl.configurablebundleservice.cart.service.JslBundleCommerceCartService;
import com.jsl.configurablebundleservice.order.service.JslBundlePlaceOrderService;
import com.jsl.configurablebundleservice.service.JslBundleTemplateService;
import com.jsl.core.constants.JslCoreConstants;
import com.jsl.core.enums.AuctionStatus;
import com.jsl.core.model.AuctionEventModel;
import com.jsl.core.services.JslAuctionService;
import com.jsl.core.services.JslBidEventService;
import com.jsl.core.util.JslB2bUnitUtil;
import com.jsl.jslConfigurableBundleService.model.BidEventModel;


/**
 *
 */
public class JslAuctionPlaceOrderJob extends AbstractJobPerformable<CronJobModel>
{
	JslBundlePlaceOrderService jslBundlePlaceOrderService;
	JslBundleCommerceCartService jslBundleCommerceCartService;
	UserService userService;
	private JslBidEventService jslBidService;
	private JslAuctionService jslAuctionService;
	private JslBundleTemplateService jslBundleTemplateService;
	private ModelService modelService;
	B2BUnitService b2bUnitService;

	private static final Logger LOG = Logger.getLogger(JslAuctionPlaceOrderJob.class);

	private static final String SPLITTER = "_";
	private static final String AUCTION_UNIT_CODE = "auction.b2bunit.code";
	private static final String DEFAULT_B2BUNIT_CODE = "default.b2b.unit.codes";


	@Override
	public PerformResult perform(final CronJobModel var1)
	{
		PerformResult result = new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);

		boolean isAllOrderPlacedSuccessfully = true;

		final List<AuctionEventModel> completedAuctionEventList = getJslAuctionService().getAuctions(AuctionStatus.CLOSED);
		for (final AuctionEventModel auctionEvent : completedAuctionEventList)
		{
			LOG.info("Placing order for aauction Id : " + auctionEvent.getAuctionId());
			final List<BundleTemplateModel> bundleTemplateList = getWinBidList(auctionEvent);
			for (final BundleTemplateModel bundleTemplateModel : bundleTemplateList)
			{
				final UserModel user = bundleTemplateModel.getBidEvent().getCreatedBy();
				final String bundleTemplateId = bundleTemplateModel.getId();

				LOG.info("Placing order for bundle template id : " + bundleTemplateId);
				final boolean processResult = placeOrder(user, bundleTemplateId);

				if (!processResult)
				{
					LOG.info("Place order failed for bundle template id : " + bundleTemplateId);
					isAllOrderPlacedSuccessfully = false;
				}

			}
			updateAuctionStatus(auctionEvent);
			releaseStockofUnsoldBundle(String.valueOf(auctionEvent.getAuctionId()));
		}

		if (!isAllOrderPlacedSuccessfully)
		{
			result = new PerformResult(CronJobResult.ERROR, CronJobStatus.FINISHED);
		}

		return result;
	}



	private boolean placeOrder(final UserModel user, final String bundleTemplateId)
	{
		try
		{
			final CartModel cartModel = getJslBundleCommerceCartService().getAuctionCart(user);

			if (cartModel.getEntries().isEmpty())
			{
				setDeliveryAddress(cartModel, bundleTemplateId);
				getJslBundleCommerceCartService().addBundle(bundleTemplateId, cartModel);
			}
			final OrderModel orderModel = getJslBundlePlaceOrderService().placeBundleOrder(cartModel);

			return true;
		}
		catch (final InvalidCartException | CommerceCartModificationException e)
		{
			LOG.error(MessageFormat.format("Error in placing order for user : {0} bundleId : {1}", user.getUid(), bundleTemplateId),
					e);
			return false;
		}
	}

	/**
	 *
	 */
	private void setDeliveryAddress(final CartModel cartModel, final String bundleTemplateId)
	{

		if (cartModel != null && cartModel.getUser() instanceof B2BCustomerModel)
		{
			final B2BUnitModel parentb2bUnit = JslB2bUnitUtil.getParentB2bUnit(cartModel.getUser());

			if (parentb2bUnit != null)
			{
				final List<ProductModel> products = getJslBundleTemplateService().getProductListByBundleId(bundleTemplateId);
				B2BUnitModel childb2bUnit = null;
				if (!products.isEmpty())
				{
					final String plantPrefix = products.get(0).getPlant().substring(0, 1);
					if (plantPrefix.equalsIgnoreCase("1") || plantPrefix.equalsIgnoreCase("6"))
					{
						childb2bUnit = (B2BUnitModel) getB2bUnitService().getUnitForUid(parentb2bUnit.getUid() + SPLITTER
								+ JslCoreConstants.SALESORGANIZATION_1000 + SPLITTER + Config.getParameter(AUCTION_UNIT_CODE));
					}
					else if (plantPrefix.equalsIgnoreCase("5") || plantPrefix.equalsIgnoreCase("7"))
					{
						childb2bUnit = (B2BUnitModel) getB2bUnitService().getUnitForUid(parentb2bUnit.getUid() + SPLITTER
								+ JslCoreConstants.SALESORGANIZATION_1010 + SPLITTER + Config.getParameter(AUCTION_UNIT_CODE));
					}

					if (childb2bUnit != null)
					{
						final Collection<AddressModel> shippingAddress = childb2bUnit.getShippingAddresses();
						if (!CollectionUtils.isEmpty(shippingAddress))
						{
							cartModel.setDeliveryAddress(shippingAddress.iterator().next());
						}
					}
				}
			}
		}
	}

	private List<BundleTemplateModel> getWinBidList(final AuctionEventModel auctionEvent)
	{
		final List<BidEventModel> bidList = getJslBidService().getWinBidList(auctionEvent);
		final List<BundleTemplateModel> bundleTemplateList = new ArrayList<>();
		for (final BidEventModel bidEvent : bidList)
		{
			final BundleTemplateModel bundleTemplate = getJslBundleTemplateService().getBundleTemplateForBundleId(
					bidEvent.getBundleTemplate().getId(), String.valueOf(bidEvent.getAuctionEvent().getAuctionId()));
			if (null != bundleTemplate)
			{
				bundleTemplate.setBidWonPrice(bidEvent.getBidPrice());
				bundleTemplate.setBidEvent(bidEvent);
				getModelService().save(bundleTemplate);
				bundleTemplateList.add(bundleTemplate);
			}
		}

		return bundleTemplateList;
	}

	private void updateAuctionStatus(final AuctionEventModel auctionEvent)
	{
		LOG.info("Updating auction to completed state  AuctionId : " + auctionEvent.getAuctionId());
		getJslAuctionService().updateAuctionStatus(auctionEvent, AuctionStatus.COMPLETED);
	}

	private void releaseStockofUnsoldBundle(final String auctionId)
	{
		LOG.info("Updating auction to completed state  AuctionId : " + auctionId);
		getJslBundleTemplateService().releaseStockofUnsoldBundle(auctionId);
	}

	/**
	 * @return the jslBundlePlaceOrderService
	 */
	public JslBundlePlaceOrderService getJslBundlePlaceOrderService()
	{
		return jslBundlePlaceOrderService;
	}

	/**
	 * @param jslBundlePlaceOrderService
	 *           the jslBundlePlaceOrderService to set
	 */
	public void setJslBundlePlaceOrderService(final JslBundlePlaceOrderService jslBundlePlaceOrderService)
	{
		this.jslBundlePlaceOrderService = jslBundlePlaceOrderService;
	}

	/**
	 * @return the jslBundleCommerceCartService
	 */
	public JslBundleCommerceCartService getJslBundleCommerceCartService()
	{
		return jslBundleCommerceCartService;
	}

	/**
	 * @param jslBundleCommerceCartService
	 *           the jslBundleCommerceCartService to set
	 */
	public void setJslBundleCommerceCartService(final JslBundleCommerceCartService jslBundleCommerceCartService)
	{
		this.jslBundleCommerceCartService = jslBundleCommerceCartService;
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}


	/**
	 * @return the jslBidService
	 */
	public JslBidEventService getJslBidService()
	{
		return jslBidService;
	}


	/**
	 * @param jslBidService
	 *           the jslBidService to set
	 */
	public void setJslBidService(final JslBidEventService jslBidService)
	{
		this.jslBidService = jslBidService;
	}


	/**
	 * @return the jslAuctionService
	 */
	public JslAuctionService getJslAuctionService()
	{
		return jslAuctionService;
	}


	/**
	 * @param jslAuctionService
	 *           the jslAuctionService to set
	 */
	public void setJslAuctionService(final JslAuctionService jslAuctionService)
	{
		this.jslAuctionService = jslAuctionService;
	}


	/**
	 * @return the jslBundleTemplateService
	 */
	public JslBundleTemplateService getJslBundleTemplateService()
	{
		return jslBundleTemplateService;
	}


	/**
	 * @param jslBundleTemplateService
	 *           the jslBundleTemplateService to set
	 */
	public void setJslBundleTemplateService(final JslBundleTemplateService jslBundleTemplateService)
	{
		this.jslBundleTemplateService = jslBundleTemplateService;
	}


	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}


	/**
	 * @param modelService
	 *           the modelService to set
	 */
	@Override
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}


	/**
	 * @return the b2bUnitService
	 */
	public B2BUnitService getB2bUnitService()
	{
		return b2bUnitService;
	}


	/**
	 * @param b2bUnitService
	 *           the b2bUnitService to set
	 */
	public void setB2bUnitService(final B2BUnitService b2bUnitService)
	{
		this.b2bUnitService = b2bUnitService;
	}


}
