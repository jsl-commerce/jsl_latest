/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.configurablebundleservice.order.service;

import jxl.write.WritableSheet;
import jxl.write.WriteException;


/**
 *
 */
public interface JslWorkbookHeaderService
{

	void addAuctionOrderExcelHeader(final WritableSheet excelSheet, final int row, int columm) throws WriteException;

	void addAuctionOrderItemExcelHeader(final WritableSheet excelSheet, final int row, int columm) throws WriteException;

	void addZdocOrderExcelHeader(final WritableSheet excelSheet, final int row, int columm) throws WriteException;

	void addZdocOrderItemExcelHeader(final WritableSheet excelSheet, final int row, int columm) throws WriteException;

}
