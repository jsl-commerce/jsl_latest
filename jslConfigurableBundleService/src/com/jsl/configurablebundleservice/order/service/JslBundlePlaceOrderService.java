/**
 *
 */
package com.jsl.configurablebundleservice.order.service;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.InvalidCartException;


/**
 * @author manav.magoo
 *
 */
public interface JslBundlePlaceOrderService
{

	OrderModel placeBundleOrder(CartModel cart) throws InvalidCartException;

}
