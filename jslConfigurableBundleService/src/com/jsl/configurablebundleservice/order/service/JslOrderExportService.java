/**
 *
 */
package com.jsl.configurablebundleservice.order.service;

import de.hybris.platform.core.model.order.OrderModel;

import java.io.File;
import java.util.List;

import com.jsl.core.exceptions.JslExportOrderException;


/**
 * @author manav.magoo
 *
 */
public interface JslOrderExportService
{

	/**
	 * @param orders
	 * @param file
	 * @return
	 */
	boolean exportAuctionOrders(List<OrderModel> orders, File file) throws JslExportOrderException;


	/**
	 * @param orders
	 * @param file
	 * @return
	 */
	boolean exportZdocOrders(List<OrderModel> orders, File file) throws JslExportOrderException;
}
