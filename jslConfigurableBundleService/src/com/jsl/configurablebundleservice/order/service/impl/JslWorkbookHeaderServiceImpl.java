/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.configurablebundleservice.order.service.impl;

import com.jsl.configurablebundleservice.order.service.JslWorkbookHeaderService;

import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;


/**
 *
 */
public class JslWorkbookHeaderServiceImpl implements JslWorkbookHeaderService
{

	/**
	 * @param excelSheet
	 * @throws WriteException
	 * @throws RowsExceededException
	 */
	@Override
	public void addAuctionOrderExcelHeader(final WritableSheet excelSheet, final int row, int columm) throws WriteException
	{
		final WritableCellFormat cFormat = new WritableCellFormat();
		final WritableFont font = new WritableFont(WritableFont.ARIAL, 14, WritableFont.BOLD);
		cFormat.setFont(font);

		excelSheet.addCell(new Label(columm, row, "Ref", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Order Type", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Sales Org.", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Dist. Ch.", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Div.", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Sales Office", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Sold to Party Code", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Ship to Party Code", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "PO No.", cFormat)); //Auction no + lot Name
		columm++;
		excelSheet.addCell(new Label(columm, row, "PO Date", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Payment Term", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Inco Term 1", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Inco Term 2", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Order Reason", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Req. Deliv. Date", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "CAM ID", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Notify1", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Notify2", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Notify3", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Spec. Stock Partner", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Third party Remitter", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Consignee", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Buyer PO No.", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "final Destination", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Guarantee", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Doc. Date", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Creation Date", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Partial Shipment", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Clubbing", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Lot Size", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "No of Lots", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Special Remark", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Reference", cFormat));
	}


	/**
	 * @param excelSheet
	 * @throws WriteException
	 * @throws RowsExceededException
	 */
	@Override
	public void addAuctionOrderItemExcelHeader(final WritableSheet excelSheet, final int row, int columm) throws WriteException
	{
		final WritableCellFormat cFormat = new WritableCellFormat();
		final WritableFont font = new WritableFont(WritableFont.ARIAL, 14, WritableFont.BOLD);
		cFormat.setFont(font);

		excelSheet.addCell(new Label(columm, row, "Ref", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Item", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Plant", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Standard", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Material Code", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Batch", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Qty MT", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Grade Group", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Grade", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Thk", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Width", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Length", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Mat Grp", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Finish", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Edge Con.", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Location", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Amount ZPRC", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Currency ZPRC", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Amount PBBS", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Currency PBBS", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Amount ZPRD", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Currency ZPRD", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Shipment Frt (ZFRB)", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Currency(ZFRB)", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Ocean Frt(ZOCF)", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Currency ZOCF", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "ZWML", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "ZCML", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Currency ZCML", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "ZLFR", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Currency ZLFR", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "ZCMP", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "ZCAP/ZASS", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Currency ZCAP", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Quality", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "UOM", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Gross Wt.", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Coil Id", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Sloc", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Route", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Usage", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "CROSS_SECTION", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "No of Sheets/Plates", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Part wt minimum", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Part wt maximum", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Packing Standard CRD", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Packing Standard HRD", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Line Item Ref", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Thick_Tol_U", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Thick_Tol_L", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Utensil Usage", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Width_Tol_L", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Length_Tol_L", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Length_Tol_U", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "PVC", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "ILP", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "INSP_3RD_PARTY", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Cust_Ind", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "NO_OF_SLIT", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "SO_REM", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Spcl_Test1", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Mark Stck", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Desired Date", cFormat));
	}

	/**
	 * @param excelSheet
	 * @param row
	 * @param columm
	 * @param orderDto
	 * @throws WriteException
	 * @throws RowsExceededException
	 */
	/**
	 *
	 */
	@Override
	public void addZdocOrderExcelHeader(final WritableSheet excelSheet, final int row, int columm) throws WriteException
	{
		final WritableCellFormat cFormat = new WritableCellFormat();
		final WritableFont font = new WritableFont(WritableFont.ARIAL, 14, WritableFont.BOLD);
		cFormat.setFont(font);
		excelSheet.addCell(new Label(columm, row, "Ref", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Order Type", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Sales Org.", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Dist. Ch.", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Div.", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Sales Office", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Sold to Party Code", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Ship to Party Code", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "PO No.", cFormat)); //Auction no + lot Name
		columm++;
		excelSheet.addCell(new Label(columm, row, "PO Date", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Inco Term 1", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Inco Term 2", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Order Reason	", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Req. Deliv. Date", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "TEMPLATE_REC_DATE", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Partial Shipment", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Clubbing", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Lot Size", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "No of Lots", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "CAM ID", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Special Remark", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Reference", cFormat));
	}



	@Override
	public void addZdocOrderItemExcelHeader(final WritableSheet excelSheet, final int row, int columm) throws WriteException
	{
		final WritableCellFormat cFormat = new WritableCellFormat();
		final WritableFont font = new WritableFont(WritableFont.ARIAL, 14, WritableFont.BOLD);
		cFormat.setFont(font);

		excelSheet.addCell(new Label(columm, row, "Ref", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Item", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Material Code", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Plant", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Qty MT", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "No of Sheets/Plates", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Amount ZPRC", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Currency ZPRC", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Amount(ZFRB)", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Currency(ZFRB)", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Amount ZCMP", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Currency ZCMP", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Grade Group", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Grade", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Thk", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Width", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Length", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Finish", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Edge Con.", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Quality", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Part wt maximum", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Part wt minimum", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Coil Id", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Route", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Usage", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "INSP_3RD_PARTY", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "ILP", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "PVC", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Standard", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "MARK_STCK", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Marking On Product", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Utensil Usage", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "MAT_GRP", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "THK_TOL_U", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "THK_TOL_L", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "WIDTH_TOL_U", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "WIDTH_TOL_L", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "LENGTH_TOL_U", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "LENGTH_TOL_L", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "SO_REM", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Desired_Date1", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "SPCL_TEST1", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "SPCL_TEST2", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "SPCL_TEST3", cFormat));
		columm++;


	}


}
