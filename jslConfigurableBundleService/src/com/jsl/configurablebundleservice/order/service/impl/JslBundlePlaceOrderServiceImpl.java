/**
 *
 */
package com.jsl.configurablebundleservice.order.service.impl;

import de.hybris.platform.b2b.model.B2BCostCenterModel;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.services.B2BCostCenterService;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.b2bacceleratorservices.order.B2BCommerceCartService;
import de.hybris.platform.commercefacades.order.data.DeliveryModeData;
import de.hybris.platform.commercefacades.order.data.ZoneDeliveryModeData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commerceservices.delivery.DeliveryService;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.order.CommerceCheckoutService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.PriceValue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Required;

import com.jsl.configurablebundleservice.order.service.JslBundlePlaceOrderService;


/**
 * @author manav.magoo
 *
 */
public class JslBundlePlaceOrderServiceImpl implements JslBundlePlaceOrderService
{

	private static final String PAYMENT_TYPE = "ACCOUNT";
	private static final String COST_CENTER_CODE = "Rustic Retail";

	private ModelService modelService;
	private CommerceCheckoutService commerceCheckoutService;
	private Converter<ZoneDeliveryModeModel, ZoneDeliveryModeData> zoneDeliveryModeConverter;
	private Converter<DeliveryModeModel, DeliveryModeData> deliveryModeConverter;
	private PriceDataFactory priceDataFactory;
	private DeliveryService deliveryService;

	private B2BCommerceCartService commerceCartService;
	private B2BCostCenterService<B2BCostCenterModel, B2BCustomerModel> b2bCostCenterService;

	@Override
	public OrderModel placeBundleOrder(final CartModel cartModel) throws InvalidCartException
	{

		beforePlaceOrder(cartModel);

		final CommerceCheckoutParameter parameter = createCommerceCheckoutParameter(cartModel, true);
		parameter.setSalesApplication(SalesApplication.CALLCENTER);

		final OrderModel orderModel = getCommerceCheckoutService().placeOrder(parameter).getOrder();

		afterPlaceOrder(cartModel, orderModel);

		return orderModel;

	}


	/**
	 *
	 */
	private void beforePlaceOrder(final CartModel cartModel)
	{
		setPaymentType(cartModel);

		//setDefaultDeliveryAddress(cartModel);

		setDeliveryModeIfAvailable(cartModel);

		calculateCart(cartModel);
	}


	protected void afterPlaceOrder(final CartModel cartModel, final OrderModel orderModel) //NOSONAR
	{
		if (orderModel != null)
		{
			getModelService().remove(cartModel);
			getModelService().refresh(orderModel);
		}
	}

	/**
	 *
	 */
	private void calculateCart(final CartModel cartModel)
	{
		final CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setEnableHooks(true);
		parameter.setCart(cartModel);
		getCommerceCartService().calculateCart(parameter);
	}

	/**
	 *
	 */
	private void setPaymentType(final CartModel cartModel)
	{
		cartModel.setPaymentType(CheckoutPaymentType.valueOf(PAYMENT_TYPE));
		cartModel.setPaymentInfo(getCommerceCartService().createInvoicePaymentInfo(cartModel));

		final B2BCostCenterModel costCenterModel = getB2bCostCenterService().getCostCenterForCode(COST_CENTER_CODE);

		for (final AbstractOrderEntryModel abstractOrderEntry : cartModel.getEntries())
		{
			if (!Objects.equals(abstractOrderEntry.getCostCenter(), costCenterModel))
			{
				abstractOrderEntry.setCostCenter(costCenterModel);
				getModelService().save(abstractOrderEntry);
			}
		}

		getCommerceCartService().calculateCartForPaymentTypeChange(cartModel);

		getModelService().save(cartModel);
	}

	private boolean setDeliveryModeIfAvailable(final CartModel cartModel)
	{
		if (cartModel != null)
		{
			// validate delivery mode if already exists
			getCommerceCheckoutService().validateDeliveryMode(createCommerceCheckoutParameter(cartModel, true));

			if (cartModel.getDeliveryMode() == null)
			{
				final List<? extends DeliveryModeData> availableDeliveryModes = getSupportedDeliveryModes(cartModel);
				if (!availableDeliveryModes.isEmpty())
				{
					return setDeliveryMode(availableDeliveryModes.get(0).getCode(), cartModel);
				}
			}
			return true;
		}
		return false;
	}


	private List<? extends DeliveryModeData> getSupportedDeliveryModes(final CartModel cartModel)
	{
		final List<DeliveryModeData> result = new ArrayList();
		if (cartModel != null)
		{
			for (final DeliveryModeModel deliveryModeModel : getDeliveryService().getSupportedDeliveryModeListForOrder(cartModel))
			{
				result.add(convert(deliveryModeModel, cartModel));
			}
		}
		return result;
	}

	private DeliveryModeData convert(final DeliveryModeModel deliveryModeModel, final CartModel cartModel)
	{
		if (deliveryModeModel instanceof ZoneDeliveryModeModel)
		{
			final ZoneDeliveryModeModel zoneDeliveryModeModel = (ZoneDeliveryModeModel) deliveryModeModel;
			if (cartModel != null)
			{
				final ZoneDeliveryModeData zoneDeliveryModeData = getZoneDeliveryModeConverter().convert(zoneDeliveryModeModel);
				final PriceValue deliveryCost = getDeliveryService().getDeliveryCostForDeliveryModeAndAbstractOrder(deliveryModeModel,
						cartModel);
				if (deliveryCost != null)
				{
					zoneDeliveryModeData.setDeliveryCost(getPriceDataFactory().create(PriceDataType.BUY,
							BigDecimal.valueOf(deliveryCost.getValue()), deliveryCost.getCurrencyIso()));
				}
				return zoneDeliveryModeData;
			}
			return null;
		}
		return getDeliveryModeConverter().convert(deliveryModeModel);
	}


	private boolean setDeliveryMode(final String deliveryModeCode, final CartModel cartModel)
	{
		if (cartModel != null && isSupportedDeliveryMode(deliveryModeCode, cartModel))
		{
			final DeliveryModeModel deliveryModeModel = getDeliveryService().getDeliveryModeForCode(deliveryModeCode);
			if (deliveryModeModel != null)
			{
				final CommerceCheckoutParameter parameter = createCommerceCheckoutParameter(cartModel, true);
				parameter.setDeliveryMode(deliveryModeModel);
				return getCommerceCheckoutService().setDeliveryMode(parameter);
			}
		}
		return false;
	}

	private boolean isSupportedDeliveryMode(final String deliveryModeCode, final CartModel cartModel)
	{
		for (final DeliveryModeModel supportedDeliveryMode : getDeliveryService().getSupportedDeliveryModeListForOrder(cartModel))
		{
			if (deliveryModeCode.equals(supportedDeliveryMode.getCode()))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * @return
	 *
	 */
	private boolean setDefaultDeliveryAddress(final CartModel cart)
	{
		final AddressModel defaultAddress = cart.getUser().getDefaultShipmentAddress();
		if (defaultAddress != null)
		{
			final CommerceCheckoutParameter parameter = createCommerceCheckoutParameter(cart, true);
			parameter.setAddress(defaultAddress);
			parameter.setIsDeliveryAddress(false);
			return getCommerceCheckoutService().setDeliveryAddress(parameter);
		}
		return false;
	}


	private CommerceCheckoutParameter createCommerceCheckoutParameter(final CartModel cart, final boolean enableHooks)
	{
		final CommerceCheckoutParameter parameter = new CommerceCheckoutParameter();
		parameter.setEnableHooks(enableHooks);
		parameter.setCart(cart);
		return parameter;
	}



	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}



	/**
	 * @return the commerceCheckoutService
	 */
	public CommerceCheckoutService getCommerceCheckoutService()
	{
		return commerceCheckoutService;
	}



	/**
	 * @param commerceCheckoutService
	 *           the commerceCheckoutService to set
	 */
	@Required
	public void setCommerceCheckoutService(final CommerceCheckoutService commerceCheckoutService)
	{
		this.commerceCheckoutService = commerceCheckoutService;
	}

	/**
	 * @return the deliveryModeConverter
	 */
	public Converter<DeliveryModeModel, DeliveryModeData> getDeliveryModeConverter()
	{
		return deliveryModeConverter;
	}

	/**
	 * @param deliveryModeConverter
	 *           the deliveryModeConverter to set
	 */
	@Required
	public void setDeliveryModeConverter(final Converter<DeliveryModeModel, DeliveryModeData> deliveryModeConverter)
	{
		this.deliveryModeConverter = deliveryModeConverter;
	}

	/**
	 * @return the priceDataFactory
	 */
	public PriceDataFactory getPriceDataFactory()
	{
		return priceDataFactory;
	}

	/**
	 * @param priceDataFactory
	 *           the priceDataFactory to set
	 */
	@Required
	public void setPriceDataFactory(final PriceDataFactory priceDataFactory)
	{
		this.priceDataFactory = priceDataFactory;
	}

	/**
	 * @return the deliveryService
	 */
	public DeliveryService getDeliveryService()
	{
		return deliveryService;
	}

	/**
	 * @param deliveryService
	 *           the deliveryService to set
	 */
	@Required
	public void setDeliveryService(final DeliveryService deliveryService)
	{
		this.deliveryService = deliveryService;
	}

	/**
	 * @return the zoneDeliveryModeConverter
	 */
	public Converter<ZoneDeliveryModeModel, ZoneDeliveryModeData> getZoneDeliveryModeConverter()
	{
		return zoneDeliveryModeConverter;
	}

	/**
	 * @param zoneDeliveryModeConverter
	 *           the zoneDeliveryModeConverter to set
	 */
	@Required
	public void setZoneDeliveryModeConverter(
			final Converter<ZoneDeliveryModeModel, ZoneDeliveryModeData> zoneDeliveryModeConverter)
	{
		this.zoneDeliveryModeConverter = zoneDeliveryModeConverter;
	}

	/**
	 * @return the b2bCostCenterService
	 */
	public B2BCostCenterService<B2BCostCenterModel, B2BCustomerModel> getB2bCostCenterService()
	{
		return b2bCostCenterService;
	}

	/**
	 * @param b2bCostCenterService
	 *           the b2bCostCenterService to set
	 */
	@Required
	public void setB2bCostCenterService(final B2BCostCenterService<B2BCostCenterModel, B2BCustomerModel> b2bCostCenterService)
	{
		this.b2bCostCenterService = b2bCostCenterService;
	}

	/**
	 * @return the commerceCartService
	 */
	public B2BCommerceCartService getCommerceCartService()
	{
		return commerceCartService;
	}

	/**
	 * @param commerceCartService
	 *           the commerceCartService to set
	 */
	@Required
	public void setCommerceCartService(final B2BCommerceCartService commerceCartService)
	{
		this.commerceCartService = commerceCartService;
	}



}
