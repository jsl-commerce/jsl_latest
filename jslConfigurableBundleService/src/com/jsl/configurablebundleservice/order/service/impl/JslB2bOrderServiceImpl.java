/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.configurablebundleservice.order.service.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.services.impl.DefaultB2BOrderService;
import de.hybris.platform.core.model.order.AbstractOrderModel;


/**
 *
 */
public class JslB2bOrderServiceImpl extends DefaultB2BOrderService
{
	@Override
	protected boolean isB2BContext(final AbstractOrderModel order)
	{
		if (order != null && order.getUser() != null)
		{
			return order.getUser() instanceof B2BCustomerModel;
		}
		else
		{
			return false;
		}

	}
}
