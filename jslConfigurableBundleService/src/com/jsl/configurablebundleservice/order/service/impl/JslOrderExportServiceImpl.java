/**
 *
 */
package com.jsl.configurablebundleservice.order.service.impl;

import de.hybris.platform.core.model.order.OrderModel;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.jsl.configurablebundleservice.order.service.JslOrderExportService;
import com.jsl.configurablebundleservice.order.service.JslWorkbookHeaderService;
import com.jsl.configurablebundleservice.utils.JslBdcOrderDtoUtil;
import com.jsl.core.dto.JslOrderExportDto;
import com.jsl.core.dto.JslOrderItemExportDto;
import com.jsl.core.dto.JslZdocOrderExportDto;
import com.jsl.core.dto.JslZdocOrderItemExportDto;
import com.jsl.core.exceptions.JslExportOrderException;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;


/**
 * @author manav.magoo
 *
 */
public class JslOrderExportServiceImpl implements JslOrderExportService
{
	int orderHeaderRow = 0;
	int orderItemRow = 0;
	int columm = 0;

	private static final String EMPTY_STRING = "";
	private static final String SPLITTER = "-";
	private static final String AUCTION = "Auction";

	JslWorkbookHeaderService jslWorkbookHeaderService;
	JslBdcOrderDtoUtil jslBdcOrderDtoUtil;



	@Override
	public boolean exportAuctionOrders(final List<OrderModel> orders, final File file) throws JslExportOrderException
	{
		try
		{
			final List<JslOrderExportDto> orderDtoList = new ArrayList<>();

			getJslBdcOrderDtoUtil().createAuctionOrderDtos(orders, orderDtoList);

			writeWorkBook(orderDtoList, file);
		}
		catch (final IOException e)
		{
			throw new JslExportOrderException(MessageFormat.format("IO exception occured! message : {0}", e.getMessage()));
		}
		catch (final WriteException e)
		{
			throw new JslExportOrderException(MessageFormat.format("WriteException occured! message : {0}", e.getMessage()));
		}
		return true;
	}

	@Override
	public boolean exportZdocOrders(final List<OrderModel> orders, final File file) throws JslExportOrderException
	{
		try
		{
			final List<JslZdocOrderExportDto> zdocOrderDtoList = new ArrayList<>();
			getJslBdcOrderDtoUtil().createZdocOrderDtos(orders, zdocOrderDtoList);
			writeZdocWorkBook(zdocOrderDtoList, file);
		}
		catch (final IOException e)
		{
			throw new JslExportOrderException(MessageFormat.format("IO exception occured! message : {0}", e.getMessage()));
		}
		catch (final WriteException e)
		{
			throw new JslExportOrderException(MessageFormat.format("WriteException occured! message : {0}", e.getMessage()));
		}
		return true;
	}


	/**
	 *
	 */
	private void writeZdocWorkBook(final List<JslZdocOrderExportDto> zdocOrderDtoList, final File file)
			throws IOException, WriteException
	{
		this.orderHeaderRow = 0;
		this.orderItemRow = 0;
		this.columm = 0;
		final WritableWorkbook myFirstWbook = Workbook.createWorkbook(file);
		final WritableSheet excelSheet1 = myFirstWbook.createSheet("Header", 0);
		final WritableSheet excelSheet2 = myFirstWbook.createSheet("Item", 1);
		getJslWorkbookHeaderService().addZdocOrderExcelHeader(excelSheet1, orderHeaderRow, columm);
		getJslWorkbookHeaderService().addZdocOrderItemExcelHeader(excelSheet2, orderItemRow, columm);
		this.orderHeaderRow++;
		this.orderItemRow++;
		zdocOrderDtoList.forEach(orderDto -> {
			final Iterator<JslZdocOrderItemExportDto> orderItems = orderDto.getOrderItems().iterator();
			try
			{
				addZdocOrderHeaderCell(excelSheet1, orderHeaderRow, 0, orderDto);
				while (orderItems.hasNext())
				{
					final JslZdocOrderItemExportDto orderItem = orderItems.next();
					addZdocOrderItemCells(excelSheet2, orderItemRow, 0, orderItem);
					orderItemRow++;
				}
				orderHeaderRow++;
			}
			catch (final WriteException e)
			{
				throw new JslExportOrderException(MessageFormat.format("WriteException occured! message : {0}", e.getMessage()));
			}
		});
		myFirstWbook.write();
		myFirstWbook.close();
	}




	/**
	 * @throws WriteException
	 * @throws RowsExceededException
	 *
	 */
	private void addZdocOrderHeaderCell(final WritableSheet excelSheet, final int row, int colummCnt,
			final JslZdocOrderExportDto orderDto) throws RowsExceededException, WriteException
	{
		excelSheet.addCell(new Label(colummCnt, row, orderDto.getRefNo()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderDto.getOrderType()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderDto.getSalesOrganization()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderDto.getDistributionChannel()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderDto.getDivision()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderDto.getSalesOffice()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderDto.getSoldToPartyCode()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderDto.getShipToPartyCode()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderDto.getPoNumber()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderDto.getPoDate()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderDto.getIncoTerm1()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderDto.getIncoTerm2()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderDto.getOrderReason()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderDto.getRequiredDeliveryDate()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderDto.getTemplateRecordingDate()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderDto.getPartialShipment()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderDto.getClubbing()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderDto.getLotsSize()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderDto.getNoOfLots()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderDto.getCamID()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderDto.getSpecialRemark()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderDto.getReference()));

		this.columm = columm;

	}

	/**
	 * @throws WriteException
	 * @throws RowsExceededException
	 *
	 */
	private void addZdocOrderItemCells(final WritableSheet excelSheet, final int row, int colummCnt,
			final JslZdocOrderItemExportDto orderItem) throws RowsExceededException, WriteException
	{
		excelSheet.addCell(new Label(colummCnt, row, orderItem.getRefNo()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderItem.getItemNo()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderItem.getMaterialCode()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderItem.getPlant()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderItem.getQtyMT()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, EMPTY_STRING)); //Number Of sheets
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderItem.getZPRC()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderItem.getZPRCCurr()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderItem.getZFRB()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderItem.getZFRBCurr()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, EMPTY_STRING));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, EMPTY_STRING));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderItem.getGradeGroup()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderItem.getGrade()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderItem.getThk()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderItem.getWidth()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderItem.getLength()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderItem.getFinish()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderItem.getEdgeCon()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderItem.getQuality()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderItem.getPartWtMaximum()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderItem.getPartWtMinimum()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderItem.getCoilId()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderItem.getRoute()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderItem.getUsage()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderItem.getiNSP_3RD_PARTY()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderItem.getILP()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderItem.getPVC()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderItem.getStandard()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderItem.getMarkSTck()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderItem.getMarkingOnProduct()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderItem.getUtensilUsage()));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, orderItem.getMaterialGroup()));
		colummCnt++;

		excelSheet.addCell(new Label(colummCnt, row, EMPTY_STRING));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, EMPTY_STRING));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, EMPTY_STRING));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, EMPTY_STRING));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, EMPTY_STRING));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, EMPTY_STRING));
		colummCnt++;
		excelSheet.addCell(new Label(colummCnt, row, EMPTY_STRING));
		colummCnt++;

		excelSheet.addCell(new Label(colummCnt, row, orderItem.getDesired_Date1()));
		colummCnt++;
		this.columm = columm;
	}


	/**
	 * @param orderDtoList
	 * @throws IOException
	 * @throws WriteException
	 */
	private void writeWorkBook(final List<JslOrderExportDto> orderDtoList, final File file) throws IOException, WriteException
	{
		this.orderHeaderRow = 0;
		this.orderItemRow = 0;
		this.columm = 0;
		final WritableWorkbook myFirstWbook = Workbook.createWorkbook(file);
		final WritableSheet excelSheet1 = myFirstWbook.createSheet("Header", 0);
		final WritableSheet excelSheet2 = myFirstWbook.createSheet("Item", 1);
		getJslWorkbookHeaderService().addAuctionOrderExcelHeader(excelSheet1, orderHeaderRow, columm);
		getJslWorkbookHeaderService().addAuctionOrderItemExcelHeader(excelSheet2, orderItemRow, columm);
		this.orderHeaderRow++;
		this.orderItemRow++;
		orderDtoList.forEach(orderDto -> {
			try
			{
				final Iterator<JslOrderItemExportDto> orderItems = orderDto.getOrderItems().iterator();
				addOrderHeaderCell(excelSheet1, orderHeaderRow, 0, orderDto);
				while (orderItems.hasNext())
				{
					final JslOrderItemExportDto orderItem = orderItems.next();
					addOrderItemCells(excelSheet2, orderItemRow, 0, orderItem);
					orderItemRow++;
				}
				orderHeaderRow++;
			}
			catch (final WriteException e)
			{
				// XXX Auto-generated catch block
				e.printStackTrace();
			}
		});
		myFirstWbook.write();
		myFirstWbook.close();
	}



	/**
	 * @param excelSheet
	 * @param row
	 * @param columm
	 * @param orderDto
	 * @throws WriteException
	 * @throws RowsExceededException
	 */
	private void addOrderItemCells(final WritableSheet excelSheet, final int row, int columm,
			final JslOrderItemExportDto orderItem) throws WriteException
	{
		excelSheet.addCell(new Label(columm, row, orderItem.getRefNo()));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderItem.getPlant()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderItem.getStandard()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderItem.getMaterialCode()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderItem.getBatch()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderItem.getQtyMT()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderItem.getGradeGroup()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderItem.getGrade()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderItem.getThk()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderItem.getWidth()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderItem.getLength()));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderItem.getFinish()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderItem.getEdgeCon()));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderItem.getZPRC()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderItem.getZPRCCurr()));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderItem.getZFRB()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderItem.getZFRBCurr()));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderItem.getQuality()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderItem.getUoM()));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderItem.getCoilId()));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderItem.getRoute()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderItem.getUsage()));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, "NO")); //orderItem.getUtensilUsage()
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderItem.getPVC()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderItem.getILP()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderItem.getiNSP_3RD_PARTY()));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderItem.getMarkSTck()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderItem.getDesired_Date1()));
		this.columm = columm;
	}

	/**
	 * @param excelSheet
	 * @param row
	 * @param columm
	 * @param orderDto
	 * @throws WriteException
	 * @throws RowsExceededException
	 */
	private void addOrderHeaderCell(final WritableSheet excelSheet, final int row, int columm, final JslOrderExportDto orderDto)
			throws WriteException
	{
		excelSheet.addCell(new Label(columm, row, orderDto.getRefNo()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderDto.getTransactionType()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderDto.getSalesOrganization()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderDto.getDistributionChannel()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderDto.getDivision()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderDto.getSalesOffice()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderDto.getSoldToPartyCode()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderDto.getShipToPartyCode()));
		columm++;
		excelSheet.addCell(new Label(columm, row, AUCTION + SPLITTER + orderDto.getAuctionId() + SPLITTER + orderDto.getLotName()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderDto.getPoDate()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderDto.getPaymentTerm()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderDto.getIncoTerm1()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderDto.getIncoTerm2()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderDto.getOrderReason()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderDto.getPoDate()));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, EMPTY_STRING));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderDto.getCreationDate()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderDto.getPartialShipment()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderDto.getClubbing()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderDto.getLotsSize()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderDto.getNoOfLots()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderDto.getSpecialRemark()));
		columm++;
		excelSheet.addCell(new Label(columm, row, orderDto.getReference()));

		this.columm = columm;
	}




	/**
	 * @return the jslWorkbookHeaderService
	 */
	public JslWorkbookHeaderService getJslWorkbookHeaderService()
	{
		return jslWorkbookHeaderService;
	}

	/**
	 * @param jslWorkbookHeaderService
	 *           the jslWorkbookHeaderService to set
	 */
	public void setJslWorkbookHeaderService(final JslWorkbookHeaderService jslWorkbookHeaderService)
	{
		this.jslWorkbookHeaderService = jslWorkbookHeaderService;
	}

	/**
	 * @return the jslBdcOrderDtoUtil
	 */
	public JslBdcOrderDtoUtil getJslBdcOrderDtoUtil()
	{
		return jslBdcOrderDtoUtil;
	}

	/**
	 * @param jslBdcOrderDtoUtil
	 *           the jslBdcOrderDtoUtil to set
	 */
	public void setJslBdcOrderDtoUtil(final JslBdcOrderDtoUtil jslBdcOrderDtoUtil)
	{
		this.jslBdcOrderDtoUtil = jslBdcOrderDtoUtil;
	}
}
