/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.configurablebundleservice.order.calculation.hook;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commerceservices.order.hook.CommerceCartCalculationMethodHook;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.configurablebundleservices.bundle.BundleTemplateService;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.order.EntryGroup;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Nonnull;


/**
 *
 */
public class JslBundleCalculateCartMethodHook implements CommerceCartCalculationMethodHook
{
	ModelService modelService;
	BundleTemplateService bundleTemplateService;

	/**
	 * Determines whether the entry applies to bundle.
	 *
	 * @param parameter
	 *           entry definition
	 * @return true is it's a bundled entry
	 */
	protected boolean isBundledEntry(@Nonnull
	final CommerceCartParameter parameter)
	{
		if (parameter.getCart().getEntryGroups() != null && !parameter.getCart().getEntryGroups().isEmpty())
		{
			return true;
		}
		return false;
	}


	private boolean checkValidBundleEntry(final EntryGroup entryGroup)
	{
		if (!entryGroup.getChildren().isEmpty())
		{
			final BundleTemplateModel bundle = getBundle(entryGroup.getChildren().get(0).getExternalReferenceId());
			if (bundle != null)
			{
				return true;
			}
		}
		return false;
	}


	/**
	 *
	 */
	private BundleTemplateModel getBundle(final String bundleTemplateId)
	{
		return getBundleTemplateService().getBundleTemplateForCode(bundleTemplateId);
	}

	@Override
	public void afterCalculate(final CommerceCartParameter parameter)
	{
		validateParameterNotNullStandardMessage("parameter", parameter);
		final CartModel cartModel = parameter.getCart();
		if (isBundledEntry(parameter))
		{
			final EntryGroup entryGroup = parameter.getCart().getEntryGroups().get(0);
			if (checkValidBundleEntry(entryGroup))
			{
				//Set Auction Won price of Bundle on order as set on cart
				final BundleTemplateModel bundleTemplate = getBundle(entryGroup.getChildren().get(0).getExternalReferenceId());
				if (null != bundleTemplate.getBidWonPrice())
				{
					final double bidWonPricePerTon = bundleTemplate.getBidWonPrice().doubleValue();
					final double totalBundlePrice = (bidWonPricePerTon * bundleTemplate.getQuantity());
					cartModel.setTotalPrice(totalBundlePrice);
					getModelService().save(cartModel);
				}
			}
		}
	}

	@Override
	public void beforeCalculate(final CommerceCartParameter parameter)
	{
		// XXX Auto-generated method stub

	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}


	/**
	 * @return the bundleTemplateService
	 */
	public BundleTemplateService getBundleTemplateService()
	{
		return bundleTemplateService;
	}


	/**
	 * @param bundleTemplateService
	 *           the bundleTemplateService to set
	 */
	public void setBundleTemplateService(final BundleTemplateService bundleTemplateService)
	{
		this.bundleTemplateService = bundleTemplateService;
	}


}
