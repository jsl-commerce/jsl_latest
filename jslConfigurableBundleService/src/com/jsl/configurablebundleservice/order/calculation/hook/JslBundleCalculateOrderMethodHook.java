/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.configurablebundleservice.order.calculation.hook;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commerceservices.order.hook.CommercePlaceOrderMethodHook;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.configurablebundleservices.bundle.BundleTemplateService;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.order.EntryGroup;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Nonnull;


/**
 *
 */
public class JslBundleCalculateOrderMethodHook implements CommercePlaceOrderMethodHook
{

	ModelService modelService;
	BundleTemplateService bundleTemplateService;

	/**
	 * @return
	 *
	 */
	private boolean checkValidBundleEntry(final EntryGroup entryGroup)
	{
		if (!entryGroup.getChildren().isEmpty())
		{
			final BundleTemplateModel bundle = getBundle(entryGroup.getChildren().get(0).getExternalReferenceId());
			if (bundle != null)
			{
				return true;
			}
		}
		return false;
	}

	/**
	 *
	 */
	private BundleTemplateModel getBundle(final String bundleTemplateId)
	{
		return getBundleTemplateService().getBundleTemplateForCode(bundleTemplateId);
	}

	@Override
	public void beforePlaceOrder(final CommerceCheckoutParameter parameter) throws InvalidCartException
	{
		// XXX Auto-generated method stub

	}

	@Override
	public void beforeSubmitOrder(final CommerceCheckoutParameter parameter, final CommerceOrderResult result)
			throws InvalidCartException
	{
		validateParameterNotNullStandardMessage("parameter", parameter);
		if (isBundledEntry(parameter))
		{
			final EntryGroup entryGroup = parameter.getCart().getEntryGroups().get(0);
			if (checkValidBundleEntry(entryGroup))
			{
				final BundleTemplateModel bundleTemplate = getBundle(entryGroup.getChildren().get(0).getExternalReferenceId());
				if (null != bundleTemplate.getBidWonPrice())
				{
					result.getOrder().setLotsQuantity(bundleTemplate.getQuantity());
					final double totalWonPrice = (bundleTemplate.getQuantity() * bundleTemplate.getBidWonPrice().doubleValue());
					result.getOrder().setTotalPrice(totalWonPrice + result.getOrder().getDeliveryCost());
					result.getOrder().setAuctionId(bundleTemplate.getAuctionEvent().getAuctionId().toString());

					result.getOrder().setPurchaseOrderNumber(result.getOrder().getAuctionId() + "-" + bundleTemplate.getName());
					modelService.save(result.getOrder());
				}
			}
		}

	}

	protected boolean isBundledEntry(@Nonnull
	final CommerceCheckoutParameter parameter)
	{
		if (parameter.getCart().getEntryGroups() != null && !parameter.getCart().getEntryGroups().isEmpty())
		{
			return true;
		}
		return false;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * @return the bundleTemplateService
	 */
	public BundleTemplateService getBundleTemplateService()
	{
		return bundleTemplateService;
	}

	/**
	 * @param bundleTemplateService
	 *           the bundleTemplateService to set
	 */
	public void setBundleTemplateService(final BundleTemplateService bundleTemplateService)
	{
		this.bundleTemplateService = bundleTemplateService;
	}

	@Override
	public void afterPlaceOrder(final CommerceCheckoutParameter parameter, final CommerceOrderResult orderModel)
			throws InvalidCartException
	{
		// XXX Auto-generated method stub

	}

}
