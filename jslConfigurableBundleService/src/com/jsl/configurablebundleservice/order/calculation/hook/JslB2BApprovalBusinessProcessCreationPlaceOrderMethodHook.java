/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.configurablebundleservice.order.calculation.hook;


import de.hybris.platform.b2b.order.hooks.B2BApprovalBusinessProcessCreationPlaceOrderMethodHook;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;

import org.apache.log4j.Logger;

import com.jsl.core.enums.JslCartType;




/**
 *
 */
public class JslB2BApprovalBusinessProcessCreationPlaceOrderMethodHook
		extends B2BApprovalBusinessProcessCreationPlaceOrderMethodHook
{
	private static final Logger LOG = Logger.getLogger(JslB2BApprovalBusinessProcessCreationPlaceOrderMethodHook.class);

	@Override
	public void afterPlaceOrder(final CommerceCheckoutParameter commerceCheckoutParameter,
			final CommerceOrderResult commerceOrderResult)
	{

		if (!isB2BContext(commerceOrderResult.getOrder()))
		{
			return;
		}

		if (!commerceOrderResult.getOrder().getType().equals(JslCartType.AUCTION))
		{
			return;
		}

		if (LOG.isDebugEnabled())
		{
			LOG.debug(String.format("Post processing a b2b order %s created from cart", commerceOrderResult.getOrder()));
		}
		getBusinessProcessCreationStrategy().createB2BBusinessProcess(commerceOrderResult.getOrder());
	}


}
