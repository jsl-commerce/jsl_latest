/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.configurablebundleservice.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.configurablebundleservices.model.BundleSelectionCriteriaModel;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateStatusModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.jsl.configurablebundleservice.daos.JslBundleTemplateDao;
import com.jsl.configurablebundleservice.service.JslBundleTemplateService;
import com.jsl.core.dto.DefaultAuctionCommonDto;
import com.jsl.core.services.JslAuctionService;
import com.jsl.jslConfigurableBundleService.model.WatchListModel;


@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class JslBundleTemplateServiceImplTest
{

	@Mock
	private KeyGenerator bundleKeyGenerator;
	@Mock
	private JslBundleTemplateDao jslBundleTemplateDao;
	@Mock
	private JslAuctionService jslAuctionService;
	@Mock
	private UserService userService;
	@Mock
	private ProductModel ProductModel;
	@Mock
	private List<ProductModel> productsList;
	@Mock
	private CatalogVersionModel catalogVersionModel;
	@Mock
	private ModelService modelService;
	@Mock
	private BundleTemplateModel bundleTemplateModel;
	@Mock
	private JslBundleTemplateService jslBundleTemplateService;
	@Mock
	private JslBundleTemplateServiceImpl jslBundleTemplateServiceImpl;
	@Mock
	private WatchListModel watchListModel;
	@Mock
	private UserModel userModel;
	@Mock
	private DefaultAuctionCommonDto auctionDateRange;
	@Mock
	private BundleSelectionCriteriaModel bundleSelectionCriteriaModel;
	@Mock
	private BundleTemplateStatusModel bundleTemplateStatusModel;
	private final String AUCTION_ID = "1001";
	private final String BUNDLE_ID = "2001";
	private final String NAME = "Jhan";
	private final String ID = "123";

	@Before
	public void setup()
	{
		final ProductModel pm = new de.hybris.platform.core.model.product.ProductModel();
		final List<ProductModel> productModelList = new ArrayList<ProductModel>();
		productModelList.add(pm);
	}

	@Test
	public void getProductListByBundleIdTest()
	{
		Mockito.when(jslBundleTemplateServiceImpl.getProductListByBundleId(Mockito.any())).thenReturn(ProductModel());
		final List<ProductModel> result = jslBundleTemplateServiceImpl.getProductListByBundleId(BUNDLE_ID);
		assertNotEquals("getProductListByBundleId should returns a list of productModels", 0, result.size());
	}

	@Test
	public void getCompletedAuctionDetailsBasedOnDateRangeTest() throws ParseException
	{
/*		Mockito.when(jslBundleTemplateServiceImpl.getCompletedAuctionDetailsBasedOnDateRange(Mockito.any()))
				.thenReturn(getBundleTemplateModelList());*/
		/*final List<BundleTemplateModel> result = jslBundleTemplateServiceImpl
				.getCompletedAuctionDetailsBasedOnDateRange(auctionDateRange);
		assertNotEquals("getCompletedAuctionDetailsBasedOnDateRange should returns a list of BundleTemplateModel", 0,
				result.size());*/
	}

	@Test
	public void createBundleTemplateTest()
	{
		final BundleTemplateModel b = new BundleTemplateModel();
		Mockito.when(jslBundleTemplateServiceImpl.createBundleTemplate(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(b);
		ProductModel = new de.hybris.platform.core.model.product.ProductModel();
		final List<ProductModel> productModelList = new ArrayList<ProductModel>();
		productModelList.add(ProductModel);
		catalogVersionModel = new CatalogVersionModel();
		final BundleTemplateModel result = jslBundleTemplateServiceImpl.createBundleTemplate(NAME, productModelList,
				catalogVersionModel);
		assertNotNull("createBundleTemplate should returns one ProductModel", result);

	}



	@Test
	public void getBundleSelectionCriteriaForCodeTest()
	{
		final BundleSelectionCriteriaModel b = new BundleSelectionCriteriaModel();
		Mockito.when(jslBundleTemplateServiceImpl.getBundleSelectionCriteriaForCode(Mockito.any(), Mockito.any())).thenReturn(b);
		final BundleSelectionCriteriaModel result = jslBundleTemplateServiceImpl.getBundleSelectionCriteriaForCode(ID,
				catalogVersionModel);
		assertNotNull("getBundleSelectionCriteriaForCode should returns one ProductModel", result);
	}

	@Test
	public void getBundleTemplateStatusForCodeTest()
	{
		final BundleTemplateStatusModel b = new BundleTemplateStatusModel();
		Mockito.when(jslBundleTemplateServiceImpl.getBundleTemplateStatusForCode(Mockito.any(), Mockito.any())).thenReturn(b);

		final BundleTemplateStatusModel result = jslBundleTemplateServiceImpl.getBundleTemplateStatusForCode(ID,
				catalogVersionModel);
		assertNotNull("getBundleSelectionCriteriaForCode should returns one ProductModel", result);
	}

	@Test
	public void getBundleTemplatesForAuctionIdTest()
	{
		Mockito.when(jslBundleTemplateServiceImpl.getBundleTemplatesForAuctionId(Mockito.any()))
				.thenReturn(getBundleTemplateModelList());
		final List<BundleTemplateModel> result = jslBundleTemplateServiceImpl.getBundleTemplatesForAuctionId(AUCTION_ID);
		assertNotEquals("getBundleTemplatesForAuctionId should returns a list of BundleTemplateModel", 0, result.size());
	}

	@Test
	public void removeFromWatchListTest()
	{
		final boolean result = jslBundleTemplateServiceImpl.removeFromWatchList(bundleTemplateModel);
		assertEquals("removeFromWatchList should return boolean result", false, result);
	}

	@Test
	public void getWatchListDetailsTest()
	{
		Mockito.when(jslBundleTemplateServiceImpl.getWatchListDetails(Mockito.any())).thenReturn(getWatchListModelList());
		final List<WatchListModel> result = jslBundleTemplateServiceImpl.getWatchListDetails(AUCTION_ID);
		assertNotEquals("getWatchListDetails should returns a list of WatchListModel", 0, result.size());
	}

	@Test
	public void getCompletedAuctionDetailsTest()
	{
		String flowName = "completedPage";
		Mockito.when(jslBundleTemplateServiceImpl.getCompletedAuctionDetails(flowName)).thenReturn(getBundleTemplateModelList());
		final List<BundleTemplateModel> result = jslBundleTemplateServiceImpl.getCompletedAuctionDetails(flowName);
		assertNotEquals("getCompletedAuctionDetails should returns a list of BundleTemplateModel", 0, result.size());

	}

	@Test
	public void getUpcomingAuctionDetailsTest()
	{
		Mockito.when(jslBundleTemplateServiceImpl.getUpcomingAuctionDetails()).thenReturn(getBundleTemplateModelList());
		final List<BundleTemplateModel> result = jslBundleTemplateServiceImpl.getUpcomingAuctionDetails();
		assertNotEquals("getCompletedAuctionDetails should returns a list of BundleTemplateModel", 0, result.size());
	}

	@Test
	public void getOngoingAuctionDetailsTest()
	{
		Mockito.when(jslBundleTemplateServiceImpl.getOngoingAuctionDetails(Mockito.any())).thenReturn(getBundleTemplateModelList());
		final List<BundleTemplateModel> result = jslBundleTemplateServiceImpl.getOngoingAuctionDetails("AUCTION_ID");
		assertNotEquals("getOngoingAuctionDetails should returns a list of BundleTemplateModel", 0, result.size());
	}


	private List<ProductModel> ProductModel()
	{
		final List<ProductModel> mockList = new ArrayList<ProductModel>();
		final ProductModel pm = new ProductModel();
		pm.setCode("1");
		mockList.add(ProductModel);
		return mockList;
	}



	private List<BundleTemplateModel> getBundleTemplateModelList()
	{
		final List<BundleTemplateModel> mockResult = new ArrayList<BundleTemplateModel>();
		final BundleTemplateModel btm = new BundleTemplateModel();
		btm.setId("1001");
		mockResult.add(btm);
		return mockResult;
	}

	private List<WatchListModel> getWatchListModelList()
	{
		final List<WatchListModel> mockResult = new ArrayList<WatchListModel>();
		final WatchListModel wlm = new WatchListModel();
		wlm.setWatchListId("1001");
		mockResult.add(wlm);
		return mockResult;
	}
}
