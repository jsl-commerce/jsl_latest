/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.configurablebundleservice.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class JslBundleNavigationServiceImplTest
{

	@InjectMocks
	JslBundleNavigationServiceImpl jslBundleNavigationServiceImpl;
	@Mock
	private BundleTemplateModel bundleTemplateModel;
	@Mock
	private ProductModel productModel;
	@Mock
	ModelService modelService;

	@Test
	public void addTest()
	{
		final List<ProductModel> productModels = new ArrayList<>();
		Mockito.when(bundleTemplateModel.getProducts()).thenReturn(productModels);

		jslBundleNavigationServiceImpl.add(bundleTemplateModel, productModels);
		Mockito.verify(bundleTemplateModel).getProducts();
	}

	@Test
	public void removeTest()
	{
		final List<ProductModel> productModels = new ArrayList<>();
		Mockito.when(bundleTemplateModel.getProducts()).thenReturn(productModels);
		jslBundleNavigationServiceImpl.remove(bundleTemplateModel, productModel);
		Mockito.verify(bundleTemplateModel).getProducts();

	}
}
