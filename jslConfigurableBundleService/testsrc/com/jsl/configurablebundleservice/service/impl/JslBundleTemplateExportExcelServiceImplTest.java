/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.configurablebundleservice.service.impl;

import static org.junit.Assert.assertNotEquals;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class JslBundleTemplateExportExcelServiceImplTest
{
	@Mock
	private JslBundleTemplateExportExcelServiceImpl jslBundleTemplateExportExcelServiceImpl;
	@Mock
	private BundleTemplateModel bundleTemplateModel;

	@Test
	public void getAllBundleTemplatesTest()
	{
		Mockito.when(jslBundleTemplateExportExcelServiceImpl.getAllBundleTemplates()).thenReturn(getBundleTemplateModelList());
		final List<BundleTemplateModel> result = jslBundleTemplateExportExcelServiceImpl.getAllBundleTemplates();
		assertNotEquals("getAllBundleTemplates should returns a list of BundleTemplateModel", 0, result.size());
	}

	private List<BundleTemplateModel> getBundleTemplateModelList()
	{
		final List<BundleTemplateModel> mockList = new ArrayList<BundleTemplateModel>();
		final BundleTemplateModel btm = new BundleTemplateModel();
		btm.setId("1001");
		mockList.add(btm);
		return mockList;

	}
}
