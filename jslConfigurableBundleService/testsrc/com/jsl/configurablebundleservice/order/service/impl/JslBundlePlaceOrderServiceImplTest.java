/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.configurablebundleservice.order.service.impl;

import static org.junit.Assert.assertNotNull;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.InvalidCartException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class JslBundlePlaceOrderServiceImplTest
{
	@Mock
	private JslBundlePlaceOrderServiceImpl jslBundlePlaceOrderServiceImpl;
	@Mock
	private CartModel cartModel;
	@Mock
	private OrderModel orderModel;

	@Test
	public void placeBundleOrdertest() throws InvalidCartException
	{

		final OrderModel om = new OrderModel();
		Mockito.when(jslBundlePlaceOrderServiceImpl.placeBundleOrder(Mockito.any())).thenReturn(om);
		final OrderModel result = jslBundlePlaceOrderServiceImpl.placeBundleOrder(cartModel);
		assertNotNull("placeBundleOrdertest should return one OrderModel", result);
	}
}
