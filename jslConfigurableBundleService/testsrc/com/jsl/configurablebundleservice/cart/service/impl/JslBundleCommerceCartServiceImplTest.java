/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.configurablebundleservice.cart.service.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.strategies.NetGrossStrategy;
import de.hybris.platform.configurablebundleservices.bundle.BundleTemplateService;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class JslBundleCommerceCartServiceImplTest
{

	@InjectMocks
	private JslBundleCommerceCartServiceImpl jslBundleCommerceCartServiceImpl;

	@Mock
	private KeyGenerator keyGenerator;
	@Mock
	private ModelService modelService;
	@Mock
	private CommonI18NService commonI18NService;
	@Mock
	private BundleTemplateService bundleTemplateService;
	@Mock
	private Converter<CommerceCartModification, CartModificationData> cartModificationConverter;
	@Mock
	private CommerceCartService commerceCartService;

	@Mock
	private I18NService i18nService;

	@Mock
	private B2BUnitService<B2BUnitModel, B2BCustomerModel> b2bUnitService;

	@Mock
	private NetGrossStrategy netGrossStrategy;
	@Mock
	private BaseSiteService baseSiteService;
	@Mock
	private BaseStoreService baseStoreService;
	@Mock
	private KeyGenerator guidKeyGenerator;


	@Test
	public void testAddBundle() throws Exception
	{
		final BundleTemplateModel bt = new BundleTemplateModel();

		final List<ProductModel> pList = new ArrayList<ProductModel>();

		final ProductModel p = new ProductModel();
		pList.add(p);
		bt.setProducts(pList);
		Mockito.when(bundleTemplateService.getBundleTemplateForCode("test")).thenReturn(bt);
		Mockito.when(cartModificationConverter.convert(Mockito.any())).thenReturn(new CartModificationData());
		final String bundleTemplateId = "test";
		final CartModel cartModel = new CartModel();
		assertTrue(jslBundleCommerceCartServiceImpl.addBundle(bundleTemplateId, cartModel).size() >= 0);
	}


	@Test
	public void testGetAuctionCart()
	{
		final UserModel user = new UserModel();
		final CartModel cart = new CartModel();
		final BaseStoreModel baseStoreModel = new BaseStoreModel();
		baseStoreModel.setDefaultCurrency(null);
		final List<CartModel> carts = new ArrayList<CartModel>();
		Mockito.when(keyGenerator.generate()).thenReturn(123);
		Mockito.when(modelService.create(Mockito.anyString())).thenReturn(cart);
		Mockito.when(guidKeyGenerator.generate()).thenReturn("123");
		user.setCarts(carts);
		Mockito.when(baseStoreService.getBaseStoreForUid("jsl")).thenReturn(baseStoreModel);
		Mockito.when(baseSiteService.getBaseSiteForUID("jsl")).thenReturn(new BaseSiteModel());
		final Locale locale = new Locale("eng");
		Mockito.when(i18nService.getCurrentLocale()).thenReturn(locale);

		assertNotNull(jslBundleCommerceCartServiceImpl.getAuctionCart(user));
	}
}
