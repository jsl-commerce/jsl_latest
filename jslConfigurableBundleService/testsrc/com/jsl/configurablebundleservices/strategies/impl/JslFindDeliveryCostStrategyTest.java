/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.configurablebundleservices.strategies.impl;

import static org.junit.Assert.assertNotNull;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.configurablebundleservices.bundle.BundleTemplateService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.order.EntryGroup;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.jsl.core.enums.JslCartType;
import com.jsl.core.services.JslAuctionDeliveryService;


/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class JslFindDeliveryCostStrategyTest
{

	@InjectMocks
	JslFindDeliveryCostStrategy jslFindDeliveryCostStrategy;
	@Mock
	ModelService modelService;
	@Mock
	BundleTemplateService bundleTemplateService;
	@Mock
	JslAuctionDeliveryService jslAuctionDeliveryService;

	@Test
	public void testGetDeliveryCost()
	{
		final AbstractOrderModel order = new AbstractOrderModel();
		final List<EntryGroup> lst = new ArrayList<EntryGroup>();
		order.setType(JslCartType.AUCTION);
		order.setEntryGroups(lst);
		final CurrencyModel cm = new CurrencyModel();
		cm.setIsocode("001");
		order.setCurrency(cm);
		order.setNet(true);
		assertNotNull(jslFindDeliveryCostStrategy.getDeliveryCost(order));
	}

}
