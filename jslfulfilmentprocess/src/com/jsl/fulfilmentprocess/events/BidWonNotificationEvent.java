/**
 *
 */
package com.jsl.fulfilmentprocess.events;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;


/**
 * @author manav.magoo
 *
 */
public class BidWonNotificationEvent extends AbstractEvent
{
	private final OrderProcessModel process;

	public BidWonNotificationEvent(final OrderProcessModel process)
	{
		this.process = process;
	}

	public OrderProcessModel getProcess()
	{
		return process;
	}
}
