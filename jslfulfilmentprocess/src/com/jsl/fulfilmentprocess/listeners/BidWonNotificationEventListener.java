/**
 *
 */
package com.jsl.fulfilmentprocess.listeners;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import de.hybris.platform.servicelayer.model.ModelService;

import org.springframework.beans.factory.annotation.Required;

import com.jsl.fulfilmentprocess.events.BidWonNotificationEvent;


/**
 * @author manav.magoo
 *
 */
public class BidWonNotificationEventListener extends AbstractEventListener<BidWonNotificationEvent>
{
	private ModelService modelService;
	private BusinessProcessService businessProcessService;

	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}


	@Override
	protected void onEvent(final BidWonNotificationEvent bidWonNotificationEvent)
	{
		final OrderModel orderModel = bidWonNotificationEvent.getProcess().getOrder();

		final OrderProcessModel orderProcessModel = (OrderProcessModel) getBusinessProcessService().createProcess(
				"bidwon-notification-process" + "-" + orderModel.getCode() + "-" + System.currentTimeMillis(),
				"bidwon-notification-process");
		orderProcessModel.setOrder(orderModel);
		getModelService().save(orderProcessModel);
		getBusinessProcessService().startProcess(orderProcessModel);

	}
}




