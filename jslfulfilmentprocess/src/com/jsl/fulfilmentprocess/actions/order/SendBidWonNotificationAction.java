/**
 *
 */
package com.jsl.fulfilmentprocess.actions.order;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.servicelayer.event.EventService;

import org.apache.log4j.Logger;

import com.jsl.core.enums.JslCartType;
import com.jsl.fulfilmentprocess.events.BidWonNotificationEvent;



/**
 * @author manav.magoo
 *
 */
public class SendBidWonNotificationAction extends AbstractProceduralAction<OrderProcessModel>
{
	private static final Logger LOG = Logger.getLogger(SendBidWonNotificationAction.class);
	private EventService eventService;

	@Override
	public void executeAction(final OrderProcessModel process)
	{
		final OrderModel orderModel = process.getOrder();
		if (JslCartType.AUCTION.equals(orderModel.getType()))
		{
			getEventService().publishEvent(new BidWonNotificationEvent(process));
			Logger.getLogger(getClass()).info("Process: " + process.getCode() + " in step " + getClass());
		}

	}

	/**
	 * @return the eventService
	 */
	public EventService getEventService()
	{
		return eventService;
	}

	/**
	 * @param eventService
	 *           the eventService to set
	 */
	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}

}
