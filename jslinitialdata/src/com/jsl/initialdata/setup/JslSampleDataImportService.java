/**
 *
 */
package com.jsl.initialdata.setup;

import de.hybris.platform.commerceservices.dataimport.impl.SampleDataImportService;


/**
 * @author krishna.p.kumar
 *
 *         hook of custom impex
 */
public class JslSampleDataImportService extends SampleDataImportService
{

	@Override
	protected void importCommonData(final String extensionName)
	{
		super.importCommonData(extensionName);

		getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/commerceorg/bundle.impex", extensionName),
				false);

		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/sampledata/commerceorg/HollowareCatalog.impex", extensionName), false);


		getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/commerceorg/rules.impex", extensionName),
				false);
		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/sampledata/commerceorg/auction-otp-job.impex", extensionName), false);
		getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/commerceorg/email.impex", extensionName),
				false);
		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/sampledata/commerceorg/personalization.impex", extensionName), false);
		getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/commerceorg/template1.impex", extensionName),
				false);
		getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/commerceorg/template2.impex", extensionName),
				false);
		getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/commerceorg/user-groups.impex", extensionName),
				false);
		getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/commerceorg/warehouse.impex", extensionName),
				false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/commerceorg/auction-place-order-job.impex", extensionName), false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/commerceorg/jsl-update-auction-status.impex", extensionName), false);
		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/sampledata/commerceorg/delivery-cost.impex", extensionName), false);


		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/commerceorg/Final_Order_Approval_Unit.impex", extensionName), false);

		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/sampledata/commerceorg/order_cms_comtent.impex", extensionName), false);

		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/sampledata/commerceorg/order_cms_comtent_en.impex", extensionName), false);

		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/sampledata/commerceorg/OrderApproval.impex", extensionName), false);

		/*
		 * CPI Impexes
		 */

		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/commerceorg/Customermastermandatory.impex", extensionName), false);

		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/commerceorg/essentialdata-InboundB2BUnit.impex", extensionName), false);

		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/commerceorg/Inbound-AuctionProduct.impex", extensionName), false);

		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/sampledata/commerceorg/Inbound-Stock.impex", extensionName), false);

		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/sampledata/commerceorg/Inbound-Pricerow.impex", extensionName), false);

		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/sampledata/commerceorg/Inbound-Pricerow.impex", extensionName), false);

		getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/commerceorg/deliveryMode.impex", extensionName),
				false);
		/*
		 * Customer 360 Reports
		 */
		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/sampledata/commerceorg/Customer360Reports.impex", extensionName), false);

		/*
		 * PAYMENT RELATED IMPEXES
		 */
		getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/commerceorg/Payment.impex", extensionName),
				false);

	}
}
