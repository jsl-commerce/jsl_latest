# -----------------------------------------------------------------------
# [y] hybris Platform
#
# Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
#
# This software is the confidential and proprietary information of SAP
# ("Confidential Information"). You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the
# license agreement you entered into with SAP.
# -----------------------------------------------------------------------
#
# Import the CMS content for the JSSL site emails
#
$contentCatalog=jsslContentCatalog
$contentCV=catalogVersion(CatalogVersion.catalog(Catalog.id[default=$contentCatalog]),CatalogVersion.version[default=Staged])[default=$contentCatalog:Staged]
$jarEmailResource=jar:com.jsl.initialdata.constants.JslInitialDataConstants&/jslinitialdata/import/coredata/contentCatalogs/jslContentCatalog/emails

# Import config properties into impex macros for modulegen
UPDATE GenericItem[processor=de.hybris.platform.commerceservices.impex.impl.ConfigPropertyImportProcessor];pk[unique=true]
$emailResource=$config-emailResourceValue

# Language
$lang=en

# CMS components and Email velocity templates
UPDATE RendererTemplate;code[unique=true];description[lang=$lang];templateScript[lang=$lang,translator=de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
 ;jsl-BannerComponent-template;"CMSBannerComponent Template";$emailResource/email-bannerComponentTemplate.vm
 ;jsl-CMSImageComponent-template;"CMSImageComponent Template";$emailResource/email-cmsImageComponentTemplate.vm
 ;jsl-CMSLinkComponent-template;"CMSLinkComponent Template";$emailResource/email-cmsLinkComponentTemplate.vm
 ;jsl-CMSParagraphComponent-template;"CMSParagraphComponent Template";$emailResource/email-cmsParagraphComponentTemplate.vm
 ;jsl-SimpleBannerComponent-template;"CMSSimpleBannerComponent Template";$emailResource/email-bannerComponentTemplate.vm
 ;jsl_Email_Customer_Registration_Body;"Customer Registration Email Body";$emailResource/email-customerRegistrationBody.vm
 ;jsl_Email_Customer_Registration_Subject;"Customer Registration Email Subject";$emailResource/email-customerRegistrationSubject.vm
 ;jsl_Email_Delivery_Sent_Body;"Delivery Sent Email Body";$emailResource/email-deliverySentBody.vm
 ;jsl_Email_Delivery_Sent_Subject;"Delivery Sent Email Subject";$emailResource/email-deliverySentSubject.vm
 ;jsl_Email_Forgotten_Password_Body;"Forgotten Password Email Body";$emailResource/email-forgottenPasswordBody.vm
 ;jsl_Email_Forgotten_Password_Subject;"Forgotten Password Email Subject";$emailResource/email-forgottenPasswordSubject.vm
 ;jsl_Email_NotPickedUpConsignment_Canceled_Body;"Not Picked Up Consignment Canceled Email Body";$emailResource/email-notPickedUpConsignmentCanceledBody.vm
 ;jsl_Email_NotPickedUpConsignment_Canceled_Subject;"Not Picked Up Consignment Canceled Email Subject";$emailResource/email-notPickedUpConsignmentCanceledSubject.vm
 ;jsl_Email_Order_Cancelled_Body;"Order Cancelled Email Body";$emailResource/email-orderCancelledBody.vm
 ;jsl_Email_Order_Cancelled_Subject;"Order Cancelled Email Subject";$emailResource/email-orderCancelledSubject.vm
 ;jsl_Email_Order_Collection_Reminder_Body;"Order Collection Reminder Body";$emailResource/email-orderCollectionReminderBody.vm
 ;jsl_Email_Order_Collection_Reminder_Subject;"Order Collection Reminder Subject";$emailResource/email-orderCollectionReminderSubject.vm
 ;jsl_Email_Order_Confirmation_Body;"Order Confirmation Email Body";$emailResource/email-orderConfirmationBody.vm
 ;jsl_Email_Order_Confirmation_Subject;"Order Confirmation Email Subject";$emailResource/email-orderConfirmationSubject.vm
 ;jsl_Email_Order_Move_To_CS_Body;"Order Move To CS Body";$emailResource/email-orderMoveToCsBody.vm
 ;jsl_Email_Order_Move_To_CS_Subject;"Order Move To CS Subject";$emailResource/email-orderMoveToCsSubject.vm
 ;jsl_Email_Order_Partially_Canceled_Body;"Order Partially Canceled Email Body";$emailResource/email-orderPartiallyCanceledBody.vm
 ;jsl_Email_Order_Partially_Canceled_Subject;"Order Partially Canceled Email Subject";$emailResource/email-orderPartiallyCanceledSubject.vm
 ;jsl_Email_Order_Partially_Refunded_Body;"Order Partially Refunded Email Body";$emailResource/email-orderPartiallyRefundedBody.vm
 ;jsl_Email_Order_Partially_Refunded_Subject;"Order Partially Refunded Email Subject";$emailResource/email-orderPartiallyRefundedSubject.vm
 ;jsl_Email_Order_Refund_Body;"Order Refund Email Body";$emailResource/email-orderRefundBody.vm
 ;jsl_Email_Order_Refund_Subject;"Order Refund Email Subject";$emailResource/email-orderRefundSubject.vm
 ;jsl_Email_Quote_Buyer_Cancellation_Body;"Quote Buyer Cancellation Email Body";$emailResource/email-quoteBuyerCancellationBody.vm
 ;jsl_Email_Quote_Buyer_Cancellation_Subject;"Quote Buyer Cancellation Email Subject";$emailResource/email-quoteBuyerCancellationSubject.vm
 ;jsl_Email_Quote_Buyer_Submission_Body;"Quote Buyer Submission Email Body";$emailResource/email-quoteBuyerSubmissionBody.vm
 ;jsl_Email_Quote_Buyer_Submission_Subject;"Quote Buyer Submission Email Subject";$emailResource/email-quoteBuyerSubmissionSubject.vm
 ;jsl_Email_Quote_Expired_Body;"Quote Expired Body";$emailResource/email-quoteExpiredBody.vm
 ;jsl_Email_Quote_Expired_Subject;"Quote Expired Subject";$emailResource/email-quoteExpiredSubject.vm
 ;jsl_Email_Quote_To_Expire_Soon_Body;"Quote To Expire Soon Body";$emailResource/email-quoteToExpireSoonBody.vm
 ;jsl_Email_Quote_To_Expire_Soon_Subject;"Quote To Expire Soon Subject";$emailResource/email-quoteToExpireSoonSubject.vm
 ;jsl_Email_Ready_For_Pickup_Body;"Ready For Pickup Email Body";$emailResource/email-readyForPickupBody.vm
 ;jsl_Email_Ready_For_Pickup_Subject;"Ready For Pickup Email Subject";$emailResource/email-readyForPickupSubject.vm

# CMS components and Email velocity templates
UPDATE RendererTemplate;code[unique=true];description[lang=$lang];templateScript[lang=$lang,translator=de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
 ;jsl_Email_Order_ApprovalRejection_Body;"Buyer Approval Rejection Email Body";$jarEmailResource/email-orderApprovalRejectionBody.vm
 ;jsl_Email_Order_ApprovalRejection_Subject;"Order Approval Rejection Email Subject";$jarEmailResource/email-orderApprovalRejectionSubject.vm
 ;jsl_Email_Order_PendingApproval_Body;"Order Pending Approval Email Body";$jarEmailResource/email-orderPendingApprovalBody.vm
 ;jsl_Email_Order_PendingApproval_Subject;"Order Pending Approval Email Subject";$jarEmailResource/email-orderPendingApprovalSubject.vm
 ;jsl_Email_Replenishment_Order_Confirmation_Body;"";$jarEmailResource/email-replenishmentOrderConfirmationBody.vm
 ;jsl_Email_Replenishment_Order_Confirmation_Subject;"Order Confirmation Email Subject";$jarEmailResource/email-replenishmentOrderConfirmationSubject.vm
 ;jsl_Email_Replenishment_Order_Placed_Body;"Order Confirmation Email Subject";$jarEmailResource/email-replenishmentOrderPlacedBody.vm
 ;jsl_Email_Replenishment_Order_Placed_Subject;"Order Confirmation Email Subject";$jarEmailResource/email-replenishmentOrderPlacedSubject.vm

# Email Pages
UPDATE EmailPage;$contentCV[unique=true];uid[unique=true];fromEmail[lang=$lang];fromName[lang=$lang]
 ;;CustomerRegistrationEmail;"customerservices@hybris.com";"Customer Services Team"
 ;;DeliverySentEmail;"customerservices@hybris.com";"Customer Services Team"
 ;;ForgottenPasswordEmail;"customerservices@hybris.com";"Customer Services Team"
 ;;OrderApprovalRejectionEmail;"customerservices@hybris.com";"Customer Services Team"
 ;;OrderCancelledEmail;"customerservices@hybris.com";"Customer Services Team"
 ;;OrderCollectionReminderEmail;"customerservices@hybris.com";"Customer Services Team"
 ;;OrderConfirmationEmail;"customerservices@hybris.com";"Customer Services Team"
 ;;OrderMoveToCsEmail;"customerservices@hybris.com";"Customer Services Team"
 ;;OrderPartiallyCanceledEmail;"customerservices@hybris.com";"Customer Services Team"
 ;;OrderPartiallyRefundedEmail;"customerservices@hybris.com";"Customer Services Team"
 ;;OrderPendingApprovalEmail;"customerservices@hybris.com";"Customer Services Team"
 ;;OrderRefundEmail;"customerservices@hybris.com";"Customer Services Team"
 ;;QuoteBuyerCancellationEmail;"customerservices@hybris.com";"Customer Services Team"
 ;;QuoteBuyerSubmissionEmail;"customerservices@hybris.com";"Customer Services Team"
 ;;QuoteExpiredEmail;"customerservices@hybris.com";"Customer Services Team"
 ;;QuoteToExpireSoonEmail;"customerservices@hybris.com";"Customer Services Team"
 ;;ReadyForPickupEmail;"customerservices@hybris.com";"Customer Services Team"
 ;;ReplenishmentOrderConfirmationEmail;"customerservices@hybris.com";"Customer Services Team"
 ;;ReplenishmentOrderPlacedEmail;"customerservices@hybris.com";"Customer Services Team"
 
 
 ###OTP IMPEX
 # CMS components and Email velocity templates
UPDATE RendererTemplate;code[unique=true];description[lang=$lang];templateScript[lang=$lang,translator=de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
 ;jsl_send_Otp_Email_Body;”send otp Email Body";$emailResource/email-sendOtpEmailBody.vm
 ;jsl_send_Otp_Email_Subject;”send top Email Subject";$emailResource/email-sendOtpEmailSubject.vm
