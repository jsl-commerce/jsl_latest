/**
 *
 */
package com.jsl.render;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.ObjectUtils;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listcell;

import com.hybris.cockpitng.core.config.impl.jaxb.listview.ListColumn;
import com.hybris.cockpitng.dataaccess.facades.type.DataType;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.util.UITools;
import com.hybris.cockpitng.widgets.collectionbrowser.mold.impl.common.AbstractMoldStrategy;
import com.hybris.cockpitng.widgets.collectionbrowser.mold.impl.listview.renderer.DefaultListCellRenderer;
import com.hybris.cockpitng.widgets.util.QualifierLabel;


/**
 * @author himanshu.sial
 *
 */
public class DateCellRender extends DefaultListCellRenderer
{
	private static final String CSS_READ_RESTRICTED = "yw-listview-cell-restricted";

	private static final String CSS_CELL_LABEL = "yw-listview-cell-label";

	@Override
	public void render(final Listcell parent, final ListColumn columnConfiguration, final Object object, final DataType dataType,
			final WidgetInstanceManager widgetInstanceManger)
	{

		String labelText;

		if (columnConfiguration.isAutoExtract())
		{
			labelText = ObjectUtils.toString(object);
		}
		else
		{
			final String qualifier = columnConfiguration.getQualifier();
			final QualifierLabel label = getWidgetRenderingUtils().getAttributeLabel(object, dataType, qualifier);
			labelText = label.getLabel();

			if (!label.isSuccessful())
			{
				UITools.modifySClass(parent, CSS_READ_RESTRICTED, true);
			}
		}
		labelText = UITools.truncateText(labelText, widgetInstanceManger.getWidgetSettings().getInt("maxCharsInCell"));

		//Change label text here and set it it will reflect at there.
		final Label label = new Label(formatDate(labelText));
		UITools.modifySClass(label, CSS_CELL_LABEL, true);
		label.setAttribute(AbstractMoldStrategy.ATTRIBUTE_HYPERLINK_CANDIDATE, Boolean.TRUE);
		parent.appendChild(label);
		fireComponentRendered(label, parent, columnConfiguration, object);

		fireComponentRendered(parent, columnConfiguration, object);

	}


	private String formatDate(final String labelDate)
	{

		final Date date = new Date(labelDate);
		final DateFormat df = new SimpleDateFormat("MMM dd, yyyy HH:mm:ss");
		try
		{
			return df.format(date);
		}
		catch (final Exception e)
		{
			return labelDate;
		}
	}
}
