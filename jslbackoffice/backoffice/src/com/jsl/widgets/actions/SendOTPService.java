/**
 *
 */
package com.jsl.widgets.actions;

import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.zkoss.zul.Messagebox;

import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;
import com.jsl.configurablebundleservice.service.JslBundleTemplateService;
import com.jsl.core.dto.JslOTPGenerationDto;
import com.jsl.core.enums.AuctionStatus;
import com.jsl.core.enums.OTPGeneratedForEnum;
import com.jsl.core.enums.OTPUserIdentificationType;
import com.jsl.core.model.AuctionEventModel;
import com.jsl.core.services.JslAuctionUserGroupService;
import com.jsl.core.services.JslUserOTPService;


/**
 * @author sunil.maurya.kumar
 *
 */
public class SendOTPService implements CockpitAction<AuctionEventModel, Object>
{
	private static final Logger LOG = Logger.getLogger(SendOTPService.class);

	@Resource(name = "jslUserOTPService")
	private JslUserOTPService jslUserOTPService;
	@Resource(name = "jslAuctionUserGroupService")
	private JslAuctionUserGroupService jslAuctionUserGroupService;
	@Resource(name = "modelService")
	private ModelService modelService;
	@Resource(name = "jslBundleTemplateService")
	private JslBundleTemplateService jslBundleTemplateService;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.hybris.cockpitng.actions.CockpitAction#perform(com.hybris.cockpitng.actions.ActionContext)
	 */
	@Override
	public ActionResult perform(final ActionContext<AuctionEventModel> actionContext)
	{

		LOG.info("SendOTPService Entered sendOtpService----->line 58");
		ActionResult result = null;
		final AuctionEventModel auctionEventModel = actionContext.getData();
		if (!AuctionStatus.READYFOROTP.equals(auctionEventModel.getAuctionStatus())
				&& !AuctionStatus.STARTED.equals(auctionEventModel.getAuctionStatus()))
		{

			Messagebox.show("Auction Should be in either ReadyForOtp or Started Status!");
			result = new ActionResult("Error"); //Auction is not in sendOTP status
			return result;

		}
		boolean isBundlesPriceSet = true;
		LOG.info("SendOTPService Entered sendOtpService----->line 72");
		final List<BundleTemplateModel> bundles = getJslBundleTemplateService()
				.getBundleTemplatesForAuctionId(String.valueOf(auctionEventModel.getAuctionId()));
		for (final BundleTemplateModel bundle : bundles)
		{
			if (bundle.getBaseBidPrice() == null || bundle.getMaxLotPrice() == null)
			{
				isBundlesPriceSet = false;
				LOG.info("basebidpricenotavailable for :" + bundle.getPk());
				break;
			}
		}
		LOG.info("SendOTPService Entered sendOtpService----->line 83");
		if (!isBundlesPriceSet)
		{
			LOG.info("SendOTPService Entered sendOtpService----->PRICENOTAVAILABLE");
			Messagebox.show("Bundles price has not been set yet!");
			result = new ActionResult("Error"); //Auction is not in sendOTP status
			return result;
		}

		LOG.info(".............sendotp123....................");
		final Collection<CustomerModel> customers = getJslAuctionUserGroupService()
				.getCustomers(auctionEventModel.getAuctionGroup().getUid());
		int otpSentCount = 0;
		for (final CustomerModel customer : customers)
		{
			LOG.info("Insideforloopcustomer----98");
			final JslOTPGenerationDto jslOTPGenerationDto = new JslOTPGenerationDto();
			jslOTPGenerationDto.setEmailAddress(customer.getContactEmail());
			jslOTPGenerationDto.setDisplayName(customer.getDisplayName());
			jslOTPGenerationDto.setFromDisplayName("JSL");
			jslOTPGenerationDto.setAuctionId(auctionEventModel.getAuctionId().toString());
			if (getJslUserOTPService().generateOTP(OTPGeneratedForEnum.AUCTION, OTPUserIdentificationType.EMAIL_ID,
					customer.getContactEmail(), jslOTPGenerationDto))
			{
				otpSentCount++;
			}
		}
		LOG.info("otpSentCount---->" + otpSentCount);
		Messagebox.show(MessageFormat.format("OTP Sent Successfully to {0} customers!", otpSentCount));
		result = new ActionResult("SUCCESS");

		// to mark auction OTP are sent
		auctionEventModel.setAuctionStatus(AuctionStatus.STARTED);
		auctionEventModel.setActive(false); // This is for visiblity of bundles in cockpit
		modelService.save(auctionEventModel);

		return result;
	}


	/**
	 * @return the jslUserOTPService
	 */
	public JslUserOTPService getJslUserOTPService()
	{
		return jslUserOTPService;
	}


	/**
	 * @param jslUserOTPService
	 *           the jslUserOTPService to set
	 */
	public void setJslUserOTPService(final JslUserOTPService jslUserOTPService)
	{
		this.jslUserOTPService = jslUserOTPService;
	}


	/**
	 * @return the jslAuctionUserGroupService
	 */
	public JslAuctionUserGroupService getJslAuctionUserGroupService()
	{
		return jslAuctionUserGroupService;
	}


	/**
	 * @param jslAuctionUserGroupService
	 *           the jslAuctionUserGroupService to set
	 */
	public void setJslAuctionUserGroupService(final JslAuctionUserGroupService jslAuctionUserGroupService)
	{
		this.jslAuctionUserGroupService = jslAuctionUserGroupService;
	}


	/**
	 * @return the jslBundleTemplateService
	 */
	public JslBundleTemplateService getJslBundleTemplateService()
	{
		return jslBundleTemplateService;
	}


	/**
	 * @param jslBundleTemplateService
	 *           the jslBundleTemplateService to set
	 */
	public void setJslBundleTemplateService(final JslBundleTemplateService jslBundleTemplateService)
	{
		this.jslBundleTemplateService = jslBundleTemplateService;
	}







}
