/**
 *
 */
package com.jsl.widgets.actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Messagebox;

import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;
import com.jsl.core.enums.AuctionStatus;
import com.jsl.core.exceptions.JslExportOrderException;
import com.jsl.core.model.AuctionEventModel;
import com.jsl.facades.csv.JslOrderExportFacade;


/**
 *
 */

/**
 * @author manav.magoo
 *
 */
public class ExportOrderService implements CockpitAction<AuctionEventModel, Object>
{
	private static final Logger LOG = Logger.getLogger(ExportOrderService.class);

	@Resource(name = "jslOrderExportFacade")
	JslOrderExportFacade jslOrderExportFacade;

	@Override
	public ActionResult perform(final ActionContext<AuctionEventModel> actionContext)
	{
		ActionResult result = null;
		final AuctionEventModel auctionEventModel = actionContext.getData();
		if (!AuctionStatus.COMPLETED.equals(auctionEventModel.getAuctionStatus()))
		{
			Messagebox.show("Orders for this auction has not been created yet!");
			result = new ActionResult("Error");
			return result;
		}
		FileInputStream fileInputStream = null;
		try
		{
			final URL resource = ExportOrderService.class.getResource("/testWorkBook.xls");
			final File file = Paths.get(resource.toURI()).toFile();
			fileInputStream = new FileInputStream(file);

			if (jslOrderExportFacade.exportAuctionOrders(auctionEventModel.getAuctionId(), file))
			{
				final byte[] data = new byte[(int) file.length()];
				fileInputStream.read(data);
				Filedownload.save(data, "text/comma-separated-values;charset=UTF-8", "export.csv");
				Messagebox.show("Orders has been exported successfully!");
				fileInputStream.close();
				result = new ActionResult("Success");
				return result;
			}
		}
		catch (final JslExportOrderException | IOException | URISyntaxException e)
		{
			LOG.debug("Exception :" + e.getMessage());
			Messagebox.show(e.getMessage());
			result = new ActionResult("Error");
			return result;
		}
		finally
		{
			try
			{
				if (fileInputStream != null)
				{
					fileInputStream.close();
				}
			}
			catch (final IOException e)
			{
				// handle an exception, or often we just ignore it
			}
		}
		return result;
	}
}
