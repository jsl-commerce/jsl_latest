/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.constants;

/**
 *
 */
public class JslSapOrderCsvColumms
{

	@SuppressWarnings("javadoc")
	public static final String product = "product";

	// order entry
	@SuppressWarnings("javadoc")
	public static final String zLocation = "zLocation";

	@SuppressWarnings("javadoc")
	public static final String zProductType = "zProductType";

	@SuppressWarnings("javadoc")
	public static final String zQuality = "zQuality";

	@SuppressWarnings("javadoc")
	public static final String zStandard = "zStandard";

	@SuppressWarnings("javadoc")
	public static final String zSeries = "zSeries";

	@SuppressWarnings("javadoc")
	public static final String zGrade = "zGrade";

	@SuppressWarnings("javadoc")
	public static final String zGradeGroup = "zGradeGroup";

	@SuppressWarnings("javadoc")
	public static final String zThickness = "zThickness";

	@SuppressWarnings("javadoc")
	public static final String zWidth = "zWidth";

	@SuppressWarnings("javadoc")
	public static final String zFinish = "zFinish";

	@SuppressWarnings("javadoc")
	public static final String zLength = "zLength";

	@SuppressWarnings("javadoc")
	public static final String zMinPartWt = "zMinPartWt";

	@SuppressWarnings("javadoc")
	public static final String zMaterialNum = "zMaterialNum";

	@SuppressWarnings("javadoc")
	public static final String zPvc = "zPvc";

	@SuppressWarnings("javadoc")
	public static final String zMaxPartWt = "zMaxPartWt";

	@SuppressWarnings("javadoc")
	public static final String zEdgeCondition = "zEdgeCondition";

	@SuppressWarnings("javadoc")
	public static final String zUsageIndicator = "zUsageIndicator";

	@SuppressWarnings("javadoc")
	public static final String zCoilId = "zCoilId";

	@SuppressWarnings("javadoc")
	public static final String zIlp = "zIlp";

	@SuppressWarnings("javadoc")
	public static final String zWeight = "zWeight";

	@SuppressWarnings("javadoc")
	public static final String zThick_Tol_U = "zThick_Tol_U";

	@SuppressWarnings("javadoc")
	public static final String zWidth_Tol_U = "zWidth_Tol_U";

	@SuppressWarnings("javadoc")
	public static final String zThick_Tol_L = "zThick_Tol_L";

	@SuppressWarnings("javadoc")
	public static final String zTpi = "zTpi";

	@SuppressWarnings("javadoc")
	public static final String zLength_Tol_L = "zLength_Tol_L";

	@SuppressWarnings("javadoc")
	public static final String zLength_Tol_U = "zLength_Tol_U";

	@SuppressWarnings("javadoc")
	public static final String zNet_Wt = "zNet_Wt";

	@SuppressWarnings("javadoc")
	public static final String zWidth_Tol_L = "zWidth_Tol_L";

	@SuppressWarnings("javadoc")
	public static final String zGross_Wt = "zGross_Wt";

	@SuppressWarnings("javadoc")
	public static final String zMarkingOnSticker = "zMarkingOnSticker";

	@SuppressWarnings("javadoc")
	public static final String zUtensilUsage = "zUtensilUsage";

	@SuppressWarnings("javadoc")
	public static final String zDesiredDate = "zDesiredDate";

	@SuppressWarnings("javadoc")
	public static final String zRoute = "zRoute";

	@SuppressWarnings("javadoc")
	public static final String zSO_Rem = "zSO_Rem";

	@SuppressWarnings("javadoc")
	public static final String zSpcl_Test1 = "zSpcl_Test1";

	@SuppressWarnings("javadoc")
	public static final String zNoofsheets = "zNoofsheets";

	@SuppressWarnings("javadoc")
	public static final String zInsp3rdparty = "zInsp3rdparty";

	@SuppressWarnings("javadoc")
	public static final String zMarkingOnProd = "zMarkingOnProd";

	@SuppressWarnings("javadoc")
	public static final String zSpcl_Test2 = "zSpcl_Test2";

	@SuppressWarnings("javadoc")
	public static final String zSpcl_Test3 = "zSpcl_Test3";

	@SuppressWarnings("javadoc")
	public static final String zMatgrp = "zMatgrp";


}
