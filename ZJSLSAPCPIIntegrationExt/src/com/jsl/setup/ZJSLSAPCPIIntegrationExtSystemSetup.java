/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.jsl.setup;

import static com.jsl.constants.ZJSLSAPCPIIntegrationExtConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import com.jsl.constants.ZJSLSAPCPIIntegrationExtConstants;
import com.jsl.service.ZJSLSAPCPIIntegrationExtService;


@SystemSetup(extension = ZJSLSAPCPIIntegrationExtConstants.EXTENSIONNAME)
public class ZJSLSAPCPIIntegrationExtSystemSetup
{
	private final ZJSLSAPCPIIntegrationExtService ZJSLSAPCPIIntegrationExtService;

	public ZJSLSAPCPIIntegrationExtSystemSetup(final ZJSLSAPCPIIntegrationExtService ZJSLSAPCPIIntegrationExtService)
	{
		this.ZJSLSAPCPIIntegrationExtService = ZJSLSAPCPIIntegrationExtService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		ZJSLSAPCPIIntegrationExtService.createLogo(PLATFORM_LOGO_CODE);
	}

	private InputStream getImageStream()
	{
		return ZJSLSAPCPIIntegrationExtSystemSetup.class.getResourceAsStream("/ZJSLSAPCPIIntegrationExt/sap-hybris-platform.png");
	}
}
