/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.service;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.sap.orderexchange.constants.OrderCsvColumns;
import de.hybris.platform.sap.orderexchange.constants.OrderEntryCsvColumns;
import de.hybris.platform.sap.orderexchange.outbound.RawItemContributor;
import de.hybris.platform.sap.sapcpiadapter.data.SapCpiOrder;
import de.hybris.platform.sap.sapcpiadapter.data.SapCpiOrderItem;
import de.hybris.platform.sap.sapcpiorderexchange.service.impl.SapCpiOmmOrderConversionService;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.RecursiveToStringStyle;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsl.constants.JslSapOrderCsvColumms;


/**
 *
 */
public class JslSapCpiOmmOrderConversionService extends SapCpiOmmOrderConversionService
{
	private RawItemContributor<OrderModel> sapOrderContributor;
	private RawItemContributor<OrderModel> sapOrderEntryContributor;
	private static final Logger LOG = LoggerFactory.getLogger(JslSapCpiOmmOrderConversionService.class);
	private ProductService productService;

	@Override
	public SapCpiOrder convertOrderToSapCpiOrder(final OrderModel orderModel)
	{
		final SapCpiOrder sapCpiOrder = new SapCpiOrder();

		sapOrderContributor.createRows(orderModel).stream().findFirst().ifPresent(row -> {

			sapCpiOrder.setSapCpiConfig(mapOrderConfigInfo(orderModel));

			sapCpiOrder.setOrderId(mapAttribute(OrderCsvColumns.ORDER_ID, row));
			sapCpiOrder.setBaseStoreUid(mapAttribute(OrderCsvColumns.BASE_STORE, row));
			sapCpiOrder.setCreationDate(mapDateAttribute(OrderCsvColumns.DATE, row));
			sapCpiOrder.setCurrencyIsoCode(mapAttribute(OrderCsvColumns.ORDER_CURRENCY_ISO_CODE, row));
			sapCpiOrder.setPaymentMode(mapAttribute(OrderCsvColumns.PAYMENT_MODE, row));
			sapCpiOrder.setDeliveryMode(mapAttribute(OrderCsvColumns.DELIVERY_MODE, row));
			sapCpiOrder.setChannel(mapAttribute(OrderCsvColumns.CHANNEL, row));
			sapCpiOrder.setPurchaseOrderNumber(mapAttribute(OrderCsvColumns.PURCHASE_ORDER_NUMBER, row));

			sapCpiOrder.setTransactionType(orderModel.getStore().getSAPConfiguration().getSapcommon_transactionType());
			sapCpiOrder.setSalesOrganization(orderModel.getStore().getSAPConfiguration().getSapcommon_salesOrganization());
			sapCpiOrder.setDistributionChannel(orderModel.getStore().getSAPConfiguration().getSapcommon_distributionChannel());
			sapCpiOrder.setDivision(orderModel.getStore().getSAPConfiguration().getSapcommon_division());

			orderModel.getStore().getSAPConfiguration().getSapDeliveryModes().stream()
					.filter(entry -> entry.getDeliveryMode().getCode().contentEquals(orderModel.getDeliveryMode().getCode()))
					.findFirst().ifPresent(entry -> sapCpiOrder.setShippingCondition(entry.getDeliveryValue()));

			sapCpiOrder.setSapCpiOrderItems(mapOrderItems(orderModel));
			sapCpiOrder.setSapCpiPartnerRoles(mapOrderPartners(orderModel));
			sapCpiOrder.setSapCpiOrderAddresses(mapOrderAddresses(orderModel));
			sapCpiOrder.setSapCpiOrderPriceComponents(mapOrderPrices(orderModel));
			sapCpiOrder.setSapCpiCreditCardPayments(mapCreditCards(orderModel));

		});

		if (LOG.isDebugEnabled())
		{
			LOG.debug(String.format("SCPI OMM order object: %n %s",
					ReflectionToStringBuilder.toString(sapCpiOrder, new RecursiveToStringStyle()).toString()));
		}

		return sapCpiOrder;
	}


	@Override
	protected List<SapCpiOrderItem> mapOrderItems(final OrderModel orderModel)
	{


		final List<SapCpiOrderItem> sapCpiOrderItems = new ArrayList<>();

		sapOrderEntryContributor.createRows(orderModel).forEach(row -> {

			final SapCpiOrderItem sapCpiOrderItem = new SapCpiOrderItem();

			sapCpiOrderItem.setOrderId(mapAttribute(OrderCsvColumns.ORDER_ID, row));
			sapCpiOrderItem.setEntryNumber(mapAttribute(OrderEntryCsvColumns.ENTRY_NUMBER, row));
			sapCpiOrderItem.setQuantity(mapAttribute(OrderEntryCsvColumns.QUANTITY, row));
			sapCpiOrderItem.setProductCode(mapAttribute(OrderEntryCsvColumns.PRODUCT_CODE, row));
			sapCpiOrderItem.setUnit(mapAttribute(OrderEntryCsvColumns.ENTRY_UNIT_CODE, row));
			sapCpiOrderItem.setProductName(mapAttribute(OrderEntryCsvColumns.PRODUCT_NAME, row));



			final ProductModel product = (ProductModel) row.get(JslSapOrderCsvColumms.product);

			sapCpiOrderItem.setZLocation(product.getLocation());
			sapCpiOrderItem.setZProductType(product.getProductType());
			sapCpiOrderItem.setZStandard(product.getStandard());
			sapCpiOrderItem.setZSeries(product.getSeries());
			sapCpiOrderItem.setZGrade(product.getGrade());
			sapCpiOrderItem.setZGradeGroup(product.getGradeGroup());
			sapCpiOrderItem.setZThickness(product.getThickness());
			sapCpiOrderItem.setZWidth(product.getWidth());
			sapCpiOrderItem.setZFinish(product.getFinish());
			sapCpiOrderItem.setZLength(product.getLength());
			sapCpiOrderItem.setZMinPartWt(product.getMinPartWt());
			sapCpiOrderItem.setZMaterialNum(product.getMaterialNum());
			sapCpiOrderItem.setZPvc(product.getPvc());
			sapCpiOrderItem.setZMaxPartWt(product.getMaxPartWt());
			sapCpiOrderItem.setZEdgeCondition(product.getEdgeCondition());
			sapCpiOrderItem.setZUsageIndicator(product.getUsageIndicator());
			sapCpiOrderItem.setZCoilId(product.getCoilId());
			sapCpiOrderItem.setZIlp(product.getIlp());
			sapCpiOrderItem.setZTpi(product.getTpi());
			sapCpiOrderItem.setZUtensilUsage("N");
			//sapCpiOrderItem.setZDesiredDate();
			sapCpiOrderItem.setZRoute("Z00001");
			sapCpiOrderItem.setZInsp3rdparty("No");

			/*
			 * *
			 *
			 *
			 * sapCpiOrderItem.setZProductType(mapAttribute(JslSapOrderCsvColumms.zProductType, row));
			 * sapCpiOrderItem.setZQuality(mapAttribute(JslSapOrderCsvColumms.zQuality, row));
			 * sapCpiOrderItem.setZStandard(mapAttribute(JslSapOrderCsvColumms.zStandard, row));
			 * sapCpiOrderItem.setZSeries(mapAttribute(JslSapOrderCsvColumms.zSeries, row));
			 * sapCpiOrderItem.setZGrade(mapAttribute(JslSapOrderCsvColumms.zGrade, row));
			 * sapCpiOrderItem.setZGradeGroup(mapAttribute(JslSapOrderCsvColumms.zGradeGroup, row));
			 * sapCpiOrderItem.setZThickness(mapAttribute(JslSapOrderCsvColumms.zThickness, row));
			 * sapCpiOrderItem.setZWidth(mapAttribute(JslSapOrderCsvColumms.zWidth, row));
			 * sapCpiOrderItem.setZFinish(mapAttribute(JslSapOrderCsvColumms.zFinish, row));
			 * sapCpiOrderItem.setZLength(mapAttribute(JslSapOrderCsvColumms.zLength, row));
			 * sapCpiOrderItem.setZMinPartWt(mapAttribute(JslSapOrderCsvColumms.zMinPartWt, row));
			 * sapCpiOrderItem.setZMaterialNum(mapAttribute(JslSapOrderCsvColumms.zMaterialNum, row));
			 * sapCpiOrderItem.setZPvc(mapAttribute(JslSapOrderCsvColumms.zPvc, row));
			 * sapCpiOrderItem.setZMaxPartWt(mapAttribute(JslSapOrderCsvColumms.zMaxPartWt, row));
			 * sapCpiOrderItem.setZEdgeCondition(mapAttribute(JslSapOrderCsvColumms.zEdgeCondition, row));
			 * sapCpiOrderItem.setZUsageIndicator(mapAttribute(JslSapOrderCsvColumms.zUsageIndicator, row));
			 * sapCpiOrderItem.setZCoilId(mapAttribute(JslSapOrderCsvColumms.zCoilId, row));
			 * sapCpiOrderItem.setZIlp(mapAttribute(JslSapOrderCsvColumms.zIlp, row));
			 * sapCpiOrderItem.setZWeight(mapAttribute(JslSapOrderCsvColumms.zWeight, row));
			 * sapCpiOrderItem.setZThick_Tol_U(mapAttribute(JslSapOrderCsvColumms.zThick_Tol_U, row));
			 * sapCpiOrderItem.setZWidth_Tol_U(mapAttribute(JslSapOrderCsvColumms.zWidth_Tol_U, row));
			 * sapCpiOrderItem.setZThick_Tol_L(mapAttribute(JslSapOrderCsvColumms.zThick_Tol_L, row));
			 * sapCpiOrderItem.setZTpi(mapAttribute(JslSapOrderCsvColumms.zTpi, row));
			 * sapCpiOrderItem.setZLength_Tol_L(mapAttribute(JslSapOrderCsvColumms.zLength_Tol_L, row));
			 * sapCpiOrderItem.setZLength_Tol_U(mapAttribute(JslSapOrderCsvColumms.zLength_Tol_U, row));
			 * sapCpiOrderItem.setZNet_Wt(mapAttribute(JslSapOrderCsvColumms.zNet_Wt, row));
			 * sapCpiOrderItem.setZWidth_Tol_L(mapAttribute(JslSapOrderCsvColumms.zWidth_Tol_L, row));
			 * sapCpiOrderItem.setZGross_Wt(mapAttribute(JslSapOrderCsvColumms.zGross_Wt, row));
			 * sapCpiOrderItem.setZMarkingOnSticker(mapAttribute(JslSapOrderCsvColumms.zMarkingOnSticker, row));
			 * sapCpiOrderItem.setZUtensilUsage(mapAttribute(JslSapOrderCsvColumms.zUtensilUsage, row));
			 * sapCpiOrderItem.setZDesiredDate(mapAttribute(JslSapOrderCsvColumms.zDesiredDate, row));
			 * sapCpiOrderItem.setZRoute(mapAttribute(JslSapOrderCsvColumms.zRoute, row));
			 * sapCpiOrderItem.setZSO_Rem(mapAttribute(JslSapOrderCsvColumms.zSO_Rem, row));
			 * sapCpiOrderItem.setZSpcl_Test1(mapAttribute(JslSapOrderCsvColumms.zSpcl_Test1, row));
			 * sapCpiOrderItem.setZNoofsheets(mapAttribute(JslSapOrderCsvColumms.zNoofsheets, row));
			 * sapCpiOrderItem.setZInsp3rdparty(mapAttribute(JslSapOrderCsvColumms.zInsp3rdparty, row));
			 * sapCpiOrderItem.setZMarkingOnProd(mapAttribute(JslSapOrderCsvColumms.zMarkingOnProd, row));
			 * sapCpiOrderItem.setZSpcl_Test2(mapAttribute(JslSapOrderCsvColumms.zSpcl_Test2, row));
			 * sapCpiOrderItem.setZSpcl_Test3(mapAttribute(JslSapOrderCsvColumms.zSpcl_Test3, row));
			 * sapCpiOrderItem.setZMatgrp(mapAttribute(JslSapOrderCsvColumms.zMatgrp, row));
			 *
			 *
			 *
			 */



			sapCpiOrderItems.add(sapCpiOrderItem);

		});

		return sapCpiOrderItems;

	}


	/**
	 * @return the sapOrderContributor
	 */
	@Override
	public RawItemContributor<OrderModel> getSapOrderContributor()
	{
		return sapOrderContributor;
	}


	/**
	 * @param sapOrderContributor
	 *           the sapOrderContributor to set
	 */
	@Override
	public void setSapOrderContributor(final RawItemContributor<OrderModel> sapOrderContributor)
	{
		this.sapOrderContributor = sapOrderContributor;
	}


	/**
	 * @return the sapOrderEntryContributor
	 */
	@Override
	public RawItemContributor<OrderModel> getSapOrderEntryContributor()
	{
		return sapOrderEntryContributor;
	}


	/**
	 * @param sapOrderEntryContributor
	 *           the sapOrderEntryContributor to set
	 */
	@Override
	public void setSapOrderEntryContributor(final RawItemContributor<OrderModel> sapOrderEntryContributor)
	{
		this.sapOrderEntryContributor = sapOrderEntryContributor;
	}


	/**
	 * @return the productService
	 */
	public ProductService getProductService()
	{
		return productService;
	}


	/**
	 * @param productService
	 *           the productService to set
	 */
	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}
}
