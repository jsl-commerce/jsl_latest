/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.service;

import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.sap.orderexchange.constants.OrderCsvColumns;
import de.hybris.platform.sap.orderexchange.constants.OrderEntryCsvColumns;
import de.hybris.platform.sap.orderexchange.outbound.impl.DefaultOrderEntryContributor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.jsl.constants.JslSapOrderCsvColumms;


/**
 *
 */
public class JslSapOrderEntryContributor extends DefaultOrderEntryContributor
{
	private final static Logger LOG = Logger.getLogger(JslSapOrderEntryContributor.class);

	@Override
	public List<Map<String, Object>> createRows(final OrderModel order)
	{
		final List<AbstractOrderEntryModel> entries = order.getEntries();
		final List<Map<String, Object>> result = new ArrayList<>();

		for (final AbstractOrderEntryModel entry : entries)
		{
			final Map<String, Object> row = new HashMap<>();
			row.put(OrderCsvColumns.ORDER_ID, order.getCode());
			row.put(OrderEntryCsvColumns.ENTRY_NUMBER, entry.getEntryNumber());
			row.put(OrderEntryCsvColumns.QUANTITY, entry.getQuantity());
			row.put(OrderEntryCsvColumns.PRODUCT_CODE, entry.getProduct().getCode());
			final UnitModel unit = entry.getUnit();
			if (unit != null)
			{
				row.put(OrderEntryCsvColumns.ENTRY_UNIT_CODE, unit.getCode());
			}
			else
			{
				LOG.warn("Could not determine unit code for product " + entry.getProduct().getCode() + "as entry "
						+ entry.getEntryNumber() + "of order " + order.getCode());
			}
			row.put(OrderEntryCsvColumns.EXTERNAL_PRODUCT_CONFIGURATION, getProductConfigurationData(entry));
			String language = order.getLanguage().getIsocode();
			String shortText = determineItemShortText(entry, language);

			if (shortText.isEmpty())
			{
				final List<LanguageModel> fallbackLanguages = order.getLanguage().getFallbackLanguages();
				if (!fallbackLanguages.isEmpty())
				{
					language = fallbackLanguages.get(0).getIsocode();
					shortText = determineItemShortText(entry, language);
				}
			}
			row.put(OrderEntryCsvColumns.PRODUCT_NAME, shortText);

			row.put(JslSapOrderCsvColumms.product, entry.getProduct());

			getBatchIdAttributes().forEach(row::putIfAbsent);
			row.put("dh_batchId", order.getCode());

			result.add(row);
		}

		return result;
	}

}
