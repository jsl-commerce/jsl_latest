/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.service;

import de.hybris.platform.sap.sapcpiadapter.data.SapCpiOrderItem;
import de.hybris.platform.sap.sapcpiadapter.model.SAPCpiOutboundOrderItemModel;
import de.hybris.platform.sap.sapcpiorderexchange.service.impl.SapCpiOmmOrderOutboundConversionService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 *
 */
public class JslSapCpiOmmOrderOutboundConversionService extends SapCpiOmmOrderOutboundConversionService
{

	@Override
	protected Set<SAPCpiOutboundOrderItemModel> mapOrderItems(final List<SapCpiOrderItem> sapCpiOrderItems)
	{

		final List<SAPCpiOutboundOrderItemModel> sapCpiOutboundOrderItems = new ArrayList<>();

		sapCpiOrderItems.forEach(item -> {

			final SAPCpiOutboundOrderItemModel sapCpiOutboundOrderItem = new SAPCpiOutboundOrderItemModel();
			sapCpiOutboundOrderItem.setOrderId(item.getOrderId());
			sapCpiOutboundOrderItem.setEntryNumber(item.getEntryNumber());
			sapCpiOutboundOrderItem.setQuantity(item.getQuantity());
			sapCpiOutboundOrderItem.setCurrencyIsoCode(item.getCurrencyIsoCode());
			sapCpiOutboundOrderItem.setUnit(item.getUnit());
			sapCpiOutboundOrderItem.setProductCode(item.getProductCode());
			sapCpiOutboundOrderItem.setProductName(item.getProductName());
			sapCpiOutboundOrderItem.setPlant(item.getPlant());
			sapCpiOutboundOrderItem.setNamedDeliveryDate(item.getNamedDeliveryDate());
			sapCpiOutboundOrderItem.setItemCategory(item.getItemCategory());
			sapCpiOutboundOrderItem.setZLocation(item.getZLocation());

			sapCpiOutboundOrderItem.setZProductType(item.getZProductType());
			sapCpiOutboundOrderItem.setZStandard(item.getZStandard());
			sapCpiOutboundOrderItem.setZSeries(item.getZSeries());
			sapCpiOutboundOrderItem.setZGrade(item.getZGrade());
			sapCpiOutboundOrderItem.setZGradeGroup(item.getZGradeGroup());
			sapCpiOutboundOrderItem.setZThickness(item.getZThickness());
			sapCpiOutboundOrderItem.setZWidth(item.getZWidth());
			sapCpiOutboundOrderItem.setZFinish(item.getZFinish());
			sapCpiOutboundOrderItem.setZLength(item.getZLength());
			sapCpiOutboundOrderItem.setZMinPartWt(item.getZMinPartWt());
			sapCpiOutboundOrderItem.setZMaterialNum(item.getZMaterialNum());
			sapCpiOutboundOrderItem.setZPvc(item.getZPvc());
			sapCpiOutboundOrderItem.setZMaxPartWt(item.getZMaxPartWt());
			sapCpiOutboundOrderItem.setZEdgeCondition(item.getZEdgeCondition());
			sapCpiOutboundOrderItem.setZUsageIndicator(item.getZUsageIndicator());
			sapCpiOutboundOrderItem.setZCoilId(item.getZCoilId());
			sapCpiOutboundOrderItem.setZIlp(item.getZIlp());
			sapCpiOutboundOrderItem.setZTpi(item.getZTpi());
			sapCpiOutboundOrderItem.setZUtensilUsage("N");
			//sapCpiOutboundOrderItem.setZDesiredDate();
			sapCpiOutboundOrderItem.setZRoute("Z00001");
			sapCpiOutboundOrderItem.setZInsp3rdparty("No");

			sapCpiOutboundOrderItems.add(sapCpiOutboundOrderItem);

		});
		return new HashSet<>(sapCpiOutboundOrderItems);

	}
}
