/**
 *
 */
package com.jsl.bundlecockpit.components.navigationarea;

import de.hybris.platform.cockpit.components.contentbrowser.AbstractContentBrowser;
import de.hybris.platform.cockpit.session.AdvancedBrowserModel;
import de.hybris.platform.cockpit.session.UIBrowserArea;
import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.cockpit.session.impl.BaseUICockpitPerspective;
import de.hybris.platform.cockpit.util.UITools;
import de.hybris.platform.configurablebundlecockpits.productcockpit.session.impl.BundleNavigationNodeBrowserArea;
import de.hybris.platform.configurablebundlecockpits.productcockpit.session.impl.BundleNavigationNodeContentMainComponent;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.zkoss.lang.Strings;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Box;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vbox;
import org.zkoss.zul.Window;


/**
 * @author manav.magoo
 *
 */
public class JslBundleNavigationNodeContentMainComponent extends BundleNavigationNodeContentMainComponent
{

	private static final String AUCTION_CATALOG = "JslAuctionProductCatalog";
	private static final String ALERT = "Alert!";
	private static final String AUCTION_CATALOGS = "jsl.auction.catalogs.id";

	/**
	 * @param model
	 * @param contentBrowser
	 */
	public JslBundleNavigationNodeContentMainComponent(final AdvancedBrowserModel model,
			final AbstractContentBrowser contentBrowser)
	{
		super(model, contentBrowser);
		// XXX Auto-generated constructor stub
	}


	private Button createBundlesButton;
	private Button addBundleButton;
	private final String sclass = null;

	private Div contentAreaDiv = null;

	public void setContentAreaDiv(final Div contentAreaDiv)
	{
		this.contentAreaDiv = contentAreaDiv;
		if (this.contentAreaDiv != null)
		{
			this.contentAreaDiv.setVisible(false);
			this.setSclass(this.sclass);
			this.updateView();
		}
	}

	protected Div getContentAreaDiv()
	{
		return this.contentAreaDiv;
	}

	protected void updateView()
	{
		if (this.getContentAreaDiv() != null)
		{
			final Vbox vbox = new Vbox();
			vbox.setHeight("100%");
			vbox.setWidth("100%");
			vbox.setAlign("center");
			this.getContentAreaDiv().appendChild(vbox);
			this.showNotification();
		}
	}

	protected void showNotification()
	{
		if (this.getContentAreaDiv() != null)
		{
			this.getContentAreaDiv().setVisible(true);
		}
	}


	@Override
	protected Button createSearchProductButton()
	{
		final Button button = new Button(Labels.getLabel("configurablebundlecockpits.product.search"));
		button.setTooltiptext(Labels.getLabel("configurablebundlecockpits.product.search"));
		UITools.modifySClass(this, "buttonDisabled", true);
		button.setVisible(this.getModel().getSelectedNode() != null);
		button.addEventListener("onClick", (event) -> {
			this.getModel().openRelatedQueryBrowser();
		});
		button.setSclass("addRelatedItemsAddBtn");
		return button;
	}

	@Override
	protected Component createButtonContainer(final Button button)
	{
		final Div marginHelper = new Div();
		marginHelper.setSclass("marginHelper");
		final Box box = new Box();
		box.setWidth("99%");
		box.setHeight("99%");
		box.setAlign("left");
		box.setSclass("search-product-button");
		box.setPack("left");
		/* box.appendChild(button); */
		this.createBundlesButton = this.createBundlesButton();
		this.addBundleButton = this.addBundleButton();
		box.appendChild(createBundlesButton);
		box.appendChild(addBundleButton);
		marginHelper.appendChild(box);
		return marginHelper;
	}


	protected Button createBundlesButton()
	{
		final Button button = new Button("Create Bundles");
		button.setTooltiptext(Labels.getLabel("configurablebundlecockpits.bundle.add.tooltip"));
		UITools.modifySClass(this, "buttonDisabled", true);
		button.setVisible(true);
		button.addEventListener("onClick", (event) -> {
			this.getModel().createBundles();
		});
		button.setSclass("addRelatedItemsAddBtn");
		return button;
	}

	protected Button addBundleButton()
	{
		final Button button = new Button("Add Bundle");
		button.setTooltiptext("Add Bundle");
		UITools.modifySClass(this, "buttonDisabled", true);
		button.setVisible(true);

		final String auctionCatalogs = Config.getParameter(AUCTION_CATALOGS);
		final List<String> auctionCatalogList = Arrays.asList(auctionCatalogs.split(","));


		button.addEventListener("onClick", (event) -> {

			final UIBrowserArea browserArea = UISessionUtils.getCurrentSession().getCurrentPerspective().getBrowserArea();

			final BundleNavigationNodeBrowserArea bArea = (BundleNavigationNodeBrowserArea) browserArea;

			if (!auctionCatalogList.contains(bArea.getActiveCatalogVersion().getCatalog().getId()))
			{
				this.getModel().showNotification(ALERT, "Please select Auction catalog!");
			}
			else
			{
				this.contentAreaDiv = this.mainArea;
				showPopUpBox();
			}
		});
		button.setSclass("addRelatedItemsAddBtn");
		return button;
	}

	private void showPopUpBox()
	{
		final String caption = "Add Bundle!";

		final Window dialog = new Window();
		dialog.setTitle(caption != null && !caption.isEmpty() ? caption : Labels.getLabel("general.notification"));
		dialog.setHeight("200px");
		dialog.setWidth("320px");
		dialog.setClosable(true);
		final Div content = new Div();
		content.setStyle("overflow-y: auto; height: 100%;");
		dialog.appendChild(content);

		final Label auctionLabel = new Label("Auction Id : ");
		auctionLabel.setHeight("20px");
		auctionLabel.setWidth("300px");
		auctionLabel.setStyle("font-weight: bold;  margin: 4px;");
		content.appendChild(auctionLabel);

		final Textbox auctionText = new Textbox();
		auctionText.setHeight("20px");
		auctionText.setWidth("300px");
		auctionText.setStyle("margin: 4px;");
		auctionText.setFocus(true);

		content.appendChild(auctionText);

		final Label productsLabel = new Label("Batch codes to add : ");
		productsLabel.setHeight("20px");
		productsLabel.setWidth("300px");
		productsLabel.setStyle("margin: 4px;");
		productsLabel.setStyle("font-weight: bold; margin: 4px;");

		content.appendChild(productsLabel);

		final Textbox productText = new Textbox();
		productText.setHeight("20px");
		productText.setWidth("300px");
		productText.setStyle("margin: 4px;");
		productText.setFocus(true);

		content.appendChild(productText);


		final Button button = new Button("Create");
		button.setHeight("40px");
		button.setWidth("300px");
		button.setStyle("margin: 4px;");
		button.setTooltiptext("Create");
		UITools.modifySClass(this, "buttonDisabled", true);
		button.setVisible(true);

		button.addEventListener("onClick", (event) -> {

			if (Strings.isEmpty(auctionText.getText()))
			{
				this.getModel().showNotification(ALERT, "Please Enter Auction Id to Proceed!");
			}
			else if (Strings.isEmpty(productText.getText()))
			{
				this.getModel().showNotification(ALERT, "Please Enter batch codes to Proceed!");
			}
			else
			{
				final List<String> productsId = new ArrayList(Arrays.asList(productText.getText().split(",")));

				if (this.getModel().addBundle(auctionText.getText(), productsId))
				{
					dialog.setVisible(false);
					final BaseUICockpitPerspective basePerspective = (BaseUICockpitPerspective) UISessionUtils.getCurrentSession()
							.getCurrentPerspective();
					basePerspective.update();

				}
			}
		});

		button.setSclass("addRelatedItemsAddBtn");
		content.appendChild(button);

		this.getContentAreaDiv().getRoot().appendChild(dialog);
		dialog.doHighlighted();
	}


	@Override
	public JslBundleNavigationNodeBrowserModel getModel()
	{
		JslBundleNavigationNodeBrowserModel ret = null;
		if (super.getModel() instanceof JslBundleNavigationNodeBrowserModel)
		{
			ret = (JslBundleNavigationNodeBrowserModel) super.getModel();
		}

		return ret;
	}
}



