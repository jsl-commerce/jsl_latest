/**
 *
 */
package com.jsl.bundlecockpit.components.navigationarea;

import de.hybris.platform.cockpit.components.contentbrowser.AbstractContentBrowser;
import de.hybris.platform.cockpit.components.notifier.Notification;
import de.hybris.platform.cockpit.model.browser.BrowserModelFactory;
import de.hybris.platform.cockpit.model.search.Query;
import de.hybris.platform.cockpit.session.UIBrowserArea;
import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.cockpit.session.impl.BaseUICockpitPerspective;
import de.hybris.platform.configurablebundlecockpits.productcockpit.session.impl.BundleNavigationNodeBrowserArea;
import de.hybris.platform.configurablebundlecockpits.productcockpit.session.impl.BundleNavigationNodeBrowserModel;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.productcockpit.session.impl.QueryBrowserCatalogVersionFilter;
import de.hybris.platform.promotions.model.PromotionGroupModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.util.Config;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.zkoss.spring.SpringUtil;

import com.jsl.bundleengineservice.service.BundleEngineService;
import com.jsl.configurablebundleservice.service.JslBundleTemplateService;
import com.jsl.core.enums.AuctionStatus;
import com.jsl.core.model.AuctionEventModel;
import com.jsl.core.model.AuctionProductModel;
import com.jsl.core.services.JslAuctionService;
import com.jsl.core.services.JslProductService;



public class JslBundleNavigationNodeBrowserModel extends BundleNavigationNodeBrowserModel
{
	private static final String BROWSER_MODEL = "JslProductSearchBrowserModel";
	private static final String AUCTION_CATALOGS = "jsl.auction.catalogs.id";
	private BundleEngineService bundleEngineService;
	private BaseSiteService baseSiteService;
	private JslBundleTemplateService jslBundleTemplateService;
	private JslAuctionService jslAuctionService;
	private ModelService modelService;
	private JslProductService jslProductService;


	/**
	 *
	 */
	public JslBundleNavigationNodeBrowserModel()
	{
		super();
	}

	@Override
	public void openRelatedQueryBrowser()
	{
		final UIBrowserArea browserArea = UISessionUtils.getCurrentSession().getCurrentPerspective().getBrowserArea();
		if (browserArea instanceof BundleNavigationNodeBrowserArea)
		{
			final BundleNavigationNodeBrowserArea bArea = (BundleNavigationNodeBrowserArea) browserArea;
			this.focus();
			bArea.setSplittable(true);
			bArea.setSplitModeActiveDirectly(true);
			final BrowserModelFactory factory = (BrowserModelFactory) SpringUtil.getBean("BrowserModelFactory");
			final JslProductSearchBrowserModel browserModel = (JslProductSearchBrowserModel) factory
					.createBrowserModel(BROWSER_MODEL);
			browserModel.setLastQuery(new Query((Collection) null, "*", 0, 0));
			browserModel.setBrowserFilterFixed(new QueryBrowserCatalogVersionFilter(bArea.getActiveCatalogVersion()));
			bArea.addVisibleBrowser(1, browserModel);
			browserModel.updateItems();
			browserModel.focus();
		}

	}


	public boolean createBundles()
	{
		final UIBrowserArea browserArea = UISessionUtils.getCurrentSession().getCurrentPerspective().getBrowserArea();

		final BundleNavigationNodeBrowserArea bArea = (BundleNavigationNodeBrowserArea) browserArea;

		final String auctionCatalogs = Config.getParameter(AUCTION_CATALOGS);
		final List<String> auctionCatalogList = Arrays.asList(auctionCatalogs.split(","));

		if (!auctionCatalogList.contains(bArea.getActiveCatalogVersion().getCatalog().getId()))
		{
			showNotification("Alert!", "Please select Auction catalog!");
			return false;
		}

		final List<AuctionEventModel> auctions = getJslAuctionService().getAuctions(AuctionStatus.READYFORBUNDLE);
		if (auctions.isEmpty())
		{
			showNotification("Alert!", "No Auction is in ReadyForBundle status!");
			return false;
		}

		getBundleEngineService().updateAuctionPromotions(getPromotionGroups(), bArea.getActiveCatalogVersion().getCatalog());

		int totalBundleCount = 0;
		final StringBuilder s = new StringBuilder();

		for (final AuctionEventModel auction : auctions)
		{
			auction.setAuctionStatus(AuctionStatus.READYFOROTP);
			getModelService().save(auction);

			final List<BundleTemplateModel> bundles = getJslBundleTemplateService()
					.getBundleTemplatesForAuctionId(String.valueOf(auction.getAuctionId()));

			totalBundleCount += bundles.size();
			s.append(auction.getAuctionId());
			s.append(":");
			s.append(bundles.size());
			s.append(",");

		}

		s.deleteCharAt(s.length() - 1);
		showNotification("Success", MessageFormat.format("Now! total {0} Bundles present for Auctions: {1} ", totalBundleCount, s));

		return true;

	}


	public boolean addBundle(final String auctionId, final List<String> productsId)
	{
		final UIBrowserArea browserArea = UISessionUtils.getCurrentSession().getCurrentPerspective().getBrowserArea();

		final BundleNavigationNodeBrowserArea bArea = (BundleNavigationNodeBrowserArea) browserArea;

		final AuctionEventModel auctionEvent = getJslAuctionService().getAuctionEventForAuctionId(auctionId);

		if (auctionEvent == null)
		{
			showNotification("Alert!", MessageFormat.format("No Auction present for Id {0}", auctionId));
			return false;
		}

		if (!auctionEvent.getAuctionStatus().getCode().equalsIgnoreCase(AuctionStatus.READYFORBUNDLE.getCode())
				&& !auctionEvent.getAuctionStatus().getCode().equalsIgnoreCase(AuctionStatus.READYFOROTP.getCode()))
		{
			showNotification("Alert!",
					MessageFormat.format("Auction {0} should be is either ReadyForBundle or ReadyForOTP status!", auctionId));
			return false;
		}

		final List<ProductModel> products = getJslProductService().getAuctionProductsForIds(productsId,
				bArea.getActiveCatalogVersion());

		if (products.isEmpty())
		{
			showNotification("Alert!", "No Product Presents!");
			return false;
		}


		for (final ProductModel product : products)
		{
			final AuctionProductModel auctionProduct = (AuctionProductModel) product;

			if (!auctionEvent.getAuctionGroup().getValidPhysicalLocations().contains(auctionProduct.getLocation()))
			{
				showNotification("Alert!",
						MessageFormat.format("product with code {0} does not belong to valid physical locations!", product.getCode()));
				return false;
			}

			if (auctionProduct.getBundled())
			{
				showNotification("Alert!",
						MessageFormat.format("product with code {0} is already bundled true in system!", product.getCode()));
				return false;
			}

		}

		final BundleTemplateModel bundleTemplate = getJslBundleTemplateService().createBundleTemplate("", products,
				bArea.getActiveCatalogVersion());

		if (bundleTemplate != null)
		{
			final int totalBundlePrsent = getJslBundleTemplateService()
					.getBundleTemplatesForAuctionId(bundleTemplate.getAuctionEvent().getAuctionId().toString()).size();
			final BundleTemplateModel rootTemplate = bundleTemplate.getParentTemplate();
			rootTemplate.setName("Lot-" + totalBundlePrsent);
			bundleTemplate.setName("Lot-" + totalBundlePrsent);
			getModelService().save(rootTemplate);
			getModelService().save(bundleTemplate);
			showNotification("Success", MessageFormat.format("Bundle {0} added for Auction {1} ", bundleTemplate.getName(),
					bundleTemplate.getAuctionEvent().getAuctionId()));

		}
		return true;
	}



	/**
	 * @param bArea
	 */
	public void showNotification(final String caption, final String message)
	{
		final BaseUICockpitPerspective basePerspective = (BaseUICockpitPerspective) UISessionUtils.getCurrentSession()
				.getCurrentPerspective();
		if (basePerspective.getNotifier() != null)
		{
			basePerspective.getNotifier().setNotification(new Notification(caption, message));
		}

		basePerspective.update();
	}

	protected ModelService getModelService()
	{
		if (this.modelService == null)
		{
			this.modelService = (ModelService) SpringUtil.getBean("modelService");
		}

		return this.modelService;
	}



	protected JslAuctionService getJslAuctionService()
	{
		if (this.jslAuctionService == null)
		{
			this.jslAuctionService = (JslAuctionService) SpringUtil.getBean("jslAuctionService");
		}

		return this.jslAuctionService;
	}

	protected JslProductService getJslProductService()
	{
		if (this.jslProductService == null)
		{
			this.jslProductService = (JslProductService) SpringUtil.getBean("jslProductService");
		}

		return this.jslProductService;
	}


	protected JslBundleTemplateService getJslBundleTemplateService()
	{
		if (this.jslBundleTemplateService == null)
		{
			this.jslBundleTemplateService = (JslBundleTemplateService) SpringUtil.getBean("jslBundleTemplateService");
		}

		return this.jslBundleTemplateService;
	}



	protected BundleEngineService getBundleEngineService()
	{
		if (this.bundleEngineService == null)
		{
			this.bundleEngineService = (BundleEngineService) SpringUtil.getBean("bundleEngineService");
		}

		return this.bundleEngineService;
	}



	@Override
	public AbstractContentBrowser createViewComponent()
	{
		return new JslBundleNavigationNodeContentBrowser();
	}


	protected Collection<PromotionGroupModel> getPromotionGroups()
	{
		final Collection<PromotionGroupModel> promotionGroupModels = new ArrayList();
		if (getBaseSiteService().getCurrentBaseSite() != null
				&& getBaseSiteService().getCurrentBaseSite().getDefaultPromotionGroup() != null)
		{
			promotionGroupModels.add(getBaseSiteService().getCurrentBaseSite().getDefaultPromotionGroup());
		}
		return promotionGroupModels;
	}


	/**
	 * @return the baseSiteService
	 */
	public BaseSiteService getBaseSiteService()
	{
		if (this.baseSiteService == null)
		{
			this.baseSiteService = (BaseSiteService) SpringUtil.getBean("baseSiteService");
		}

		return this.baseSiteService;
	}



}
