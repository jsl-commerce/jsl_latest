/**
 *
 */
package com.jsl.bundlecockpit.components.navigationarea;


import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cockpit.model.meta.ObjectTemplate;
import de.hybris.platform.cockpit.model.meta.PropertyDescriptor;
import de.hybris.platform.cockpit.model.search.ExtendedSearchResult;
import de.hybris.platform.cockpit.model.search.Query;
import de.hybris.platform.cockpit.model.search.SearchType;
import de.hybris.platform.cockpit.model.search.impl.DefaultExtendedSearchResult;
import de.hybris.platform.cockpit.services.search.SearchProvider;
import de.hybris.platform.configurablebundlecockpits.productcockpit.session.impl.BundleProductSearchBrowserModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.jsl.core.model.AuctionProductModel;


public class JslProductSearchBrowserModel extends BundleProductSearchBrowserModel
{
	private static final Logger LOG = Logger.getLogger(JslProductSearchBrowserModel.class);

	@Override
	protected ExtendedSearchResult doSearchInternal(final Query query)
	{
		ServicesUtil.validateParameterNotNull(query, "Query can not be null.");
		final SearchProvider searchProvider = this.getSearchProvider();
		if (searchProvider == null)
		{
			return null;
		}
		else
		{
			final int pageSize = query.getCount() > 0 ? query.getCount() : this.getPageSize();
			final SearchType selectedType = this.getSelectedType(query);
			final Query searchQuery = new Query(Collections.singletonList(selectedType), query.getSimpleText(), query.getStart(),
					pageSize);
			searchQuery.setParameterValues(query.getParameterValues());
			searchQuery.setParameterOrValues(query.getParameterOrValues());
			searchQuery.setExcludeSubTypes(query.isExcludeSubTypes());
			final ObjectTemplate selTemplate = (ObjectTemplate) query.getContextParameter("objectTemplate");
			if (selTemplate != null)
			{
				searchQuery.setContextParameter("objectTemplate", selTemplate);
			}

			this.updateCatalogVersionsFromQuery(query);
			this.updateSelectedCategoriesFromQuery(query);
			Collection<CatalogVersionModel> catalogVersions = this.getSelectedCatalogVersions();
			if (((Collection) catalogVersions).isEmpty() && this.getSelectedCategories().isEmpty())
			{
				catalogVersions = this.getProductCockpitCatalogService().getAvailableCatalogVersions();
			}

			searchQuery.setContextParameter("selectedCatalogVersions", catalogVersions);
			if (!this.getSelectedCategories().isEmpty())
			{
				searchQuery.setContextParameter("selectedCategories", this.getSelectedCategories());
			}

			final Map<PropertyDescriptor, Boolean> sortCriterion = this.getSortCriterion(query);
			this.addCriterionToSearchQuery(sortCriterion, searchQuery);

			try
			{
				final Query clonedQuery = (Query) searchQuery.clone();
				this.setLastQuery(clonedQuery);
			}
			catch (final CloneNotSupportedException var10)
			{
				LOG.error("Cloning the query is not supported");
				LOG.debug("Cloning exception", var10);
			}

			if (this.getBrowserFilter() != null)
			{
				this.getBrowserFilter().filterQuery(searchQuery);
			}

			final ExtendedSearchResult result = searchProvider.search(searchQuery);
			final List filteredList;
			//Filtration of bundled products in case of Auction Product Instance
			if (result.getResult() != null && result.getTotalCount() > 0)
			{
				filteredList = new ArrayList<>(result.getTotalCount());
				result.getResult().stream().filter(product -> ((AuctionProductModel) product.getObject()).getBundled() == false)
						.forEach(product -> filteredList.add(product));

				final DefaultExtendedSearchResult modifiedResult = new DefaultExtendedSearchResult(result.getQuery(), filteredList,
						filteredList.size());
				this.updateLabels();
				return modifiedResult;
			}

			this.updateLabels();
			return result;
		}
	}
}
