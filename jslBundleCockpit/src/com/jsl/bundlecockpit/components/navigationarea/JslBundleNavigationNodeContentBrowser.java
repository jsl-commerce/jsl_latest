/**
 *
 */
package com.jsl.bundlecockpit.components.navigationarea;

import de.hybris.platform.cockpit.components.contentbrowser.AbstractBrowserComponent;
import de.hybris.platform.configurablebundlecockpits.productcockpit.session.impl.BundleNavigationNodeContentBrowser;


/**
 * @author manav.magoo
 *
 */
public class JslBundleNavigationNodeContentBrowser extends BundleNavigationNodeContentBrowser
{
	@Override
	protected AbstractBrowserComponent createMainAreaComponent()
	{
		return new JslBundleNavigationNodeContentMainComponent(this.getModel(), this);
	}

	@Override
	public JslBundleNavigationNodeBrowserModel getModel()
	{
		return (JslBundleNavigationNodeBrowserModel) super.getModel();
	}

}
