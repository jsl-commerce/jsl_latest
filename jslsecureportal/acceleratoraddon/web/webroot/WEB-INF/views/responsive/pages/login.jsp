
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sptemplate" tagdir="/WEB-INF/tags/addons/jslsecureportal/responsive/sptemplate" %>
<%@ taglib prefix="spuser" tagdir="/WEB-INF/tags/addons/jslsecureportal/responsive/spuser" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>

<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="spheader" tagdir="/WEB-INF/tags/addons/jslsecureportal/responsive/common/spheader" %>
<%@ taglib prefix="footer" tagdir="/WEB-INF/tags/responsive/common/footer" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<spring:htmlEscape defaultHtmlEscape="true" />
<sptemplate:page pageTitle="${pageTitle}">

    <link href="${commonResourcePath}/customAssets/css/jslLoginmain.css" rel="stylesheet" type="text/css">
    <link href="${commonResourcePath}/customAssets/css/inc-header.css" rel="stylesheet" type="text/css">
    <link href="${commonResourcePath}/customAssets/css/lightslider.css" rel="stylesheet" type="text/css">

    <script src="${commonResourcePath}/customAssets/js/1.7.2.jquery.min.js" type="application/javascript"></script>

    <script src="${commonResourcePath}/customAssets/js/lightslider.js" type="application/javascript"></script>
    <script src="${commonResourcePath}/customAssets/js/jquery.prettyPhoto.js" type="application/javascript"></script>
    <script src="${commonResourcePath}/customAssets/js/main.js" type="application/javascript"></script>
    <script src="${commonResourcePath}/customAssets/js/login.js" type="application/javascript"></script>
    <script src="${commonResourcePath}/customAssets/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="${commonResourcePath}/customAssets/css/font-awesome.min.css" />


    <div class="wrapper">
        <!--------------------------------------For Validation------------------------------------------------------>

        <header class="main-header">
           <!--  <div class="top-strip">
                <ul class="top-header-list">
                    <li><a href="#" class="btn btn-block" data-toggle="modal" data-target="#myModal">Log In</a></li>

                </ul>
            </div> -->
            
          
            <div class="menu-header">
            <div class="nav_icon"><i class="lni-menu"></i></div>
                <div class="logo">
                    <img src="${commonResourcePath}/customAssets/images/logo.jpg" alt="Jindal">
                </div>
                <nav class="navigation">
                    <li><a href="#">About us</a>
                        <div class="main-dropdown">
                            <div class="drop-container">
                                <div class="drop-1st-header">
                                    <ul class="menu-filter" id="menu-filter1">
                                        <li><a class="show-article current" target="1">Our Founder</a></li>
                                        <li><a class="show-article" target="2">Vision</a></li>
                                        <li><a class="show-article" target="3">Group Overview</a></li>
                                        <li><a class="show-article" target="4">Group Companies</a></li>
                                        <li><a class="show-article" target="5">Leadership Team</a></li>
                                        <li><a class="show-article" target="6">Social Initiatives</a></li>
                                        <li><a class="show-article" target="7">Stainless Steel Initiatives</a></li>
                                    </ul>
                                    <div class="show-article-drop1 display" id="article-drop1">
                                        <div class="article-sec">
                                            <img src="${commonResourcePath}/customAssets/images/mr-op-jindal.jpg">
                                            <article>
                                                <h4>Our Founder</h4>
                                                <br>
                                                <p>Shri. Om Prakash Jindal, Founder Chairman of the Jindal Group was born on August 7, 1930, to a farmer in Nalwa village of Hisar district in Haryana. Having interest in technology from a young age, he started
                                                    his industrial career with a humble bucket-manufacturing unit in Hisar in 1952. In 1964, he commissioned a pipe unit, Jindal India Limited followed by a large factory in 1969 under the name of Jindal
                                                    Strips Limited... <a href="http://www.jindalstainless.com/our-founder.php">Read More</a>
                                                </p>
                                            </article>
                                        </div>
                                    </div>
                                    <div class="show-article-drop1" id="article-drop2">
                                        <div class="article-sec">
                                            <img src="${commonResourcePath}/customAssets/images/banner2.jpg">
                                            <article>
                                                <h4>Our Vision</h4>
                                                <br>
                                                <p>Improving lives through trustworthy and innovative Stain-less solutions.. <a href="vision-mission.php">Read More</a>
                                                </p>
                                            </article>
                                        </div>
                                    </div>
                                    <div class="show-article-drop1" id="article-drop3">
                                        <div class="article-sec">
                                            <img src="${commonResourcePath}/customAssets/images/overview-groupoverview.jpg">
                                            <article>
                                                <h4>Group Overview</h4>
                                                <br>
                                                <p>Founded by Shri O.P Jindal in 1970, Jindal Stainless is one of the largest stainless steel conglomerates in India and ranks amongst the top 10 stainless steel conglomerates in the world. It's not only the
                                                    magnitude of our operations that determines our credibility and name, but we remain inspired by our vision for innovation and enriching lives. Jindal Stainless Group has an annual crude steel capacity
                                                    of 1.8 MTPA and the group has an annual turnover of US $ 3.1 billion (as on March'18)... <a href="group-overview.php">Read More</a>
                                                </p>
                                            </article>
                                        </div>
                                    </div>
                                    <div class="show-article-drop1" id="article-drop4">
                                        <section class="dsection">
                                            <div class="article-sec">
                                                <img src="${commonResourcePath}/customAssets/images/white-coil.jpg">
                                                <article class="inner-dsection">
                                                    <ul>
                                                        <li><a href="http://www.jindalstainless.com/jindal-stainless-corp-mgt-pvt-ltd.php">Jindal Stainless Corp Mgt Services PVT. LTD.</a></li>
                                                        <li><a href="http://www.jindalstainless.com/jindal-stainless.php">Jindal Stainless LTD.</a></li>
                                                        <li><a href="http://www.jindalstainless.com/jindal-stainless-hisar-ltd.php">Jindal Stainless (Hisar) LTD.</a></li>

                                                    </ul>
                                                </article>
                                            </div>
                                        </section>
                                    </div>
                                    <div class="show-article-drop1" id="article-drop5">
                                        <div class="article-sec">
                                            <img src="${commonResourcePath}/customAssets/images/leadership-team.jpg">
                                            <article>
                                                <h4>Leadership Team</h4>
                                                <br>
                                                <p>Meet our leadership team. <a href="http://www.jindalstainless.com/leadership.php">Read More</a>
                                                </p>
                                            </article>
                                        </div>
                                    </div>
                                    <div class="show-article-drop1" id="article-drop6">
                                        <div class="article-sec">
                                            <img src="${commonResourcePath}/customAssets/images/overview-csr.jpg">
                                            <article>
                                                <h4>Social Initiatives</h4>
                                                <br>
                                                <p>Founder of Jindal Group, Mr. O.P. Jindal was one who envisioned success not only for himself and his business, but also for those who shared his vision and had the faith to see his dreams transform into
                                                    India's shining example of corporate excellence. OPJEMS is the initiative in support of this vision. The OPJEMS (O. P. Jindal Engineering & Management Scholarship) was started in the year 2007 to commemorate
                                                    Shri O.P. Jindal, the founding father of the... <a href="http://www.jindalstainless.com/social-initiatives-report.php">Read More</a>
                                                </p>
                                            </article>
                                        </div>
                                    </div>
                                    <div class="show-article-drop1" id="article-drop7">
                                        <div class="article-sec">
                                            <img src="${commonResourcePath}/customAssets/images/overview-stainless-initiatives.jpg">
                                            <article>
                                                <h4>Stainless Steel Initiatives</h4>
                                                <br>
                                                <p>Stainless Steel is truly the metal of the century. Here at 'The Stainless' we travel many geographies from art, design and architecture to provide a platform for creative minds for expressing their interpretation
                                                    & experimentation with this metal. Launched in 2007 by Ms.Deepikaa Jindal, The Stainless Gallery has showcased works of eminent designers, architects, artists & sculptors through our previous exhibitions
                                                    like Saptarishis, Ashtanayika & Ekant to name a... <a href="http://www.jindalstainless.com/stainless-initiatives.php">Read More</a>
                                                </p>
                                            </article>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li><a href="javascript:;">products</a>
                        <div class="main-dropdown">
                            <div class="drop-container">
                                <div class="drop-1st-header">
                                    <ul class="menu-filter" id="menu-filter-application">
                                        <li><a class="showstainless current" target="1">Overview</a></li>
                                        <li><a class="showstainless" target="2">JSHL Products</a></li>
                                        <li><a class="showstainless" target="3">JSL Products</a></li>
                                        <li><a class="showstainless" target="4">Brochures</a></li>
                                    </ul>
                                </div>
                                <div class="show-drop3 display" id="stainless1">
                                    <div class="article-sec">
                                        <img src="${commonResourcePath}/customAssets/images/jsp.jpg">
                                        <article>
                                            <h4>Overview</h4>
                                            <br>
                                            <p>Jindal Stainless group is India's largest producer of stainless steel in 200, 300, 400 and duplex grades. Combining an experience of over four decades, a clear focus on customer needs, coupled with cross market
                                                expertise, a wide range of products and modern technology has been the mainstay of their success.... <a href="stainless-products.php">Read More</a>
                                            </p>
                                        </article>
                                    </div>
                                </div>
                                <div class="show-drop3" id="stainless2">
                                    <div class="article-sec">
                                        <img src="${commonResourcePath}/customAssets/images/jshl-products.jpg">
                                        <article>
                                            <ul class="list">
                                                <li><a href="http://www.jindalstainless.com/jshl-product-line.php">Product Line</a></li>
                                                <li><a href="http://www.jindalstainless.com/grade.php">Grades</a></li>
                                                <li><a href="http://www.jindalstainless.com/business-query.php">Business Query</a></li>
                                                <li><a href="jshl-quality-technology.php">Quality & Technology</a></li>
                                                <li><a href="http://www.jindalstainless.com/utilities.php">Utilities</a></li>
                                            </ul>
                                        </article>
                                    </div>
                                </div>
                                <div class="show-drop3" id="stainless3">
                                    <div class="article-sec">
                                        <img src="${commonResourcePath}/customAssets/images/jsl-products.jpg">
                                        <article>
                                            <ul class="list">
                                                <li><a href="http://www.jindalstainless.com/jsl-product-line.php">Product Line</a></li>
                                                <li><a href="http://www.jindalstainless.com/grade.php">Grades</a></li>
                                                <li><a href="http://www.jindalstainless.com/business-query.php">Business Query</a></li>
                                                <li><a href="http://www.jindalstainless.com/jsl-quality-technology.php">Quality & Technology</a></li>
                                                <li><a href="http://www.jindalstainless.com/utilities.php">Utilities</a></li>
                                            </ul>
                                        </article>
                                    </div>
                                </div>
                                <div class="show-drop3" id="stainless4">
                                    <div class="article-sec">
                                        <img src="${commonResourcePath}/customAssets/images/white-coil.jpg">
                                        <article>
                                            <ul class="list">
                                                <li><a href="http://www.jindalstainless.com/brochures.php">Long Products</a></li>
                                                <li><a href="http://www.jindalstainless.com/brochures.php">Defence</a></li>
                                                <li><a href="http://www.jindalstainless.com/brochures.php">JSL Corporate</a></li>
                                                <li><a href="http://www.jindalstainless.com/brochures.php">Jindal Stainless Product</a></li>
                                                <li class="viewMoreBtn"><a href="http://www.jindalstainless.com/brochures.php">View More</a></li>
                                            </ul>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li><a href="javascript:;">Stainless Steel</a>
                        <div class="main-dropdown">
                            <div class="drop-container">
                                <div class="drop-1st-header">
                                    <ul class="menu-filter" id="menu-filter-application">
                                        <li><a class="application current" target="1">Magic Of Stainless Steel</a></li>
                                        <li><a class="application" target="2">Applications of Stainless Steel</a></li>
                                    </ul>
                                </div>
                                <div class="single-menu5 applicationShow display" id="applicationdrop1">
                                    <div class="article-sec">
                                        <img src="${commonResourcePath}/customAssets/images/mss.jpg">
                                        <article>
                                            <h4>Overview</h4>
                                            <br>
                                            <p>When stainless steel was accidentally discovered by an English metallurgist, Harry Brearley, in August 1913, no one could have ever thought that it would so quickly become the material of the modern progressive
                                                world in such a short time. But it did. Stainless steel is attractive because of its resistance to erosion and corrosion; can you believe that the global economy loose over $ 5000 bn USD every year...
                                                <a href="http://www.jindalstainless.com/magic-of-stainless-steel.php">Read More</a>
                                            </p>
                                        </article>
                                    </div>
                                </div>
                                <div class="single-menu5 applicationShow" id="applicationdrop2">
                                    <div class="article-sec">
                                        <img src="${commonResourcePath}/customAssets/images/ass.jpg">
                                        <article>
                                            <h4>Overview</h4>
                                            <br>
                                            <p>Limited to the usage in the kitchens earlier, stainless steel is now increasingly making its presence felt in places we could not have imagined a decade ago. Perhaps this is the reason why stainless steel has
                                                been the fastest growing metal for over two decades. Today, stainless steel has already established itself in sectors like automotive, railway & transport (ART), architecture, building & construction (ABC),
                                                chemical &... <a href="http://www.jindalstainless.com/application-of-stainless-steel.php">Read More</a>
                                            </p>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li><a href="javascript:;">Contact</a>
                        <div class="main-dropdown">
                            <div class="drop-container">
                                <div class="show-drop1 display">
                                    <section class="dsection">
                                        <div class="article-sec">
                                            <img src="${commonResourcePath}/customAssets/images/contact-us.jpg">
                                            <article class="inner-dsection">
                                                <h5><a href="http://www.jindalstainless.com/contact.php">Contact</a></h5>
                                                <ul>
                                                    <li><a href="http://www.jindalstainless.com/business-query.php">Business Query</a></li>
                                                    <li><a href="http://www.jindalstainless.com/contact.php">Contact us</a></li>
                                                </ul>
                                            </article>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </li>
                </nav>
                
                <ul class="top-header-login">
                    <li class="loginExisting"><a href="javascript:void(0)" class="btn">Login</a>
                    <div class="loginExistingDiv">
                     <div class="login-section">
                    <c:url value="/j_spring_security_check" var="loginActionUrl"/>
                    <spuser:login actionNameKey="login.login" action="${loginActionUrl}"/>
                </div>
                    <div class="forgotten-password">
        	        	<spring:url value="/login/pw/request" var="forgottenPwdUrl" htmlEscape="false"/>
			            <a href="javascript:void(0)" data-link="${fn:escapeXml(forgottenPwdUrl)}" class="js-password-forgotten"
			               data-cbox-title="<spring:theme code="forgottenPwd.title"/>">
			                <spring:theme code="login.link.forgottenPwd"/>
			            </a>
        
    				</div>
                    
                    </div>
                    
                    </li>

                </ul>
            </div>
        </header>

        <!--social media-->
        <ul class="social-media">
            <!-- <div class="left-sticky-h">
                <a href="http://jindalstainless.com/business-query.php"><img src="images/get_in_touch.jpg"></a>
            </div> -->
            <li><a href="https://www.facebook.com/JindalStainlessSolutions/" target="_blank"><i class="fa fa-facebook"></i></a></li>
            <li><a href="https://www.linkedin.com/company/jindal-stainlees-steel-jsl-stainless-ltd--?trk=vsrp_companies_res_name&trkInfo=VSRPsearchId%3A4448043831442550905936%2CVSRPtargetId%3A1241509%2CVSRPcmpt%3Aprimary" target="_blank"><i class="fa fa-linkedin"></i></a></li>
            <li><a href="https://twitter.com/MagicofSS" target="_blank"><i class="fa fa-twitter"></i></a></li>
            <li><a href="https://www.youtube.com/channel/UCi8c6I3UfAJl0HacYcxyhqA/videos" target="_blank"><i class="fa fa-youtube"></i></a></li>
            <!--<li class="enq-btn mb-enq-btn"><a href="javascript:void(0);"><i class="fa fa-edit"></i></a></li>-->
            <!--<div class="left-sticky"><a href="http://jindalstainless.com/Careformilk/" target="_blank"><img src="images/sticky-left.png"></a></div>-->
        </ul>


        <!-----------------------Mobile Menu--------------------->


        <div class="mbheader">
            <div class="mblogo">
                <img src="${commonResourcePath}/customAssets/images/logo.png" alt="Jindal Stainless" />
            </div>

            <div class="mbnavbar">
                <ul class="mbheader-list">
                    <li><a class="btn1" href="javascript:;" data-toggle="modal" data-target="#myModal">Login</a></li>

                </ul>

                <div class="mbicon">
                    <img src="${commonResourcePath}/customAssets/images/mob-icon.jpg" alt="mobile icon" />
                </div>

                <div class="mbmenu">
                    <ul>
                        <li class="maintab"><a href="javascript:void(0);">About us</a></li>
                        <!--1st li-->
                        <div class="mbsubmenu">
                            <ul>
                                <li><a href="http://www.jindalstainless.com/our-founder.php">Our Founder</a></li>
                                <li><a href="http://www.jindalstainless.com/vision.php">Vision</a></li>
                                <li><a href="http://www.jindalstainless.com/group-overview.php">Group Overview</a></li>
                                <li><a href="http://www.jindalstainless.com/jindal-stainless-corp-mgt-pvt-ltd.php">Group Companies</a></li>
                                <li><a href="http://www.jindalstainless.com/leadership.php">Leadership Team</a></li>
                                <li><a href="http://www.jindalstainless.com/social-initiatives.php">Social Initiatives</a></li>
                                <li><a href="http://www.jindalstainless.com/stainless-initiative.php">Stainless Steel Initiatives</a></li>
                            </ul>
                        </div>

                        <li class="maintab"><a href="javascript:void(0);">Products</a></li>
                        <!--1st li-->
                        <div class="mbsubmenu">
                            <ul>
                                <li><a href="http://www.jindalstainless.com/stainless-products.php">Overview</a></li>
                                <li><a href="http://www.jindalstainless.com/jshl-product-line.php">JSHL Products</a></li>
                                <li><a href="http://www.jindalstainless.com/jsl-product-line.php">JSL Products</a></li>
                            </ul>
                        </div>

                        <li class="maintab"><a href="javascript:void(0);">Stainless Steel</a></li>
                        <div class="mbsubmenu">
                            <ul>
                                <li><a href="http://www.jindalstainless.com/magic-of-stainless-steel.php">Magic Of Stainless Steel</a></li>
                                <li><a href="http://www.jindalstainless.com/application-of-stainless-steel.php">Applications of Stainless Steel</a></li>
                            </ul>
                        </div>

                        <li class="maintab"><a href="javascript:void(0);">Media</a></li>
                        <div class="mbsubmenu">
                            <ul>
                                <li><a href="http://www.jindalstainless.com/news.php">News</a></li>
                                <li><a href="http://www.jindalstainless.com/media-contact.php">Media Contact</a></li>
                                <li><a href="http://www.jindalstainless.com/press-release.php">Press Releases</a></li>
                                <li><a href="http://www.jindalstainless.com/awards.php">Awards</a></li>
                                <li><a href="http://www.jindalstainless.com/newsletter.php">Newsletter</a></li>
                            </ul>
                        </div>

                        <li class="maintab"><a href="javascript:;">Contact</a></li>
                        <div class="mbsubmenu">
                            <ul>
                                <li><a href="http://www.jindalstainless.com/business-query.php">Business Query</a></li>
                                <li><a href="http://www.jindalstainless.com/contact.php">Contact us</a></li>
                            </ul>
                        </div>
                    </ul>
                </div>
            </div>
        </div>



        <div class="box-container">

            <div class="app-box">
                <img src="${commonResourcePath}/customAssets/images/mss.jpg" alt="MSS">
                <h4>Magic of Stainless Steel</h4>
                <p>When stainless steel was accidentally discovered by an English metallurgist, Harry Brearley, in August 1913, no one...
                </p>
                <a class="read_more" href="http://www.jindalstainless.com/magic-of-stainless-steel.php">Read More</a>
            </div>
            <div class="app-box">
                <img src="${commonResourcePath}/customAssets/images/ass.jpg" alt="ASS">
                <h4>Applications of Stainless Steel</h4>
                <p>Limited to the usage in the kitchens earlier, stainless steel is now increasingly making its presence felt in places...
                </p>
                <a class="read_more" href="http://www.jindalstainless.com/application-of-stainless-steel.php">Read More</a>
            </div>
            <div class="app-box">
                <img src="${commonResourcePath}/customAssets/images/jsp.jpg" alt="JSS">
                <h4>Jindal Stainless Products</h4>
                <p>Jindal Stainless group is India's largest producer of stainless steel in 200, 300, 400 and duplex grades. Combining an...
                </p><a class="read_more" href="http://www.jindalstainless.com/stainless-products.php">Read More</a>
            </div>
        </div>

        <div class="right-sticky">
            <a href="http://www.makestainless.com/" target="_blank"><img src="${commonResourcePath}/customAssets/images/looking-solutions-logo-215.gif"></a>
            <a href="http://jindalstainless.com/HealthGenie/Ipromise/" target="_blank"><img src="${commonResourcePath}/customAssets/images/stickeyHealthGenie.png"></a>
            <!--<a href="http://jindalstainless.com/homesmarthome/mysmarthome/" target="_blank"><img src="images/stickey--RHS.png"></a>-->
        </div>
    </div>

    <div class="modal fade" id="myModal" role="dialog" style="z-index: 99999;">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Login</h4>
                </div>
                
            </div>

        </div>
    </div>
</sptemplate:page>

<script>

$(window).scroll(function() {    
    var scroll = $(window).scrollTop();
	if (scroll >= 50) {
        $(".navigation--middle, .menu-header").addClass("top-head-fixed fadeInDown animated");		
	}
	
	else {
         $(".navigation--middle, .menu-header").removeClass("top-head-fixed fadeInDown animated");
    }
});

$(document).ready(function(){
	$(".loginExisting a.btn").click(function(){
		$(this).toggleClass("active");
		$(".loginExistingDiv").slideUp();
		$(".loginExisting a.active").next(".loginExistingDiv").slideDown();
	});
	
	$(".nav_icon").click(function(){
		$(this).parent(".menu-header").toggleClass("nav_open");
		$(".nav_icon .lni-menu").toggleClass("lni-close");
	});
	
});

</script>