package com.jsl.core.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.junit.Ignore;

import com.jsl.core.dto.DefaultAuctionCommonDto;
import com.jsl.core.dto.DefaultBundleCommonDto;
import com.jsl.core.model.AuctionEventModel;
import com.jsl.core.services.impl.JslBidEventServiceImpl;
import com.jsl.jslConfigurableBundleService.model.BidEventModel;


@RunWith(MockitoJUnitRunner.class)
@UnitTest
@Ignore
public class JslBidEventServiceImplTest
{
	@Mock
	private DefaultAuctionCommonDto defaultAuctionCommonDto;
	@Mock
	private DefaultBundleCommonDto defaultBundleDto;
	@Mock
	private AuctionEventModel auctionEvent;
	@Mock
	private JslBidEventServiceImpl jslBidEventServiceImpl;
	@Mock
	private BidEventModel bidEventModel;
	@Mock
	private BundleTemplateModel bundleTemplateModel;
	//need values
	private final int AUCTION_ID = 00;
	private final String BID_PRICE = "10123";
	private final String PROXY_BID_PRICE = "11234";
	private final String ID = "jsl098";

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		Mockito.when(jslBidEventServiceImpl.getMaxBidForCurrentUser(Mockito.any())).thenReturn(java.util.Collections.EMPTY_LIST);
		//Mockito.when(jslBidEventServiceImpl.getProxyBidForCurrentUser(Mockito.any())).thenReturn(java.util.Collections.EMPTY_LIST);
		Mockito.when(jslBidEventServiceImpl.getWinBidList(Mockito.any())).thenReturn(java.util.Collections.EMPTY_LIST);

	}

	@Test
	public void getMaxBidForCurrentUserTest()
	{
		/*auctionEvent.setAuctionId(14);
		Mockito.when(jslBidEventServiceImpl.getMaxBidForCurrentUser(Mockito.any())).thenReturn(getDefaultBundleCommonDto());
		final List<DefaultAuctionCommonDto> result = jslBidEventServiceImpl.getMaxBidForCurrentUser(auctionEvent);
		assertNotEquals("getMaxBidForCurrentUser should returns list of DefaultAuctionCommonDto", 0, result.size());*/
	}
	
	@Test
	public void getAllMaxBidPriceTest() {
		/*Mockito.when(jslBidEventServiceImpl.getAllMaxBidPrice(Mockito.any())).thenReturn(getDefaultBundleCommonDto());
		final List<DefaultAuctionCommonDto> result = jslBidEventServiceImpl.getAllMaxBidPrice(auctionEvent);
		assertNotEquals("getMaxBidForCurrentUser should returns list of DefaultAuctionCommonDto", 0, result.size());*/
	}
	@Test
	public void getWinBidListTest()
	{
		auctionEvent.setAuctionId(14);
		Mockito.when(jslBidEventServiceImpl.getWinBidList(Mockito.any())).thenReturn(getBidEventModel());
		final List<BidEventModel> result = jslBidEventServiceImpl.getWinBidList(auctionEvent);
		assertNotEquals("getWinBidList should returns list of BidEventModel", 0, result.size());
	}
	
	@Test
	public void placeBidTest()
	{
		final BundleTemplateModel bundleTemplate = getBundleTemplateModel(ID);
		final boolean result = false;//jslBidEventServiceImpl.placeBid(bundleTemplate, BID_PRICE, true);
		assertEquals("placeBid should returns either true or false", false, result);
	}
	
	@Test
	public void getAllBidHistoryForAuctionTest() {
		Mockito.when(jslBidEventServiceImpl.getAllBidHistoryForAuction(Mockito.any())).thenReturn(getBidEventModel());
//		final List<BidEventModel> result = jslBidEventServiceImpl.getAllBidHistoryForAuction("1001");
	//	assertNotEquals("getAllBidHistoryForAuction should returns list of BidEventModel", 0, result.size());
	}
	
	@Test
	public void getAllBidHistoryForLotTest()
	{
		Mockito.when(jslBidEventServiceImpl.getAllBidHistoryForLot(Mockito.any())).thenReturn(getAllBidHistoryForLot());
		Map<String, Object> result = jslBidEventServiceImpl.getAllBidHistoryForLot(defaultBundleDto);
		assertEquals("getAllBidHistoryForLot should return one map object", getAllBidHistoryForLot(), result);
	}
	 

	private BundleTemplateModel getBundleTemplateModel(final String value)
	{
		final BundleTemplateModel btm = new BundleTemplateModel();
		btm.setId(value);
		return btm;
	}

	private AuctionEventModel getAuctionEventModel(final int value)
	{
		final AuctionEventModel aem = new AuctionEventModel();
		aem.setAuctionId(value);
		return aem;
	}

	private List<DefaultBundleCommonDto> getDefaultBundleCommonDto()
	{
		final List<DefaultBundleCommonDto> mockList = new ArrayList<DefaultBundleCommonDto>();
		final DefaultBundleCommonDto dto = new DefaultBundleCommonDto();
		mockList.add(dto);
		return mockList;

	}
	
	private List<BidEventModel> getBidEventModel()
	{
		final List<BidEventModel> mockList = new ArrayList<BidEventModel>();
		final BidEventModel bem = new BidEventModel();
		mockList.add(bem);
		return mockList;

	}
	
	
	private Map<String, Object> getAllBidHistoryForLot()
	{
		Map<String, Object> mockmap = new HashMap<String, Object>();
		return mockmap;
	}
	 
}
