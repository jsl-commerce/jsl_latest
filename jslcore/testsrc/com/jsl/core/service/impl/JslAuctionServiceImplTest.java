/**
 *
 */
package com.jsl.core.service.impl;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.jsl.core.daos.JslAuctionDao;
import com.jsl.core.model.AuctionEventModel;
import com.jsl.core.services.impl.JslAuctionServiceImpl;

/**
 * @author himanshu.sial
 *
 */
@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class JslAuctionServiceImplTest
{
	private AuctionEventModel auctionEventModel;

	@Mock
	private JslAuctionServiceImpl jslAuctionServiceImpl;
	@Mock
	private com.jsl.core.enums.AuctionStatus status;
	private final int AuctionId1 = 00;
	private final int AuctionId2 = 00;

	private final String auctionEventId = "jsl123";

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		final AuctionEventModel auctionEventModel1 = getAuctionEventModel(AuctionId1);
		final AuctionEventModel auctionEventModel2 = getAuctionEventModel(AuctionId2);
		final List<AuctionEventModel> auctionEventModels = new ArrayList();
		auctionEventModels.add(auctionEventModel1);
		auctionEventModels.add(auctionEventModel2);
	}

	@Test
	public void getActiveAuctionListTest()
	{

		Mockito.when(jslAuctionServiceImpl.getActiveAuctionList()).thenReturn(getAuctionEventModelList());
		final List<AuctionEventModel> result = jslAuctionServiceImpl.getActiveAuctionList();
		assertNotEquals("getActiveAuctionList should returns list of AuctionEventModel", 0, result.size());

	}

	@Test
	public void getUpcomingAuctionDetailsTest()
	{
		Mockito.when(jslAuctionServiceImpl.getUpcomingAuctionDetails()).thenReturn(getAuctionEventModelList());
		final List<AuctionEventModel> result = jslAuctionServiceImpl.getUpcomingAuctionDetails();
		assertNotEquals("getUpcomingAuctionDetails should returns list of AuctionEventModel", 0, result.size());
	}

	@Test
	public void getAuctionEventForAuctionIdTest()
	{
		final AuctionEventModel result = jslAuctionServiceImpl.getAuctionEventForAuctionId(auctionEventId);
		assertEquals("getAuctionEventForAuctionId should return one AuctionEventModel", auctionEventModel, result);

	}

	@Test
	public void getLiveAuctionListTest()
	{
		Mockito.when(jslAuctionServiceImpl.getLiveAuctionList()).thenReturn(getAuctionEventModelList());
		final List<AuctionEventModel> result = jslAuctionServiceImpl.getLiveAuctionList();
		assertNotEquals("getLiveAuctionListTest shud return list of AuctionEventModel", 0, result.size());
	}

	@Test
	public void getAuctionsTest()
	{
		Mockito.when(jslAuctionServiceImpl.getAuctions(Mockito.any())).thenReturn(getAuctionEventModelList());
		final List<AuctionEventModel> result = jslAuctionServiceImpl.getAuctions(status);
		assertNotEquals("getLiveAuctionListTest shud return list of AuctionEventModel", 0, result.size());
	}

	private AuctionEventModel getAuctionEventModel(final int value)
	{
		final AuctionEventModel aem = new AuctionEventModel();
		aem.setAuctionId(value);
		return aem;
	}

	private List<AuctionEventModel> getAuctionEventModelList()
	{
		final List<AuctionEventModel> mockResult = new ArrayList<AuctionEventModel>();
		final AuctionEventModel aem = new AuctionEventModel();
		aem.setAuctionId(1);
		mockResult.add(aem);
		return mockResult;
	}

	public JslAuctionDao getJslAuctionDao()
	{
		return getJslAuctionDao();
	}


}
