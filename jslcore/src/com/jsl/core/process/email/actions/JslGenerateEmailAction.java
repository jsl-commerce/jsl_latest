/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.jsl.core.process.email.actions;

import de.hybris.platform.acceleratorservices.process.email.actions.GenerateEmailAction;

import org.apache.log4j.Logger;

import com.jsl.core.services.JslCMSEmailPageService;




/**
 * A process action to generate email.
 */
public class JslGenerateEmailAction extends GenerateEmailAction
{
	private static final Logger LOG = Logger.getLogger(JslGenerateEmailAction.class);

	private JslCMSEmailPageService cmsEmailPageService;


	/*
	 * @Override public Transition executeAction(final BusinessProcessModel businessProcessModel) throws
	 * RetryLaterException { getContextResolutionStrategy().initializeContext(businessProcessModel);
	 * 
	 * final CatalogVersionModel contentCatalogVersion = getContextResolutionStrategy()
	 * .getContentCatalogVersion(businessProcessModel);
	 * 
	 * final BaseSiteModel cmsSite = getContextResolutionStrategy().getCmsSite(businessProcessModel);
	 * 
	 * if (contentCatalogVersion != null) { final EmailPageModel emailPageModel =
	 * getCmsEmailPageService().getEmailPageForFrontendTemplate(getFrontendTemplateName(), contentCatalogVersion,
	 * cmsSite);
	 * 
	 * if (emailPageModel != null) { final EmailMessageModel emailMessageModel =
	 * getEmailGenerationService().generate(businessProcessModel, emailPageModel); if (emailMessageModel != null) { final
	 * List<EmailMessageModel> emails = new ArrayList<>(); emails.addAll(businessProcessModel.getEmails());
	 * emails.add(emailMessageModel); businessProcessModel.setEmails(emails);
	 * 
	 * getModelService().save(businessProcessModel);
	 * 
	 * LOG.info("Email message generated"); return Transition.OK; } else { LOG.warn("Failed to generate email message");
	 * } } else { LOG.warn("Could not retrieve email page model for " + getFrontendTemplateName() + " and " +
	 * contentCatalogVersion.getCatalog().getName() + ":" + contentCatalogVersion.getVersion() +
	 * ", cannot generate email content"); } } else {
	 * LOG.warn("Could not resolve the content catalog version, cannot generate email content"); }
	 * 
	 * return Transition.NOK; }
	 */

	/**
	 * @return the cmsEmailPageService
	 */
	@Override
	public JslCMSEmailPageService getCmsEmailPageService()
	{
		return cmsEmailPageService;
	}


	/**
	 * @param cmsEmailPageService
	 *           the cmsEmailPageService to set
	 */
	public void setCmsEmailPageService(final JslCMSEmailPageService cmsEmailPageService)
	{
		this.cmsEmailPageService = cmsEmailPageService;
	}
}
