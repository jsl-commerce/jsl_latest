/**
 *
 */
package com.jsl.core.process.email.actions;

import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;
import de.hybris.platform.acceleratorservices.process.email.actions.RemoveSentEmailAction;
import de.hybris.platform.processengine.model.BusinessProcessModel;


/**
 * @author manav.magoo
 *
 */
public class JslRemoveSentEmailAction extends RemoveSentEmailAction
{

	@Override
	public void executeAction(final BusinessProcessModel businessProcessModel)
	{
		for (final EmailMessageModel emailMessage : businessProcessModel.getEmails())
		{
			if (emailMessage.isSent())
			{
				/*
				 * //remove to address email models containing customer email addresses. if
				 * (CollectionUtils.isNotEmpty(emailMessage.getToAddresses())) {
				 * getModelService().removeAll(emailMessage.getToAddresses()); }
				 */
				getModelService().remove(emailMessage);
			}
		}
	}
}
