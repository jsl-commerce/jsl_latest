package com.jsl.core.constants;

public enum JslAuctionEnum {

	SUCCESS("000", "Request Processed Successfully"),

	FAIL("999", "Something went wrong"),

	MANDATORY_FIELDS_MISSING_ERROR_CODE("205", "Mandatory Fields Missing"),

	INVALID_REQUEST_ERROR_CODE("101", "Invalid Request"),

	INTERNAL_SERVER_ERROR_CODE("102", "Internal Server Error"),

	ERROR_WHILE_FETCHING_DATA("103", "Something went wrong while fetching data"),

	ERROR_WHILE_UPDATING_DATA("104", "Something went wrong while updating data"),

	ERROR_WHILE_SAVING_DATA("105", "Something went wrong while saving data"),

	INVALID_AUCTIONID_ERROR_CODE("106", "Invalid Auction Event"),

	INVALID_BUNDLEID_ERROR_CODE("107", "Invalid Bundle Template"),

	BID_PRICE_EXCEED_MAX_LOT_PRICE_ERROR("108", "Bid price cannot be greater than lot max price"),

	BID_PRICE_LESS_THAN_H1_ERROR("109", "Bid price cannot be less than than current H1 price"),

	BID_PRICE_LESS_THAN_BASEBID_ERROR("110", "Bid price cannot be less than lot base bid price"),

	BID_PRICE_EQUAL_PROXY_PRICE_ERROR("114", "Bid price cannot be equal to proxy bid price"),

	BUNDLE_ALREADY_PRESENT_IN_WATCHLIST_ERROR("111", "Lot already present in watch list"),

	AUCTION_NOT_IN_STARTED_STATE("112", "Auction is closed"),

	PROXY_BID_ALREADY_SET_FOR_THIS_PRICE("113", "Proxy bid already set with this price"),

	PROXY_BID_LESS_THAN_FOR_ALREADY_SET_PROXY_PRICE("115", "Proxy bid cannot be less than already set proxy price");

	/** The response code. */
	private String responseCode;

	/** The response description. */
	private String responseDescription;

	/**
	 * Instantiates a new response enum.
	 *
	 * @param responseCode the response code
	 * @param responseDescription the response description
	 */
	private JslAuctionEnum(final String responseCode, final String responseDescription){
		this.responseCode = responseCode;
		this.responseDescription = responseDescription;
	}

	/**
	 * Gets the response code.
	 *
	 * @return the response code
	 */
	public String getResponseCode() {
		return responseCode;
	}

	/**
	 * Gets the response description.
	 *
	 * @return the response description
	 */
	public String getResponseDescription() {
		return responseDescription;
	}

	/**
	 * Gets the response desrciption by code.
	 *
	 * @param responseCode the response code
	 * @return the response desrciption by code
	 */
	public static String getResponseDesrciptionByCode(final String responseCode){
		for(final JslAuctionEnum enumPE: JslAuctionEnum.values()) {
		      if(enumPE.responseCode.equals(responseCode)) {
		        return enumPE.getResponseDescription();
		      }
		    }

		return null;
	}
}
