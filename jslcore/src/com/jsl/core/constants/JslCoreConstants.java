/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.jsl.core.constants;

/**
 * Global class for all JslCore constants. You can add global constants for your extension into this class.
 */
public final class JslCoreConstants extends GeneratedJslCoreConstants
{
	public static final String EXTENSIONNAME = "jslcore";
	public static final String OTP_CODE_MAX_ATTEMPTS = "otp.max.attempts";


	public JslCoreConstants()
	{
		//empty
	}

	// implement here constants used by this extension
	public static final String QUOTE_BUYER_PROCESS = "quote-buyer-process";
	public static final String QUOTE_SALES_REP_PROCESS = "quote-salesrep-process";
	public static final String QUOTE_USER_TYPE = "QUOTE_USER_TYPE";
	public static final String QUOTE_SELLER_APPROVER_PROCESS = "quote-seller-approval-process";
	public static final String QUOTE_TO_EXPIRE_SOON_EMAIL_PROCESS = "quote-to-expire-soon-email-process";
	public static final String QUOTE_EXPIRED_EMAIL_PROCESS = "quote-expired-email-process";
	public static final String QUOTE_POST_CANCELLATION_PROCESS = "quote-post-cancellation-process";
	public static final String OTP_CODE_TTL = "otp.code.ttl";
	public static final String DEFAULTSTORE = "jsl";
	public static final String DEFAULTSITE = "jsl";
	public static final String DEFAULTLANGUAGE = "en";
	public static final String DEFAULTCURRENCY = "INR";
	public static final String SALESORGANIZATION_1000 = "1000";
	public static final String SALESORGANIZATION_1010 = "1010";
	public static final String STOCK_SORT_CODE = "stockSortCode";
	public static final String DEFAULT_B2BUNIT_CODE = "default.b2b.unit.codes";
	public static final String CUSTOMER_ID = "customerId";
	public static final String CUSTOMER_NAME = "customerName";
	public static final String KAM_ID = "kamId";
	public static final String ECC_LEAD_ID = "eccLeadId";
	public static final String STAINLESS = "STAINLESS";
	public static final String ECC_REGISTERED_USER_QUALIFICATION_CODE = "03";
	public static final String ECC_ORIGIN_TYPE_CODE = "Z03";
	public static final String ECC_LEAD_DOCUMENT_CODE = "ZQ";
	public static final String ECC_PROODUCT_CATEGORY_INTERNAL_ID = "20";
	public static final String PRODUCT_ENQUIRY_ID = "enquiryId";
	public static final String C4C_COMPLAINT_ID = "c4cComplaintId";
}
