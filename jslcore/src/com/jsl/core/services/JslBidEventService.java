/**
 *
 */
package com.jsl.core.services;

import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.order.OrderModel;

import java.util.List;
import java.util.Map;

import com.jsl.core.dto.DefaultBundleCommonDto;
import com.jsl.core.model.AuctionEventModel;
import com.jsl.jslConfigurableBundleService.model.BidEventModel;


public interface JslBidEventService
{
	public List<DefaultBundleCommonDto> getMaxBidForCurrentUser(AuctionEventModel auctionEvent);

	public List<DefaultBundleCommonDto> getAllMaxBidPrice(AuctionEventModel auctionEvent);

	public boolean placeBid(BundleTemplateModel bundleTemplate, String bidPrice);

	public List<BidEventModel> getWinBidList(AuctionEventModel auctionEvent);

	public List<BidEventModel> getAllBidHistoryForAuction(List<AuctionEventModel> auctionEventList);

	public Map<String, Object> getAllBidHistoryForLot(DefaultBundleCommonDto lotDetails);

	public List<BidEventModel> getAllBidHistoryForBundle(DefaultBundleCommonDto lotDetails);

	public List<BidEventModel> getLastBidEventForBundle(String bundleId);

	public List<BidEventModel> getLostAuctionLotsDetailsForCurrentuser(List<BundleTemplateModel> bundleTemplateList,
			String flowName);

	public List<BidEventModel> getLostAuctionLotsDetailsForCurrentUserByDateRange(List<BundleTemplateModel> bundleTemplateList,
			DefaultBundleCommonDto auctionDateRange);

	public List<OrderModel> getOrdersForAuction(List<Integer> suctionIdList);

	public List<BidEventModel> getBundleOnWhichUserDidNotPlaceBid(List<BundleTemplateModel> bundleTemplateList);

	public List<BidEventModel> getBundleOnWhichUserDidNotPlaceBidByDateRange(List<BundleTemplateModel> bundleTemplateList,
			DefaultBundleCommonDto auctionDateRange);

}
