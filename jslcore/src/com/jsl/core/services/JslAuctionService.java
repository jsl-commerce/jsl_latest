/**
 *
 */
package com.jsl.core.services;

import java.util.List;

import com.jsl.core.dto.DefaultBundleCommonDto;
import com.jsl.core.enums.AuctionStatus;
import com.jsl.core.model.AuctionEventModel;


/**
 * @author manav.magoo
 *
 */
public interface JslAuctionService
{

	public List<AuctionEventModel> getActiveAuctionList();

	public List<AuctionEventModel> getUpcomingAuctionDetails();

	public List<AuctionEventModel> getLiveAuctionList();

	public AuctionEventModel getAuctionEventForAuctionId(String auctionId);

	public List<AuctionEventModel> getAuctions(AuctionStatus status);

	public void updateAuctionStatus(AuctionEventModel auctionEvent, AuctionStatus status);

	public long updateAuctionEventWithAdditionalTime(AuctionEventModel auctionEvent);

	public List<AuctionEventModel> getAllAuctionList();

	public List<AuctionEventModel> getAuctionByDateRange(DefaultBundleCommonDto auctionDateRange);

	public List<AuctionEventModel> getAuctionByDate(DefaultBundleCommonDto auctionDateRange);
}
