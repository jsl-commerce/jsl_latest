/**
 *
 */
package com.jsl.core.services;

import java.util.Map;

import com.jsl.core.dto.JslOTPGenerationDto;
import com.jsl.core.dto.JslOTPValidationDto;
import com.jsl.core.enums.OTPGeneratedForEnum;
import com.jsl.core.enums.OTPUserIdentificationType;
import com.jsl.core.model.JSLUserOTPGenerationModel;


/**
 * @author suyash.trivedi
 *
 */
public interface JslUserOTPService
{
	boolean generateOTP(final OTPGeneratedForEnum generatedModule, final OTPUserIdentificationType identityType,
			final String identityValue, final JslOTPGenerationDto jslOTPGenerationDto);


	/**
	 * @param otp
	 * @param identityType
	 * @param identityValue
	 * @param generatedModule
	 * @param enteredOTPTime
	 * @param jslOTPValidationDto
	 * @return
	 */
	Map<String, String> validateOTP(String otp, OTPUserIdentificationType identityType, String identityValue,
			OTPGeneratedForEnum generatedModule, long enteredOTPTime, JslOTPValidationDto jslOTPValidationDto);


	/**
	 * @param identityType
	 * @param identityValue
	 * @param generatedModule
	 * @param enteredOTPTime
	 * @param jslOTPValidationDto
	 * @return
	 */
	JSLUserOTPGenerationModel getUserAuctionOTPGeneratedRecord(OTPUserIdentificationType identityType, String identityValue,
			OTPGeneratedForEnum generatedModule, JslOTPValidationDto jslOTPValidationDto);


	/**
	 * @param identityType
	 * @param identityValue
	 * @param generatedModule
	 * @param jslOTPValidationDto
	 * @param status
	 * @return
	 */
	JSLUserOTPGenerationModel getUserAuctionOTPGeneratedRecordByOTPStatus(OTPUserIdentificationType identityType,
			String identityValue, OTPGeneratedForEnum generatedModule, JslOTPValidationDto jslOTPValidationDto, String status);
}
