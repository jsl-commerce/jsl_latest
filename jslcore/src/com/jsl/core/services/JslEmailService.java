/**
 *
 */
package com.jsl.core.services;

import de.hybris.platform.acceleratorservices.email.impl.DefaultEmailService;
import de.hybris.platform.acceleratorservices.model.email.EmailAddressModel;
import de.hybris.platform.acceleratorservices.model.email.EmailAttachmentModel;
import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;
import de.hybris.platform.catalog.CatalogService;

import java.io.DataInputStream;
import java.util.Date;
import java.util.List;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * @author manav.magoo
 *
 */
public class JslEmailService extends DefaultEmailService
{
	private static final Logger LOG = Logger.getLogger(JslEmailService.class);
	private static final int MAXRETRYCOUNT = 3;
	private static final int THREADSLEEPTIME = 120000;

	@Autowired
	private CatalogService catalogService;

	protected void logJslInfo(final EmailMessageModel message, final EmailException e, final int retryCount)
	{
		LOG.warn(
				"Could not send e-mail pk [" + message.getPk() + "] subject [" + message.getSubject() + "] cause: " + e.getMessage());
		LOG.warn(" JslEmailService : exception occured while sending email is :  " + e + " cause: " + e.getCause()
				+ " Retry Count is : " + retryCount);


	}


	@Override
	public boolean send(final EmailMessageModel message)
	{
		if (message == null)
		{
			throw new IllegalArgumentException("message must not be null");
		}

		final boolean sendEnabled = getConfigurationService().getConfiguration().getBoolean(EMAILSERVICE_SEND_ENABLED_CONFIG_KEY,
				true);
		if (sendEnabled)
		{
			try
			{
				final HtmlEmail email = getPerConfiguredEmail();
				email.setCharset("UTF-8");

				final List<EmailAddressModel> toAddresses = message.getToAddresses();
				setAddresses(message, email, toAddresses);

				final EmailAddressModel fromAddress = message.getFromAddress();
				email.setFrom(fromAddress.getEmailAddress(), nullifyEmpty(fromAddress.getDisplayName()));

				addReplyTo(message, email);

				email.setSubject(message.getSubject());
				email.setHtmlMsg(getBody(message));

				// To support plain text parts use email.setTextMsg()

				final List<EmailAttachmentModel> attachments = message.getAttachments();
				if (!processAttachmentsSuccessful(email, attachments))
				{
					return false;
				}

				// Important to log all emails sent out
				LOG.info("Sending Email [" + message.getPk() + "] To [" + convertToStrings(toAddresses) + "] From ["
						+ fromAddress.getEmailAddress() + "] Subject [" + email.getSubject() + "]");

				// Send the email and capture the message ID
				sendMessage(message, email);


				getModelService().save(message);

				return true;
			}
			catch (final EmailException | InterruptedException e)
			{
				LOG.warn(" JslEmailService : exception occured in send method while sending email is :  " + e + " cause: "
						+ e.getCause() + " Message is  : " + e.getMessage());
			}
		}
		else
		{
			LOG.warn("Could not send e-mail pk [" + message.getPk() + "] subject [" + message.getSubject() + "]");
			LOG.info("Email sending has been disabled. Check the config property 'emailservice.send.enabled'");
			return true;
		}

		return false;
	}


	/**
	 * @param message
	 * @param email
	 */
	private void sendMessage(final EmailMessageModel message, final HtmlEmail email) throws InterruptedException
	{
		int retryCount = 0;

		while (retryCount <= MAXRETRYCOUNT)
		{
			try
			{
				final String messageID;
				if (retryCount == 0)
				{
					messageID = email.send();
				}
				else
				{
					messageID = email.sendMimeMessage();
				}
				message.setSent(true);
				message.setSentMessageID(messageID);
				message.setSentDate(new Date());
				break;

			}
			catch (final EmailException e)
			{
				logJslInfo(message, e, retryCount);
				retryCount++;
				Thread.sleep(THREADSLEEPTIME);
			}
		}
	}

	@Override
	public EmailAttachmentModel createEmailAttachment(final DataInputStream masterDataStream, final String filename,
			final String mimeType)
	{
		final EmailAttachmentModel attachment = getModelService().create(EmailAttachmentModel.class);
		attachment.setCode(filename);
		attachment.setMime(mimeType);
		attachment.setRealFileName(filename);
		attachment.setCatalogVersion(getCatalogVersion());
		if (null == getCatalogVersion())
		{
			attachment.setCatalogVersion(catalogService.getCatalogVersion("jslContentCatalog", "Online"));
		}
		getModelService().save(attachment);
		getMediaService().setStreamForMedia(attachment, masterDataStream, filename, mimeType, getEmailAttachmentsMediaFolder());
		return attachment;
	}

}
