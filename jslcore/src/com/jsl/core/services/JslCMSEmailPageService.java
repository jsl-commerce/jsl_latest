/**
 *
 */
package com.jsl.core.services;

import de.hybris.platform.acceleratorservices.email.CMSEmailPageService;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;


/**
 *
 */
public interface JslCMSEmailPageService extends CMSEmailPageService
{
	/**
	 * Retrieves EmailPage given its frontend template name.
	 *
	 * @param frontendTemplateName
	 *           Frontend template name
	 * @param catalogVersion
	 *           Catalog version
	 * @return EmailPage object if found, null otherwise
	 */
	EmailPageModel getEmailPageForFrontendTemplate(String frontendTemplateName, CatalogVersionModel catalogVersion,
			BaseSiteModel site);

}
