/**
 *
 */
package com.jsl.core.services;

import de.hybris.platform.core.model.user.CustomerModel;

import java.util.Collection;



/**
 * @author manav.magoo
 *
 */
public interface JslAuctionUserGroupService
{
	public Collection<CustomerModel> getCustomers(String uid);

}
