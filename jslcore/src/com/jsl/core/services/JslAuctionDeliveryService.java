/**
 *
 */
package com.jsl.core.services;

import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;

import com.jsl.core.enums.JslProductSourceType;


/**
 * @author manav.magoo
 *
 */
public interface JslAuctionDeliveryService
{

	/**
	 * @param location
	 * @param postalCode
	 * @return
	 */
	public double getDeliveryCharge(String location, String postalCode);

	public ZoneDeliveryModeModel getDeliveryMode(String code);

	/**
	 * @param plantCode
	 * @return
	 */
	JslProductSourceType getProductSource(String plantCode);

}
