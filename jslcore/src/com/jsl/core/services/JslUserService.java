/**
 *
 */
package com.jsl.core.services;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.List;


/**
 * @author manav.magoo
 *
 */
public interface JslUserService extends UserService
{
	List<UserModel> getLiveUsers();

	/**
	 * @param userId
	 * @param active
	 */
	void setLogin(String userId, boolean active);


	void setLastLogin(final String userId);
}
