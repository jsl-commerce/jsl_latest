/**
 *
 */
package com.jsl.core.services;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;

import java.util.List;


/**
 * @author manav.magoo
 *
 */
public interface JslProductService extends ProductService
{

	public List<ProductModel> getAuctionProductsForIds(List<String> productsId, CatalogVersionModel catalogVersion);
}
