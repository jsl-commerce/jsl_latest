/**
 *
 */
package com.jsl.core.services.impl;

import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;

import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.fest.util.Collections;
import org.springframework.beans.factory.annotation.Required;

import com.jsl.core.constants.JslCoreConstants;
import com.jsl.core.daos.JslUserOTPDao;
import com.jsl.core.dto.JslOTPGenerationDto;
import com.jsl.core.dto.JslOTPValidationDto;
import com.jsl.core.enums.OTPGeneratedForEnum;
import com.jsl.core.enums.OTPStatus;
import com.jsl.core.enums.OTPUserIdentificationType;
import com.jsl.core.event.JslAuctionOTPGenerationEvent;
import com.jsl.core.model.JSLUserOTPGenerationModel;
import com.jsl.core.services.JslAuctionService;
import com.jsl.core.services.JslUserOTPService;


/**
 * @author suyash.trivedi
 *
 */
public class JslUserOTPServiceImpl implements JslUserOTPService
{
	private JslUserOTPDao jslUserOTPDao;
	private EventService eventService;
	private SiteConfigService siteConfigService;
	private BaseStoreService baseStoreService;
	private BaseSiteService baseSiteService;
	private CommonI18NService commonI18NService;
	private JslAuctionService jslAuctionService;
	private UserService userService;
	private ModelService modelService;

	private static final String FALSE = String.valueOf("false");
	private static final String REASON_PARAM = "reason";
	private static final String IS_VALID_PARAM = "isValid";
	private static final Logger LOG = Logger.getLogger(JslUserOTPServiceImpl.class);

	protected AbstractCommerceUserEvent initializeEvent(final AbstractCommerceUserEvent event)
	{
		event.setSite((getBaseSiteService().getCurrentBaseSite() != null) ? getBaseSiteService().getCurrentBaseSite()
				: getBaseSiteService().getBaseSiteForUID(JslCoreConstants.DEFAULTSITE));
		event.setBaseStore((getBaseStoreService().getCurrentBaseStore() != null) ? getBaseStoreService().getCurrentBaseStore()
				: getBaseStoreService().getBaseStoreForUid(JslCoreConstants.DEFAULTSTORE));
		event.setLanguage(getcommonI18NService().getCurrentLanguage() != null ? getcommonI18NService().getCurrentLanguage()
				: getcommonI18NService().getLanguage(JslCoreConstants.DEFAULTLANGUAGE));
		event.setCurrency((getcommonI18NService().getCurrentCurrency() != null) ? getcommonI18NService().getCurrentCurrency()
				: getcommonI18NService().getCurrency(JslCoreConstants.DEFAULTCURRENCY));
		return event;
	}


	/**
	 * @param siteConfigService
	 *           the siteConfigService to set
	 */
	public void setSiteConfigService(final SiteConfigService siteConfigService)
	{
		this.siteConfigService = siteConfigService;
	}


	public boolean generateOTP(final OTPGeneratedForEnum generatedModule, final OTPUserIdentificationType identityType,
			final String identityValue, final JslOTPGenerationDto jslOTPGenerationDto)
	{
		boolean processInitiatedSuccessfully = false;
		final UserModel user = userService.getUserForUID(identityValue);
		final List<JSLUserOTPGenerationModel> otpGeneratedList = jslUserOTPDao.getUserAuctionOTPGeneratedRecord(generatedModule,
				identityType, identityValue, jslOTPGenerationDto.getAuctionId());
		if (Collections.isEmpty(otpGeneratedList) && user != null && !user.isLoginDisabled())
		{

			generate(generatedModule, identityType, identityValue, jslOTPGenerationDto, null);
			processInitiatedSuccessfully = true;
			LOG.info(processInitiatedSuccessfully + " JslUserOTPServiceImpl--100");
		}
		else
		{
			if (user != null && user.getResendOtp() && !user.isLoginDisabled())
			{
				generate(generatedModule, identityType, identityValue, jslOTPGenerationDto, otpGeneratedList);
				user.setResendOtp(false);
				modelService.save(user);
				processInitiatedSuccessfully = true;
				LOG.info(MessageFormat.format("OTP is generated and resent again for customer with email id : {0} for Auction ID {1}",
						identityValue, jslOTPGenerationDto.getAuctionId()));
			}
			LOG.info(MessageFormat.format("OTP is already generated and resent option for customer is : {0} for Auction ID {1}",
					user != null ? user.getResendOtp() : "No user Present", jslOTPGenerationDto.getAuctionId()));
		}
		LOG.info(processInitiatedSuccessfully + " JslUserOTPServiceImpl--115");
		return processInitiatedSuccessfully;
	}


	protected void generate(final OTPGeneratedForEnum generatedModule, final OTPUserIdentificationType identityType,
			final String identityValue, final JslOTPGenerationDto jslOTPGenerationDto,
			final List<JSLUserOTPGenerationModel> otpGeneratedList)
	{
		setBaseSiteIfNull();
		final int randomValue = generateRandomOTPNumber();
		final String otp = getOTPCode(randomValue);
		final Calendar currentTime = Calendar.getInstance();

		final Calendar calendar = Calendar.getInstance();

		calendar.setTimeInMillis(
				currentTime.getTimeInMillis() + Integer.parseInt(getSiteConfigService().getProperty(JslCoreConstants.OTP_CODE_TTL)));
		final long expirationTime = calendar.getTimeInMillis();
		if (!Collections.isEmpty(otpGeneratedList))
		{
			jslUserOTPDao.updateUserOTP(otpGeneratedList.get(0), otp, currentTime.getTime(), calendar.getTime());
		}
		else
		{
			if (OTPGeneratedForEnum.AUCTION.equals(generatedModule))
			{
				jslUserOTPDao.insertUserAuctionOTP(generatedModule, otp, currentTime.getTime(), calendar.getTime(), identityType,
						identityValue, jslOTPGenerationDto.getAuctionId());
			}
			else
			{
				jslUserOTPDao.insertUserOTP(generatedModule, otp, currentTime.getTime(), calendar.getTime(), identityType,
						identityValue);
			}

		}

		if (OTPGeneratedForEnum.AUCTION.equals(generatedModule))
		{
			jslOTPGenerationDto.setOtpCode(getOTPCode(randomValue));
			jslOTPGenerationDto.setExpirationTime(getExpirationTime(expirationTime));
			getEventService().publishEvent(initializeEvent(new JslAuctionOTPGenerationEvent(jslOTPGenerationDto)));
		}

	}


	/**
	 *
	 */
	private void setBaseSiteIfNull()
	{
		if (getBaseSiteService().getCurrentBaseSite() == null)
		{
			getBaseSiteService().setCurrentBaseSite(getBaseSiteService().getBaseSiteForUID(JslCoreConstants.DEFAULTSITE), true);
		}
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, String> validateOTP(final String otp, final OTPUserIdentificationType identityType,
			final String identityValue, final OTPGeneratedForEnum generatedModule, final long enteredOTPTime,
			final JslOTPValidationDto jslOTPValidationDto)
	{
		final List<JSLUserOTPGenerationModel> otpGeneratedModuleList;
		if (OTPGeneratedForEnum.AUCTION.equals(generatedModule))
		{
			otpGeneratedModuleList = getJslUserOTPDao().getUserAuctionOTPGeneratedRecord(generatedModule, identityType,
					identityValue, jslOTPValidationDto.getAuctionId());
		}
		else
		{
			otpGeneratedModuleList = getJslUserOTPDao().getUserOTPGeneratedRecord(generatedModule, identityType, identityValue);
		}
		final Map<String, String> responseStatusMap = new HashMap<>();

		if (Collections.isEmpty(otpGeneratedModuleList))
		{
			LOG.debug(MessageFormat.format(
					"Active record for OTP is not found for user with identityType : {0}, identityValue : {1}, for module : {2}",
					identityType, identityValue, generatedModule));
			responseStatusMap.put(IS_VALID_PARAM, FALSE);
			responseStatusMap.put(REASON_PARAM, "notfound");

			return responseStatusMap;
		}

		else if (otpGeneratedModuleList.size() > 1)
		{
			throw new AmbiguousIdentifierException(MessageFormat.format(
					"Duplicate records exist in JslUserOTPGeneration with identityType : {0}, identityValue : {1}, for module : {2}",
					identityType, identityValue, generatedModule));
		}


		final JSLUserOTPGenerationModel otpGeneratedModule = otpGeneratedModuleList.get(0);

		/*
		 * if (Integer.parseInt(Config.getParameter(JslCoreConstants.OTP_CODE_MAX_ATTEMPTS)) <= otpGeneratedModule
		 * .getVerificationAttempts().intValue() + 1) { LOG.debug(MessageFormat.format(
		 * "Max attempts exceeded for OTP authentication with identityType : {0}, identityValue : {1}, for module : {2}" ,
		 * identityType, identityValue, generatedModule)); responseStatusMap.put(IS_VALID_PARAM, FALSE);
		 * responseStatusMap.put(REASON_PARAM, "maxattemptsexceeded"); } else
		 */
		if (otp.equals(otpGeneratedModule.getEncodedOTP()))
		{
			final Calendar calendar = Calendar.getInstance();
			calendar.setTime(otpGeneratedModule.getOtpExpirationTime());
			if (calendar.getTimeInMillis() >= enteredOTPTime)
			{
				LOG.debug(MessageFormat.format("OTP authenticated with identityType : {0}, identityValue : {1}, for module : {2}",
						identityType, identityValue, generatedModule));
				jslUserOTPDao.updateOTPStatus(otpGeneratedModule, OTPStatus.ACCEPTED.getCode());
				responseStatusMap.put(IS_VALID_PARAM, "true");
			}
			else
			{
				LOG.debug(MessageFormat.format("OTP expired for identityType : {0}, identityValue : {1}, for module : {2}",
						identityType, identityValue, generatedModule));
				jslUserOTPDao.expireUserOTPGeneratedRecord(otpGeneratedModule);
				jslUserOTPDao.updateOTPStatus(otpGeneratedModule, OTPStatus.EXPIRED.getCode());
				responseStatusMap.put(IS_VALID_PARAM, FALSE);
				responseStatusMap.put(REASON_PARAM, "expired");
			}
		}
		else
		{
			LOG.debug(MessageFormat.format("Invalid OTP entered for identityType : {0}, identityValue : {1}, for module : {2}",
					identityType, identityValue, generatedModule));
			jslUserOTPDao.incrementUserOTPAttempts(otpGeneratedModule);
			responseStatusMap.put(IS_VALID_PARAM, FALSE);
			responseStatusMap.put(REASON_PARAM, "invalid");
		}

		return responseStatusMap;
	}

	@Override
	public JSLUserOTPGenerationModel getUserAuctionOTPGeneratedRecord(final OTPUserIdentificationType identityType,
			final String identityValue, final OTPGeneratedForEnum generatedModule, final JslOTPValidationDto jslOTPValidationDto)
	{
		final List<JSLUserOTPGenerationModel> otpGeneratedModuleList = getJslUserOTPDao()
				.getUserAuctionOTPGeneratedRecord(generatedModule, identityType, identityValue, jslOTPValidationDto.getAuctionId());

		if (!CollectionUtils.isEmpty(otpGeneratedModuleList))
		{
			return otpGeneratedModuleList.get(0);
		}
		else
		{
			return null;
		}
	}

	@Override
	public JSLUserOTPGenerationModel getUserAuctionOTPGeneratedRecordByOTPStatus(final OTPUserIdentificationType identityType,
			final String identityValue, final OTPGeneratedForEnum generatedModule, final JslOTPValidationDto jslOTPValidationDto,
			final String status)
	{
		final List<JSLUserOTPGenerationModel> otpGeneratedModuleList = getJslUserOTPDao()
				.getUserAuctionOTPGeneratedRecordByOTPStatus(generatedModule, identityType, identityValue,
						jslOTPValidationDto.getAuctionId(), status);

		if (!CollectionUtils.isEmpty(otpGeneratedModuleList))
		{
			return otpGeneratedModuleList.get(0);
		}
		else
		{
			return null;
		}
	}

	/**
	 * @return
	 */
	private EventService getEventService()
	{
		return eventService;
	}

	/**
	 * @param eventService
	 *           the eventService to set
	 */
	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}


	/**
	 * @param expirationTime
	 * @return
	 */
	private String getExpirationTime(final long expirationTime)
	{
		final Date expirationDateTime = new Date(expirationTime);

		return expirationDateTime.toString();
	}


	/**
	 * @return
	 */
	private SiteConfigService getSiteConfigService()
	{
		return siteConfigService;
	}

	/**
	 * @param randomValue
	 * @return
	 */
	private String getOTPCode(final int randomValue)
	{
		return String.format("%06d", Integer.valueOf(randomValue));
	}

	/**
	 * @return
	 */
	private int generateRandomOTPNumber()
	{
		final Random random = new Random();
		return random.nextInt(999999);
	}

	/**
	 * @return the jslAuctionService
	 */
	public JslAuctionService getJslAuctionService()
	{
		return jslAuctionService;
	}

	/**
	 * @param jslAuctionService
	 *           the jslAuctionService to set
	 */
	@Required
	public void setJslAuctionService(final JslAuctionService jslAuctionService)
	{
		this.jslAuctionService = jslAuctionService;
	}

	/**
	 * @return the commonI18NService
	 */
	public CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	/**
	 * @param commonI18NService
	 *           the commonI18NService to set
	 */
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	/**
	 * @param baseStoreService
	 *           the baseStoreService to set
	 */
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}

	/**
	 * @param baseSiteService
	 *           the baseSiteService to set
	 */
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}

	/**
	 * @return the jslUserOTPDao
	 */
	public JslUserOTPDao getJslUserOTPDao()
	{
		return jslUserOTPDao;
	}

	/**
	 * @param jslUserOTPDao
	 *           the jslUserOTPDao to set
	 */
	public void setJslUserOTPDao(final JslUserOTPDao jslUserOTPDao)
	{
		this.jslUserOTPDao = jslUserOTPDao;
	}




	/**
	 * @return
	 */
	private BaseStoreService getBaseStoreService()
	{
		// XXX Auto-generated method stub
		return baseStoreService;
	}

	/**
	 * @return
	 */
	private BaseSiteService getBaseSiteService()
	{
		// XXX Auto-generated method stub
		return baseSiteService;
	}

	/**
	 * @return
	 */
	private CommonI18NService getcommonI18NService()
	{
		// XXX Auto-generated method stub
		return commonI18NService;
	}


	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}


	/**
	 * @param userService
	 *           the userService to set
	 */
	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}


	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}


	/**
	 * @param modelService
	 *           the modelService to set
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}


}
