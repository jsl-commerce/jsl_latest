/**
 *
 */
package com.jsl.core.services.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.impl.DefaultProductService;

import java.util.List;

import com.jsl.core.daos.JslProductDao;
import com.jsl.core.services.JslProductService;


/**
 * @author manav.magoo
 *
 */
public class JslProductServiceImpl extends DefaultProductService implements JslProductService
{

	JslProductDao jslProductDao;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.jsl.core.services.JslProductService#getProductsForIds(java.util.List)
	 */
	@Override
	public List<ProductModel> getAuctionProductsForIds(final List<String> productsId, final CatalogVersionModel catalogVersion)
	{
		// XXX Auto-generated method stub
		return getJslProductDao().findAuctionProductsForIds(productsId, catalogVersion);
	}

	/**
	 * @return the jslProductDao
	 */
	public JslProductDao getJslProductDao()
	{
		return jslProductDao;
	}

	/**
	 * @param jslProductDao
	 *           the jslProductDao to set
	 */
	public void setJslProductDao(final JslProductDao jslProductDao)
	{
		this.jslProductDao = jslProductDao;
	}

}
