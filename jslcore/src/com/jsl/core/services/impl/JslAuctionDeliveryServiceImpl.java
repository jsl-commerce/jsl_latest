/**
 *
 */
package com.jsl.core.services.impl;

import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.util.Config;

import java.util.Arrays;
import java.util.List;

import com.jsl.core.daos.JslAuctionDeliveryDao;
import com.jsl.core.enums.JslProductSourceType;
import com.jsl.core.services.JslAuctionDeliveryService;


/**
 * @author manav.magoo
 *
 */
public class JslAuctionDeliveryServiceImpl implements JslAuctionDeliveryService
{
	JslAuctionDeliveryDao jslAuctionDeliveryDao;

	private static final String JSL_PRODUCT_YARD_CODE_KEY = "jsl.product.yard.codes";
	private static final String JSL_PRODUCT_PLANT_CODE_KEY = "jsl.product.plant.codes";

	@Override
	public double getDeliveryCharge(final String location, final String postalCode)
	{
		return getJslAuctionDeliveryDao().getDeliveryCharge(location, postalCode);

	}

	@Override
	public JslProductSourceType getProductSource(final String plantCode)
	{
		final List<String> yardList = Arrays.asList(Config.getParameter(JSL_PRODUCT_YARD_CODE_KEY).split(","));
		final List<String> plantList = Arrays.asList(Config.getParameter(JSL_PRODUCT_PLANT_CODE_KEY).split(","));

		if (yardList.stream().anyMatch(s -> plantCode.startsWith(s)))
		{
			return JslProductSourceType.WAREHOUSE;
		}
		else if (plantList.stream().anyMatch(s -> plantCode.startsWith(s)))
		{
			return JslProductSourceType.PLANT;
		}
		return JslProductSourceType.PLANT;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.jsl.core.services.JslAuctionDeliveryService#getDeliveryMode(java.lang.String)
	 */
	@Override
	public ZoneDeliveryModeModel getDeliveryMode(final String code)
	{
		// XXX Auto-generated method stub
		return getJslAuctionDeliveryDao().getDeliveryMode(code);
	}

	/**
	 * @return the jslAuctionDeliveryDao
	 */
	public JslAuctionDeliveryDao getJslAuctionDeliveryDao()
	{
		return jslAuctionDeliveryDao;
	}

	/**
	 * @param jslAuctionDeliveryDao
	 *           the jslAuctionDeliveryDao to set
	 */
	public void setJslAuctionDeliveryDao(final JslAuctionDeliveryDao jslAuctionDeliveryDao)
	{
		this.jslAuctionDeliveryDao = jslAuctionDeliveryDao;
	}
}
