/**
 *
 */
package com.jsl.core.services.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BCustomerService;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.jsl.core.constants.JslAuctionEnum;
import com.jsl.core.daos.JslBidEventDao;
import com.jsl.core.dto.BidEventData;
import com.jsl.core.dto.DefaultBundleCommonDto;
import com.jsl.core.event.JslAuctionBidPlacedEvent;
import com.jsl.core.exceptions.JslAuctionException;
import com.jsl.core.model.AuctionEventModel;
import com.jsl.core.services.JslBidEventService;
import com.jsl.core.services.JslProxyBidRuleService;
import com.jsl.core.util.JslB2bUnitUtil;
import com.jsl.jslConfigurableBundleService.model.BidEventModel;
import com.jsl.jslConfigurableBundleService.model.ProxyBidRuleModel;


public class JslBidEventServiceImpl implements JslBidEventService
{

	@Autowired
	private EventService eventService;
	private JslBidEventDao jslBidEventDao;
	private UserService userService;
	private B2BCustomerService b2bCustomerService;
	private KeyGenerator bidEventKeyGenerator;
	private ModelService modelService;
	private JslProxyBidRuleService jslProxyBidRuleService;
	private Converter<BidEventModel, BidEventData> bidEventConverter;
	private static String flowName = "placeBid";
	private static final String DEFAULT_B2BUNIT_CODE = "default.b2b.unit.codes";

	private static final Logger LOG = Logger.getLogger(JslBidEventServiceImpl.class);

	@Override
	public List<DefaultBundleCommonDto> getMaxBidForCurrentUser(final AuctionEventModel auctionEvent)
	{
		List<BidEventModel> bidEventList = null;
		final List<DefaultBundleCommonDto> bidEventDtoList = new ArrayList<>();
		try
		{
			bidEventList = getJslBidEventDao().getMaxBidForCurrentUser(auctionEvent, getUserService().getCurrentUser());
			convertBidEventModelToDto(bidEventList, bidEventDtoList);
		}
		catch (final Exception e)
		{
			LOG.error("Exception in JslBidEventServiceImpl.getMaxBidForCurrentUser() max bid price for current user", e);
		}

		return bidEventDtoList;
	}

	@Override
	public List<DefaultBundleCommonDto> getAllMaxBidPrice(final AuctionEventModel auctionEvent)
	{
		List<BidEventModel> bidEventList = null;
		final List<DefaultBundleCommonDto> bidEventDtoList = new ArrayList<>();
		try
		{
			bidEventList = getJslBidEventDao().getAllMaxBidPrice(auctionEvent);
			convertBidEventModelToDto(bidEventList, bidEventDtoList);
		}
		catch (final Exception e)
		{
			LOG.error("Exception in JslBidEventServiceImpl.getAllMaxBidPrice()", e);
		}

		return bidEventDtoList;
	}

	@Override
	public List<BidEventModel> getWinBidList(final AuctionEventModel auctionEvent)
	{
		List<BidEventModel> bidEventList = null;
		bidEventList = getJslBidEventDao().getAllMaxBidPrice(auctionEvent);

		return removeDuplicateLotIfProxyIsH1(bidEventList);
	}

	@Override
	public boolean placeBid(final BundleTemplateModel bundleTemplate, final String bidPrice)
	{
		boolean isBidPlaced = false;
		try
		{
			final String maxBidForBundle = getJslBidEventDao().getMaxBidPriceForBundle(bundleTemplate);
			final List<ProxyBidRuleModel> proxyBidList = getJslProxyBidRuleService().getAllProxyBidPrice(bundleTemplate);
			isBidValid(bundleTemplate, bidPrice, maxBidForBundle, proxyBidList);
			isBidPlaced = placeBidBasedOnValidation(bundleTemplate, bidPrice);
		}
		catch (final JslAuctionException ex)
		{
			if (ex.getCode().equals("114"))
			{
				placeBidBasedOnValidation(bundleTemplate, bidPrice);
			}
			throw ex;
		}
		catch (final Exception e)
		{
			LOG.error(MessageFormat.format("Exception in placing bid on lot ID: {0} auction id: {1} for userid: {2}",
					bundleTemplate.getId(), bundleTemplate.getAuctionEvent().getAuctionId(),
					getUserService().getCurrentUser().getUid()));
			throw new JslAuctionException(JslAuctionEnum.INTERNAL_SERVER_ERROR_CODE.getResponseCode(),
					JslAuctionEnum.INTERNAL_SERVER_ERROR_CODE.getResponseDescription(), flowName, bundleTemplate);
		}

		return isBidPlaced;
	}

	private boolean placeBidBasedOnValidation(final BundleTemplateModel bundleTemplate, final String bidPrice)
	{
		boolean isBidPlaced;
		final BidEventModel bidEvent = getModelService().create(BidEventModel.class);
		bidEvent.setBidId(getBidEventUniqueId());
		bidEvent.setBidPrice(new BigDecimal(bidPrice));
		bidEvent.setBundleTemplate(bundleTemplate);
		bidEvent.setAuctionEvent(bundleTemplate.getAuctionEvent());
		bidEvent.setCreatedOn(new Date(System.currentTimeMillis()));
		bidEvent.setCreatedBy(getUserService().getCurrentUser());
		bidEvent.setIsProxyBid(Boolean.FALSE);

		getModelService().save(bidEvent);

		isBidPlaced = true;
		getEventService().publishEvent(new JslAuctionBidPlacedEvent(bundleTemplate, flowName));

		return isBidPlaced;
	}

	@Override
	public List<BidEventModel> getAllBidHistoryForAuction(final List<AuctionEventModel> auctionEventList)
	{
		return getJslBidEventDao().getAllBidHistoryForAuction(auctionEventList);
	}

	@Override
	public Map<String, Object> getAllBidHistoryForLot(final DefaultBundleCommonDto lotDetails)
	{
		List<BidEventModel> bidEventList = null;
		final Map<String, Object> lotDetailsMap = new HashMap<>();
		final List<String> bidUserList = new ArrayList<>();
		bidEventList = getAllBidHistoryForBundle(lotDetails);
		final List<BidEventData> bidEventDataList = sortBidList(bidEventList);

		for (final BidEventModel bidEvent : bidEventList)
		{
			try
			{
				bidUserList.add(((B2BCustomerModel) bidEvent.getCreatedBy()).getOriginalUid());
				final B2BCustomerModel user = (B2BCustomerModel) bidEvent.getCreatedBy();

				final B2BUnitModel parentb2bUnit = JslB2bUnitUtil.getParentB2bUnit(user);

				if (parentb2bUnit != null)
				{
					lotDetailsMap.put(parentb2bUnit.getDisplayName(),
							Collections.frequency(bidUserList, ((B2BCustomerModel) bidEvent.getCreatedBy()).getOriginalUid()));
				}
				else
				{
					lotDetailsMap.put(((B2BCustomerModel) bidEvent.getCreatedBy()).getOriginalUid(),
							Collections.frequency(bidUserList, ((B2BCustomerModel) bidEvent.getCreatedBy()).getOriginalUid()));
				}
			}
			catch (final Exception e)
			{
				LOG.error("B2BUnit not found for user" + bidEvent.getCreatedBy());
			}
		}
		if (!CollectionUtils.isEmpty(bidEventDataList))
		{
			final B2BCustomerModel user = ((B2BCustomerModel) bidEventList.get(bidEventList.size() - 1).getCreatedBy());
			final B2BUnitModel parentb2bUnit = JslB2bUnitUtil.getParentB2bUnit(user);
			if (parentb2bUnit != null)
			{
				lotDetailsMap.put("h1Bidder", parentb2bUnit.getDisplayName());
			}
			else
			{
				lotDetailsMap.put("h1Bidder",
						((B2BCustomerModel) bidEventList.get(bidEventList.size() - 1).getCreatedBy()).getOriginalUid());
			}
		}
		lotDetailsMap.put("totalBidCount", bidEventDataList.size());

		return lotDetailsMap;
	}

	@Override
	public List<BidEventModel> getAllBidHistoryForBundle(final DefaultBundleCommonDto lotDetails)
	{
		final List<BidEventModel> bidEventList = getJslBidEventDao().getAllBidHistoryForLot(lotDetails.getAuctionEventId(),
				lotDetails.getBundleTemplateId());

		return removeDuplicateBidPrice(bidEventList);
	}

	@Override
	public List<BidEventModel> getLastBidEventForBundle(final String bundleId)
	{
		return getJslBidEventDao().getLastBidEventForBundle(bundleId);
	}

	@Override
	public List<BidEventModel> getLostAuctionLotsDetailsForCurrentuser(final List<BundleTemplateModel> bundleTemplateList,
			final String flowName)
	{
		List<BidEventModel> bidEventList = null;
		try
		{
			if (flowName.equalsIgnoreCase("completedPage"))
			{
				bidEventList = getJslBidEventDao().getLostAuctionLotsDetailsForCurrentuserForCurrentDate(bundleTemplateList,
						getUserService().getCurrentUser());
			}
			else
			{
				bidEventList = getJslBidEventDao().getLostAuctionLotsDetailsForCurrentuser(bundleTemplateList,
						getUserService().getCurrentUser());
			}
		}
		catch (final Exception e)
		{
			LOG.error("Exception in JslBidEventServiceImpl.getLostAuctionLotsDetailsForCurrentuser()", e);
		}

		return bidEventList;
	}

	@Override
	public List<BidEventModel> getLostAuctionLotsDetailsForCurrentUserByDateRange(
			final List<BundleTemplateModel> bundleTemplateList, final DefaultBundleCommonDto auctionDateRange)
	{
		List<BidEventModel> bidEventList = null;
		try
		{
			bidEventList = getJslBidEventDao().getLostAuctionLotsDetailsForCurrentUserByDateRange(bundleTemplateList,
					getUserService().getCurrentUser(), auctionDateRange.getAuctionStartDate(), auctionDateRange.getAuctionEndDate());
		}
		catch (final Exception e)
		{
			LOG.error("Exception in JslBidEventServiceImpl.getLostAuctionLotsDetailsForCurrentUserByDateRange()", e);
		}

		return bidEventList;
	}

	@Override
	public List<OrderModel> getOrdersForAuction(final List<Integer> auctionIdList)
	{
		List<OrderModel> orderList = null;
		try
		{
			orderList = getJslBidEventDao().getOrdersForAuction(auctionIdList);
		}
		catch (final Exception e)
		{
			LOG.error("Exception in JslBidEventServiceImpl.getOrdersForAuction()", e);
		}

		return orderList;
	}

	@Override
	public List<BidEventModel> getBundleOnWhichUserDidNotPlaceBid(final List<BundleTemplateModel> bundleTemplateList)
	{
		List<BidEventModel> bidEventList = new ArrayList<>();
		try
		{
			bidEventList = getJslBidEventDao().getBundleOnWhichUserDidNotPlaceBid(bundleTemplateList);
		}
		catch (final Exception e)
		{
			LOG.error("Exception in JslBidEventServiceImpl.getBundleOnWhichUserDidNotPlaceBid()", e);
		}

		return bidEventList;
	}

	@Override
	public List<BidEventModel> getBundleOnWhichUserDidNotPlaceBidByDateRange(final List<BundleTemplateModel> bundleTemplateList,
			final DefaultBundleCommonDto auctionDateRange)
	{
		List<BidEventModel> bidEventList = new ArrayList<>();
		try
		{
			bidEventList = getJslBidEventDao().getBundleOnWhichUserDidNotPlaceBidByDateRange(bundleTemplateList,
					auctionDateRange.getAuctionStartDate(), auctionDateRange.getAuctionEndDate());
		}
		catch (final Exception e)
		{
			LOG.error("Exception in JslBidEventServiceImpl.getBundleOnWhichUserDidNotPlaceBidByDateRange()", e);
		}

		return bidEventList;
	}

	private boolean isBidValid(final BundleTemplateModel bundleTemplate, final String bidPrice, final String maxBidForBundle,
			final List<ProxyBidRuleModel> proxyBidList)
	{
		boolean isBidValid = false;
		if (!bundleTemplate.getAuctionEvent().getAuctionStatus().getCode().equalsIgnoreCase("STARTED"))
		{
			LOG.error(MessageFormat.format(
					"Bid cannot be placed on lot ID: {0} auction id: {1} as auction status is {2} for userid: {3}",
					bundleTemplate.getId(), bundleTemplate.getAuctionEvent().getAuctionId(),
					bundleTemplate.getAuctionEvent().getAuctionStatus().getCode(), getUserService().getCurrentUser().getUid()));

			throw new JslAuctionException(JslAuctionEnum.AUCTION_NOT_IN_STARTED_STATE.getResponseCode(),
					JslAuctionEnum.AUCTION_NOT_IN_STARTED_STATE.getResponseDescription(), flowName, bundleTemplate);
		}
		if (bundleTemplate.getMaxLotPrice() != null && bundleTemplate.getMaxLotPrice().doubleValue() >= Double.valueOf(bidPrice))
		{
			if ((StringUtils.hasText(maxBidForBundle) && Double.valueOf(bidPrice) <= Double.valueOf(maxBidForBundle)))
			{
				LOG.error(MessageFormat.format(
						"Bid price {0} cannot be less than current H1 price: {1} lot ID: {2} auction id: {3} for userid: {4}", bidPrice,
						maxBidForBundle, bundleTemplate.getId(), bundleTemplate.getAuctionEvent().getAuctionId(),
						getUserService().getCurrentUser().getUid()));

				throw new JslAuctionException(JslAuctionEnum.BID_PRICE_LESS_THAN_H1_ERROR.getResponseCode(),
						JslAuctionEnum.BID_PRICE_LESS_THAN_H1_ERROR.getResponseDescription(), flowName, bundleTemplate);
			}
			else if ((StringUtils.hasText(maxBidForBundle) && (bundleTemplate.getBaseBidPrice() != null
					&& Double.valueOf(bidPrice) <= bundleTemplate.getBaseBidPrice().doubleValue())))
			{
				LOG.error(MessageFormat.format(
						"Bid price {0} cannot be less than lot base price: {1} lot ID: {2} auction id: {3} for userid: {4}", bidPrice,
						bundleTemplate.getBaseBidPrice(), bundleTemplate.getId(), bundleTemplate.getAuctionEvent().getAuctionId(),
						getUserService().getCurrentUser().getUid()));

				throw new JslAuctionException(JslAuctionEnum.BID_PRICE_LESS_THAN_BASEBID_ERROR.getResponseCode(),
						JslAuctionEnum.BID_PRICE_LESS_THAN_BASEBID_ERROR.getResponseDescription(), flowName, bundleTemplate);
			}
			else
			{
				isBidValid = true;
			}
		}
		else
		{
			LOG.error(MessageFormat.format(
					"Bid price {0} cannot be greater than lot max price: {1} lot ID: {2} auction id: {3} for userid: {4}", bidPrice,
					bundleTemplate.getMaxLotPrice(), bundleTemplate.getId(), bundleTemplate.getAuctionEvent().getAuctionId(),
					getUserService().getCurrentUser().getUid()));

			throw new JslAuctionException(JslAuctionEnum.BID_PRICE_EXCEED_MAX_LOT_PRICE_ERROR.getResponseCode(),
					JslAuctionEnum.BID_PRICE_EXCEED_MAX_LOT_PRICE_ERROR.getResponseDescription(), flowName, bundleTemplate);
		}

		final boolean isPresent = proxyBidList.stream()
				.anyMatch(p -> p.getMaxProxyBidPrice().compareTo(new BigDecimal(bidPrice)) == 0);
		if (isPresent)
		{
			LOG.error(MessageFormat.format(
					"Bid price {0} cannot be equal to proxy bid price already set on lot ID: {1} auction id: {2} for userid: {3}",
					bidPrice, bundleTemplate.getId(), bundleTemplate.getAuctionEvent().getAuctionId(),
					getUserService().getCurrentUser().getUid()));

			throw new JslAuctionException(JslAuctionEnum.BID_PRICE_EQUAL_PROXY_PRICE_ERROR.getResponseCode(),
					JslAuctionEnum.BID_PRICE_EQUAL_PROXY_PRICE_ERROR.getResponseDescription(), flowName, bundleTemplate);
		}
		return isBidValid;
	}

	private void convertBidEventModelToDto(final List<BidEventModel> bidEventList,
			final List<DefaultBundleCommonDto> bidEventDtoList)
	{
		for (final BidEventModel bidEvent : bidEventList)
		{
			final DefaultBundleCommonDto defaultBundleCommonDto = new DefaultBundleCommonDto();
			defaultBundleCommonDto.setAuctionEventId(String.valueOf(bidEvent.getAuctionEvent().getAuctionId()));
			defaultBundleCommonDto.setBundleTemplateId(bidEvent.getBundleTemplate().getId());
			defaultBundleCommonDto.setBidPrice(String.valueOf(bidEvent.getBidPrice()));
			defaultBundleCommonDto.setH1Bidder(String.valueOf(bidEvent.getCreatedBy().getPk()));
			if (bidEvent.getIsProxyBid() != null)
			{
				defaultBundleCommonDto.setProxyBid(bidEvent.getIsProxyBid());
			}
			else
			{
				defaultBundleCommonDto.setProxyBid(Boolean.FALSE);

			}
			bidEventDtoList.add(defaultBundleCommonDto);

		}

		if (!CollectionUtils.isEmpty(bidEventDtoList))
		{
			final ListIterator<DefaultBundleCommonDto> bidEventDtoIterator = bidEventDtoList.listIterator();
			String bundleId = "";
			String currentBundleId;
			while (bidEventDtoIterator.hasNext())
			{
				currentBundleId = bidEventDtoIterator.next().getBundleTemplateId();
				if (bundleId.equalsIgnoreCase(currentBundleId))
				{
					bidEventDtoIterator.remove();
				}
				bundleId = currentBundleId;
			}
		}
	}

	private List<BidEventData> sortBidList(final List<BidEventModel> bidEventList)
	{
		final List<BidEventData> bidEventDataList = new ArrayList<>();
		try
		{
			bidEventList.forEach(bidEventModel -> {
				bidEventDataList.add(getBidEventConverter().convert(bidEventModel));
			});
			bidEventDataList.sort((final BidEventData p1, final BidEventData p2) -> p1.getBidPrice().compareTo(p2.getBidPrice()));
		}
		catch (final Exception e)
		{
			LOG.error("Exception while sorting list JslBidEventServiceImpl.class sortBidList()");
		}

		return bidEventDataList;
	}

	private B2BCustomerModel getB2BcustById(final String Uid)
	{
		B2BCustomerModel user = null;
		try
		{
			user = (B2BCustomerModel) getB2bCustomerService().getUserForUID(Uid);
		}
		catch (final Exception e)
		{
			return user;
		}

		return user;
	}

	private List<BidEventModel> removeDuplicateLotIfProxyIsH1(final List<BidEventModel> bidEventList)
	{
		String bundleId = "";
		String currentBundleId;
		final List<BidEventModel> filteredBidEventList = new ArrayList<>();
		for (final BidEventModel bidEvent : bidEventList)
		{
			currentBundleId = bidEvent.getBundleTemplate().getId();
			if (!bundleId.equalsIgnoreCase(currentBundleId))
			{
				filteredBidEventList.add(bidEvent);
			}
			bundleId = currentBundleId;
		}
		return filteredBidEventList;
	}

	private List<BidEventModel> removeDuplicateBidPrice(final List<BidEventModel> bidEventList)
	{
		String bidPrice = "";
		String currentBidPrice;
		final List<BidEventModel> filteredBidEventList = new ArrayList<>();
		for (final BidEventModel bidEvent : bidEventList)
		{
			currentBidPrice = bidEvent.getBidPrice().toString();
			if (!bidPrice.equalsIgnoreCase(currentBidPrice))
			{
				filteredBidEventList.add(bidEvent);
			}
			bidPrice = currentBidPrice;
		}
		return filteredBidEventList;
	}

	private String getBidEventUniqueId()
	{
		return (String) getBidEventKeyGenerator().generate();
	}

	public JslBidEventDao getJslBidEventDao()
	{
		return jslBidEventDao;
	}

	public void setJslBidEventDao(final JslBidEventDao jslBidEventDao)
	{
		this.jslBidEventDao = jslBidEventDao;
	}

	public UserService getUserService()
	{
		return userService;
	}

	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	public KeyGenerator getBidEventKeyGenerator()
	{
		return bidEventKeyGenerator;
	}

	public void setBidEventKeyGenerator(final KeyGenerator bidEventKeyGenerator)
	{
		this.bidEventKeyGenerator = bidEventKeyGenerator;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public JslProxyBidRuleService getJslProxyBidRuleService()
	{
		return jslProxyBidRuleService;
	}

	public void setJslProxyBidRuleService(final JslProxyBidRuleService jslProxyBidRuleService)
	{
		this.jslProxyBidRuleService = jslProxyBidRuleService;
	}

	public EventService getEventService()
	{
		return eventService;
	}

	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}

	public Converter<BidEventModel, BidEventData> getBidEventConverter()
	{
		return bidEventConverter;
	}

	public void setBidEventConverter(final Converter<BidEventModel, BidEventData> bidEventConverter)
	{
		this.bidEventConverter = bidEventConverter;
	}

	public B2BCustomerService getB2bCustomerService()
	{
		return b2bCustomerService;
	}

	public void setB2bCustomerService(final B2BCustomerService b2bCustomerService)
	{
		this.b2bCustomerService = b2bCustomerService;
	}

}
