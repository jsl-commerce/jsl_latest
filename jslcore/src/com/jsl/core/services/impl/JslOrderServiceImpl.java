/**
 *
 */
package com.jsl.core.services.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.List;

import com.jsl.core.daos.JslOrderDao;
import com.jsl.core.services.JslOrderService;


/**
 * @author manav.magoo
 *
 */
public class JslOrderServiceImpl implements JslOrderService
{

	JslOrderDao jslOrderDao;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.jsl.core.services.JslOrderService#getAuctionOrders(java.lang.String)
	 */
	@Override
	public List<OrderModel> getAuctionOrders(final int auctionId)
	{
		// XXX Auto-generated method stub
		return getJslOrderDao().getAuctionOrders(auctionId);
	}

	/**
	 * @return the jslOrderDao
	 */
	public JslOrderDao getJslOrderDao()
	{
		return jslOrderDao;
	}

	/**
	 * @param jslOrderDao
	 *           the jslOrderDao to set
	 */
	public void setJslOrderDao(final JslOrderDao jslOrderDao)
	{
		this.jslOrderDao = jslOrderDao;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.jsl.core.services.JslOrderService#getAuctionOrders(java.util.Date, java.util.Date)
	 */
	@Override
	public List<OrderModel> getOrders(final String startDate, final String endDate)
	{
		return jslOrderDao.getOrders(startDate, endDate);
	}

	/* (non-Javadoc)
	 * @see com.jsl.core.services.JslOrderService#getOrdersForTimeInterval(java.lang.String, java.lang.String, de.hybris.platform.core.model.user.UserModel)
	 */
	@Override
	public List<OrderModel> getOrdersForTimeInterval(String timeInterval, String orderType, UserModel user)
	{
		// XXX Auto-generated method stub
		return jslOrderDao.getOrdersForTimeInterval(timeInterval, orderType, user);
	}


}
