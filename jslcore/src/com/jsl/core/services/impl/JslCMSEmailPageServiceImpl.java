/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.jsl.core.services.impl;

import de.hybris.platform.acceleratorservices.email.impl.DefaultCMSEmailPageService;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import com.jsl.core.daos.JslEmailPageDao;
import com.jsl.core.services.JslCMSEmailPageService;


/**
 * Service to handle CMS EmailPage and EmailPageTemplate.
 */
public class JslCMSEmailPageServiceImpl extends DefaultCMSEmailPageService implements JslCMSEmailPageService
{
	private JslEmailPageDao emailPageDao;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.amway.lynxcore.services.LynxCMSEmailPageService#getEmailPageForFrontendTemplate(java.lang.String,
	 * de.hybris.platform.catalog.model.CatalogVersionModel, de.hybris.platform.basecommerce.model.site.BaseSiteModel)
	 */
	@Override
	public EmailPageModel getEmailPageForFrontendTemplate(final String frontendTemplateName,
			final CatalogVersionModel catalogVersion, final BaseSiteModel site)
	{
		ServicesUtil.validateParameterNotNull(site, "Site Should Not be Null");
		return emailPageDao.findEmailPageByFrontendTemplateName(frontendTemplateName, catalogVersion, site);
	}

	/**
	 * @return the emailPageDao
	 */
	@Override
	public JslEmailPageDao getEmailPageDao()
	{
		return emailPageDao;
	}

	/**
	 * @param emailPageDao
	 *           the emailPageDao to set
	 */
	public void setEmailPageDao(final JslEmailPageDao emailPageDao)
	{
		this.emailPageDao = emailPageDao;
	}

}
