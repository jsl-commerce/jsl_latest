/**
 *
 */
package com.jsl.core.services.impl;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.impl.DefaultUserService;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import com.jsl.core.daos.JslUserDao;
import com.jsl.core.services.JslUserService;


/**
 * @author manav.magoo
 *
 */
public class JslUserServiceImpl extends DefaultUserService implements JslUserService
{

	private JslUserDao jslUserDao;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.jsl.core.services.JslUserService#getLiveUsers()
	 */
	@Override
	public List<UserModel> getLiveUsers()
	{
		// XXX Auto-generated method stub
		return getJslUserDao().getLiveUsers();
	}

	/**
	 * @param currentUser
	 */
	@Override
	public void setLogin(final String userId, final boolean active)
	{
		final UserModel user = getUserForUID(userId);
		user.setLogin(active);
		getModelService().save(user);

	}


	/**
	 * @param currentUser
	 */
	@Override
	public void setLastLogin(final String userId)
	{
		final UserModel user = getUserForUID(userId);
		if (!user.getLogin())
		{
			user.setLastLogin(new Date());
			getModelService().save(user);
		}
	}

	/**
	 * @return the jslUserDao
	 */
	public JslUserDao getJslUserDao()
	{
		return jslUserDao;
	}

	/**
	 * @param jslUserDao
	 *           the jslUserDao to set
	 */
	@Required
	public void setJslUserDao(final JslUserDao jslUserDao)
	{
		this.jslUserDao = jslUserDao;
	}

}
