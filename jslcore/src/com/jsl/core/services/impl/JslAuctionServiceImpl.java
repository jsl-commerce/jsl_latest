/**
 *
 */
package com.jsl.core.services.impl;

import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.sql.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.jsl.core.daos.JslAuctionDao;
import com.jsl.core.dto.DefaultBundleCommonDto;
import com.jsl.core.enums.AuctionStatus;
import com.jsl.core.model.AuctionEventModel;
import com.jsl.core.services.JslAuctionService;


public class JslAuctionServiceImpl implements JslAuctionService
{

	private static final Logger LOG = Logger.getLogger(JslAuctionServiceImpl.class);

	private JslAuctionDao jslAuctionDao;
	private ModelService modelService;
	private SiteConfigService siteConfigService;

	public static final String ADDITIONAL_TIME = "lot.additional.time";

	@Override
	public List<AuctionEventModel> getActiveAuctionList()
	{
		return getJslAuctionDao().getActiveAuctionList();
	}

	@Override
	public List<AuctionEventModel> getUpcomingAuctionDetails()
	{
		final Date curDate = new Date(System.currentTimeMillis());

		return getJslAuctionDao().getUpcomingAuctionDetails(curDate);
	}

	@Override
	public AuctionEventModel getAuctionEventForAuctionId(final String auctionEventId)
	{
		return getJslAuctionDao().getAuctionEventForAuctionId(auctionEventId);
	}

	@Override
	public List<AuctionEventModel> getLiveAuctionList()
	{
		final Date curDate = new Date(System.currentTimeMillis());
		return getJslAuctionDao().getLiveAuctionList(curDate);
	}

	@Override
	public List<AuctionEventModel> getAllAuctionList()
	{
		final Date curDate = new Date(System.currentTimeMillis());
		return getJslAuctionDao().getAllAuctionList(curDate);
	}

	@Override
	public List<AuctionEventModel> getAuctions(final AuctionStatus status)
	{
		return getJslAuctionDao().getAuctions(status);
	}

	@Override
	public void updateAuctionStatus(final AuctionEventModel auctionEvent, final AuctionStatus status)
	{
		auctionEvent.setAuctionStatus(status);
		getModelService().save(auctionEvent);
	}

	@Override
	public long updateAuctionEventWithAdditionalTime(final AuctionEventModel auctionEvent)
	{
		long additionalTime = 0;
		final String configurableAdditionalTime = getSiteConfigService().getProperty(ADDITIONAL_TIME);
		try {
			if (auctionEvent != null)
			{
				if (auctionEvent.getAdditionalTime() == null)
				{
					auctionEvent.setAdditionalTime(Long.valueOf(configurableAdditionalTime));
					auctionEvent.setUpdatedOn(System.currentTimeMillis());
				}
				else if (auctionEvent.getAdditionalTime() != null)
				{
					auctionEvent.setAdditionalTime(auctionEvent.getAdditionalTime() + Long.valueOf(configurableAdditionalTime));
					auctionEvent.setUpdatedOn(System.currentTimeMillis());
				}
				additionalTime = auctionEvent.getAdditionalTime();
			}
			getModelService().save(auctionEvent);
		}
		catch (final Exception e)
		{
			LOG.error("Exception in updating AuctionEvent with additional time JslAuctionServiceImpl.updateAuctionEvent()", e);
		}

		return additionalTime;
	}

	@Override
	public List<AuctionEventModel> getAuctionByDateRange(final DefaultBundleCommonDto auctionDateRange)
	{
		return getJslAuctionDao().getAuctionByDateRange(auctionDateRange.getAuctionStartDate(),
				auctionDateRange.getAuctionEndDate());
	}

	@Override
	public List<AuctionEventModel> getAuctionByDate(final DefaultBundleCommonDto auctionDateRange)
	{
		return getJslAuctionDao().getAuctionByDate(auctionDateRange.getAuctionStartDate());
	}

	public JslAuctionDao getJslAuctionDao()
	{
		return jslAuctionDao;
	}

	public void setJslAuctionDao(final JslAuctionDao jslAuctionDao)
	{
		this.jslAuctionDao = jslAuctionDao;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public SiteConfigService getSiteConfigService()
	{
		return siteConfigService;
	}

	public void setSiteConfigService(final SiteConfigService siteConfigService)
	{
		this.siteConfigService = siteConfigService;
	}

}
