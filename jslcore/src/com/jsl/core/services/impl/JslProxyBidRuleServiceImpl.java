package com.jsl.core.services.impl;

import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.jsl.core.constants.JslAuctionEnum;
import com.jsl.core.daos.JslBidEventDao;
import com.jsl.core.daos.JslProxyBidRuleDao;
import com.jsl.core.dto.DefaultBundleCommonDto;
import com.jsl.core.dto.ProxyBidEventData;
import com.jsl.core.enums.AuctionStatus;
import com.jsl.core.event.JslAuctionBidPlacedEvent;
import com.jsl.core.exceptions.JslAuctionException;
import com.jsl.core.model.AuctionEventModel;
import com.jsl.core.services.JslProxyBidRuleService;
import com.jsl.jslConfigurableBundleService.model.BidEventModel;
import com.jsl.jslConfigurableBundleService.model.ProxyBidRuleModel;

public class JslProxyBidRuleServiceImpl implements JslProxyBidRuleService
{

	private static final Logger LOG = Logger.getLogger(JslProxyBidRuleServiceImpl.class);

	@Autowired
	private EventService eventService;
	private JslProxyBidRuleDao jslProxyBidRuleDao;
	private JslBidEventDao jslBidEventDao;
	private UserService userService;
	private KeyGenerator proxyBidEventKeyGenerator;
	private ModelService modelService;
	private Converter<ProxyBidRuleModel, ProxyBidEventData> proxyBidConverter;

	private static String flowName = "placeProxyBid";
	private static final String JSL_AUCTION_EXCEPTION = "JslAuctionException while placing proxy bid for user : {0} on lot: {1} Error Description: {2} ";
	private static final String BASE_EXCEPTION = "Exception in while placing proxy bid for user : {0} on lot: {1} for bidPrice: {2} ";
	private static final String MODALSAVING_EXCEPTION = "ModelSavingException while placing proxy bid for user : {0} on lot: {1} ";
	private static final String AUCTION_CLOSED_EXCEPTION = "Proxy bid cannot be placed on lot ID: {0} auction id: {1} as auction status is {2} for userid: {3}";

	@Override
	public List<DefaultBundleCommonDto> getProxyBidForCurrentUser(final AuctionEventModel auctionEvent)
	{
		List<ProxyBidRuleModel> proxyBidRuleList = null;
		final List<DefaultBundleCommonDto> proxyBidRuleDtoList = new ArrayList<>();
		proxyBidRuleList = getJslProxyBidRuleDao().getAllProxyBidForCurrentUser(auctionEvent, getUserService().getCurrentUser());
		convertProxyBidModelToDto(proxyBidRuleList, proxyBidRuleDtoList);

		return proxyBidRuleDtoList;
	}

	@Override
	public boolean setProxy(final BundleTemplateModel bundleTemplate, final String proxyBidPrice)
	{
		boolean isProxySet = false;
		try
		{
			final BigDecimal proxyBid = new BigDecimal(proxyBidPrice);
			ProxyBidRuleModel proxyBidRule = null;
			proxyBidRule = getJslProxyBidRuleDao().getProxyBidPriceForCurrentUser(bundleTemplate, getUserService().getCurrentUser());
			final String maxBidForBundle = getJslBidEventDao().getMaxBidPriceForBundle(bundleTemplate);
			final List<ProxyBidRuleModel> proxyBidList = getAllProxyBidPrice(bundleTemplate);
			isBidValid(bundleTemplate, maxBidForBundle, proxyBid, proxyBidList);
			if (null != proxyBidRule)
			{
				isProxySet = updateProxyBid(bundleTemplate, proxyBidPrice, proxyBidRule, maxBidForBundle);
			}
			else
			{
				isProxySet = setFirstProxyBid(bundleTemplate, proxyBidPrice, maxBidForBundle);
			}
		}
		catch (final JslAuctionException ex)
		{
			throw ex;
		}
		catch (final Exception e)
		{
			LOG.error(MessageFormat.format("Exception in Setting Proxy Bid on lot ID: {0} auction id: {1} for userid: {2}",
					bundleTemplate.getId(), bundleTemplate.getAuctionEvent().getAuctionId(),
					getUserService().getCurrentUser().getUid()));

			throw new JslAuctionException(JslAuctionEnum.INTERNAL_SERVER_ERROR_CODE.getResponseCode(),
					JslAuctionEnum.INTERNAL_SERVER_ERROR_CODE.getResponseDescription(), flowName, bundleTemplate);
		}

		return isProxySet;
	}

	@Override
	public boolean placeProxyBid(final BundleTemplateModel bundleTemplate, final String bidPrice,
			final boolean fromProxyBidEventListener, final String eventFlowName)
	{
		final boolean isProxyPlaced = false;
		try
		{
			final List<ProxyBidRuleModel> proxyBidRuleList = getJslProxyBidRuleDao().getProxyBidPrice(bundleTemplate);
			if (fromProxyBidEventListener)
			{
				placeProxyBidFromEventListener(bundleTemplate, proxyBidRuleList, eventFlowName);
			}
			else
			{
				placeProxyBidFromRegularFlow(bundleTemplate, bidPrice, isProxyPlaced, proxyBidRuleList);
			}
		}
		catch (final JslAuctionException ex)
		{
			throw ex;
		}
		catch (final Exception e)
		{
			LOG.error("Exception in JslProxyBidRuleServiceImpl.placeProxyBid() Placing Proxy Bid ", e);
		}

		return isProxyPlaced;
	}

	@Override
	public List<ProxyBidRuleModel> getAllProxyBidPrice(final BundleTemplateModel bundleTemplate)
	{
		List<ProxyBidRuleModel> proxyBidRuleList = null;
		proxyBidRuleList = getJslProxyBidRuleDao().getProxyBidPrice(bundleTemplate);

		return proxyBidRuleList;
	}

	/**
	 * placeProxyBidFromEventListener()
	 *
	 * @param bundleTemplate
	 * @param proxyBidRuleList
	 * @param eventFlowName
	 */
	private void placeProxyBidFromEventListener(final BundleTemplateModel bundleTemplate,
			final List<ProxyBidRuleModel> proxyBidRuleList, final String eventFlowName)
	{
		boolean isProxyBidPlacedForAll = false;
		int count;
		while (!isProxyBidPlacedForAll)
		{
			count = 0;
			for (int i = 0; i < proxyBidRuleList.size(); i++)
   		{
				final ProxyBidRuleModel proxyBidRule = proxyBidRuleList.get(i);
				final List<BidEventModel> bidEventList = getJslBidEventDao().getMaxBidEventForProxyByBundle(bundleTemplate);
				final BidEventModel bidEvent = removeDuplicateBidPriceIfProxyIsH1(bidEventList);
				if (bidEvent != null && !bidEvent.getCreatedBy().getUid().equalsIgnoreCase(proxyBidRule.getCreatedBy().getUid()))
				{
					count = placeProxyBidBasedOnCondition(bundleTemplate, proxyBidRuleList, eventFlowName, count, proxyBidRule,
							bidEvent);
				}
   		}
			if (count == 0)
			{
				isProxyBidPlacedForAll = true;
			}
		}
	}

	/**
	 * placeProxyBidFromRegularFlow()
	 * Description : When Bid placed and Proxy is set on lot by other users
	 * @param bundleTemplate
	 * @param bidPrice
	 * @param isProxyPlaced
	 * @param proxyBidRuleList
	 * @return
	 */
	private boolean placeProxyBidFromRegularFlow(final BundleTemplateModel bundleTemplate, final String bidPrice,
			boolean isProxyPlaced,
			final List<ProxyBidRuleModel> proxyBidRuleList)
	{
		for (final ProxyBidRuleModel proxyBidRule : proxyBidRuleList)
		{
			BigDecimal currentBid = null;
			final BigDecimal proxyBidPrice = new BigDecimal(bidPrice);
			if (bundleTemplate.getBaseBidPrice().compareTo(proxyBidPrice) == 0)
			{
				currentBid = proxyBidPrice;
			}
			else
			{
				currentBid = proxyBidPrice.add(proxyBidRule.getIncrementByBidPrice());
			}
			if (proxyBidRule.getMaxProxyBidPrice().max(currentBid).compareTo(proxyBidRule.getMaxProxyBidPrice()) == 0
					&& bundleTemplate.getMaxLotPrice() != null
					&& bundleTemplate.getMaxLotPrice().doubleValue() >= Double.valueOf(bidPrice))
			{
				isProxyPlaced = placeProxyBid(bundleTemplate, proxyBidRule, currentBid);
			}
		}
		getEventService().publishEvent(new JslAuctionBidPlacedEvent(bundleTemplate, flowName));

		return isProxyPlaced;
	}

	/**
	 * @param bundleTemplate
	 * @param proxyBidRuleList
	 * @param eventFlowName
	 * @param count
	 * @param proxyBidRule
	 * @param bidEvent
	 * @return
	 */
	private int placeProxyBidBasedOnCondition(final BundleTemplateModel bundleTemplate,
			final List<ProxyBidRuleModel> proxyBidRuleList, final String eventFlowName, int count,
			final ProxyBidRuleModel proxyBidRule, final BidEventModel bidEvent)
	{
		final BigDecimal currentBid = bidEvent.getBidPrice().add(proxyBidRule.getIncrementByBidPrice());
		if (currentBid.compareTo(proxyBidRule.getMaxProxyBidPrice()) <= 0 && (bundleTemplate.getMaxLotPrice() != null
				&& bundleTemplate.getMaxLotPrice().doubleValue() >= currentBid.doubleValue()))
		{
			placeProxyBid(bundleTemplate, proxyBidRule, currentBid);
			count++;
		}
		else if (bidEvent.getBidPrice().compareTo(proxyBidRule.getMaxProxyBidPrice()) == 0
				&& eventFlowName.equalsIgnoreCase("placeBid"))
		{
			placeProxyBidWhenH1SameAsProxySet(bundleTemplate, proxyBidRule, proxyBidRule.getMaxProxyBidPrice());
			count++;
		}
		else if (bidEvent.getBidPrice().compareTo(proxyBidRule.getMaxProxyBidPrice()) == 0
				&& eventFlowName.equalsIgnoreCase(flowName))
		{
			placeProxyBidWhenMaxProxySameAsCurrentProxy(bundleTemplate, proxyBidRuleList, bidEvent);
			count++;
		}

		return count;
	}

	/**
	 * placeProxyBid()
	 *
	 * @param bundleTemplate
	 * @param proxyBidRule
	 * @param currentBid
	 * @return
	 */
	private boolean placeProxyBid(final BundleTemplateModel bundleTemplate, final ProxyBidRuleModel proxyBidRule,
			final BigDecimal currentBid)
	{
		boolean isProxyPlaced = false;
		try
		{
			final String maxBidForBundle = getJslBidEventDao().getMaxBidPriceForBundle(bundleTemplate);
			final BigDecimal newBid = currentBid;
			isBidValid(bundleTemplate, maxBidForBundle, newBid, null);
			final BidEventModel bidEvent = getModelService().create(BidEventModel.class);
			bidEvent.setBidId(getProxyBidRuleUniqueId());
			bidEvent.setBidPrice(newBid);
			bidEvent.setBundleTemplate(bundleTemplate);
			bidEvent.setAuctionEvent(bundleTemplate.getAuctionEvent());
			bidEvent.setCreatedOn(new Date(System.currentTimeMillis()));
			bidEvent.setCreatedBy(proxyBidRule.getCreatedBy());
			bidEvent.setIsProxyBid(Boolean.TRUE);
			getModelService().save(bidEvent);
			isProxyPlaced = true;
		}
		catch (final JslAuctionException ex)
		{
			LOG.error(MessageFormat.format(JSL_AUCTION_EXCEPTION, proxyBidRule.getCreatedBy(),
					bundleTemplate.getId(), ex.getDescription()));
		}
		catch (final ModelSavingException e)
		{
			LOG.error(MessageFormat.format(MODALSAVING_EXCEPTION, proxyBidRule.getCreatedBy(), bundleTemplate.getId()));

			throw new JslAuctionException(JslAuctionEnum.ERROR_WHILE_SAVING_DATA.getResponseCode(),
					JslAuctionEnum.ERROR_WHILE_SAVING_DATA.getResponseDescription(), flowName, bundleTemplate);
		}
		catch(final Exception ex) {
			LOG.error("placeProxyBid(): "
					+ MessageFormat.format(BASE_EXCEPTION, proxyBidRule.getCreatedBy(), bundleTemplate.getId(), currentBid), ex);
		}

		return isProxyPlaced;
	}

	/**
	 * placeProxyBidWhenH1SameAsProxySet()
	 * Description : place proxy bid when H1 is same as max proxy bid
	 * @param bundleTemplate
	 * @param proxyBidRule
	 * @param currentBid
	 * @return
	 */
	private boolean placeProxyBidWhenH1SameAsProxySet(final BundleTemplateModel bundleTemplate,
			final ProxyBidRuleModel proxyBidRule, final BigDecimal currentBid)
	{
		boolean isProxyPlaced = false;
		try
		{
			if (!bundleTemplate.getAuctionEvent().getAuctionStatus().getCode().equalsIgnoreCase(AuctionStatus.STARTED.getCode()))
			{
				LOG.error(MessageFormat.format(AUCTION_CLOSED_EXCEPTION, bundleTemplate.getId(),
						bundleTemplate.getAuctionEvent().getAuctionId(), bundleTemplate.getAuctionEvent().getAuctionStatus().getCode(),
						getUserService().getCurrentUser().getUid()));

				throw new JslAuctionException(JslAuctionEnum.AUCTION_NOT_IN_STARTED_STATE.getResponseCode(),
						JslAuctionEnum.AUCTION_NOT_IN_STARTED_STATE.getResponseDescription(), flowName, bundleTemplate);
			}
			if (bundleTemplate.getMaxLotPrice() != null && bundleTemplate.getMaxLotPrice().doubleValue() < currentBid.doubleValue())
			{
				LOG.error(MessageFormat.format(
						"Proxy bid price cannot be greater than lot max price: {0} lot ID: {1} auction id: {2} for userid: {3}",
						bundleTemplate.getMaxLotPrice(), bundleTemplate.getId(), bundleTemplate.getAuctionEvent().getAuctionId(),
						getUserService().getCurrentUser().getUid()));

				throw new JslAuctionException(JslAuctionEnum.BID_PRICE_EXCEED_MAX_LOT_PRICE_ERROR.getResponseCode(),
						JslAuctionEnum.BID_PRICE_EXCEED_MAX_LOT_PRICE_ERROR.getResponseDescription(), flowName, bundleTemplate);
			}
			final BigDecimal newBid = currentBid;
			final BidEventModel bidEvent = getModelService().create(BidEventModel.class);
			bidEvent.setBidId(getProxyBidRuleUniqueId());
			bidEvent.setBidPrice(newBid);
			bidEvent.setBundleTemplate(bundleTemplate);
			bidEvent.setAuctionEvent(bundleTemplate.getAuctionEvent());
			bidEvent.setCreatedOn(new Date(System.currentTimeMillis()));
			bidEvent.setCreatedBy(proxyBidRule.getCreatedBy());
			bidEvent.setIsProxyBid(Boolean.TRUE);
			getModelService().save(bidEvent);
			isProxyPlaced = true;
		}
		catch (final JslAuctionException ex)
		{
			LOG.error(MessageFormat.format(JSL_AUCTION_EXCEPTION, proxyBidRule.getCreatedBy(), bundleTemplate.getId(),
					ex.getDescription()));
		}
		catch (final ModelSavingException e)
		{
			LOG.error(MessageFormat.format(MODALSAVING_EXCEPTION, proxyBidRule.getCreatedBy(), bundleTemplate.getId()));

			throw new JslAuctionException(JslAuctionEnum.ERROR_WHILE_SAVING_DATA.getResponseCode(),
					JslAuctionEnum.ERROR_WHILE_SAVING_DATA.getResponseDescription(), flowName, bundleTemplate);
		}
		catch (final Exception ex)
		{
			LOG.error("placeProxyBidWhenH1SameAsProxySet(): "
					+ MessageFormat.format(BASE_EXCEPTION, proxyBidRule.getCreatedBy(), bundleTemplate.getId(), currentBid), ex);
		}

		return isProxyPlaced;
	}

	/**
	 * placeProxyBidWhenMaxProxySameAsCurrentProxy()
	 * Description: Place proxy bid for another user when max proxy is equal to current H1 set by proxy bid
	 *
	 * @param bundleTemplate
	 * @param proxyBidRuleList
	 * @param prevBidEvent
	 * @return
	 */
	private boolean placeProxyBidWhenMaxProxySameAsCurrentProxy(final BundleTemplateModel bundleTemplate,
			final List<ProxyBidRuleModel> proxyBidRuleList, final BidEventModel prevBidEvent)
	{
		boolean isProxyPlaced = false;
		UserModel createdBy = null;
		BigDecimal currentBid = null;
		try
		{
			for (final ProxyBidRuleModel proxyBidRule : proxyBidRuleList)
			{
				if (prevBidEvent.getCreatedBy().getUid().equals(proxyBidRule.getCreatedBy().getUid()))
				{
					currentBid = prevBidEvent.getBidPrice().add(proxyBidRule.getIncrementByBidPrice());
					createdBy = proxyBidRule.getCreatedBy();
				}
			}
			if (!bundleTemplate.getAuctionEvent().getAuctionStatus().getCode().equalsIgnoreCase("STARTED"))
			{
				LOG.error(MessageFormat.format(AUCTION_CLOSED_EXCEPTION,
						bundleTemplate.getId(), bundleTemplate.getAuctionEvent().getAuctionId(),
						bundleTemplate.getAuctionEvent().getAuctionStatus().getCode(), getUserService().getCurrentUser().getUid()));

				throw new JslAuctionException(JslAuctionEnum.AUCTION_NOT_IN_STARTED_STATE.getResponseCode(),
						JslAuctionEnum.AUCTION_NOT_IN_STARTED_STATE.getResponseDescription(), flowName, bundleTemplate);
			}
			if (bundleTemplate.getMaxLotPrice() != null && bundleTemplate.getMaxLotPrice().doubleValue() < currentBid.doubleValue())
			{
				LOG.error(MessageFormat.format(
						"Proxy bid price cannot be greater than lot max price: {0} lot ID: {1} auction id: {2} for userid: {3}",
						bundleTemplate.getMaxLotPrice(), bundleTemplate.getId(), bundleTemplate.getAuctionEvent().getAuctionId(),
						getUserService().getCurrentUser().getUid()));

				throw new JslAuctionException(JslAuctionEnum.BID_PRICE_EXCEED_MAX_LOT_PRICE_ERROR.getResponseCode(),
						JslAuctionEnum.BID_PRICE_EXCEED_MAX_LOT_PRICE_ERROR.getResponseDescription(), flowName, bundleTemplate);
			}
			final BigDecimal newBid = currentBid;
			final BidEventModel bidEvent = getModelService().create(BidEventModel.class);
			bidEvent.setBidId(getProxyBidRuleUniqueId());
			bidEvent.setBidPrice(newBid);
			bidEvent.setBundleTemplate(bundleTemplate);
			bidEvent.setAuctionEvent(bundleTemplate.getAuctionEvent());
			bidEvent.setCreatedOn(new Date(System.currentTimeMillis()));
			bidEvent.setCreatedBy(createdBy);
			bidEvent.setIsProxyBid(Boolean.TRUE);
			getModelService().save(bidEvent);
			isProxyPlaced = true;
		}
		catch (final JslAuctionException ex)
		{
			LOG.error(MessageFormat.format(JSL_AUCTION_EXCEPTION,
					createdBy, bundleTemplate.getId(), ex.getDescription()));
		}
		catch (final ModelSavingException e)
		{
			LOG.error(MessageFormat.format(MODALSAVING_EXCEPTION, createdBy, bundleTemplate.getId()));

			throw new JslAuctionException(JslAuctionEnum.ERROR_WHILE_SAVING_DATA.getResponseCode(),
					JslAuctionEnum.ERROR_WHILE_SAVING_DATA.getResponseDescription(), flowName, bundleTemplate);
		}
		catch (final Exception ex)
		{
			LOG.error("placeProxyBidWhenMaxProxySameAsCurrentProxy(): "
					+ MessageFormat.format(BASE_EXCEPTION, createdBy, bundleTemplate.getId(), currentBid), ex);
		}

		return isProxyPlaced;
	}

	/**
	 * setFirstProxyBid() Description: Set first time proxy and place first proxy bid
	 *
	 * @param bundleTemplate
	 * @param proxyBidPrice
	 * @param maxBidForBundle
	 * @return boolean isProxySet
	 */
	private boolean setFirstProxyBid(final BundleTemplateModel bundleTemplate, final String proxyBidPrice,
			final String maxBidForBundle)
	{
		boolean isProxySet;
		ProxyBidRuleModel proxyBidRule;
		BigDecimal firstProxyBid = null;
		proxyBidRule = getModelService().create(ProxyBidRuleModel.class);
		proxyBidRule.setProxyBidId(getProxyBidRuleUniqueId());
		proxyBidRule.setMaxProxyBidPrice(new BigDecimal(proxyBidPrice));
		proxyBidRule.setMinBidPrice(bundleTemplate.getBaseBidPrice());
		proxyBidRule.setIncrementByBidPrice(new BigDecimal(500));
		proxyBidRule.setBundleTemplate(bundleTemplate);
		proxyBidRule.setAuctionEvent(bundleTemplate.getAuctionEvent());
		proxyBidRule.setCreatedOn(new Date(System.currentTimeMillis()));
		proxyBidRule.setCreatedBy(getUserService().getCurrentUser());
		getModelService().save(proxyBidRule);
		LOG.info("setFirstProxyBid() for user:" + getUserService().getCurrentUser().getUid() + "proxy bid price: " + proxyBidPrice
				+ " lot id:" + bundleTemplate.getId());
		if (StringUtils.hasText(maxBidForBundle))
		{
			final BigDecimal h1ForBundle = new BigDecimal(maxBidForBundle);
			if (h1ForBundle.compareTo(bundleTemplate.getBaseBidPrice()) == 0)
			{
				placeProxyBid(bundleTemplate, String.valueOf(firstProxyBid), true, flowName);
			}
			else
			{
				firstProxyBid = h1ForBundle;
				placeProxyBid(bundleTemplate, String.valueOf(firstProxyBid), false, flowName);
			}
		}
		else
		{
			firstProxyBid = bundleTemplate.getBaseBidPrice();
			placeProxyBid(bundleTemplate, String.valueOf(firstProxyBid), false, flowName);
		}
		isProxySet = true;

		return isProxySet;
	}

	/**
	 * updateProxyBid() Description: Update proxy bid if already set
	 *
	 * @param bundleTemplate
	 * @param proxyBidPrice
	 * @param proxyBidRule
	 * @param maxBidForBundle
	 * @return
	 */
	private boolean updateProxyBid(final BundleTemplateModel bundleTemplate, final String proxyBidPrice,
			final ProxyBidRuleModel proxyBidRule, final String maxBidForBundle)
	{
		boolean isProxySet;
		BigDecimal firstProxyBid = null;
		proxyBidRule.setMaxProxyBidPrice(new BigDecimal(proxyBidPrice));
		proxyBidRule.setCreatedOn(new Date(System.currentTimeMillis()));
		getModelService().save(proxyBidRule);
		LOG.info("updateProxyBid() for user:" + getUserService().getCurrentUser().getUid() + "proxy bid price: " + proxyBidPrice
				+ " lot id:" + bundleTemplate.getId());
		if (StringUtils.hasText(maxBidForBundle))
		{
			firstProxyBid = new BigDecimal(maxBidForBundle);
		}
		placeProxyBid(bundleTemplate, String.valueOf(firstProxyBid), true, flowName);
		isProxySet = true;

		return isProxySet;
	}

	/**
	 * isBidValid() Description: validate proxy bid based
	 * 1: Auction must be in started state.
	 * 2: Bid price must be not greater than max lot price.
	 * 3: Bid price must be greater than current H1 price.
	 * 4: Bid price must not be less than current H1 price.
	 * 5: Bid price must not be less than lot base price.
	 * 6: Proxy bid price must not be equal to proxy already set by another user.
	 *
	 * @param bundleTemplate
	 * @param maxBidForBundle
	 * @param newBid
	 * @param proxyBidList
	 * @return boolean isBidValid
	 */
	private boolean isBidValid(final BundleTemplateModel bundleTemplate, final String maxBidForBundle, final BigDecimal newBid,
			final List<ProxyBidRuleModel> proxyBidList)
	{
		boolean isBidValid = false;
		if (!bundleTemplate.getAuctionEvent().getAuctionStatus().getCode().equalsIgnoreCase("STARTED"))
		{
			LOG.error(MessageFormat.format(AUCTION_CLOSED_EXCEPTION, bundleTemplate.getId(),
					bundleTemplate.getAuctionEvent().getAuctionId(), bundleTemplate.getAuctionEvent().getAuctionStatus().getCode(),
					getUserService().getCurrentUser().getUid()));

			throw new JslAuctionException(JslAuctionEnum.AUCTION_NOT_IN_STARTED_STATE.getResponseCode(),
					JslAuctionEnum.AUCTION_NOT_IN_STARTED_STATE.getResponseDescription(), flowName, bundleTemplate);
		}

		if (bundleTemplate.getMaxLotPrice() != null && bundleTemplate.getMaxLotPrice().doubleValue() >= newBid.doubleValue())
		{
			if ((StringUtils.hasText(maxBidForBundle) && newBid.doubleValue() <= Double.valueOf(maxBidForBundle)))
			{
				LOG.error(MessageFormat.format(
						"Proxy bid price {0} cannot be less than current H1 price: {1} lot ID: {2} auction id: {3} for userid: {4}",
						newBid, maxBidForBundle, bundleTemplate.getId(), bundleTemplate.getAuctionEvent().getAuctionId(),
						getUserService().getCurrentUser().getUid()));

				throw new JslAuctionException(JslAuctionEnum.BID_PRICE_LESS_THAN_H1_ERROR.getResponseCode(),
						JslAuctionEnum.BID_PRICE_LESS_THAN_H1_ERROR.getResponseDescription(), flowName, bundleTemplate);
			}
			else if ((StringUtils.hasText(maxBidForBundle) && (bundleTemplate.getBaseBidPrice() != null
					&& newBid.doubleValue() <= bundleTemplate.getBaseBidPrice().doubleValue())))
			{
				LOG.error(MessageFormat.format(
						"Proxy bid price {0} cannot be less than lot base price: {1} lot ID: {2} auction id: {3} for userid: {4}",
						newBid, bundleTemplate.getBaseBidPrice(), bundleTemplate.getId(),
						bundleTemplate.getAuctionEvent().getAuctionId(),
						getUserService().getCurrentUser().getUid()));

				throw new JslAuctionException(JslAuctionEnum.BID_PRICE_LESS_THAN_BASEBID_ERROR.getResponseCode(),
						JslAuctionEnum.BID_PRICE_LESS_THAN_BASEBID_ERROR.getResponseDescription(), flowName, bundleTemplate);
			}
			else
			{
				isBidValid = true;
			}
			validateProxyBidIfProxyAmountisLessThanOrEqualToMaxProxyBid(bundleTemplate, newBid, proxyBidList);
		}
		else
		{
			LOG.error(MessageFormat.format(
					"Proxy bid price {0} cannot be greater than lot max price: {1} lot ID: {2} auction id: {3} for userid: {4}",
					newBid, bundleTemplate.getMaxLotPrice(), bundleTemplate.getId(), bundleTemplate.getAuctionEvent().getAuctionId(),
					getUserService().getCurrentUser().getUid()));

			throw new JslAuctionException(JslAuctionEnum.BID_PRICE_EXCEED_MAX_LOT_PRICE_ERROR.getResponseCode(),
					JslAuctionEnum.BID_PRICE_EXCEED_MAX_LOT_PRICE_ERROR.getResponseDescription(), flowName, bundleTemplate);
		}

		return isBidValid;
	}

	/**
	 * validateProxyBidIfProxyAmountisLessThanOrEqualToMaxProxyBid()
	 * Description:  Proxy bid price must not be equal to proxy already
	 * set by another user.
	 *
	 * @param bundleTemplate
	 * @param newBid
	 * @param proxyBidList
	 */
	private void validateProxyBidIfProxyAmountisLessThanOrEqualToMaxProxyBid(final BundleTemplateModel bundleTemplate,
			final BigDecimal newBid, final List<ProxyBidRuleModel> proxyBidList)
	{
		if (!CollectionUtils.isEmpty(proxyBidList))
		{
			final boolean isPresent = proxyBidList.stream().anyMatch(p -> p.getMaxProxyBidPrice().compareTo(newBid) == 0);
			if (isPresent)
			{
				LOG.error(MessageFormat.format(
						"Proxy bid already set with this price:{0} on lot ID: {1} auction id: {2} for userid: {3}", newBid,
						bundleTemplate.getId(), bundleTemplate.getAuctionEvent().getAuctionId(),
						getUserService().getCurrentUser().getUid()));

				throw new JslAuctionException(JslAuctionEnum.PROXY_BID_ALREADY_SET_FOR_THIS_PRICE.getResponseCode(),
						JslAuctionEnum.PROXY_BID_ALREADY_SET_FOR_THIS_PRICE.getResponseDescription(), flowName, bundleTemplate);
			}
		}
	}

	/**
	 * convertProxyBidModelToDto()
	 * Description : convert proxybidRuleModel to custom dto
	 *
	 * @param bidEventList
	 * @param proxyBidRuleDtoList
	 */
	private void convertProxyBidModelToDto(final List<ProxyBidRuleModel> bidEventList,
			final List<DefaultBundleCommonDto> proxyBidRuleDtoList)
	{
		for (final ProxyBidRuleModel proxyBid : bidEventList)
		{
			final DefaultBundleCommonDto defaultBundleCommonDto = new DefaultBundleCommonDto();
			defaultBundleCommonDto.setAuctionEventId(String.valueOf(proxyBid.getAuctionEvent().getAuctionId()));
			defaultBundleCommonDto.setBundleTemplateId(proxyBid.getBundleTemplate().getId());
			defaultBundleCommonDto.setBidPrice(String.valueOf(proxyBid.getMaxProxyBidPrice()));
			proxyBidRuleDtoList.add(defaultBundleCommonDto);
		}
	}

	/**
	 * removeDuplicateBidPriceIfProxyIsH1()
	 * Description : remove bid price if proxy is placed for same bid price
	 * @param bidEventList
	 * @return
	 */
	private BidEventModel removeDuplicateBidPriceIfProxyIsH1(final List<BidEventModel> bidEventList)
	{
		BidEventModel bidEventModel = null;
		String bidPrice = "";
		String currentBidPrice;
		final List<BidEventModel> filteredBidEventList = new ArrayList<>();
		for (final BidEventModel bidEvent : bidEventList)
		{
			currentBidPrice = bidEvent.getBidPrice().toString();
			if (!bidPrice.equalsIgnoreCase(currentBidPrice))
			{
				filteredBidEventList.add(bidEvent);
			}
			bidPrice = currentBidPrice;
		}
		if(!CollectionUtils.isEmpty(filteredBidEventList)) {
			bidEventModel = filteredBidEventList.get(0);
		}

		return bidEventModel;
	}

	private String getProxyBidRuleUniqueId()
	{
		return (String) getProxyBidEventKeyGenerator().generate();
	}

	public JslProxyBidRuleDao getJslProxyBidRuleDao()
	{
		return jslProxyBidRuleDao;
	}

	public void setJslProxyBidRuleDao(final JslProxyBidRuleDao jslProxyBidRuleDao)
	{
		this.jslProxyBidRuleDao = jslProxyBidRuleDao;
	}

	public JslBidEventDao getJslBidEventDao()
	{
		return jslBidEventDao;
	}

	public void setJslBidEventDao(final JslBidEventDao jslBidEventDao)
	{
		this.jslBidEventDao = jslBidEventDao;
	}

	public UserService getUserService()
	{
		return userService;
	}

	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	public KeyGenerator getProxyBidEventKeyGenerator()
	{
		return proxyBidEventKeyGenerator;
	}

	public void setProxyBidEventKeyGenerator(final KeyGenerator proxyBidEventKeyGenerator)
	{
		this.proxyBidEventKeyGenerator = proxyBidEventKeyGenerator;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public Converter<ProxyBidRuleModel, ProxyBidEventData> getProxyBidConverter()
	{
		return proxyBidConverter;
	}

	public void setProxyBidConverter(final Converter<ProxyBidRuleModel, ProxyBidEventData> proxyBidConverter)
	{
		this.proxyBidConverter = proxyBidConverter;
	}

	public EventService getEventService()
	{
		return eventService;
	}

	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}

}
