/**
 *
 */
package com.jsl.core.services.impl;

import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Required;

import com.jsl.core.daos.JslAuctionUserGroupDao;
import com.jsl.core.model.AuctionUserGroupModel;
import com.jsl.core.services.JslAuctionUserGroupService;


/**
 * @author manav.magoo
 *
 */
public class JslAuctionUserGroupServiceImpl implements JslAuctionUserGroupService
{

	JslAuctionUserGroupDao jslAuctionUserGroupDao;
	UserService userService;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.jsl.core.services.JslAuctionUserGroupService#getCustomers(java.lang.String)
	 */
	@Override
	public Collection<CustomerModel> getCustomers(final String uid)
	{
		// XXX Auto-generated method stub

		final HashSet<CustomerModel> customers = new HashSet<>();

		final AuctionUserGroupModel auctionUserGroup = getJslAuctionUserGroupDao().getAuctionUserGroup(uid);
		final Set<PrincipalModel> groupMembers = auctionUserGroup.getMembers();

		for (final PrincipalModel userGroup : groupMembers)
		{
			if (userGroup instanceof UserGroupModel)
			{
				final Set<PrincipalModel> members = ((UserGroupModel) userGroup).getMembers();
				for (final PrincipalModel principal : members)
				{
					if (principal instanceof CustomerModel)
					{
						customers.add((CustomerModel) principal);
					}
				}
			}
		}

		return customers;
	}


	/**
	 * @return the jslAuctionUserGroupDao
	 */
	public JslAuctionUserGroupDao getJslAuctionUserGroupDao()
	{
		return jslAuctionUserGroupDao;
	}

	/**
	 * @param jslAuctionUserGroupDao
	 *           the jslAuctionUserGroupDao to set
	 */
	@Required
	public void setJslAuctionUserGroupDao(final JslAuctionUserGroupDao jslAuctionUserGroupDao)
	{
		this.jslAuctionUserGroupDao = jslAuctionUserGroupDao;
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

}
