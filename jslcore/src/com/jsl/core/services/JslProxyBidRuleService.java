package com.jsl.core.services;

import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;

import java.util.List;

import com.jsl.core.dto.DefaultBundleCommonDto;
import com.jsl.core.model.AuctionEventModel;
import com.jsl.jslConfigurableBundleService.model.ProxyBidRuleModel;


public interface JslProxyBidRuleService
{
	/**
	 * getProxyBidForCurrentUser()
	 * Description : get the list of all proxy bid set by current user on * lots
	 * @param auctionEvent
	 * @return
	 */
	public List<DefaultBundleCommonDto> getProxyBidForCurrentUser(AuctionEventModel auctionEvent);

	/**
	 * setProxy() 
	 * Description: Set/Update proxy with max proxy amount and increment value
	 * 
	 * @param bundleTemplate
	 * @param bidPrice
	 * @return boolean isProxySet
	 */
	public boolean setProxy(BundleTemplateModel bundleTemplate, String bidPrice);

	/**
	 * placeProxyBid() 
	 * Description : Place proxy bid based on different scenarios
	 *
	 * @param bundleTemplate
	 * @param bidPrice
	 * @param fromProxyBidEventListener
	 * @param flowName
	 * @return boolean isProxyPlaced
	 */
	public boolean placeProxyBid(BundleTemplateModel bundleTemplate, String bidPrice, boolean fromProxyBidEventListener,
			String flowName);

	/**
	 * getAllProxyBidPrice()
	 * Description : get the list of proxy bid set by all users
	 * @param bundleTemplate
	 * @return
	 */
	public List<ProxyBidRuleModel> getAllProxyBidPrice(BundleTemplateModel bundleTemplate);

}
