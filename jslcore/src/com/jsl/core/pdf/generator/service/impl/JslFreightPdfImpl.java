/**
 * 
 */
package com.jsl.core.pdf.generator.service.impl;

import java.io.ByteArrayOutputStream;
import java.util.List;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.jsl.core.daos.JslAuctionDeliveryDao;
import com.jsl.core.model.AuctionDeliveryChargeModel;
import com.jsl.core.pdf.generator.service.JslFreightPdf;


/**
 * @author himanshu.sial
 *
 */
public class JslFreightPdfImpl implements JslFreightPdf
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jsl.core.pdf.generator.service.JslFreightPdf#generatePdf(java.lang.String)
	 */

	private JslAuctionDeliveryDao jslAuctionDeliveryDao;

	private static final String HYPHEN = "-";
	private static final String OPEN_PARENTHESIS = " (";
	private static final String CLOSE_PARENTHESIS = ")";
	private static final String JSL = "JSL";
	private static final String JSHL = "JSHL";
	private static final String JAJPUR = "- Jajpur";
	private static final String HISAR = "- Hisar";


	@Override
	public void generatePdf(String plantLocation, ByteArrayOutputStream baos) throws Exception
	{
		// XXX Auto-generated method stub
		List<AuctionDeliveryChargeModel> freightChargesList = jslAuctionDeliveryDao.getFreightCharges(plantLocation.toUpperCase());

		if (JSL.equalsIgnoreCase(plantLocation))
		{
			this.createPdf(freightChargesList, baos, JAJPUR);
		}
		else if (JSHL.equalsIgnoreCase(plantLocation))
		{
			this.createPdf(freightChargesList, baos, HISAR);
		}
	}

	private List<AuctionDeliveryChargeModel> getSortedListByLocation(List<AuctionDeliveryChargeModel> freightChargesList)
	{
		freightChargesList.sort((f1,f2)-> f1.getLocation().compareTo(f2.getLocation()));
		return freightChargesList;
	}
	private void createPdf(List<AuctionDeliveryChargeModel> freightChargesModels, ByteArrayOutputStream baos, String plantLocation)
			throws Exception
	{
		Document document = new Document();

		PdfWriter writer = PdfWriter.getInstance(document, baos);
		document.open();

		PdfPTable table = new PdfPTable(2); // 3 columns.
		table.setWidthPercentage(100); //Width 100%
		table.setSpacingBefore(10f); //Space before table
		table.setSpacingAfter(10f); //Space after table

		//Set Column widths
		float[] columnWidths =
		{ 1f, 1f };
		table.setWidths(columnWidths);

		PdfPCell headerCell = new PdfPCell(new Paragraph("ROUTE"));
		headerCell.setBorderColor(BaseColor.ORANGE);
		headerCell.setPaddingLeft(10);
		headerCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headerCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		PdfPCell headerCell2 = new PdfPCell(new Paragraph("RATE/MT"));
		headerCell2.setBorderColor(BaseColor.ORANGE);
		headerCell2.setPaddingLeft(10);
		headerCell2.setHorizontalAlignment(Element.ALIGN_CENTER);
		headerCell2.setVerticalAlignment(Element.ALIGN_MIDDLE);


		Font boldFond = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);

		Paragraph headParagraph = new Paragraph("Freight Charges\r\n" + plantLocation, boldFond);

		document.add(headParagraph);

		table.addCell(headerCell);
		table.addCell(headerCell2);


		freightChargesModels.forEach(freightChargesModel -> {
			this.addPdfCell(table, freightChargesModel);
		});
		//To avoid having the cell border and the content overlap, if you are having thick cell borders
		//cell1.setUserBorderPadding(true);
		//cell2.setUserBorderPadding(true);
		//cell3.setUserBorderPadding(true);

		document.add(table);

		document.close();
	}

	private void addPdfCell(PdfPTable table, AuctionDeliveryChargeModel freightCharges)
	{


		String route = freightCharges.getLocation() + HYPHEN + freightCharges.getCity() ;

		PdfPCell cell1 = new PdfPCell(new Paragraph(route));//"JSL-GURGAON-NEW DELHI (110002)"));
		cell1.setBorderColor(BaseColor.ORANGE);
		cell1.setPaddingLeft(10);
		cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

		PdfPCell cell2 = new PdfPCell(new Paragraph(freightCharges.getAmount().toString()));
		cell2.setBorderColor(BaseColor.ORANGE);
		cell2.setPaddingLeft(10);
		cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

		table.addCell(cell1);
		table.addCell(cell2);

	}

	/**
	 * @return the jslAuctionDeliveryDao
	 */
	public JslAuctionDeliveryDao getJslAuctionDeliveryDao()
	{
		return jslAuctionDeliveryDao;
	}

	/**
	 * @param jslAuctionDeliveryDao
	 *           the jslAuctionDeliveryDao to set
	 */
	public void setJslAuctionDeliveryDao(JslAuctionDeliveryDao jslAuctionDeliveryDao)
	{
		this.jslAuctionDeliveryDao = jslAuctionDeliveryDao;
	}


}
