/**
 * 
 */
package com.jsl.core.pdf.generator.service;

import java.io.ByteArrayOutputStream;


/**
 * @author himanshu.sial
 *
 */
public interface JslFreightPdf
{

	public void generatePdf(String plantLocation, ByteArrayOutputStream baos) throws Exception;

}
