/**
 *
 */
package com.jsl.core.dto;

import java.util.ArrayList;
import java.util.List;


/**
 * @author manav.magoo
 *
 */
public class JslZdocOrderExportDto
{
	private String refNo;
	private String orderCode;
	private String orderType;
	private String salesOrganization;
	private String distributionChannel;
	private String division;
	private String salesOffice;
	private String soldToPartyCode;
	private String shipToPartyCode;
	private String poNumber;
	private String poDate;
	private String incoTerm1;
	private String incoTerm2;
	private String OrderReason;

	/**
	 * @return the orderReason
	 */
	public String getOrderReason()
	{
		return OrderReason;
	}




	/**
	 * @param orderReason
	 *           the orderReason to set
	 */
	public void setOrderReason(final String orderReason)
	{
		OrderReason = orderReason;
	}




	private String requiredDeliveryDate;
	private String templateRecordingDate;
	private String partialShipment;
	private String clubbing;
	private String lotsSize;
	private String noOfLots;
	private String camID;
	private String specialRemark;
	private String reference;


	private List<JslZdocOrderItemExportDto> orderItems;

	public JslZdocOrderExportDto()
	{
		orderItems = new ArrayList<>();
	}




	/**
	 * @return the salesOrganization
	 */
	public String getSalesOrganization()
	{
		return salesOrganization;
	}

	/**
	 * @param salesOrganization
	 *           the salesOrganization to set
	 */
	public void setSalesOrganization(final String salesOrganization)
	{
		this.salesOrganization = salesOrganization;
	}

	/**
	 * @return the distributionChannel
	 */
	public String getDistributionChannel()
	{
		return distributionChannel;
	}

	/**
	 * @param distributionChannel
	 *           the distributionChannel to set
	 */
	public void setDistributionChannel(final String distributionChannel)
	{
		this.distributionChannel = distributionChannel;
	}

	/**
	 * @return the division
	 */
	public String getDivision()
	{
		return division;
	}

	/**
	 * @param division
	 *           the division to set
	 */
	public void setDivision(final String division)
	{
		this.division = division;
	}

	/**
	 * @return the salesOffice
	 */
	public String getSalesOffice()
	{
		return salesOffice;
	}

	/**
	 * @param salesOffice
	 *           the salesOffice to set
	 */
	public void setSalesOffice(final String salesOffice)
	{
		this.salesOffice = salesOffice;
	}

	/**
	 * @return the soldToPartyCode
	 */
	public String getSoldToPartyCode()
	{
		return soldToPartyCode;
	}

	/**
	 * @param soldToPartyCode
	 *           the soldToPartyCode to set
	 */
	public void setSoldToPartyCode(final String soldToPartyCode)
	{
		this.soldToPartyCode = soldToPartyCode;
	}

	/**
	 * @return the shipToPartyCode
	 */
	public String getShipToPartyCode()
	{
		return shipToPartyCode;
	}

	/**
	 * @param shipToPartyCode
	 *           the shipToPartyCode to set
	 */
	public void setShipToPartyCode(final String shipToPartyCode)
	{
		this.shipToPartyCode = shipToPartyCode;
	}



	/**
	 * @return the partialShipment
	 */
	public String getPartialShipment()
	{
		return partialShipment;
	}

	/**
	 * @param partialShipment
	 *           the partialShipment to set
	 */
	public void setPartialShipment(final String partialShipment)
	{
		this.partialShipment = partialShipment;
	}

	/**
	 * @return the clubbing
	 */
	public String getClubbing()
	{
		return clubbing;
	}

	/**
	 * @param clubbing
	 *           the clubbing to set
	 */
	public void setClubbing(final String clubbing)
	{
		this.clubbing = clubbing;
	}

	/**
	 * @return the lotsSize
	 */
	public String getLotsSize()
	{
		return lotsSize;
	}

	/**
	 * @param lotsSize
	 *           the lotsSize to set
	 */
	public void setLotsSize(final String lotsSize)
	{
		this.lotsSize = lotsSize;
	}

	/**
	 * @return the noOfLots
	 */
	public String getNoOfLots()
	{
		return noOfLots;
	}

	/**
	 * @param i
	 *           the noOfLots to set
	 */
	public void setNoOfLots(final String i)
	{
		this.noOfLots = i;
	}

	/**
	 * @return the camID
	 */
	public String getCamID()
	{
		return camID;
	}

	/**
	 * @param camID
	 *           the camID to set
	 */
	public void setCamID(final String camID)
	{
		this.camID = camID;
	}

	/**
	 * @return the specialRemark
	 */
	public String getSpecialRemark()
	{
		return specialRemark;
	}

	/**
	 * @param specialRemark
	 *           the specialRemark to set
	 */
	public void setSpecialRemark(final String specialRemark)
	{
		this.specialRemark = specialRemark;
	}

	/**
	 * @return the reference
	 */
	public String getReference()
	{
		return reference;
	}

	/**
	 * @param reference
	 *           the reference to set
	 */
	public void setReference(final String reference)
	{
		this.reference = reference;
	}

	/**
	 * @return the orderItems
	 */
	public List<JslZdocOrderItemExportDto> getOrderItems()
	{
		return orderItems;
	}

	/**
	 * @param orderItems
	 *           the orderItems to set
	 */
	public void setOrderItems(final List<JslZdocOrderItemExportDto> orderItems)
	{
		this.orderItems = orderItems;
	}



	/**
	 * @return the orderCode
	 */
	public String getOrderCode()
	{
		return orderCode;
	}



	/**
	 * @param orderCode
	 *           the orderCode to set
	 */
	public void setOrderCode(final String orderCode)
	{
		this.orderCode = orderCode;
	}



	/**
	 * @return the refNo
	 */
	public String getRefNo()
	{
		return refNo;
	}



	/**
	 * @param refNo
	 *           the refNo to set
	 */
	public void setRefNo(final String refNo)
	{
		this.refNo = refNo;
	}





	/**
	 * @return the poDate
	 */
	public String getPoDate()
	{
		return poDate;
	}



	/**
	 * @param poDate
	 *           the poDate to set
	 */
	public void setPoDate(final String poDate)
	{
		this.poDate = poDate;
	}



	/**
	 * @return the incoTerm1
	 */
	public String getIncoTerm1()
	{
		return incoTerm1;
	}



	/**
	 * @param incoTerm1
	 *           the incoTerm1 to set
	 */
	public void setIncoTerm1(final String incoTerm1)
	{
		this.incoTerm1 = incoTerm1;
	}



	/**
	 * @return the incoTerm2
	 */
	public String getIncoTerm2()
	{
		return incoTerm2;
	}



	/**
	 * @param incoTerm2
	 *           the incoTerm2 to set
	 */
	public void setIncoTerm2(final String incoTerm2)
	{
		this.incoTerm2 = incoTerm2;
	}




	/**
	 * @return the orderType
	 */
	public String getOrderType()
	{
		return orderType;
	}




	/**
	 * @param orderType
	 *           the orderType to set
	 */
	public void setOrderType(final String orderType)
	{
		this.orderType = orderType;
	}




	/**
	 * @return the poNumber
	 */
	public String getPoNumber()
	{
		return poNumber;
	}




	/**
	 * @param poNumber
	 *           the poNumber to set
	 */
	public void setPoNumber(final String poNumber)
	{
		this.poNumber = poNumber;
	}




	/**
	 * @return the requiredDeliveryDate
	 */
	public String getRequiredDeliveryDate()
	{
		return requiredDeliveryDate;
	}




	/**
	 * @param requiredDeliveryDate
	 *           the requiredDeliveryDate to set
	 */
	public void setRequiredDeliveryDate(final String requiredDeliveryDate)
	{
		this.requiredDeliveryDate = requiredDeliveryDate;
	}




	/**
	 * @return the templateRecordingDate
	 */
	public String getTemplateRecordingDate()
	{
		return templateRecordingDate;
	}




	/**
	 * @param templateRecordingDate
	 *           the templateRecordingDate to set
	 */
	public void setTemplateRecordingDate(final String templateRecordingDate)
	{
		this.templateRecordingDate = templateRecordingDate;
	}









}
