/**
 *
 */
package com.jsl.core.dto;

/**
 * @author suyash.trivedi
 *
 */
public class JslOTPGenerationDto
{


	private String emailAddress;

	private String otpCode;

	private String expirationTime;

	private String genearetdTime;

	private String displayName;

	private String fromDisplayName;

	private String auctionId;

	/**
	 * @return the auctionId
	 */
	public String getAuctionId()
	{
		return auctionId;
	}

	/**
	 * @param auctionId
	 *           the auctionId to set
	 */
	public void setAuctionId(final String auctionId)
	{
		this.auctionId = auctionId;
	}

	/**
	 * @return the fromDisplayName
	 */
	public String getFromDisplayName()
	{
		return fromDisplayName;
	}

	/**
	 * @param fromDisplayName
	 *           the fromDisplayName to set
	 */
	public void setFromDisplayName(final String fromDisplayName)
	{
		this.fromDisplayName = fromDisplayName;
	}

	/**
	 * @return the displayName
	 */
	public String getDisplayName()
	{
		return displayName;
	}

	/**
	 * @param displayName
	 *           the displayName to set
	 */
	public void setDisplayName(final String displayName)
	{
		this.displayName = displayName;
	}

	/**
	 * @return the genearetdTime
	 */
	public String getGenearetdTime()
	{
		return genearetdTime;
	}

	/**
	 * @param genearetdTime
	 *           the genearetdTime to set
	 */
	public void setGenearetdTime(final String genearetdTime)
	{
		this.genearetdTime = genearetdTime;
	}

	/**
	 * @return the expirationTime
	 */
	public String getExpirationTime()
	{
		return expirationTime;
	}

	/**
	 * @param expirationTime
	 *           the expirationTime to set
	 */
	public void setExpirationTime(final String expirationTime)
	{
		this.expirationTime = expirationTime;
	}

	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress()
	{
		return emailAddress;
	}

	/**
	 * @param emailAddress
	 *           the emailAddress to set
	 */
	public void setEmailAddress(final String emailAddress)
	{
		this.emailAddress = emailAddress;
	}

	/**
	 * @return the otpCode
	 */
	public String getOtpCode()
	{
		return otpCode;
	}

	/**
	 * @param otpCode
	 *           the otpCode to set
	 */
	public void setOtpCode(final String otpCode)
	{
		this.otpCode = otpCode;
	}

	/**
	 * @param otpCode
	 *           the otpCode to set
	 */





}
