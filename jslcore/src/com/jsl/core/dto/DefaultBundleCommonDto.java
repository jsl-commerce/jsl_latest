package com.jsl.core.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(Include.NON_NULL)
public class DefaultBundleCommonDto
{
	@JsonProperty(value = "bidPrice")
	private String bidPrice;

	@JsonProperty(value = "bundleTemplateId")
	private String bundleTemplateId;

	@JsonProperty(value = "auctionEventId")
	private String auctionEventId;

	@JsonProperty(value = "auctionStartDate")
	private String auctionStartDate;

	@JsonProperty(value = "auctionEndDate")
	private String auctionEndDate;

	@JsonProperty(value = "lotIndex")
	private String lotIndex;

	@JsonProperty(value = "h1Bidder")
	private String h1Bidder;

	@JsonProperty(value = "dateRange")
	private boolean dateRange;

	@JsonProperty(value = "isProxyBid")
	private boolean isProxyBid;

	public String getBidPrice()
	{
		return bidPrice;
	}

	public void setBidPrice(final String bidPrice)
	{
		this.bidPrice = bidPrice;
	}

	public String getBundleTemplateId()
	{
		return bundleTemplateId;
	}

	public void setBundleTemplateId(final String bundleTemplateId)
	{
		this.bundleTemplateId = bundleTemplateId;
	}

	public String getAuctionEventId()
	{
		return auctionEventId;
	}

	public void setAuctionEventId(final String auctionEventId)
	{
		this.auctionEventId = auctionEventId;
	}

	public String getAuctionStartDate()
	{
		return auctionStartDate;
	}

	public void setAuctionStartDate(final String auctionStartDate)
	{
		this.auctionStartDate = auctionStartDate;
	}

	public String getAuctionEndDate()
	{
		return auctionEndDate;
	}

	public void setAuctionEndDate(final String auctionEndDate)
	{
		this.auctionEndDate = auctionEndDate;
	}

	public String getLotIndex()
	{
		return lotIndex;
	}

	public void setLotIndex(final String lotIndex)
	{
		this.lotIndex = lotIndex;
	}

	public String getH1Bidder()
	{
		return h1Bidder;
	}

	public void setH1Bidder(final String h1Bidder)
	{
		this.h1Bidder = h1Bidder;
	}

	public boolean isDateRange()
	{
		return dateRange;
	}

	public void setDateRange(final boolean dateRange)
	{
		this.dateRange = dateRange;
	}

	public boolean isProxyBid()
	{
		return isProxyBid;
	}

	public void setProxyBid(final boolean isProxyBid)
	{
		this.isProxyBid = isProxyBid;
	}

	@Override
	public String toString()
	{
		final StringBuilder builder = new StringBuilder();
		builder.append("DefaultBundleCommonDto [bidPrice=");
		builder.append(bidPrice);
		builder.append(", bundleTemplateId=");
		builder.append(bundleTemplateId);
		builder.append(", auctionEventId=");
		builder.append(auctionEventId);
		builder.append(", auctionStartDate=");
		builder.append(auctionStartDate);
		builder.append(", auctionEndDate=");
		builder.append(auctionEndDate);
		builder.append(", lotIndex=");
		builder.append(lotIndex);
		builder.append(", h1Bidder=");
		builder.append(h1Bidder);
		builder.append(", dateRange=");
		builder.append(dateRange);
		builder.append(", isProxyBid=");
		builder.append(isProxyBid);
		builder.append("]");
		return builder.toString();
	}

}
