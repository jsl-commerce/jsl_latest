package com.jsl.core.dto;

import de.hybris.platform.commercefacades.product.data.ProductData;

import java.util.Date;
import java.util.List;


public class JslCompletedAuctionDto
{
	private String auctionID;

	private Date auctionStartDate;

	private String bundleID;

	private String bundleName;

	private Double bundleQty;

	private String startBidPrice;

	private String userLastBidPrice;

	private String h1Price;

	private String bidStatus;

	private String orderStatus;

	private String bidWonBy;

	private List<ProductData> productData;

	public String getAuctionID()
	{
		return auctionID;
	}

	public void setAuctionID(final String auctionID)
	{
		this.auctionID = auctionID;
	}

	public Date getAuctionStartDate()
	{
		return auctionStartDate;
	}

	public void setAuctionStartDate(final Date auctionStartDate)
	{
		this.auctionStartDate = auctionStartDate;
	}

	public String getBundleID()
	{
		return bundleID;
	}

	public void setBundleID(final String bundleID)
	{
		this.bundleID = bundleID;
	}

	public String getBundleName()
	{
		return bundleName;
	}

	public void setBundleName(final String bundleName)
	{
		this.bundleName = bundleName;
	}

	public Double getBundleQty()
	{
		return bundleQty;
	}

	public void setBundleQty(final Double bundleQty)
	{
		this.bundleQty = bundleQty;
	}

	public String getStartBidPrice()
	{
		return startBidPrice;
	}

	public void setStartBidPrice(final String startBidPrice)
	{
		this.startBidPrice = startBidPrice;
	}

	public String getUserLastBidPrice()
	{
		return userLastBidPrice;
	}

	public void setUserLastBidPrice(final String userLastBidPrice)
	{
		this.userLastBidPrice = userLastBidPrice;
	}

	public String getH1Price()
	{
		return h1Price;
	}

	public void setH1Price(final String h1Price)
	{
		this.h1Price = h1Price;
	}

	public String getBidStatus()
	{
		return bidStatus;
	}

	public void setBidStatus(final String bidStatus)
	{
		this.bidStatus = bidStatus;
	}

	public String getOrderStatus()
	{
		return orderStatus;
	}

	public void setOrderStatus(final String orderStatus)
	{
		this.orderStatus = orderStatus;
	}

	public List<ProductData> getProductData()
	{
		return productData;
	}

	public void setProductData(final List<ProductData> productData)
	{
		this.productData = productData;
	}

	public String getBidWonBy()
	{
		return bidWonBy;
	}

	public void setBidWonBy(final String bidWonBy)
	{
		this.bidWonBy = bidWonBy;
	}

	@Override
	public String toString()
	{
		final StringBuilder builder = new StringBuilder();
		builder.append("JslCompletedAuctionDto [auctionID=");
		builder.append(auctionID);
		builder.append(", auctionStartDate=");
		builder.append(auctionStartDate);
		builder.append(", bundleID=");
		builder.append(bundleID);
		builder.append(", bundleName=");
		builder.append(bundleName);
		builder.append(", bundleQty=");
		builder.append(bundleQty);
		builder.append(", startBidPrice=");
		builder.append(startBidPrice);
		builder.append(", userLastBidPrice=");
		builder.append(userLastBidPrice);
		builder.append(", h1Price=");
		builder.append(h1Price);
		builder.append(", bidStatus=");
		builder.append(bidStatus);
		builder.append(", orderStatus=");
		builder.append(orderStatus);
		builder.append(", bidWonBy=");
		builder.append(bidWonBy);
		builder.append(", productData=");
		builder.append(productData);
		builder.append("]");
		return builder.toString();
	}

}
