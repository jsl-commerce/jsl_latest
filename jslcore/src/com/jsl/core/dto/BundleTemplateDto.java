/**
 *
 */
package com.jsl.core.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


public class BundleTemplateDto implements Serializable
{

	private static final long serialVersionUID = 1L;

	private String lotId;

	private String lotName;

	private String type;

	private String version;

	private String description;

	private BigDecimal baseBidPrice;

	private BigDecimal bidWonPrice;

	private Integer auctionId;

	private Date auctionDate;

	private String plant;

	private String quality;

	private String gradeGroup;

	private Double thickness;

	private Double length;

	private Double weight;

	private Double quantity;

	private String auctionGroup;

	private String width;

	private String location;

	private String materialNum;

	private String edge;

	private String pvc;

	private String finish;

	private String grade;

	private String batchId;

	private String age;

	private String orderStatus;

	private String bidStatus;

	private String yourBidPrice;

	private String approvalsStatus;

	private String salesOrderNumber;

	private String bundleParentId;

	public String getLotId()
	{
		return lotId;
	}

	public void setLotId(final String lotId)
	{
		this.lotId = lotId;
	}

	public String getLotName()
	{
		return lotName;
	}

	public void setLotName(final String lotName)
	{
		this.lotName = lotName;
	}

	public String getType()
	{
		return type;
	}

	public void setType(final String type)
	{
		this.type = type;
	}

	public String getVersion()
	{
		return version;
	}

	public void setVersion(final String version)
	{
		this.version = version;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(final String description)
	{
		this.description = description;
	}

	public BigDecimal getBaseBidPrice()
	{
		return baseBidPrice;
	}

	public void setBaseBidPrice(final BigDecimal baseBidPrice)
	{
		this.baseBidPrice = baseBidPrice;
	}

	public BigDecimal getBidWonPrice()
	{
		return bidWonPrice;
	}

	public void setBidWonPrice(final BigDecimal bidWonPrice)
	{
		this.bidWonPrice = bidWonPrice;
	}

	public Integer getAuctionId()
	{
		return auctionId;
	}

	public void setAuctionId(final Integer auctionId)
	{
		this.auctionId = auctionId;
	}

	public String getPlant()
	{
		return plant;
	}

	public void setPlant(final String plant)
	{
		this.plant = plant;
	}

	public String getQuality()
	{
		return quality;
	}

	public void setQuality(final String quality)
	{
		this.quality = quality;
	}

	public String getGradeGroup()
	{
		return gradeGroup;
	}

	public void setGradeGroup(final String gradeGroup)
	{
		this.gradeGroup = gradeGroup;
	}

	public Double getThickness()
	{
		return thickness;
	}

	public void setThickness(final Double thickness)
	{
		this.thickness = thickness;
	}

	public Double getLength()
	{
		return length;
	}

	public void setLength(final Double length)
	{
		this.length = length;
	}

	public Double getWeight()
	{
		return weight;
	}

	public void setWeight(final Double weight)
	{
		this.weight = weight;
	}

	public Double getQuantity()
	{
		return quantity;
	}

	public void setQuantity(final Double quantity)
	{
		this.quantity = quantity;
	}

	public String getAuctionGroup()
	{
		return auctionGroup;
	}

	public void setAuctionGroup(final String auctionGroup)
	{
		this.auctionGroup = auctionGroup;
	}

	public String getWidth()
	{
		return width;
	}

	public void setWidth(final String width)
	{
		this.width = width;
	}

	public String getLocation()
	{
		return location;
	}

	public void setLocation(final String location)
	{
		this.location = location;
	}

	public String getMaterialNum()
	{
		return materialNum;
	}

	public void setMaterialNum(final String materialNum)
	{
		this.materialNum = materialNum;
	}

	public String getEdge()
	{
		return edge;
	}

	public void setEdge(final String edge)
	{
		this.edge = edge;
	}

	public String getPvc()
	{
		return pvc;
	}

	public void setPvc(final String pvc)
	{
		this.pvc = pvc;
	}

	public String getFinish()
	{
		return finish;
	}

	public void setFinish(final String finish)
	{
		this.finish = finish;
	}

	public String getGrade()
	{
		return grade;
	}

	public void setGrade(final String grade)
	{
		this.grade = grade;
	}

	public String getBatchId()
	{
		return batchId;
	}

	public void setBatchId(final String batchId)
	{
		this.batchId = batchId;
	}

	public static long getSerialversionuid()
	{
		return serialVersionUID;
	}

	public String getAge()
	{
		return age;
	}

	public void setAge(final String age)
	{
		this.age = age;
	}

	public Date getAuctionDate()
	{
		return auctionDate;
	}

	public void setAuctionDate(final Date auctionDate)
	{
		this.auctionDate = auctionDate;
	}

	public String getBidStatus()
	{
		return bidStatus;
	}

	public void setBidStatus(final String bidStatus)
	{
		this.bidStatus = bidStatus;
	}

	public String getOrderStatus()
	{
		return orderStatus;
	}

	public void setOrderStatus(final String orderStatus)
	{
		this.orderStatus = orderStatus;
	}

	public String getYourBidPrice()
	{
		return yourBidPrice;
	}

	public void setYourBidPrice(final String yourBidPrice)
	{
		this.yourBidPrice = yourBidPrice;
	}

	public String getApprovalsStatus()
	{
		return approvalsStatus;
	}

	public void setApprovalsStatus(final String approvalsStatus)
	{
		this.approvalsStatus = approvalsStatus;
	}

	/**
	 * @return the salesOrderNumber
	 */
	public String getSalesOrderNumber()
	{
		return salesOrderNumber;
	}

	/**
	 * @param salesOrderNumber
	 *           the salesOrderNumber to set
	 */
	public void setSalesOrderNumber(final String salesOrderNumber)
	{
		this.salesOrderNumber = salesOrderNumber;
	}


	/**
	 * @return the bundleId
	 */
	public String getBundleParentId()
	{
		return bundleParentId;
	}

	/**
	 * @param bundleId
	 *           the bundleId to set
	 */
	public void setBundleParentId(final String bundleParentId)
	{
		this.bundleParentId = bundleParentId;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		final StringBuilder builder = new StringBuilder();
		builder.append("BundleTemplateDto [lotId=");
		builder.append(lotId);
		builder.append(", lotName=");
		builder.append(lotName);
		builder.append(", type=");
		builder.append(type);
		builder.append(", version=");
		builder.append(version);
		builder.append(", description=");
		builder.append(description);
		builder.append(", baseBidPrice=");
		builder.append(baseBidPrice);
		builder.append(", bidWonPrice=");
		builder.append(bidWonPrice);
		builder.append(", auctionId=");
		builder.append(auctionId);
		builder.append(", auctionDate=");
		builder.append(auctionDate);
		builder.append(", plant=");
		builder.append(plant);
		builder.append(", quality=");
		builder.append(quality);
		builder.append(", gradeGroup=");
		builder.append(gradeGroup);
		builder.append(", thickness=");
		builder.append(thickness);
		builder.append(", length=");
		builder.append(length);
		builder.append(", weight=");
		builder.append(weight);
		builder.append(", quantity=");
		builder.append(quantity);
		builder.append(", auctionGroup=");
		builder.append(auctionGroup);
		builder.append(", width=");
		builder.append(width);
		builder.append(", location=");
		builder.append(location);
		builder.append(", materialNum=");
		builder.append(materialNum);
		builder.append(", edge=");
		builder.append(edge);
		builder.append(", pvc=");
		builder.append(pvc);
		builder.append(", finish=");
		builder.append(finish);
		builder.append(", grade=");
		builder.append(grade);
		builder.append(", batchId=");
		builder.append(batchId);
		builder.append(", age=");
		builder.append(age);
		builder.append(", orderStatus=");
		builder.append(orderStatus);
		builder.append(", bidStatus=");
		builder.append(bidStatus);
		builder.append(", yourBidPrice=");
		builder.append(yourBidPrice);
		builder.append(", approvalsStatus=");
		builder.append(approvalsStatus);
		builder.append(", salesOrderNumber=");
		builder.append(salesOrderNumber);
		builder.append(", bundleId=");
		builder.append(bundleParentId);
		builder.append("]");
		return builder.toString();
	}

}
