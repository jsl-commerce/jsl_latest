package com.jsl.core.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;


public class DefaultAuctionCommonDto
{
	@JsonProperty(value = "auctionEventId")
	private String auctionEventId;

	@JsonProperty(value = "auctionStartDate")
	private String auctionStartDate;

	@JsonProperty(value = "auctionEndDate")
	private String auctionEndDate;

	@JsonProperty(value = "additionalTime")
	private String additionalTime;

	@JsonProperty(value = "currentTime")
	private long currentTime;

	@JsonProperty(value = "auctionStatus")
	private String auctionStatus;

	@JsonProperty(value = "isLast30Sec")
	private boolean isLast30Sec;

	@JsonProperty(value = "lotIdOf30Sec")
	private String lotIdOf30Sec;

	private List<DefaultBundleCommonDto> defaultBundleCommonDto;

	public String getAuctionEventId()
	{
		return auctionEventId;
	}

	public void setAuctionEventId(final String auctionEventId)
	{
		this.auctionEventId = auctionEventId;
	}

	public String getAuctionStartDate()
	{
		return auctionStartDate;
	}

	public void setAuctionStartDate(final String auctionStartDate)
	{
		this.auctionStartDate = auctionStartDate;
	}

	public String getAuctionEndDate()
	{
		return auctionEndDate;
	}

	public void setAuctionEndDate(final String auctionEndDate)
	{
		this.auctionEndDate = auctionEndDate;
	}

	public String getAdditionalTime()
	{
		return additionalTime;
	}

	public void setAdditionalTime(final String additionalTime)
	{
		this.additionalTime = additionalTime;
	}

	public long getCurrentTime()
	{
		return currentTime;
	}

	public void setCurrentTime(final long currentTime)
	{
		this.currentTime = currentTime;
	}

	public String getAuctionStatus()
	{
		return auctionStatus;
	}

	public void setAuctionStatus(final String auctionStatus)
	{
		this.auctionStatus = auctionStatus;
	}

	public List<DefaultBundleCommonDto> getDefaultBundleCommonDto()
	{
		return defaultBundleCommonDto;
	}

	public void setDefaultBundleCommonDto(final List<DefaultBundleCommonDto> defaultBundleCommonDto)
	{
		this.defaultBundleCommonDto = defaultBundleCommonDto;
	}

	public boolean isLast30Sec()
	{
		return isLast30Sec;
	}

	public void setLast30Sec(final boolean isLast30Sec)
	{
		this.isLast30Sec = isLast30Sec;
	}

	public String getLotIdOf30Sec()
	{
		return lotIdOf30Sec;
	}

	public void setLotIdOf30Sec(final String lotIdOf30Sec)
	{
		this.lotIdOf30Sec = lotIdOf30Sec;
	}

	@Override
	public String toString()
	{
		final StringBuilder builder = new StringBuilder();
		builder.append("DefaultAuctionCommonDto [auctionEventId=");
		builder.append(auctionEventId);
		builder.append(", auctionStartDate=");
		builder.append(auctionStartDate);
		builder.append(", auctionEndDate=");
		builder.append(auctionEndDate);
		builder.append(", additionalTime=");
		builder.append(additionalTime);
		builder.append(", currentTime=");
		builder.append(currentTime);
		builder.append(", auctionStatus=");
		builder.append(auctionStatus);
		builder.append(", isLast30Sec=");
		builder.append(isLast30Sec);
		builder.append(", lotIdOf30Sec=");
		builder.append(lotIdOf30Sec);
		builder.append(", defaultBundleCommonDto=");
		builder.append(defaultBundleCommonDto);
		builder.append("]");
		return builder.toString();
	}

}
