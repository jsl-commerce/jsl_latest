/**
 *
 */
package com.jsl.core.dto;

/**
 * @author manav.magoo
 *
 */
public class JslOTPValidationDto
{

	private String AuctionId;

	/**
	 * @return the auctionId
	 */
	public String getAuctionId()
	{
		return AuctionId;
	}

	/**
	 * @param auctionId
	 *           the auctionId to set
	 */
	public void setAuctionId(final String auctionId)
	{
		AuctionId = auctionId;
	}
}
