/**
 *
 */
package com.jsl.core.dto;

/**
 * @author manav.magoo
 *
 */
public class JslZdocOrderItemExportDto
{
	private String refNo;
	private String orderCode;
	private String itemNo;
	private String materialCode;
	private String plant;
	private String qtyMT;
	private String plates;
	private String ZPRC;
	private String ZPRCCurr;
	private String ZFRB;
	private String ZFRBCurr;
	private String ZCMP;
	private String ZCMPCurr;
	private String gradeGroup;
	private String grade;
	private String thk;
	private String width;
	private String length;
	private String finish;
	private String edgeCon;
	private String quality;
	private String partWtMinimum;
	private String partWtMaximum;
	private String coilId;
	private String route;
	private String usage;
	private String iNSP_3RD_PARTY;
	private String ILP;
	private String PVC;
	private String standard;
	private String markSTck;
	private String markingOnProduct;
	private String utensilUsage;
	private String materialGroup;
	private String desired_Date1;


	/**
	 * @return the itemNo
	 */
	public String getItemNo()
	{
		return itemNo;
	}

	/**
	 * @param itemNo
	 *           the itemNo to set
	 */
	public void setItemNo(final String itemNo)
	{
		this.itemNo = itemNo;
	}

	/**
	 * @return the plant
	 */
	public String getPlant()
	{
		return plant;
	}

	/**
	 * @param plant
	 *           the plant to set
	 */
	public void setPlant(final String plant)
	{
		this.plant = plant;
	}

	/**
	 * @return the materialCode
	 */
	public String getMaterialCode()
	{
		return materialCode;
	}

	/**
	 * @param materialCode
	 *           the materialCode to set
	 */
	public void setMaterialCode(final String materialCode)
	{
		this.materialCode = materialCode;
	}


	/**
	 * @return the qtyMT
	 */
	public String getQtyMT()
	{
		return qtyMT;
	}

	/**
	 * @param qtyMT
	 *           the qtyMT to set
	 */
	public void setQtyMT(final String qtyMT)
	{
		this.qtyMT = qtyMT;
	}

	/**
	 * @return the route
	 */
	public String getRoute()
	{
		return route;
	}

	/**
	 * @param route
	 *           the route to set
	 */
	public void setRoute(final String route)
	{
		this.route = route;
	}

	/**
	 * @return the standard
	 */
	public String getStandard()
	{
		return standard;
	}

	/**
	 * @param standard
	 *           the standard to set
	 */
	public void setStandard(final String standard)
	{
		this.standard = standard;
	}

	/**
	 * @return the gradeGroup
	 */
	public String getGradeGroup()
	{
		return gradeGroup;
	}

	/**
	 * @param gradeGroup
	 *           the gradeGroup to set
	 */
	public void setGradeGroup(final String gradeGroup)
	{
		this.gradeGroup = gradeGroup;
	}

	/**
	 * @return the grade
	 */
	public String getGrade()
	{
		return grade;
	}

	/**
	 * @param grade
	 *           the grade to set
	 */
	public void setGrade(final String grade)
	{
		this.grade = grade;
	}

	/**
	 * @return the thk
	 */
	public String getThk()
	{
		return thk;
	}

	/**
	 * @param thk
	 *           the thk to set
	 */
	public void setThk(final String thk)
	{
		this.thk = thk;
	}

	/**
	 * @return the width
	 */
	public String getWidth()
	{
		return width;
	}

	/**
	 * @param width
	 *           the width to set
	 */
	public void setWidth(final String width)
	{
		this.width = width;
	}

	/**
	 * @return the length
	 */
	public String getLength()
	{
		return length;
	}

	/**
	 * @param length
	 *           the length to set
	 */
	public void setLength(final String length)
	{
		this.length = length;
	}

	/**
	 * @return the finish
	 */
	public String getFinish()
	{
		return finish;
	}

	/**
	 * @param finish
	 *           the finish to set
	 */
	public void setFinish(final String finish)
	{
		this.finish = finish;
	}

	/**
	 * @return the edgeCon
	 */
	public String getEdgeCon()
	{
		return edgeCon;
	}

	/**
	 * @param edgeCon
	 *           the edgeCon to set
	 */
	public void setEdgeCon(final String edgeCon)
	{
		this.edgeCon = edgeCon;
	}

	/**
	 * @return the quality
	 */
	public String getQuality()
	{
		return quality;
	}

	/**
	 * @param quality
	 *           the quality to set
	 */
	public void setQuality(final String quality)
	{
		this.quality = quality;
	}

	/**
	 * @return the coilId
	 */
	public String getCoilId()
	{
		return coilId;
	}

	/**
	 * @param coilId
	 *           the coilId to set
	 */
	public void setCoilId(final String coilId)
	{
		this.coilId = coilId;
	}

	/**
	 * @return the usage
	 */
	public String getUsage()
	{
		return usage;
	}

	/**
	 * @param usage
	 *           the usage to set
	 */
	public void setUsage(final String usage)
	{
		this.usage = usage;
	}


	/**
	 * @return the partWtMinimum
	 */
	public String getPartWtMinimum()
	{
		return partWtMinimum;
	}

	/**
	 * @param partWtMinimum
	 *           the partWtMinimum to set
	 */
	public void setPartWtMinimum(final String partWtMinimum)
	{
		this.partWtMinimum = partWtMinimum;
	}

	/**
	 * @return the partWtMaximum
	 */
	public String getPartWtMaximum()
	{
		return partWtMaximum;
	}

	/**
	 * @param partWtMaximum
	 *           the partWtMaximum to set
	 */
	public void setPartWtMaximum(final String partWtMaximum)
	{
		this.partWtMaximum = partWtMaximum;
	}

	/**
	 * @return the iLP
	 */
	public String getILP()
	{
		return ILP;
	}

	/**
	 * @param iLP
	 *           the iLP to set
	 */
	public void setILP(final String iLP)
	{
		ILP = iLP;
	}

	/**
	 * @return the pVC
	 */
	public String getPVC()
	{
		return PVC;
	}

	/**
	 * @param pVC
	 *           the pVC to set
	 */
	public void setPVC(final String pVC)
	{
		PVC = pVC;
	}

	/**
	 * @return the iNSP_3RD_PARTY
	 */
	public String getiNSP_3RD_PARTY()
	{
		return iNSP_3RD_PARTY;
	}

	/**
	 * @param iNSP_3RD_PARTY
	 *           the iNSP_3RD_PARTY to set
	 */
	public void setiNSP_3RD_PARTY(final String iNSP_3RD_PARTY)
	{
		this.iNSP_3RD_PARTY = iNSP_3RD_PARTY;
	}

	/**
	 * @return the markingOnProduct
	 */
	public String getMarkingOnProduct()
	{
		return markingOnProduct;
	}

	/**
	 * @param markingOnProduct
	 *           the markingOnProduct to set
	 */
	public void setMarkingOnProduct(final String markingOnProduct)
	{
		this.markingOnProduct = markingOnProduct;
	}

	/**
	 * @return the markSTck
	 */
	public String getMarkSTck()
	{
		return markSTck;
	}

	/**
	 * @param markSTck
	 *           the markSTck to set
	 */
	public void setMarkSTck(final String markSTck)
	{
		this.markSTck = markSTck;
	}

	/**
	 * @return the utensilUsage
	 */
	public String getUtensilUsage()
	{
		return utensilUsage;
	}

	/**
	 * @param utensilUsage
	 *           the utensilUsage to set
	 */
	public void setUtensilUsage(final String utensilUsage)
	{
		this.utensilUsage = utensilUsage;
	}

	/**
	 * @return the desired_Date1
	 */
	public String getDesired_Date1()
	{
		return desired_Date1;
	}

	/**
	 * @param desired_Date1
	 *           the desired_Date1 to set
	 */
	public void setDesired_Date1(final String desired_Date1)
	{
		this.desired_Date1 = desired_Date1;
	}

	/**
	 * @return the zPRC
	 */
	public String getZPRC()
	{
		return ZPRC;
	}

	/**
	 * @param zPRC
	 *           the zPRC to set
	 */
	public void setZPRC(final String zPRC)
	{
		ZPRC = zPRC;
	}

	/**
	 * @return the zPRCCurr
	 */
	public String getZPRCCurr()
	{
		return ZPRCCurr;
	}

	/**
	 * @param zPRCCurr
	 *           the zPRCCurr to set
	 */
	public void setZPRCCurr(final String zPRCCurr)
	{
		ZPRCCurr = zPRCCurr;
	}

	/**
	 * @return the zFRB
	 */
	public String getZFRB()
	{
		return ZFRB;
	}

	/**
	 * @param zFRB
	 *           the zFRB to set
	 */
	public void setZFRB(final String zFRB)
	{
		ZFRB = zFRB;
	}

	/**
	 * @return the zFRBCurr
	 */
	public String getZFRBCurr()
	{
		return ZFRBCurr;
	}

	/**
	 * @param zFRBCurr
	 *           the zFRBCurr to set
	 */
	public void setZFRBCurr(final String zFRBCurr)
	{
		ZFRBCurr = zFRBCurr;
	}

	/**
	 * @return the orderCode
	 */
	public String getOrderCode()
	{
		return orderCode;
	}

	/**
	 * @param orderCode
	 *           the orderCode to set
	 */
	public void setOrderCode(final String orderCode)
	{
		this.orderCode = orderCode;
	}

	/**
	 * @return the refNo
	 */
	public String getRefNo()
	{
		return refNo;
	}

	/**
	 * @param refNo
	 *           the refNo to set
	 */
	public void setRefNo(final String refNo)
	{
		this.refNo = refNo;
	}

	/**
	 * @return the plates
	 */
	public String getPlates()
	{
		return plates;
	}

	/**
	 * @param plates
	 *           the plates to set
	 */
	public void setPlates(final String plates)
	{
		this.plates = plates;
	}

	/**
	 * @return the zCMP
	 */
	public String getZCMP()
	{
		return ZCMP;
	}

	/**
	 * @param zCMP
	 *           the zCMP to set
	 */
	public void setZCMP(final String zCMP)
	{
		ZCMP = zCMP;
	}

	/**
	 * @return the zCMPCurr
	 */
	public String getZCMPCurr()
	{
		return ZCMPCurr;
	}

	/**
	 * @param zCMPCurr
	 *           the zCMPCurr to set
	 */
	public void setZCMPCurr(final String zCMPCurr)
	{
		ZCMPCurr = zCMPCurr;
	}

	/**
	 * @return the materialGroup
	 */
	public String getMaterialGroup()
	{
		return materialGroup;
	}

	/**
	 * @param materialGroup
	 *           the materialGroup to set
	 */
	public void setMaterialGroup(final String materialGroup)
	{
		this.materialGroup = materialGroup;
	}
}
