package com.jsl.core.dto;

import java.util.Date;


public class BidEventDto extends BundleTemplateDto
{
	private String userId;

	private String userName;

	private String bidPrice;

	private Date bidPlacedDateTime;

	private String h1Bidder;

	private String h1BidderId;

	private String h1BidPricePerMT;

	private String noOfBids;

	private String stoLoc;

	private String descStoLoc;

	private String sbp;

	private String statusBidType;

	private String bidBasis;

	private String bidPricePerMT;

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(final String userId)
	{
		this.userId = userId;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(final String userName)
	{
		this.userName = userName;
	}

	public String getBidPrice()
	{
		return bidPrice;
	}

	public void setBidPrice(final String bidPrice)
	{
		this.bidPrice = bidPrice;
	}

	public Date getBidPlacedDateTime()
	{
		return bidPlacedDateTime;
	}

	public void setBidPlacedDateTime(final Date bidPlacedDateTime)
	{
		this.bidPlacedDateTime = bidPlacedDateTime;
	}

	public String getH1Bidder()
	{
		return h1Bidder;
	}

	public void setH1Bidder(final String h1Bidder)
	{
		this.h1Bidder = h1Bidder;
	}

	public String getH1BidPricePerMT()
	{
		return h1BidPricePerMT;
	}

	public void setH1BidPricePerMT(final String h1BidPricePerMT)
	{
		this.h1BidPricePerMT = h1BidPricePerMT;
	}

	public String getNoOfBids()
	{
		return noOfBids;
	}

	public void setNoOfBids(final String noOfBids)
	{
		this.noOfBids = noOfBids;
	}

	public String getStoLoc()
	{
		return stoLoc;
	}

	public void setStoLoc(final String stoLoc)
	{
		this.stoLoc = stoLoc;
	}

	public String getDescStoLoc()
	{
		return descStoLoc;
	}

	public void setDescStoLoc(final String descStoLoc)
	{
		this.descStoLoc = descStoLoc;
	}

	public String getSbp()
	{
		return sbp;
	}

	public void setSbp(final String sbp)
	{
		this.sbp = sbp;
	}

	public String getH1BidderId()
	{
		return h1BidderId;
	}

	public void setH1BidderId(final String h1BidderId)
	{
		this.h1BidderId = h1BidderId;
	}

	public String getStatusBidType()
	{
		return statusBidType;
	}

	public void setStatusBidType(final String statusBidType)
	{
		this.statusBidType = statusBidType;
	}

	public String getBidBasis()
	{
		return bidBasis;
	}

	public void setBidBasis(final String bidBasis)
	{
		this.bidBasis = bidBasis;
	}

	public String getBidPricePerMT()
	{
		return bidPricePerMT;
	}

	public void setBidPricePerMT(final String bidPricePerMT)
	{
		this.bidPricePerMT = bidPricePerMT;
	}

	@Override
	public String toString()
	{
		final StringBuilder builder = new StringBuilder();
		builder.append("BidEventDto [userId=");
		builder.append(userId);
		builder.append(", userName=");
		builder.append(userName);
		builder.append(", bidPrice=");
		builder.append(bidPrice);
		builder.append(", bidPlacedDateTime=");
		builder.append(bidPlacedDateTime);
		builder.append(", h1Bidder=");
		builder.append(h1Bidder);
		builder.append(", h1BidderId=");
		builder.append(h1BidderId);
		builder.append(", h1BidPricePerMT=");
		builder.append(h1BidPricePerMT);
		builder.append(", noOfBids=");
		builder.append(noOfBids);
		builder.append(", stoLoc=");
		builder.append(stoLoc);
		builder.append(", descStoLoc=");
		builder.append(descStoLoc);
		builder.append(", sbp=");
		builder.append(sbp);
		builder.append(", statusBidType=");
		builder.append(statusBidType);
		builder.append(", bidBasis=");
		builder.append(bidBasis);
		builder.append(", bidPricePerMT=");
		builder.append(bidPricePerMT);
		builder.append("]");
		return builder.toString();
	}

}
