/**
 *
 */
package com.jsl.core.strategies.impl;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.web.DefaultSessionCloseStrategy;

import javax.servlet.http.HttpSession;

import com.jsl.core.services.JslUserService;


/**
 * @author manav.magoo
 *
 */
public class JslSessionCloseStrategy extends DefaultSessionCloseStrategy
{
	JslUserService jslUserService;

	/**
	 * @return the jslUserService
	 */
	public JslUserService getJslUserService()
	{
		return jslUserService;
	}

	/**
	 * @param jslUserService
	 *           the jslUserService to set
	 */
	public void setJslUserService(final JslUserService jslUserService)
	{
		this.jslUserService = jslUserService;
	}

	@Override
	public void closeSessionInHttpSession(final HttpSession httpSession)
	{

		final UserModel currentUser = getJslUserService().getCurrentUser();
		getJslUserService().setLogin(currentUser.getUid(), false);

		super.closeSessionInHttpSession(httpSession);
	}

}
