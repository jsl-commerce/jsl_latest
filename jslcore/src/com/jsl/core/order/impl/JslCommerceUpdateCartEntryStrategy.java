/**
 *
 */
package com.jsl.core.order.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceUpdateCartEntryStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.util.Config;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.xml.soap.SOAPException;

import org.apache.log4j.Logger;

import com.jsl.core.enums.JslCartType;
import com.jsl.core.search.solrfacetsearch.service.JslProductSourceService;
import com.jsl.core.services.JslOrderService;
import com.jsl.cpi.integration.service.CpiReportsService;


/**
 * @author manav.magoo
 *
 */
public class JslCommerceUpdateCartEntryStrategy extends DefaultCommerceUpdateCartEntryStrategy
{
	public static final String MIN_ORDER_QUANTITY_NOT_MATCHED = "minOrderQuantityNotMatched";
	public static final String MOU_CONDITION_FAILED = "mouConditionFailed";

	public static final String MIN_ADDED_PRODUCT_TYPE_NOT_MATCHED = "productTypeNotMatched";

	private static final String ORDER_TIME_INTERVAL = "order.time.interval";
	long allowedQuantity = 0;
	private CpiReportsService cpiReportService;
	private JslOrderService jslOrderService;
	private JslProductSourceService jslProductSourceService;

	private static final Logger LOG = Logger.getLogger(JslCommerceUpdateCartEntryStrategy.class);

	@Override
	public CommerceCartModification updateQuantityForCartEntry(final CommerceCartParameter parameters)
			throws CommerceCartModificationException
	{
		final CartModel cartModel = parameters.getCart();
		if (cartModel.getType().equals(JslCartType.AUCTION))
		{
			return super.updateQuantityForCartEntry(parameters);
		}

		beforeUpdateCartEntry(parameters);
		final long newQuantity = parameters.getQuantity();
		final long entryNumber = parameters.getEntryNumber();

		validateParameterNotNull(cartModel, "Cart model cannot be null");
		CommerceCartModification modification;

		final AbstractOrderEntryModel entryToUpdate = getEntryForNumber(cartModel, (int) entryNumber);
		validateEntryBeforeModification(newQuantity, entryToUpdate);

		final Integer maxOrderQuantity = entryToUpdate.getProduct().getMaxOrderQuantity();
		// Work out how many we want to add (could be negative if we are
		// removing items)
		final long quantityToAdd = newQuantity - entryToUpdate.getQuantity().longValue();
		// So now work out what the maximum allowed to be added is (note that
		// this may be negative!)
		final long actualAllowedQuantityChange = getAllowedCartAdjustmentForProduct(cartModel, entryToUpdate.getProduct(),
				quantityToAdd, entryToUpdate.getDeliveryPointOfService());
		//Now do the actual cartModification

		final long cartLevel = checkCartLevel(entryToUpdate.getProduct(), cartModel, entryToUpdate.getDeliveryPointOfService());
		final long cartLevelAfterQuantityChange = actualAllowedQuantityChange + cartLevel;

		if(!validateProducttype(cartModel, entryToUpdate.getProduct()))
		{
			modification = createAddToCartResp(parameters, MIN_ORDER_QUANTITY_NOT_MATCHED, entryToUpdate, 0);
			modification.setProductTypeFailed(true);
			return modification;
		}
		if (entryToUpdate.getProduct().getMinOrderQuantity() != null && cartLevelAfterQuantityChange != 0
				&& cartLevelAfterQuantityChange < entryToUpdate.getProduct().getMinOrderQuantity())
		{
			modification = createAddToCartResp(parameters, MIN_ORDER_QUANTITY_NOT_MATCHED, entryToUpdate, 0);
			modification.setMinQuanityFailed(true);
			modification.setAllowedQuantity(entryToUpdate.getProduct().getMinOrderQuantity().longValue());
			return modification;
		}

		if (quantityToAdd > 0 && !validateMouQuantityCheck(cartModel, quantityToAdd))
		{
			modification = createAddToCartResp(parameters, MOU_CONDITION_FAILED, entryToUpdate, 0);
			modification.setAllowedQuantity(allowedQuantity);
			modification.setMouExceeded(true);
			return modification;
		}

		modification = modifyEntry(cartModel, entryToUpdate, actualAllowedQuantityChange, newQuantity, maxOrderQuantity);
		afterUpdateCartEntry(parameters, modification);
		modification.setMinQuanityFailed(false);
		modification.setMouExceeded(false);
		return modification;

	}

	protected CommerceCartModification createAddToCartResp(final CommerceCartParameter parameter, final String status,
			final AbstractOrderEntryModel entryToUpdate, final long quantityAdded)
	{
		final long quantityToAdd = parameter.getQuantity();
		final CommerceCartModification modification = new CommerceCartModification();
		modification.setStatusCode(status);
		modification.setQuantityAdded(quantityAdded);
		modification.setQuantity(quantityToAdd);
		modification.setEntry(entryToUpdate);
		modification.setMinQuanityFailed(false);
		modification.setMouExceeded(false);
		return modification;
	}

	/**
	 * @param cartModel
	 * @param productModel
	 * @param quantityToAdd
	 * @param allowedQuantity
	 * @return
	 */
	private boolean validateMouQuantityCheck(final CartModel cartModel, final long quantityToAdd)
	{
		try
		{
			final CustomerModel customer = (CustomerModel) cartModel.getUser();
			final Optional<Long> sum = cartModel.getEntries().stream().map(entry -> entry.getQuantity()).reduce(Long::sum);
			if (sum.isPresent())
			{
				final long totalCartQuantity = sum.get();
				return calculateMouRule(totalCartQuantity, quantityToAdd, customer);
			}
			else
			{
				return calculateMouRule(0, quantityToAdd, customer);
			}
		}
		catch (final SOAPException ex)
		{
			LOG.error("Jsl validateMouQuantityCheck exception " + ex.getMessage());
			return false;
		}
	}

	/**
	 * @param quantityToAdd
	 * @param d
	 * @param totalProductQuantity
	 * @param allowedQuantity
	 * @return
	 * @throws SOAPException
	 */
	private boolean calculateMouRule(final long totalCartQuantity, final long quantityToAdd, final CustomerModel customer)
			throws SOAPException
	{
		// XXX Auto-generated method stub
		double allowedMouQuanity = getCpiReportService().gePopMouValue() != null
				? getCpiReportService().gePopMouValue().doubleValue()
				: 0;

		final List<OrderModel> orders = jslOrderService.getOrdersForTimeInterval(Config.getParameter(ORDER_TIME_INTERVAL),
				JslCartType.WEB.getCode(), customer);

		final Optional<Long> sum = orders.stream().map(OrderModel::getEntries).collect(Collectors.toList()).stream()
				.flatMap(List::stream).collect(Collectors.toList()).stream().map(entry -> entry.getQuantity()).reduce(Long::sum);

		if (sum.isPresent() && allowedMouQuanity != 0)
		{
			allowedMouQuanity = allowedMouQuanity - sum.get().doubleValue();
		}
		if (allowedMouQuanity < (totalCartQuantity + quantityToAdd))
		{
			allowedQuantity = (long) (quantityToAdd - ((totalCartQuantity + quantityToAdd) - allowedMouQuanity));

			allowedQuantity = allowedQuantity > 0 ? allowedQuantity : 0;

			return false;
		}

		return true;
	}

	/**
	 * @param customer
	 * @return
	 */
	private double getPopValue(final CustomerModel customer)
	{
		// XXX Auto-generated method stub
		//Hit api to get POP value for customer
		return 0;
	}
	
	private boolean validateProducttype(CartModel cartModel,ProductModel productModel)
	{
		if(cartModel.getEntries().isEmpty())
		{
			return true;
		}
		else if(jslProductSourceService.getProductSourceType(productModel).equals(jslProductSourceService.getProductSourceType(cartModel.getEntries().get(0).getProduct())))
		{
			return true;
		}
		return false;
	}

	/**
	 * @return the cpiReportService
	 */
	public CpiReportsService getCpiReportService()
	{
		return cpiReportService;
	}

	/**
	 * @param cpiReportService
	 *           the cpiReportService to set
	 */
	public void setCpiReportService(final CpiReportsService cpiReportService)
	{
		this.cpiReportService = cpiReportService;
	}

	/**
	 * @return the jslProductSourceService
	 */
	public JslProductSourceService getJslProductSourceService()
	{
		return jslProductSourceService;
	}

	/**
	 * @param jslProductSourceService the jslProductSourceService to set
	 */
	public void setJslProductSourceService(JslProductSourceService jslProductSourceService)
	{
		this.jslProductSourceService = jslProductSourceService;
	}
	
	/**
	 * @return the jslOrderService
	 */
	public JslOrderService getJslOrderService()
	{
		return jslOrderService;
	}

	/**
	 * @param jslOrderService
	 *           the jslOrderService to set
	 */
	public void setJslOrderService(JslOrderService jslOrderService)
	{
		this.jslOrderService = jslOrderService;
	}

}
