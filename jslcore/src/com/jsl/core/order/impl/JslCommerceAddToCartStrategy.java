/**
 *
 */
package com.jsl.core.order.impl;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceAddToCartStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.util.Config;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.xml.soap.SOAPException;

import org.apache.log4j.Logger;

import com.jsl.core.daos.JslOrderDao;
import com.jsl.core.enums.JslCartType;
import com.jsl.core.search.solrfacetsearch.service.JslProductSourceService;
import com.jsl.core.services.JslOrderService;
import com.jsl.cpi.integration.service.CpiReportsService;


/**
 * @author manav.magoo
 *
 */
public class JslCommerceAddToCartStrategy extends DefaultCommerceAddToCartStrategy
{

	public static final String MIN_ORDER_QUANTITY_NOT_MATCHED = "minOrderQuantityNotMatched";
	public static final String MOU_CONDITION_FAILED = "mouConditionFailed";
	private static final String ORDER_TIME_INTERVAL = "order.time.interval";
	long allowedQuantity = 0;

	private CpiReportsService cpiReportService;
	
	private JslProductSourceService jslProductSourceService;

	private JslOrderService jslOrderService;
	
	private static final Logger LOG = Logger.getLogger(JslCommerceAddToCartStrategy.class);

	/**
	 * Do add to cart.
	 *
	 * @param parameter
	 *           the parameter
	 * @return the commerce cart modification
	 * @throws CommerceCartModificationException
	 *            the commerce cart modification exception
	 */
	@Override
	protected CommerceCartModification doAddToCart(final CommerceCartParameter parameter) throws CommerceCartModificationException
	{
		CommerceCartModification modification;
		final CartModel cartModel = parameter.getCart();

		if (cartModel.getType().equals(JslCartType.AUCTION))
		{
			return super.doAddToCart(parameter);
		}

		final ProductModel productModel = parameter.getProduct();
		final long quantityToAdd = parameter.getQuantity();
		final PointOfServiceModel deliveryPointOfService = parameter.getPointOfService();

		this.beforeAddToCart(parameter);
		validateAddToCart(parameter);

		if(validateProducttype(cartModel, productModel))
		{
		if (isProductForCode(parameter).booleanValue())
		{
			// So now work out what the maximum allowed to be added is (note that this may be negative!)
			final long actualAllowedQuantityChange = getAllowedCartAdjustmentForProduct(cartModel, productModel, quantityToAdd,
					deliveryPointOfService);
			final Integer maxOrderQuantity = productModel.getMaxOrderQuantity();
			final long cartLevel = checkCartLevel(productModel, cartModel, deliveryPointOfService);
			final long cartLevelAfterQuantityChange = actualAllowedQuantityChange + cartLevel;

			if (productModel.getMinOrderQuantity() != null && cartLevelAfterQuantityChange >= productModel.getMinOrderQuantity())
			{
				if (!validateMouQuantityCheck(cartModel, quantityToAdd))
				{
					modification = createAddToCartResp(parameter, MOU_CONDITION_FAILED, createEmptyCartEntry(parameter), 0);
					modification.setAllowedQuantity(allowedQuantity);
					modification.setMouExceeded(true);
					return modification;
				}

				if (actualAllowedQuantityChange > 0)
				{
					// We are allowed to add items to the cart
					final CartEntryModel entryModel = addCartEntry(parameter, actualAllowedQuantityChange);
					getModelService().save(entryModel);

					final String statusCode = getStatusCodeAllowedQuantityChange(actualAllowedQuantityChange, maxOrderQuantity,
							quantityToAdd, cartLevelAfterQuantityChange);

					modification = createAddToCartResp(parameter, statusCode, entryModel, actualAllowedQuantityChange);
				}
				else
				{
					// Not allowed to add any quantity, or maybe even asked to reduce the quantity
					// Do nothing!
					final String status = getStatusCodeForNotAllowedQuantityChange(maxOrderQuantity, maxOrderQuantity);

					modification = createAddToCartResp(parameter, status, createEmptyCartEntry(parameter), 0);

				}
			}
			else
			{

				modification = createAddToCartResp(parameter, MIN_ORDER_QUANTITY_NOT_MATCHED, createEmptyCartEntry(parameter), 0);
				modification.setMinQuanityFailed(true);
				modification.setAllowedQuantity(productModel.getMinOrderQuantity().longValue());
			}
		}
		else
		{
			modification = createAddToCartResp(parameter, CommerceCartModificationStatus.UNAVAILABLE,
					createEmptyCartEntry(parameter), 0);
		}

		}
		else
		{
			modification = createAddToCartResp(parameter, CommerceCartModificationStatus.UNAVAILABLE,
					createEmptyCartEntry(parameter), 0);
			modification.setProductTypeFailed(true);
		}
		return modification;
	}

	/**
	 * @param cartModel
	 * @param productModel
	 * @param quantityToAdd
	 * @param allowedQuantity
	 * @return
	 * @throws SOAPException
	 */
	private boolean validateMouQuantityCheck(final CartModel cartModel, final long quantityToAdd)
	{
		try
		{
			final CustomerModel customer = (CustomerModel) cartModel.getUser();
			final Optional<Long> sum = cartModel.getEntries().stream().map(entry -> entry.getQuantity()).reduce(Long::sum);
			if (sum.isPresent())
			{
				final long totalCartQuantity = sum.get();
				return calculateMouRule(totalCartQuantity, quantityToAdd, customer);
			}
			else
			{
				return calculateMouRule(0, quantityToAdd, customer);
			}
		}
		catch (final SOAPException ex)
		{
			LOG.error("Jsl validateMouQuantityCheck exception " + ex.getMessage());
			return false;
		}
	}

	/**
	 * @param quantityToAdd
	 * @param d
	 * @param totalProductQuantity
	 * @param allowedQuantity
	 * @return
	 * @throws SOAPException
	 */
	private boolean calculateMouRule(final long totalCartQuantity, final long quantityToAdd, final CustomerModel customer)
			throws SOAPException
	{
		// XXX Auto-generated method stub
		double allowedMouQuanity = getCpiReportService().gePopMouValue() != null
				? getCpiReportService().gePopMouValue().doubleValue()
				: 0;

		final List<OrderModel> orders = jslOrderService.getOrdersForTimeInterval(Config.getParameter(ORDER_TIME_INTERVAL),
				JslCartType.WEB.getCode(), customer);
		
		final Optional<Long> sum = orders.stream().map(OrderModel::getEntries).collect(Collectors.toList()).stream()
				.flatMap(List::stream).collect(Collectors.toList()).stream().map(entry -> entry.getQuantity()).reduce(Long::sum);

		if (sum.isPresent() && allowedMouQuanity != 0)
		{
			allowedMouQuanity = allowedMouQuanity - sum.get().doubleValue();
		}

		if (allowedMouQuanity < (totalCartQuantity + quantityToAdd))
		{
			allowedQuantity = (long) (quantityToAdd - ((totalCartQuantity + quantityToAdd) - allowedMouQuanity));
			allowedQuantity = allowedQuantity > 0 ? allowedQuantity : 0;
			return false;
		}

		return true;
	}

	private boolean validateProducttype(CartModel cartModel,ProductModel productModel)
	{
		if(cartModel.getEntries().isEmpty())
		{
			return true;
		}
		else if(jslProductSourceService.getProductSourceType(productModel).equals(jslProductSourceService.getProductSourceType(cartModel.getEntries().get(0).getProduct())))
		{
			return true;
		}
		return false;
	}

	@Override
	protected CommerceCartModification createAddToCartResp(final CommerceCartParameter parameter, final String status,
			final CartEntryModel entry, final long quantityAdded)
	{
		final long quantityToAdd = parameter.getQuantity();
		final CommerceCartModification modification = new CommerceCartModification();
		modification.setStatusCode(status);
		modification.setQuantityAdded(quantityAdded);
		modification.setQuantity(quantityToAdd);
		modification.setEntry(entry);
		modification.setMinQuanityFailed(false);
		modification.setMouExceeded(false);
		return modification;
	}

	/**
	 * @return the cpiReportService
	 */
	public CpiReportsService getCpiReportService()
	{
		return cpiReportService;
	}

	/**
	 * @param cpiReportService
	 *           the cpiReportService to set
	 */
	public void setCpiReportService(final CpiReportsService cpiReportService)
	{
		this.cpiReportService = cpiReportService;
	}

	/**
	 * @return the jslProductSourceService
	 */
	public JslProductSourceService getJslProductSourceService()
	{
		return jslProductSourceService;
	}

	/**
	 * @param jslProductSourceService the jslProductSourceService to set
	 */
	public void setJslProductSourceService(JslProductSourceService jslProductSourceService)
	{
		this.jslProductSourceService = jslProductSourceService;
	}
	
	

	/**
	 * @return the jslOrderService
	 */
	public JslOrderService getJslOrderService()
	{
		return jslOrderService;
	}

	/**
	 * @param jslOrderService the jslOrderService to set
	 */
	public void setJslOrderService(JslOrderService jslOrderService)
	{
		this.jslOrderService = jslOrderService;
	}
	
}
