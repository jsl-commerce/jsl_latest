/**
 *
 */
package com.jsl.core.order.service.impl;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;

import org.apache.log4j.Logger;

import com.jsl.core.daos.JslOrderDao;
import com.jsl.core.order.service.JslOrderEmailService;


/**
 * @author himanshu.sial
 *
 */
public class JslOrderEmailServiceImpl implements JslOrderEmailService
{

	private static final Logger LOG = Logger.getLogger(JslOrderEmailServiceImpl.class);
	private BusinessProcessService businessProcessService;
	private ModelService modelService;
	private JslOrderDao jslOrderDao;

	@Override
	public void sendOrderEmail(final AbstractOrderData orderData)
	{
		try
		{
			final OrderProcessModel orderProcessModel = (OrderProcessModel) getBusinessProcessService()
					.createProcess("OrderData" + System.currentTimeMillis(), "orderConfirmationEmailProcess");

			// Fill the process with the appropriate data
			orderProcessModel.setOrder(jslOrderDao.getOrderFromCode(orderData.getCode()).get(0));

			// Save the process
			getModelService().save(orderProcessModel);

			// Then start the process = send the Email
			getBusinessProcessService().startProcess(orderProcessModel);


		}
		catch (final Exception e)
		{
			// XXX: handle exception
			LOG.error("Error in sending email for place order", e);
		}

	}

	/**
	 * @return the businessProcessService
	 */
	public BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	/**
	 * @param businessProcessService
	 *           the businessProcessService to set
	 */
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * @return the jslOrderDao
	 */
	public JslOrderDao getJslOrderDao()
	{
		return jslOrderDao;
	}

	/**
	 * @param jslOrderDao
	 *           the jslOrderDao to set
	 */
	public void setJslOrderDao(final JslOrderDao jslOrderDao)
	{
		this.jslOrderDao = jslOrderDao;
	}

}
