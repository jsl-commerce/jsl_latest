/**
 *
 */
package com.jsl.core.order.service;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;


/**
 * @author himanshu.sial
 *
 */
public interface JslOrderEmailService
{
	public void sendOrderEmail(final AbstractOrderData orderData);
}
