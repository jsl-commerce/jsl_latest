/**
 * 
 */
package com.jsl.core.order.calculation;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.order.impl.DefaultCalculationService;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;

/**
 * @author himanshu.sial
 *
 */
public class JslDefaultCalculationServiceImpl extends DefaultCalculationService
{

	@Override
	public void calculateEntries(final AbstractOrderModel order, final boolean forceRecalculate) throws CalculationException
	{
		
		double subtotal = 0.0;
		for (final AbstractOrderEntryModel e : order.getEntries())
		{
			if(!e.getBasePrice().equals(e.getProduct().getEurope1Prices().iterator().next().getPrice()))
			{
				recalculateOrderEntryIfNeeded(e, true);
			}
			else
			{
				recalculateOrderEntryIfNeeded(e, forceRecalculate);
			}
			
			subtotal += e.getTotalPrice().doubleValue();
		}
		order.setTotalPrice(Double.valueOf(subtotal));

	}
}
