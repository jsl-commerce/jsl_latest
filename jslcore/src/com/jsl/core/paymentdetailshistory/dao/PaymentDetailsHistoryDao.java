/**
 *
 */
package com.jsl.core.paymentdetailshistory.dao;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.sap.sapinvoiceaddon.model.SapB2BDocumentModel;

import java.util.List;

import com.jsl.core.model.JslPaymentTransactionModel;
import com.jsl.core.model.PaymentSaleOrderModel;


/**
 * @author Kiranraj S.
 *
 */
public interface PaymentDetailsHistoryDao
{
	public List<SapB2BDocumentModel> getPaymentHistoryForCurrentUser(UserModel user) throws Exception;

	public List<SapB2BDocumentModel> getPendingPaymentDetailsForCurrentUser(UserModel currentUser) throws Exception;

	public List<SapB2BDocumentModel> getPendingPaymentDetailsBasedOnSearchCriteria(String searchCriteria, String searchValue,
			UserModel currentUser) throws Exception;

	public List<SapB2BDocumentModel> getAllInvoiceDetailsByInvoiceNumbers(final List<String> invoiceNumbers,
			final UserModel currentUser) throws Exception;

	public List<SapB2BDocumentModel> getAllInvoiceDetailsByInvoiceNumber(final List<String> invoiceNumber) throws Exception;

	public List<SapB2BDocumentModel> getPendingPaymentDetailsBasedOnSearchCriteria(final String searchCriteria,
			final List<String> searchValue, final UserModel currentUser) throws Exception;

	public List<SapB2BDocumentModel> getPaymentDetailsFromJslRefNumber(final String jslRefNumber) throws Exception;

	public List<SapB2BDocumentModel> getallInvoices() throws Exception;

	public List<PaymentSaleOrderModel> getSaleOrderDetailsFromNumber(String searchCriteria, String number, final String type)
			throws Exception;

	public List<JslPaymentTransactionModel> getTransactionDetailsForCurrentUser() throws Exception;

	public List<JslPaymentTransactionModel> getTransactionDetailsFromDate(final String startDate, final String endDate)
			throws Exception;

	public List<SapB2BDocumentModel> getAllInvoicesAdminByInvoiceNumbers(final List<String> invoiceNumbers) throws Exception;

	public List<PaymentSaleOrderModel> getAdminSaleOrderDetailsFromNumber(final List<String> saleOrders) throws Exception;

	public List<PaymentSaleOrderModel> getSaleOrderPaymentDetailsFromJslRefNumber(String jslRefNumber) throws Exception;

	public List<JslPaymentTransactionModel> getTransactionModels(String jslRefNumber) throws Exception;
}
