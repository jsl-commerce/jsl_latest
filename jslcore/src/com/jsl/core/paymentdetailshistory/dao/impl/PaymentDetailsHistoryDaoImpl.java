/**
 *
 */
package com.jsl.core.paymentdetailshistory.dao.impl;

import de.hybris.platform.accountsummaryaddon.enums.DocumentStatus;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.sap.sapinvoiceaddon.model.SapB2BDocumentModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.jsl.core.enums.JslPaymentStatus;
import com.jsl.core.model.JslPaymentTransactionModel;
import com.jsl.core.model.PaymentSaleOrderModel;
import com.jsl.core.paymentdetailshistory.dao.PaymentDetailsHistoryDao;
import com.jsl.core.util.JslB2bUnitUtil;


/**
 * @author Kiranraj S.
 *
 */
public class PaymentDetailsHistoryDaoImpl implements PaymentDetailsHistoryDao
{
	private static final Logger LOG = Logger.getLogger(PaymentDetailsHistoryDaoImpl.class);

	private static final String QUERY_LIKE = "_%";
	private FlexibleSearchService flexibleSearchService;
	private SessionService sessionService;
	private UserService userService;

	private static final String OPEN = "open";

	private static final String GET_COMPLETED_PAYMENT_DETAILS_HISTORY_FOR_CURRENT_USER = "SELECT {" + SapB2BDocumentModel.PK
			+ "} FROM {" + SapB2BDocumentModel._TYPECODE + " as p JOIN DocumentStatus as s ON {p.status} = {s.pk}}" + " WHERE {"
			+ SapB2BDocumentModel.CUSTOMERNUMBER + "} = ?customerNumber AND {s.code} = ?status";

	private static final String GET_IVNOICES_BY_INVOICE_NUMBERS = "SELECT {" + SapB2BDocumentModel.PK + "} FROM {"
			+ SapB2BDocumentModel._TYPECODE + " as p JOIN DocumentStatus as s ON {p.status} = {s.pk}}" + " WHERE {"
			+ SapB2BDocumentModel.INVOICENUMBER + "} in ?invoiceNumbers AND {s.code} = ?status";

	private static final String GET_PENDING_PAYMENT_DETAILS_FOR_CURRENT_USER = "SELECT {" + SapB2BDocumentModel.PK + "} FROM {"
			+ SapB2BDocumentModel._TYPECODE + " as p JOIN DocumentStatus as s ON {p.status} = {s.pk}}" + " WHERE {"
			+ SapB2BDocumentModel.CUSTOMERNUMBER + "} = ?customerNumber AND {s.code} = ?status";

	private static final String GET_ALL_B2B_UNITS = "SELECT {" + B2BUnitModel.PK + "} FROM {" + B2BUnitModel._TYPECODE + "}"
			+ " WHERE {" + B2BUnitModel.UID + "} LIKE ?uid";

	private static final String GET_PAYMENT_DETAILS_FROM_JSL_REF_NUMBER = "SELECT {" + SapB2BDocumentModel.PK + "} FROM {"
			+ SapB2BDocumentModel._TYPECODE + "} WHERE {" + SapB2BDocumentModel.JSLREFERENCENUMBER + "} = ?jslrefnumber";

	private static final String GET_SALE_ORDER_PAYMENT_DETAILS_FROM_JSL_REF_NUMBER = "SELECT {" + PaymentSaleOrderModel.PK
			+ "} FROM {" + PaymentSaleOrderModel._TYPECODE + "} WHERE {" + PaymentSaleOrderModel.JSLREFNUMBER + "} = ?jslrefnumber";

	private static final String GET_TRANSACTION_DETAILS_FROM_JSL_REF_NUMBER = "SELECT {" + JslPaymentTransactionModel.PK
			+ "} FROM {" + JslPaymentTransactionModel._TYPECODE + "} WHERE {" + JslPaymentTransactionModel.JSLREFNUMBER
			+ "} = ?jslrefnumber";


	private static final String GET_SALE_ORDER_DETAILS = "SELECT {" + PaymentSaleOrderModel.PK + "} FROM {"
			+ PaymentSaleOrderModel._TYPECODE + "} WHERE {" + PaymentSaleOrderModel.CUSTOMERNUMBER + "} = ?customernumer AND {";

	private static final String FROM_SALE_ORDER_NUMBER = PaymentSaleOrderModel.SALEORDERNUMBER + "} = ?number";

	private static final String FROM_PURCHASE_ORDER_NUMBER = PaymentSaleOrderModel.PURCHASEORDERNUMBER + "} = ?number";

	private static final String SEARCH_BY_INVOICE_NO = " AND " + SapB2BDocumentModel.INVOICENUMBER + " = ?value ";

	private static final String SEARCH_BY_ORDER_NO = " AND " + SapB2BDocumentModel.ORDERNUMBER + " = ?value ";

	private static final String GET_ALL_INVOICES = "SELECT {" + SapB2BDocumentModel.PK + "} FROM {"
			+ SapB2BDocumentModel._TYPECODE;

	private static final String SALE_ORDER_NUMBER = "soNumber";

	private static final String PURCHASE_ORDER_NUMBER = "poNumber";

	private static final String GET_TRANSACTION_DETAILS = "SELECT {" + JslPaymentTransactionModel.PK + "} FROM {"
			+ JslPaymentTransactionModel._TYPECODE + "} WHERE {" + JslPaymentTransactionModel.USER + "} = ?user";

	private static final String TRANSACTION_TYPE = "transaction";

	private static final String GET_TRANSACTION_DETAILS_FROM_DATE = "SELECT {" + JslPaymentTransactionModel.PK + "} FROM {"
			+ JslPaymentTransactionModel._TYPECODE + "} WHERE CAST({" + JslPaymentTransactionModel.CREATIONTIME
			+ "} as DATE) between ?startdate AND ?endDate";

	private static final String GET_IVNOICES_BY_INVOICE_NUMBERS_ADMIN = "SELECT {" + SapB2BDocumentModel.PK + "} FROM {"
			+ SapB2BDocumentModel._TYPECODE + "} WHERE {" + SapB2BDocumentModel.INVOICENUMBER + "} IN (?invoices) ";

	private static final String GET_ADMIN_SALE_ORDER_DETAILS = "SELECT {" + PaymentSaleOrderModel.PK + "} FROM {"
			+ PaymentSaleOrderModel._TYPECODE + "} WHERE {" + PaymentSaleOrderModel.SALEORDERNUMBER + "} IN (?saleorders) ";


	//CAST({startdate} as DATE) between ?startdate AND ?endDate


	@Override
	public List<JslPaymentTransactionModel> getTransactionModels(final String jslRefNumber) throws Exception
	{
		// XXX Auto-generated method stub
		return sessionService.executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public Object execute()
			{
				final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_TRANSACTION_DETAILS_FROM_JSL_REF_NUMBER);
				query.addQueryParameter("jslrefnumber", jslRefNumber);
				final SearchResult<JslPaymentTransactionModel> result = getFlexibleSearchService().search(query);

				return result.getResult();
			}
		}, userService.getAdminUser());
	}

	@Override
	public List<PaymentSaleOrderModel> getSaleOrderPaymentDetailsFromJslRefNumber(final String jslRefNumber) throws Exception
	{
		// XXX Auto-generated method stub
		return sessionService.executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public Object execute()
			{
				final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_SALE_ORDER_PAYMENT_DETAILS_FROM_JSL_REF_NUMBER);
				query.addQueryParameter("jslrefnumber", jslRefNumber);
				final SearchResult<PaymentSaleOrderModel> result = getFlexibleSearchService().search(query);

				return result.getResult();
			}
		}, userService.getAdminUser());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.jsl.core.paymentdetailshistory.dao.PaymentDetailsHistoryDao#getAdminSaleOrderDetailsFromNumber(java.util.List)
	 */
	@Override
	public List<PaymentSaleOrderModel> getAdminSaleOrderDetailsFromNumber(final List<String> saleOrders) throws Exception
	{
		// XXX Auto-generated method stub
		return sessionService.executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public Object execute()
			{
				final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ADMIN_SALE_ORDER_DETAILS);
				query.addQueryParameter("saleorders", saleOrders);
				final SearchResult<PaymentSaleOrderModel> result = getFlexibleSearchService().search(query);

				return result.getResult();
			}
		}, userService.getAdminUser());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.jsl.core.paymentdetailshistory.dao.PaymentDetailsHistoryDao#getAllInvoicesAdminByInvoiceNumbers(java.util.
	 * List)
	 */
	@Override
	public List<SapB2BDocumentModel> getAllInvoicesAdminByInvoiceNumbers(final List<String> invoiceNumbers) throws Exception
	{
		// XXX Auto-generated method stub
		return sessionService.executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public Object execute()
			{
				final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_IVNOICES_BY_INVOICE_NUMBERS_ADMIN);
				query.addQueryParameter("invoices", invoiceNumbers);
				final SearchResult<SapB2BDocumentModel> result = getFlexibleSearchService().search(query);

				return result.getResult().stream()
						.filter(d -> (d.getOpenAmount() != null
								? (((d.getOpenAmount().compareTo(new BigDecimal(d.getGrandTotal())) == 0) ? true : false))
								: false))
						.collect(Collectors.toList());
			}
		}, userService.getAdminUser());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.jsl.core.paymentdetailshistory.dao.PaymentDetailsHistoryDao#getTransactionDetailsFromDate(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public List<JslPaymentTransactionModel> getTransactionDetailsFromDate(final String startDate, final String endDate)
			throws Exception
	{
		// XXX Auto-generated method stub
		return sessionService.executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public Object execute()
			{
				final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_TRANSACTION_DETAILS_FROM_DATE);
				query.addQueryParameter("startdate", startDate);
				query.addQueryParameter("enddate", endDate);
				final SearchResult<JslPaymentTransactionModel> result = getFlexibleSearchService().search(query);

				return result.getResult();
			}
		}, userService.getAdminUser());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.jsl.core.paymentdetailshistory.dao.PaymentDetailsHistoryDao#getTransactionDetailsForCurrentUser()
	 */
	@Override
	public List<JslPaymentTransactionModel> getTransactionDetailsForCurrentUser() throws Exception
	{
		// XXX Auto-generated method stub
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_TRANSACTION_DETAILS);
		query.addQueryParameter("user", userService.getCurrentUser());
		final SearchResult<JslPaymentTransactionModel> result = getFlexibleSearchService().search(query);
		return result.getResult();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.jsl.core.paymentdetailshistory.dao.PaymentDetailsHistoryDao#getSaleOrderDetailsFromNumber(java.lang.String)
	 */
	@Override
	public List<PaymentSaleOrderModel> getSaleOrderDetailsFromNumber(final String searchCriteria, final String number,
			final String type) throws Exception
	{
		// XXX Auto-generated method stub
		final List<PaymentSaleOrderModel> saleOrderList = sessionService.executeInLocalView(new SessionExecutionBody()
		{
			final UserModel currentUser = userService.getCurrentUser();

			@Override
			public Object execute()
			{
				FlexibleSearchQuery query = null;
				if (SALE_ORDER_NUMBER.equalsIgnoreCase(searchCriteria))
				{
					query = new FlexibleSearchQuery(GET_SALE_ORDER_DETAILS + FROM_SALE_ORDER_NUMBER);
					query.addQueryParameter("number", String.format("%010d", Long.parseLong(number)));
				}
				else
				{
					query = new FlexibleSearchQuery(GET_SALE_ORDER_DETAILS + FROM_PURCHASE_ORDER_NUMBER);
					query.addQueryParameter("number", number);
				}
				query.addQueryParameter("customernumer", JslB2bUnitUtil.getParentB2bUnit(currentUser).getUid());
				query.setDisableCaching(true);
				final SearchResult<PaymentSaleOrderModel> result = getFlexibleSearchService().search(query);
				return result.getResult();
			}
		}, userService.getAdminUser());
		if (TRANSACTION_TYPE.equalsIgnoreCase(type))
		{
			return saleOrderList;
		}
		return saleOrderList.stream()
				.filter(s -> (s.getHybrisPaymentStatus() != JslPaymentStatus.PROCESSING && OPEN.equalsIgnoreCase(s.getStatus())))
				.collect(Collectors.toList());
	}

	@Override
	public List<SapB2BDocumentModel> getPaymentHistoryForCurrentUser(final UserModel user) throws Exception
	{
		return getDocumentsFromB2BUnits(getAllB2BUnit(user), DocumentStatus.CLOSED);
	}


	@Override
	public List<SapB2BDocumentModel> getPaymentDetailsFromJslRefNumber(final String jslRefNumber) throws Exception
	{
		// XXX Auto-generated method stub
		return sessionService.executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public Object execute()
			{
				final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_PAYMENT_DETAILS_FROM_JSL_REF_NUMBER);
				query.addQueryParameter("jslrefnumber", jslRefNumber);
				final SearchResult<SapB2BDocumentModel> result = getFlexibleSearchService().search(query);

				return result.getResult();
			}
		}, userService.getAdminUser());

	}

	private List<B2BUnitModel> getAllB2BUnit(final UserModel user) throws Exception
	{
		return sessionService.executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public Object execute()
			{
				final B2BUnitModel b2bUnit = JslB2bUnitUtil.getParentB2bUnit(user);
				final String uid = b2bUnit.getUid() + QUERY_LIKE;
				final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ALL_B2B_UNITS);
				query.setDisableCaching(true);
				query.addQueryParameter("uid", uid);
				final SearchResult<B2BUnitModel> result = getFlexibleSearchService().search(query);
				return result.getResult().stream().filter(b -> (CollectionUtils.isNotEmpty(b.getDocument())))
						.collect(Collectors.toList());
			}
		}, userService.getAdminUser());
	}

	@Override
	public List<SapB2BDocumentModel> getPendingPaymentDetailsForCurrentUser(final UserModel currentUser) throws Exception
	{
		return getDocumentsFromB2BUnits(getAllB2BUnit(currentUser), DocumentStatus.OPEN).stream()
				.filter(d -> (d.getHybrisPaymentStatus() != JslPaymentStatus.PROCESSING
						&& d.getHybrisPaymentStatus() != JslPaymentStatus.SUCCESS
						&& d.getHybrisPaymentStatus() != JslPaymentStatus.PENDING
						&& (d.getOpenAmount() != null
								? (((d.getOpenAmount().compareTo(new BigDecimal(d.getGrandTotal())) == 0) ? true : false))
								: false)
						&& (StringUtils.isEmpty(d.getTermsOfPayment()) ? true : !d.getTermsOfPayment().startsWith("DL"))))
				.collect(Collectors.toList());
	}

	@Override
	public List<SapB2BDocumentModel> getPendingPaymentDetailsBasedOnSearchCriteria(final String searchCriteria,
			final String searchValue, final UserModel currentUser) throws Exception
	{

		final List<SapB2BDocumentModel> documentList = getDocumentsFromB2BUnits(getAllB2BUnit(currentUser), DocumentStatus.OPEN);
		if (SALE_ORDER_NUMBER.equalsIgnoreCase(searchCriteria))
		{
			return documentList.stream().filter(d -> searchValue.equalsIgnoreCase(d.getOrderNumber())).collect(Collectors.toList());
		}
		else if (PURCHASE_ORDER_NUMBER.equalsIgnoreCase(searchCriteria))
		{
			return documentList.stream().filter(d -> searchValue.equalsIgnoreCase(d.getOurTaxNumber())).collect(Collectors.toList());
		}
		else
		{
			return documentList.stream().filter(d -> searchValue.equalsIgnoreCase(d.getInvoiceNumber()))
					.collect(Collectors.toList());
		}
	}

	@Override
	public List<SapB2BDocumentModel> getPendingPaymentDetailsBasedOnSearchCriteria(final String searchCriteria,
			final List<String> searchValue, final UserModel currentUser) throws Exception
	{

		final List<SapB2BDocumentModel> documentList = getDocumentsFromB2BUnits(getAllB2BUnit(currentUser), DocumentStatus.OPEN);
		if (SALE_ORDER_NUMBER.equalsIgnoreCase(searchCriteria))
		{
			return documentList.stream()
					.filter(d -> (searchValue.contains(d.getOrderNumber()) && (d.getOpenAmount() != null
							? (((d.getOpenAmount().compareTo(new BigDecimal(d.getGrandTotal())) == 0) ? true : false))
							: false)))
					.collect(Collectors.toList());
		}
		else if (PURCHASE_ORDER_NUMBER.equalsIgnoreCase(searchCriteria))
		{
			return documentList.stream()
					.filter(d -> (searchValue.contains(d.getOurTaxNumber()) && (d.getOpenAmount() != null
							? (((d.getOpenAmount().compareTo(new BigDecimal(d.getGrandTotal())) == 0) ? true : false))
							: false)))
					.collect(Collectors.toList());
		}
		else
		{
			return documentList.stream()
					.filter(d -> (searchValue.contains(d.getInvoiceNumber()) && (d.getOpenAmount() != null
							? (((d.getOpenAmount().compareTo(new BigDecimal(d.getGrandTotal())) == 0) ? true : false))
							: false)))
					.collect(Collectors.toList());
		}
	}

	@Override
	public List<SapB2BDocumentModel> getallInvoices() throws Exception
	{

		return sessionService.executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public Object execute()
			{
				final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ALL_INVOICES);
				final SearchResult<SapB2BDocumentModel> result = getFlexibleSearchService().search(query);
				return result.getResult();
			}
		}, userService.getAdminUser());
	}

	@Override
	public List<SapB2BDocumentModel> getAllInvoiceDetailsByInvoiceNumbers(final List<String> invoiceNumbers,
			final UserModel currentUser) throws Exception
	{
		final List<SapB2BDocumentModel> documentList = getAllDocumentsFromB2BUnits(getAllB2BUnit(currentUser));
		return documentList.stream().filter(d -> invoiceNumbers.contains(d.getInvoiceNumber())).collect(Collectors.toList());
	}

	@Override
	public List<SapB2BDocumentModel> getAllInvoiceDetailsByInvoiceNumber(final List<String> invoiceNumber) throws Exception
	{
		return sessionService.executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public Object execute()
			{
				final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_IVNOICES_BY_INVOICE_NUMBERS);
				query.addQueryParameter("invoiceNumbers", invoiceNumber);
				query.addQueryParameter("status", DocumentStatus.OPEN);
				final SearchResult<SapB2BDocumentModel> result = getFlexibleSearchService().search(query);
				return result.getResult();
			}
		}, userService.getAdminUser());
	}

	private List<SapB2BDocumentModel> getAllDocumentsFromB2BUnits(final List<B2BUnitModel> list) throws Exception
	{
		return list.stream().flatMap(b -> b.getDocument().stream()).collect(Collectors.toList()).stream().filter(d -> (d != null))
				.map(s -> (SapB2BDocumentModel) s).collect(Collectors.toList());
	}

	private List<SapB2BDocumentModel> getDocumentsFromB2BUnits(final List<B2BUnitModel> list, final DocumentStatus status)
			throws Exception
	{
		if (status.equals(DocumentStatus.OPEN))
		{
			return list.stream().flatMap(b -> b.getDocument().stream()).collect(Collectors.toList()).stream()
					.filter(d -> (d != null && d.getStatus().equals(DocumentStatus.OPEN))).map(s -> (SapB2BDocumentModel) s)
					.collect(Collectors.toList());
		}
		else
		{
			return list.stream().flatMap(b -> b.getDocument().stream()).collect(Collectors.toList()).stream()
					.filter(d -> (d != null && d.getStatus().equals(DocumentStatus.CLOSED))).map(s -> (SapB2BDocumentModel) s)
					.collect(Collectors.toList());
		}
	}


	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	/**
	 * @param flexibleSearchService
	 *           the flexibleSearchService to set
	 */
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}


	/**
	 * @return the sessionService
	 */
	public SessionService getSessionService()
	{
		return sessionService;
	}


	/**
	 * @param sessionService
	 *           the sessionService to set
	 */
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}


	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}


	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}
}
