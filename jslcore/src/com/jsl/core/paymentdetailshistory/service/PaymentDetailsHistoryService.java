/**
 *
 */
package com.jsl.core.paymentdetailshistory.service;

import de.hybris.platform.sap.sapinvoiceaddon.model.SapB2BDocumentModel;

import java.util.List;

import com.jsl.core.model.JslPaymentTransactionModel;
import com.jsl.core.model.PaymentSaleOrderModel;
import com.jsl.facades.paymentdetailshistory.dto.PaymentDetailsDto;


/**
 * @author Kiranraj S.
 *
 */
public interface PaymentDetailsHistoryService
{
	public List<SapB2BDocumentModel> getPaymentHistoryForCurrentUser() throws Exception;

	public List<SapB2BDocumentModel> getPendingPaymentDetailsForCurrentUser() throws Exception;

	public List<SapB2BDocumentModel> getPendingPaymentDetailsBasedOnSearchCriteria(PaymentDetailsDto paymentDetails)
			throws Exception;

	public List<SapB2BDocumentModel> getAllInvoiceDetailsByInvoiceNumbers(List<String> invoiceNumbers) throws Exception;

	public List<PaymentSaleOrderModel> getSaleOrderByNumber(final PaymentDetailsDto paymentDetails, final String type)
			throws Exception;

	public List<JslPaymentTransactionModel> getTransactionDetailsForCurrentUser() throws Exception;

	public List<JslPaymentTransactionModel> getTransactionDetailsFromDate(final String startDate, final String endDate)
			throws Exception;

	public List<SapB2BDocumentModel> getAllInvoicesAdminByInvoiceNumbers(List<String> invoiceNumbers) throws Exception;

	public List<PaymentSaleOrderModel> getAdminSaleOrderDetailsFromNumber(List<String> saleOrders) throws Exception;
}
