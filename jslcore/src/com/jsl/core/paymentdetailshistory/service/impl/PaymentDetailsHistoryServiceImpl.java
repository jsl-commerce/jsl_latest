/**
 *
 */
package com.jsl.core.paymentdetailshistory.service.impl;

import de.hybris.platform.sap.sapinvoiceaddon.model.SapB2BDocumentModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.jsl.core.model.JslPaymentTransactionModel;
import com.jsl.core.model.PaymentSaleOrderModel;
import com.jsl.core.paymentdetailshistory.dao.PaymentDetailsHistoryDao;
import com.jsl.core.paymentdetailshistory.service.PaymentDetailsHistoryService;
import com.jsl.facades.paymentdetailshistory.dto.PaymentDetailsDto;


/**
 * @author Kiranraj S.
 *
 */
public class PaymentDetailsHistoryServiceImpl implements PaymentDetailsHistoryService
{
	private static final Logger LOG = Logger.getLogger(PaymentDetailsHistoryServiceImpl.class);

	private UserService userService;

	private PaymentDetailsHistoryDao paymentDetailsHistoryDao;

	private static final String PAYMENT_TYPE = "payment";

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.jsl.core.paymentdetailshistory.service.PaymentDetailsHistoryService#getAdminSaleOrderDetailsFromNumber(java.
	 * util.List)
	 */
	@Override
	public List<PaymentSaleOrderModel> getAdminSaleOrderDetailsFromNumber(final List<String> saleOrders) throws Exception
	{
		// XXX Auto-generated method stub
		return paymentDetailsHistoryDao.getAdminSaleOrderDetailsFromNumber(saleOrders);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.jsl.core.paymentdetailshistory.service.PaymentDetailsHistoryService#getAllInvoicesAdminByInvoiceNumbers(java.
	 * util.List)
	 */
	@Override
	public List<SapB2BDocumentModel> getAllInvoicesAdminByInvoiceNumbers(final List<String> invoiceNumbers) throws Exception
	{
		// XXX Auto-generated method stub
		return paymentDetailsHistoryDao.getAllInvoicesAdminByInvoiceNumbers(invoiceNumbers);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.jsl.core.paymentdetailshistory.service.PaymentDetailsHistoryService#getTransactionDetailsFromDate(java.lang.
	 * String, java.lang.String)
	 */
	@Override
	public List<JslPaymentTransactionModel> getTransactionDetailsFromDate(final String startDate, final String endDate)
			throws Exception
	{
		// XXX Auto-generated method stub
		final List<JslPaymentTransactionModel> transactionList = new ArrayList<JslPaymentTransactionModel>(
				paymentDetailsHistoryDao.getTransactionDetailsFromDate(startDate, endDate));
		Collections.sort(transactionList,
				(a1, a2) -> (a1 == null || a2 == null) ? 0 : a2.getCreationtime().compareTo(a1.getCreationtime()));
		return transactionList;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.jsl.core.paymentdetailshistory.service.PaymentDetailsHistoryService#getTransactionDetailsForCurrentUser()
	 */
	@Override
	public List<JslPaymentTransactionModel> getTransactionDetailsForCurrentUser() throws Exception
	{
		final List<JslPaymentTransactionModel> transactionList = new ArrayList<JslPaymentTransactionModel>(
				paymentDetailsHistoryDao.getTransactionDetailsForCurrentUser());
		Collections.sort(transactionList,
				(a1, a2) -> (a1 == null || a2 == null) ? 0 : a2.getCreationtime().compareTo(a1.getCreationtime()));

		return transactionList;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.jsl.core.paymentdetailshistory.service.PaymentDetailsHistoryService#getSaleOrderByNumber(java.lang.String)
	 */
	@Override
	public List<PaymentSaleOrderModel> getSaleOrderByNumber(final PaymentDetailsDto paymentDetails, final String type)
			throws Exception
	{
		// XXX Auto-generated method stub
		return paymentDetailsHistoryDao
				.getSaleOrderDetailsFromNumber(paymentDetails.getSearchCriteria(), paymentDetails.getSearchValue(), type).stream()
				.filter(s -> StringUtils.isEmpty(s.getCompanyCode()) ? true
						: s.getCompanyCode().equalsIgnoreCase(paymentDetails.getCompanyCode()))
				.collect(Collectors.toList());

	}

	@Override
	public List<SapB2BDocumentModel> getPaymentHistoryForCurrentUser() throws Exception
	{
		List<SapB2BDocumentModel> paymentDetailsHistoryModelList = new ArrayList<>();
		try
		{
			paymentDetailsHistoryModelList = getPaymentDetailsHistoryDao()
					.getPaymentHistoryForCurrentUser(getUserService().getCurrentUser());
		}
		catch (final Exception e)
		{
			LOG.error("Exception in PaymentDetailsHistoryService.getPaymentHistoryForCurrentUser() :", e);
			throw e;
		}
		return paymentDetailsHistoryModelList;
	}

	@Override
	public List<SapB2BDocumentModel> getPendingPaymentDetailsForCurrentUser() throws Exception
	{
		List<SapB2BDocumentModel> paymentDetailsHistoryModelList = null;
		try
		{
			paymentDetailsHistoryModelList = new ArrayList<SapB2BDocumentModel>(
					getPaymentDetailsHistoryDao().getPendingPaymentDetailsForCurrentUser(getUserService().getCurrentUser()));
		}
		catch (final Exception e)
		{
			LOG.error("Exception in PaymentDetailsHistoryService.getPendingPaymentDetailsForCurrentUser() :", e);
			throw e;
		}
		Collections.sort(paymentDetailsHistoryModelList,
				(a1, a2) -> (a1 == null || a2 == null) ? 0 : a1.getDueDate().compareTo(a2.getDueDate()));

		return paymentDetailsHistoryModelList;
	}

	@Override
	public List<SapB2BDocumentModel> getPendingPaymentDetailsBasedOnSearchCriteria(final PaymentDetailsDto paymentDetails)
			throws Exception
	{
		List<SapB2BDocumentModel> paymentDetailsHistoryModelList = new ArrayList<>();
		try
		{
			paymentDetailsHistoryModelList = getPaymentDetailsHistoryDao().getPendingPaymentDetailsBasedOnSearchCriteria(
					paymentDetails.getSearchCriteria(), paymentDetails.getSearchValue(), getUserService().getCurrentUser());
		}
		catch (final Exception e)
		{
			LOG.error("Exception in PaymentDetailsHistoryService.getPendingPaymentDetailsBasedOnSearchCriteria() :", e);
			throw e;
		}
		return paymentDetailsHistoryModelList;
	}

	@Override
	public List<SapB2BDocumentModel> getAllInvoiceDetailsByInvoiceNumbers(final List<String> invoiceNumbers) throws Exception
	{
		List<SapB2BDocumentModel> invoiceList = new ArrayList<>();
		try
		{
			invoiceList = getPaymentDetailsHistoryDao().getAllInvoiceDetailsByInvoiceNumbers(invoiceNumbers,
					getUserService().getCurrentUser());
		}
		catch (final Exception e)
		{
			LOG.error("Exception in PaymentDetailsHistoryService.getAllInvoiceDetailsByInvoiceNumber() :", e);
			throw e;
		}
		return invoiceList;
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	/**
	 * @return the paymentDetailsHistoryDao
	 */
	public PaymentDetailsHistoryDao getPaymentDetailsHistoryDao()
	{
		return paymentDetailsHistoryDao;
	}

	/**
	 * @param paymentDetailsHistoryDao
	 *           the paymentDetailsHistoryDao to set
	 */
	public void setPaymentDetailsHistoryDao(final PaymentDetailsHistoryDao paymentDetailsHistoryDao)
	{
		this.paymentDetailsHistoryDao = paymentDetailsHistoryDao;
	}
}
