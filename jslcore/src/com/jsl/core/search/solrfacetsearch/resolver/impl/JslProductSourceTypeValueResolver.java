/**
 * 
 */
package com.jsl.core.search.solrfacetsearch.resolver.impl;

import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import com.jsl.core.search.solrfacetsearch.service.JslProductSourceService;


/**
 * @author himanshu.sial
 *
 */
public class JslProductSourceTypeValueResolver extends AbstractValueResolver<ProductModel, Object, Object>
{

	private FieldNameProvider fieldNameProvider;

	private JslProductSourceService jslProductSourceService;


	protected FieldNameProvider getFieldNameProvider()
	{
		return fieldNameProvider;
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver#addFieldValues(de.hybris.platform.
	 * solrfacetsearch.indexer.spi.InputDocument, de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext,
	 * de.hybris.platform.solrfacetsearch.config.IndexedProperty, de.hybris.platform.core.model.ItemModel,
	 * de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver.ValueResolverContext)
	 */
	@Override
	protected void addFieldValues(final InputDocument document, final IndexerBatchContext paramIndexerBatchContext,
			final IndexedProperty indexedProperty, final ProductModel product,
			final de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver.ValueResolverContext<Object, Object> paramValueResolverContext)
			throws FieldValueProviderException
	{
		addProductSource(document, indexedProperty, product);
	}


	private void addProductSource(final InputDocument document, final IndexedProperty indexedProperty, final ProductModel product)
			throws FieldValueProviderException
	{

		String productType = jslProductSourceService.getProductSourceType(product).getCode();
		//Passed language as null.
		final Collection<String> fieldNames = getFieldNameProvider().getFieldNames(indexedProperty,null);
		for (final String fieldName : fieldNames)
		{
			//To-Do - yard / plant		
			document.addField(fieldName, productType);
		}
	}




	@Required
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}

	/**
	 * @return the jslProductSourceService
	 */
	public JslProductSourceService getJslProductSourceService()
	{
		return jslProductSourceService;
	}

	/**
	 * @param jslProductSourceService
	 *           the jslProductSourceService to set
	 */
	@Required
	public void setJslProductSourceService(JslProductSourceService jslProductSourceService)
	{
		this.jslProductSourceService = jslProductSourceService;
	}


}
