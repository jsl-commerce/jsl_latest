/**
 *
 */
package com.jsl.core.search.solrfacetsearch.resolver.impl;

import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.stock.StockService;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.jsl.core.constants.JslCoreConstants;


/**
 * @author manav.magoo
 *
 */
public class JslProductWareshouseStockValueResolver extends AbstractValueResolver<ProductModel, Object, Object>
{
	private FieldNameProvider fieldNameProvider;

	private StockService stockService;

	protected FieldNameProvider getFieldNameProvider()
	{
		return fieldNameProvider;
	}

	@Required
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}

	/**
	 * @return the stockService
	 */
	public StockService getStockService()
	{
		return stockService;
	}

	/**
	 * @param stockService
	 *           the stockService to set
	 */
	@Required
	public void setStockService(final StockService stockService)
	{
		this.stockService = stockService;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver#addFieldValues(de.hybris.platform.
	 * solrfacetsearch.indexer.spi.InputDocument, de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext,
	 * de.hybris.platform.solrfacetsearch.config.IndexedProperty, de.hybris.platform.core.model.ItemModel,
	 * de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver.ValueResolverContext)
	 */
	@Override
	protected void addFieldValues(final InputDocument document, final IndexerBatchContext paramIndexerBatchContext,
			final IndexedProperty indexedProperty, final ProductModel product,
			final de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver.ValueResolverContext<Object, Object> paramValueResolverContext)
			throws FieldValueProviderException
	{
		addWarehouseStock(document, indexedProperty, product);
	}

	/**
	 * @param document
	 * @param indexedProperty
	 * @param product
	 * @throws FieldValueProviderException
	 */
	private void addWarehouseStock(final InputDocument document, final IndexedProperty indexedProperty, final ProductModel product)
			throws FieldValueProviderException
	{
		Collection<String> fieldNames;
		final Collection<StockLevelModel> stockLevelModel = this.stockService.getAllStockLevels(product);
		for (final StockLevelModel eachStockLevelModel : stockLevelModel)
		{
			final WarehouseModel warehouseModel = eachStockLevelModel.getWarehouse();
			final Collection<BaseStoreModel> warehouseBaseStores = warehouseModel.getBaseStores();
			StockLevelStatus inStockStatus = null;
			if (CollectionUtils.isEmpty(warehouseBaseStores))
			{
				inStockStatus = StockLevelStatus.OUTOFSTOCK;
			}
			else
			{
				inStockStatus = getStockService().getProductStatus(product, warehouseModel);
			}
			final String warehouseCode = eachStockLevelModel.getWarehouse().getPk().toString();
			fieldNames = getFieldNames(indexedProperty, warehouseCode);
			if (fieldNames != null)
			{
				for (final String fieldName : fieldNames)
				{
					if (inStockStatus != null)
					{
						addField(document, fieldName, indexedProperty, inStockStatus.getCode());
					}
				}
			}
		}
	}



	protected Collection<String> getFieldNames(final IndexedProperty indexedProperty, final String warehouseCode)
			throws FieldValueProviderException
	{
		Collection<String> fieldNames = null;
		if (!StringUtils.isEmpty(warehouseCode))
		{
			fieldNames = getFieldNameProvider().getFieldNames(indexedProperty, warehouseCode);
		}

		return fieldNames;
	}

	private void addField(final InputDocument document, final String fieldName, final IndexedProperty indexedProperty,
			final String value) throws FieldValueProviderException
	{

		if (JslCoreConstants.STOCK_SORT_CODE.equals(indexedProperty.getValueProviderParameter()))
		{
			//If indexing is for stock sort than for OUTOFSTOCK 0 is indexed else 1
			if (InStockStatus.FORCEOUTOFSTOCK.getCode().equalsIgnoreCase(value))
			{
				document.addField(fieldName, 0);
			}
			else
			{
				document.addField(fieldName, 1);
			}
		}
		else
		{
			//for warehouse indexing actual instockstatus code will be indexed
			document.addField(fieldName, value);
		}
	}

}
