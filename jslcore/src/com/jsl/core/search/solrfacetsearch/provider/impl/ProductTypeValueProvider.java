/**
 *
 */
package com.jsl.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * @author manav.magoo
 *
 */
public class ProductTypeValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider
{


	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.solrfacetsearch.provider.FieldValueProvider#getFieldValues(de.hybris.platform.solrfacetsearch.
	 * config.IndexConfig, de.hybris.platform.solrfacetsearch.config.IndexedProperty, java.lang.Object)
	 */
	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig var1, final IndexedProperty var2, final Object var3)
			throws FieldValueProviderException
	{

		final ProductModel product = (ProductModel) var3;
		final Collection<FieldValue> fieldValues = new ArrayList<FieldValue>();
		fieldValues.addAll(createFieldValue(product.getProductType(), var2));
		return fieldValues;

	}

	protected List<FieldValue> createFieldValue(final String productType, final IndexedProperty indexedProperty)
	{
		final List<FieldValue> fieldValues = new ArrayList<FieldValue>();
		fieldValues.add(new FieldValue(productType, productType));

		return fieldValues;
	}


}
