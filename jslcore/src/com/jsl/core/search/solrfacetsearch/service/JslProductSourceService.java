/**
 * 
 */
package com.jsl.core.search.solrfacetsearch.service;

import de.hybris.platform.core.model.product.ProductModel;

import com.jsl.core.enums.JslProductSourceType;

/**
 * @author himanshu.sial
 *
 */
public interface JslProductSourceService
{
	public JslProductSourceType getProductSourceType(ProductModel productModel);	
}
