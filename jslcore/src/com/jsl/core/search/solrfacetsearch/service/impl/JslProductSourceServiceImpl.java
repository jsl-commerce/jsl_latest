/**
 * 
 */
package com.jsl.core.search.solrfacetsearch.service.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.util.Config;

import java.util.Arrays;
import java.util.List;

import com.jsl.core.enums.JslProductSourceType;
import com.jsl.core.search.solrfacetsearch.service.JslProductSourceService;


/**
 * @author himanshu.sial
 *
 */
public class JslProductSourceServiceImpl implements JslProductSourceService
{

	private static final String JSL_PRODUCT_YARD_CODE_KEY = "jsl.product.yard.codes";
	private static final String JSL_PRODUCT_PLANT_CODE_KEY = "jsl.product.plant.codes";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.jsl.core.search.solrfacetsearch.service.JslProductSourceService#getProductSourceType(de.hybris.platform.core.
	 * model.product.ProductModel)
	 */
	@Override
	public JslProductSourceType getProductSourceType(ProductModel productModel)
	{
		List<String> yardList = Arrays.asList(Config.getParameter(JSL_PRODUCT_YARD_CODE_KEY).split(","));
		List<String> plantList = Arrays.asList(Config.getParameter(JSL_PRODUCT_PLANT_CODE_KEY).split(","));

		// XXX Auto-generated method stub		
		if (yardList.stream().anyMatch(s -> productModel.getPlant().startsWith(s)))
		{
			return JslProductSourceType.WAREHOUSE;
		}
		else if (plantList.stream().anyMatch(s -> productModel.getPlant().startsWith(s)))
		{
			return JslProductSourceType.PLANT;
		}
		return JslProductSourceType.PLANT;
	}

}
