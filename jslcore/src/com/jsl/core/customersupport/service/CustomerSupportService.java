/**
 *
 */
package com.jsl.core.customersupport.service;

import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.jsl.core.model.CustomerComplaintModel;
import com.jsl.core.model.InvoiceDetailsModel;
import com.jsl.core.model.JslCustomerFeedbackModel;
import com.jsl.facades.supportticket.dto.CustomerComplaintDto;


/**
 * @author Kiranraj S.
 *
 */
public interface CustomerSupportService
{

	public void submitInvoiceDetails(List<InvoiceDetailsModel> invoiceDetailsLists);

	public Map<String, String> submitComplaint(CustomerComplaintModel customerComplaint, List<MultipartFile> attachment)
			throws Exception;

	public List<CustomerComplaintModel> getSupportTicketHistory();

	public List<CustomerComplaintModel> getAllComplaintListForAdmin();

	public boolean submitCustomerFeedBack(CustomerComplaintDto complaintDto);

	public boolean submitCustomerFeedBackForm(final JslCustomerFeedbackModel feedbackModel, String complaintNumber)
			throws Exception;

	public List<JslCustomerFeedbackModel> getAllCustomerFeedbackFormData() throws Exception;

	public boolean submitCustomerSatisfaction(CustomerComplaintDto complaintDto) throws Exception;

	public CustomerComplaintModel isBatchAlreadyAdded(final String invoiceNumber, final String batchNumber) throws Exception;
}
