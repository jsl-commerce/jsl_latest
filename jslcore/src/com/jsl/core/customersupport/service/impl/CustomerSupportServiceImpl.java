/**
 *
 */
package com.jsl.core.customersupport.service.impl;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.jsl.core.constants.JslCoreConstants;
import com.jsl.core.customer.complaint.request.dto.CustomerComplaintRequestDto;
import com.jsl.core.customer.complaint.request.dto.CustomerComplaintServiceRequestDto;
import com.jsl.core.customer.complaint.request.dto.ServiceRequestAttachmentFolderC;
import com.jsl.core.customer.complaint.request.dto.ServiceRequestAttachmentFolderP;
import com.jsl.core.customer.complaint.request.dto.ServiceRequestOtherParty;
import com.jsl.core.customer.complaint.request.dto.ServiceRequestReferenceC;
import com.jsl.core.customer.complaint.request.dto.ServiceRequestReferenceP;
import com.jsl.core.customer.complaint.request.dto.ServiceRequestTextCollectionC;
import com.jsl.core.customer.complaint.request.dto.ServiceRequestTextCollectionP;
import com.jsl.core.customer.complaint.response.dto.CustomerComplaintReponseDto;
import com.jsl.core.customersupport.dao.CustomerSupportDao;
import com.jsl.core.customersupport.service.CustomerSupportService;
import com.jsl.core.model.CustomerComplaintModel;
import com.jsl.core.model.InvoiceDetailsModel;
import com.jsl.core.model.JslCustomerFeedbackModel;
import com.jsl.core.util.JslB2bUnitUtil;
import com.jsl.facades.supportticket.dto.CustomerComplaintDto;
import com.jsl.rest.service.JslRestService;


/**
 * @author Kiranraj S.
 *
 */
//@Service
public class CustomerSupportServiceImpl implements CustomerSupportService
{
	private static final Logger LOG = Logger.getLogger(CustomerSupportServiceImpl.class);

	private static final String HYPHEN = "-";

	@Resource(name = "jslRestServiceImpl")
	private JslRestService jslRestService;

	private static final String CUSTOMER_COMPLAINT_URL_KEY = "customer.complaint.url";

	@Autowired
	private ModelService modelService;

	@Autowired
	private UserService userService;

	@Autowired
	CustomerSupportDao csDao;


	/*
	 * (non-Javadoc)
	 *
	 * @see com.jsl.core.customersupport.service.CustomerSupportService#isBatchAlreadyAdded(java.lang.String)
	 */
	@Override
	public CustomerComplaintModel isBatchAlreadyAdded(final String invoiceNumber, final String batchNumber) throws Exception
	{
		// XXX Auto-generated method stub
		final List<CustomerComplaintModel> complaintList = csDao.getComplaintByInvoiceNumber(invoiceNumber);

		CustomerComplaintModel complaintModel = null;
		if (complaintList.isEmpty())
		{
			complaintModel = null;
		}
		else
		{
			for (final CustomerComplaintModel complaint : complaintList)
			{
				final List<String> batchList = complaint.getComplaintInvoice().stream().map(InvoiceDetailsModel::getBatchNumber)
						.collect(Collectors.toList());

				if (batchList.contains(batchNumber))
				{
					complaintModel = complaint;
				}
			}
		}
		return complaintModel;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.jsl.core.customersupport.service.CustomerSupportService#submitCustomerSatisfaction(com.jsl.facades.
	 * supportticket.dto.CustomerComplaintDto)
	 */
	@Override
	public boolean submitCustomerSatisfaction(final CustomerComplaintDto complaintDto) throws Exception
	{
		// XXX Auto-generated method stub
		boolean result = false;
		final CustomerComplaintModel customerComplaintModel = csDao.getComplaintByNumber(complaintDto.getComplaintNumber());
		if (customerComplaintModel != null)
		{
			customerComplaintModel.setCustomerSatisfaction(complaintDto.getCustomerSatisfaction());
			customerComplaintModel.setSatisfactionSubmitted(true);

			final CustomerComplaintRequestDto customerComplaintRequestDto = new CustomerComplaintRequestDto();
			final CustomerComplaintServiceRequestDto customerComplaintServiceRequestDto = new CustomerComplaintServiceRequestDto();

			customerComplaintServiceRequestDto.setComplaintId(customerComplaintModel.getComplaintNumber());
			customerComplaintServiceRequestDto.setzCustomerRemark_SDK(customerComplaintModel.getCustomerSatisfaction());
			customerComplaintRequestDto.setCustomerComplaintServiceRequestDto(customerComplaintServiceRequestDto);


			final Object response = jslRestService.postForObjectCommand(Config.getParameter(CUSTOMER_COMPLAINT_URL_KEY),
					customerComplaintRequestDto, Object.class, "", "", MediaType.APPLICATION_ATOM_XML);
			modelService.save(customerComplaintModel);
			result = true;
		}
		else
		{
			throw new Exception("No complaint with number exists : " + complaintDto.getComplaintNumber());
		}
		return result;
	}


	@Override
	public void submitInvoiceDetails(final List<InvoiceDetailsModel> invoiceDetailsLists)
	{
		getModelService().saveAll(invoiceDetailsLists);
	}

	@Override
	public Map<String, String> submitComplaint(final CustomerComplaintModel customerComplaint,
			final List<MultipartFile> attachment) throws Exception
	{
		customerComplaint.setUser(getUserService().getCurrentUser());
		final Map<String, String> response = createCustomerComplaintInC4C(customerComplaint, attachment);
		if (StringUtils.hasText(response.get(JslCoreConstants.C4C_COMPLAINT_ID)))
		{
			customerComplaint.setComplaintNumber(response.get(JslCoreConstants.C4C_COMPLAINT_ID));
			customerComplaint.setStatus(response.get("Status"));
			customerComplaint.setUuid(response.get("uuid"));
			customerComplaint.setFeedbackSubmitted(false);
		}
		else
		{
			throw new Exception("Error in getting response check logs.");
		}
		getModelService().save(customerComplaint);
		return response;
	}

	@Override
	public List<CustomerComplaintModel> getSupportTicketHistory()
	{
		return getCsDao().getSupportTicketHistory(getUserService().getCurrentUser());
	}

	@Override
	public List<CustomerComplaintModel> getAllComplaintListForAdmin()
	{
		return getCsDao().getAllComplaintListForAdmin();
	}

	@Override
	public boolean submitCustomerFeedBack(final CustomerComplaintDto complaintDto)
	{
		boolean feedBackSubmitted = false;
		final CustomerComplaintModel complaintModel = getCsDao().getComplaintByNumber(complaintDto.getComplaintNumber());
		if (null != complaintModel)
		{
			complaintModel.setCustomerFeedBackComment(complaintDto.getCustomerFeedBackComment());
			complaintModel.setCustomerFeedBackRating(Integer.valueOf(complaintDto.getCustomerFeedBackRating()));
			getModelService().save(complaintModel);
			feedBackSubmitted = true;
		}
		return feedBackSubmitted;
	}

	@Override
	public boolean submitCustomerFeedBackForm(final JslCustomerFeedbackModel feedbackModel, final String complaintNumber)
			throws Exception
	{
		boolean feedBackSubmitted = false;
		final CustomerComplaintModel customerComplaintModel = csDao.getComplaintByNumber(complaintNumber);
		if (null != customerComplaintModel)
		{
			feedbackModel.setUser(userService.getCurrentUser());
			feedbackModel.setCustomerComplaint(customerComplaintModel);
			customerComplaintModel.setFeedbackSubmitted(true);
			getModelService().save(customerComplaintModel);
			getModelService().save(feedbackModel);
			feedBackSubmitted = true;
		}
		return feedBackSubmitted;
	}

	@Override
	public List<JslCustomerFeedbackModel> getAllCustomerFeedbackFormData() throws Exception
	{
		return csDao.getAllCustomerFeedbackFormData();
	}

	private Map<String, String> createCustomerComplaintInC4C(final CustomerComplaintModel customerComplaintModel,
			final List<MultipartFile> attachment) throws IOException
	{
		// XXX Auto-generated method stub
		final Map<String, String> customerMap = getCustomerId();
		final CustomerComplaintRequestDto request = createComplaintRequest(customerComplaintModel, attachment, customerMap);
		final CustomerComplaintReponseDto response = jslRestService.postForObjectCommand(
				Config.getParameter(CUSTOMER_COMPLAINT_URL_KEY), request, CustomerComplaintReponseDto.class, "", "",
				MediaType.APPLICATION_ATOM_XML);
		if (null != response)
		{
			customerMap.put(JslCoreConstants.C4C_COMPLAINT_ID, response.getCustomerComplaintServiceDto().getId());
			customerMap.put("Status", response.getCustomerComplaintServiceDto().getServiceRequestUserLifeCycleStatusCodeText());
			customerMap.put("uuid", response.getCustomerComplaintServiceDto().getuUID());
		}

		return customerMap;
	}

	private CustomerComplaintRequestDto createComplaintRequest(final CustomerComplaintModel customerComplaintModel,
			final List<MultipartFile> attachment, final Map<String, String> map) throws IOException
	{
		final CustomerComplaintRequestDto customerComplaintRequestDto = new CustomerComplaintRequestDto();
		final CustomerComplaintServiceRequestDto customerComplaintServiceRequestDto = new CustomerComplaintServiceRequestDto();

		customerComplaintServiceRequestDto.setName(map.get(JslCoreConstants.CUSTOMER_NAME) + HYPHEN
				+ customerComplaintModel.getTypeOfComplaint() + HYPHEN + customerComplaintModel.getContactPersonName() + HYPHEN
				+ LocalDateTime.now().format(DateTimeFormatter.ofPattern("MM/dd/yyyy")));
		//customerComplaintModel.getComplaintDescription());
		customerComplaintServiceRequestDto.setCustomerId(map.get(JslCoreConstants.CUSTOMER_ID));
		customerComplaintServiceRequestDto.setProcessingTypeCode(customerComplaintModel.getTypeOfComplaint());
		customerComplaintServiceRequestDto.setContactSDK(customerComplaintModel.getContactNumber());
		customerComplaintServiceRequestDto.setMaterialLocation(customerComplaintModel.getMaterialLocation());
		customerComplaintServiceRequestDto.setReportedBy(customerComplaintModel.getContactPersonName());
		customerComplaintServiceRequestDto.setNatureOfComplaints(customerComplaintModel.getNatureOfComplaints());
		customerComplaintServiceRequestDto.setCompanyCode(customerComplaintModel.getInvoiceCompanyCode());
		customerComplaintServiceRequestDto.setSalesDistrict(customerComplaintModel.getSalesDistrict());
		customerComplaintServiceRequestDto.setSalesGroup(customerComplaintModel.getSalesGroup());
		customerComplaintServiceRequestDto.setSalesOffice(customerComplaintModel.getSalesOffice());
		customerComplaintServiceRequestDto.setPresentMaterialConditions(customerComplaintModel.getPresentMaterialCondition());
		customerComplaintServiceRequestDto.setSalesOrg(customerComplaintModel.getSalesOrg());

		if (!customerComplaintModel.getComplaintInvoice().isEmpty())
		{
			final ServiceRequestReferenceP serviceRequestReferenceP = new ServiceRequestReferenceP();
			final List<ServiceRequestReferenceC> listServiceRequestReferenceC = new ArrayList<ServiceRequestReferenceC>();

			customerComplaintModel.getComplaintInvoice().forEach(c -> {
				final ServiceRequestReferenceC serviceRequestReferenceC = new ServiceRequestReferenceC();
				serviceRequestReferenceC.setInvoiceNo(customerComplaintModel.getInvoiceNumber());
				serviceRequestReferenceC.setExternalProductId(c.getProductCode());
				serviceRequestReferenceC.setPlantSDK(c.getPlant());
				serviceRequestReferenceC.setPrimaryDefect(c.getPrimaryDefect());
				serviceRequestReferenceC.setSalesOffice(customerComplaintModel.getSalesOffice());
				serviceRequestReferenceC.setBatchNumber(c.getBatchNumber());
				serviceRequestReferenceC.setComplaintQuantity(c.getComplaintQuantity());
				serviceRequestReferenceC.setFinish(c.getFinish());
				serviceRequestReferenceC.setGrade(c.getGrade());
				serviceRequestReferenceC.setGradeGroup(c.getGradeGroup());
				serviceRequestReferenceC.setInvoiceDate(c.getInvoiceDate());
				serviceRequestReferenceC.setKamId(c.getKamId());
				serviceRequestReferenceC.setKamName(c.getKamName());
				serviceRequestReferenceC.setLength(c.getLength());
				//serviceRequestReferenceC.setPriceGroup("2");
				serviceRequestReferenceC.setQuantity(c.getSupplyQuantity());
				serviceRequestReferenceC.setSeries(c.getSeries());
				serviceRequestReferenceC.setSoNo(c.getSoNum());
				serviceRequestReferenceC.setStandard(c.getStandard());
				serviceRequestReferenceC.setThickness(c.getThickness());
				serviceRequestReferenceC.setUsage(c.getUsage());
				serviceRequestReferenceC.setWidth(c.getWidth());
				serviceRequestReferenceC.setEdge(c.getEdge());
				listServiceRequestReferenceC.add(serviceRequestReferenceC);
			});
			serviceRequestReferenceP.setServiceRequestReferenceCList(listServiceRequestReferenceC);

			final List<ServiceRequestOtherParty> serviceRequestOtherParties = new ArrayList<ServiceRequestOtherParty>();
			final ServiceRequestOtherParty otherParty = new ServiceRequestOtherParty();
			otherParty.setPartyTypeCode("147");
			otherParty.setRoleCode("Z90");
			if (CollectionUtils.isNotEmpty(listServiceRequestReferenceC))
			{
				otherParty.setPartyId(listServiceRequestReferenceC.get(0).getKamId());
				customerComplaintServiceRequestDto.setKamID(listServiceRequestReferenceC.get(0).getKamId());
			}
			serviceRequestOtherParties.add(otherParty);
			customerComplaintServiceRequestDto.setServiceRequestOtherParty(serviceRequestOtherParties);
			customerComplaintServiceRequestDto.setServiceRequestReferenceObjectP(serviceRequestReferenceP);
		}

		final ServiceRequestTextCollectionP serviceRequestTextCollectionP = new ServiceRequestTextCollectionP();
		final ServiceRequestTextCollectionC serviceRequestTextCollectionC = new ServiceRequestTextCollectionC();

		serviceRequestTextCollectionC.setLanguageCode("EN");
		serviceRequestTextCollectionC.setTypeCode("10004");
		serviceRequestTextCollectionC.setText(customerComplaintModel.getContactPersonName() + " : "
				+ (!StringUtils.isEmpty(customerComplaintModel.getComplaintDescription())
						? customerComplaintModel.getComplaintDescription() : ""));
		serviceRequestTextCollectionP.setServiceRequestTextCollectionC(serviceRequestTextCollectionC);
		customerComplaintServiceRequestDto.setServiceRequestTextCollectionP(serviceRequestTextCollectionP);
		if (null != attachment && CollectionUtils.isNotEmpty(attachment))
		{
			final ServiceRequestAttachmentFolderP serviceRequestAttachmentFolderP = new ServiceRequestAttachmentFolderP();
			final List<ServiceRequestAttachmentFolderC> listServiceRequestAttachmentFolderC = new ArrayList<ServiceRequestAttachmentFolderC>();
			for (final MultipartFile file : attachment)
			{
				final ServiceRequestAttachmentFolderC serviceRequestAttachmentFolderC = new ServiceRequestAttachmentFolderC();
				serviceRequestAttachmentFolderC.setCategoryCode("2");
				serviceRequestAttachmentFolderC.setBinary(Base64.getEncoder().encodeToString(file.getBytes()));
				serviceRequestAttachmentFolderC.setName(file.getOriginalFilename());
				serviceRequestAttachmentFolderC.setTypeCode("10001");
				listServiceRequestAttachmentFolderC.add(serviceRequestAttachmentFolderC);
			}
			serviceRequestAttachmentFolderP.setServiceRequestAttachmentFolderC(listServiceRequestAttachmentFolderC);
			customerComplaintServiceRequestDto.setServiceRequestAttachmentFolder(serviceRequestAttachmentFolderP);
		}
		customerComplaintRequestDto.setCustomerComplaintServiceRequestDto(customerComplaintServiceRequestDto);

		return customerComplaintRequestDto;
	}

	private Map<String, String> getCustomerId()
	{

		final Map<String, String> response = new HashMap<String, String>();

		final UserModel user = getUserService().getCurrentUser();

		final B2BUnitModel parentb2bUnit = JslB2bUnitUtil.getParentB2bUnit(user);

		if (parentb2bUnit != null)
		{
			response.put(JslCoreConstants.CUSTOMER_ID, parentb2bUnit.getUid());
			response.put(JslCoreConstants.CUSTOMER_NAME, parentb2bUnit.getDisplayName());
		}
		else
		{
			response.put(JslCoreConstants.CUSTOMER_ID, user.getUid());
			response.put(JslCoreConstants.CUSTOMER_NAME, user.getDisplayName());
		}
		return response;
	}


	/**
	 * @return the csDao
	 */
	public CustomerSupportDao getCsDao()
	{
		return csDao;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

}
