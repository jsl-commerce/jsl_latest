/**
 *
 */
package com.jsl.core.customersupport.dao;

import de.hybris.platform.core.model.user.UserModel;

import java.util.List;

import com.jsl.core.model.CustomerComplaintModel;
import com.jsl.core.model.JslCustomerFeedbackModel;


/**
 * @author Kirnaraj S.
 *
 */
public interface CustomerSupportDao
{
	public List<CustomerComplaintModel> getSupportTicketHistory(UserModel currentUser);

	public List<CustomerComplaintModel> getAllComplaintListForAdmin();

	public List<CustomerComplaintModel> getAllComplaintListForAdminByDateRange(String startDate, String endDate);

	public CustomerComplaintModel getComplaintByNumber(String complaintNumber);

	public List<JslCustomerFeedbackModel> getAllCustomerFeedbackFormData() throws Exception;

	public List<CustomerComplaintModel> getComplaintByInvoiceNumber(final String invoiceNumber) throws Exception;
}
