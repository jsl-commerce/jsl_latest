/**
 *
 */
package com.jsl.core.customersupport.dao.impl;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.jsl.core.customersupport.dao.CustomerSupportDao;
import com.jsl.core.model.CustomerComplaintModel;
import com.jsl.core.model.JslCustomerFeedbackModel;



/**
 * @author Kiranraj S.
 *
 */

public class CustomerSupportDaoImpl implements CustomerSupportDao
{
	private static final Logger LOG = Logger.getLogger(CustomerSupportDaoImpl.class);

	@Autowired
	private FlexibleSearchService flexibleSearchService;

	private UserService userService;

	private SessionService sessionService;

	private static final String GET_ALL_SUPPORT_TICKET = "SELECT {pk} FROM {" + CustomerComplaintModel._TYPECODE + "} " + "where {"
			+ CustomerComplaintModel.USER + "} = ?currentuser";

	private static final String GET_ALL_REGISTERED_COMPLAINT_LIST = "SELECT {cc.pk} FROM {" + CustomerComplaintModel._TYPECODE
			+ " as cc}";

	private static final String GET_ALL_CUSTOMER_FEEDBACK_FORM_LIST = "SELECT {" + JslCustomerFeedbackModel.PK + "} FROM {"
			+ JslCustomerFeedbackModel._TYPECODE + " }";

	private static final String GET_ALL_COMPLAINT_LIST_BY_INVOICE_NUMBER = "SELECT {" + CustomerComplaintModel.PK + "} FROM {"
			+ CustomerComplaintModel._TYPECODE + "} WHERE {" + CustomerComplaintModel.INVOICENUMBER + "} = ?invoicenumber";

	@Override
	public List<CustomerComplaintModel> getSupportTicketHistory(final UserModel currentUser)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ALL_SUPPORT_TICKET);
		query.addQueryParameter("currentuser", currentUser);
		query.setDisableCaching(true);
		final SearchResult<CustomerComplaintModel> res = getFlexibleSearchService().search(query);
		return res.getResult();
	}

	@Override
	public List<JslCustomerFeedbackModel> getAllCustomerFeedbackFormData() throws Exception
	{
		return sessionService.executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public Object execute()
			{
				final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ALL_CUSTOMER_FEEDBACK_FORM_LIST);
				query.setDisableCaching(true);
				final SearchResult<JslCustomerFeedbackModel> res = getFlexibleSearchService().search(query);
				return res.getResult();
			}
		}, userService.getAdminUser());
	}

	@Override
	public List<CustomerComplaintModel> getAllComplaintListForAdmin()
	{
		List<CustomerComplaintModel> complaintList = Collections.EMPTY_LIST;
		try
		{
			SearchResult<CustomerComplaintModel> result = null;
			final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ALL_REGISTERED_COMPLAINT_LIST);
			result = getFlexibleSearchService().search(query);
			complaintList = result.getResult();
		}
		catch (final Exception e)
		{
			LOG.error("Exception in CustomerSupportDaoImpl.getAllComplaintListForAdmin(): ", e);
		}
		return complaintList;
	}

	@Override
	public List<CustomerComplaintModel> getAllComplaintListForAdminByDateRange(final String startDate, final String endDate)
	{
		List<CustomerComplaintModel> complaintList = Collections.EMPTY_LIST;
		try
		{
			final StringBuilder dateRangeQuery = new StringBuilder(GET_ALL_REGISTERED_COMPLAINT_LIST);
			dateRangeQuery.append(" Where CAST({cc.creationtime} as DATE) between ?startDate AND ?endDate");
			SearchResult<CustomerComplaintModel> result = null;
			final FlexibleSearchQuery query = new FlexibleSearchQuery(dateRangeQuery);
			query.addQueryParameter("startDate", startDate);
			query.addQueryParameter("endDate", endDate);
			result = getFlexibleSearchService().search(query);
			complaintList = result.getResult();
		}
		catch (final Exception e)
		{
			LOG.error("Exception in CustomerSupportDaoImpl.getAllComplaintListForAdminByDateRange(): ", e);
		}
		return complaintList;
	}


	@Override
	public CustomerComplaintModel getComplaintByNumber(final String complaintNumber)
	{
		final SearchResult<CustomerComplaintModel> result = null;
		try
		{
			final StringBuilder Query = new StringBuilder(GET_ALL_REGISTERED_COMPLAINT_LIST);
			Query.append(" Where {" + CustomerComplaintModel.COMPLAINTNUMBER + "} = ?complaintNumber");
			final FlexibleSearchQuery query = new FlexibleSearchQuery(Query);
			query.addQueryParameter("complaintNumber", complaintNumber);
			return (CustomerComplaintModel) getFlexibleSearchService().searchUnique(query);
		}
		catch (final Exception e)
		{
			LOG.error("Exception in CustomerSupportDaoImpl.getComplaintByNumber(): ", e);
		}
		return null;
	}

	@Override
	public List<CustomerComplaintModel> getComplaintByInvoiceNumber(final String invoiceNumber) throws Exception
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ALL_COMPLAINT_LIST_BY_INVOICE_NUMBER);
		query.addQueryParameter("invoicenumber", invoiceNumber);
		final SearchResult<CustomerComplaintModel> result = getFlexibleSearchService().search(query);
		return result.getResult();
	}

	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	/**
	 * @param flexibleSearchService
	 *           the flexibleSearchService to set
	 */
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	/**
	 * @return the sessionService
	 */
	public SessionService getSessionService()
	{
		return sessionService;
	}

	/**
	 * @param sessionService
	 *           the sessionService to set
	 */
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}
}
