/**
 * 
 */
package com.jsl.core.payment.dto;

/**
 * @author himanshu.sial
 *
 */
public class JslPaymentResponse
{
	private String sbi_ref_no;
	private String status;
	private String status_desc;

	private String merchant_code;
	private String comp_code;
	private String cust_code;
	private String jsl_ref_no;
	private String fi_doc_number;
	private String po_no;
	private double amount;
	private double jsl_ref_amt;
	private String gst_inv_number;
	private String invoice_amount;
	private String sale_order_no;
	private String payment_source;
	private String pmt_typ_sec_dep;
	private String fis_yr;
	private String pmt_typ_adv;
	private String inv_no;
	private String pstng_date;
	private String doc_date;
	private String bus_area;


	@Override
	public String toString()
	{
		return "PaymentResponse [sbi_ref_no=" + sbi_ref_no + ", status=" + status + ", status_desc=" + status_desc
				+ ", merchant_code=" + merchant_code + ", comp_code=" + comp_code + ", cust_code=" + cust_code + ", jsl_ref_no="
				+ jsl_ref_no + ", fi_doc_number=" + fi_doc_number + ", po_no=" + po_no + ", amount=" + amount + ", jsl_ref_amt="
				+ jsl_ref_amt + ", gst_inv_number=" + gst_inv_number + ", invoice_amount=" + invoice_amount + ", sale_order_no="
				+ sale_order_no + ", payment_source=" + payment_source + ", pmt_typ_sec_dep=" + pmt_typ_sec_dep + ", fis_yr=" + fis_yr
				+ ", pmt_typ_adv=" + pmt_typ_adv + ", inv_no=" + inv_no + ", pstng_date=" + pstng_date + ", doc_date=" + doc_date
				+ ", bus_area=" + bus_area + "]";
	}


	/**
	 * @return the sbi_ref_no
	 */
	public String getSbi_ref_no()
	{
		return sbi_ref_no;
	}



	/**
	 * @param sbi_ref_no
	 *           the sbi_ref_no to set
	 */
	public void setSbi_ref_no(String sbi_ref_no)
	{
		this.sbi_ref_no = sbi_ref_no;
	}



	/**
	 * @return the status
	 */
	public String getStatus()
	{
		return status;
	}


	/**
	 * @param status
	 *           the status to set
	 */
	public void setStatus(String status)
	{
		this.status = status;
	}


	/**
	 * @return the status_desc
	 */
	public String getStatus_desc()
	{
		return status_desc;
	}


	/**
	 * @param status_desc
	 *           the status_desc to set
	 */
	public void setStatus_desc(String status_desc)
	{
		this.status_desc = status_desc;
	}


	/**
	 * @return the merchant_code
	 */
	public String getMerchant_code()
	{
		return merchant_code;
	}


	/**
	 * @param merchant_code
	 *           the merchant_code to set
	 */
	public void setMerchant_code(String merchant_code)
	{
		this.merchant_code = merchant_code;
	}


	/**
	 * @return the comp_code
	 */
	public String getComp_code()
	{
		return comp_code;
	}


	/**
	 * @param comp_code
	 *           the comp_code to set
	 */
	public void setComp_code(String comp_code)
	{
		this.comp_code = comp_code;
	}


	/**
	 * @return the cust_code
	 */
	public String getCust_code()
	{
		return cust_code;
	}


	/**
	 * @param cust_code
	 *           the cust_code to set
	 */
	public void setCust_code(String cust_code)
	{
		this.cust_code = cust_code;
	}


	/**
	 * @return the jsl_ref_no
	 */
	public String getJsl_ref_no()
	{
		return jsl_ref_no;
	}


	/**
	 * @param jsl_ref_no
	 *           the jsl_ref_no to set
	 */
	public void setJsl_ref_no(String jsl_ref_no)
	{
		this.jsl_ref_no = jsl_ref_no;
	}


	/**
	 * @return the fi_doc_number
	 */
	public String getFi_doc_number()
	{
		return fi_doc_number;
	}


	/**
	 * @param fi_doc_number
	 *           the fi_doc_number to set
	 */
	public void setFi_doc_number(String fi_doc_number)
	{
		this.fi_doc_number = fi_doc_number;
	}


	/**
	 * @return the po_no
	 */
	public String getPo_no()
	{
		return po_no;
	}


	/**
	 * @param po_no
	 *           the po_no to set
	 */
	public void setPo_no(String po_no)
	{
		this.po_no = po_no;
	}


	/**
	 * @return the amount
	 */
	public double getAmount()
	{
		return amount;
	}


	/**
	 * @param amount
	 *           the amount to set
	 */
	public void setAmount(double amount)
	{
		this.amount = amount;
	}


	/**
	 * @return the jsl_ref_amt
	 */
	public double getJsl_ref_amt()
	{
		return jsl_ref_amt;
	}


	/**
	 * @param jsl_ref_amt
	 *           the jsl_ref_amt to set
	 */
	public void setJsl_ref_amt(double jsl_ref_amt)
	{
		this.jsl_ref_amt = jsl_ref_amt;
	}


	/**
	 * @return the gst_inv_number
	 */
	public String getGst_inv_number()
	{
		return gst_inv_number;
	}


	/**
	 * @param gst_inv_number
	 *           the gst_inv_number to set
	 */
	public void setGst_inv_number(String gst_inv_number)
	{
		this.gst_inv_number = gst_inv_number;
	}


	/**
	 * @return the invoice_amount
	 */
	public String getInvoice_amount()
	{
		return invoice_amount;
	}


	/**
	 * @param invoice_amount
	 *           the invoice_amount to set
	 */
	public void setInvoice_amount(String invoice_amount)
	{
		this.invoice_amount = invoice_amount;
	}


	/**
	 * @return the sale_order_no
	 */
	public String getSale_order_no()
	{
		return sale_order_no;
	}


	/**
	 * @param sale_order_no
	 *           the sale_order_no to set
	 */
	public void setSale_order_no(String sale_order_no)
	{
		this.sale_order_no = sale_order_no;
	}


	/**
	 * @return the payment_source
	 */
	public String getPayment_source()
	{
		return payment_source;
	}


	/**
	 * @param payment_source
	 *           the payment_source to set
	 */
	public void setPayment_source(String payment_source)
	{
		this.payment_source = payment_source;
	}


	/**
	 * @return the pmt_typ_sec_dep
	 */
	public String getPmt_typ_sec_dep()
	{
		return pmt_typ_sec_dep;
	}


	/**
	 * @param pmt_typ_sec_dep
	 *           the pmt_typ_sec_dep to set
	 */
	public void setPmt_typ_sec_dep(String pmt_typ_sec_dep)
	{
		this.pmt_typ_sec_dep = pmt_typ_sec_dep;
	}


	/**
	 * @return the fis_yr
	 */
	public String getFis_yr()
	{
		return fis_yr;
	}


	/**
	 * @param fis_yr
	 *           the fis_yr to set
	 */
	public void setFis_yr(String fis_yr)
	{
		this.fis_yr = fis_yr;
	}


	/**
	 * @return the pmt_typ_adv
	 */
	public String getPmt_typ_adv()
	{
		return pmt_typ_adv;
	}


	/**
	 * @param pmt_typ_adv
	 *           the pmt_typ_adv to set
	 */
	public void setPmt_typ_adv(String pmt_typ_adv)
	{
		this.pmt_typ_adv = pmt_typ_adv;
	}


	/**
	 * @return the inv_no
	 */
	public String getInv_no()
	{
		return inv_no;
	}


	/**
	 * @param inv_no
	 *           the inv_no to set
	 */
	public void setInv_no(String inv_no)
	{
		this.inv_no = inv_no;
	}


	/**
	 * @return the pstng_date
	 */
	public String getPstng_date()
	{
		return pstng_date;
	}


	/**
	 * @param pstng_date
	 *           the pstng_date to set
	 */
	public void setPstng_date(String pstng_date)
	{
		this.pstng_date = pstng_date;
	}


	/**
	 * @return the doc_date
	 */
	public String getDoc_date()
	{
		return doc_date;
	}


	/**
	 * @param doc_date
	 *           the doc_date to set
	 */
	public void setDoc_date(String doc_date)
	{
		this.doc_date = doc_date;
	}


	/**
	 * @return the bus_area
	 */
	public String getBus_area()
	{
		return bus_area;
	}


	/**
	 * @param bus_area
	 *           the bus_area to set
	 */
	public void setBus_area(String bus_area)
	{
		this.bus_area = bus_area;
	}
}
