/**
 * 
 */
package com.jsl.core.payment.dto;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * @author himanshu.sial
 *
 */
public class JslPaymentRequestDto
{

	private String merchantCode;
	private String companyCode;
	private String customerCode;
	private String jslReferenceNumber;
	private String fiDocumentNumber;
	private String poNumber;
	private String totalAmount;
	private String invoiceAmount;
	private String saleOrderNumber;
	private String paymentSource;
	private String fiscalYear;
	private String paymentTypeAdvance;
	private String invoiceNumber;
	private String gstInvoiceNumber;
	private String postingDate;
	private String documentDate;
	private String busArea;
	private String access_token;
	private String actionUrl;
	private String customerName;
	private String paymentDate;
	private String bankAccountNumber;


	/**
	 * @return the actionUrl
	 */
	public String getActionUrl()
	{
		return actionUrl;
	}

	/**
	 * @param actionUrl
	 *           the actionUrl to set
	 */
	public void setActionUrl(String actionUrl)
	{
		this.actionUrl = actionUrl;
	}

	/**
	 * @return the merchantCode
	 */
	public String getMerchantCode()
	{
		return merchantCode;
	}

	/**
	 * @param merchantCode
	 *           the merchantCode to set
	 */
	public void setMerchantCode(String merchantCode)
	{
		this.merchantCode = merchantCode;
	}

	/**
	 * @return the customerCode
	 */
	public String getCustomerCode()
	{
		return customerCode;
	}

	/**
	 * @param customerCode
	 *           the customerCode to set
	 */
	public void setCustomerCode(String customerCode)
	{
		this.customerCode = customerCode;
	}


	/**
	 * @return the jslReferenceNumber
	 */
	public String getJslReferenceNumber()
	{
		return jslReferenceNumber;
	}

	/**
	 * @param jslReferenceNumber
	 *           the jslReferenceNumber to set
	 */
	public void setJslReferenceNumber(String jslReferenceNumber)
	{
		this.jslReferenceNumber = jslReferenceNumber;
	}



	/**
	 * @return the fiDocumentNumber
	 */
	public String getFiDocumentNumber()
	{
		return fiDocumentNumber;
	}

	/**
	 * @param fiDocumentNumber
	 *           the fiDocumentNumber to set
	 */
	public void setFiDocumentNumber(String fiDocumentNumber)
	{
		this.fiDocumentNumber = fiDocumentNumber;
	}

	/**
	 * @return the poNumber
	 */
	public String getPoNumber()
	{
		return poNumber;
	}

	/**
	 * @param poNumber
	 *           the poNumber to set
	 */
	public void setPoNumber(String poNumber)
	{
		this.poNumber = poNumber;
	}


	/**
	 * @return the fiscalYear
	 */
	public String getFiscalYear()
	{
		return fiscalYear;
	}

	/**
	 * @param fiscalYear
	 *           the fiscalYear to set
	 */
	public void setFiscalYear(String fiscalYear)
	{
		this.fiscalYear = fiscalYear;
	}



	/**
	 * @return the invoiceNumber
	 */
	public String getInvoiceNumber()
	{
		return invoiceNumber;
	}

	/**
	 * @param invoiceNumber
	 *           the invoiceNumber to set
	 */
	public void setInvoiceNumber(String invoiceNumber)
	{
		this.invoiceNumber = invoiceNumber;
	}

	/**
	 * @return the gstInvoiceNumber
	 */
	public String getGstInvoiceNumber()
	{
		return gstInvoiceNumber;
	}

	/**
	 * @param gstInvoiceNumber
	 *           the gstInvoiceNumber to set
	 */
	public void setGstInvoiceNumber(String gstInvoiceNumber)
	{
		this.gstInvoiceNumber = gstInvoiceNumber;
	}

	/**
	 * @return the companyCode
	 */
	public String getCompanyCode()
	{
		return companyCode;
	}

	/**
	 * @param companyCode
	 *           the companyCode to set
	 */
	public void setCompanyCode(String companyCode)
	{
		this.companyCode = companyCode;
	}

	/**
	 * @return the totalAmount
	 */
	public String getTotalAmount()
	{
		return totalAmount;
	}

	/**
	 * @param totalAmount
	 *           the totalAmount to set
	 */
	public void setTotalAmount(String totalAmount)
	{
		this.totalAmount = totalAmount;
	}

	/**
	 * @return the invoiceAmount
	 */
	public String getInvoiceAmount()
	{
		return invoiceAmount;
	}

	/**
	 * @param invoiceAmount
	 *           the invoiceAmount to set
	 */
	public void setInvoiceAmount(String invoiceAmount)
	{
		this.invoiceAmount = invoiceAmount;
	}

	/**
	 * @return the saleOrderNumber
	 */
	public String getSaleOrderNumber()
	{
		return saleOrderNumber;
	}

	/**
	 * @param saleOrderNumber
	 *           the saleOrderNumber to set
	 */
	public void setSaleOrderNumber(String saleOrderNumber)
	{
		this.saleOrderNumber = saleOrderNumber;
	}

	/**
	 * @return the paymentSource
	 */
	public String getPaymentSource()
	{
		return paymentSource;
	}

	/**
	 * @param paymentSource
	 *           the paymentSource to set
	 */
	public void setPaymentSource(String paymentSource)
	{
		this.paymentSource = paymentSource;
	}

	/**
	 * @return the paymentTypeAdvance
	 */
	public String getPaymentTypeAdvance()
	{
		return paymentTypeAdvance;
	}

	/**
	 * @param paymentTypeAdvance
	 *           the paymentTypeAdvance to set
	 */
	public void setPaymentTypeAdvance(String paymentTypeAdvance)
	{
		this.paymentTypeAdvance = paymentTypeAdvance;
	}

	/**
	 * @return the postingDate
	 */
	public String getPostingDate()
	{
		return postingDate;
	}

	/**
	 * @param postingDate
	 *           the postingDate to set
	 */
	public void setPostingDate(String postingDate)
	{
		this.postingDate = postingDate;
	}

	/**
	 * @return the documentDate
	 */
	public String getDocumentDate()
	{
		return documentDate;
	}

	/**
	 * @param documentDate
	 *           the documentDate to set
	 */
	public void setDocumentDate(String documentDate)
	{
		this.documentDate = documentDate;
	}

	/**
	 * @return the busArea
	 */
	public String getBusArea()
	{
		return busArea;
	}

	/**
	 * @param busArea
	 *           the busArea to set
	 */
	public void setBusArea(String busArea)
	{
		this.busArea = busArea;
	}

	/**
	 * @return the access_token
	 */
	public String getAccess_token()
	{
		return access_token;
	}

	/**
	 * @param access_token
	 *           the access_token to set
	 */
	public void setAccess_token(String access_token)
	{
		this.access_token = access_token;
	}

	/**
	 * @return the customerName
	 */
	public String getCustomerName()
	{
		return customerName;
	}

	/**
	 * @param customerName the customerName to set
	 */
	public void setCustomerName(String customerName)
	{
		this.customerName = customerName;
	}

	/**
	 * @return the paymentDate
	 */
	public String getPaymentDate()
	{
		return paymentDate;
	}

	/**
	 * @param paymentDate the paymentDate to set
	 */
	public void setPaymentDate(String paymentDate)
	{
		this.paymentDate = paymentDate;
	}

	/**
	 * @return the bankAccountNumber
	 */
	public String getBankAccountNumber()
	{
		return bankAccountNumber;
	}

	/**
	 * @param bankAccountNumber the bankAccountNumber to set
	 */
	public void setBankAccountNumber(String bankAccountNumber)
	{
		this.bankAccountNumber = bankAccountNumber;
	}
}
