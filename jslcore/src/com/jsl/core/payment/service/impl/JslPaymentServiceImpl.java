/**
 *
 */
package com.jsl.core.payment.service.impl;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.sap.sapinvoiceaddon.model.SapB2BDocumentModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.jsl.core.enums.JslPaymentStatus;
import com.jsl.core.exceptions.JslPaymentException;
import com.jsl.core.model.JslPaymentTransactionModel;
import com.jsl.core.model.PaymentSaleOrderModel;
import com.jsl.core.payment.dto.JslPaymentRequestDto;
import com.jsl.core.payment.dto.JslPaymentResponse;
import com.jsl.core.payment.service.JslPaymentService;
import com.jsl.core.paymentdetailshistory.dao.PaymentDetailsHistoryDao;
import com.jsl.core.util.JslB2bUnitUtil;
import com.jsl.facades.payment.dto.TransactionMasterDto;
import com.jsl.facades.paymentdetailshistory.dto.PaymentDetailsDto;
import com.jsl.facades.paymentdetailshistory.dto.PaymentDetailsHistoryDto;
import com.jsl.payment.dto.SecurityTokenResponseDto;
import com.jsl.rest.service.JslRestService;


/**
 * @author himanshu.sial
 *
 */
public class JslPaymentServiceImpl implements JslPaymentService
{
	private static final Logger LOG = Logger.getLogger(JslPaymentServiceImpl.class);

	private static final String PAYMENT_PROCESS_URL_KEY = "payment.process.url";

	private static final String COMMA = ",";

	private static final String MERCHANT_CODE_JS01 = "JINDAL0144"; //UAT JINDAL

	private static final String MERCHANT_CODE_JS03 = "JINDAL0377"; // UAT JINDAL

	private static final String JSL_REF_NUMBER_PREFIX_INV = "INV";

	private static final String JSL_REF_NUMBER_PREFIX_ADV = "ADV";

	private static final String HYBRIS_ONLINE = "HYBONLINE";

	private static final String EMPTY_STRING = "";

	private JslRestService jslRestService;

	private PaymentDetailsHistoryDao paymentDetailsHistoryDao;

	private UserService userService;

	private ModelService modelService;

	private static final String SALE_ORDER_NUMBER = "soNumber";

	private static final String PURCHASE_ORDER_NUMBER = "poNumber";

	private static final String INVOICE_NUMBER = "invoice";

	private static final BigDecimal ZERO = new BigDecimal(0);

	private final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");

	private static final String PAYMENT_OAUTH_TOKEN_URL_KEY = "payment.oauth.token.url";

	private static final String PAYMENT_USERNAME_KEY = "rest.payment.user.credentials.username";
	private static final String PAYMENT_PASSWORD_KEY = "rest.payment.user.credentials.password";
	private static final String PAYMENT_BASIC_USERNAME_KEY = "rest.payment.basic.credentials.username";

	private static final String JS01 = "JS01";
	private static final String JS03 = "JS03";

	private static final String JS01_BANK_ACCOUNT_NUMBER = "38703228744";
	private static final String JS03_BANK_ACCOUNT_NUMBER = "38703229577";
	private static final String CUSTOMER_NAME = "customerName";
	private static final String CUSTOMER_ID = "customerId";
	private static final String PAYMENT_TYPE = "payment";

	/*
	 * (non-Javadoc)
	 *
	 * @see com.jsl.core.payment.service.JslPaymentService#processPendingPayment(com.jsl.facades.payment.dto.
	 * TransactionMasterDto)
	 */
	@Override
	public JslPaymentRequestDto processPendingPayment(final TransactionMasterDto transactionMasterDto) throws Exception
	{
		// XXX Auto-generated method stub
		JslPaymentRequestDto request = null;
		final String jslRefNumber = transactionMasterDto.getJslRefNumber();
		final JslPaymentTransactionModel transactionModel = paymentDetailsHistoryDao.getTransactionModels(jslRefNumber).get(0);
		if (transactionModel.getJslRefNumber().startsWith("INV"))
		{
			final List<SapB2BDocumentModel> invoiceList = paymentDetailsHistoryDao.getPaymentDetailsFromJslRefNumber(jslRefNumber);
			request = this.createInvoiceRequest(invoiceList, jslRefNumber);
			this.getSecurityToken(request);

			for (final SapB2BDocumentModel document : invoiceList)
			{
				document.setPaymentDate(dateFormat.format(LocalDateTime.now()));
				document.setJslReferenceNumber(request.getJslReferenceNumber());
				document.setPaymentTypeAdvance(request.getPaymentTypeAdvance());
				document.setPaymentSource(request.getPaymentSource());
				document.setJslTotalRefAmount(request.getTotalAmount());
				document.setHybrisPaymentStatus(JslPaymentStatus.PROCESSING);
			}
			modelService.saveAll(invoiceList);
		}
		else
		{
			final List<PaymentSaleOrderModel> saleOrderList = paymentDetailsHistoryDao
					.getSaleOrderPaymentDetailsFromJslRefNumber(jslRefNumber);
			request = this.createAdvanceRequest(saleOrderList, transactionModel.getJslRefAmount(), jslRefNumber);
			this.getSecurityToken(request);
			for (final PaymentSaleOrderModel document : saleOrderList)
			{
				document.setPaymentDate(dateFormat.format(LocalDateTime.now()));
				document.setJslRefNumber(request.getJslReferenceNumber());
				document.setPaymentSource(request.getPaymentSource());
				document.setJslRefAmount(request.getTotalAmount());
				document.setHybrisPaymentStatus(JslPaymentStatus.PROCESSING);
			}
			modelService.saveAll(saleOrderList);

		}

		transactionModel.setHybrisPaymentStatus(JslPaymentStatus.PROCESSING);
		transactionModel.setPaymentStatus("PROCESSING");
		modelService.save(transactionModel);
		return request;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.jsl.core.payment.service.JslPaymentService#processPaymentReponse(com.jsl.facades.payment.dto.PaymentResponse)
	 */
	@Override
	public void processPaymentReponse(final JslPaymentResponse paymentResponse) throws Exception
	{
		// XXX Auto-generated method stub
		final List<SapB2BDocumentModel> documentList = paymentDetailsHistoryDao
				.getPaymentDetailsFromJslRefNumber(paymentResponse.getJsl_ref_no());

		documentList.forEach(document -> {
			document.setSbiReferenceNumber(paymentResponse.getSbi_ref_no());
			document.setPaymentStatus(paymentResponse.getStatus());
			document.setPaymentStatusDescription(paymentResponse.getStatus_desc());
		});
		modelService.saveAll(documentList);
	}


	@Override
	public JslPaymentRequestDto processPayment(final PaymentDetailsDto paymentDetailsDto) throws Exception
	{

		JslPaymentRequestDto request = null;
		if (INVOICE_NUMBER.equalsIgnoreCase(paymentDetailsDto.getSearchCriteria()))
		{
			List<String> invoiceList = invoiceList = paymentDetailsDto.getPaymentdetailshistory().stream()
					.map(PaymentDetailsHistoryDto::getInvoiceNumber).collect(Collectors.toList());
			final List<SapB2BDocumentModel> sapB2BDocumentList = paymentDetailsHistoryDao
					.getPendingPaymentDetailsBasedOnSearchCriteria(paymentDetailsDto.getSearchCriteria(), invoiceList,
							userService.getCurrentUser());

			request = this.createInvoiceRequest(sapB2BDocumentList, null);
			this.getSecurityToken(request);
			final String transactionNumber = Base64.getEncoder().encodeToString(request.getInvoiceNumber().getBytes());

			for (final SapB2BDocumentModel document : sapB2BDocumentList)
			{
				document.setPaymentDate(dateFormat.format(LocalDateTime.now()));
				document.setTransactionId(transactionNumber);
				document.setJslReferenceNumber(request.getJslReferenceNumber());
				document.setPaymentTypeAdvance(request.getPaymentTypeAdvance());
				document.setPaymentSource(request.getPaymentSource());
				document.setJslTotalRefAmount(request.getTotalAmount());
				document.setHybrisPaymentStatus(JslPaymentStatus.PROCESSING);
			}
			modelService.saveAll(sapB2BDocumentList);

		}
		else if (SALE_ORDER_NUMBER.equalsIgnoreCase(paymentDetailsDto.getSearchCriteria()))
		{
			final List<PaymentSaleOrderModel> saleOrderList = paymentDetailsHistoryDao.getSaleOrderDetailsFromNumber(
					paymentDetailsDto.getSearchCriteria(), paymentDetailsDto.getSearchValue(), PAYMENT_TYPE);

			request = this.createAdvanceRequest(saleOrderList, paymentDetailsDto.getAdvanceAmount(), null);
			this.getSecurityToken(request);
			for (final PaymentSaleOrderModel document : saleOrderList)
			{
				document.setPaymentDate(dateFormat.format(LocalDateTime.now()));
				document.setJslRefNumber(request.getJslReferenceNumber());
				document.setPaymentSource(request.getPaymentSource());
				document.setJslRefAmount(request.getTotalAmount());
				document.setHybrisPaymentStatus(JslPaymentStatus.PROCESSING);
			}
			modelService.saveAll(saleOrderList);

		}
		else
		{
			final List<PaymentSaleOrderModel> saleOrderList = paymentDetailsHistoryDao.getSaleOrderDetailsFromNumber(
					paymentDetailsDto.getSearchCriteria(), paymentDetailsDto.getSearchValue(), PAYMENT_TYPE);
			request = this.createAdvanceRequest(saleOrderList, paymentDetailsDto.getAdvanceAmount(), null);
			this.getSecurityToken(request);
			for (final PaymentSaleOrderModel document : saleOrderList)
			{
				document.setPaymentDate(dateFormat.format(LocalDateTime.now()));
				document.setJslRefNumber(request.getJslReferenceNumber());
				document.setPaymentSource(request.getPaymentSource());
				document.setJslRefAmount(request.getTotalAmount());
				document.setHybrisPaymentStatus(JslPaymentStatus.PROCESSING);
			}
			modelService.saveAll(saleOrderList);
		}

		final JslPaymentTransactionModel transactionModel = this.createTransaction(request, paymentDetailsDto.getPaymentNote());
		modelService.save(transactionModel);
		return request;
	}

	private JslPaymentTransactionModel createTransaction(final JslPaymentRequestDto request, final String paymentNote)
	{
		final JslPaymentTransactionModel transactionModel = new JslPaymentTransactionModel();

		transactionModel.setCompanyCode(request.getCompanyCode());
		transactionModel.setUser(userService.getCurrentUser());
		final Map<String, String> map = this.getCustomerMap();
		transactionModel.setCustomerNumber(map.get(CUSTOMER_ID));
		transactionModel.setCustomerName(map.get(CUSTOMER_NAME));
		transactionModel.setHybrisPaymentStatus(JslPaymentStatus.PROCESSING);
		transactionModel.setInvoiceNumbers(request.getInvoiceNumber());
		transactionModel.setJslRefAmount(request.getTotalAmount());
		transactionModel.setJslRefNumber(request.getJslReferenceNumber());
		transactionModel.setPaymentDate(dateFormat.format(LocalDateTime.now()));
		transactionModel.setPurchaseOrderNumber(request.getPoNumber());
		transactionModel.setSaleOrderNumber(request.getSaleOrderNumber());
		transactionModel.setPaymentSource(request.getPaymentSource());
		transactionModel.setPaymentTypeAdv(request.getPaymentTypeAdvance());
		transactionModel.setPaymentStatus("PROCESSING");
		transactionModel.setPaymentNote(paymentNote);
		transactionModel.setBankAccNumber(request.getBankAccountNumber());
		return transactionModel;
	}

	private void getSecurityToken(final JslPaymentRequestDto request) throws Exception
	{
		final MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();


		map.add("grant_type", "password");
		map.add("username", Config.getParameter(PAYMENT_USERNAME_KEY));
		map.add("password", Config.getParameter(PAYMENT_PASSWORD_KEY));
		map.add("client", Config.getParameter(PAYMENT_BASIC_USERNAME_KEY));

		final SecurityTokenResponseDto response = jslRestService.postForObjectCommand(
				Config.getParameter(PAYMENT_OAUTH_TOKEN_URL_KEY), map, SecurityTokenResponseDto.class,
				request.getJslReferenceNumber(), request.getCustomerCode(), MediaType.APPLICATION_FORM_URLENCODED);

		if (response == null)
		{
			LOG.error("Null response for security token");
			throw new JslPaymentException("Got null response while accessing token");
		}
		request.setAccess_token(response.getAccessToken());
		request.setActionUrl(Config.getParameter(PAYMENT_PROCESS_URL_KEY));
	}

	private JslPaymentRequestDto createAdvanceRequest(final List<PaymentSaleOrderModel> saleOrderList, final String advanceAmount,
			final String jslRefNumberOld)
	{
		final JslPaymentRequestDto jslPaymentRequestDto = new JslPaymentRequestDto();
		final PaymentSaleOrderModel saleOrder = saleOrderList.get(0);
		final Map<String, String> map = this.getCustomerMap();
		jslPaymentRequestDto.setCustomerCode(saleOrder.getCompanyCode() + map.get(CUSTOMER_ID));
		jslPaymentRequestDto.setCustomerName(map.get(CUSTOMER_NAME));
		jslPaymentRequestDto.setSaleOrderNumber(saleOrder.getSaleOrderNumber());
		jslPaymentRequestDto.setPoNumber(saleOrder.getPurchaseOrderNumber());
		jslPaymentRequestDto.setPostingDate(saleOrder.getSaleOrderDate());
		jslPaymentRequestDto.setCompanyCode(saleOrder.getCompanyCode());
		jslPaymentRequestDto.setTotalAmount(advanceAmount);
		jslPaymentRequestDto.setPaymentTypeAdvance("Y");
		jslPaymentRequestDto.setPaymentSource(HYBRIS_ONLINE);

		String jslRefNumber = "";

		if (jslRefNumberOld == null)
		{
			jslRefNumber = JSL_REF_NUMBER_PREFIX_ADV + saleOrder.getCompanyCode() + RandomStringUtils.randomNumeric(10);
		}
		else
		{
			jslRefNumber = jslRefNumberOld;
		}

		jslPaymentRequestDto.setJslReferenceNumber(jslRefNumber);
		jslPaymentRequestDto.setPaymentDate(dateFormat.format(LocalDateTime.now()));
		if (JS01.equalsIgnoreCase(saleOrder.getCompanyCode()))
		{
			jslPaymentRequestDto.setBankAccountNumber(JS01_BANK_ACCOUNT_NUMBER);
			jslPaymentRequestDto.setMerchantCode(MERCHANT_CODE_JS01);
		}
		else
		{
			jslPaymentRequestDto.setBankAccountNumber(JS03_BANK_ACCOUNT_NUMBER);
			jslPaymentRequestDto.setMerchantCode(MERCHANT_CODE_JS03);
		}

		return jslPaymentRequestDto;
	}

	private JslPaymentRequestDto createInvoiceRequest(final List<SapB2BDocumentModel> sapB2BDocumentList,
			final String jslRefNumberOld)
	{
		final JslPaymentRequestDto jslPaymentRequestDto = new JslPaymentRequestDto();

		final String companyCode = sapB2BDocumentList.get(0).getInvoiceCompanyCode();
		final Map<String, String> map = this.getCustomerMap();
		jslPaymentRequestDto.setCustomerCode(companyCode + map.get(CUSTOMER_ID));
		jslPaymentRequestDto.setCustomerName(map.get(CUSTOMER_NAME));

		final ArrayList<String> invoiceNumber = new ArrayList<String>();

		final ArrayList<String> fiscalYear = new ArrayList<String>();
		final ArrayList<String> orderNumber = new ArrayList<String>();
		final ArrayList<String> gstInvoiceNumber = new ArrayList<String>();
		final ArrayList<String> toEncodeInvoiceId = new ArrayList<String>();

		Double totalAmount = 0d;

		final ArrayList<String> invoiceAmounts = new ArrayList<String>();
		final ArrayList<String> pstnDates = new ArrayList<String>();
		final ArrayList<String> documentDates = new ArrayList<String>();
		final ArrayList<String> busAreas = new ArrayList<String>();
		final ArrayList<String> fiDocumentNumber = new ArrayList<String>();

		for (final SapB2BDocumentModel document : sapB2BDocumentList)
		{
			invoiceNumber.add(document.getInvoiceNumber() == null ? EMPTY_STRING : document.getInvoiceNumber());
			fiscalYear.add(document.getFiscalYear());
			pstnDates.add(StringUtils.isEmpty(document.getInvoiceDate()) ? document.getInvoiceDate()
					: document.getInvoiceDate().substring(0, 10));
			documentDates.add(StringUtils.isEmpty(document.getDocumentDate()) ? document.getDocumentDate()
					: document.getDocumentDate().substring(0, 10));
			fiDocumentNumber.add(document.getDocumentNumber());
			busAreas.add(document.getBusArea());
			orderNumber.add(StringUtils.isEmpty(document.getOrderNumber()) ? EMPTY_STRING : document.getOrderNumber());
			gstInvoiceNumber
					.add(StringUtils.isEmpty(document.getGstInvoiceNumber()) ? EMPTY_STRING : document.getGstInvoiceNumber());
			invoiceAmounts.add(StringUtils.isEmpty(document.getGrandTotal()) ? EMPTY_STRING : document.getGrandTotal());
			toEncodeInvoiceId.add(StringUtils.isEmpty(document.getInvoiceNumber()) ? EMPTY_STRING : document.getInvoiceNumber());
			totalAmount = totalAmount
					+ (StringUtils.isEmpty(document.getGrandTotal()) ? 0d : Double.parseDouble(document.getGrandTotal()));
		}


		final String invoiceConcatinatedNumbers = String.join(COMMA, invoiceNumber);


		jslPaymentRequestDto.setInvoiceNumber(invoiceConcatinatedNumbers);
		jslPaymentRequestDto.setInvoiceAmount(String.join(COMMA, invoiceAmounts));
		jslPaymentRequestDto.setFiscalYear(String.join(COMMA, fiscalYear));
		jslPaymentRequestDto.setSaleOrderNumber(String.join(COMMA, orderNumber));
		jslPaymentRequestDto.setGstInvoiceNumber(String.join(COMMA, gstInvoiceNumber));
		jslPaymentRequestDto.setTotalAmount(String.format("%.2f", totalAmount));
		jslPaymentRequestDto.setBusArea(String.join(COMMA, busAreas));
		jslPaymentRequestDto.setPaymentSource(HYBRIS_ONLINE);
		jslPaymentRequestDto.setFiDocumentNumber(String.join(COMMA, fiDocumentNumber));
		jslPaymentRequestDto.setCompanyCode(companyCode);

		//TO-DO
		jslPaymentRequestDto.setPostingDate(String.join(COMMA, pstnDates));
		jslPaymentRequestDto.setDocumentDate(String.join(COMMA, documentDates));

		String jslRefNumber = "";


		jslPaymentRequestDto.setPoNumber(sapB2BDocumentList.get(0).getOurTaxNumber());
		jslPaymentRequestDto.setPaymentTypeAdvance("N");



		if (jslRefNumberOld == null)
		{
			jslRefNumber = JSL_REF_NUMBER_PREFIX_INV + companyCode + RandomStringUtils.randomNumeric(10);
		}
		else
		{
			jslRefNumber = jslRefNumberOld;
		}
		jslPaymentRequestDto.setJslReferenceNumber(jslRefNumber);
		jslPaymentRequestDto.setPaymentDate(dateFormat.format(LocalDateTime.now()));
		if (JS01.equalsIgnoreCase(companyCode))
		{
			jslPaymentRequestDto.setBankAccountNumber(JS01_BANK_ACCOUNT_NUMBER);
			jslPaymentRequestDto.setMerchantCode(MERCHANT_CODE_JS01);
		}
		else
		{
			jslPaymentRequestDto.setBankAccountNumber(JS03_BANK_ACCOUNT_NUMBER);
			jslPaymentRequestDto.setMerchantCode(MERCHANT_CODE_JS03);
		}
		return jslPaymentRequestDto;
	}

	private Map<String, String> getCustomerMap()
	{

		final UserModel user = getUserService().getCurrentUser();

		final B2BUnitModel parentb2bUnit = JslB2bUnitUtil.getParentB2bUnit(user);

		final Map<String, String> map = new HashMap<String, String>();

		if (parentb2bUnit != null)
		{
			map.put(CUSTOMER_ID, parentb2bUnit.getUid());
			map.put(CUSTOMER_NAME, parentb2bUnit.getDisplayName());
		}
		else
		{
			map.put(CUSTOMER_ID, user.getUid());
			map.put(CUSTOMER_NAME, user.getDisplayName());
		}

		return map;
	}

	/**
	 * @return the jslRestService
	 */
	public JslRestService getJslRestService()
	{
		return jslRestService;
	}

	/**
	 * @param jslRestService
	 *           the jslRestService to set
	 */
	public void setJslRestService(final JslRestService jslRestService)
	{
		this.jslRestService = jslRestService;
	}

	/**
	 * @return the paymentDetailsHistoryDao
	 */
	public PaymentDetailsHistoryDao getPaymentDetailsHistoryDao()
	{
		return paymentDetailsHistoryDao;
	}

	/**
	 * @param paymentDetailsHistoryDao
	 *           the paymentDetailsHistoryDao to set
	 */
	public void setPaymentDetailsHistoryDao(final PaymentDetailsHistoryDao paymentDetailsHistoryDao)
	{
		this.paymentDetailsHistoryDao = paymentDetailsHistoryDao;
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

}
