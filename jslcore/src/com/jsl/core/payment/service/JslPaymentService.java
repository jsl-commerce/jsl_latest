
/**
 *
 */
package com.jsl.core.payment.service;

import com.jsl.core.payment.dto.JslPaymentRequestDto;
import com.jsl.core.payment.dto.JslPaymentResponse;
import com.jsl.facades.payment.dto.TransactionMasterDto;
import com.jsl.facades.paymentdetailshistory.dto.PaymentDetailsDto;


/**
 * @author himanshu.sial
 *
 */
public interface JslPaymentService
{

	public JslPaymentRequestDto processPayment(PaymentDetailsDto paymentDetailsDto) throws Exception;

	public JslPaymentRequestDto processPendingPayment(TransactionMasterDto paymentDetailsDto) throws Exception;

	public void processPaymentReponse(JslPaymentResponse jslPaymentResponse) throws Exception;
}
