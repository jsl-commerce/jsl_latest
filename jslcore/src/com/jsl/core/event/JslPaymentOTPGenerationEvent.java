/**
 *
 */
package com.jsl.core.event;

import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;

import com.jsl.core.dto.JslOTPGenerationDto;


/**
 * @author suyash.trivedi
 *
 */
public class JslPaymentOTPGenerationEvent extends AbstractCommerceUserEvent
{
	private final JslOTPGenerationDto jslOTPGenerationDto;

	public JslPaymentOTPGenerationEvent(final JslOTPGenerationDto jslOTPGenerationDto)
	{
		super();
		this.jslOTPGenerationDto = jslOTPGenerationDto;
	}
}
