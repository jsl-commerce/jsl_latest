package com.jsl.core.event;

import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;


public class JslAuctionBidPlacedEvent extends AbstractEvent
{
	private final BundleTemplateModel bundleTemplate;
	private final String flowName;

	public JslAuctionBidPlacedEvent(final BundleTemplateModel bundleTemplate, final String flowName)
	{
		this.bundleTemplate = bundleTemplate;
		this.flowName = flowName;
	}

	public BundleTemplateModel getBundleTemplate()
	{
		return bundleTemplate;
	}

	public String getFlowName()
	{
		return flowName;
	}

}
