package com.jsl.core.event.listener;

import de.hybris.platform.servicelayer.event.events.AbstractEvent;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;

import org.apache.log4j.Logger;

import com.jsl.core.event.JslAuctionBidPlacedEvent;
import com.jsl.core.services.JslProxyBidRuleService;


public class JslAuctionBidPlacedEventListener extends AbstractEventListener
{
	private static final Logger LOG = Logger.getLogger(JslAuctionBidPlacedEventListener.class);

	private JslProxyBidRuleService jslProxyBidRuleService;

	@Override
	protected void onEvent(final AbstractEvent bidPlacedEvent)
	{
		if (bidPlacedEvent instanceof JslAuctionBidPlacedEvent)
		{
			final String eventFlowName = ((JslAuctionBidPlacedEvent) bidPlacedEvent).getFlowName();
			final String bidPrice = "";
			getJslProxyBidRuleService().placeProxyBid(((JslAuctionBidPlacedEvent) bidPlacedEvent).getBundleTemplate(), bidPrice,
					true, eventFlowName);
		}
	}

	public JslProxyBidRuleService getJslProxyBidRuleService()
	{
		return jslProxyBidRuleService;
	}

	public void setJslProxyBidRuleService(final JslProxyBidRuleService jslProxyBidRuleService)
	{
		this.jslProxyBidRuleService = jslProxyBidRuleService;
	}

}
