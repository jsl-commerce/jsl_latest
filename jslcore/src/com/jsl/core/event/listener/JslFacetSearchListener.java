/**
 *
 */
package com.jsl.core.event.listener;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.solrfacetsearch.search.FacetSearchException;
import de.hybris.platform.solrfacetsearch.search.OrderField;
import de.hybris.platform.solrfacetsearch.search.OrderField.SortOrder;
import de.hybris.platform.solrfacetsearch.search.QueryField;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.SearchQuery.Operator;
import de.hybris.platform.solrfacetsearch.search.SearchQuery.QueryOperator;
import de.hybris.platform.solrfacetsearch.search.context.FacetSearchContext;
import de.hybris.platform.solrfacetsearch.search.context.FacetSearchListener;

import org.fest.util.Collections;
import org.springframework.beans.factory.annotation.Autowired;

import com.jsl.core.constants.JslCoreConstants;


/**
 * @author manav.magoo
 *
 */
public class JslFacetSearchListener implements FacetSearchListener
{

	@Autowired
	UserService userService;

	@Autowired
	private CartService cartService;

	@Autowired
	private WarehouseService warehouseService;

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.solrfacetsearch.search.context.FacetSearchListener#afterSearch(de.hybris.platform.
	 * solrfacetsearch.search.context.FacetSearchContext)
	 */
	@Override
	public void afterSearch(final FacetSearchContext arg0) throws FacetSearchException
	{
		// XXX Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.solrfacetsearch.search.context.FacetSearchListener#afterSearchError(de.hybris.platform.
	 * solrfacetsearch.search.context.FacetSearchContext)
	 */
	@Override
	public void afterSearchError(final FacetSearchContext arg0) throws FacetSearchException
	{
		// XXX Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.solrfacetsearch.search.context.FacetSearchListener#beforeSearch(de.hybris.platform.
	 * solrfacetsearch.search.context.FacetSearchContext)
	 */
	@Override
	public void beforeSearch(final FacetSearchContext paramFacetSearchContext) throws FacetSearchException
	{
		final SearchQuery searchQuery = paramFacetSearchContext.getSearchQuery();

		if (!userService.isAnonymousUser(userService.getCurrentUser()))
		{
			final CartModel cart = cartService.getSessionCart();
			final WarehouseModel warehouseModel = cart.getWarehouse();
			if (warehouseModel != null)
			{
				final String warehouseCode = warehouseModel.getPk().toString();
				addQueryFields(searchQuery, warehouseCode);
			}
			else
			{
				if (!Collections.isEmpty(warehouseService.getDefWarehouse()))
				{
					final String warehouseCode = warehouseService.getDefWarehouse().get(0).getPk().toString();
					addQueryFields(searchQuery, warehouseCode);
				}

			}
		}
	}

	/**
	 * @param searchQuery
	 * @param warehouseCode
	 */
	private void addQueryFields(final SearchQuery searchQuery, final String warehouseCode)
	{
		final QueryField queryField = new QueryField("warehouse_" + warehouseCode + "_string", Operator.AND, QueryOperator.CONTAINS,
				new String[]
				{ "" });
		searchQuery.addFilterQuery(queryField);
		final OrderField orderFieldStockSort = new OrderField(JslCoreConstants.STOCK_SORT_CODE + "_" + warehouseCode + "_string",
				SortOrder.DESCENDING);
		searchQuery.getSorts().add(0, orderFieldStockSort);
	}
}
