/**
 *
 */
package com.jsl.core.event.listener;

import de.hybris.platform.acceleratorservices.site.AbstractAcceleratorSiteEventListener;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.jsl.core.event.JslAuctionOTPGenerationEvent;
import com.jsl.core.model.SendCustomerOtpProcessModel;
import com.jsl.core.services.JslAuctionService;


/**
 * @author suyash.trivedi
 *
 */
public class JslAuctionOTPEventListener extends AbstractAcceleratorSiteEventListener<JslAuctionOTPGenerationEvent>
{
	private final static Logger LOG = Logger.getLogger(JslAuctionOTPEventListener.class);
	private ModelService modelService;
	private BusinessProcessService businessProcessService;
	private JslAuctionService jslAuctionService;

	/**
	 * @return the jslAuctionService
	 */
	public JslAuctionService getJslAuctionService()
	{
		return jslAuctionService;
	}

	/**
	 * @param jslAuctionService
	 *           the jslAuctionService to set
	 */
	public void setJslAuctionService(final JslAuctionService jslAuctionService)
	{
		this.jslAuctionService = jslAuctionService;
	}

	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}



	@Override
	protected void onSiteEvent(final JslAuctionOTPGenerationEvent otpGenerationEvent)
	{
		LOG.info("entered in Send OTP event");
		if (otpGenerationEvent.getJslOTPGenerationDto().getEmailAddress() != null
				&& !StringUtils.isEmpty(otpGenerationEvent.getJslOTPGenerationDto().getEmailAddress()))
		{
			this.sendEmail(otpGenerationEvent);
		}
	}

	/**
	 * @param otpGenerationEvent
	 */
	private void sendEmail(final JslAuctionOTPGenerationEvent otpGenerationEvent)
	{
		final SendCustomerOtpProcessModel auctionOTPValidationProcessModel = (SendCustomerOtpProcessModel) getBusinessProcessService()
				.createProcess("sendOTPEmailProcess-" + otpGenerationEvent.getJslOTPGenerationDto().getEmailAddress().toLowerCase()
						+ "-" + System.currentTimeMillis(), "sendOTPEmailProcess");
		auctionOTPValidationProcessModel.setSite(otpGenerationEvent.getSite());
		auctionOTPValidationProcessModel.setEmail(otpGenerationEvent.getJslOTPGenerationDto().getEmailAddress().toLowerCase());
		auctionOTPValidationProcessModel.setStore(otpGenerationEvent.getBaseStore());
		auctionOTPValidationProcessModel.setOtp(otpGenerationEvent.getJslOTPGenerationDto().getOtpCode());
		auctionOTPValidationProcessModel.setAuctionEventId(otpGenerationEvent.getJslOTPGenerationDto().getAuctionId());
		auctionOTPValidationProcessModel.setDisplayName(otpGenerationEvent.getJslOTPGenerationDto().getDisplayName());
		auctionOTPValidationProcessModel.setExpirationTime(otpGenerationEvent.getJslOTPGenerationDto().getExpirationTime());
		auctionOTPValidationProcessModel.setLanguage(otpGenerationEvent.getLanguage());
		getModelService().save(auctionOTPValidationProcessModel);
		getBusinessProcessService().startProcess(auctionOTPValidationProcessModel);
	}


	@Override
	protected SiteChannel getSiteChannelForEvent(final JslAuctionOTPGenerationEvent event)
	{
		// XXX Auto-generated method stub
		final BaseSiteModel site = event.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.site", site);
		return site.getChannel();
	}

}


