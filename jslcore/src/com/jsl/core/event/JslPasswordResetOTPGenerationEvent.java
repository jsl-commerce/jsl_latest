/**
 *
 */
package com.jsl.core.event;

import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;

import com.jsl.core.dto.JslOTPGenerationDto;


/**
 * @author suyash.trivedi
 *
 */
public class JslPasswordResetOTPGenerationEvent extends AbstractCommerceUserEvent
{
	private final JslOTPGenerationDto jslOTPGenerationDto;

	public JslPasswordResetOTPGenerationEvent(final JslOTPGenerationDto jslOTPGenerationDto)
	{
		super();
		this.jslOTPGenerationDto = jslOTPGenerationDto;
	}
}
