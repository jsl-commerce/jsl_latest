/**
 *
 */
package com.jsl.core.event;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;

/**
 * @author suyash.trivedi
 *
 */
public class SendOTPEvent extends AbstractCommerceUserEvent<BaseSiteModel>
{

	final String otp;

	public SendOTPEvent(final String otp)
	{
		this.otp = otp;

	}

	public String getOtp()
	{
		return otp;
	}

	/**
	 * @return
	 */
	public Integer getOTP()
	{
		// XXX Auto-generated method stub
		return null;
	}




}
