/**
 *
 */
package com.jsl.core.event;

import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;

import com.jsl.core.dto.JslOTPGenerationDto;


/**
 * @author suyash.trivedi
 *
 */
public class JslAuctionOTPGenerationEvent extends AbstractCommerceUserEvent
{

	private final JslOTPGenerationDto jslOTPGenerationDto;

	/**
	 * @return the jslOTPGenerationDto
	 */
	public JslOTPGenerationDto getJslOTPGenerationDto()
	{
		return jslOTPGenerationDto;
	}

	/**
	 * @param jslOTPGenerationDto
	 */
	public JslAuctionOTPGenerationEvent(final JslOTPGenerationDto jslOTPGenerationDto)
	{
		super();
		this.jslOTPGenerationDto = jslOTPGenerationDto;
	}

}
