/**
 *
 */
package com.jsl.core.event;

import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;

import com.jsl.core.dto.JslOTPGenerationDto;


/**
 * @author suyash.trivedi
 *
 */
public class JslAccountOTPGenerationEvent extends AbstractCommerceUserEvent
{

	private final JslOTPGenerationDto jslOtpGenerationDto;

	public JslAccountOTPGenerationEvent(final JslOTPGenerationDto jslOtpGenerationDto)
	{
		super();
		this.jslOtpGenerationDto = jslOtpGenerationDto;
	}

}
