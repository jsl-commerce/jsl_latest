/**
 * 
 */
package com.jsl.core.customer.complaint.response.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author himanshu.sial
 *
 */
@XmlRootElement(name = "ServiceRequest")
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomerComplaintServiceDto
{

	@XmlElement(name = "ReportedPartyName")
	private String reportedPartyName;
	@XmlElement(name = "SalesTerritoryUUID")
	private String salesTerritoryUUID;
	@XmlElement(name = "ServiceIssueCategoryID")
	private String serviceIssueCategoryID;
	@XmlElement(name = "DivisionCodeText")
	private String divisionCodeText;
	@XmlElement(name = "InstalledBaseID")
	private String installedBaseID;
	@XmlElement(name = "ResponseByProcessorDateTimeZoneCode")
	private String responseByProcessorDateTimeZoneCode;
	@XmlElement(name = "CompleteDuedatetimeContent")
	private String completeDuedatetimeContent;
	@XmlElement(name = "WarrantyGoodwillCodeText")
	private String warrantyGoodwillCodeText;
	@XmlElement(name = "FirstReactionDuedatetimeZoneCodeText")
	private String firstReactionDuedatetimeZoneCodeText;
	@XmlElement(name = "ResolutionDuetimeZoneCodeText")
	private String resolutionDuetimeZoneCodeText;
	@XmlElement(name = "ProcessorPartyUUID")
	private String processorPartyUUID;
	@XmlElement(name = "RequestFinisheddatetimeZoneCodeText")
	private String requestFinisheddatetimeZoneCodeText;
	@XmlElement(name = "RequestInitialReceiptdatetimeZoneCode")
	private String requestInitialReceiptdatetimeZoneCode;
	@XmlElement(name = "ProcessingTypeCodeText")
	private String processingTypeCodeText;
	@XmlElement(name = "ID")
	private String id;
	@XmlElement(name = "PartnerContactPartyUUID")
	private String partnerContactPartyUUID;
	@XmlElement(name = "ServiceLevelObjectiveNamelanguageCode")
	private String serviceLevelObjectiveNamelanguageCode;
	@XmlElement(name = "ResponseByProcessorDueDateTimeZoneCodeText")
	private String responseByProcessorDueDateTimeZoneCodeText;
	@XmlElement(name = "RequestFinisheddatetimeContent")
	private String requestFinisheddatetimeContent;
	@XmlElement(name = "OnSiteArrivaltimeZoneCodeText")
	private String nnSiteArrivaltimeZoneCodeText;
	@XmlElement(name = "ReportedPartyID")
	private String reportedPartyID;
	@XmlElement(name = "ActivityServiceIssueCategoryID")
	private String activityServiceIssueCategoryID;
	@XmlElement(name = "RequestedTotalRequestorDuration")
	private String requestedTotalRequestorDuration;
	@XmlElement(name = "BuyerPartyID")
	private String buyerPartyID;
	@XmlElement(name = "EscalationStatusCode")
	private String escalationStatusCode;
	@XmlElement(name = "BuyerMainContactPartyID")
	private String buyerMainContactPartyID;
	@XmlElement(name = "ResponseByRequesterDateTimeZoneCode")
	private String responseByRequesterDateTimeZoneCode;
	@XmlElement(name = "ServiceRequestClassificationCodeText")
	private String serviceRequestClassificationCodeText;
	@XmlElement(name = "ReportedPartyUUID")
	private String reportedPartyUUID;
	@XmlElement(name = "RequestInProcessdatetimeZoneCode")
	private String requestInProcessdatetimeZoneCode;
	@XmlElement(name = "ETag")
	private String eTag;
	@XmlElement(name = "OnSiteArrivalDateTime")
	private String onSiteArrivalDateTime;
	@XmlElement(name = "ResponseByProcessorDueDateTime")
	private String responseByProcessorDueDateTime;
	@XmlElement(name = "RequestInitialReceiptdatetimeZoneCodeText")
	private String requestInitialReceiptdatetimeZoneCodeText;
	@XmlElement(name = "DistributionChannelCodeText")
	private String distributionChannelCodeText;
	@XmlElement(name = "EscalationTimePointtimeZoneCode")
	private String escalationTimePointtimeZoneCode;
	@XmlElement(name = "ChangedByCustomerIndicator")
	private String changedByCustomerIndicator;
	@XmlElement(name = "ServiceExecutionTeamPartyID")
	private String serviceExecutionTeamPartyID;
	@XmlElement(name = "ResolvedOnDateTime")
	private String resolvedOnDateTime;
	@XmlElement(name = "ServicePerformerPartyName")
	private String servicePerformerPartyName;
	@XmlElement(name = "CompleteDuedatetimeZoneCodeText")
	private String completeDuedatetimeZoneCodeText;
	@XmlElement(name = "RequestedFulfillmentPeriodEndtimeZoneCode")
	private String requestedFulfillmentPeriodEndtimeZoneCode;
	@XmlElement(name = "WarrantyGoodwillCode")
	private String warrantyGoodwillCode;
	@XmlElement(name = "RequestedFulfillmentPeriodEndDateTime")
	private String requestedFulfillmentPeriodEndDateTime;
	@XmlElement(name = "RequestedFulfillmentPeriodStarttimeZoneCode")
	private String requestedFulfillmentPeriodStarttimeZoneCode;
	@XmlElement(name = "DivisionCode")
	private String divisionCode;
	@XmlElement(name = "WarrantyID")
	private String warrantyID;
	@XmlElement(name = "RequestInitialReceiptdatetimecontent")
	private String requestInitialReceiptdatetimecontent;
	@XmlElement(name = "WarrantyStartdatetimeZoneCodeText")
	private String warrantyStartdatetimeZoneCodeText;
	@XmlElement(name = "CauseServiceIssueCategoryID")
	private String causeServiceIssueCategoryID;
	@XmlElement(name = "RequestFinisheddatetimeZoneCode")
	private String requestFinisheddatetimeZoneCode;
	@XmlElement(name = "ServicePriorityCode")
	private String servicePriorityCode;
	@XmlElement(name = "ReportedForPartyName")
	private String reportedForPartyName;
	@XmlElement(name = "RequestCloseddatetimeContent")
	private String requestCloseddatetimeContent;
	@XmlElement(name = "FirstReactionDuedatetimeContent")
	private String firstReactionDuedatetimeContent;
	@XmlElement(name = "EscalationTimePointtimeZoneCodeText")
	private String escalationTimePointtimeZoneCodeText;
	@XmlElement(name = "RequestedFulfillmentPeriodEndtimeZoneCodeText")
	private String requestedFulfillmentPeriodEndtimeZoneCodeText;
	@XmlElement(name = "ProductRecipientPartyName")
	private String productRecipientPartyName;
	@XmlElement(name = "ReportedForPartyUUID")
	private String reportedForPartyUUID;
	@XmlElement(name = "EscalationTimePointDateTime")
	private String escalationTimePointDateTime;
	@XmlElement(name = "ProcessorPartyName")
	private String processorPartyName;
	@XmlElement(name = "ProductRecipientPartyUUID")
	private String productRecipientPartyUUID;
	@XmlElement(name = "ServiceExecutionTeamPartyName")
	private String serviceExecutionTeamPartyName;
	@XmlElement(name = "SalesTerritoryName")
	private String salesTerritoryName;
	@XmlElement(name = "ServicePerformerPartyUUID")
	private String servicePerformerPartyUUID;
	@XmlElement(name = "WarrantyStartdatetimeZoneCode")
	private String warrantyStartdatetimeZoneCode;
	@XmlElement(name = "ProcessorPartyID")
	private String processorPartyID;
	@XmlElement(name = "RequestedFulfillmentPeriodStarttimeZoneCodeText")
	private String requestedFulfillmentPeriodStarttimeZoneCodeText;
	@XmlElement(name = "SerialID")
	private String serialID;
	@XmlElement(name = "PartnerContactPartyID")
	private String partnerContactPartyID;
	@XmlElement(name = "ProcessingTypeCode")
	private String processingTypeCode;
	@XmlElement(name = "WarrantyStartdatetimeContent")
	private String warrantyStartdatetimeContent;
	@XmlElement(name = "ObjectServiceIssueCategoryID")
	private String objectServiceIssueCategoryID;
	@XmlElement(name = "FirstReactionDuedatetimeZoneCode")
	private String firstReactionDuedatetimeZoneCode;
	@XmlElement(name = "ObjectID")
	private String objectID;
	@XmlElement(name = "ServiceExecutionTeamPartyUUID")
	private String serviceExecutionTeamPartyUUID;
	@XmlElement(name = "ServiceLevelObjectiveNamelanguageCodeText")
	private String serviceLevelObjectiveNamelanguageCodeText;
	@XmlElement(name = "InstallationPointID")
	private String installationPointID;
	@XmlElement(name = "Name")
	private String name;
	@XmlElement(name = "CompleteDuedatetimeZoneCode")
	private String completeDuedatetimeZoneCode;
	@XmlElement(name = "ServiceRequestUserLifeCycleStatusCode")
	private String serviceRequestUserLifeCycleStatusCode;
	@XmlElement(name = "ServiceTermsServiceIssueName")
	private String serviceTermsServiceIssueName;
	@XmlElement(name = "OnSiteArrivalDueDateTime")
	private String onSiteArrivalDueDateTime;
	@XmlElement(name = "OnSiteArrivaltimeZoneCode")
	private String onSiteArrivaltimeZoneCode;
	@XmlElement(name = "UUID")
	private String uUID;
	@XmlElement(name = "ProductRecipientPartyID")
	private String productRecipientPartyID;
	@XmlElement(name = "ServiceSupportTeamPartyID")
	private String serviceSupportTeamPartyID;
	@XmlElement(name = "InstallationPointDescription")
	private String installationPointDescription;
	@XmlElement(name = "ServicePriorityCodeText")
	private String servicePriorityCodeText;
	@XmlElement(name = "ServiceRequestClassificationCode")
	private String serviceRequestClassificationCode;
	@XmlElement(name = "SalesOrganisationID")
	private String salesOrganisationID;
	@XmlElement(name = "SalesUnitPartyName")
	private String salesUnitPartyName;
	@XmlElement(name = "RequestedTotalProcessingDuration")
	private String requestedTotalProcessingDuration;
	@XmlElement(name = "ResolutionDuetimeZoneCode")
	private String resolutionDuetimeZoneCode;
	@XmlElement(name = "ResolvedOntimeZoneCode")
	private String resolvedOntimeZoneCode;
	@XmlElement(name = "OnSiteArrivalDuetimeZoneCode")
	private String onSiteArrivalDuetimeZoneCode;
	@XmlElement(name = "ServiceRequestUserLifeCycleStatusCodeText")
	private String serviceRequestUserLifeCycleStatusCodeText;
	@XmlElement(name = "InstalledBaseUUID")
	private String installedBaseUUID;
	@XmlElement(name = "RequestInProcessdatetimeContent")
	private String requestInProcessdatetimeContent;
	@XmlElement(name = "BuyerPartyUUID")
	private String buyerPartyUUID;
	@XmlElement(name = "InstalledBaseDescription")
	private String installedBaseDescription;
	@XmlElement(name = "ResolvedOntimeZoneCodeText")
	private String resolvedOntimeZoneCodeText;
	@XmlElement(name = "SalesUnitPartyUUID")
	private String salesUnitPartyUUID;
	@XmlElement(name = "RequestedFulfillmentPeriodStartDateTime")
	private String requestedFulfillmentPeriodStartDateTime;
	@XmlElement(name = "ResponseByProcessorDateTimeZoneCodeText")
	private String responseByProcessorDateTimeZoneCodeText;
	@XmlElement(name = "RequestInProcessdatetimeZoneCodeText")
	private String requestInProcessdatetimeZoneCodeText;
	@XmlElement(name = "RequestCloseddatetimeZoneCode")
	private String requestCloseddatetimeZoneCode;
	@XmlElement(name = "ResolutionDueDateTime")
	private String resolutionDueDateTime;
	@XmlElement(name = "EscalationStatusCodeText")
	private String escalationStatusCodeText;
	@XmlElement(name = "ServiceSupportTeamPartyName")
	private String serviceSupportTeamPartyName;
	@XmlElement(name = "RequestCloseddatetimeZoneCodeText")
	private String requestCloseddatetimeZoneCodeText;
	@XmlElement(name = "ContractID")
	private String contractID;
	@XmlElement(name = "DataOriginTypeCodeText")
	private String dataOriginTypeCodeText;
	@XmlElement(name = "BuyerPartyName")
	private String buyerPartyName;
	@XmlElement(name = "MainTicketID")
	private String mainTicketID;
	@XmlElement(name = "ResponseByProcessorDueDateTimeZoneCode")
	private String responseByProcessorDueDateTimeZoneCode;
	@XmlElement(name = "ServicePerformerPartyID")
	private String servicePerformerPartyID;
	@XmlElement(name = "BuyerMainContactPartyName")
	private String buyerMainContactPartyName;
	@XmlElement(name = "ServiceLevelObjectiveID")
	private String serviceLevelObjectiveID;
	@XmlElement(name = "SalesUnitPartyID")
	private String salesUnitPartyID;
	@XmlElement(name = "IncidentServiceIssueCategoryID")
	private String incidentServiceIssueCategoryID;
	@XmlElement(name = "SalesTerritoryID")
	private String salesTerritoryID;
	@XmlElement(name = "OnSiteArrivalDuetimeZoneCodeText")
	private String OnSiteArrivalDuetimeZoneCodeText;
	@XmlElement(name = "BuyerMainContactPartyUUID")
	private String buyerMainContactPartyUUID;
	@XmlElement(name = "WarrantyUUID")
	private String warrantyUUID;
	@XmlElement(name = "ServiceLevelObjectiveName")
	private String serviceLevelObjectiveName;
	@XmlElement(name = "InstallationPointUUID")
	private String installationPointUUID;
	@XmlElement(name = "ProductID")
	private String productID;
	@XmlElement(name = "ResponseByRequesterDateTimeContent")
	private String responseByRequesterDateTimeContent;
	@XmlElement(name = "CreationDateTime")
	private String creationDateTime;
	@XmlElement(name = "ResponseByProcessorDateTimeContent")
	private String responseByProcessorDateTimeContent;
	@XmlElement(name = "ReportedForPartyID")
	private String reportedForPartyID;
	@XmlElement(name = "DistributionChannelCode")
	private String distributionChannelCode;
	@XmlElement(name = "DataOriginTypeCode")
	private String dataOriginTypeCode;
	@XmlElement(name = "ProductDescription")
	private String productDescription;
	@XmlElement(name = "ResponseByRequesterDateTimeZoneCodeText")
	private String responseByRequesterDateTimeZoneCodeText;
	@XmlElement(name = "ServiceSupportTeamPartyUUID")
	private String serviceSupportTeamPartyUUID;

	/**
	 * @return the reportedPartyName
	 */
	public String getReportedPartyName()
	{
		return reportedPartyName;
	}

	/**
	 * @param reportedPartyName
	 *           the reportedPartyName to set
	 */
	public void setReportedPartyName(String reportedPartyName)
	{
		this.reportedPartyName = reportedPartyName;
	}

	/**
	 * @return the salesTerritoryUUID
	 */
	public String getSalesTerritoryUUID()
	{
		return salesTerritoryUUID;
	}

	/**
	 * @param salesTerritoryUUID
	 *           the salesTerritoryUUID to set
	 */
	public void setSalesTerritoryUUID(String salesTerritoryUUID)
	{
		this.salesTerritoryUUID = salesTerritoryUUID;
	}

	/**
	 * @return the serviceIssueCategoryID
	 */
	public String getServiceIssueCategoryID()
	{
		return serviceIssueCategoryID;
	}

	/**
	 * @param serviceIssueCategoryID
	 *           the serviceIssueCategoryID to set
	 */
	public void setServiceIssueCategoryID(String serviceIssueCategoryID)
	{
		this.serviceIssueCategoryID = serviceIssueCategoryID;
	}

	/**
	 * @return the divisionCodeText
	 */
	public String getDivisionCodeText()
	{
		return divisionCodeText;
	}

	/**
	 * @param divisionCodeText
	 *           the divisionCodeText to set
	 */
	public void setDivisionCodeText(String divisionCodeText)
	{
		this.divisionCodeText = divisionCodeText;
	}

	/**
	 * @return the installedBaseID
	 */
	public String getInstalledBaseID()
	{
		return installedBaseID;
	}

	/**
	 * @param installedBaseID
	 *           the installedBaseID to set
	 */
	public void setInstalledBaseID(String installedBaseID)
	{
		this.installedBaseID = installedBaseID;
	}

	/**
	 * @return the responseByProcessorDateTimeZoneCode
	 */
	public String getResponseByProcessorDateTimeZoneCode()
	{
		return responseByProcessorDateTimeZoneCode;
	}

	/**
	 * @param responseByProcessorDateTimeZoneCode
	 *           the responseByProcessorDateTimeZoneCode to set
	 */
	public void setResponseByProcessorDateTimeZoneCode(String responseByProcessorDateTimeZoneCode)
	{
		this.responseByProcessorDateTimeZoneCode = responseByProcessorDateTimeZoneCode;
	}

	/**
	 * @return the completeDuedatetimeContent
	 */
	public String getCompleteDuedatetimeContent()
	{
		return completeDuedatetimeContent;
	}

	/**
	 * @param completeDuedatetimeContent
	 *           the completeDuedatetimeContent to set
	 */
	public void setCompleteDuedatetimeContent(String completeDuedatetimeContent)
	{
		this.completeDuedatetimeContent = completeDuedatetimeContent;
	}

	/**
	 * @return the warrantyGoodwillCodeText
	 */
	public String getWarrantyGoodwillCodeText()
	{
		return warrantyGoodwillCodeText;
	}

	/**
	 * @param warrantyGoodwillCodeText
	 *           the warrantyGoodwillCodeText to set
	 */
	public void setWarrantyGoodwillCodeText(String warrantyGoodwillCodeText)
	{
		this.warrantyGoodwillCodeText = warrantyGoodwillCodeText;
	}

	/**
	 * @return the firstReactionDuedatetimeZoneCodeText
	 */
	public String getFirstReactionDuedatetimeZoneCodeText()
	{
		return firstReactionDuedatetimeZoneCodeText;
	}

	/**
	 * @param firstReactionDuedatetimeZoneCodeText
	 *           the firstReactionDuedatetimeZoneCodeText to set
	 */
	public void setFirstReactionDuedatetimeZoneCodeText(String firstReactionDuedatetimeZoneCodeText)
	{
		this.firstReactionDuedatetimeZoneCodeText = firstReactionDuedatetimeZoneCodeText;
	}

	/**
	 * @return the resolutionDuetimeZoneCodeText
	 */
	public String getResolutionDuetimeZoneCodeText()
	{
		return resolutionDuetimeZoneCodeText;
	}

	/**
	 * @param resolutionDuetimeZoneCodeText
	 *           the resolutionDuetimeZoneCodeText to set
	 */
	public void setResolutionDuetimeZoneCodeText(String resolutionDuetimeZoneCodeText)
	{
		this.resolutionDuetimeZoneCodeText = resolutionDuetimeZoneCodeText;
	}

	/**
	 * @return the processorPartyUUID
	 */
	public String getProcessorPartyUUID()
	{
		return processorPartyUUID;
	}

	/**
	 * @param processorPartyUUID
	 *           the processorPartyUUID to set
	 */
	public void setProcessorPartyUUID(String processorPartyUUID)
	{
		this.processorPartyUUID = processorPartyUUID;
	}

	/**
	 * @return the requestFinisheddatetimeZoneCodeText
	 */
	public String getRequestFinisheddatetimeZoneCodeText()
	{
		return requestFinisheddatetimeZoneCodeText;
	}

	/**
	 * @param requestFinisheddatetimeZoneCodeText
	 *           the requestFinisheddatetimeZoneCodeText to set
	 */
	public void setRequestFinisheddatetimeZoneCodeText(String requestFinisheddatetimeZoneCodeText)
	{
		this.requestFinisheddatetimeZoneCodeText = requestFinisheddatetimeZoneCodeText;
	}

	/**
	 * @return the requestInitialReceiptdatetimeZoneCode
	 */
	public String getRequestInitialReceiptdatetimeZoneCode()
	{
		return requestInitialReceiptdatetimeZoneCode;
	}

	/**
	 * @param requestInitialReceiptdatetimeZoneCode
	 *           the requestInitialReceiptdatetimeZoneCode to set
	 */
	public void setRequestInitialReceiptdatetimeZoneCode(String requestInitialReceiptdatetimeZoneCode)
	{
		this.requestInitialReceiptdatetimeZoneCode = requestInitialReceiptdatetimeZoneCode;
	}

	/**
	 * @return the processingTypeCodeText
	 */
	public String getProcessingTypeCodeText()
	{
		return processingTypeCodeText;
	}

	/**
	 * @param processingTypeCodeText
	 *           the processingTypeCodeText to set
	 */
	public void setProcessingTypeCodeText(String processingTypeCodeText)
	{
		this.processingTypeCodeText = processingTypeCodeText;
	}

	/**
	 * @return the id
	 */
	public String getId()
	{
		return id;
	}

	/**
	 * @param id
	 *           the id to set
	 */
	public void setId(String id)
	{
		this.id = id;
	}

	/**
	 * @return the partnerContactPartyUUID
	 */
	public String getPartnerContactPartyUUID()
	{
		return partnerContactPartyUUID;
	}

	/**
	 * @param partnerContactPartyUUID
	 *           the partnerContactPartyUUID to set
	 */
	public void setPartnerContactPartyUUID(String partnerContactPartyUUID)
	{
		this.partnerContactPartyUUID = partnerContactPartyUUID;
	}

	/**
	 * @return the serviceLevelObjectiveNamelanguageCode
	 */
	public String getServiceLevelObjectiveNamelanguageCode()
	{
		return serviceLevelObjectiveNamelanguageCode;
	}

	/**
	 * @param serviceLevelObjectiveNamelanguageCode
	 *           the serviceLevelObjectiveNamelanguageCode to set
	 */
	public void setServiceLevelObjectiveNamelanguageCode(String serviceLevelObjectiveNamelanguageCode)
	{
		this.serviceLevelObjectiveNamelanguageCode = serviceLevelObjectiveNamelanguageCode;
	}

	/**
	 * @return the responseByProcessorDueDateTimeZoneCodeText
	 */
	public String getResponseByProcessorDueDateTimeZoneCodeText()
	{
		return responseByProcessorDueDateTimeZoneCodeText;
	}

	/**
	 * @param responseByProcessorDueDateTimeZoneCodeText
	 *           the responseByProcessorDueDateTimeZoneCodeText to set
	 */
	public void setResponseByProcessorDueDateTimeZoneCodeText(String responseByProcessorDueDateTimeZoneCodeText)
	{
		this.responseByProcessorDueDateTimeZoneCodeText = responseByProcessorDueDateTimeZoneCodeText;
	}

	/**
	 * @return the requestFinisheddatetimeContent
	 */
	public String getRequestFinisheddatetimeContent()
	{
		return requestFinisheddatetimeContent;
	}

	/**
	 * @param requestFinisheddatetimeContent
	 *           the requestFinisheddatetimeContent to set
	 */
	public void setRequestFinisheddatetimeContent(String requestFinisheddatetimeContent)
	{
		this.requestFinisheddatetimeContent = requestFinisheddatetimeContent;
	}

	/**
	 * @return the nnSiteArrivaltimeZoneCodeText
	 */
	public String getNnSiteArrivaltimeZoneCodeText()
	{
		return nnSiteArrivaltimeZoneCodeText;
	}

	/**
	 * @param nnSiteArrivaltimeZoneCodeText
	 *           the nnSiteArrivaltimeZoneCodeText to set
	 */
	public void setNnSiteArrivaltimeZoneCodeText(String nnSiteArrivaltimeZoneCodeText)
	{
		this.nnSiteArrivaltimeZoneCodeText = nnSiteArrivaltimeZoneCodeText;
	}

	/**
	 * @return the reportedPartyID
	 */
	public String getReportedPartyID()
	{
		return reportedPartyID;
	}

	/**
	 * @param reportedPartyID
	 *           the reportedPartyID to set
	 */
	public void setReportedPartyID(String reportedPartyID)
	{
		this.reportedPartyID = reportedPartyID;
	}

	/**
	 * @return the activityServiceIssueCategoryID
	 */
	public String getActivityServiceIssueCategoryID()
	{
		return activityServiceIssueCategoryID;
	}

	/**
	 * @param activityServiceIssueCategoryID
	 *           the activityServiceIssueCategoryID to set
	 */
	public void setActivityServiceIssueCategoryID(String activityServiceIssueCategoryID)
	{
		this.activityServiceIssueCategoryID = activityServiceIssueCategoryID;
	}

	/**
	 * @return the requestedTotalRequestorDuration
	 */
	public String getRequestedTotalRequestorDuration()
	{
		return requestedTotalRequestorDuration;
	}

	/**
	 * @param requestedTotalRequestorDuration
	 *           the requestedTotalRequestorDuration to set
	 */
	public void setRequestedTotalRequestorDuration(String requestedTotalRequestorDuration)
	{
		this.requestedTotalRequestorDuration = requestedTotalRequestorDuration;
	}

	/**
	 * @return the buyerPartyID
	 */
	public String getBuyerPartyID()
	{
		return buyerPartyID;
	}

	/**
	 * @param buyerPartyID
	 *           the buyerPartyID to set
	 */
	public void setBuyerPartyID(String buyerPartyID)
	{
		this.buyerPartyID = buyerPartyID;
	}

	/**
	 * @return the escalationStatusCode
	 */
	public String getEscalationStatusCode()
	{
		return escalationStatusCode;
	}

	/**
	 * @param escalationStatusCode
	 *           the escalationStatusCode to set
	 */
	public void setEscalationStatusCode(String escalationStatusCode)
	{
		this.escalationStatusCode = escalationStatusCode;
	}

	/**
	 * @return the buyerMainContactPartyID
	 */
	public String getBuyerMainContactPartyID()
	{
		return buyerMainContactPartyID;
	}

	/**
	 * @param buyerMainContactPartyID
	 *           the buyerMainContactPartyID to set
	 */
	public void setBuyerMainContactPartyID(String buyerMainContactPartyID)
	{
		this.buyerMainContactPartyID = buyerMainContactPartyID;
	}

	/**
	 * @return the responseByRequesterDateTimeZoneCode
	 */
	public String getResponseByRequesterDateTimeZoneCode()
	{
		return responseByRequesterDateTimeZoneCode;
	}

	/**
	 * @param responseByRequesterDateTimeZoneCode
	 *           the responseByRequesterDateTimeZoneCode to set
	 */
	public void setResponseByRequesterDateTimeZoneCode(String responseByRequesterDateTimeZoneCode)
	{
		this.responseByRequesterDateTimeZoneCode = responseByRequesterDateTimeZoneCode;
	}

	/**
	 * @return the serviceRequestClassificationCodeText
	 */
	public String getServiceRequestClassificationCodeText()
	{
		return serviceRequestClassificationCodeText;
	}

	/**
	 * @param serviceRequestClassificationCodeText
	 *           the serviceRequestClassificationCodeText to set
	 */
	public void setServiceRequestClassificationCodeText(String serviceRequestClassificationCodeText)
	{
		this.serviceRequestClassificationCodeText = serviceRequestClassificationCodeText;
	}

	/**
	 * @return the reportedPartyUUID
	 */
	public String getReportedPartyUUID()
	{
		return reportedPartyUUID;
	}

	/**
	 * @param reportedPartyUUID
	 *           the reportedPartyUUID to set
	 */
	public void setReportedPartyUUID(String reportedPartyUUID)
	{
		this.reportedPartyUUID = reportedPartyUUID;
	}

	/**
	 * @return the requestInProcessdatetimeZoneCode
	 */
	public String getRequestInProcessdatetimeZoneCode()
	{
		return requestInProcessdatetimeZoneCode;
	}

	/**
	 * @param requestInProcessdatetimeZoneCode
	 *           the requestInProcessdatetimeZoneCode to set
	 */
	public void setRequestInProcessdatetimeZoneCode(String requestInProcessdatetimeZoneCode)
	{
		this.requestInProcessdatetimeZoneCode = requestInProcessdatetimeZoneCode;
	}

	/**
	 * @return the eTag
	 */
	public String geteTag()
	{
		return eTag;
	}

	/**
	 * @param eTag
	 *           the eTag to set
	 */
	public void seteTag(String eTag)
	{
		this.eTag = eTag;
	}

	/**
	 * @return the onSiteArrivalDateTime
	 */
	public String getOnSiteArrivalDateTime()
	{
		return onSiteArrivalDateTime;
	}

	/**
	 * @param onSiteArrivalDateTime
	 *           the onSiteArrivalDateTime to set
	 */
	public void setOnSiteArrivalDateTime(String onSiteArrivalDateTime)
	{
		this.onSiteArrivalDateTime = onSiteArrivalDateTime;
	}

	/**
	 * @return the responseByProcessorDueDateTime
	 */
	public String getResponseByProcessorDueDateTime()
	{
		return responseByProcessorDueDateTime;
	}

	/**
	 * @param responseByProcessorDueDateTime
	 *           the responseByProcessorDueDateTime to set
	 */
	public void setResponseByProcessorDueDateTime(String responseByProcessorDueDateTime)
	{
		this.responseByProcessorDueDateTime = responseByProcessorDueDateTime;
	}

	/**
	 * @return the requestInitialReceiptdatetimeZoneCodeText
	 */
	public String getRequestInitialReceiptdatetimeZoneCodeText()
	{
		return requestInitialReceiptdatetimeZoneCodeText;
	}

	/**
	 * @param requestInitialReceiptdatetimeZoneCodeText
	 *           the requestInitialReceiptdatetimeZoneCodeText to set
	 */
	public void setRequestInitialReceiptdatetimeZoneCodeText(String requestInitialReceiptdatetimeZoneCodeText)
	{
		this.requestInitialReceiptdatetimeZoneCodeText = requestInitialReceiptdatetimeZoneCodeText;
	}

	/**
	 * @return the distributionChannelCodeText
	 */
	public String getDistributionChannelCodeText()
	{
		return distributionChannelCodeText;
	}

	/**
	 * @param distributionChannelCodeText
	 *           the distributionChannelCodeText to set
	 */
	public void setDistributionChannelCodeText(String distributionChannelCodeText)
	{
		this.distributionChannelCodeText = distributionChannelCodeText;
	}

	/**
	 * @return the escalationTimePointtimeZoneCode
	 */
	public String getEscalationTimePointtimeZoneCode()
	{
		return escalationTimePointtimeZoneCode;
	}

	/**
	 * @param escalationTimePointtimeZoneCode
	 *           the escalationTimePointtimeZoneCode to set
	 */
	public void setEscalationTimePointtimeZoneCode(String escalationTimePointtimeZoneCode)
	{
		this.escalationTimePointtimeZoneCode = escalationTimePointtimeZoneCode;
	}

	/**
	 * @return the changedByCustomerIndicator
	 */
	public String getChangedByCustomerIndicator()
	{
		return changedByCustomerIndicator;
	}

	/**
	 * @param changedByCustomerIndicator
	 *           the changedByCustomerIndicator to set
	 */
	public void setChangedByCustomerIndicator(String changedByCustomerIndicator)
	{
		this.changedByCustomerIndicator = changedByCustomerIndicator;
	}

	/**
	 * @return the serviceExecutionTeamPartyID
	 */
	public String getServiceExecutionTeamPartyID()
	{
		return serviceExecutionTeamPartyID;
	}

	/**
	 * @param serviceExecutionTeamPartyID
	 *           the serviceExecutionTeamPartyID to set
	 */
	public void setServiceExecutionTeamPartyID(String serviceExecutionTeamPartyID)
	{
		this.serviceExecutionTeamPartyID = serviceExecutionTeamPartyID;
	}

	/**
	 * @return the resolvedOnDateTime
	 */
	public String getResolvedOnDateTime()
	{
		return resolvedOnDateTime;
	}

	/**
	 * @param resolvedOnDateTime
	 *           the resolvedOnDateTime to set
	 */
	public void setResolvedOnDateTime(String resolvedOnDateTime)
	{
		this.resolvedOnDateTime = resolvedOnDateTime;
	}

	/**
	 * @return the servicePerformerPartyName
	 */
	public String getServicePerformerPartyName()
	{
		return servicePerformerPartyName;
	}

	/**
	 * @param servicePerformerPartyName
	 *           the servicePerformerPartyName to set
	 */
	public void setServicePerformerPartyName(String servicePerformerPartyName)
	{
		this.servicePerformerPartyName = servicePerformerPartyName;
	}

	/**
	 * @return the completeDuedatetimeZoneCodeText
	 */
	public String getCompleteDuedatetimeZoneCodeText()
	{
		return completeDuedatetimeZoneCodeText;
	}

	/**
	 * @param completeDuedatetimeZoneCodeText
	 *           the completeDuedatetimeZoneCodeText to set
	 */
	public void setCompleteDuedatetimeZoneCodeText(String completeDuedatetimeZoneCodeText)
	{
		this.completeDuedatetimeZoneCodeText = completeDuedatetimeZoneCodeText;
	}

	/**
	 * @return the requestedFulfillmentPeriodEndtimeZoneCode
	 */
	public String getRequestedFulfillmentPeriodEndtimeZoneCode()
	{
		return requestedFulfillmentPeriodEndtimeZoneCode;
	}

	/**
	 * @param requestedFulfillmentPeriodEndtimeZoneCode
	 *           the requestedFulfillmentPeriodEndtimeZoneCode to set
	 */
	public void setRequestedFulfillmentPeriodEndtimeZoneCode(String requestedFulfillmentPeriodEndtimeZoneCode)
	{
		this.requestedFulfillmentPeriodEndtimeZoneCode = requestedFulfillmentPeriodEndtimeZoneCode;
	}

	/**
	 * @return the warrantyGoodwillCode
	 */
	public String getWarrantyGoodwillCode()
	{
		return warrantyGoodwillCode;
	}

	/**
	 * @param warrantyGoodwillCode
	 *           the warrantyGoodwillCode to set
	 */
	public void setWarrantyGoodwillCode(String warrantyGoodwillCode)
	{
		this.warrantyGoodwillCode = warrantyGoodwillCode;
	}

	/**
	 * @return the requestedFulfillmentPeriodEndDateTime
	 */
	public String getRequestedFulfillmentPeriodEndDateTime()
	{
		return requestedFulfillmentPeriodEndDateTime;
	}

	/**
	 * @param requestedFulfillmentPeriodEndDateTime
	 *           the requestedFulfillmentPeriodEndDateTime to set
	 */
	public void setRequestedFulfillmentPeriodEndDateTime(String requestedFulfillmentPeriodEndDateTime)
	{
		this.requestedFulfillmentPeriodEndDateTime = requestedFulfillmentPeriodEndDateTime;
	}

	/**
	 * @return the requestedFulfillmentPeriodStarttimeZoneCode
	 */
	public String getRequestedFulfillmentPeriodStarttimeZoneCode()
	{
		return requestedFulfillmentPeriodStarttimeZoneCode;
	}

	/**
	 * @param requestedFulfillmentPeriodStarttimeZoneCode
	 *           the requestedFulfillmentPeriodStarttimeZoneCode to set
	 */
	public void setRequestedFulfillmentPeriodStarttimeZoneCode(String requestedFulfillmentPeriodStarttimeZoneCode)
	{
		this.requestedFulfillmentPeriodStarttimeZoneCode = requestedFulfillmentPeriodStarttimeZoneCode;
	}

	/**
	 * @return the divisionCode
	 */
	public String getDivisionCode()
	{
		return divisionCode;
	}

	/**
	 * @param divisionCode
	 *           the divisionCode to set
	 */
	public void setDivisionCode(String divisionCode)
	{
		this.divisionCode = divisionCode;
	}

	/**
	 * @return the warrantyID
	 */
	public String getWarrantyID()
	{
		return warrantyID;
	}

	/**
	 * @param warrantyID
	 *           the warrantyID to set
	 */
	public void setWarrantyID(String warrantyID)
	{
		this.warrantyID = warrantyID;
	}

	/**
	 * @return the requestInitialReceiptdatetimecontent
	 */
	public String getRequestInitialReceiptdatetimecontent()
	{
		return requestInitialReceiptdatetimecontent;
	}

	/**
	 * @param requestInitialReceiptdatetimecontent
	 *           the requestInitialReceiptdatetimecontent to set
	 */
	public void setRequestInitialReceiptdatetimecontent(String requestInitialReceiptdatetimecontent)
	{
		this.requestInitialReceiptdatetimecontent = requestInitialReceiptdatetimecontent;
	}

	/**
	 * @return the warrantyStartdatetimeZoneCodeText
	 */
	public String getWarrantyStartdatetimeZoneCodeText()
	{
		return warrantyStartdatetimeZoneCodeText;
	}

	/**
	 * @param warrantyStartdatetimeZoneCodeText
	 *           the warrantyStartdatetimeZoneCodeText to set
	 */
	public void setWarrantyStartdatetimeZoneCodeText(String warrantyStartdatetimeZoneCodeText)
	{
		this.warrantyStartdatetimeZoneCodeText = warrantyStartdatetimeZoneCodeText;
	}

	/**
	 * @return the causeServiceIssueCategoryID
	 */
	public String getCauseServiceIssueCategoryID()
	{
		return causeServiceIssueCategoryID;
	}

	/**
	 * @param causeServiceIssueCategoryID
	 *           the causeServiceIssueCategoryID to set
	 */
	public void setCauseServiceIssueCategoryID(String causeServiceIssueCategoryID)
	{
		this.causeServiceIssueCategoryID = causeServiceIssueCategoryID;
	}

	/**
	 * @return the requestFinisheddatetimeZoneCode
	 */
	public String getRequestFinisheddatetimeZoneCode()
	{
		return requestFinisheddatetimeZoneCode;
	}

	/**
	 * @param requestFinisheddatetimeZoneCode
	 *           the requestFinisheddatetimeZoneCode to set
	 */
	public void setRequestFinisheddatetimeZoneCode(String requestFinisheddatetimeZoneCode)
	{
		this.requestFinisheddatetimeZoneCode = requestFinisheddatetimeZoneCode;
	}

	/**
	 * @return the servicePriorityCode
	 */
	public String getServicePriorityCode()
	{
		return servicePriorityCode;
	}

	/**
	 * @param servicePriorityCode
	 *           the servicePriorityCode to set
	 */
	public void setServicePriorityCode(String servicePriorityCode)
	{
		this.servicePriorityCode = servicePriorityCode;
	}

	/**
	 * @return the reportedForPartyName
	 */
	public String getReportedForPartyName()
	{
		return reportedForPartyName;
	}

	/**
	 * @param reportedForPartyName
	 *           the reportedForPartyName to set
	 */
	public void setReportedForPartyName(String reportedForPartyName)
	{
		this.reportedForPartyName = reportedForPartyName;
	}

	/**
	 * @return the requestCloseddatetimeContent
	 */
	public String getRequestCloseddatetimeContent()
	{
		return requestCloseddatetimeContent;
	}

	/**
	 * @param requestCloseddatetimeContent
	 *           the requestCloseddatetimeContent to set
	 */
	public void setRequestCloseddatetimeContent(String requestCloseddatetimeContent)
	{
		this.requestCloseddatetimeContent = requestCloseddatetimeContent;
	}

	/**
	 * @return the firstReactionDuedatetimeContent
	 */
	public String getFirstReactionDuedatetimeContent()
	{
		return firstReactionDuedatetimeContent;
	}

	/**
	 * @param firstReactionDuedatetimeContent
	 *           the firstReactionDuedatetimeContent to set
	 */
	public void setFirstReactionDuedatetimeContent(String firstReactionDuedatetimeContent)
	{
		this.firstReactionDuedatetimeContent = firstReactionDuedatetimeContent;
	}

	/**
	 * @return the escalationTimePointtimeZoneCodeText
	 */
	public String getEscalationTimePointtimeZoneCodeText()
	{
		return escalationTimePointtimeZoneCodeText;
	}

	/**
	 * @param escalationTimePointtimeZoneCodeText
	 *           the escalationTimePointtimeZoneCodeText to set
	 */
	public void setEscalationTimePointtimeZoneCodeText(String escalationTimePointtimeZoneCodeText)
	{
		this.escalationTimePointtimeZoneCodeText = escalationTimePointtimeZoneCodeText;
	}

	/**
	 * @return the requestedFulfillmentPeriodEndtimeZoneCodeText
	 */
	public String getRequestedFulfillmentPeriodEndtimeZoneCodeText()
	{
		return requestedFulfillmentPeriodEndtimeZoneCodeText;
	}

	/**
	 * @param requestedFulfillmentPeriodEndtimeZoneCodeText
	 *           the requestedFulfillmentPeriodEndtimeZoneCodeText to set
	 */
	public void setRequestedFulfillmentPeriodEndtimeZoneCodeText(String requestedFulfillmentPeriodEndtimeZoneCodeText)
	{
		this.requestedFulfillmentPeriodEndtimeZoneCodeText = requestedFulfillmentPeriodEndtimeZoneCodeText;
	}

	/**
	 * @return the productRecipientPartyName
	 */
	public String getProductRecipientPartyName()
	{
		return productRecipientPartyName;
	}

	/**
	 * @param productRecipientPartyName
	 *           the productRecipientPartyName to set
	 */
	public void setProductRecipientPartyName(String productRecipientPartyName)
	{
		this.productRecipientPartyName = productRecipientPartyName;
	}

	/**
	 * @return the reportedForPartyUUID
	 */
	public String getReportedForPartyUUID()
	{
		return reportedForPartyUUID;
	}

	/**
	 * @param reportedForPartyUUID
	 *           the reportedForPartyUUID to set
	 */
	public void setReportedForPartyUUID(String reportedForPartyUUID)
	{
		this.reportedForPartyUUID = reportedForPartyUUID;
	}

	/**
	 * @return the escalationTimePointDateTime
	 */
	public String getEscalationTimePointDateTime()
	{
		return escalationTimePointDateTime;
	}

	/**
	 * @param escalationTimePointDateTime
	 *           the escalationTimePointDateTime to set
	 */
	public void setEscalationTimePointDateTime(String escalationTimePointDateTime)
	{
		this.escalationTimePointDateTime = escalationTimePointDateTime;
	}

	/**
	 * @return the processorPartyName
	 */
	public String getProcessorPartyName()
	{
		return processorPartyName;
	}

	/**
	 * @param processorPartyName
	 *           the processorPartyName to set
	 */
	public void setProcessorPartyName(String processorPartyName)
	{
		this.processorPartyName = processorPartyName;
	}

	/**
	 * @return the productRecipientPartyUUID
	 */
	public String getProductRecipientPartyUUID()
	{
		return productRecipientPartyUUID;
	}

	/**
	 * @param productRecipientPartyUUID
	 *           the productRecipientPartyUUID to set
	 */
	public void setProductRecipientPartyUUID(String productRecipientPartyUUID)
	{
		this.productRecipientPartyUUID = productRecipientPartyUUID;
	}

	/**
	 * @return the serviceExecutionTeamPartyName
	 */
	public String getServiceExecutionTeamPartyName()
	{
		return serviceExecutionTeamPartyName;
	}

	/**
	 * @param serviceExecutionTeamPartyName
	 *           the serviceExecutionTeamPartyName to set
	 */
	public void setServiceExecutionTeamPartyName(String serviceExecutionTeamPartyName)
	{
		this.serviceExecutionTeamPartyName = serviceExecutionTeamPartyName;
	}

	/**
	 * @return the salesTerritoryName
	 */
	public String getSalesTerritoryName()
	{
		return salesTerritoryName;
	}

	/**
	 * @param salesTerritoryName
	 *           the salesTerritoryName to set
	 */
	public void setSalesTerritoryName(String salesTerritoryName)
	{
		this.salesTerritoryName = salesTerritoryName;
	}

	/**
	 * @return the servicePerformerPartyUUID
	 */
	public String getServicePerformerPartyUUID()
	{
		return servicePerformerPartyUUID;
	}

	/**
	 * @param servicePerformerPartyUUID
	 *           the servicePerformerPartyUUID to set
	 */
	public void setServicePerformerPartyUUID(String servicePerformerPartyUUID)
	{
		this.servicePerformerPartyUUID = servicePerformerPartyUUID;
	}

	/**
	 * @return the warrantyStartdatetimeZoneCode
	 */
	public String getWarrantyStartdatetimeZoneCode()
	{
		return warrantyStartdatetimeZoneCode;
	}

	/**
	 * @param warrantyStartdatetimeZoneCode
	 *           the warrantyStartdatetimeZoneCode to set
	 */
	public void setWarrantyStartdatetimeZoneCode(String warrantyStartdatetimeZoneCode)
	{
		this.warrantyStartdatetimeZoneCode = warrantyStartdatetimeZoneCode;
	}

	/**
	 * @return the processorPartyID
	 */
	public String getProcessorPartyID()
	{
		return processorPartyID;
	}

	/**
	 * @param processorPartyID
	 *           the processorPartyID to set
	 */
	public void setProcessorPartyID(String processorPartyID)
	{
		this.processorPartyID = processorPartyID;
	}

	/**
	 * @return the requestedFulfillmentPeriodStarttimeZoneCodeText
	 */
	public String getRequestedFulfillmentPeriodStarttimeZoneCodeText()
	{
		return requestedFulfillmentPeriodStarttimeZoneCodeText;
	}

	/**
	 * @param requestedFulfillmentPeriodStarttimeZoneCodeText
	 *           the requestedFulfillmentPeriodStarttimeZoneCodeText to set
	 */
	public void setRequestedFulfillmentPeriodStarttimeZoneCodeText(String requestedFulfillmentPeriodStarttimeZoneCodeText)
	{
		this.requestedFulfillmentPeriodStarttimeZoneCodeText = requestedFulfillmentPeriodStarttimeZoneCodeText;
	}

	/**
	 * @return the serialID
	 */
	public String getSerialID()
	{
		return serialID;
	}

	/**
	 * @param serialID
	 *           the serialID to set
	 */
	public void setSerialID(String serialID)
	{
		this.serialID = serialID;
	}

	/**
	 * @return the partnerContactPartyID
	 */
	public String getPartnerContactPartyID()
	{
		return partnerContactPartyID;
	}

	/**
	 * @param partnerContactPartyID
	 *           the partnerContactPartyID to set
	 */
	public void setPartnerContactPartyID(String partnerContactPartyID)
	{
		this.partnerContactPartyID = partnerContactPartyID;
	}

	/**
	 * @return the processingTypeCode
	 */
	public String getProcessingTypeCode()
	{
		return processingTypeCode;
	}

	/**
	 * @param processingTypeCode
	 *           the processingTypeCode to set
	 */
	public void setProcessingTypeCode(String processingTypeCode)
	{
		this.processingTypeCode = processingTypeCode;
	}

	/**
	 * @return the warrantyStartdatetimeContent
	 */
	public String getWarrantyStartdatetimeContent()
	{
		return warrantyStartdatetimeContent;
	}

	/**
	 * @param warrantyStartdatetimeContent
	 *           the warrantyStartdatetimeContent to set
	 */
	public void setWarrantyStartdatetimeContent(String warrantyStartdatetimeContent)
	{
		this.warrantyStartdatetimeContent = warrantyStartdatetimeContent;
	}

	/**
	 * @return the objectServiceIssueCategoryID
	 */
	public String getObjectServiceIssueCategoryID()
	{
		return objectServiceIssueCategoryID;
	}

	/**
	 * @param objectServiceIssueCategoryID
	 *           the objectServiceIssueCategoryID to set
	 */
	public void setObjectServiceIssueCategoryID(String objectServiceIssueCategoryID)
	{
		this.objectServiceIssueCategoryID = objectServiceIssueCategoryID;
	}

	/**
	 * @return the firstReactionDuedatetimeZoneCode
	 */
	public String getFirstReactionDuedatetimeZoneCode()
	{
		return firstReactionDuedatetimeZoneCode;
	}

	/**
	 * @param firstReactionDuedatetimeZoneCode
	 *           the firstReactionDuedatetimeZoneCode to set
	 */
	public void setFirstReactionDuedatetimeZoneCode(String firstReactionDuedatetimeZoneCode)
	{
		this.firstReactionDuedatetimeZoneCode = firstReactionDuedatetimeZoneCode;
	}

	/**
	 * @return the objectID
	 */
	public String getObjectID()
	{
		return objectID;
	}

	/**
	 * @param objectID
	 *           the objectID to set
	 */
	public void setObjectID(String objectID)
	{
		this.objectID = objectID;
	}

	/**
	 * @return the serviceExecutionTeamPartyUUID
	 */
	public String getServiceExecutionTeamPartyUUID()
	{
		return serviceExecutionTeamPartyUUID;
	}

	/**
	 * @param serviceExecutionTeamPartyUUID
	 *           the serviceExecutionTeamPartyUUID to set
	 */
	public void setServiceExecutionTeamPartyUUID(String serviceExecutionTeamPartyUUID)
	{
		this.serviceExecutionTeamPartyUUID = serviceExecutionTeamPartyUUID;
	}

	/**
	 * @return the serviceLevelObjectiveNamelanguageCodeText
	 */
	public String getServiceLevelObjectiveNamelanguageCodeText()
	{
		return serviceLevelObjectiveNamelanguageCodeText;
	}

	/**
	 * @param serviceLevelObjectiveNamelanguageCodeText
	 *           the serviceLevelObjectiveNamelanguageCodeText to set
	 */
	public void setServiceLevelObjectiveNamelanguageCodeText(String serviceLevelObjectiveNamelanguageCodeText)
	{
		this.serviceLevelObjectiveNamelanguageCodeText = serviceLevelObjectiveNamelanguageCodeText;
	}

	/**
	 * @return the installationPointID
	 */
	public String getInstallationPointID()
	{
		return installationPointID;
	}

	/**
	 * @param installationPointID
	 *           the installationPointID to set
	 */
	public void setInstallationPointID(String installationPointID)
	{
		this.installationPointID = installationPointID;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name
	 *           the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @return the completeDuedatetimeZoneCode
	 */
	public String getCompleteDuedatetimeZoneCode()
	{
		return completeDuedatetimeZoneCode;
	}

	/**
	 * @param completeDuedatetimeZoneCode
	 *           the completeDuedatetimeZoneCode to set
	 */
	public void setCompleteDuedatetimeZoneCode(String completeDuedatetimeZoneCode)
	{
		this.completeDuedatetimeZoneCode = completeDuedatetimeZoneCode;
	}

	/**
	 * @return the serviceRequestUserLifeCycleStatusCode
	 */
	public String getServiceRequestUserLifeCycleStatusCode()
	{
		return serviceRequestUserLifeCycleStatusCode;
	}

	/**
	 * @param serviceRequestUserLifeCycleStatusCode
	 *           the serviceRequestUserLifeCycleStatusCode to set
	 */
	public void setServiceRequestUserLifeCycleStatusCode(String serviceRequestUserLifeCycleStatusCode)
	{
		this.serviceRequestUserLifeCycleStatusCode = serviceRequestUserLifeCycleStatusCode;
	}

	/**
	 * @return the serviceTermsServiceIssueName
	 */
	public String getServiceTermsServiceIssueName()
	{
		return serviceTermsServiceIssueName;
	}

	/**
	 * @param serviceTermsServiceIssueName
	 *           the serviceTermsServiceIssueName to set
	 */
	public void setServiceTermsServiceIssueName(String serviceTermsServiceIssueName)
	{
		this.serviceTermsServiceIssueName = serviceTermsServiceIssueName;
	}

	/**
	 * @return the onSiteArrivalDueDateTime
	 */
	public String getOnSiteArrivalDueDateTime()
	{
		return onSiteArrivalDueDateTime;
	}

	/**
	 * @param onSiteArrivalDueDateTime
	 *           the onSiteArrivalDueDateTime to set
	 */
	public void setOnSiteArrivalDueDateTime(String onSiteArrivalDueDateTime)
	{
		this.onSiteArrivalDueDateTime = onSiteArrivalDueDateTime;
	}

	/**
	 * @return the onSiteArrivaltimeZoneCode
	 */
	public String getOnSiteArrivaltimeZoneCode()
	{
		return onSiteArrivaltimeZoneCode;
	}

	/**
	 * @param onSiteArrivaltimeZoneCode
	 *           the onSiteArrivaltimeZoneCode to set
	 */
	public void setOnSiteArrivaltimeZoneCode(String onSiteArrivaltimeZoneCode)
	{
		this.onSiteArrivaltimeZoneCode = onSiteArrivaltimeZoneCode;
	}

	/**
	 * @return the uUID
	 */
	public String getuUID()
	{
		return uUID;
	}

	/**
	 * @param uUID
	 *           the uUID to set
	 */
	public void setuUID(String uUID)
	{
		this.uUID = uUID;
	}

	/**
	 * @return the productRecipientPartyID
	 */
	public String getProductRecipientPartyID()
	{
		return productRecipientPartyID;
	}

	/**
	 * @param productRecipientPartyID
	 *           the productRecipientPartyID to set
	 */
	public void setProductRecipientPartyID(String productRecipientPartyID)
	{
		this.productRecipientPartyID = productRecipientPartyID;
	}

	/**
	 * @return the serviceSupportTeamPartyID
	 */
	public String getServiceSupportTeamPartyID()
	{
		return serviceSupportTeamPartyID;
	}

	/**
	 * @param serviceSupportTeamPartyID
	 *           the serviceSupportTeamPartyID to set
	 */
	public void setServiceSupportTeamPartyID(String serviceSupportTeamPartyID)
	{
		this.serviceSupportTeamPartyID = serviceSupportTeamPartyID;
	}

	/**
	 * @return the installationPointDescription
	 */
	public String getInstallationPointDescription()
	{
		return installationPointDescription;
	}

	/**
	 * @param installationPointDescription
	 *           the installationPointDescription to set
	 */
	public void setInstallationPointDescription(String installationPointDescription)
	{
		this.installationPointDescription = installationPointDescription;
	}

	/**
	 * @return the servicePriorityCodeText
	 */
	public String getServicePriorityCodeText()
	{
		return servicePriorityCodeText;
	}

	/**
	 * @param servicePriorityCodeText
	 *           the servicePriorityCodeText to set
	 */
	public void setServicePriorityCodeText(String servicePriorityCodeText)
	{
		this.servicePriorityCodeText = servicePriorityCodeText;
	}

	/**
	 * @return the serviceRequestClassificationCode
	 */
	public String getServiceRequestClassificationCode()
	{
		return serviceRequestClassificationCode;
	}

	/**
	 * @param serviceRequestClassificationCode
	 *           the serviceRequestClassificationCode to set
	 */
	public void setServiceRequestClassificationCode(String serviceRequestClassificationCode)
	{
		this.serviceRequestClassificationCode = serviceRequestClassificationCode;
	}

	/**
	 * @return the salesOrganisationID
	 */
	public String getSalesOrganisationID()
	{
		return salesOrganisationID;
	}

	/**
	 * @param salesOrganisationID
	 *           the salesOrganisationID to set
	 */
	public void setSalesOrganisationID(String salesOrganisationID)
	{
		this.salesOrganisationID = salesOrganisationID;
	}

	/**
	 * @return the salesUnitPartyName
	 */
	public String getSalesUnitPartyName()
	{
		return salesUnitPartyName;
	}

	/**
	 * @param salesUnitPartyName
	 *           the salesUnitPartyName to set
	 */
	public void setSalesUnitPartyName(String salesUnitPartyName)
	{
		this.salesUnitPartyName = salesUnitPartyName;
	}

	/**
	 * @return the requestedTotalProcessingDuration
	 */
	public String getRequestedTotalProcessingDuration()
	{
		return requestedTotalProcessingDuration;
	}

	/**
	 * @param requestedTotalProcessingDuration
	 *           the requestedTotalProcessingDuration to set
	 */
	public void setRequestedTotalProcessingDuration(String requestedTotalProcessingDuration)
	{
		this.requestedTotalProcessingDuration = requestedTotalProcessingDuration;
	}

	/**
	 * @return the resolutionDuetimeZoneCode
	 */
	public String getResolutionDuetimeZoneCode()
	{
		return resolutionDuetimeZoneCode;
	}

	/**
	 * @param resolutionDuetimeZoneCode
	 *           the resolutionDuetimeZoneCode to set
	 */
	public void setResolutionDuetimeZoneCode(String resolutionDuetimeZoneCode)
	{
		this.resolutionDuetimeZoneCode = resolutionDuetimeZoneCode;
	}

	/**
	 * @return the resolvedOntimeZoneCode
	 */
	public String getResolvedOntimeZoneCode()
	{
		return resolvedOntimeZoneCode;
	}

	/**
	 * @param resolvedOntimeZoneCode
	 *           the resolvedOntimeZoneCode to set
	 */
	public void setResolvedOntimeZoneCode(String resolvedOntimeZoneCode)
	{
		this.resolvedOntimeZoneCode = resolvedOntimeZoneCode;
	}

	/**
	 * @return the onSiteArrivalDuetimeZoneCode
	 */
	public String getOnSiteArrivalDuetimeZoneCode()
	{
		return onSiteArrivalDuetimeZoneCode;
	}

	/**
	 * @param onSiteArrivalDuetimeZoneCode
	 *           the onSiteArrivalDuetimeZoneCode to set
	 */
	public void setOnSiteArrivalDuetimeZoneCode(String onSiteArrivalDuetimeZoneCode)
	{
		this.onSiteArrivalDuetimeZoneCode = onSiteArrivalDuetimeZoneCode;
	}

	/**
	 * @return the serviceRequestUserLifeCycleStatusCodeText
	 */
	public String getServiceRequestUserLifeCycleStatusCodeText()
	{
		return serviceRequestUserLifeCycleStatusCodeText;
	}

	/**
	 * @param serviceRequestUserLifeCycleStatusCodeText
	 *           the serviceRequestUserLifeCycleStatusCodeText to set
	 */
	public void setServiceRequestUserLifeCycleStatusCodeText(String serviceRequestUserLifeCycleStatusCodeText)
	{
		this.serviceRequestUserLifeCycleStatusCodeText = serviceRequestUserLifeCycleStatusCodeText;
	}

	/**
	 * @return the installedBaseUUID
	 */
	public String getInstalledBaseUUID()
	{
		return installedBaseUUID;
	}

	/**
	 * @param installedBaseUUID
	 *           the installedBaseUUID to set
	 */
	public void setInstalledBaseUUID(String installedBaseUUID)
	{
		this.installedBaseUUID = installedBaseUUID;
	}

	/**
	 * @return the requestInProcessdatetimeContent
	 */
	public String getRequestInProcessdatetimeContent()
	{
		return requestInProcessdatetimeContent;
	}

	/**
	 * @param requestInProcessdatetimeContent
	 *           the requestInProcessdatetimeContent to set
	 */
	public void setRequestInProcessdatetimeContent(String requestInProcessdatetimeContent)
	{
		this.requestInProcessdatetimeContent = requestInProcessdatetimeContent;
	}

	/**
	 * @return the buyerPartyUUID
	 */
	public String getBuyerPartyUUID()
	{
		return buyerPartyUUID;
	}

	/**
	 * @param buyerPartyUUID
	 *           the buyerPartyUUID to set
	 */
	public void setBuyerPartyUUID(String buyerPartyUUID)
	{
		this.buyerPartyUUID = buyerPartyUUID;
	}

	/**
	 * @return the installedBaseDescription
	 */
	public String getInstalledBaseDescription()
	{
		return installedBaseDescription;
	}

	/**
	 * @param installedBaseDescription
	 *           the installedBaseDescription to set
	 */
	public void setInstalledBaseDescription(String installedBaseDescription)
	{
		this.installedBaseDescription = installedBaseDescription;
	}

	/**
	 * @return the resolvedOntimeZoneCodeText
	 */
	public String getResolvedOntimeZoneCodeText()
	{
		return resolvedOntimeZoneCodeText;
	}

	/**
	 * @param resolvedOntimeZoneCodeText
	 *           the resolvedOntimeZoneCodeText to set
	 */
	public void setResolvedOntimeZoneCodeText(String resolvedOntimeZoneCodeText)
	{
		this.resolvedOntimeZoneCodeText = resolvedOntimeZoneCodeText;
	}

	/**
	 * @return the salesUnitPartyUUID
	 */
	public String getSalesUnitPartyUUID()
	{
		return salesUnitPartyUUID;
	}

	/**
	 * @param salesUnitPartyUUID
	 *           the salesUnitPartyUUID to set
	 */
	public void setSalesUnitPartyUUID(String salesUnitPartyUUID)
	{
		this.salesUnitPartyUUID = salesUnitPartyUUID;
	}

	/**
	 * @return the requestedFulfillmentPeriodStartDateTime
	 */
	public String getRequestedFulfillmentPeriodStartDateTime()
	{
		return requestedFulfillmentPeriodStartDateTime;
	}

	/**
	 * @param requestedFulfillmentPeriodStartDateTime
	 *           the requestedFulfillmentPeriodStartDateTime to set
	 */
	public void setRequestedFulfillmentPeriodStartDateTime(String requestedFulfillmentPeriodStartDateTime)
	{
		this.requestedFulfillmentPeriodStartDateTime = requestedFulfillmentPeriodStartDateTime;
	}

	/**
	 * @return the responseByProcessorDateTimeZoneCodeText
	 */
	public String getResponseByProcessorDateTimeZoneCodeText()
	{
		return responseByProcessorDateTimeZoneCodeText;
	}

	/**
	 * @param responseByProcessorDateTimeZoneCodeText
	 *           the responseByProcessorDateTimeZoneCodeText to set
	 */
	public void setResponseByProcessorDateTimeZoneCodeText(String responseByProcessorDateTimeZoneCodeText)
	{
		this.responseByProcessorDateTimeZoneCodeText = responseByProcessorDateTimeZoneCodeText;
	}

	/**
	 * @return the requestInProcessdatetimeZoneCodeText
	 */
	public String getRequestInProcessdatetimeZoneCodeText()
	{
		return requestInProcessdatetimeZoneCodeText;
	}

	/**
	 * @param requestInProcessdatetimeZoneCodeText
	 *           the requestInProcessdatetimeZoneCodeText to set
	 */
	public void setRequestInProcessdatetimeZoneCodeText(String requestInProcessdatetimeZoneCodeText)
	{
		this.requestInProcessdatetimeZoneCodeText = requestInProcessdatetimeZoneCodeText;
	}

	/**
	 * @return the requestCloseddatetimeZoneCode
	 */
	public String getRequestCloseddatetimeZoneCode()
	{
		return requestCloseddatetimeZoneCode;
	}

	/**
	 * @param requestCloseddatetimeZoneCode
	 *           the requestCloseddatetimeZoneCode to set
	 */
	public void setRequestCloseddatetimeZoneCode(String requestCloseddatetimeZoneCode)
	{
		this.requestCloseddatetimeZoneCode = requestCloseddatetimeZoneCode;
	}

	/**
	 * @return the resolutionDueDateTime
	 */
	public String getResolutionDueDateTime()
	{
		return resolutionDueDateTime;
	}

	/**
	 * @param resolutionDueDateTime
	 *           the resolutionDueDateTime to set
	 */
	public void setResolutionDueDateTime(String resolutionDueDateTime)
	{
		this.resolutionDueDateTime = resolutionDueDateTime;
	}

	/**
	 * @return the escalationStatusCodeText
	 */
	public String getEscalationStatusCodeText()
	{
		return escalationStatusCodeText;
	}

	/**
	 * @param escalationStatusCodeText
	 *           the escalationStatusCodeText to set
	 */
	public void setEscalationStatusCodeText(String escalationStatusCodeText)
	{
		this.escalationStatusCodeText = escalationStatusCodeText;
	}

	/**
	 * @return the serviceSupportTeamPartyName
	 */
	public String getServiceSupportTeamPartyName()
	{
		return serviceSupportTeamPartyName;
	}

	/**
	 * @param serviceSupportTeamPartyName
	 *           the serviceSupportTeamPartyName to set
	 */
	public void setServiceSupportTeamPartyName(String serviceSupportTeamPartyName)
	{
		this.serviceSupportTeamPartyName = serviceSupportTeamPartyName;
	}

	/**
	 * @return the requestCloseddatetimeZoneCodeText
	 */
	public String getRequestCloseddatetimeZoneCodeText()
	{
		return requestCloseddatetimeZoneCodeText;
	}

	/**
	 * @param requestCloseddatetimeZoneCodeText
	 *           the requestCloseddatetimeZoneCodeText to set
	 */
	public void setRequestCloseddatetimeZoneCodeText(String requestCloseddatetimeZoneCodeText)
	{
		this.requestCloseddatetimeZoneCodeText = requestCloseddatetimeZoneCodeText;
	}

	/**
	 * @return the contractID
	 */
	public String getContractID()
	{
		return contractID;
	}

	/**
	 * @param contractID
	 *           the contractID to set
	 */
	public void setContractID(String contractID)
	{
		this.contractID = contractID;
	}

	/**
	 * @return the dataOriginTypeCodeText
	 */
	public String getDataOriginTypeCodeText()
	{
		return dataOriginTypeCodeText;
	}

	/**
	 * @param dataOriginTypeCodeText
	 *           the dataOriginTypeCodeText to set
	 */
	public void setDataOriginTypeCodeText(String dataOriginTypeCodeText)
	{
		this.dataOriginTypeCodeText = dataOriginTypeCodeText;
	}

	/**
	 * @return the buyerPartyName
	 */
	public String getBuyerPartyName()
	{
		return buyerPartyName;
	}

	/**
	 * @param buyerPartyName
	 *           the buyerPartyName to set
	 */
	public void setBuyerPartyName(String buyerPartyName)
	{
		this.buyerPartyName = buyerPartyName;
	}

	/**
	 * @return the mainTicketID
	 */
	public String getMainTicketID()
	{
		return mainTicketID;
	}

	/**
	 * @param mainTicketID
	 *           the mainTicketID to set
	 */
	public void setMainTicketID(String mainTicketID)
	{
		this.mainTicketID = mainTicketID;
	}

	/**
	 * @return the responseByProcessorDueDateTimeZoneCode
	 */
	public String getResponseByProcessorDueDateTimeZoneCode()
	{
		return responseByProcessorDueDateTimeZoneCode;
	}

	/**
	 * @param responseByProcessorDueDateTimeZoneCode
	 *           the responseByProcessorDueDateTimeZoneCode to set
	 */
	public void setResponseByProcessorDueDateTimeZoneCode(String responseByProcessorDueDateTimeZoneCode)
	{
		this.responseByProcessorDueDateTimeZoneCode = responseByProcessorDueDateTimeZoneCode;
	}

	/**
	 * @return the servicePerformerPartyID
	 */
	public String getServicePerformerPartyID()
	{
		return servicePerformerPartyID;
	}

	/**
	 * @param servicePerformerPartyID
	 *           the servicePerformerPartyID to set
	 */
	public void setServicePerformerPartyID(String servicePerformerPartyID)
	{
		this.servicePerformerPartyID = servicePerformerPartyID;
	}

	/**
	 * @return the buyerMainContactPartyName
	 */
	public String getBuyerMainContactPartyName()
	{
		return buyerMainContactPartyName;
	}

	/**
	 * @param buyerMainContactPartyName
	 *           the buyerMainContactPartyName to set
	 */
	public void setBuyerMainContactPartyName(String buyerMainContactPartyName)
	{
		this.buyerMainContactPartyName = buyerMainContactPartyName;
	}

	/**
	 * @return the serviceLevelObjectiveID
	 */
	public String getServiceLevelObjectiveID()
	{
		return serviceLevelObjectiveID;
	}

	/**
	 * @param serviceLevelObjectiveID
	 *           the serviceLevelObjectiveID to set
	 */
	public void setServiceLevelObjectiveID(String serviceLevelObjectiveID)
	{
		this.serviceLevelObjectiveID = serviceLevelObjectiveID;
	}

	/**
	 * @return the salesUnitPartyID
	 */
	public String getSalesUnitPartyID()
	{
		return salesUnitPartyID;
	}

	/**
	 * @param salesUnitPartyID
	 *           the salesUnitPartyID to set
	 */
	public void setSalesUnitPartyID(String salesUnitPartyID)
	{
		this.salesUnitPartyID = salesUnitPartyID;
	}

	/**
	 * @return the incidentServiceIssueCategoryID
	 */
	public String getIncidentServiceIssueCategoryID()
	{
		return incidentServiceIssueCategoryID;
	}

	/**
	 * @param incidentServiceIssueCategoryID
	 *           the incidentServiceIssueCategoryID to set
	 */
	public void setIncidentServiceIssueCategoryID(String incidentServiceIssueCategoryID)
	{
		this.incidentServiceIssueCategoryID = incidentServiceIssueCategoryID;
	}

	/**
	 * @return the salesTerritoryID
	 */
	public String getSalesTerritoryID()
	{
		return salesTerritoryID;
	}

	/**
	 * @param salesTerritoryID
	 *           the salesTerritoryID to set
	 */
	public void setSalesTerritoryID(String salesTerritoryID)
	{
		this.salesTerritoryID = salesTerritoryID;
	}

	/**
	 * @return the onSiteArrivalDuetimeZoneCodeText
	 */
	public String getOnSiteArrivalDuetimeZoneCodeText()
	{
		return OnSiteArrivalDuetimeZoneCodeText;
	}

	/**
	 * @param onSiteArrivalDuetimeZoneCodeText
	 *           the onSiteArrivalDuetimeZoneCodeText to set
	 */
	public void setOnSiteArrivalDuetimeZoneCodeText(String onSiteArrivalDuetimeZoneCodeText)
	{
		OnSiteArrivalDuetimeZoneCodeText = onSiteArrivalDuetimeZoneCodeText;
	}

	/**
	 * @return the buyerMainContactPartyUUID
	 */
	public String getBuyerMainContactPartyUUID()
	{
		return buyerMainContactPartyUUID;
	}

	/**
	 * @param buyerMainContactPartyUUID
	 *           the buyerMainContactPartyUUID to set
	 */
	public void setBuyerMainContactPartyUUID(String buyerMainContactPartyUUID)
	{
		this.buyerMainContactPartyUUID = buyerMainContactPartyUUID;
	}

	/**
	 * @return the warrantyUUID
	 */
	public String getWarrantyUUID()
	{
		return warrantyUUID;
	}

	/**
	 * @param warrantyUUID
	 *           the warrantyUUID to set
	 */
	public void setWarrantyUUID(String warrantyUUID)
	{
		this.warrantyUUID = warrantyUUID;
	}

	/**
	 * @return the serviceLevelObjectiveName
	 */
	public String getServiceLevelObjectiveName()
	{
		return serviceLevelObjectiveName;
	}

	/**
	 * @param serviceLevelObjectiveName
	 *           the serviceLevelObjectiveName to set
	 */
	public void setServiceLevelObjectiveName(String serviceLevelObjectiveName)
	{
		this.serviceLevelObjectiveName = serviceLevelObjectiveName;
	}

	/**
	 * @return the installationPointUUID
	 */
	public String getInstallationPointUUID()
	{
		return installationPointUUID;
	}

	/**
	 * @param installationPointUUID
	 *           the installationPointUUID to set
	 */
	public void setInstallationPointUUID(String installationPointUUID)
	{
		this.installationPointUUID = installationPointUUID;
	}

	/**
	 * @return the productID
	 */
	public String getProductID()
	{
		return productID;
	}

	/**
	 * @param productID
	 *           the productID to set
	 */
	public void setProductID(String productID)
	{
		this.productID = productID;
	}

	/**
	 * @return the responseByRequesterDateTimeContent
	 */
	public String getResponseByRequesterDateTimeContent()
	{
		return responseByRequesterDateTimeContent;
	}

	/**
	 * @param responseByRequesterDateTimeContent
	 *           the responseByRequesterDateTimeContent to set
	 */
	public void setResponseByRequesterDateTimeContent(String responseByRequesterDateTimeContent)
	{
		this.responseByRequesterDateTimeContent = responseByRequesterDateTimeContent;
	}

	/**
	 * @return the creationDateTime
	 */
	public String getCreationDateTime()
	{
		return creationDateTime;
	}

	/**
	 * @param creationDateTime
	 *           the creationDateTime to set
	 */
	public void setCreationDateTime(String creationDateTime)
	{
		this.creationDateTime = creationDateTime;
	}

	/**
	 * @return the responseByProcessorDateTimeContent
	 */
	public String getResponseByProcessorDateTimeContent()
	{
		return responseByProcessorDateTimeContent;
	}

	/**
	 * @param responseByProcessorDateTimeContent
	 *           the responseByProcessorDateTimeContent to set
	 */
	public void setResponseByProcessorDateTimeContent(String responseByProcessorDateTimeContent)
	{
		this.responseByProcessorDateTimeContent = responseByProcessorDateTimeContent;
	}

	/**
	 * @return the reportedForPartyID
	 */
	public String getReportedForPartyID()
	{
		return reportedForPartyID;
	}

	/**
	 * @param reportedForPartyID
	 *           the reportedForPartyID to set
	 */
	public void setReportedForPartyID(String reportedForPartyID)
	{
		this.reportedForPartyID = reportedForPartyID;
	}

	/**
	 * @return the distributionChannelCode
	 */
	public String getDistributionChannelCode()
	{
		return distributionChannelCode;
	}

	/**
	 * @param distributionChannelCode
	 *           the distributionChannelCode to set
	 */
	public void setDistributionChannelCode(String distributionChannelCode)
	{
		this.distributionChannelCode = distributionChannelCode;
	}

	/**
	 * @return the dataOriginTypeCode
	 */
	public String getDataOriginTypeCode()
	{
		return dataOriginTypeCode;
	}

	/**
	 * @param dataOriginTypeCode
	 *           the dataOriginTypeCode to set
	 */
	public void setDataOriginTypeCode(String dataOriginTypeCode)
	{
		this.dataOriginTypeCode = dataOriginTypeCode;
	}

	/**
	 * @return the productDescription
	 */
	public String getProductDescription()
	{
		return productDescription;
	}

	/**
	 * @param productDescription
	 *           the productDescription to set
	 */
	public void setProductDescription(String productDescription)
	{
		this.productDescription = productDescription;
	}

	/**
	 * @return the responseByRequesterDateTimeZoneCodeText
	 */
	public String getResponseByRequesterDateTimeZoneCodeText()
	{
		return responseByRequesterDateTimeZoneCodeText;
	}

	/**
	 * @param responseByRequesterDateTimeZoneCodeText
	 *           the responseByRequesterDateTimeZoneCodeText to set
	 */
	public void setResponseByRequesterDateTimeZoneCodeText(String responseByRequesterDateTimeZoneCodeText)
	{
		this.responseByRequesterDateTimeZoneCodeText = responseByRequesterDateTimeZoneCodeText;
	}

	/**
	 * @return the serviceSupportTeamPartyUUID
	 */
	public String getServiceSupportTeamPartyUUID()
	{
		return serviceSupportTeamPartyUUID;
	}

	/**
	 * @param serviceSupportTeamPartyUUID
	 *           the serviceSupportTeamPartyUUID to set
	 */
	public void setServiceSupportTeamPartyUUID(String serviceSupportTeamPartyUUID)
	{
		this.serviceSupportTeamPartyUUID = serviceSupportTeamPartyUUID;
	}

}
