/**
 * 
 */
package com.jsl.core.customer.complaint.response.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author himanshu.sial
 *
 */

@XmlRootElement(name = "ServiceRequestCollection")
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomerComplaintReponseDto
{

	@XmlElement(name = "ServiceRequest")
	private CustomerComplaintServiceDto customerComplaintServiceDto;

	/**
	 * @return the customerComplaintServiceDto
	 */
	public CustomerComplaintServiceDto getCustomerComplaintServiceDto()
	{
		return customerComplaintServiceDto;
	}

	/**
	 * @param customerComplaintServiceDto the customerComplaintServiceDto to set
	 */
	public void setCustomerComplaintServiceDto(CustomerComplaintServiceDto customerComplaintServiceDto)
	{
		this.customerComplaintServiceDto = customerComplaintServiceDto;
	}

}
