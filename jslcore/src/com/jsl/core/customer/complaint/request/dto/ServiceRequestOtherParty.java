/**
 * 
 */
package com.jsl.core.customer.complaint.request.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author himanshu.sial
 *
 */
@XmlRootElement(name = "ServiceRequestOtherParty")
@XmlAccessorType(XmlAccessType.FIELD)
public class ServiceRequestOtherParty
{

	@XmlElement(name = "RoleCode")
	private String roleCode;
	@XmlElement(name = "PartyID")
	private String partyId;
	@XmlElement(name = "PartyTypeCode")
	private String partyTypeCode;
	
	/**
	 * @return the roleCode
	 */
	public String getRoleCode()
	{
		return roleCode;
	}
	/**
	 * @param roleCode the roleCode to set
	 */
	public void setRoleCode(String roleCode)
	{
		this.roleCode = roleCode;
	}
	/**
	 * @return the partyId
	 */
	public String getPartyId()
	{
		return partyId;
	}
	/**
	 * @param partyId the partyId to set
	 */
	public void setPartyId(String partyId)
	{
		this.partyId = partyId;
	}
	/**
	 * @return the partyTypeCode
	 */
	public String getPartyTypeCode()
	{
		return partyTypeCode;
	}
	/**
	 * @param partyTypeCode the partyTypeCode to set
	 */
	public void setPartyTypeCode(String partyTypeCode)
	{
		this.partyTypeCode = partyTypeCode;
	}

	
}
