/**
 * 
 */
package com.jsl.core.customer.complaint.request.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author himanshu.sial
 *
 */
@XmlRootElement(name = "ServiceRequestAttachmentFolder")
@XmlAccessorType(XmlAccessType.FIELD)
public class ServiceRequestAttachmentFolderP
{

	@XmlElement(name = "ServiceRequestAttachmentFolder")
	private List<ServiceRequestAttachmentFolderC> listServiceRequestAttachmentFolderC;

	/**
	 * @return the serviceRequestAttachmentFolderC
	 */
	public List<ServiceRequestAttachmentFolderC> getServiceRequestAttachmentFolderC()
	{
		return listServiceRequestAttachmentFolderC;
	}

	/**
	 * @param serviceRequestAttachmentFolderC the serviceRequestAttachmentFolderC to set
	 */
	public void setServiceRequestAttachmentFolderC(List<ServiceRequestAttachmentFolderC> listServiceRequestAttachmentFolderC)
	{
		this.listServiceRequestAttachmentFolderC = listServiceRequestAttachmentFolderC;
	}
}
