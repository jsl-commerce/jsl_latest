/**
 * 
 */
package com.jsl.core.customer.complaint.request.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author himanshu.sial
 *
 */
@XmlRootElement(name = "ServiceRequestAttachmentFolder")
@XmlAccessorType(XmlAccessType.FIELD)
public class ServiceRequestAttachmentFolderC
{

	@XmlElement(name = "Binary")
	private String binary;
	@XmlElement(name = "CategoryCode")
	private String categoryCode;
	@XmlElement(name = "MimeType")
	private String mimeType;
	@XmlElement(name = "Name")
	private String name;
	@XmlElement(name = "TypeCode")
	private String typeCode;

	/**
	 * @return the categoryCode
	 */
	public String getCategoryCode()
	{
		return categoryCode;
	}

	/**
	 * @param categoryCode
	 *           the categoryCode to set
	 */
	public void setCategoryCode(String categoryCode)
	{
		this.categoryCode = categoryCode;
	}

	/**
	 * @return the mimeType
	 */
	public String getMimeType()
	{
		return mimeType;
	}

	/**
	 * @param mimeType
	 *           the mimeType to set
	 */
	public void setMimeType(String mimeType)
	{
		this.mimeType = mimeType;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name
	 *           the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @return the typeCode
	 */
	public String getTypeCode()
	{
		return typeCode;
	}

	/**
	 * @param typeCode
	 *           the typeCode to set
	 */
	public void setTypeCode(String typeCode)
	{
		this.typeCode = typeCode;
	}

	/**
	 * @return the binary
	 */
	public String getBinary()
	{
		return binary;
	}

	/**
	 * @param binary the binary to set
	 */
	public void setBinary(String binary)
	{
		this.binary = binary;
	}
}
