/**
 *
 */
package com.jsl.core.customer.complaint.request.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author himanshu.sial
 *
 */
@XmlRootElement(name = "ServiceRequestTextCollection")
@XmlAccessorType(XmlAccessType.FIELD)
public class ServiceRequestTextCollectionC
{

	@XmlElement(name = "TypeCode")
	private String typeCode;

	@XmlElement(name = "LanguageCode")
	private String languageCode;

	@XmlElement(name = "Text")
	private String text;

	/**
	 * @return the typeCode
	 */
	public String getTypeCode()
	{
		return typeCode;
	}

	/**
	 * @param typeCode
	 *           the typeCode to set
	 */
	public void setTypeCode(final String typeCode)
	{
		this.typeCode = typeCode;
	}

	/**
	 * @return the languageCode
	 */
	public String getLanguageCode()
	{
		return languageCode;
	}

	/**
	 * @param languageCode
	 *           the languageCode to set
	 */
	public void setLanguageCode(final String languageCode)
	{
		this.languageCode = languageCode;
	}

	/**
	 * @return the text
	 */
	public String getText()
	{
		return text;
	}

	/**
	 * @param text
	 *           the text to set
	 */
	public void setText(final String text)
	{
		this.text = text;
	}

}
