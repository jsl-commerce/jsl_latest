/**
 * 
 */
package com.jsl.core.customer.complaint.request.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author himanshu.sial
 *
 */
@XmlRootElement(name = "ServiceRequestCollection")
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomerComplaintRequestDto
{
	@XmlElement(name = "ServiceRequest")
	private CustomerComplaintServiceRequestDto customerComplaintServiceRequestDto;

	/**
	 * @return the customerComplaintServiceRequestDto
	 */
	public CustomerComplaintServiceRequestDto getCustomerComplaintServiceRequestDto()
	{
		return customerComplaintServiceRequestDto;
	}

	/**
	 * @param customerComplaintServiceRequestDto
	 *           the customerComplaintServiceRequestDto to set
	 */
	public void setCustomerComplaintServiceRequestDto(CustomerComplaintServiceRequestDto customerComplaintServiceRequestDto)
	{
		this.customerComplaintServiceRequestDto = customerComplaintServiceRequestDto;
	}
}
