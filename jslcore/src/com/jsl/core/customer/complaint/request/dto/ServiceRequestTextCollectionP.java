/**
 *
 */
package com.jsl.core.customer.complaint.request.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author himanshu.sial
 *
 */
@XmlRootElement(name = "ServiceRequestTextCollection")
@XmlAccessorType(XmlAccessType.FIELD)
public class ServiceRequestTextCollectionP
{

	@XmlElement(name = "ServiceRequestTextCollection")
	private ServiceRequestTextCollectionC serviceRequestTextCollectionC;

	/**
	 * @return the serviceRequestTextCollectionC
	 */
	public ServiceRequestTextCollectionC getServiceRequestTextCollectionC()
	{
		return serviceRequestTextCollectionC;
	}

	/**
	 * @param serviceRequestTextCollectionC
	 *           the serviceRequestTextCollectionC to set
	 */
	public void setServiceRequestTextCollectionC(final ServiceRequestTextCollectionC serviceRequestTextCollectionC)
	{
		this.serviceRequestTextCollectionC = serviceRequestTextCollectionC;
	}

}
