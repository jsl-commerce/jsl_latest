/**
 * 
 */
package com.jsl.core.customer.complaint.request.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author himanshu.sial
 *
 */

@XmlRootElement(name = "ServiceRequestServiceReferenceObject")
@XmlAccessorType(XmlAccessType.FIELD)
public class ServiceRequestReferenceC
{

	@XmlElement(name = "ZExternalProductID1content_SDK")
	private String externalProductId;
	@XmlElement(name = "ZInvoiceNo1_SDK")
	private String invoiceNo;
	@XmlElement(name = "ZPlant1_SDK")
	private String plantSDK;
	@XmlElement(name = "ZPrimaryDefect1_SDK")
	private String primaryDefect;
	@XmlElement(name = "ZResponsibility1_SDK")
	private String responsibilitySDK;
	@XmlElement(name = "ZSONo1_SDK")
	private String soNo;
	@XmlElement(name = "ZSPriceGroup1_SDK")
	private String priceGroup;
	@XmlElement(name = "ZSalesOffice1_SDK")
	private String salesOffice;
	@XmlElement(name = "ZSettlementRemarks1_SDK")
	private String settlementRemarks;

	@XmlElement(name = "Finish1content_SDK")
	private String finish;
	@XmlElement(name = "Size1content_SDK")
	private String size;
	@XmlElement(name = "ZBatchNo1_SDK")
	private String batchNumber;
	@XmlElement(name = "ZCompliantWeight1content_SDK")
	private String complaintQuantity;
	@XmlElement(name = "ZGradeGroupScontent_SDK")
	private String gradeGroup;
	@XmlElement(name = "ZGradeScontent_SDK")
	private String grade;
	@XmlElement(name = "ZInvoiceDate1_SDK")
	private String invoiceDate;
	@XmlElement(name = "ZKAMIDcontent_SDK")
	private String kamId;
	@XmlElement(name = "ZKAMName_SDK")
	private String kamName;
	@XmlElement(name = "ZLengthScontent_SDK")
	private String length;
	@XmlElement(name = "ZSeriesScontent_SDK")
	private String series;
	@XmlElement(name = "ZStandardScontent_SDK")
	private String standard;
	@XmlElement(name = "ZSuppliedWeight1content_SDK")
	private String quantity;
	@XmlElement(name = "ZThicknessScontent_SDK")
	private String thickness;
	@XmlElement(name = "ZUsageScontent_SDK")
	private String usage;
	@XmlElement(name = "ZWidthScontent_SDK")
	private String width;
	@XmlElement(name = "ZEdgConScontent_SDK")
	private String edge;


	/**
	 * @return the externalProductId
	 */
	public String getExternalProductId()
	{
		return externalProductId;
	}

	/**
	 * @param externalProductId
	 *           the externalProductId to set
	 */
	public void setExternalProductId(String externalProductId)
	{
		this.externalProductId = externalProductId;
	}

	/**
	 * @return the invoiceNo
	 */
	public String getInvoiceNo()
	{
		return invoiceNo;
	}

	/**
	 * @param invoiceNo
	 *           the invoiceNo to set
	 */
	public void setInvoiceNo(String invoiceNo)
	{
		this.invoiceNo = invoiceNo;
	}

	/**
	 * @return the plantSDK
	 */
	public String getPlantSDK()
	{
		return plantSDK;
	}

	/**
	 * @param plantSDK
	 *           the plantSDK to set
	 */
	public void setPlantSDK(String plantSDK)
	{
		this.plantSDK = plantSDK;
	}

	/**
	 * @return the primaryDefect
	 */
	public String getPrimaryDefect()
	{
		return primaryDefect;
	}

	/**
	 * @param primaryDefect
	 *           the primaryDefect to set
	 */
	public void setPrimaryDefect(String primaryDefect)
	{
		this.primaryDefect = primaryDefect;
	}

	/**
	 * @return the responsibilitySDK
	 */
	public String getResponsibilitySDK()
	{
		return responsibilitySDK;
	}

	/**
	 * @param responsibilitySDK
	 *           the responsibilitySDK to set
	 */
	public void setResponsibilitySDK(String responsibilitySDK)
	{
		this.responsibilitySDK = responsibilitySDK;
	}

	/**
	 * @return the soNo
	 */
	public String getSoNo()
	{
		return soNo;
	}

	/**
	 * @param soNo
	 *           the soNo to set
	 */
	public void setSoNo(String soNo)
	{
		this.soNo = soNo;
	}

	/**
	 * @return the priceGroup
	 */
	public String getPriceGroup()
	{
		return priceGroup;
	}

	/**
	 * @param priceGroup
	 *           the priceGroup to set
	 */
	public void setPriceGroup(String priceGroup)
	{
		this.priceGroup = priceGroup;
	}

	/**
	 * @return the salesOffice
	 */
	public String getSalesOffice()
	{
		return salesOffice;
	}

	/**
	 * @param salesOffice
	 *           the salesOffice to set
	 */
	public void setSalesOffice(String salesOffice)
	{
		this.salesOffice = salesOffice;
	}

	/**
	 * @return the settlementRemarks
	 */
	public String getSettlementRemarks()
	{
		return settlementRemarks;
	}

	/**
	 * @param settlementRemarks
	 *           the settlementRemarks to set
	 */
	public void setSettlementRemarks(String settlementRemarks)
	{
		this.settlementRemarks = settlementRemarks;
	}

	/**
	 * @return the finish
	 */
	public String getFinish()
	{
		return finish;
	}

	/**
	 * @param finish
	 *           the finish to set
	 */
	public void setFinish(String finish)
	{
		this.finish = finish;
	}

	/**
	 * @return the size
	 */
	public String getSize()
	{
		return size;
	}

	/**
	 * @param size
	 *           the size to set
	 */
	public void setSize(String size)
	{
		this.size = size;
	}

	/**
	 * @return the batchNumber
	 */
	public String getBatchNumber()
	{
		return batchNumber;
	}

	/**
	 * @param batchNumber
	 *           the batchNumber to set
	 */
	public void setBatchNumber(String batchNumber)
	{
		this.batchNumber = batchNumber;
	}

	/**
	 * @return the complaintQuantity
	 */
	public String getComplaintQuantity()
	{
		return complaintQuantity;
	}

	/**
	 * @param complaintQuantity
	 *           the complaintQuantity to set
	 */
	public void setComplaintQuantity(String complaintQuantity)
	{
		this.complaintQuantity = complaintQuantity;
	}

	/**
	 * @return the gradeGroup
	 */
	public String getGradeGroup()
	{
		return gradeGroup;
	}

	/**
	 * @param gradeGroup
	 *           the gradeGroup to set
	 */
	public void setGradeGroup(String gradeGroup)
	{
		this.gradeGroup = gradeGroup;
	}

	/**
	 * @return the grade
	 */
	public String getGrade()
	{
		return grade;
	}

	/**
	 * @param grade
	 *           the grade to set
	 */
	public void setGrade(String grade)
	{
		this.grade = grade;
	}

	/**
	 * @return the invoiceDate
	 */
	public String getInvoiceDate()
	{
		return invoiceDate;
	}

	/**
	 * @param invoiceDate
	 *           the invoiceDate to set
	 */
	public void setInvoiceDate(String invoiceDate)
	{
		this.invoiceDate = invoiceDate;
	}

	/**
	 * @return the kamId
	 */
	public String getKamId()
	{
		return kamId;
	}

	/**
	 * @param kamId
	 *           the kamId to set
	 */
	public void setKamId(String kamId)
	{
		this.kamId = kamId;
	}

	/**
	 * @return the kamName
	 */
	public String getKamName()
	{
		return kamName;
	}

	/**
	 * @param kamName
	 *           the kamName to set
	 */
	public void setKamName(String kamName)
	{
		this.kamName = kamName;
	}

	/**
	 * @return the length
	 */
	public String getLength()
	{
		return length;
	}

	/**
	 * @param length
	 *           the length to set
	 */
	public void setLength(String length)
	{
		this.length = length;
	}

	/**
	 * @return the series
	 */
	public String getSeries()
	{
		return series;
	}

	/**
	 * @param series
	 *           the series to set
	 */
	public void setSeries(String series)
	{
		this.series = series;
	}

	/**
	 * @return the standard
	 */
	public String getStandard()
	{
		return standard;
	}

	/**
	 * @param standard
	 *           the standard to set
	 */
	public void setStandard(String standard)
	{
		this.standard = standard;
	}

	/**
	 * @return the quantity
	 */
	public String getQuantity()
	{
		return quantity;
	}

	/**
	 * @param quantity
	 *           the quantity to set
	 */
	public void setQuantity(String quantity)
	{
		this.quantity = quantity;
	}

	/**
	 * @return the thickness
	 */
	public String getThickness()
	{
		return thickness;
	}

	/**
	 * @param thickness
	 *           the thickness to set
	 */
	public void setThickness(String thickness)
	{
		this.thickness = thickness;
	}

	/**
	 * @return the usage
	 */
	public String getUsage()
	{
		return usage;
	}

	/**
	 * @param usage
	 *           the usage to set
	 */
	public void setUsage(String usage)
	{
		this.usage = usage;
	}

	/**
	 * @return the width
	 */
	public String getWidth()
	{
		return width;
	}

	/**
	 * @param width
	 *           the width to set
	 */
	public void setWidth(String width)
	{
		this.width = width;
	}

	/**
	 * @return the edge
	 */
	public String getEdge()
	{
		return edge;
	}

	/**
	 * @param edge the edge to set
	 */
	public void setEdge(String edge)
	{
		this.edge = edge;
	}
}
