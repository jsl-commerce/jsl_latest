/**
 *
 */
package com.jsl.core.customer.complaint.request.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author himanshu.sial
 *
 */

@XmlRootElement(name = "ServiceRequest")
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomerComplaintServiceRequestDto
{

	@XmlElement(name = "ID")
	private String complaintId;
	@XmlElement(name = "ZSettlementRemarks3_SDK")
	private String zSettlementRemarks3_SDK;
	@XmlElement(name = "ZCustomerRemark_SDK")
	private String zCustomerRemark_SDK;
	@XmlElement(name = "BuyerPartyID")
	private String buyerPartyID;
	@XmlElement(name = "Name")
	private String name;
	@XmlElement(name = "ProcessingTypeCode")
	private String processingTypeCode;
	@XmlElement(name = "ZCAPA_Remarks_SDK")
	private String remarksSDK;
	@XmlElement(name = "ZContact_No_SDK")
	private String contactSDK;
	@XmlElement(name = "ZMaterial_Location_SDK")
	private String materialLocation;
	@XmlElement(name = "ZReported_By_SDK")
	private String reportedBy;
	@XmlElement(name = "ZExternal_Account_ID1_SDK")
	private String customerId;
	@XmlElement(name = "ZSalesDistrict1content_SDK")
	private String salesDistrict;
	@XmlElement(name = "ZSalesGroupID_SDK")
	private String salesGroup;
	@XmlElement(name = "ZSalesOfficeID_SDK")
	private String salesOffice;
	@XmlElement(name = "ZSalesOrg1_SDK")
	private String salesOrg;
	@XmlElement(name = "ZCompanyCode_SDK")
	private String companyCode;
	@XmlElement(name = "ServiceIssueCategoryID")
	private String natureOfComplaints;
	@XmlElement(name = "ZPresent_Material_Conditioncontent_SDK")
	private String presentMaterialConditions;
	@XmlElement(name = "ServiceRequestServiceReferenceObject")
	private ServiceRequestReferenceP serviceRequestReferenceObjectP;
	@XmlElement(name = "ServiceRequestAttachmentFolder")
	private ServiceRequestAttachmentFolderP ServiceRequestAttachmentFolder;
	@XmlElement(name = "ServiceRequestOtherParty")
	private List<ServiceRequestOtherParty> serviceRequestOtherParty;
	@XmlElement(name = "ZKAMID1content_SDK")
	private String kamID;
	@XmlElement(name = "ServiceRequestTextCollection")
	private ServiceRequestTextCollectionP serviceRequestTextCollectionP;



	/**
	 * @return the zCustomerRemark_SDK
	 */
	public String getzCustomerRemark_SDK()
	{
		return zCustomerRemark_SDK;
	}

	/**
	 * @param zCustomerRemark_SDK
	 *           the zCustomerRemark_SDK to set
	 */
	public void setzCustomerRemark_SDK(final String zCustomerRemark_SDK)
	{
		this.zCustomerRemark_SDK = zCustomerRemark_SDK;
	}

	/**
	 * @return the buyerPartyID
	 */
	public String getBuyerPartyID()
	{
		return buyerPartyID;
	}

	/**
	 * @param buyerPartyID
	 *           the buyerPartyID to set
	 */
	public void setBuyerPartyID(final String buyerPartyID)
	{
		this.buyerPartyID = buyerPartyID;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name
	 *           the name to set
	 */
	public void setName(final String name)
	{
		this.name = name;
	}

	/**
	 * @return the processingTypeCode
	 */
	public String getProcessingTypeCode()
	{
		return processingTypeCode;
	}

	/**
	 * @param processingTypeCode
	 *           the processingTypeCode to set
	 */
	public void setProcessingTypeCode(final String processingTypeCode)
	{
		this.processingTypeCode = processingTypeCode;
	}

	/**
	 * @return the serviceRequestAttachmentFolder
	 */
	public ServiceRequestAttachmentFolderP getServiceRequestAttachmentFolder()
	{
		return ServiceRequestAttachmentFolder;
	}

	/**
	 * @param serviceRequestAttachmentFolder
	 *           the serviceRequestAttachmentFolder to set
	 */
	public void setServiceRequestAttachmentFolder(final ServiceRequestAttachmentFolderP serviceRequestAttachmentFolder)
	{
		ServiceRequestAttachmentFolder = serviceRequestAttachmentFolder;
	}

	/**
	 * @return the remarksSDK
	 */
	public String getRemarksSDK()
	{
		return remarksSDK;
	}

	/**
	 * @param remarksSDK
	 *           the remarksSDK to set
	 */
	public void setRemarksSDK(final String remarksSDK)
	{
		this.remarksSDK = remarksSDK;
	}

	/**
	 * @return the contactSDK
	 */
	public String getContactSDK()
	{
		return contactSDK;
	}

	/**
	 * @param contactSDK
	 *           the contactSDK to set
	 */
	public void setContactSDK(final String contactSDK)
	{
		this.contactSDK = contactSDK;
	}

	/**
	 * @return the materialLocation
	 */
	public String getMaterialLocation()
	{
		return materialLocation;
	}

	/**
	 * @param materialLocation
	 *           the materialLocation to set
	 */
	public void setMaterialLocation(final String materialLocation)
	{
		this.materialLocation = materialLocation;
	}

	/**
	 * @return the reportedBy
	 */
	public String getReportedBy()
	{
		return reportedBy;
	}

	/**
	 * @param reportedBy
	 *           the reportedBy to set
	 */
	public void setReportedBy(final String reportedBy)
	{
		this.reportedBy = reportedBy;
	}

	/**
	 * @return the customerId
	 */
	public String getCustomerId()
	{
		return customerId;
	}

	/**
	 * @param customerId
	 *           the customerId to set
	 */
	public void setCustomerId(final String customerId)
	{
		this.customerId = customerId;
	}

	/**
	 * @return the salesDistrict
	 */
	public String getSalesDistrict()
	{
		return salesDistrict;
	}

	/**
	 * @param salesDistrict
	 *           the salesDistrict to set
	 */
	public void setSalesDistrict(final String salesDistrict)
	{
		this.salesDistrict = salesDistrict;
	}

	/**
	 * @return the salesGroup
	 */
	public String getSalesGroup()
	{
		return salesGroup;
	}

	/**
	 * @param salesGroup
	 *           the salesGroup to set
	 */
	public void setSalesGroup(final String salesGroup)
	{
		this.salesGroup = salesGroup;
	}

	/**
	 * @return the salesOffice
	 */
	public String getSalesOffice()
	{
		return salesOffice;
	}

	/**
	 * @param salesOffice
	 *           the salesOffice to set
	 */
	public void setSalesOffice(final String salesOffice)
	{
		this.salesOffice = salesOffice;
	}

	/**
	 * @return the companyCode
	 */
	public String getCompanyCode()
	{
		return companyCode;
	}

	/**
	 * @param companyCode
	 *           the companyCode to set
	 */
	public void setCompanyCode(final String companyCode)
	{
		this.companyCode = companyCode;
	}

	/**
	 * @return the natureOfComplaints
	 */
	public String getNatureOfComplaints()
	{
		return natureOfComplaints;
	}

	/**
	 * @param natureOfComplaints
	 *           the natureOfComplaints to set
	 */
	public void setNatureOfComplaints(final String natureOfComplaints)
	{
		this.natureOfComplaints = natureOfComplaints;
	}

	/**
	 * @return the serviceRequestReferenceObjectP
	 */
	public ServiceRequestReferenceP getServiceRequestReferenceObjectP()
	{
		return serviceRequestReferenceObjectP;
	}

	/**
	 * @param serviceRequestReferenceObjectP
	 *           the serviceRequestReferenceObjectP to set
	 */
	public void setServiceRequestReferenceObjectP(final ServiceRequestReferenceP serviceRequestReferenceObjectP)
	{
		this.serviceRequestReferenceObjectP = serviceRequestReferenceObjectP;
	}

	/**
	 * @return the serviceRequestOtherParty
	 */
	public List<ServiceRequestOtherParty> getServiceRequestOtherParty()
	{
		return serviceRequestOtherParty;
	}

	/**
	 * @param serviceRequestOtherParty
	 *           the serviceRequestOtherParty to set
	 */
	public void setServiceRequestOtherParty(final List<ServiceRequestOtherParty> serviceRequestOtherParty)
	{
		this.serviceRequestOtherParty = serviceRequestOtherParty;
	}

	/**
	 * @return the presentMaterialConditions
	 */
	public String getPresentMaterialConditions()
	{
		return presentMaterialConditions;
	}

	/**
	 * @param presentMaterialConditions
	 *           the presentMaterialConditions to set
	 */
	public void setPresentMaterialConditions(final String presentMaterialConditions)
	{
		this.presentMaterialConditions = presentMaterialConditions;
	}

	/**
	 * @return the kamID
	 */
	public String getKamID()
	{
		return kamID;
	}

	/**
	 * @param kamID
	 *           the kamID to set
	 */
	public void setKamID(final String kamID)
	{
		this.kamID = kamID;
	}

	/**
	 * @return the salesOrg
	 */
	public String getSalesOrg()
	{
		return salesOrg;
	}

	/**
	 * @param salesOrg
	 *           the salesOrg to set
	 */
	public void setSalesOrg(final String salesOrg)
	{
		this.salesOrg = salesOrg;
	}

	/**
	 * @return the complaintId
	 */
	public String getComplaintId()
	{
		return complaintId;
	}

	/**
	 * @param complaintId
	 *           the complaintId to set
	 */
	public void setComplaintId(final String complaintId)
	{
		this.complaintId = complaintId;
	}

	/**
	 * @return the zSettlementRemarks3_SDK
	 */
	public String getzSettlementRemarks3_SDK()
	{
		return zSettlementRemarks3_SDK;
	}

	/**
	 * @param zSettlementRemarks3_SDK
	 *           the zSettlementRemarks3_SDK to set
	 */
	public void setzSettlementRemarks3_SDK(final String zSettlementRemarks3_SDK)
	{
		this.zSettlementRemarks3_SDK = zSettlementRemarks3_SDK;
	}

	/**
	 * @return the serviceRequestTextCollectionP
	 */
	public ServiceRequestTextCollectionP getServiceRequestTextCollectionP()
	{
		return serviceRequestTextCollectionP;
	}

	/**
	 * @param serviceRequestTextCollectionP
	 *           the serviceRequestTextCollectionP to set
	 */
	public void setServiceRequestTextCollectionP(final ServiceRequestTextCollectionP serviceRequestTextCollectionP)
	{
		this.serviceRequestTextCollectionP = serviceRequestTextCollectionP;
	}
}
