/**
 * 
 */
package com.jsl.core.customer.complaint.request.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author himanshu.sial
 *
 */
@XmlRootElement(name = "ServiceRequestServiceReferenceObject")
@XmlAccessorType(XmlAccessType.FIELD)
public class ServiceRequestReferenceP
{

	@XmlElement(name = "ServiceRequestServiceReferenceObject")
	private List<ServiceRequestReferenceC> serviceRequestReferenceCList;

	/**
	 * @return the serviceRequestReferenceCList
	 */
	public List<ServiceRequestReferenceC> getServiceRequestReferenceCList()
	{
		return serviceRequestReferenceCList;
	}

	/**
	 * @param serviceRequestReferenceCList the serviceRequestReferenceCList to set
	 */
	public void setServiceRequestReferenceCList(List<ServiceRequestReferenceC> serviceRequestReferenceCList)
	{
		this.serviceRequestReferenceCList = serviceRequestReferenceCList;
	}
}
