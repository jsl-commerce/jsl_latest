/**
 *
 */
package com.jsl.core.stock.strategies.impl;

import de.hybris.platform.order.CartService;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.warehousing.atp.strategy.impl.WarehousingWarehouseSelectionStrategy;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;


/**
 * @author manav.magoo
 *
 */
public class JslWarehouseSelectionStrategy extends WarehousingWarehouseSelectionStrategy
{

	@Autowired
	private CartService cartService;

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.commerceservices.stock.strategies.impl.DefaultWarehouseSelectionStrategy#
	 * getWarehousesForBaseStore (de.hybris.platform.store.BaseStoreModel)
	 */
	@Override
	public List<WarehouseModel> getWarehousesForBaseStore(final BaseStoreModel baseStore)
	{
		if (cartService.hasSessionCart())
		{
			final WarehouseModel cartWarehouse = cartService.getSessionCart().getWarehouse();
			if (baseStore.getWarehouses().contains(cartWarehouse))
			{
				return Collections.singletonList(cartWarehouse);
			}
		}
		return Collections.emptyList();
	}
}
