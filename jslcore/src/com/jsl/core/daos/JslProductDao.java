/**
 *
 */
package com.jsl.core.daos;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.daos.ProductDao;

import java.util.List;


/**
 * @author manav.magoo
 *
 */
public interface JslProductDao extends ProductDao
{

	public List<ProductModel> findAuctionProductsForIds(List<String> productsId, CatalogVersionModel catalogVersion);
}
