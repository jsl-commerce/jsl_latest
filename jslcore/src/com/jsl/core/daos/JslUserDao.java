/**
 *
 */
package com.jsl.core.daos;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.daos.UserDao;

import java.util.List;


/**
 * @author manav.magoo
 *
 */
public interface JslUserDao extends UserDao
{

	/**
	 * @return
	 */
	List<UserModel> getLiveUsers();

}
