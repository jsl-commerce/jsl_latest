/**
 *
 */
package com.jsl.core.daos;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.List;


/**
 * @author manav.magoo
 *
 */
public interface JslOrderDao
{

	List<OrderModel> getAuctionOrders(int auctionId);

	List<OrderModel> getOrders(String startDate, String endDate);

	List<OrderModel> getOrdersForTimeInterval(String timeInterval, String orderType, UserModel user);

	List<OrderModel> getOrderFromCode(final String orderCode);
}
