/**
 *
 */
package com.jsl.core.daos;

import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.List;

import com.jsl.core.model.AuctionEventModel;
import com.jsl.jslConfigurableBundleService.model.BidEventModel;
import com.jsl.jslConfigurableBundleService.model.ProxyBidRuleModel;


public interface JslBidEventDao
{
	public String getMaxBidPriceForBundle(BundleTemplateModel bundleTemplate);

	public List<BidEventModel> getMaxBidForCurrentUser(AuctionEventModel auctionEvent, UserModel user);

	public List<BidEventModel> getAllMaxBidPrice(AuctionEventModel auctionEvent);

	public ProxyBidRuleModel getProxyBidPriceForCurrentUser(BundleTemplateModel bundleTemplate, UserModel user);

	public List<ProxyBidRuleModel> getProxyBidPrice(BundleTemplateModel bundleTemplate);

	public List<ProxyBidRuleModel> getAllProxyBidForCurrentUser(AuctionEventModel auctionEvent, UserModel currentUser);

	public List<BidEventModel> getAllBidHistoryForAuction(List<AuctionEventModel> auctionEventList);

	public List<BidEventModel> getAllBidHistoryForLot(String auctionID, String bundleID);

	public List<BidEventModel> getMaxBidEventForProxyByBundle(BundleTemplateModel bundleTemplate);

	public List<BidEventModel> getLastBidEventForBundle(String bundleId);

	public List<BidEventModel> getLostAuctionLotsDetailsForCurrentuser(List<BundleTemplateModel> bundleTemplateList,
			UserModel user);

	public List<BidEventModel> getLostAuctionLotsDetailsForCurrentUserByDateRange(List<BundleTemplateModel> bundleTemplateList,
			UserModel user, String startdate, String endDate);

	public List<OrderModel> getOrdersForAuction(List<Integer> auctionIdList);

	public List<BidEventModel> getBundleOnWhichUserDidNotPlaceBid(List<BundleTemplateModel> bundleTemplateList);

	public List<BidEventModel> getBundleOnWhichUserDidNotPlaceBidByDateRange(List<BundleTemplateModel> bundleTemplateList,
			String startdate, String endDate);

	public List<BidEventModel> getLostAuctionLotsDetailsForCurrentuserForCurrentDate(List<BundleTemplateModel> bundleTemplateList,
			UserModel user);

}
