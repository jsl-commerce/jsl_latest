/**
 *
 */
package com.jsl.core.daos;

import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;

import java.util.List;

import com.jsl.core.model.AuctionDeliveryChargeModel;


/**
 * @author manav.magoo
 *
 */
public interface JslAuctionDeliveryDao
{
	public double getDeliveryCharge(final String location, final String postalCode);

	public ZoneDeliveryModeModel getDeliveryMode(final String code);
	
	public List<AuctionDeliveryChargeModel> getFreightCharges(String plantLocation);
}
