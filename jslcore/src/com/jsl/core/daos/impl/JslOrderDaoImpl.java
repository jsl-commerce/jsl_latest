/**
 *
 */
package com.jsl.core.daos.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.jsl.core.daos.JslOrderDao;
import com.jsl.core.enums.JslCartType;


/**
 * @author manav.magoo
 *
 */
public class JslOrderDaoImpl extends AbstractItemDao implements JslOrderDao
{

	private static final String FIND_ORDERS_BY_CUSTOMER_CODE = "SELECT {" + OrderModel.PK + "} FROM {" + OrderModel._TYPECODE
			+ "} WHERE {" + OrderModel.CODE + "} = ?code AND {" + OrderModel.VERSIONID + "} IS NULL AND {" + OrderModel.USER
			+ "} = ?customer";
	@Autowired
	private SessionService sessionService;

	@Autowired
	private UserService userService;

	@Override
	public List<OrderModel> getAuctionOrders(final int auctionId)
	{
		final StringBuilder query = new StringBuilder();
		query.append("select {").append(OrderModel.PK).append("} from {").append(OrderModel._TYPECODE)
				.append("} where {auctionId} = ?auctionId");
		SearchResult<OrderModel> result = null;
		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query);
		searchQuery.addQueryParameter("auctionId", auctionId);
		result = getFlexibleSearchService().search(searchQuery);
		return result.getResult();
	}

	@Override
	public List<OrderModel> getOrders(final String startDate, final String endDate)
	{
		final StringBuilder query = new StringBuilder();

		query.append("select {").append(OrderModel.PK).append("} from {").append(OrderModel._TYPECODE).append(" as o JOIN ")
				.append(JslCartType._TYPECODE).append(" as ct ON {").append(OrderModel.TYPE).append("} = {ct.pk}} where {")
				.append("ct.code} = 'web' AND CAST({").append(OrderModel.DATE).append("} as DATE) between ?startDate AND ?endDate")
				.append(" ORDER BY {o.").append(OrderModel.DATE).append("},{o.").append(OrderModel.CODE).append("},{o.")
				.append(OrderModel.PURCHASEORDERNUMBER).append("}");

		SearchResult<OrderModel> result = null;
		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
		searchQuery.addQueryParameter("startDate", startDate);
		searchQuery.addQueryParameter("endDate", endDate);
		result = sessionService.executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public Object execute()
			{
				return getFlexibleSearchService().search(searchQuery);
			}
		}, userService.getAdminUser());
		return result.getResult();
	}


	@Override
	public List<OrderModel> getOrderFromCode(final String orderCode)
	{

		SearchResult<OrderModel> result = null;
		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(FIND_ORDERS_BY_CUSTOMER_CODE);
		searchQuery.addQueryParameter("code", orderCode);
		searchQuery.addQueryParameter("customer", userService.getCurrentUser());
		result = sessionService.executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public Object execute()
			{
				return getFlexibleSearchService().search(searchQuery);
			}
		}, userService.getAdminUser());
		return result.getResult();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.jsl.core.daos.JslOrderDao#getOrdersForTimeInterval(java.lang.String, java.lang.String)
	 */
	@Override
	public List<OrderModel> getOrdersForTimeInterval(final String timeInterval, final String orderType, final UserModel user)
	{
		// XXX Auto-generated method stub
		final StringBuilder query = new StringBuilder();

		query.append("select {").append(OrderModel.PK).append("} from {").append(OrderModel._TYPECODE).append(" as o JOIN ")
				.append(JslCartType._TYPECODE).append(" as ct ON {o.").append(OrderModel.TYPE)
				.append("} = {ct.pk}} where {ct.code} = ?orderType AND {o.").append(OrderModel.DATE)
				.append("} > DATEADD(HOUR, -?timeInterval, getdate()) AND {o.").append(OrderModel.USER).append("} = ?user");

		SearchResult<OrderModel> result = null;
		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
		searchQuery.addQueryParameter("timeInterval", Integer.parseInt(timeInterval));
		searchQuery.addQueryParameter("orderType", orderType);
		searchQuery.addQueryParameter("user", user);

		result = sessionService.executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public Object execute()
			{
				return getFlexibleSearchService().search(searchQuery);
			}
		}, userService.getAdminUser());
		return result.getResult();
	}

}
