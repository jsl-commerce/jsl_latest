package com.jsl.core.daos.impl;

import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;

import com.jsl.core.daos.JslBidEventDao;
import com.jsl.core.enums.AuctionStatus;
import com.jsl.core.model.AuctionEventModel;
import com.jsl.jslConfigurableBundleService.model.BidEventModel;
import com.jsl.jslConfigurableBundleService.model.ProxyBidRuleModel;


public class JslBidEventDaoImpl implements JslBidEventDao
{
	private static final Logger LOG = Logger.getLogger(JslBidEventDaoImpl.class);

	private FlexibleSearchService flexibleSearchService;

	private static final String AUCTION_EVENT = "auctionEvent";
	private static final String BUNDLE_TEMPLATE = "bundleTemplate";
	private static final String CREATED_BY = "createdBy";

	private static final String GET_MAX_BID_PRICE = "SELECT MAX({b." + BidEventModel.BIDPRICE + "}) " + "FROM {"
			+ BidEventModel._TYPECODE + " as b} " + "where {b." + BidEventModel.BUNDLETEMPLATE + "} = ?bundleTemplate AND {b."
			+ BidEventModel.AUCTIONEVENT + "}= ?auctionEvent";

	private static final String GET_MAX_BID_PRICE_OF_USER = "SELECT {a." + BidEventModel.PK + "} FROM {" + BidEventModel._TYPECODE
			+ " as a} " + " WHERE {a." + BidEventModel.BIDPRICE + "} = ({{" + "SELECT Max({" + BidEventModel.BIDPRICE + "}) FROM {"
			+ BidEventModel._TYPECODE + " as b} " + "WHERE {" + BidEventModel.AUCTIONEVENT
			+ "}= ?auctionEvent AND {a.bundletemplate} = {b.bundletemplate} " + "GROUP BY {" + BidEventModel.BUNDLETEMPLATE
			+ "}}}) AND {a." + BidEventModel.CREATEDBY + "} =?createdBy";

	private static final String GET_LIST_MAX_BID_PRICE = "SELECT {a." + BidEventModel.PK + "} FROM {" + BidEventModel._TYPECODE
			+ " as a} " + " WHERE {a." + BidEventModel.BIDPRICE + "} = ({{" + "SELECT Max({" + BidEventModel.BIDPRICE + "}) FROM {"
			+ BidEventModel._TYPECODE + " as b} " + "WHERE {" + BidEventModel.AUCTIONEVENT
			+ "}= ?auctionEvent AND {a.bundletemplate} = {b.bundletemplate} " + "GROUP BY {" + BidEventModel.BUNDLETEMPLATE
			+ "}}}) ORDER BY {a.bundleTemplate}, {a.isProxyBid} desc";

	private static final String GET_PROXY_BID_PRICE_FOR_CURRENT_USER = "SELECT {p." + ProxyBidRuleModel.PK + "} " + "FROM {"
			+ ProxyBidRuleModel._TYPECODE + " as p} " + "where  {p." + ProxyBidRuleModel.AUCTIONEVENT + "}= ?auctionEvent "
			+ "AND {p." + ProxyBidRuleModel.BUNDLETEMPLATE + "} =?bundletemplate AND {p." + ProxyBidRuleModel.CREATEDBY
			+ "} =?createdBy";

	private static final String GET_PROXY_BID_PRICE = "SELECT {p." + ProxyBidRuleModel.PK + "} " + "FROM {"
			+ ProxyBidRuleModel._TYPECODE + " as p} " + "where  {p." + ProxyBidRuleModel.AUCTIONEVENT + "}= ?auctionEvent "
			+ "AND {p." + ProxyBidRuleModel.BUNDLETEMPLATE + "} =?bundletemplate";

	private static final String GET_ALL_PROXY_BID_PRICE_FOR_CURRENT_USER = "SELECT {p." + ProxyBidRuleModel.PK + "} " + "FROM {"
			+ ProxyBidRuleModel._TYPECODE + " as p} " + "where  {p." + ProxyBidRuleModel.AUCTIONEVENT + "}= ?auctionEvent "
			+ "AND {p." + ProxyBidRuleModel.CREATEDBY + "} =?createdBy";

	private static final String GET_ALL_BID_DATA_FOR_AUCTION = "SELECT {b." + BidEventModel.PK + "} " + "FROM {"
			+ BidEventModel._TYPECODE + " as b JOIN AuctionEvent as a ON {a.pk} = {b.auctionEvent} }"
			+ "where  {a.auctionId} = ?auctionEvent";

	private static final String GET_ALL_BID_DATA_FOR_LOT = "SELECT {b." + BidEventModel.PK + "} " + "FROM {"
			+ BidEventModel._TYPECODE + " as b JOIN AuctionEvent as a ON {a.pk} = {b.auctionEvent} "
			+ "JOIN BundleTemplate as bt ON {b.bundleTemplate} = {bt.pk}}"
			+ " where {a.auctionId} = ?auctionEvent AND {bt.id} = ?bundleTemplate ORDER BY {" + BidEventModel.BIDPRICE
			+ "}, {b.isProxyBid} desc";

	private static final String GET_MAX_BID_EVENT_FOR_PROXY = "SELECT {a." + BidEventModel.PK + "} FROM {"
			+ BidEventModel._TYPECODE + " as a} " + " WHERE {a." + BidEventModel.BIDPRICE + "} = ({{" + "SELECT Max({"
			+ BidEventModel.BIDPRICE + "}) FROM {" + BidEventModel._TYPECODE + " as b} " + "WHERE {" + BidEventModel.AUCTIONEVENT
			+ "}= ?auctionEvent AND {bundleTemplate}= ?bundleTemplate}}) "
			+ "AND {a.auctionEvent}= ?auctionEvent AND {a.bundleTemplate}=?bundleTemplate ORDER BY {a." + BidEventModel.BIDPRICE
			+ "}, {a.isProxyBid} desc";

	private static final String GET_ALL_BID_DATA_BY_AUCTION = "SELECT {b." + BidEventModel.PK + "} " + "FROM {"
			+ BidEventModel._TYPECODE + " as b} where  {b.auctionEvent} IN (?auctionEvent)";

	private static final String GET_LAST_BID_EVENT_FOR_LOT = "SELECT {a." + BidEventModel.PK + "} FROM {" + BidEventModel._TYPECODE
			+ " as a JOIN BundleTemplate as bt ON {a.bundleTemplate} = {bt.pk}} WHERE {bt.id} = ?bundleTemplate ORDER BY {"
			+ BidEventModel.CREATEDON + "} desc";

	private static final String GET_ALL_MAX_BID_PRICE_OF_USER = "SELECT {b." + BidEventModel.PK + "} FROM {"
			+ BidEventModel._TYPECODE + " as b JOIN AuctionEvent as a ON {a.pk} = {b.auctionEvent} "
			+ "JOIN auctionStatus as aes on {a.auctionStatus} = {aes.pk} AND {aes.code} = ?auctionStatus} WHERE {b."
			+ BidEventModel.BIDPRICE + "} = ({{" + "SELECT Max({"
			+ BidEventModel.BIDPRICE + "}) FROM {" + BidEventModel._TYPECODE + " as be} WHERE {b." + BidEventModel.BUNDLETEMPLATE
			+ "} = {be.bundleTemplate} AND {be." + BidEventModel.CREATEDBY
			+ "} = ?createdBy GROUP BY {" + BidEventModel.BUNDLETEMPLATE + "}}}) ";

	private static final String GET_ALL_MAX_BID_PRICE_OF_USER_BY_DATE_RANGE = "SELECT {b." + BidEventModel.PK + "} FROM {"
			+ BidEventModel._TYPECODE + " as b JOIN AuctionEvent as a ON {a.pk} = {b.auctionEvent} "
			+ "AND CAST({a.startdate} as DATE) = ?startdate "
			+ "JOIN auctionStatus as aes on {a.auctionStatus} = {aes.pk} AND {aes.code} = ?auctionStatus} WHERE {b."
			+ BidEventModel.BIDPRICE + "} = ({{" + "SELECT Max({" + BidEventModel.BIDPRICE + "}) FROM {" + BidEventModel._TYPECODE
			+ " as be} WHERE {b." + BidEventModel.BUNDLETEMPLATE + "} = {be.bundleTemplate} AND {be." + BidEventModel.CREATEDBY
			+ "} = ?createdBy GROUP BY {" + BidEventModel.BUNDLETEMPLATE + "}}}) ";

	private static final String GET_ORDERS_FROM_AUCTION_ID = "SELECT {" + OrderModel.PK + "} FROM {" + OrderModel._TYPECODE
			+ "} WHERE {" + OrderModel.VERSIONID + "} IS NULL AND {" + OrderModel.AUCTIONID + "} IN (?auctionIdList)";

	private static final String BUNDLE_TEMPLATE_LIST = " AND {b.bundletemplate} NOT IN (?bundletemplate) ORDER BY {b."
			+ BidEventModel.BUNDLETEMPLATE + "}";

	private static final String GET_ALL_MAX_BID_PRICE = "SELECT {b." + BidEventModel.PK + "} FROM {" + BidEventModel._TYPECODE
			+ " as b JOIN AuctionEvent as a ON {a.pk} = {b.auctionEvent} "
			+ "AND CAST({a.startdate} as DATE) = ?curDatetime "
			+ "JOIN auctionStatus as aes on {a.auctionStatus} = {aes.pk} AND {aes.code} = ?auctionStatus} WHERE {b."
			+ BidEventModel.BIDPRICE + "} = ({{" + "SELECT Max({" + BidEventModel.BIDPRICE + "}) FROM {" + BidEventModel._TYPECODE
			+ " as be} WHERE {b." + BidEventModel.BUNDLETEMPLATE + "} = {be.bundleTemplate} GROUP BY {"
			+ BidEventModel.BUNDLETEMPLATE + "}}}) ";

	private static final String GET_ALL_MAX_BID_PRICE_BY_DATE_RANGE = "SELECT {b." + BidEventModel.PK + "} FROM {"
			+ BidEventModel._TYPECODE + " as b JOIN AuctionEvent as a ON {a.pk} = {b.auctionEvent} "
			+ "AND CAST({a.startdate} as DATE) = ?startdate "
			+ "JOIN auctionStatus as aes on {a.auctionStatus} = {aes.pk} AND {aes.code} = ?auctionStatus} WHERE {b."
			+ BidEventModel.BIDPRICE + "} = ({{" + "SELECT Max({" + BidEventModel.BIDPRICE + "}) FROM {" + BidEventModel._TYPECODE
			+ " as be} WHERE {b." + BidEventModel.BUNDLETEMPLATE + "} = {be.bundleTemplate} GROUP BY {"
			+ BidEventModel.BUNDLETEMPLATE + "}}}) ";

	private static final String GET_ALL_MAX_BID_PRICE_OF_USER_FOR_CURRENTDATE = "SELECT {b." + BidEventModel.PK + "} FROM {"
			+ BidEventModel._TYPECODE + " as b JOIN AuctionEvent as a ON {a.pk} = {b.auctionEvent} "
			+ "JOIN auctionStatus as aes on {a.auctionStatus} = {aes.pk} AND {aes.code} = ?auctionStatus} WHERE {b."
			+ BidEventModel.BIDPRICE + "} = ({{" + "SELECT Max({" + BidEventModel.BIDPRICE + "}) FROM {" + BidEventModel._TYPECODE
			+ " as be} WHERE {b." + BidEventModel.BUNDLETEMPLATE + "} = {be.bundleTemplate} AND {be." + BidEventModel.CREATEDBY
			+ "} = ?createdBy GROUP BY {" + BidEventModel.BUNDLETEMPLATE + "}}}) AND CAST({a." + AuctionEventModel.STARTDATE
			+ " } as DATE) = ?curDateTime ";

	@Override
	public String getMaxBidPriceForBundle(final BundleTemplateModel bundleTemplate)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_MAX_BID_PRICE);
		query.setResultClassList(Collections.singletonList(String.class));
		query.addQueryParameter(BUNDLE_TEMPLATE, bundleTemplate);
		query.addQueryParameter(AUCTION_EVENT, bundleTemplate.getAuctionEvent());
		final SearchResult<String> result = getFlexibleSearchService().search(query);

		return result.getResult().get(0);
	}

	@Override
	public List<BidEventModel> getMaxBidForCurrentUser(final AuctionEventModel auctionEvent, final UserModel user)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_MAX_BID_PRICE_OF_USER);
		query.addQueryParameter(AUCTION_EVENT, auctionEvent);
		query.addQueryParameter(CREATED_BY, user);
		final SearchResult<BidEventModel> result = getFlexibleSearchService().search(query);

		return result.getResult();
	}

	@Override
	public List<BidEventModel> getAllMaxBidPrice(final AuctionEventModel auctionEvent)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_LIST_MAX_BID_PRICE);
		query.addQueryParameter(AUCTION_EVENT, auctionEvent);
		final SearchResult<BidEventModel> result = getFlexibleSearchService().search(query);

		return result.getResult();
	}

	@Override
	public ProxyBidRuleModel getProxyBidPriceForCurrentUser(final BundleTemplateModel bundleTemplate, final UserModel user)
	{
		try
		{
			final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_PROXY_BID_PRICE_FOR_CURRENT_USER);
			query.addQueryParameter(AUCTION_EVENT, bundleTemplate.getAuctionEvent());
			query.addQueryParameter(BUNDLE_TEMPLATE, bundleTemplate);
			query.addQueryParameter(CREATED_BY, user);

			return getFlexibleSearchService().searchUnique(query);
		}
		catch (final Exception e)
		{
			return null;
		}
	}

	@Override
	public List<ProxyBidRuleModel> getProxyBidPrice(final BundleTemplateModel bundleTemplate)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_PROXY_BID_PRICE);
		query.addQueryParameter(AUCTION_EVENT, bundleTemplate.getAuctionEvent());
		query.addQueryParameter(BUNDLE_TEMPLATE, bundleTemplate);
		query.setDisableCaching(true);
		final SearchResult<ProxyBidRuleModel> result = getFlexibleSearchService().search(query);

		return result.getResult();
	}

	@Override
	public List<ProxyBidRuleModel> getAllProxyBidForCurrentUser(final AuctionEventModel auctionEvent, final UserModel user)
	{
		try
		{
			final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ALL_PROXY_BID_PRICE_FOR_CURRENT_USER);
			query.addQueryParameter(AUCTION_EVENT, auctionEvent);
			query.addQueryParameter(CREATED_BY, user);
			query.setDisableCaching(true);
			final SearchResult<ProxyBidRuleModel> result = getFlexibleSearchService().search(query);

			return result.getResult();
		}
		catch (final Exception e)
		{
			return Collections.emptyList();
		}
	}

	@Override
	public List<BidEventModel> getAllBidHistoryForAuction(final List<AuctionEventModel> auctionEventList)
	{
		try
		{
			final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ALL_BID_DATA_BY_AUCTION);
			query.addQueryParameter(AUCTION_EVENT, auctionEventList);
			query.setDisableCaching(true);
			final SearchResult<BidEventModel> result = getFlexibleSearchService().search(query);

			return result.getResult();
		}
		catch (final Exception e)
		{
			return Collections.emptyList();
		}
	}

	@Override
	public List<BidEventModel> getAllBidHistoryForLot(final String auctionID, final String bundleID)
	{
		try
		{
			final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ALL_BID_DATA_FOR_LOT);
			query.addQueryParameter(AUCTION_EVENT, auctionID);
			query.addQueryParameter(BUNDLE_TEMPLATE, bundleID);
			query.setDisableCaching(true);
			final SearchResult<BidEventModel> result = getFlexibleSearchService().search(query);

			return result.getResult();
		}
		catch (final Exception e)
		{
			return Collections.emptyList();
		}
	}

	@Override
	public List<BidEventModel> getMaxBidEventForProxyByBundle(final BundleTemplateModel bundleTemplate)
	{
		try
		{
			final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_MAX_BID_EVENT_FOR_PROXY);
			query.addQueryParameter(BUNDLE_TEMPLATE, bundleTemplate);
			query.addQueryParameter(AUCTION_EVENT, bundleTemplate.getAuctionEvent());
			final SearchResult<BidEventModel> result = getFlexibleSearchService().search(query);

			return result.getResult();

		}
		catch (final Exception e)
		{
			LOG.error("Exception while getMaxBidEventForProxyByBundle()", e);
			return Collections.emptyList();
		}
	}

	@Override
	public List<BidEventModel> getLastBidEventForBundle(final String bundleId)
	{
		try
		{
			final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_LAST_BID_EVENT_FOR_LOT);
			query.addQueryParameter(BUNDLE_TEMPLATE, bundleId);
			final SearchResult<BidEventModel> result = getFlexibleSearchService().search(query);

			return result.getResult();
		}
		catch (final Exception e)
		{
			LOG.error("Exception while getLastBidEventForBundle()", e);
			return Collections.emptyList();
		}
	}

	@Override
	public List<BidEventModel> getLostAuctionLotsDetailsForCurrentuserForCurrentDate(
			final List<BundleTemplateModel> bundleTemplateList,
			final UserModel user)
	{
		final Timestamp curDatetime = new Timestamp(System.currentTimeMillis());
		FlexibleSearchQuery query = null;
		if (bundleTemplateList != null && !bundleTemplateList.isEmpty())
		{
			query = new FlexibleSearchQuery(GET_ALL_MAX_BID_PRICE_OF_USER_FOR_CURRENTDATE + BUNDLE_TEMPLATE_LIST);
			query.addQueryParameter(BUNDLE_TEMPLATE, bundleTemplateList);
		}
		else
		{
			query = new FlexibleSearchQuery(GET_ALL_MAX_BID_PRICE_OF_USER_FOR_CURRENTDATE);
		}
		query.addQueryParameter(CREATED_BY, user);
		query.addQueryParameter("auctionStatus", AuctionStatus.COMPLETED.getCode());
		query.addQueryParameter("curDateTime", curDatetime.toString());

		final SearchResult<BidEventModel> result = getFlexibleSearchService().search(query);

		return result.getResult();
	}

	@Override
	public List<BidEventModel> getLostAuctionLotsDetailsForCurrentuser(final List<BundleTemplateModel> bundleTemplateList,
			final UserModel user)
	{
		FlexibleSearchQuery query = null;
		if (bundleTemplateList != null && !bundleTemplateList.isEmpty())
		{
			query = new FlexibleSearchQuery(GET_ALL_MAX_BID_PRICE_OF_USER + BUNDLE_TEMPLATE_LIST);
			query.addQueryParameter(BUNDLE_TEMPLATE, bundleTemplateList);
		}
		else
		{
			query = new FlexibleSearchQuery(GET_ALL_MAX_BID_PRICE_OF_USER);
		}
		query.addQueryParameter(CREATED_BY, user);
		query.addQueryParameter("auctionStatus", AuctionStatus.COMPLETED.getCode());

		final SearchResult<BidEventModel> result = getFlexibleSearchService().search(query);

		return result.getResult();
	}

	@Override
	public List<BidEventModel> getLostAuctionLotsDetailsForCurrentUserByDateRange(
			final List<BundleTemplateModel> bundleTemplateList, final UserModel user, final String startdate, final String endDate)
	{
		FlexibleSearchQuery query = null;

		if (bundleTemplateList != null && !bundleTemplateList.isEmpty())
		{
			query = new FlexibleSearchQuery(GET_ALL_MAX_BID_PRICE_OF_USER_BY_DATE_RANGE + BUNDLE_TEMPLATE_LIST);
			query.addQueryParameter(BUNDLE_TEMPLATE, bundleTemplateList);
		}
		else
		{
			query = new FlexibleSearchQuery(GET_ALL_MAX_BID_PRICE_OF_USER_BY_DATE_RANGE);
		}
		query.addQueryParameter(CREATED_BY, user);
		query.addQueryParameter("auctionStatus", AuctionStatus.COMPLETED.getCode());
		query.addQueryParameter("startdate", startdate);

		final SearchResult<BidEventModel> result = getFlexibleSearchService().search(query);

		return result.getResult();
	}

	@Override
	public List<OrderModel> getOrdersForAuction(final List<Integer> auctionIdList)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ORDERS_FROM_AUCTION_ID);
		query.addQueryParameter("auctionIdList", auctionIdList);
		final SearchResult<OrderModel> result = getFlexibleSearchService().search(query);

		return result.getResult();
	}

	@Override
	public List<BidEventModel> getBundleOnWhichUserDidNotPlaceBid(final List<BundleTemplateModel> bundleTemplateList)
	{
		FlexibleSearchQuery query = null;
		SearchResult<BidEventModel> result = null;
		List<BidEventModel> bidList = new ArrayList<>();
		final Timestamp curDatetime = new Timestamp(System.currentTimeMillis());
		if (!CollectionUtils.isEmpty(bundleTemplateList))
		{
			query = new FlexibleSearchQuery(GET_ALL_MAX_BID_PRICE + BUNDLE_TEMPLATE_LIST);
			query.addQueryParameter(BUNDLE_TEMPLATE, bundleTemplateList);
		}
		else
		{
			query = new FlexibleSearchQuery(GET_ALL_MAX_BID_PRICE);
		}
		query.addQueryParameter("auctionStatus", AuctionStatus.COMPLETED.getCode());
		query.addQueryParameter("curDatetime", curDatetime.toString());

		result = getFlexibleSearchService().search(query);
		bidList = result.getResult();

		return bidList;
	}

	@Override
	public List<BidEventModel> getBundleOnWhichUserDidNotPlaceBidByDateRange(final List<BundleTemplateModel> bundleTemplateList,
			final String startdate, final String endDate)
	{
		FlexibleSearchQuery query = null;
		SearchResult<BidEventModel> result = null;
		List<BidEventModel> bidList = new ArrayList<>();
		if (!CollectionUtils.isEmpty(bundleTemplateList))
		{
			query = new FlexibleSearchQuery(GET_ALL_MAX_BID_PRICE_BY_DATE_RANGE + BUNDLE_TEMPLATE_LIST);
			query.addQueryParameter(BUNDLE_TEMPLATE, bundleTemplateList);
		}
		else
		{
			query = new FlexibleSearchQuery(GET_ALL_MAX_BID_PRICE_BY_DATE_RANGE);
		}
		query.addQueryParameter("auctionStatus", AuctionStatus.COMPLETED.getCode());
		query.addQueryParameter("startdate", startdate);
		result = getFlexibleSearchService().search(query);
		bidList = result.getResult();

		return bidList;
	}

	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

}
