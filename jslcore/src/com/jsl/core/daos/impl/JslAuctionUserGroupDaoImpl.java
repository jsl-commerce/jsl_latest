/**
 *
 */
package com.jsl.core.daos.impl;

import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

import com.jsl.core.daos.JslAuctionUserGroupDao;
import com.jsl.core.model.AuctionUserGroupModel;


/**
 * @author manav.magoo
 *
 */
public class JslAuctionUserGroupDaoImpl extends AbstractItemDao implements JslAuctionUserGroupDao
{

	private static final String GET_AUCTIONUSERGROUP_BY_UID_QUERY = "SELECT {" + AuctionUserGroupModel.PK + "} FROM {"
			+ AuctionUserGroupModel._TYPECODE + "} where {" + AuctionUserGroupModel.UID + "}= ?uid";

	/*
	 * (non-Javadoc)
	 *
	 * @see com.jsl.core.daos.JslAuctionUserGroupDao#getAuctionUserGroup(java.lang.String)
	 */
	@Override
	public AuctionUserGroupModel getAuctionUserGroup(final String uid)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_AUCTIONUSERGROUP_BY_UID_QUERY);
		query.addQueryParameter("uid", uid);
		return getFlexibleSearchService().searchUnique(query);
	}

}
