/**
 *
 */
package com.jsl.core.daos.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.daos.impl.DefaultProductDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jsl.core.daos.JslProductDao;
import com.jsl.core.model.AuctionProductModel;


/**
 * @author manav.magoo
 *
 */
public class JslProductDaoImpl extends DefaultProductDao implements JslProductDao
{

	private static final String FIND_PRODUCTS_FOR_CODE = "select {" + AuctionProductModel.PK + "} from {"
			+ AuctionProductModel._TYPECODE + "} where {" + AuctionProductModel.CODE + "} IN (?productsId) and {"
			+ AuctionProductModel.CATALOGVERSION + "} = ?catalogVersion and {" + AuctionProductModel.BUNDLED + "} = ?bundled";

	/**
	 * @param typecode
	 */
	public JslProductDaoImpl(final String typecode)
	{
		super(typecode);
		// XXX Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.jsl.core.suggestion.dao.JslProductDao#findProductsForIds(java.util.List)
	 */
	@Override
	public List<ProductModel> findAuctionProductsForIds(final List<String> productsId, final CatalogVersionModel catalogVersion)
	{

		final Map params = new HashMap();
		params.put("productsId", productsId);
		params.put("catalogVersion", catalogVersion);
		params.put("bundled", Boolean.FALSE);

		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_PRODUCTS_FOR_CODE, params);


		final SearchResult<ProductModel> result = getFlexibleSearchService().search(query);

		return result.getResult();
	}


}
