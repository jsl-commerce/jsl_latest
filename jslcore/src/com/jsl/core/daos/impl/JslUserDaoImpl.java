/**
 *
 */
package com.jsl.core.daos.impl;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.user.daos.impl.DefaultUserDao;

import java.sql.Date;
import java.util.List;

import com.jsl.core.daos.JslUserDao;


/**
 * @author manav.magoo
 *
 */
public class JslUserDaoImpl extends DefaultUserDao implements JslUserDao
{

	private static final String GET_LIVE_USERS = "SELECT {" + UserModel.PK + "} FROM {" + UserModel._TYPECODE + "} where {"
			+ UserModel.LOGIN + "} = ?value AND CAST({" + UserModel.LASTLOGIN + "} as DATE) = ?curDate";

	@Override
	public List<UserModel> getLiveUsers()
	{
		SearchResult<UserModel> result = null;
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_LIVE_USERS);
		query.addQueryParameter("curDate", new Date(System.currentTimeMillis()).toString());
		query.addQueryParameter("value", Boolean.TRUE);
		result = getFlexibleSearchService().search(query);
		return result.getResult();
	}
}
