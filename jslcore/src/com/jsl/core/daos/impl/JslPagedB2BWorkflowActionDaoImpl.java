/**
 *
 */
package com.jsl.core.daos.impl;

import de.hybris.platform.b2bacceleratorservices.dao.impl.DefaultPagedB2BWorkflowActionDao;
import de.hybris.platform.commerceservices.search.flexiblesearch.data.SortQueryData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.workflow.enums.WorkflowActionStatus;
import de.hybris.platform.workflow.enums.WorkflowActionType;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import de.hybris.platform.workflow.model.WorkflowDecisionModel;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.util.StringUtils;

import com.jsl.core.daos.JslPagedB2BWorkflowActionDao;
import com.jsl.core.model.AuctionEventModel;


/**
 * @author himanshu.sial
 *
 */
public class JslPagedB2BWorkflowActionDaoImpl extends DefaultPagedB2BWorkflowActionDao implements JslPagedB2BWorkflowActionDao
{

	private static final String SORT_BY_DATE = " ORDER BY {wa: " + WorkflowActionModel.CREATIONTIME + "} DESC, {wa: "
			+ WorkflowActionModel.PK + "}";

	private static final String FIND_WORKFLOW_ACTIONS_BY_USER_QUERY = "SELECT  {" + WorkflowActionModel.PK + "} FROM {"
			+ WorkflowActionModel._TYPECODE + " AS wa";

	private static final String WORKFLOW_ACTION_TYPE = "} WHERE {wa:" + WorkflowActionModel.PRINCIPALASSIGNED
			+ "} = ?principalAssigned" + " AND {wa: " + WorkflowActionModel.ACTIONTYPE + " } IN (?actionType)";


	private static final String JOIN_WORKFLOW_DECISION_APPROVE_REJECT = " JOIN " + WorkflowDecisionModel._TYPECODE
			+ " AS wd ON {wa:selecteddecision}={wd:pk}";


	private static final String FILTER_DATE_RANGE = " AND CAST({wa: " + WorkflowActionModel.CREATIONTIME
			+ "} as DATE) between ?startdate AND ?endDate";
	private static final String FILTER_ORDER_STATUS = " AND {wa: " + WorkflowActionModel.STATUS + "} = ?orderStatus";

	private static final String FILTER_ORDER_STATUS_APPROVED_REJECTED = " AND {wd: " + WorkflowDecisionModel.QUALIFIER
			+ "} = ?qualifier";


	private static final String FILTER_AUCTION_ID = " AND {wa:" + WorkflowActionModel.WORKFLOW + " } in ({{select {"
			+ OrderModel.WORKFLOW + "} from {" + OrderModel._TYPECODE + " as o} where {o:" + OrderModel.AUCTIONID
			+ "} = ?auctionId }})";

	private static final String GET_ALL_AUCTION_IDS = "select  {pk} from {auctionevent} where {auctionId} in ({{select distinct({auctionId}) from {order} where {workflow} in ({{select distinct({workflow}) from {workflowaction}}}) and {auctionId} is not null}}) order by {auctionId}";

	private FlexibleSearchService flexibleSearchService;

	/**
	 * @param code
	 */
	public JslPagedB2BWorkflowActionDaoImpl(final String code)
	{
		super(code);
		// XXX Auto-generated constructor stub
	}

	@Override
	public List<AuctionEventModel> getAuctionIds()
	{
		SearchResult<AuctionEventModel> result = null;
		result = getFlexibleSearchService().search(GET_ALL_AUCTION_IDS);
		return result.getResult();
	}
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.jsl.core.daos.JslPagedB2BWorkflowActionDao#findFilteredPagedWorkflowActionsByUserAndActionTypes(de.hybris.
	 * platform.core.model.user.UserModel, de.hybris.platform.workflow.enums.WorkflowActionType[],
	 * de.hybris.platform.commerceservices.search.pagedata.PageableData, java.lang.String, java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public SearchPageData<WorkflowActionModel> findFilteredPagedWorkflowActionsByUserAndActionTypes(final UserModel user,
			final WorkflowActionType[] actionTypes, final PageableData pageableData, final String startDate, final String endDate,
			final String orderStatus, final String auctionId)
	{

		final Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put(WorkflowActionModel.PRINCIPALASSIGNED, user);
		queryParams.put(WorkflowActionModel.ACTIONTYPE, Arrays.asList(actionTypes));

		final List<SortQueryData> sortQueries = Arrays.asList(
				createSortQueryData("byDate", FIND_WORKFLOW_ACTIONS_BY_USER_QUERY
						+ getFilterQuery(queryParams, startDate, endDate, orderStatus)
						+ getAuctionIdFilteredQuery(auctionId, queryParams) + SORT_BY_DATE));

		return getPagedFlexibleSearchService().search(sortQueries, "byDate", queryParams, pageableData);
	}

	private String getFilterQuery(final Map<String, Object> queryParams, final String startDate, final String endDate,
			final String orderStatus)
	{

		String returnQuery = "";
		if (StringUtils.hasText(startDate) && StringUtils.hasText(endDate) && StringUtils.hasText(orderStatus))
		{
			queryParams.put("startDate", startDate);
			queryParams.put("endDate", endDate);

			returnQuery = getOrderStatusQuery(orderStatus, queryParams) + FILTER_DATE_RANGE;
		}
		else if (StringUtils.hasText(startDate) && StringUtils.hasText(endDate))
		{
			queryParams.put("startDate", startDate);
			queryParams.put("endDate", endDate);
			returnQuery = WORKFLOW_ACTION_TYPE + FILTER_DATE_RANGE;
		}
		else if (StringUtils.hasText(orderStatus))
		{
			queryParams.put("orderStatus", orderStatus);
			returnQuery = getOrderStatusQuery(orderStatus, queryParams);
		}

		if (StringUtils.hasText(returnQuery))
		{
			return returnQuery;
		}
		else
		{
			return WORKFLOW_ACTION_TYPE;
		}
	}

	private String getOrderStatusQuery(final String orderStatus, final Map<String, Object> queryParams)
	{
		if (orderStatus.equalsIgnoreCase("pending"))
		{
			queryParams.put("orderStatus", WorkflowActionStatus.IN_PROGRESS);
			return WORKFLOW_ACTION_TYPE + FILTER_ORDER_STATUS;
		}
		else if (orderStatus.equalsIgnoreCase("approved") || orderStatus.equalsIgnoreCase("rejected"))
		{
			queryParams.put("orderStatus", WorkflowActionStatus.COMPLETED);
			if (orderStatus.equalsIgnoreCase("rejected"))
			{
				queryParams.put("qualifier", "REJECT");
			}
			else
			{
				queryParams.put("qualifier", "APPROVE");
			}
			return JOIN_WORKFLOW_DECISION_APPROVE_REJECT + WORKFLOW_ACTION_TYPE + FILTER_ORDER_STATUS
					+ FILTER_ORDER_STATUS_APPROVED_REJECTED;
		}
		else
		{
			return WORKFLOW_ACTION_TYPE;
		}
	}

	private String getAuctionIdFilteredQuery(final String auctionId, final Map<String, Object> queryParams)
	{
		final String returnQuery = "";
		if (StringUtils.hasText(auctionId))
		{
			queryParams.put("auctionId", auctionId);
			return FILTER_AUCTION_ID;
		}
		else
		{
			return returnQuery;
		}
	}

	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	/**
	 * @param flexibleSearchService
	 *           the flexibleSearchService to set
	 */
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}


}
