/**
 *
 */
package com.jsl.core.daos.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.workflow.daos.impl.DefaultWorkflowTemplateDao;
import de.hybris.platform.workflow.model.WorkflowTemplateModel;

import java.util.List;


/**
 * @author himanshu.sial
 *
 */
public class JslWorkflowTemplateDao extends DefaultWorkflowTemplateDao
{

	/**
	 * @param typecode
	 */
	public JslWorkflowTemplateDao(final String typecode)
	{
		super(typecode);
		// XXX Auto-generated constructor stub
	}


	@Override
	public List<WorkflowTemplateModel> findWorkflowTemplatesByCode(final String code)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery("SELECT {pk} FROM {WorkflowTemplate} WHERE {code} = ?code");
		query.setDisableCaching(true);
		query.addQueryParameter("code", code);
		final SearchResult<WorkflowTemplateModel> res = this.getFlexibleSearchService().search(query);
		return res.getResult();
	}

}
