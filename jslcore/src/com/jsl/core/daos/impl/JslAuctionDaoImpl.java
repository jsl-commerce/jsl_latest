/**
 *
 */
package com.jsl.core.daos.impl;

import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.time.TimeService;

import java.sql.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.jsl.core.daos.JslAuctionDao;
import com.jsl.core.enums.AuctionStatus;
import com.jsl.core.model.AuctionEventModel;


public class JslAuctionDaoImpl extends AbstractItemDao implements JslAuctionDao
{

	private FlexibleSearchService flexibleSearchService;

	@Resource
	private TimeService timeService;

	private static final Logger LOG = Logger.getLogger(JslAuctionDaoImpl.class);

	private static final String GET_ALL_AUCTIONEVENT_LIST = "SELECT {" + AuctionEventModel.PK + "} FROM {"
			+ AuctionEventModel._TYPECODE + "} where {" + AuctionEventModel.ACTIVE + "} = ?active";

	private static final String GET_AUCTIONEVENT_BY_ID_QUERY = "SELECT {" + AuctionEventModel.PK + "} FROM {"
			+ AuctionEventModel._TYPECODE + "} where {" + AuctionEventModel.AUCTIONID + "}= ?auctionId";

	private static final String GET_ALL_UPCOMING_AUCTION_LIST_QUERY = "SELECT {ae.pk} FROM {AuctionEvent as ae JOIN AuctionStatus as aes ON {ae.auctionStatus}"
			+ " = {aes.pk}} where {aes.code} = ?auctionStatus AND CAST({ae.startDate} as DATE) > ?curDate";

	private static final String GET_ALL_STARTED_AUCTION_LIST_QUERY = "SELECT {ae.pk} FROM {AuctionEvent as ae JOIN AuctionStatus as aes ON {ae.auctionStatus}"
			+ " = {aes.pk}} where {aes.code} = ?auctionStatusStarted ORDER BY {ae.auctionid}";

	private static final String GET_ALL_AUCTION_LIST_QUERY = "SELECT {ae.pk} FROM {AuctionEvent as ae JOIN AuctionStatus as aes ON {ae.auctionStatus}"
			+ " = {aes.pk}} where {aes.code} IN ( ?auctionStatusStarted, ?auctionStatusClosed, ?auctionStatusCompleted) AND CAST({ae.startDate} as DATE) = ?curDate";

	private static final String GET_AUCTIONS_BY_STATUS = "SELECT {ae.pk} FROM {AuctionEvent as ae JOIN AuctionStatus as aes ON {ae.auctionStatus}"
			+ " = {aes.pk}} where {aes.code} = ?auctionStatus";

	private static final String GET_AUCTIONS_BY_DATE_RANGE = "select {pk} from {AuctionEvent}  where CAST({startdate} as DATE) between ?startdate AND ?endDate";

	private static final String GET_AUCTIONS_BY_DATE = "select {pk} from {AuctionEvent}  where CAST({startdate} as DATE) =?startdate";

	private static final String CUR_DATE = "curDate";

	@Override
	public List<AuctionEventModel> getActiveAuctionList()
	{
		List<AuctionEventModel> auctionEventList = null;
		SearchResult<AuctionEventModel> result = null;
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ALL_AUCTIONEVENT_LIST);
		query.addQueryParameter("active", Boolean.TRUE);
		result = getFlexibleSearchService().search(query);
		auctionEventList = result.getResult();
		return auctionEventList;
	}

	@Override
	public List<AuctionEventModel> getLiveAuctionList(final Date curDate)
	{
		SearchResult<AuctionEventModel> result = null;
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ALL_STARTED_AUCTION_LIST_QUERY);
		query.addQueryParameter("auctionStatusStarted", AuctionStatus.STARTED.getCode());
		result = getFlexibleSearchService().search(query);

		return result.getResult();
	}

	@Override
	public List<AuctionEventModel> getAllAuctionList(final Date curDate)
	{
		SearchResult<AuctionEventModel> result = null;
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ALL_AUCTION_LIST_QUERY);
		query.addQueryParameter("auctionStatusStarted", AuctionStatus.STARTED.getCode());
		query.addQueryParameter("auctionStatusClosed", AuctionStatus.CLOSED.getCode());
		query.addQueryParameter("auctionStatusCompleted", AuctionStatus.COMPLETED.getCode());
		query.addQueryParameter(CUR_DATE, curDate.toString());
		result = getFlexibleSearchService().search(query);

		return result.getResult();
	}

	@Override
	public List<AuctionEventModel> getUpcomingAuctionDetails(final Date curDate)
	{
		List<AuctionEventModel> auctionEventList = null;
		SearchResult<AuctionEventModel> result = null;
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ALL_UPCOMING_AUCTION_LIST_QUERY);
		query.addQueryParameter(CUR_DATE, curDate.toString());
		query.addQueryParameter("auctionStatus", AuctionStatus.STARTED.getCode());
		result = getFlexibleSearchService().search(query);
		auctionEventList = result.getResult();

		return auctionEventList;
	}

	@Override
	public AuctionEventModel getAuctionEventForAuctionId(final String auctionEventId)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_AUCTIONEVENT_BY_ID_QUERY);
		query.addQueryParameter("auctionId", auctionEventId);

		try
		{
			return getFlexibleSearchService().searchUnique(query);
		}
		catch (final ModelNotFoundException ex)
		{
			return null;
		}
	}

	@Override
	public List<AuctionEventModel> getAuctions(final AuctionStatus status)
	{
		SearchResult<AuctionEventModel> result = null;
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_AUCTIONS_BY_STATUS);
		query.addQueryParameter("auctionStatus", status.getCode());
		result = getFlexibleSearchService().search(query);
		return result.getResult();
	}

	@Override
	public List<AuctionEventModel> getAuctionByDateRange(final String auctionStartDate, final String auctionEndDate)
	{
		SearchResult<AuctionEventModel> result = null;
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_AUCTIONS_BY_DATE_RANGE);
		query.addQueryParameter("startdate", auctionStartDate);
		query.addQueryParameter("endDate", auctionEndDate);
		result = getFlexibleSearchService().search(query);

		return result.getResult();
	}

	@Override
	public List<AuctionEventModel> getAuctionByDate(final String auctionStartDate)
	{
		SearchResult<AuctionEventModel> result = null;
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_AUCTIONS_BY_DATE);
		query.addQueryParameter("startdate", auctionStartDate);
		result = getFlexibleSearchService().search(query);

		return result.getResult();
	}

	@Override
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	@Override
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

}
