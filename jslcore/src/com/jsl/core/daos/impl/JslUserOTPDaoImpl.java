/**
 *
 */
package com.jsl.core.daos.impl;

import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Date;
import java.util.List;

import com.jsl.core.constants.GeneratedJslCoreConstants.Enumerations.OTPStatus;
import com.jsl.core.daos.JslUserOTPDao;
import com.jsl.core.enums.OTPGeneratedForEnum;
import com.jsl.core.enums.OTPUserIdentificationType;
import com.jsl.core.model.JSLUserOTPGenerationModel;


/**
 * @author suyash.trivedi
 *
 */
public class JslUserOTPDaoImpl extends AbstractItemDao implements JslUserOTPDao
{
	private ModelService modelService;
	private FlexibleSearchService flexibleSearchService;

	@Override
	public void insertUserOTP(final OTPGeneratedForEnum generatedModule, final String otp, final Date generatedTime,
			final Date expirationTime, final OTPUserIdentificationType identificationType, final String identificationValue)
	{
		final JSLUserOTPGenerationModel jslUserOTPGenerationModel = getModelService().create(JSLUserOTPGenerationModel.class);
		jslUserOTPGenerationModel.setGeneratedFor(generatedModule);
		jslUserOTPGenerationModel.setIdentificationType(identificationType);
		jslUserOTPGenerationModel.setIdentificationValue(identificationValue);
		jslUserOTPGenerationModel.setOtpStatus(OTPStatus.PENDING.intern());
		populateOPTGenerationModel(otp, generatedTime, expirationTime, jslUserOTPGenerationModel);
		modelService.save(jslUserOTPGenerationModel);
	}

	@Override
	public void insertUserAuctionOTP(final OTPGeneratedForEnum generatedModule, final String otp, final Date generatedTime,
			final Date expirationTime, final OTPUserIdentificationType identificationType, final String identificationValue,
			final String auctionEventId)
	{
		final JSLUserOTPGenerationModel jslUserOTPGenerationModel = getModelService().create(JSLUserOTPGenerationModel.class);
		jslUserOTPGenerationModel.setGeneratedFor(generatedModule);
		jslUserOTPGenerationModel.setIdentificationType(identificationType);
		jslUserOTPGenerationModel.setIdentificationValue(identificationValue);
		jslUserOTPGenerationModel.setAuctionEventId(auctionEventId);
		jslUserOTPGenerationModel.setOtpStatus(OTPStatus.PENDING.intern());
		populateOPTGenerationModel(otp, generatedTime, expirationTime, jslUserOTPGenerationModel);
		modelService.save(jslUserOTPGenerationModel);
	}

	/**
	 * @param otp
	 * @param generatedTime
	 * @param expirationTime
	 * @param jslUserOTPGenerationModel
	 */
	private void populateOPTGenerationModel(final String otp, final Date generatedTime, final Date expirationTime,
			final JSLUserOTPGenerationModel jslUserOTPGenerationModel)
	{
		{
			jslUserOTPGenerationModel.setEncodedOTP(otp);
			jslUserOTPGenerationModel.setOtpGeneratedTime(generatedTime);
			jslUserOTPGenerationModel.setOtpExpirationTime(expirationTime);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.jsl.core.daos.JslUserOTPDao#getUserOTPGeneratedRecord(com.jsl.core.enums.OTPGeneratedForEnum,
	 * com.jsl.core.enums.OTPUserIdentificationType, java.lang.String)
	 */
	@Override
	public List<JSLUserOTPGenerationModel> getUserOTPGeneratedRecord(final OTPGeneratedForEnum generatedModule,
			final OTPUserIdentificationType identityType, final String identityValue)
	{

		final StringBuilder query = new StringBuilder();
		final String andOperator = " AND {";

		query.append("SELECT {").append(JSLUserOTPGenerationModel.PK).append('}');
		query.append(" FROM {").append(JSLUserOTPGenerationModel._TYPECODE).append('}');
		query.append(" WHERE {").append(JSLUserOTPGenerationModel.GENERATEDFOR).append("} = ?generatedModule");
		query.append(andOperator).append(JSLUserOTPGenerationModel.IDENTIFICATIONTYPE).append("} = ?identificationType");
		query.append(andOperator).append(JSLUserOTPGenerationModel.IDENTIFICATIONVALUE).append("} = ?identificationValue");
		query.append(andOperator).append(JSLUserOTPGenerationModel.DELETED).append("} = ?deleted");

		final FlexibleSearchQuery flexiQuery = new FlexibleSearchQuery(query);
		flexiQuery.addQueryParameter("generatedModule", generatedModule);
		flexiQuery.addQueryParameter("identificationType", identityType);
		flexiQuery.addQueryParameter("identificationValue", identityValue);
		flexiQuery.addQueryParameter("deleted", Boolean.FALSE);

		final SearchResult<JSLUserOTPGenerationModel> result = flexibleSearchService.search(flexiQuery);

		return result.getResult();
	}


	@Override
	public List<JSLUserOTPGenerationModel> getUserAuctionOTPGeneratedRecord(final OTPGeneratedForEnum generatedModule,
			final OTPUserIdentificationType identityType, final String identityValue, final String auctionEventId)
	{

		final StringBuilder query = new StringBuilder();
		final String andOperator = " AND {";

		query.append("SELECT {").append(JSLUserOTPGenerationModel.PK).append('}');
		query.append(" FROM {").append(JSLUserOTPGenerationModel._TYPECODE).append('}');
		query.append(" WHERE {").append(JSLUserOTPGenerationModel.GENERATEDFOR).append("} = ?generatedModule");
		query.append(andOperator).append(JSLUserOTPGenerationModel.IDENTIFICATIONTYPE).append("} = ?identificationType");
		query.append(andOperator).append(JSLUserOTPGenerationModel.IDENTIFICATIONVALUE).append("} = ?identificationValue");
		query.append(andOperator).append(JSLUserOTPGenerationModel.DELETED).append("} = ?deleted");
		query.append(andOperator).append(JSLUserOTPGenerationModel.AUCTIONEVENTID).append("} = ?auctionEventId");

		final FlexibleSearchQuery flexiQuery = new FlexibleSearchQuery(query);
		flexiQuery.addQueryParameter("generatedModule", generatedModule);
		flexiQuery.addQueryParameter("identificationType", identityType);
		flexiQuery.addQueryParameter("identificationValue", identityValue);
		flexiQuery.addQueryParameter("auctionEventId", auctionEventId);
		flexiQuery.addQueryParameter("deleted", Boolean.FALSE);

		final SearchResult<JSLUserOTPGenerationModel> result = flexibleSearchService.search(flexiQuery);

		return result.getResult();
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean expireUserOTPGeneratedRecord(final JSLUserOTPGenerationModel jslUserOTPGenerationModel)
	{
		jslUserOTPGenerationModel.setDeleted(Boolean.TRUE.booleanValue());
		modelService.save(jslUserOTPGenerationModel);
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean incrementUserOTPAttempts(final JSLUserOTPGenerationModel jslUserOTPGenerationModel)
	{
		jslUserOTPGenerationModel
				.setVerificationAttempts(Integer.valueOf(jslUserOTPGenerationModel.getVerificationAttempts().intValue() + 1));
		modelService.save(jslUserOTPGenerationModel);
		return true;
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean updateUserOTP(final JSLUserOTPGenerationModel jslUserOTPGenerationModel, final String otp,
			final Date generatedTime, final Date expirationTime)
	{
		populateOPTGenerationModel(otp, generatedTime, expirationTime, jslUserOTPGenerationModel);
		jslUserOTPGenerationModel.setVerificationAttempts(Integer.valueOf(0));
		modelService.save(jslUserOTPGenerationModel);
		return true;
	}

	@Override
	public boolean updateOTPStatus(final JSLUserOTPGenerationModel jslUserOTPGenerationModel, final String status)
	{
		jslUserOTPGenerationModel.setOtpStatus(status);
		modelService.save(jslUserOTPGenerationModel);
		return true;
	}

	@Override
	public List<JSLUserOTPGenerationModel> getUserAuctionOTPGeneratedRecordByOTPStatus(final OTPGeneratedForEnum generatedModule,
			final OTPUserIdentificationType identityType, final String identityValue, final String auctionEventId,
			final String status)
	{

		final StringBuilder query = new StringBuilder();
		final String andOperator = " AND {";

		query.append("SELECT {").append(JSLUserOTPGenerationModel.PK).append('}');
		query.append(" FROM {").append(JSLUserOTPGenerationModel._TYPECODE).append('}');
		query.append(" WHERE {").append(JSLUserOTPGenerationModel.GENERATEDFOR).append("} = ?generatedModule");
		query.append(andOperator).append(JSLUserOTPGenerationModel.IDENTIFICATIONTYPE).append("} = ?identificationType");
		query.append(andOperator).append(JSLUserOTPGenerationModel.IDENTIFICATIONVALUE).append("} = ?identificationValue");
		query.append(andOperator).append(JSLUserOTPGenerationModel.DELETED).append("} = ?deleted");
		query.append(andOperator).append(JSLUserOTPGenerationModel.AUCTIONEVENTID).append("} = ?auctionEventId");
		query.append(andOperator).append(JSLUserOTPGenerationModel.OTPSTATUS).append("} = ?otpStatus");

		final FlexibleSearchQuery flexiQuery = new FlexibleSearchQuery(query);
		flexiQuery.addQueryParameter("generatedModule", generatedModule);
		flexiQuery.addQueryParameter("identificationType", identityType);
		flexiQuery.addQueryParameter("identificationValue", identityValue);
		flexiQuery.addQueryParameter("auctionEventId", auctionEventId);
		flexiQuery.addQueryParameter("deleted", Boolean.FALSE);
		flexiQuery.addQueryParameter("otpStatus", status);

		final SearchResult<JSLUserOTPGenerationModel> result = flexibleSearchService.search(flexiQuery);

		return result.getResult();
	}

	/**
	 * @return
	 */
	@Override
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @return the flexibleSearchService
	 */
	@Override
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	/**
	 * @param flexibleSearchService
	 *           the flexibleSearchService to set
	 */
	@Override
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	@Override
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}




}
