/**
 *
 */
package com.jsl.core.daos.impl;

import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.List;

import com.jsl.core.daos.JslAuctionDeliveryDao;
import com.jsl.core.model.AuctionDeliveryChargeModel;

import reactor.util.CollectionUtils;


/**
 * @author manav.magoo
 *
 */
public class JslAuctionDeliveryDaoImpl implements JslAuctionDeliveryDao
{

	private static String GET_FREIGHT_CHARGES = "select max({" + AuctionDeliveryChargeModel.PK + "}) FROM {"
			+ AuctionDeliveryChargeModel._TYPECODE + "} WHERE Upper({" + AuctionDeliveryChargeModel.PLANTLOCATION
			+ "}) = ?plantLocation AND {" + AuctionDeliveryChargeModel.AMOUNT + "}>0 GROUP BY {"
			+ AuctionDeliveryChargeModel.LOCATION + "}, {" + AuctionDeliveryChargeModel.CITY + "} ORDER BY {"
			+ AuctionDeliveryChargeModel.LOCATION + "}, {" + AuctionDeliveryChargeModel.CITY + "}";

	private FlexibleSearchService flexibleSearchService;

	private SessionService sessionService;


	private UserService userService;


	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jsl.core.daos.JslFreightChargeDao#getFreightCharges(java.lang.String)
	 */
	@Override
	public List<AuctionDeliveryChargeModel> getFreightCharges(String plantLocation)
	{
		// XXX Auto-generated method stub

		return sessionService.executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public Object execute()
			{
				SearchResult<AuctionDeliveryChargeModel> result = null;
				final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(GET_FREIGHT_CHARGES);
				searchQuery.addQueryParameter("plantLocation", plantLocation);
				result = getFlexibleSearchService().search(searchQuery);
				return result.getResult();
			}
		}, userService.getAdminUser());
	}


	@Override
	public double getDeliveryCharge(final String location, final String postalCode)
	{
		final StringBuilder query = new StringBuilder();
		query.append("select {").append(AuctionDeliveryChargeModel.PK).append("} from {")
				.append(AuctionDeliveryChargeModel._TYPECODE).append("} where {location} = ?location and {postalCode} = ?postalCode");
		SearchResult<AuctionDeliveryChargeModel> result = null;
		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query);
		searchQuery.addQueryParameter("location", location);
		searchQuery.addQueryParameter("postalCode", postalCode);
		result = getFlexibleSearchService().search(searchQuery);

		if (CollectionUtils.isEmpty(result.getResult()))
		{
			return 0;
		}
		else
		{
			return result.getResult().get(0).getAmount();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jsl.core.daos.JslAuctionDeliveryDao#getDeliveryMode(java.lang.String)
	 */
	@Override
	public ZoneDeliveryModeModel getDeliveryMode(final String code)
	{
		final StringBuilder query = new StringBuilder();
		query.append("select {").append(ZoneDeliveryModeModel.PK).append("} from {").append(ZoneDeliveryModeModel._TYPECODE)
				.append("} where {" + ZoneDeliveryModeModel.CODE + "} = ?code");
		SearchResult<ZoneDeliveryModeModel> result = null;
		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query);
		searchQuery.addQueryParameter("code", code);
		result = getFlexibleSearchService().search(searchQuery);
		if (result.getResult().isEmpty())
		{
			return null;
		}
		return result.getResult().get(0);
	}

	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	/**
	 * @param flexibleSearchService
	 *           the flexibleSearchService to set
	 */
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

	/**
	 * @return the sessionService
	 */
	public SessionService getSessionService()
	{
		return sessionService;
	}

	/**
	 * @param sessionService
	 *           the sessionService to set
	 */
	public void setSessionService(SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(UserService userService)
	{
		this.userService = userService;
	}


}
