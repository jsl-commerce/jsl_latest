package com.jsl.core.daos.impl;

import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.List;

import com.jsl.core.daos.JslProxyBidRuleDao;
import com.jsl.core.model.AuctionEventModel;
import com.jsl.jslConfigurableBundleService.model.ProxyBidRuleModel;

public class JslProxyBidRuleDaoImpl implements JslProxyBidRuleDao
{
	private FlexibleSearchService flexibleSearchService;

	private static final String AUCTION_EVENT = "auctionEvent";
	private static final String BUNDLE_TEMPLATE = "bundleTemplate";
	private static final String CREATED_BY = "createdBy";

	private static final String GET_PROXY_BID_PRICE_FOR_CURRENT_USER = "SELECT {p." + ProxyBidRuleModel.PK + "} " + "FROM {"
			+ ProxyBidRuleModel._TYPECODE + " as p} " + "where  {p." + ProxyBidRuleModel.AUCTIONEVENT + "}= ?auctionEvent "
			+ "AND {p." + ProxyBidRuleModel.BUNDLETEMPLATE + "} =?bundletemplate AND {p." + ProxyBidRuleModel.CREATEDBY
			+ "} =?createdBy";

	private static final String GET_PROXY_BID_PRICE = "SELECT {p." + ProxyBidRuleModel.PK + "} " + "FROM {"
			+ ProxyBidRuleModel._TYPECODE + " as p} " + "where  {p." + ProxyBidRuleModel.AUCTIONEVENT + "}= ?auctionEvent "
			+ "AND {p." + ProxyBidRuleModel.BUNDLETEMPLATE + "} =?bundletemplate ORDER BY {maxProxyBidPrice} asc";

	private static final String GET_ALL_PROXY_BID_PRICE_FOR_CURRENT_USER = "SELECT {p." + ProxyBidRuleModel.PK + "} " + "FROM {"
			+ ProxyBidRuleModel._TYPECODE + " as p} " + "where  {p." + ProxyBidRuleModel.AUCTIONEVENT + "}= ?auctionEvent "
			+ "AND {p." + ProxyBidRuleModel.CREATEDBY + "} =?createdBy";


	@Override
	public ProxyBidRuleModel getProxyBidPriceForCurrentUser(final BundleTemplateModel bundleTemplate, final UserModel user)
	{
		try
		{
			final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_PROXY_BID_PRICE_FOR_CURRENT_USER);
			query.addQueryParameter(AUCTION_EVENT, bundleTemplate.getAuctionEvent());
			query.addQueryParameter(BUNDLE_TEMPLATE, bundleTemplate);
			query.addQueryParameter(CREATED_BY, user);

			return getFlexibleSearchService().searchUnique(query);
		}
		catch (final Exception e)
		{
			return null;
		}
	}

	@Override
	public List<ProxyBidRuleModel> getProxyBidPrice(final BundleTemplateModel bundleTemplate)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_PROXY_BID_PRICE);
		query.addQueryParameter(AUCTION_EVENT, bundleTemplate.getAuctionEvent());
		query.addQueryParameter(BUNDLE_TEMPLATE, bundleTemplate);
		query.setDisableCaching(true);
		final SearchResult<ProxyBidRuleModel> result = getFlexibleSearchService().search(query);

		return result.getResult();
	}

	@Override
	public List<ProxyBidRuleModel> getAllProxyBidForCurrentUser(final AuctionEventModel auctionEvent, final UserModel user)
	{
		try
		{
			final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ALL_PROXY_BID_PRICE_FOR_CURRENT_USER);
			query.addQueryParameter(AUCTION_EVENT, auctionEvent);
			query.addQueryParameter(CREATED_BY, user);
			query.setDisableCaching(true);
			final SearchResult<ProxyBidRuleModel> result = getFlexibleSearchService().search(query);

			return result.getResult();
		}
		catch (final Exception e)
		{
			return Collections.emptyList();
		}
	}

	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

}
