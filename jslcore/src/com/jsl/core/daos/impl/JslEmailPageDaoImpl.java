/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.jsl.core.daos.impl;

import de.hybris.platform.acceleratorservices.email.dao.impl.DefaultEmailPageDao;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageTemplateModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.HashMap;
import java.util.Map;

import com.jsl.core.daos.JslEmailPageDao;


/**
 * Default Data Access for looking up the CMS email page for a template name.
 */
public class JslEmailPageDaoImpl extends DefaultEmailPageDao implements JslEmailPageDao
{
	protected static final String QUERY = "SELECT {" + EmailPageModel.PK + "} " + "FROM {" + EmailPageModel._TYPECODE + " AS p "
			+ "JOIN " + EmailPageTemplateModel._TYPECODE + " AS t ON {p:" + EmailPageModel.MASTERTEMPLATE + "}={t:"
			+ EmailPageTemplateModel.PK + "}} " + "WHERE {t:" + EmailPageTemplateModel.FRONTENDTEMPLATENAME
			+ "}=?templateUid AND {t:" + EmailPageTemplateModel.CATALOGVERSION + "}=?catalogVersion AND ({t:"
			+ EmailPageTemplateModel.SITE + "}=?site OR {t:" + EmailPageTemplateModel.SITE + "} IS NULL)";


	@Override
	public EmailPageModel findEmailPageByFrontendTemplateName(final String frontendTemplateName,
			final CatalogVersionModel catalogVersion, final BaseSiteModel site)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put("templateUid", frontendTemplateName);
		params.put("catalogVersion", catalogVersion);
		params.put("site", site);
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(QUERY, params);
		final SearchResult<EmailPageModel> result = getFlexibleSearchService().search(fsq);

		return result.getCount() > 0 ? result.getResult().get(0) : null;
	}
}
