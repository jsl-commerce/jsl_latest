/**
 *
 */
package com.jsl.core.daos;

import java.sql.Date;
import java.util.List;

import com.jsl.core.enums.AuctionStatus;
import com.jsl.core.model.AuctionEventModel;


/**
 * @author manav.magoo
 *
 */
public interface JslAuctionDao
{

	public List<AuctionEventModel> getActiveAuctionList();

	public List<AuctionEventModel> getUpcomingAuctionDetails(Date curDate);

	public AuctionEventModel getAuctionEventForAuctionId(String auctionId);

	public List<AuctionEventModel> getLiveAuctionList(Date curDate);

	public List<AuctionEventModel> getAuctions(AuctionStatus status);

	public List<AuctionEventModel> getAllAuctionList(Date curDate);

	public List<AuctionEventModel> getAuctionByDateRange(String auctionStartDate, String auctionEndDate);

	public List<AuctionEventModel> getAuctionByDate(String auctionStartDate);
}
