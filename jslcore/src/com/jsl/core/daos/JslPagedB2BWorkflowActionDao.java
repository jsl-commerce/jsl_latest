/**
 *
 */
package com.jsl.core.daos;

import de.hybris.platform.b2bacceleratorservices.dao.PagedB2BWorkflowActionDao;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.workflow.enums.WorkflowActionType;
import de.hybris.platform.workflow.model.WorkflowActionModel;

import java.util.List;

import com.jsl.core.model.AuctionEventModel;


/**
 * @author himanshu.sial
 *
 */
public interface JslPagedB2BWorkflowActionDao extends PagedB2BWorkflowActionDao
{

	SearchPageData<WorkflowActionModel> findFilteredPagedWorkflowActionsByUserAndActionTypes(UserModel user,
			WorkflowActionType[] actionTypes, PageableData pageableData, String startDate, String endDate, String orderStatus,
			final String auctionId);

	public List<AuctionEventModel> getAuctionIds();
}
