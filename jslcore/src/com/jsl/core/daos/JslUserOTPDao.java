/**
 *
 */
package com.jsl.core.daos;

import java.util.Date;
import java.util.List;

import com.jsl.core.enums.OTPGeneratedForEnum;
import com.jsl.core.enums.OTPUserIdentificationType;
import com.jsl.core.model.JSLUserOTPGenerationModel;


/**
 * @author suyash.trivedi
 *
 */
public interface JslUserOTPDao
{

	/**
	 * @param generatedModule
	 * @param identityType
	 * @param identityValue
	 * @return
	 */
	List<JSLUserOTPGenerationModel> getUserOTPGeneratedRecord(OTPGeneratedForEnum generatedModule,
			OTPUserIdentificationType identityType, String identityValue);

	/**
	 * @param jslUserOTPGenerationModel
	 * @param otp
	 * @param time
	 * @param time2
	 */
	public boolean updateUserOTP(JSLUserOTPGenerationModel jslUserOTPGenerationModel, String otp, Date time, Date time2);

	/**
	 * @param generatedModule
	 * @param otp
	 * @param time
	 * @param time2
	 * @param identityType
	 * @param identityValue
	 */
	public void insertUserOTP(OTPGeneratedForEnum generatedModule, String otp, Date time, Date time2,
			OTPUserIdentificationType identityType, String identityValue);

	/**
	 * @param amwayUserOTPGenerationModel
	 * @return
	 */
	boolean incrementUserOTPAttempts(JSLUserOTPGenerationModel jslUserOTPGenerationModel);

	/**
	 * @param amwayUserOTPGenerationModel
	 * @return
	 */
	boolean expireUserOTPGeneratedRecord(JSLUserOTPGenerationModel jslUserOTPGenerationModel);

	/**
	 * @param generatedModule
	 * @param otp
	 * @param generatedTime
	 * @param expirationTime
	 * @param identificationType
	 * @param identificationValue
	 * @param auctionEvent
	 */
	void insertUserAuctionOTP(OTPGeneratedForEnum generatedModule, String otp, Date generatedTime, Date expirationTime,
			OTPUserIdentificationType identificationType, String identificationValue, String auctionEventId);

	/**
	 * @param generatedModule
	 * @param identityType
	 * @param identityValue
	 * @param auctionEvent
	 * @return
	 */
	List<JSLUserOTPGenerationModel> getUserAuctionOTPGeneratedRecord(OTPGeneratedForEnum generatedModule,
			OTPUserIdentificationType identityType, String identityValue, String auctionEventId);

	/**
	 * @param jslUserOTPGenerationModel
	 * @return
	 */
	boolean updateOTPStatus(JSLUserOTPGenerationModel jslUserOTPGenerationModel, String status);

	/**
	 * @param generatedModule
	 * @param identityType
	 * @param identityValue
	 * @param auctionEventId
	 * @param status
	 * @return
	 */
	List<JSLUserOTPGenerationModel> getUserAuctionOTPGeneratedRecordByOTPStatus(OTPGeneratedForEnum generatedModule,
			OTPUserIdentificationType identityType, String identityValue, String auctionEventId, String status);

}
