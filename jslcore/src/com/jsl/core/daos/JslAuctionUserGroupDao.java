/**
 *
 */
package com.jsl.core.daos;

import com.jsl.core.model.AuctionUserGroupModel;


/**
 * @author manav.magoo
 *
 */
public interface JslAuctionUserGroupDao
{

	AuctionUserGroupModel getAuctionUserGroup(String uid);
}
