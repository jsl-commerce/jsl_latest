/**
 *
 */
package com.jsl.core.daos;

import de.hybris.platform.acceleratorservices.email.dao.EmailPageDao;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;


/**
 *
 */
public interface JslEmailPageDao extends EmailPageDao
{
	/**
	 * Retrieves EmailPage given its frontend template name.
	 *
	 * @param frontendTemplateName
	 *           the frontend template name
	 * @param catalogVersion
	 *           the catalog version
	 * @return EmailPage object if found, null otherwise
	 */
	EmailPageModel findEmailPageByFrontendTemplateName(String frontendTemplateName, CatalogVersionModel catalogVersion,
			BaseSiteModel site);

}
