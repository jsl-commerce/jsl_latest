package com.jsl.core.daos;

import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.List;

import com.jsl.core.model.AuctionEventModel;
import com.jsl.jslConfigurableBundleService.model.ProxyBidRuleModel;


public interface JslProxyBidRuleDao
{

	public ProxyBidRuleModel getProxyBidPriceForCurrentUser(BundleTemplateModel bundleTemplate, UserModel user);

	public List<ProxyBidRuleModel> getProxyBidPrice(BundleTemplateModel bundleTemplate);

	public List<ProxyBidRuleModel> getAllProxyBidForCurrentUser(AuctionEventModel auctionEvent, UserModel currentUser);
}
