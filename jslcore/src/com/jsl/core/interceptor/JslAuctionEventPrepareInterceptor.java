/**
 *
 */
package com.jsl.core.interceptor;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;

import java.util.Calendar;

import com.jsl.core.model.AuctionEventModel;


/**
 * @author himanshu.sial
 *
 */
public class JslAuctionEventPrepareInterceptor implements PrepareInterceptor<AuctionEventModel>
{

	public void setKeyGenerator(final KeyGenerator keyGenerator)
	{
		this.keyGenerator = keyGenerator;
	}

	private KeyGenerator keyGenerator;

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.servicelayer.interceptor.PrepareInterceptor#onPrepare(java.lang.Object,
	 * de.hybris.platform.servicelayer.interceptor.InterceptorContext)
	 */
	@Override
	public void onPrepare(final AuctionEventModel model, final InterceptorContext ctx) throws InterceptorException
	{



			final AuctionEventModel auctionEvent = model;

		if (auctionEvent.getAuctionId() == null && auctionEvent.getStartDate() != null && auctionEvent.getEndDate() != null
				&& auctionEvent.getAuctionGroup() != null && auctionEvent.getAuctionStatus() != null)
			{
			if (auctionEvent.getEndDate().compareTo(auctionEvent.getStartDate()) == 1
					&& auctionEvent.getStartDate().compareTo(Calendar.getInstance().getTime()) == 1)
			{

				auctionEvent.setAuctionId(Integer.valueOf(keyGenerator.generate().toString()));
			}
			}


	}


}
