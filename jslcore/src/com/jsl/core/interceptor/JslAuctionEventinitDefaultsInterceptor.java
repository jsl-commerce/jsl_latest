/**
 *
 */
package com.jsl.core.interceptor;

import de.hybris.platform.servicelayer.interceptor.InitDefaultsInterceptor;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;

import com.jsl.core.model.AuctionEventModel;


/**
 * @author s.bhambhani
 *
 */
public class JslAuctionEventinitDefaultsInterceptor implements InitDefaultsInterceptor
{


	public void setKeyGenerator(final KeyGenerator keyGenerator)
	{
		this.keyGenerator = keyGenerator;
	}

	private KeyGenerator keyGenerator;

	@Override
	public void onInitDefaults(final Object model, final InterceptorContext ctx) throws InterceptorException
	{
		if (model instanceof AuctionEventModel)
		{

			final AuctionEventModel auctionEvent = (AuctionEventModel) model;
			auctionEvent.setAuctionId(Integer.valueOf(keyGenerator.generate().toString()));
		}
	}
}



