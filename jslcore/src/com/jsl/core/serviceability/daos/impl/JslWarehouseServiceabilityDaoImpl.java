/**
 *
 */
package com.jsl.core.serviceability.daos.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.jsl.core.model.JslWarehouseServiceabilityModel;
import com.jsl.core.serviceability.daos.JslWarehouseServiceabilityDao;


/**
 * @author manav.magoo
 *
 */
public class JslWarehouseServiceabilityDaoImpl implements JslWarehouseServiceabilityDao<JslWarehouseServiceabilityModel>
{

	@Autowired
	private FlexibleSearchService flexibleSearchService;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.jsl.core.serviceability.daos.JslWarehouseServiceabilityDao#getServiceableWareHouse(java.lang.String,
	 * de.hybris.platform.basecommerce.model.site.BaseSiteModel)
	 */
	@Override
	public WarehouseModel getServiceableWarehouse(final String salesOffice, final BaseSiteModel baseSite)
	{
		final String queryString = "SELECT " + JslWarehouseServiceabilityModel.PK + " FROM {"
				+ JslWarehouseServiceabilityModel._TYPECODE + "} " + "WHERE {" + JslWarehouseServiceabilityModel.SALESOFFICE
				+ "} = (?salesOffice)" + "AND {" + JslWarehouseServiceabilityModel.SITE + "} = (?baseSite)";

		final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
		query.addQueryParameter("salesOffice", salesOffice);
		query.addQueryParameter("baseSite", baseSite);
		query.setResultClassList(Arrays.asList(JslWarehouseServiceabilityModel.class));

		final List<JslWarehouseServiceabilityModel> searchResult = flexibleSearchService
				.<JslWarehouseServiceabilityModel> search(query).getResult();

		return CollectionUtils.isNotEmpty(searchResult) ? searchResult.get(0).getWarehouse() : null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.jsl.core.serviceability.daos.JslWarehouseServiceabilityDao#getServiceableWarehouseForPostalCodeRegion(java.
	 * lang.String, de.hybris.platform.basecommerce.model.site.BaseSiteModel,
	 * de.hybris.platform.core.model.c2l.RegionModel)
	 */
	@Override
	public WarehouseModel getServiceableWarehouseForSalesOfficeRegion(final String salesOffice, final BaseSiteModel baseSite,
			final RegionModel region)
	{
		final String queryString = "SELECT " + JslWarehouseServiceabilityModel.PK + " FROM {"
				+ JslWarehouseServiceabilityModel._TYPECODE + "} " + "WHERE {" + JslWarehouseServiceabilityModel.SALESOFFICE
				+ "} = (?salesOffice)" + "AND {" + JslWarehouseServiceabilityModel.SITE + "} = (?baseSite) " + "AND {"
				+ JslWarehouseServiceabilityModel.REGION + "} = (?region)";
		final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
		query.addQueryParameter("salesOffice", salesOffice);
		query.addQueryParameter("baseSite", baseSite);
		query.addQueryParameter("region", region);
		query.setResultClassList(Arrays.asList(JslWarehouseServiceabilityModel.class));

		final List<JslWarehouseServiceabilityModel> searchResult = flexibleSearchService
				.<JslWarehouseServiceabilityModel> search(query).getResult();

		return CollectionUtils.isNotEmpty(searchResult) ? searchResult.get(0).getWarehouse() : null;
	}

}
