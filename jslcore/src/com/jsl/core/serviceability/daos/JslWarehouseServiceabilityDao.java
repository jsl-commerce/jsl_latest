/**
 *
 */
package com.jsl.core.serviceability.daos;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;


/**
 * @author manav.magoo
 *
 */
public interface JslWarehouseServiceabilityDao<T>
{
	/**
	 * Get the warehouse from the postal code.
	 *
	 * @param postalCode
	 *           the postal code
	 * @param baseSite
	 * @return the warehouse model
	 */
	WarehouseModel getServiceableWarehouse(String postalCode, BaseSiteModel baseSite);


	/**
	 * @param salesOffice
	 * @param baseSite
	 * @param region
	 * @return
	 */
	WarehouseModel getServiceableWarehouseForSalesOfficeRegion(String salesOffice, BaseSiteModel baseSite, RegionModel region);


}
