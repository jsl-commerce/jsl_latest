/**
 *
 */
package com.jsl.core.serviceability.services;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;


/**
 * @author manav.magoo
 *
 */
public interface JslWarehouseServiceabilityService
{

	/**
	 * Get the warehouse from the postal code.
	 *
	 * @param postalCode
	 *           the postal code
	 * @param baseSite
	 * @return the warehouse model
	 */
	WarehouseModel getServiceableWareHouse(String salesOffice, BaseSiteModel baseSite);



	/**
	 * This service checks if the postal code is serviceable or not
	 *
	 * @param postalCode
	 * @param baseSite
	 * @param region
	 *
	 * @return true if the postal code is serviceable else false
	 */
	public Boolean isSalesOfficeServiceable(final String salesOffice, BaseSiteModel baseSite, RegionModel region);



	/**
	 * @param salesOffice
	 * @param baseSite
	 * @param region
	 * @return
	 */
	WarehouseModel getServiceableWareHouse(String salesOffice, BaseSiteModel baseSite, RegionModel region);


}
