/**
 *
 */
package com.jsl.core.serviceability.services.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import com.jsl.core.model.JslWarehouseServiceabilityModel;
import com.jsl.core.serviceability.daos.JslWarehouseServiceabilityDao;
import com.jsl.core.serviceability.services.JslWarehouseServiceabilityService;


/**
 * @author manav.magoo
 *
 */
public class JslWarehouseServiceabilityServiceImpl implements JslWarehouseServiceabilityService
{

	JslWarehouseServiceabilityDao<JslWarehouseServiceabilityModel> jslWarehouseServiceabilityDao;



	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.jsl.core.serviceability.services.JslWarehouseServiceabilityService#getServiceableWareHouse(java.lang.String,
	 * de.hybris.platform.basecommerce.model.site.BaseSiteModel)
	 */
	@Override
	public WarehouseModel getServiceableWareHouse(final String salesOffice, final BaseSiteModel baseSite)
	{
		return getJslWarehouseServiceabilityDao().getServiceableWarehouse(salesOffice, baseSite);
	}

	@Override
	public WarehouseModel getServiceableWareHouse(final String salesOffice, final BaseSiteModel baseSite, final RegionModel region)
	{
		return getJslWarehouseServiceabilityDao().getServiceableWarehouseForSalesOfficeRegion(salesOffice, baseSite, region);
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.jsl.core.serviceability.services.JslWarehouseServiceabilityService#isPostalCodeServiceable(java.lang.String,
	 * de.hybris.platform.basecommerce.model.site.BaseSiteModel, de.hybris.platform.core.model.c2l.RegionModel)
	 */
	@Override
	public Boolean isSalesOfficeServiceable(final String salesOffice, final BaseSiteModel baseSite, final RegionModel region)
	{
		Boolean isServiceable = Boolean.FALSE;

		final WarehouseModel warehouseModel = getJslWarehouseServiceabilityDao()
				.getServiceableWarehouseForSalesOfficeRegion(salesOffice, baseSite, region);

		if (null != warehouseModel)
		{
			isServiceable = Boolean.TRUE;
		}
		return isServiceable;
	}



	/**
	 * @return the jslWarehouseServiceabilityDao
	 */
	public JslWarehouseServiceabilityDao<JslWarehouseServiceabilityModel> getJslWarehouseServiceabilityDao()
	{
		return jslWarehouseServiceabilityDao;
	}

	/**
	 * @param jslWarehouseServiceabilityDao
	 *           the jslWarehouseServiceabilityDao to set
	 */
	public void setJslWarehouseServiceabilityDao(
			final JslWarehouseServiceabilityDao<JslWarehouseServiceabilityModel> jslWarehouseServiceabilityDao)
	{
		this.jslWarehouseServiceabilityDao = jslWarehouseServiceabilityDao;
	}

}
