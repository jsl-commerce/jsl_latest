/**
 *
 */
package com.jsl.core.job;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;

import java.sql.Date;
import java.util.List;

import com.jsl.core.enums.AuctionStatus;
import com.jsl.core.model.AuctionEventModel;
import com.jsl.core.services.JslAuctionService;


/**
 * @author manav.magoo
 *
 */
public class JslUpdateAuctionStatusJob extends AbstractJobPerformable<CronJobModel>
{

	private JslAuctionService jslAuctionService;
	private ModelService modelService;

	@Override
	public PerformResult perform(final CronJobModel arg0)
	{

		PerformResult result = null;

		final List<AuctionEventModel> ongoingAuctions = jslAuctionService.getLiveAuctionList();

		final Date curDate = new Date(System.currentTimeMillis());

		for (final AuctionEventModel auction : ongoingAuctions)
		{
			if (auction.getEndDate().before(curDate))
			{
				auction.setAuctionStatus(AuctionStatus.CLOSED);
				modelService.save(auction);
			}
		}


		result = new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);

		return result;
	}


	/**
	 * @return the jslAuctionService
	 */
	public JslAuctionService getJslAuctionService()
	{
		return jslAuctionService;
	}


	/**
	 * @param jslAuctionService
	 *           the jslAuctionService to set
	 */
	public void setJslAuctionService(final JslAuctionService jslAuctionService)
	{
		this.jslAuctionService = jslAuctionService;
	}


	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}


	/**
	 * @param modelService
	 *           the modelService to set
	 */
	@Override
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}




}
