/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.core.job;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import javax.annotation.Resource;

import com.jsl.core.dto.JslOTPGenerationDto;
import com.jsl.core.enums.OTPGeneratedForEnum;
import com.jsl.core.enums.OTPUserIdentificationType;
import com.jsl.core.services.JslUserOTPService;


/**
 *
 */
public class JslAuctionOtpJob extends AbstractJobPerformable<CronJobModel>
{

	@Resource(name = "jslUserOTPService")
	private JslUserOTPService jslUserOTPService;

	/**
	 * @return the jslUserOTPService
	 */
	public JslUserOTPService getJslUserOTPService()
	{
		return jslUserOTPService;
	}

	/**
	 * @param jslUserOTPService
	 *           the jslUserOTPService to set
	 */
	public void setJslUserOTPService(final JslUserOTPService jslUserOTPService)
	{
		this.jslUserOTPService = jslUserOTPService;
	}

	@Override
	public PerformResult perform(final CronJobModel arg0)
	{
		PerformResult result = null;

		final JslOTPGenerationDto jslOTPGenerationDto = new JslOTPGenerationDto();
		jslOTPGenerationDto.setEmailAddress("manav.magan@gmail.com");
		jslOTPGenerationDto.setDisplayName("Manav Magan");
		jslOTPGenerationDto.setFromDisplayName("JSL");
		jslOTPGenerationDto.setAuctionId("1");
		final String identityValue = "manav.magan@gmail.com";

		getJslUserOTPService().generateOTP(OTPGeneratedForEnum.AUCTION, OTPUserIdentificationType.EMAIL_ID, identityValue,
				jslOTPGenerationDto);


		result = new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);

		return result;
	}




}
