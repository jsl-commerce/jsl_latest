/**
 *
 */
package com.jsl.core.util;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.jsl.core.constants.JslCoreConstants;


/**
 * @author manav.magoo
 *
 */
public class JslB2bUnitUtil
{
	private static final String DEFAULT_B2BUNIT_CODE = "default.b2b.unit.codes";
	private static final String UNDERSCORE = "_";
	private static final String AUCTION_UNIT_CODE = "auction.b2bunit.code";
	@Autowired
	SessionService sessionService;
	@Autowired
	UserService userService;
	@Autowired
	B2BUnitService b2bUnitService;

	private static final Logger LOG = Logger.getLogger(JslB2bUnitUtil.class);

	public static B2BUnitModel getParentB2bUnit(final UserModel user)
	{
		final String defaultB2bUnit = Config.getParameter(DEFAULT_B2BUNIT_CODE);
		final List<String> defaultB2bUnitList = Arrays.asList(defaultB2bUnit.split(","));

		final Collection<PrincipalGroupModel> b2bUnits = user.getAllGroups().stream()
				.filter(event -> (event.getItemtype().equalsIgnoreCase(B2BUnitModel._TYPECODE)
						&& !defaultB2bUnitList.contains(event.getUid())))
				.collect(Collectors.toList());

		return !b2bUnits.isEmpty() ? (B2BUnitModel) b2bUnits.iterator().next() : null;
	}



	public Map<String, B2BUnitModel> getChildB2BUnits(final B2BUnitModel parentUnit)
	{
		return sessionService.executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public Object execute()
			{
				final B2BUnitModel childb2bUnit1000 = (B2BUnitModel) b2bUnitService.getUnitForUid(parentUnit.getUid() + UNDERSCORE
						+ JslCoreConstants.SALESORGANIZATION_1000 + UNDERSCORE + Config.getParameter(AUCTION_UNIT_CODE));

				LOG.info("JSL child unit In execute for 1000" + childb2bUnit1000.getId());

				final B2BUnitModel childb2bUnit1010 = (B2BUnitModel) b2bUnitService.getUnitForUid(parentUnit.getUid() + UNDERSCORE
						+ JslCoreConstants.SALESORGANIZATION_1010 + UNDERSCORE + Config.getParameter(AUCTION_UNIT_CODE));

				LOG.info("JSL child unit In execute for 1010" + childb2bUnit1010.getId());

				final HashMap<String, B2BUnitModel> childB2BUnits = new HashMap<>(2);
				childB2BUnits.put(JslCoreConstants.SALESORGANIZATION_1000, childb2bUnit1000);
				childB2BUnits.put(JslCoreConstants.SALESORGANIZATION_1010, childb2bUnit1010);
				return childB2BUnits;
			}
			// set because of LynxTradeInRestriction
		}, userService.getAdminUser());
	}


}
