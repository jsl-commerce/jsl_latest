/**
 *
 */
package com.jsl.core.product.enquiry.service;

import java.util.List;
import java.util.Map;

import com.jsl.core.dto.ProductEnquiry;
import com.jsl.core.model.ProductEnquiryModel;


/**
 * @author himanshu.sial
 *
 */
public interface JslProductEnquiryService
{

	public List<ProductEnquiryModel> getProductEnquiriesByUser();

	public Map<String, String> createLeadInEcc(ProductEnquiry productEnquiry);
}
