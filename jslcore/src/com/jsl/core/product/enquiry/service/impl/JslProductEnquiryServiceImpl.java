/**
 *
 */
package com.jsl.core.product.enquiry.service.impl;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.http.MediaType;

import com.jsl.core.constants.JslCoreConstants;
import com.jsl.core.dto.ProductEnquiry;
import com.jsl.core.model.ProductEnquiryModel;
import com.jsl.core.product.enquiry.dao.JslProductEnquiryDao;
import com.jsl.core.product.enquiry.request.dto.LeadDto;
import com.jsl.core.product.enquiry.request.dto.LeadItem;
import com.jsl.core.product.enquiry.request.dto.LeadItemDto;
import com.jsl.core.product.enquiry.request.dto.ProductEnquiryRequestDto;
import com.jsl.core.product.enquiry.response.dto.ProductEnquiryResponseDto;
import com.jsl.core.product.enquiry.service.JslProductEnquiryService;
import com.jsl.core.util.JslB2bUnitUtil;
import com.jsl.rest.service.JslRestService;


/**
 * @author himanshu.sial
 *
 */
public class JslProductEnquiryServiceImpl implements JslProductEnquiryService
{

	private static final Logger LOG = Logger.getLogger(JslProductEnquiryServiceImpl.class);

	private UserService userService;

	private JslProductEnquiryDao jslProductEnquiryDao;

	private JslRestService jslRestService;

	private static final String HYBRIS_PREFIX = "CommerceEnquiry-";

	private static final String LEAD_CREATION_URL_KEY = "lead.creation.url";

	public List<ProductEnquiryModel> getProductEnquiriesByUser()
	{
		final UserModel user = getUserService().getCurrentUser();
		LOG.info("Getting product enquiry history for user : " + user.getUid());
		return getJslProductEnquiryDao().getProductEnquiriesByUser(user);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.jsl.core.product.enquiry.service.JslProductEnquiryService#createLeadInEcc(com.jsl.core.dto.ProductEnquiry)
	 */
	@Override
	public Map<String, String> createLeadInEcc(final ProductEnquiry productEnquiry)
	{

		final Map<String, String> customerMap = getCustomerId();
		final ProductEnquiryRequestDto request = createLeadRequest(productEnquiry, customerMap.get(JslCoreConstants.CUSTOMER_ID));
		final ProductEnquiryResponseDto response = jslRestService.postForObjectCommand(Config.getParameter(LEAD_CREATION_URL_KEY),
				request, ProductEnquiryResponseDto.class, "", "", MediaType.APPLICATION_ATOM_XML);
		customerMap.put(JslCoreConstants.ECC_LEAD_ID, response.getLeadResponse().getLeadId());
		customerMap.put(JslCoreConstants.KAM_ID, response.getLeadResponse().getOwnerPartyName());

		return customerMap;
	}

	private ProductEnquiryRequestDto createLeadRequest(final ProductEnquiry productEnquiry, final String customerId)
	{
		final ProductEnquiryRequestDto request = new ProductEnquiryRequestDto();

		final LeadDto lead = new LeadDto();
		lead.setCustomerId(customerId);
		lead.setEnquiryType("D");
		lead.setLeadDocType(JslCoreConstants.ECC_LEAD_DOCUMENT_CODE);
		lead.setName(HYBRIS_PREFIX + productEnquiry.getEnquiryId());
		lead.setQualifcationLevelcode(JslCoreConstants.ECC_REGISTERED_USER_QUALIFICATION_CODE);
		lead.setTeamLead(JslCoreConstants.STAINLESS);
		lead.setOriginTypeCode(JslCoreConstants.ECC_ORIGIN_TYPE_CODE);
		lead.setNote(productEnquiry.getComments());

		final LeadItem leadItem = new LeadItem();

		productEnquiry.getEnquiryEntries().forEach(product -> {
			final LeadItemDto dto = new LeadItemDto();
			dto.setEdge(product.getEdgeCondition());
			dto.setProductCategooryInternalId(JslCoreConstants.ECC_PROODUCT_CATEGORY_INTERNAL_ID);
			dto.setLength(product.getLength());
			dto.setPvc(product.getPvc());
			dto.setThickness(product.getThickness());
			dto.setWidth(product.getWidth());
			dto.setQuantity(product.getQuantity());
			dto.setProductType(product.getProductType());
			dto.setFinish(product.getFinish());
			dto.setGrade(product.getGrade());
			dto.setIlp(product.getIlp());
			dto.setSeries(product.getSeries());
			dto.setGradeGroupContent(product.getGrade());
			leadItem.getLeadItems().add(dto);
		});

		lead.setLeadItem(leadItem);
		request.setLead(lead);

		return request;
	}


	private Map<String, String> getCustomerId()
	{

		final Map<String, String> response = new HashMap<String, String>();

		final UserModel user = getUserService().getCurrentUser();

		final B2BUnitModel parentb2bUnit = JslB2bUnitUtil.getParentB2bUnit(user);

		if (parentb2bUnit != null)
		{
			response.put(JslCoreConstants.CUSTOMER_ID, parentb2bUnit.getUid());
		}
		else
		{
			response.put(JslCoreConstants.CUSTOMER_ID, user.getUid());
		}
		return response;
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}


	/**
	 * @return the jslProductEnquiryDao
	 */
	public JslProductEnquiryDao getJslProductEnquiryDao()
	{
		return jslProductEnquiryDao;
	}


	/**
	 * @param jslProductEnquiryDao
	 *           the jslProductEnquiryDao to set
	 */
	public void setJslProductEnquiryDao(final JslProductEnquiryDao jslProductEnquiryDao)
	{
		this.jslProductEnquiryDao = jslProductEnquiryDao;
	}

	/**
	 * @return the jslRestService
	 */
	public JslRestService getJslRestService()
	{
		return jslRestService;
	}

	/**
	 * @param jslRestService
	 *           the jslRestService to set
	 */
	public void setJslRestService(final JslRestService jslRestService)
	{
		this.jslRestService = jslRestService;
	}
}
