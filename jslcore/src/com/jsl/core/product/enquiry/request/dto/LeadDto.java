/**
 * 
 */
package com.jsl.core.product.enquiry.request.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author himanshu.sial
 *
 */
@XmlRootElement(name = "Lead")
@XmlAccessorType(XmlAccessType.FIELD)
public class LeadDto
{
	@XmlElement(name = "AccountPartyID")
	private String accountPartyId;

	@XmlElement(name = "QualificationLevelCode")
	private String qualifcationLevelcode;

	@XmlElement(name = "Name")
	private String name;

	@XmlElement(name = "ZLeadDocType")
	private String leadDocType;

	@XmlElement(name = "ZTeamLead")
	private String teamLead;

	@XmlElement(name = "LeadItem")
	private LeadItem leadItem;

	@XmlElement(name = "OriginTypeCode")
	private String originTypeCode;

	@XmlElement(name = "ZExternal_Account_ID")
	private String customerId;
	
	@XmlElement(name = "ZExp_DomL")
	private String enquiryType;
	
	@XmlElement(name = "Note")
	private String note;
	
	/**
	 * @return the accountPartyId
	 */
	public String getAccountPartyId()
	{
		return accountPartyId;
	}

	/**
	 * @param accountPartyId
	 *           the accountPartyId to set
	 */
	public void setAccountPartyId(String accountPartyId)
	{
		this.accountPartyId = accountPartyId;
	}

	/**
	 * @return the qualifcationLevelcode
	 */
	public String getQualifcationLevelcode()
	{
		return qualifcationLevelcode;
	}

	/**
	 * @param qualifcationLevelcode
	 *           the qualifcationLevelcode to set
	 */
	public void setQualifcationLevelcode(String qualifcationLevelcode)
	{
		this.qualifcationLevelcode = qualifcationLevelcode;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name
	 *           the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @return the leadDocType
	 */
	public String getLeadDocType()
	{
		return leadDocType;
	}

	/**
	 * @param leadDocType
	 *           the leadDocType to set
	 */
	public void setLeadDocType(String leadDocType)
	{
		this.leadDocType = leadDocType;
	}

	/**
	 * @return the teamLead
	 */
	public String getTeamLead()
	{
		return teamLead;
	}

	/**
	 * @param teamLead
	 *           the teamLead to set
	 */
	public void setTeamLead(String teamLead)
	{
		this.teamLead = teamLead;
	}

	/**
	 * @return the leadItem
	 */
	public LeadItem getLeadItem()
	{
		return leadItem;
	}

	/**
	 * @param leadItem
	 *           the leadItem to set
	 */
	public void setLeadItem(LeadItem leadItem)
	{
		this.leadItem = leadItem;
	}

	/**
	 * @return the originTypeCode
	 */
	public String getOriginTypeCode()
	{
		return originTypeCode;
	}

	/**
	 * @param originTypeCode
	 *           the originTypeCode to set
	 */
	public void setOriginTypeCode(String originTypeCode)
	{
		this.originTypeCode = originTypeCode;
	}

	/**
	 * @return the customerId
	 */
	public String getCustomerId()
	{
		return customerId;
	}

	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(String customerId)
	{
		this.customerId = customerId;
	}

	/**
	 * @return the enquiryType
	 */
	public String getEnquiryType()
	{
		return enquiryType;
	}

	/**
	 * @param enquiryType the enquiryType to set
	 */
	public void setEnquiryType(String enquiryType)
	{
		this.enquiryType = enquiryType;
	}

	/**
	 * @return the note
	 */
	public String getNote()
	{
		return note;
	}

	/**
	 * @param note the note to set
	 */
	public void setNote(String note)
	{
		this.note = note;
	}
	
}
