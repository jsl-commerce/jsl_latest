/**
 * 
 */
package com.jsl.core.product.enquiry.request.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author himanshu.sial
 *
 */
@XmlRootElement(name = "LeadItem")
@XmlAccessorType(XmlAccessType.FIELD)
public class LeadItem
{

	@XmlElement(name="LeadItem")
	private List<LeadItemDto> leadItems;

	/**
	 * @return the leadItems
	 */
	public List<LeadItemDto> getLeadItems()
	{
		if (this.leadItems == null)
		{
			this.leadItems = new ArrayList<LeadItemDto>();
		}
		return leadItems;
	}

	/**
	 * @param leadItems
	 *           the leadItems to set
	 */
	public void setLeadItems(List<LeadItemDto> leadItems)
	{
		this.leadItems = leadItems;
	}


}
