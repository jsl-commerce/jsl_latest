/**
 * 
 */
package com.jsl.core.product.enquiry.request.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author himanshu.sial
 *
 */
@XmlRootElement(name = "LeadItem")
@XmlAccessorType(XmlAccessType.FIELD)
public class LeadItemDto
{
	@XmlElement(name = "ProductCategoryInternalID")
	private String productCategooryInternalId;

	@XmlElement(name = "ZEdgCon1content")
	private String edge;

	@XmlElement(name = "ZThickness1content")
	private String thickness;

	@XmlElement(name = "ZLength1content")
	private String length;

	@XmlElement(name = "ZILP1")
	private String ilp;

	@XmlElement(name = "ZPVC1content")
	private String pvc;

	@XmlElement(name = "ZProdExtID")
	private String productExtId;

	@XmlElement(name = "ZStandard1content")
	private String standardContent;


	@XmlElement(name = "ZWidth1content")
	private String width;


	@XmlElement(name = "Quantity")
	private String quantity;

	@XmlElement(name = "ZProductType1content")
	private String productType;

	@XmlElement(name = "ZSeries1content")
	private String series;

	@XmlElement(name = "ZGrade1content")
	private String grade;

	@XmlElement(name = "ZFinish1content")
	private String finish;

	@XmlElement(name = "ZGradeGroup1content")
	private String gradeGroupContent;
	
	/**
	 * @return the edge
	 */
	public String getEdge()
	{
		return edge;
	}

	/**
	 * @param edge
	 *           the edge to set
	 */
	public void setEdge(String edge)
	{
		this.edge = edge;
	}

	/**
	 * @return the thichness
	 */
	public String getThickness()
	{
		return thickness;
	}

	/**
	 * @param thichness
	 *           the thichness to set
	 */
	public void setThickness(String thickness)
	{
		this.thickness = thickness;
	}

	/**
	 * @return the length
	 */
	public String getLength()
	{
		return length;
	}

	/**
	 * @param length
	 *           the length to set
	 */
	public void setLength(String length)
	{
		this.length = length;
	}

	/**
	 * @return the ilp
	 */
	public String getIlp()
	{
		return ilp;
	}

	/**
	 * @param ilp
	 *           the ilp to set
	 */
	public void setIlp(String ilp)
	{
		this.ilp = ilp;
	}

	/**
	 * @return the pvc
	 */
	public String getPvc()
	{
		return pvc;
	}

	/**
	 * @param pvc
	 *           the pvc to set
	 */
	public void setPvc(String pvc)
	{
		this.pvc = pvc;
	}

	/**
	 * @return the productExtId
	 */
	public String getProductExtId()
	{
		return productExtId;
	}

	/**
	 * @param productExtId
	 *           the productExtId to set
	 */
	public void setProductExtId(String productExtId)
	{
		this.productExtId = productExtId;
	}

	/**
	 * @return the standardContent
	 */
	public String getStandardContent()
	{
		return standardContent;
	}

	/**
	 * @param standardContent
	 *           the standardContent to set
	 */
	public void setStandardContent(String standardContent)
	{
		this.standardContent = standardContent;
	}

	/**
	 * @return the width
	 */
	public String getWidth()
	{
		return width;
	}

	/**
	 * @param width
	 *           the width to set
	 */
	public void setWidth(String width)
	{
		this.width = width;
	}

	/**
	 * @return the quantity
	 */
	public String getQuantity()
	{
		return quantity;
	}

	/**
	 * @param quantity
	 *           the quantity to set
	 */
	public void setQuantity(String quantity)
	{
		this.quantity = quantity;
	}

	/**
	 * @return the productType
	 */
	public String getProductType()
	{
		return productType;
	}

	/**
	 * @param productType
	 *           the productType to set
	 */
	public void setProductType(String productType)
	{
		this.productType = productType;
	}

	/**
	 * @return the series
	 */
	public String getSeries()
	{
		return series;
	}

	/**
	 * @param series
	 *           the series to set
	 */
	public void setSeries(String series)
	{
		this.series = series;
	}

	/**
	 * @return the grade
	 */
	public String getGrade()
	{
		return grade;
	}

	/**
	 * @param grade
	 *           the grade to set
	 */
	public void setGrade(String grade)
	{
		this.grade = grade;
	}

	/**
	 * @return the finish
	 */
	public String getFinish()
	{
		return finish;
	}

	/**
	 * @param finish
	 *           the finish to set
	 */
	public void setFinish(String finish)
	{
		this.finish = finish;
	}

	/**
	 * @return the productCategooryInternalId
	 */
	public String getProductCategooryInternalId()
	{
		return productCategooryInternalId;
	}

	/**
	 * @param productCategooryInternalId
	 *           the productCategooryInternalId to set
	 */
	public void setProductCategooryInternalId(String productCategooryInternalId)
	{
		this.productCategooryInternalId = productCategooryInternalId;
	}

	/**
	 * @return the gradeGroupContent
	 */
	public String getGradeGroupContent()
	{
		return gradeGroupContent;
	}

	/**
	 * @param gradeGroupContent the gradeGroupContent to set
	 */
	public void setGradeGroupContent(String gradeGroupContent)
	{
		this.gradeGroupContent = gradeGroupContent;
	}
	
}
