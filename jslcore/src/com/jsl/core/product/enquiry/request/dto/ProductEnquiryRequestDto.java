/**
 * 
 */
package com.jsl.core.product.enquiry.request.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author himanshu.sial
 *
 */
@XmlRootElement(name = "LeadCollection")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductEnquiryRequestDto
{
	@XmlElement(name = "Lead")
	private LeadDto lead;

	/**
	 * @return the lead
	 */
	public LeadDto getLead()
	{
		return lead;
	}

	/**
	 * @param lead
	 *           the lead to set
	 */
	public void setLead(LeadDto lead)
	{
		this.lead = lead;
	}
}
