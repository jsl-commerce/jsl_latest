/**
 *
 */
package com.jsl.core.product.enquiry.dao;

import de.hybris.platform.core.model.user.UserModel;

import java.util.List;

import com.jsl.core.model.ProductEnquiryModel;


/**
 * @author himanshu.sial
 *
 */
public interface JslProductEnquiryDao
{
	public List<ProductEnquiryModel> getProductEnquiriesByUser(UserModel user);
}
