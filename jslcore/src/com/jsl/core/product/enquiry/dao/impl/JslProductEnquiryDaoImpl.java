/**
 *
 */
package com.jsl.core.product.enquiry.dao.impl;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.List;

import com.jsl.core.model.ProductEnquiryModel;
import com.jsl.core.product.enquiry.dao.JslProductEnquiryDao;


/**
 * @author himanshu.sial
 *
 */
public class JslProductEnquiryDaoImpl implements JslProductEnquiryDao
{

	private FlexibleSearchService flexibleSearchService;

	private static final String GET_PRODUCT_ENQUIRY_BY_USER = "SELECT {pk} FROM {" + ProductEnquiryModel._TYPECODE + "} WHERE {"
			+ ProductEnquiryModel.USER + "} = ?user ORDER BY {"+ProductEnquiryModel.CREATIONTIME+"} DESC";

	@Override
	public List<ProductEnquiryModel> getProductEnquiriesByUser(final UserModel user)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_PRODUCT_ENQUIRY_BY_USER);
		query.setDisableCaching(true);
		query.addQueryParameter("user", user);
		final SearchResult<ProductEnquiryModel> res = getFlexibleSearchService().search(query);
		return res.getResult();
	}

	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	/**
	 * @param flexibleSearchService
	 *           the flexibleSearchService to set
	 */
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}
}
