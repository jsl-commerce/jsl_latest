/**
 * 
 */
package com.jsl.core.product.enquiry.response.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author himanshu.sial
 *
 */

@XmlRootElement(name = "LeadCollection")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductEnquiryResponseDto
{

	@XmlElement(name = "Lead")
	private LeadResponseDto leadResponse;

	/**
	 * @return the leadResponse
	 */
	public LeadResponseDto getLeadResponse()
	{
		return leadResponse;
	}

	/**
	 * @param leadResponse the leadResponse to set
	 */
	public void setLeadResponse(LeadResponseDto leadResponse)
	{
		this.leadResponse = leadResponse;
	}
	
	
}
