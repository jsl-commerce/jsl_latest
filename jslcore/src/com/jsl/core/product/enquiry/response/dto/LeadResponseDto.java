/**
 * 
 */
package com.jsl.core.product.enquiry.response.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author himanshu.sial
 *
 */

@XmlRootElement(name = "Lead")
@XmlAccessorType(XmlAccessType.FIELD)
public class LeadResponseDto
{

	@XmlElement(name = "GroupCodeText")
	private String groupCodeText;
	@XmlElement(name = "ZTotalOrderQuantityLeadcontent")
	private String totalOrderQuantity;
	@XmlElement(name = "NameLanguageCodeText")
	private String language;
	@XmlElement(name = "SalesTerritoryUUID")
	private String salesTerritoryUUID;
	@XmlElement(name = "ContactFacsimileFormattedNumberDescription")
	private String contactFacsimileFormattedNumberDescription;
	@XmlElement(name = "AccountLegalForm")
	private String accountLegalForm;
	@XmlElement(name = "IndividualCustomerAddressCity")
	private String individualCustomerAddressCity;
	@XmlElement(name = "AccountPostalAddressElementsStreetPostalCode")
	private String accountPostalAddressElementsStreetPostalCode;
	@XmlElement(name = "AccountCity")
	private String accountCity;
	@XmlElement(name = "IndividualCustomerPhone")
	private String individualCustomerPhone;
	@XmlElement(name = "AccountLatitudeMeasure")
	private String accountLatitudeMeasure;
	@XmlElement(name = "CreatedBy")
	private String createdBy;
	@XmlElement(name = "ContactAllowedCode")
	private String contactAllowedCode;
	@XmlElement(name = "PriorityCodeText")
	private String priorityCodeText;
	@XmlElement(name = "AccountPartyUUID")
	private String accountPartyUUID;
	@XmlElement(name = "MarketingUnitPartyUUID")
	private String marketingUnitPartyUUID;
	@XmlElement(name = "ZAttachmentIcon")
	private String attachmentIcon;
	@XmlElement(name = "IndividualCustomerMaritalStatusCode")
	private String individualCustomerMaritalStatusCode;
	@XmlElement(name = "UserStatusCodeText")
	private String userStatusCodeText;
	@XmlElement(name = "AccountLifeCycleStatusCodeText")
	private String accountLifeCycleStatusCodeText;
	@XmlElement(name = "ContactMaritalStatusCode")
	private String contactMaritalStatusCode;
	@XmlElement(name = "Note")
	private String note;
	@XmlElement(name = "EmployeeResponsibleUUID")
	private String employeeResponsibleUUID;
	@XmlElement(name = "MainContactLifeCycleStatusCodeText")
	private String mainContactLifeCycleStatusCodeText;
	@XmlElement(name = "CompanyFourthName")
	private String companyFourthName;
	@XmlElement(name = "AccountMobile")
	private String accountMobile;
	@XmlElement(name = "IndividualCustomerContactAllowedCode")
	private String individualCustomerContactAllowedCode;
	@XmlElement(name = "LastChangeIdentityUUID")
	private String lastChangeIdentityUUID;
	@XmlElement(name = "ContactPhone")
	private String contactPhone;
	@XmlElement(name = "CreationIdentityUUID")
	private String creationIdentityUUID;
	@XmlElement(name = "OwnerUUIDSales")
	private String ownerUUIDSales;
	@XmlElement(name = "SalesUnitName")
	private String salesUnitName;
	@XmlElement(name = "ContactFirstName")
	private String contactFirstName;
	@XmlElement(name = "ZApproximateValuecurrencyCode")
	private String approximateValuecurrencyCode;
	@XmlElement(name = "ZTotalMScontent")
	private String totalMScontent;
	@XmlElement(name = "ContactUUID")
	private String contactUUID;
	@XmlElement(name = "OrganisationAccountIndustrialSectorCode")
	private String organisationAccountIndustrialSectorCode;
	@XmlElement(name = "IndividualCustomerGenderCode")
	private String individualCustomerGenderCode;
	@XmlElement(name = "ExpectedRevenueCurrencyCodeText")
	private String expectedRevenueCurrencyCodeText;
	@XmlElement(name = "EndDate")
	private String endDate;
	@XmlElement(name = "CampaignID")
	private String campaignID;
	@XmlElement(name = "OwnerSalesName")
	private String ownerSalesName;
	@XmlElement(name = "ExpectedRevenueAmount")
	private String expectedRevenueAmount;
	@XmlElement(name = "AccountPartyID")
	private String accountPartyID;
	@XmlElement(name = "LastChangeDateTime")
	private String lastChangeDateTime;
	@XmlElement(name = "ContactEMailUsageDeniedIndicator")
	private String contactEMailUsageDeniedIndicator;
	@XmlElement(name = "AccountPostalAddressElementsStreetSufix")
	private String accountPostalAddressElementsStreetSufix;
	@XmlElement(name = "OwnerPartyID")
	private String ownerPartyID;
	@XmlElement(name = "Name")
	private String name;
	@XmlElement(name = "MarketingName")
	private String marketingName;
	@XmlElement(name = "AccountNote")
	private String accountNote;
	@XmlElement(name = "IndividualCustomerNamePrefixCode")
	private String individualCustomerNamePrefixCode;
	@XmlElement(name = "ApprovalStatusCodeText")
	private String approvalStatusCodeText;
	@XmlElement(name = "IndividualCustomerAddressPostalCode")
	private String individualCustomerAddressPostalCode;
	@XmlElement(name = "ZTotalOrderQuantityLeadunitCode")
	private String totalOrderQuantityLeadunitCode;
	@XmlElement(name = "AccountDUNS")
	private String accountDUNS;
	@XmlElement(name = "zsetteamInd")
	private String zsetteamInd;
	@XmlElement(name = "ZApproximateValuecurrencyCodeText")
	private String approximateValuecurrencyCodeText;
	@XmlElement(name = "ZLeadDocTypeText")
	private String leadDocTypeText;
	@XmlElement(name = "AccountPostalAddressElementsAdditionalStreetPrefixName")
	private String accountPostalAddressElementsAdditionalStreetPrefixName;
	@XmlElement(name = "ID")
	private String leadId;
	@XmlElement(name = "IndividualCustomerInitialsName")
	private String individualCustomerInitialsName;
	@XmlElement(name = "SalesOrganisationID")
	private String salesOrganisationID;
	@XmlElement(name = "ZTotalMSunitCode")
	private String totalMSunitCode;
	@XmlElement(name = "IndividualCustomerAddressCountyName")
	private String individualCustomerAddressCountyName;
	@XmlElement(name = "ConsistencyStatusCode")
	private String consistencyStatusCode;
	@XmlElement(name = "AccountState")
	private String accountState;
	@XmlElement(name = "Score")
	private String score;
	@XmlElement(name = "ZExp_DomLText")
	private String enquiryTypeText;
	@XmlElement(name = "ZTotalSSunitCodeText")
	private String totalSSunitCodeText;
	@XmlElement(name = "SalesOrganisationUUID")
	private String salesOrganisationUUID;
	@XmlElement(name = "IndividualCustomerFax")
	private String individualCustomerFax;
	@XmlElement(name = "QualificationLevelCode")
	private String qualificationLevelCode;
	@XmlElement(name = "OriginTypeCodeText")
	private String originTypeCodeText;
	@XmlElement(name = "AccountPostalAddressElementsCountyName")
	private String accountPostalAddressElementsCountyName;
	@XmlElement(name = "OwnerPartyUUID")
	private String ownerPartyUUID;
	@XmlElement(name = "ContactNickName")
	private String contactNickName;
	@XmlElement(name = "SalesOfficeID")
	private String salesOfficeID;
	@XmlElement(name = "ZDontSave")
	private String dontSave;
	@XmlElement(name = "AccountCountry")
	private String accountCountry;
	@XmlElement(name = "ZExp_DomL")
	private String enquiryTypeId;
	@XmlElement(name = "AccountPostalAddressElementsHouseID")
	private String accountPostalAddressElementsHouseID;
	@XmlElement(name = "IndividualCustomerMiddleName")
	private String individualCustomerMiddleName;
	@XmlElement(name = "SalesGroupUUID")
	private String salesGroupUUID;
	@XmlElement(name = "CreationDateTime")
	private String creationDateTime;
	@XmlElement(name = "ZRepeatCount")
	private String repeatCount;
	@XmlElement(name = "ZSalesDistrictLcontentText")
	private String salesDistrictLcontentText;
	@XmlElement(name = "DistributionChannelCode")
	private String distributionChannelCode;
	@XmlElement(name = "AccountEMail")
	private String accountEMail;
	@XmlElement(name = "MarketingUnitPartyID")
	private String marketingUnitPartyID;
	@XmlElement(name = "BusinessPartnerRelationshipBusinessPartnerFunctionTypeCode")
	private String businessPartnerRelationshipBusinessPartnerFunctionTypeCode;
	@XmlElement(name = "ContactAcademicTitleCode")
	private String contactAcademicTitleCode;
	@XmlElement(name = "DivisionCodeText")
	private String divisionCodeText;
	@XmlElement(name = "ApprovalStatusCode")
	private String approvalStatusCode;
	@XmlElement(name = "DocTypeEdit")
	private String docTypeEdit;
	@XmlElement(name = "CompanySecondName")
	private String companySecondName;
	@XmlElement(name = "OriginTypeCode")
	private String originTypeCode;
	@XmlElement(name = "ZSaveCounter")
	private String saveCounter;
	@XmlElement(name = "IndividualCustomerAddressHouseID")
	private String individualCustomerAddressHouseID;
	@XmlElement(name = "ContactMobile")
	private String contactMobile;
	@XmlElement(name = "SurveyTotalScoreValue")
	private String surveyTotalScoreValue;
	@XmlElement(name = "OwnerPartyIDSales")
	private String ownerPartyIDSales;
	@XmlElement(name = "ContactRoomID")
	private String contactRoomID;
	@XmlElement(name = "ContactBuildingID")
	private String contactBuildingID;
	@XmlElement(name = "OrganisationAccountContactAllowedCode")
	private String organisationAccountContactAllowedCode;
	@XmlElement(name = "ZExternal_Lead_ID")
	private String externalLeadId;
	@XmlElement(name = "ExpectedRevenueCurrencyCode")
	private String expectedRevenueCurrencyCode;
	@XmlElement(name = "ContactGenderCode")
	private String contactGenderCode;
	@XmlElement(name = "ContactLastName")
	private String contactLastName;
	@XmlElement(name = "AccountPartyName")
	private String accountPartyName;
	@XmlElement(name = "ETag")
	private String eTag;
	@XmlElement(name = "ContactFunctionalTitleName")
	private String contactFunctionalTitleName;
	@XmlElement(name = "IndividualCustomerMobile")
	private String individualCustomerMobile;
	@XmlElement(name = "DistributionChannelCodeText")
	private String distributionChannelCodeText;
	@XmlElement(name = "IndividualCustomerAddressCountry")
	private String individualCustomerAddressCountry;
	@XmlElement(name = "AccountFax")
	private String accountFax;
	@XmlElement(name = "IndividualCustomerAddressStreetName")
	private String individualCustomerAddressStreetName;
	@XmlElement(name = "AccountCountryText")
	private String accountCountryText;
	@XmlElement(name = "IndividualCustomerOccupationCode")
	private String individualCustomerOccupationCode;
	@XmlElement(name = "ConsistencyStatusCodeText")
	private String consistencyStatusCodeText;
	@XmlElement(name = "AccountPreferredCommunicationMediumTypeCode")
	private String accountPreferredCommunicationMediumTypeCode;
	@XmlElement(name = "IndividualCustomerAddressState")
	private String individualCustomerAddressState;
	@XmlElement(name = "ZApproximateValuecontent")
	private String approximateValuecontent;
	@XmlElement(name = "IndividualCustomerAcademicTitleCode")
	private String individualCustomerAcademicTitleCode;
	@XmlElement(name = "PriorityCode")
	private String priorityCode;
	@XmlElement(name = "ContactDepartmentName")
	private String contactDepartmentName;
	@XmlElement(name = "ZCopyInd")
	private String copyInd;
	@XmlElement(name = "UserStatusCode")
	private String userStatusCode;
	@XmlElement(name = "DivisionCode")
	private String divisionCode;
	@XmlElement(name = "ZTotalMSunitCodeText")
	private String totalMSunitCodeText;
	@XmlElement(name = "BusinessPartnerRelationshipBusinessPartnerFunctionalAreaCode")
	private String businessPartnerRelationshipBusinessPartnerFunctionalAreaCode;
	@XmlElement(name = "ResultReasonCode")
	private String resultReasonCode;
	@XmlElement(name = "MainContactLifeCycleStatusCode")
	private String mainContactLifeCycleStatusCode;
	@XmlElement(name = "SalesGroupID")
	private String salesGroupID;
	@XmlElement(name = "ZTotalOrderQuantityLeadunitCodeText")
	private String totalOrderQuantityLeadunitCodeText;
	@XmlElement(name = "ZTotalSSunitCode")
	private String totalSSunitCode;
	@XmlElement(name = "CompanyThirdName")
	private String companyThirdName;
	@XmlElement(name = "ContactMiddleName")
	private String contactMiddleName;
	@XmlElement(name = "AccountPhone")
	private String accountPhone;
	@XmlElement(name = "ZSalesDistrictLcontent")
	private String salesDistrictLcontent;
	@XmlElement(name = "IndividualCustomerEMail")
	private String individualCustomerEMail;
	@XmlElement(name = "ZSaveInd")
	private String saveInd;
	@XmlElement(name = "AccountPostalAddressElementsStreetName")
	private String accountPostalAddressElementsStreetName;
	@XmlElement(name = "SalesTerritoryName")
	private String salesTerritoryName;
	@XmlElement(name = "ContactFormOfAddressCode")
	private String contactFormOfAddressCode;
	@XmlElement(name = "AccountPostalAddressElementsStreetPrefix")
	private String accountPostalAddressElementsStreetPrefix;
	@XmlElement(name = "NameLanguageCode")
	private String nameLanguageCode;
	@XmlElement(name = "IndividualCustomerBirthDate")
	private String individualCustomerBirthDate;
	@XmlElement(name = "ContactName")
	private String contactName;
	@XmlElement(name = "LastChangedBy")
	private String lastChangedBy;
	@XmlElement(name = "AccountCorrespondenceLanguageCode")
	private String accountCorrespondenceLanguageCode;
	@XmlElement(name = "Company")
	private String company;
	@XmlElement(name = "SalesOfficeUUID")
	private String salesOfficeUUID;
	@XmlElement(name = "AccountStateText")
	private String accountStateText;
	@XmlElement(name = "ObjectID")
	private String objectID;
	@XmlElement(name = "QualificationLevelCodeText")
	private String qualificationLevelCodeText;
	@XmlElement(name = "ZTotalSScontent")
	private String totalSScontent;
	@XmlElement(name = "IndividualCustomerFormOfAddressCode")
	private String individualCustomerFormOfAddressCode;
	@XmlElement(name = "StartDate")
	private String startDate;
	@XmlElement(name = "ZUnique_IDL")
	private String uniqueIDL;
	@XmlElement(name = "ContactCorrespondenceLanguageCode")
	private String contactCorrespondenceLanguageCode;
	@XmlElement(name = "LifeCycleStatusCode")
	private String lifeCycleStatusCode;
	@XmlElement(name = "ZTeamLead")
	private String teamLead;
	@XmlElement(name = "OwnerPartyName")
	private String ownerPartyName;
	@XmlElement(name = "IndividualCustomerNonVerbalCommunicationLanguageCode")
	private String individualCustomerNonVerbalCommunicationLanguageCode;
	@XmlElement(name = "ZProdAvailInd")
	private String prodAvailInd;
	@XmlElement(name = "IndividualCustomerGivenName")
	private String individualCustomerGivenName;
	@XmlElement(name = "OrganisationAccountABCClassificationCode")
	private String organisationAccountABCClassificationCode;
	@XmlElement(name = "ContactFloorID")
	private String contactFloorID;
	@XmlElement(name = "IndividualCustomerABCClassificationCode")
	private String individualCustomerABCClassificationCode;
	@XmlElement(name = "BusinessPartnerRelationshipContactVIPReasonCode")
	private String businessPartnerRelationshipContactVIPReasonCode;
	@XmlElement(name = "AccountLongitudeMeasure")
	private String accountLongitudeMeasure;
	@XmlElement(name = "IndividualCustomerFamilyName")
	private String individualCustomerFamilyName;
	@XmlElement(name = "SalesUnitPartyUUID")
	private String salesUnitPartyUUID;
	@XmlElement(name = "ZLeadDocType")
	private String leadDocType;
	@XmlElement(name = "ContactAdditionalAcademicTitleCode")
	private String contactAdditionalAcademicTitleCode;
	@XmlElement(name = "ContactNote")
	private String contactNote;
	@XmlElement(name = "IndividualCustomerNationalityCountryCode")
	private String individualCustomerNationalityCountryCode;
	@XmlElement(name = "AccountWebsite")
	private String accountWebsite;
	@XmlElement(name = "ExternalID")
	private String externalID;
	@XmlElement(name = "teamLeadText")
	private String ZTeamLeadText;
	@XmlElement(name = "LifeCycleStatusCodeText")
	private String lifeCycleStatusCodeText;
	@XmlElement(name = "AccountPostalAddressElementsAdditionalStreetSuffixName")
	private String accountPostalAddressElementsAdditionalStreetSuffixName;
	@XmlElement(name = "SalesUnitPartyID")
	private String salesUnitPartyID;
	@XmlElement(name = "AccountLatitudeMeasureUnitCode")
	private String accountLatitudeMeasureUnitCode;
	@XmlElement(name = "SalesTerritoryID")
	private String salesTerritoryID;
	@XmlElement(name = "BusinessPartnerRelationshipEngagementScoreNumberValue")
	private String businessPartnerRelationshipEngagementScoreNumberValue;
	@XmlElement(name = "AccountPostalAddressElementsPOBoxID")
	private String accountPostalAddressElementsPOBoxID;
	@XmlElement(name = "AccountLongitudeMeasureUnitCode")
	private String accountLongitudeMeasureUnitCode;
	@XmlElement(name = "ZExternal_Account_ID")
	private String customerId;
	@XmlElement(name = "GroupCode")
	private String groupCode;
	@XmlElement(name = "AccountLifeCycleStatusCode")
	private String accountLifeCycleStatusCode;
	@XmlElement(name = "ContactEMail")
	private String contactEMail;
	@XmlElement(name = "ContactID")
	private String contactID;
	@XmlElement(name = "ResultReasonCodeText")
	private String resultReasonCodeText;
	@XmlElement(name = "ZProdAdd")
	private String prodAdd;

	/**
	 * @return the groupCodeText
	 */
	public String getGroupCodeText()
	{
		return groupCodeText;
	}


	/**
	 * @param groupCodeText
	 *           the groupCodeText to set
	 */
	public void setGroupCodeText(String groupCodeText)
	{
		this.groupCodeText = groupCodeText;
	}


	/**
	 * @return the totalOrderQuantuty
	 */
	public String getTotalOrderQuantity()
	{
		return totalOrderQuantity;
	}


	/**
	 * @param totalOrderQuantuty
	 *           the totalOrderQuantuty to set
	 */
	public void setTotalOrderQuantity(String totalOrderQuantity)
	{
		this.totalOrderQuantity = totalOrderQuantity;
	}


	/**
	 * @return the language
	 */
	public String getLanguage()
	{
		return language;
	}


	/**
	 * @param language
	 *           the language to set
	 */
	public void setLanguage(String language)
	{
		this.language = language;
	}


	/**
	 * @return the salesTerritoryUUID
	 */
	public String getSalesTerritoryUUID()
	{
		return salesTerritoryUUID;
	}


	/**
	 * @param salesTerritoryUUID
	 *           the salesTerritoryUUID to set
	 */
	public void setSalesTerritoryUUID(String salesTerritoryUUID)
	{
		this.salesTerritoryUUID = salesTerritoryUUID;
	}


	/**
	 * @return the contactFacsimileFormattedNumberDescription
	 */
	public String getContactFacsimileFormattedNumberDescription()
	{
		return contactFacsimileFormattedNumberDescription;
	}


	/**
	 * @param contactFacsimileFormattedNumberDescription
	 *           the contactFacsimileFormattedNumberDescription to set
	 */
	public void setContactFacsimileFormattedNumberDescription(String contactFacsimileFormattedNumberDescription)
	{
		this.contactFacsimileFormattedNumberDescription = contactFacsimileFormattedNumberDescription;
	}


	/**
	 * @return the accountLegalForm
	 */
	public String getAccountLegalForm()
	{
		return accountLegalForm;
	}


	/**
	 * @param accountLegalForm
	 *           the accountLegalForm to set
	 */
	public void setAccountLegalForm(String accountLegalForm)
	{
		this.accountLegalForm = accountLegalForm;
	}


	/**
	 * @return the individualCustomerAddressCity
	 */
	public String getIndividualCustomerAddressCity()
	{
		return individualCustomerAddressCity;
	}


	/**
	 * @param individualCustomerAddressCity
	 *           the individualCustomerAddressCity to set
	 */
	public void setIndividualCustomerAddressCity(String individualCustomerAddressCity)
	{
		this.individualCustomerAddressCity = individualCustomerAddressCity;
	}


	/**
	 * @return the accountPostalAddressElementsStreetPostalCode
	 */
	public String getAccountPostalAddressElementsStreetPostalCode()
	{
		return accountPostalAddressElementsStreetPostalCode;
	}


	/**
	 * @param accountPostalAddressElementsStreetPostalCode
	 *           the accountPostalAddressElementsStreetPostalCode to set
	 */
	public void setAccountPostalAddressElementsStreetPostalCode(String accountPostalAddressElementsStreetPostalCode)
	{
		this.accountPostalAddressElementsStreetPostalCode = accountPostalAddressElementsStreetPostalCode;
	}


	/**
	 * @return the accountCity
	 */
	public String getAccountCity()
	{
		return accountCity;
	}


	/**
	 * @param accountCity
	 *           the accountCity to set
	 */
	public void setAccountCity(String accountCity)
	{
		this.accountCity = accountCity;
	}


	/**
	 * @return the individualCustomerPhone
	 */
	public String getIndividualCustomerPhone()
	{
		return individualCustomerPhone;
	}


	/**
	 * @param individualCustomerPhone
	 *           the individualCustomerPhone to set
	 */
	public void setIndividualCustomerPhone(String individualCustomerPhone)
	{
		this.individualCustomerPhone = individualCustomerPhone;
	}


	/**
	 * @return the accountLatitudeMeasure
	 */
	public String getAccountLatitudeMeasure()
	{
		return accountLatitudeMeasure;
	}


	/**
	 * @param accountLatitudeMeasure
	 *           the accountLatitudeMeasure to set
	 */
	public void setAccountLatitudeMeasure(String accountLatitudeMeasure)
	{
		this.accountLatitudeMeasure = accountLatitudeMeasure;
	}


	/**
	 * @return the createdBy
	 */
	public String getCreatedBy()
	{
		return createdBy;
	}


	/**
	 * @param createdBy
	 *           the createdBy to set
	 */
	public void setCreatedBy(String createdBy)
	{
		this.createdBy = createdBy;
	}


	/**
	 * @return the contactAllowedCode
	 */
	public String getContactAllowedCode()
	{
		return contactAllowedCode;
	}


	/**
	 * @param contactAllowedCode
	 *           the contactAllowedCode to set
	 */
	public void setContactAllowedCode(String contactAllowedCode)
	{
		this.contactAllowedCode = contactAllowedCode;
	}


	/**
	 * @return the priorityCodeText
	 */
	public String getPriorityCodeText()
	{
		return priorityCodeText;
	}


	/**
	 * @param priorityCodeText
	 *           the priorityCodeText to set
	 */
	public void setPriorityCodeText(String priorityCodeText)
	{
		this.priorityCodeText = priorityCodeText;
	}


	/**
	 * @return the accountPartyUUID
	 */
	public String getAccountPartyUUID()
	{
		return accountPartyUUID;
	}


	/**
	 * @param accountPartyUUID
	 *           the accountPartyUUID to set
	 */
	public void setAccountPartyUUID(String accountPartyUUID)
	{
		this.accountPartyUUID = accountPartyUUID;
	}


	/**
	 * @return the marketingUnitPartyUUID
	 */
	public String getMarketingUnitPartyUUID()
	{
		return marketingUnitPartyUUID;
	}


	/**
	 * @param marketingUnitPartyUUID
	 *           the marketingUnitPartyUUID to set
	 */
	public void setMarketingUnitPartyUUID(String marketingUnitPartyUUID)
	{
		this.marketingUnitPartyUUID = marketingUnitPartyUUID;
	}


	/**
	 * @return the attachmentIcon
	 */
	public String getAttachmentIcon()
	{
		return attachmentIcon;
	}


	/**
	 * @param attachmentIcon
	 *           the attachmentIcon to set
	 */
	public void setAttachmentIcon(String attachmentIcon)
	{
		this.attachmentIcon = attachmentIcon;
	}


	/**
	 * @return the individualCustomerMaritalStatusCode
	 */
	public String getIndividualCustomerMaritalStatusCode()
	{
		return individualCustomerMaritalStatusCode;
	}


	/**
	 * @param individualCustomerMaritalStatusCode
	 *           the individualCustomerMaritalStatusCode to set
	 */
	public void setIndividualCustomerMaritalStatusCode(String individualCustomerMaritalStatusCode)
	{
		this.individualCustomerMaritalStatusCode = individualCustomerMaritalStatusCode;
	}


	/**
	 * @return the userStatusCodeText
	 */
	public String getUserStatusCodeText()
	{
		return userStatusCodeText;
	}


	/**
	 * @param userStatusCodeText
	 *           the userStatusCodeText to set
	 */
	public void setUserStatusCodeText(String userStatusCodeText)
	{
		this.userStatusCodeText = userStatusCodeText;
	}


	/**
	 * @return the accountLifeCycleStatusCodeText
	 */
	public String getAccountLifeCycleStatusCodeText()
	{
		return accountLifeCycleStatusCodeText;
	}


	/**
	 * @param accountLifeCycleStatusCodeText
	 *           the accountLifeCycleStatusCodeText to set
	 */
	public void setAccountLifeCycleStatusCodeText(String accountLifeCycleStatusCodeText)
	{
		this.accountLifeCycleStatusCodeText = accountLifeCycleStatusCodeText;
	}


	/**
	 * @return the contactMaritalStatusCode
	 */
	public String getContactMaritalStatusCode()
	{
		return contactMaritalStatusCode;
	}


	/**
	 * @param contactMaritalStatusCode
	 *           the contactMaritalStatusCode to set
	 */
	public void setContactMaritalStatusCode(String contactMaritalStatusCode)
	{
		this.contactMaritalStatusCode = contactMaritalStatusCode;
	}


	/**
	 * @return the note
	 */
	public String getNote()
	{
		return note;
	}


	/**
	 * @param note
	 *           the note to set
	 */
	public void setNote(String note)
	{
		this.note = note;
	}


	/**
	 * @return the employeeResponsibleUUID
	 */
	public String getEmployeeResponsibleUUID()
	{
		return employeeResponsibleUUID;
	}


	/**
	 * @param employeeResponsibleUUID
	 *           the employeeResponsibleUUID to set
	 */
	public void setEmployeeResponsibleUUID(String employeeResponsibleUUID)
	{
		this.employeeResponsibleUUID = employeeResponsibleUUID;
	}


	/**
	 * @return the mainContactLifeCycleStatusCodeText
	 */
	public String getMainContactLifeCycleStatusCodeText()
	{
		return mainContactLifeCycleStatusCodeText;
	}


	/**
	 * @param mainContactLifeCycleStatusCodeText
	 *           the mainContactLifeCycleStatusCodeText to set
	 */
	public void setMainContactLifeCycleStatusCodeText(String mainContactLifeCycleStatusCodeText)
	{
		this.mainContactLifeCycleStatusCodeText = mainContactLifeCycleStatusCodeText;
	}


	/**
	 * @return the companyFourthName
	 */
	public String getCompanyFourthName()
	{
		return companyFourthName;
	}


	/**
	 * @param companyFourthName
	 *           the companyFourthName to set
	 */
	public void setCompanyFourthName(String companyFourthName)
	{
		this.companyFourthName = companyFourthName;
	}


	/**
	 * @return the accountMobile
	 */
	public String getAccountMobile()
	{
		return accountMobile;
	}


	/**
	 * @param accountMobile
	 *           the accountMobile to set
	 */
	public void setAccountMobile(String accountMobile)
	{
		this.accountMobile = accountMobile;
	}


	/**
	 * @return the individualCustomerContactAllowedCode
	 */
	public String getIndividualCustomerContactAllowedCode()
	{
		return individualCustomerContactAllowedCode;
	}


	/**
	 * @param individualCustomerContactAllowedCode
	 *           the individualCustomerContactAllowedCode to set
	 */
	public void setIndividualCustomerContactAllowedCode(String individualCustomerContactAllowedCode)
	{
		this.individualCustomerContactAllowedCode = individualCustomerContactAllowedCode;
	}


	/**
	 * @return the lastChangeIdentityUUID
	 */
	public String getLastChangeIdentityUUID()
	{
		return lastChangeIdentityUUID;
	}


	/**
	 * @param lastChangeIdentityUUID
	 *           the lastChangeIdentityUUID to set
	 */
	public void setLastChangeIdentityUUID(String lastChangeIdentityUUID)
	{
		this.lastChangeIdentityUUID = lastChangeIdentityUUID;
	}


	/**
	 * @return the contactPhone
	 */
	public String getContactPhone()
	{
		return contactPhone;
	}


	/**
	 * @param contactPhone
	 *           the contactPhone to set
	 */
	public void setContactPhone(String contactPhone)
	{
		this.contactPhone = contactPhone;
	}


	/**
	 * @return the creationIdentityUUID
	 */
	public String getCreationIdentityUUID()
	{
		return creationIdentityUUID;
	}


	/**
	 * @param creationIdentityUUID
	 *           the creationIdentityUUID to set
	 */
	public void setCreationIdentityUUID(String creationIdentityUUID)
	{
		this.creationIdentityUUID = creationIdentityUUID;
	}


	/**
	 * @return the ownerUUIDSales
	 */
	public String getOwnerUUIDSales()
	{
		return ownerUUIDSales;
	}


	/**
	 * @param ownerUUIDSales
	 *           the ownerUUIDSales to set
	 */
	public void setOwnerUUIDSales(String ownerUUIDSales)
	{
		this.ownerUUIDSales = ownerUUIDSales;
	}


	/**
	 * @return the salesUnitName
	 */
	public String getSalesUnitName()
	{
		return salesUnitName;
	}


	/**
	 * @param salesUnitName
	 *           the salesUnitName to set
	 */
	public void setSalesUnitName(String salesUnitName)
	{
		this.salesUnitName = salesUnitName;
	}


	/**
	 * @return the contactFirstName
	 */
	public String getContactFirstName()
	{
		return contactFirstName;
	}


	/**
	 * @param contactFirstName
	 *           the contactFirstName to set
	 */
	public void setContactFirstName(String contactFirstName)
	{
		this.contactFirstName = contactFirstName;
	}


	/**
	 * @return the approximateValuecurrencyCode
	 */
	public String getApproximateValuecurrencyCode()
	{
		return approximateValuecurrencyCode;
	}


	/**
	 * @param approximateValuecurrencyCode
	 *           the approximateValuecurrencyCode to set
	 */
	public void setApproximateValuecurrencyCode(String approximateValuecurrencyCode)
	{
		this.approximateValuecurrencyCode = approximateValuecurrencyCode;
	}


	/**
	 * @return the totalMScontent
	 */
	public String getTotalMScontent()
	{
		return totalMScontent;
	}


	/**
	 * @param totalMScontent
	 *           the totalMScontent to set
	 */
	public void setTotalMScontent(String totalMScontent)
	{
		this.totalMScontent = totalMScontent;
	}


	/**
	 * @return the contactUUID
	 */
	public String getContactUUID()
	{
		return contactUUID;
	}


	/**
	 * @param contactUUID
	 *           the contactUUID to set
	 */
	public void setContactUUID(String contactUUID)
	{
		this.contactUUID = contactUUID;
	}


	/**
	 * @return the organisationAccountIndustrialSectorCode
	 */
	public String getOrganisationAccountIndustrialSectorCode()
	{
		return organisationAccountIndustrialSectorCode;
	}


	/**
	 * @param organisationAccountIndustrialSectorCode
	 *           the organisationAccountIndustrialSectorCode to set
	 */
	public void setOrganisationAccountIndustrialSectorCode(String organisationAccountIndustrialSectorCode)
	{
		this.organisationAccountIndustrialSectorCode = organisationAccountIndustrialSectorCode;
	}


	/**
	 * @return the individualCustomerGenderCode
	 */
	public String getIndividualCustomerGenderCode()
	{
		return individualCustomerGenderCode;
	}


	/**
	 * @param individualCustomerGenderCode
	 *           the individualCustomerGenderCode to set
	 */
	public void setIndividualCustomerGenderCode(String individualCustomerGenderCode)
	{
		this.individualCustomerGenderCode = individualCustomerGenderCode;
	}


	/**
	 * @return the expectedRevenueCurrencyCodeText
	 */
	public String getExpectedRevenueCurrencyCodeText()
	{
		return expectedRevenueCurrencyCodeText;
	}


	/**
	 * @param expectedRevenueCurrencyCodeText
	 *           the expectedRevenueCurrencyCodeText to set
	 */
	public void setExpectedRevenueCurrencyCodeText(String expectedRevenueCurrencyCodeText)
	{
		this.expectedRevenueCurrencyCodeText = expectedRevenueCurrencyCodeText;
	}


	/**
	 * @return the endDate
	 */
	public String getEndDate()
	{
		return endDate;
	}


	/**
	 * @param endDate
	 *           the endDate to set
	 */
	public void setEndDate(String endDate)
	{
		this.endDate = endDate;
	}


	/**
	 * @return the campaignID
	 */
	public String getCampaignID()
	{
		return campaignID;
	}


	/**
	 * @param campaignID
	 *           the campaignID to set
	 */
	public void setCampaignID(String campaignID)
	{
		this.campaignID = campaignID;
	}


	/**
	 * @return the ownerSalesName
	 */
	public String getOwnerSalesName()
	{
		return ownerSalesName;
	}


	/**
	 * @param ownerSalesName
	 *           the ownerSalesName to set
	 */
	public void setOwnerSalesName(String ownerSalesName)
	{
		this.ownerSalesName = ownerSalesName;
	}


	/**
	 * @return the expectedRevenueAmount
	 */
	public String getExpectedRevenueAmount()
	{
		return expectedRevenueAmount;
	}


	/**
	 * @param expectedRevenueAmount
	 *           the expectedRevenueAmount to set
	 */
	public void setExpectedRevenueAmount(String expectedRevenueAmount)
	{
		this.expectedRevenueAmount = expectedRevenueAmount;
	}


	/**
	 * @return the accountPartyID
	 */
	public String getAccountPartyID()
	{
		return accountPartyID;
	}


	/**
	 * @param accountPartyID
	 *           the accountPartyID to set
	 */
	public void setAccountPartyID(String accountPartyID)
	{
		this.accountPartyID = accountPartyID;
	}


	/**
	 * @return the lastChangeDateTime
	 */
	public String getLastChangeDateTime()
	{
		return lastChangeDateTime;
	}


	/**
	 * @param lastChangeDateTime
	 *           the lastChangeDateTime to set
	 */
	public void setLastChangeDateTime(String lastChangeDateTime)
	{
		this.lastChangeDateTime = lastChangeDateTime;
	}


	/**
	 * @return the contactEMailUsageDeniedIndicator
	 */
	public String getContactEMailUsageDeniedIndicator()
	{
		return contactEMailUsageDeniedIndicator;
	}


	/**
	 * @param contactEMailUsageDeniedIndicator
	 *           the contactEMailUsageDeniedIndicator to set
	 */
	public void setContactEMailUsageDeniedIndicator(String contactEMailUsageDeniedIndicator)
	{
		this.contactEMailUsageDeniedIndicator = contactEMailUsageDeniedIndicator;
	}


	/**
	 * @return the accountPostalAddressElementsStreetSufix
	 */
	public String getAccountPostalAddressElementsStreetSufix()
	{
		return accountPostalAddressElementsStreetSufix;
	}


	/**
	 * @param accountPostalAddressElementsStreetSufix
	 *           the accountPostalAddressElementsStreetSufix to set
	 */
	public void setAccountPostalAddressElementsStreetSufix(String accountPostalAddressElementsStreetSufix)
	{
		this.accountPostalAddressElementsStreetSufix = accountPostalAddressElementsStreetSufix;
	}


	/**
	 * @return the ownerPartyID
	 */
	public String getOwnerPartyID()
	{
		return ownerPartyID;
	}


	/**
	 * @param ownerPartyID
	 *           the ownerPartyID to set
	 */
	public void setOwnerPartyID(String ownerPartyID)
	{
		this.ownerPartyID = ownerPartyID;
	}


	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}


	/**
	 * @param name
	 *           the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}


	/**
	 * @return the marketingName
	 */
	public String getMarketingName()
	{
		return marketingName;
	}


	/**
	 * @param marketingName
	 *           the marketingName to set
	 */
	public void setMarketingName(String marketingName)
	{
		this.marketingName = marketingName;
	}


	/**
	 * @return the accountNote
	 */
	public String getAccountNote()
	{
		return accountNote;
	}


	/**
	 * @param accountNote
	 *           the accountNote to set
	 */
	public void setAccountNote(String accountNote)
	{
		this.accountNote = accountNote;
	}


	/**
	 * @return the individualCustomerNamePrefixCode
	 */
	public String getIndividualCustomerNamePrefixCode()
	{
		return individualCustomerNamePrefixCode;
	}


	/**
	 * @param individualCustomerNamePrefixCode
	 *           the individualCustomerNamePrefixCode to set
	 */
	public void setIndividualCustomerNamePrefixCode(String individualCustomerNamePrefixCode)
	{
		this.individualCustomerNamePrefixCode = individualCustomerNamePrefixCode;
	}


	/**
	 * @return the approvalStatusCodeText
	 */
	public String getApprovalStatusCodeText()
	{
		return approvalStatusCodeText;
	}


	/**
	 * @param approvalStatusCodeText
	 *           the approvalStatusCodeText to set
	 */
	public void setApprovalStatusCodeText(String approvalStatusCodeText)
	{
		this.approvalStatusCodeText = approvalStatusCodeText;
	}


	/**
	 * @return the individualCustomerAddressPostalCode
	 */
	public String getIndividualCustomerAddressPostalCode()
	{
		return individualCustomerAddressPostalCode;
	}


	/**
	 * @param individualCustomerAddressPostalCode
	 *           the individualCustomerAddressPostalCode to set
	 */
	public void setIndividualCustomerAddressPostalCode(String individualCustomerAddressPostalCode)
	{
		this.individualCustomerAddressPostalCode = individualCustomerAddressPostalCode;
	}


	/**
	 * @return the totalOrderQuantityLeadunitCode
	 */
	public String getTotalOrderQuantityLeadunitCode()
	{
		return totalOrderQuantityLeadunitCode;
	}


	/**
	 * @param totalOrderQuantityLeadunitCode
	 *           the totalOrderQuantityLeadunitCode to set
	 */
	public void setTotalOrderQuantityLeadunitCode(String totalOrderQuantityLeadunitCode)
	{
		this.totalOrderQuantityLeadunitCode = totalOrderQuantityLeadunitCode;
	}


	/**
	 * @return the accountDUNS
	 */
	public String getAccountDUNS()
	{
		return accountDUNS;
	}


	/**
	 * @param accountDUNS
	 *           the accountDUNS to set
	 */
	public void setAccountDUNS(String accountDUNS)
	{
		this.accountDUNS = accountDUNS;
	}


	/**
	 * @return the zsetteamInd
	 */
	public String getZsetteamInd()
	{
		return zsetteamInd;
	}


	/**
	 * @param zsetteamInd
	 *           the zsetteamInd to set
	 */
	public void setZsetteamInd(String zsetteamInd)
	{
		this.zsetteamInd = zsetteamInd;
	}


	/**
	 * @return the approximateValuecurrencyCodeText
	 */
	public String getApproximateValuecurrencyCodeText()
	{
		return approximateValuecurrencyCodeText;
	}


	/**
	 * @param approximateValuecurrencyCodeText
	 *           the approximateValuecurrencyCodeText to set
	 */
	public void setApproximateValuecurrencyCodeText(String approximateValuecurrencyCodeText)
	{
		this.approximateValuecurrencyCodeText = approximateValuecurrencyCodeText;
	}


	/**
	 * @return the leadDocTypeText
	 */
	public String getLeadDocTypeText()
	{
		return leadDocTypeText;
	}


	/**
	 * @param leadDocTypeText
	 *           the leadDocTypeText to set
	 */
	public void setLeadDocTypeText(String leadDocTypeText)
	{
		this.leadDocTypeText = leadDocTypeText;
	}


	/**
	 * @return the accountPostalAddressElementsAdditionalStreetPrefixName
	 */
	public String getAccountPostalAddressElementsAdditionalStreetPrefixName()
	{
		return accountPostalAddressElementsAdditionalStreetPrefixName;
	}


	/**
	 * @param accountPostalAddressElementsAdditionalStreetPrefixName
	 *           the accountPostalAddressElementsAdditionalStreetPrefixName to set
	 */
	public void setAccountPostalAddressElementsAdditionalStreetPrefixName(
			String accountPostalAddressElementsAdditionalStreetPrefixName)
	{
		this.accountPostalAddressElementsAdditionalStreetPrefixName = accountPostalAddressElementsAdditionalStreetPrefixName;
	}


	/**
	 * @return the leadId
	 */
	public String getLeadId()
	{
		return leadId;
	}


	/**
	 * @param leadId
	 *           the leadId to set
	 */
	public void setLeadId(String leadId)
	{
		this.leadId = leadId;
	}


	/**
	 * @return the individualCustomerInitialsName
	 */
	public String getIndividualCustomerInitialsName()
	{
		return individualCustomerInitialsName;
	}


	/**
	 * @param individualCustomerInitialsName
	 *           the individualCustomerInitialsName to set
	 */
	public void setIndividualCustomerInitialsName(String individualCustomerInitialsName)
	{
		this.individualCustomerInitialsName = individualCustomerInitialsName;
	}


	/**
	 * @return the salesOrganisationID
	 */
	public String getSalesOrganisationID()
	{
		return salesOrganisationID;
	}


	/**
	 * @param salesOrganisationID
	 *           the salesOrganisationID to set
	 */
	public void setSalesOrganisationID(String salesOrganisationID)
	{
		this.salesOrganisationID = salesOrganisationID;
	}


	/**
	 * @return the totalMSunitCode
	 */
	public String getTotalMSunitCode()
	{
		return totalMSunitCode;
	}


	/**
	 * @param totalMSunitCode
	 *           the totalMSunitCode to set
	 */
	public void setTotalMSunitCode(String totalMSunitCode)
	{
		this.totalMSunitCode = totalMSunitCode;
	}


	/**
	 * @return the individualCustomerAddressCountyName
	 */
	public String getIndividualCustomerAddressCountyName()
	{
		return individualCustomerAddressCountyName;
	}


	/**
	 * @param individualCustomerAddressCountyName
	 *           the individualCustomerAddressCountyName to set
	 */
	public void setIndividualCustomerAddressCountyName(String individualCustomerAddressCountyName)
	{
		this.individualCustomerAddressCountyName = individualCustomerAddressCountyName;
	}


	/**
	 * @return the consistencyStatusCode
	 */
	public String getConsistencyStatusCode()
	{
		return consistencyStatusCode;
	}


	/**
	 * @param consistencyStatusCode
	 *           the consistencyStatusCode to set
	 */
	public void setConsistencyStatusCode(String consistencyStatusCode)
	{
		this.consistencyStatusCode = consistencyStatusCode;
	}


	/**
	 * @return the accountState
	 */
	public String getAccountState()
	{
		return accountState;
	}


	/**
	 * @param accountState
	 *           the accountState to set
	 */
	public void setAccountState(String accountState)
	{
		this.accountState = accountState;
	}


	/**
	 * @return the score
	 */
	public String getScore()
	{
		return score;
	}


	/**
	 * @param score
	 *           the score to set
	 */
	public void setScore(String score)
	{
		this.score = score;
	}


	/**
	 * @return the enquiryTypeText
	 */
	public String getEnquiryTypeText()
	{
		return enquiryTypeText;
	}


	/**
	 * @param enquiryTypeText
	 *           the enquiryTypeText to set
	 */
	public void setEnquiryTypeText(String enquiryTypeText)
	{
		this.enquiryTypeText = enquiryTypeText;
	}


	/**
	 * @return the totalSSunitCodeText
	 */
	public String getTotalSSunitCodeText()
	{
		return totalSSunitCodeText;
	}


	/**
	 * @param totalSSunitCodeText
	 *           the totalSSunitCodeText to set
	 */
	public void setTotalSSunitCodeText(String totalSSunitCodeText)
	{
		this.totalSSunitCodeText = totalSSunitCodeText;
	}


	/**
	 * @return the salesOrganisationUUID
	 */
	public String getSalesOrganisationUUID()
	{
		return salesOrganisationUUID;
	}


	/**
	 * @param salesOrganisationUUID
	 *           the salesOrganisationUUID to set
	 */
	public void setSalesOrganisationUUID(String salesOrganisationUUID)
	{
		this.salesOrganisationUUID = salesOrganisationUUID;
	}


	/**
	 * @return the individualCustomerFax
	 */
	public String getIndividualCustomerFax()
	{
		return individualCustomerFax;
	}


	/**
	 * @param individualCustomerFax
	 *           the individualCustomerFax to set
	 */
	public void setIndividualCustomerFax(String individualCustomerFax)
	{
		this.individualCustomerFax = individualCustomerFax;
	}


	/**
	 * @return the qualificationLevelCode
	 */
	public String getQualificationLevelCode()
	{
		return qualificationLevelCode;
	}


	/**
	 * @param qualificationLevelCode
	 *           the qualificationLevelCode to set
	 */
	public void setQualificationLevelCode(String qualificationLevelCode)
	{
		this.qualificationLevelCode = qualificationLevelCode;
	}


	/**
	 * @return the originTypeCodeText
	 */
	public String getOriginTypeCodeText()
	{
		return originTypeCodeText;
	}


	/**
	 * @param originTypeCodeText
	 *           the originTypeCodeText to set
	 */
	public void setOriginTypeCodeText(String originTypeCodeText)
	{
		this.originTypeCodeText = originTypeCodeText;
	}


	/**
	 * @return the accountPostalAddressElementsCountyName
	 */
	public String getAccountPostalAddressElementsCountyName()
	{
		return accountPostalAddressElementsCountyName;
	}


	/**
	 * @param accountPostalAddressElementsCountyName
	 *           the accountPostalAddressElementsCountyName to set
	 */
	public void setAccountPostalAddressElementsCountyName(String accountPostalAddressElementsCountyName)
	{
		this.accountPostalAddressElementsCountyName = accountPostalAddressElementsCountyName;
	}


	/**
	 * @return the ownerPartyUUID
	 */
	public String getOwnerPartyUUID()
	{
		return ownerPartyUUID;
	}


	/**
	 * @param ownerPartyUUID
	 *           the ownerPartyUUID to set
	 */
	public void setOwnerPartyUUID(String ownerPartyUUID)
	{
		this.ownerPartyUUID = ownerPartyUUID;
	}


	/**
	 * @return the contactNickName
	 */
	public String getContactNickName()
	{
		return contactNickName;
	}


	/**
	 * @param contactNickName
	 *           the contactNickName to set
	 */
	public void setContactNickName(String contactNickName)
	{
		this.contactNickName = contactNickName;
	}


	/**
	 * @return the salesOfficeID
	 */
	public String getSalesOfficeID()
	{
		return salesOfficeID;
	}


	/**
	 * @param salesOfficeID
	 *           the salesOfficeID to set
	 */
	public void setSalesOfficeID(String salesOfficeID)
	{
		this.salesOfficeID = salesOfficeID;
	}


	/**
	 * @return the dontSave
	 */
	public String getDontSave()
	{
		return dontSave;
	}


	/**
	 * @param dontSave
	 *           the dontSave to set
	 */
	public void setDontSave(String dontSave)
	{
		this.dontSave = dontSave;
	}


	/**
	 * @return the accountCountry
	 */
	public String getAccountCountry()
	{
		return accountCountry;
	}


	/**
	 * @param accountCountry
	 *           the accountCountry to set
	 */
	public void setAccountCountry(String accountCountry)
	{
		this.accountCountry = accountCountry;
	}


	/**
	 * @return the enquiryTypeId
	 */
	public String getEnquiryTypeId()
	{
		return enquiryTypeId;
	}


	/**
	 * @param enquiryTypeId
	 *           the enquiryTypeId to set
	 */
	public void setEnquiryTypeId(String enquiryTypeId)
	{
		this.enquiryTypeId = enquiryTypeId;
	}


	/**
	 * @return the accountPostalAddressElementsHouseID
	 */
	public String getAccountPostalAddressElementsHouseID()
	{
		return accountPostalAddressElementsHouseID;
	}


	/**
	 * @param accountPostalAddressElementsHouseID
	 *           the accountPostalAddressElementsHouseID to set
	 */
	public void setAccountPostalAddressElementsHouseID(String accountPostalAddressElementsHouseID)
	{
		this.accountPostalAddressElementsHouseID = accountPostalAddressElementsHouseID;
	}


	/**
	 * @return the individualCustomerMiddleName
	 */
	public String getIndividualCustomerMiddleName()
	{
		return individualCustomerMiddleName;
	}


	/**
	 * @param individualCustomerMiddleName
	 *           the individualCustomerMiddleName to set
	 */
	public void setIndividualCustomerMiddleName(String individualCustomerMiddleName)
	{
		this.individualCustomerMiddleName = individualCustomerMiddleName;
	}


	/**
	 * @return the salesGroupUUID
	 */
	public String getSalesGroupUUID()
	{
		return salesGroupUUID;
	}


	/**
	 * @param salesGroupUUID
	 *           the salesGroupUUID to set
	 */
	public void setSalesGroupUUID(String salesGroupUUID)
	{
		this.salesGroupUUID = salesGroupUUID;
	}


	/**
	 * @return the creationDateTime
	 */
	public String getCreationDateTime()
	{
		return creationDateTime;
	}


	/**
	 * @param creationDateTime
	 *           the creationDateTime to set
	 */
	public void setCreationDateTime(String creationDateTime)
	{
		this.creationDateTime = creationDateTime;
	}


	/**
	 * @return the repeatCount
	 */
	public String getRepeatCount()
	{
		return repeatCount;
	}


	/**
	 * @param repeatCount
	 *           the repeatCount to set
	 */
	public void setRepeatCount(String repeatCount)
	{
		this.repeatCount = repeatCount;
	}


	/**
	 * @return the salesDistrictLcontentText
	 */
	public String getSalesDistrictLcontentText()
	{
		return salesDistrictLcontentText;
	}


	/**
	 * @param salesDistrictLcontentText
	 *           the salesDistrictLcontentText to set
	 */
	public void setSalesDistrictLcontentText(String salesDistrictLcontentText)
	{
		this.salesDistrictLcontentText = salesDistrictLcontentText;
	}


	/**
	 * @return the distributionChannelCode
	 */
	public String getDistributionChannelCode()
	{
		return distributionChannelCode;
	}


	/**
	 * @param distributionChannelCode
	 *           the distributionChannelCode to set
	 */
	public void setDistributionChannelCode(String distributionChannelCode)
	{
		this.distributionChannelCode = distributionChannelCode;
	}


	/**
	 * @return the accountEMail
	 */
	public String getAccountEMail()
	{
		return accountEMail;
	}


	/**
	 * @param accountEMail
	 *           the accountEMail to set
	 */
	public void setAccountEMail(String accountEMail)
	{
		this.accountEMail = accountEMail;
	}


	/**
	 * @return the marketingUnitPartyID
	 */
	public String getMarketingUnitPartyID()
	{
		return marketingUnitPartyID;
	}


	/**
	 * @param marketingUnitPartyID
	 *           the marketingUnitPartyID to set
	 */
	public void setMarketingUnitPartyID(String marketingUnitPartyID)
	{
		this.marketingUnitPartyID = marketingUnitPartyID;
	}


	/**
	 * @return the businessPartnerRelationshipBusinessPartnerFunctionTypeCode
	 */
	public String getBusinessPartnerRelationshipBusinessPartnerFunctionTypeCode()
	{
		return businessPartnerRelationshipBusinessPartnerFunctionTypeCode;
	}


	/**
	 * @param businessPartnerRelationshipBusinessPartnerFunctionTypeCode
	 *           the businessPartnerRelationshipBusinessPartnerFunctionTypeCode to set
	 */
	public void setBusinessPartnerRelationshipBusinessPartnerFunctionTypeCode(
			String businessPartnerRelationshipBusinessPartnerFunctionTypeCode)
	{
		this.businessPartnerRelationshipBusinessPartnerFunctionTypeCode = businessPartnerRelationshipBusinessPartnerFunctionTypeCode;
	}


	/**
	 * @return the contactAcademicTitleCode
	 */
	public String getContactAcademicTitleCode()
	{
		return contactAcademicTitleCode;
	}


	/**
	 * @param contactAcademicTitleCode
	 *           the contactAcademicTitleCode to set
	 */
	public void setContactAcademicTitleCode(String contactAcademicTitleCode)
	{
		this.contactAcademicTitleCode = contactAcademicTitleCode;
	}


	/**
	 * @return the divisionCodeText
	 */
	public String getDivisionCodeText()
	{
		return divisionCodeText;
	}


	/**
	 * @param divisionCodeText
	 *           the divisionCodeText to set
	 */
	public void setDivisionCodeText(String divisionCodeText)
	{
		this.divisionCodeText = divisionCodeText;
	}


	/**
	 * @return the approvalStatusCode
	 */
	public String getApprovalStatusCode()
	{
		return approvalStatusCode;
	}


	/**
	 * @param approvalStatusCode
	 *           the approvalStatusCode to set
	 */
	public void setApprovalStatusCode(String approvalStatusCode)
	{
		this.approvalStatusCode = approvalStatusCode;
	}


	/**
	 * @return the docTypeEdit
	 */
	public String getDocTypeEdit()
	{
		return docTypeEdit;
	}


	/**
	 * @param docTypeEdit
	 *           the docTypeEdit to set
	 */
	public void setDocTypeEdit(String docTypeEdit)
	{
		this.docTypeEdit = docTypeEdit;
	}


	/**
	 * @return the companySecondName
	 */
	public String getCompanySecondName()
	{
		return companySecondName;
	}


	/**
	 * @param companySecondName
	 *           the companySecondName to set
	 */
	public void setCompanySecondName(String companySecondName)
	{
		this.companySecondName = companySecondName;
	}


	/**
	 * @return the originTypeCode
	 */
	public String getOriginTypeCode()
	{
		return originTypeCode;
	}


	/**
	 * @param originTypeCode
	 *           the originTypeCode to set
	 */
	public void setOriginTypeCode(String originTypeCode)
	{
		this.originTypeCode = originTypeCode;
	}


	/**
	 * @return the saveCounter
	 */
	public String getSaveCounter()
	{
		return saveCounter;
	}


	/**
	 * @param saveCounter
	 *           the saveCounter to set
	 */
	public void setSaveCounter(String saveCounter)
	{
		this.saveCounter = saveCounter;
	}


	/**
	 * @return the individualCustomerAddressHouseID
	 */
	public String getIndividualCustomerAddressHouseID()
	{
		return individualCustomerAddressHouseID;
	}


	/**
	 * @param individualCustomerAddressHouseID
	 *           the individualCustomerAddressHouseID to set
	 */
	public void setIndividualCustomerAddressHouseID(String individualCustomerAddressHouseID)
	{
		this.individualCustomerAddressHouseID = individualCustomerAddressHouseID;
	}


	/**
	 * @return the contactMobile
	 */
	public String getContactMobile()
	{
		return contactMobile;
	}


	/**
	 * @param contactMobile
	 *           the contactMobile to set
	 */
	public void setContactMobile(String contactMobile)
	{
		this.contactMobile = contactMobile;
	}


	/**
	 * @return the surveyTotalScoreValue
	 */
	public String getSurveyTotalScoreValue()
	{
		return surveyTotalScoreValue;
	}


	/**
	 * @param surveyTotalScoreValue
	 *           the surveyTotalScoreValue to set
	 */
	public void setSurveyTotalScoreValue(String surveyTotalScoreValue)
	{
		this.surveyTotalScoreValue = surveyTotalScoreValue;
	}


	/**
	 * @return the ownerPartyIDSales
	 */
	public String getOwnerPartyIDSales()
	{
		return ownerPartyIDSales;
	}


	/**
	 * @param ownerPartyIDSales
	 *           the ownerPartyIDSales to set
	 */
	public void setOwnerPartyIDSales(String ownerPartyIDSales)
	{
		this.ownerPartyIDSales = ownerPartyIDSales;
	}


	/**
	 * @return the contactRoomID
	 */
	public String getContactRoomID()
	{
		return contactRoomID;
	}


	/**
	 * @param contactRoomID
	 *           the contactRoomID to set
	 */
	public void setContactRoomID(String contactRoomID)
	{
		this.contactRoomID = contactRoomID;
	}


	/**
	 * @return the contactBuildingID
	 */
	public String getContactBuildingID()
	{
		return contactBuildingID;
	}


	/**
	 * @param contactBuildingID
	 *           the contactBuildingID to set
	 */
	public void setContactBuildingID(String contactBuildingID)
	{
		this.contactBuildingID = contactBuildingID;
	}


	/**
	 * @return the organisationAccountContactAllowedCode
	 */
	public String getOrganisationAccountContactAllowedCode()
	{
		return organisationAccountContactAllowedCode;
	}


	/**
	 * @param organisationAccountContactAllowedCode
	 *           the organisationAccountContactAllowedCode to set
	 */
	public void setOrganisationAccountContactAllowedCode(String organisationAccountContactAllowedCode)
	{
		this.organisationAccountContactAllowedCode = organisationAccountContactAllowedCode;
	}


	/**
	 * @return the externalLeadId
	 */
	public String getExternalLeadId()
	{
		return externalLeadId;
	}


	/**
	 * @param externalLeadId
	 *           the externalLeadId to set
	 */
	public void setExternalLeadId(String externalLeadId)
	{
		this.externalLeadId = externalLeadId;
	}


	/**
	 * @return the expectedRevenueCurrencyCode
	 */
	public String getExpectedRevenueCurrencyCode()
	{
		return expectedRevenueCurrencyCode;
	}


	/**
	 * @param expectedRevenueCurrencyCode
	 *           the expectedRevenueCurrencyCode to set
	 */
	public void setExpectedRevenueCurrencyCode(String expectedRevenueCurrencyCode)
	{
		this.expectedRevenueCurrencyCode = expectedRevenueCurrencyCode;
	}


	/**
	 * @return the contactGenderCode
	 */
	public String getContactGenderCode()
	{
		return contactGenderCode;
	}


	/**
	 * @param contactGenderCode
	 *           the contactGenderCode to set
	 */
	public void setContactGenderCode(String contactGenderCode)
	{
		this.contactGenderCode = contactGenderCode;
	}


	/**
	 * @return the contactLastName
	 */
	public String getContactLastName()
	{
		return contactLastName;
	}


	/**
	 * @param contactLastName
	 *           the contactLastName to set
	 */
	public void setContactLastName(String contactLastName)
	{
		this.contactLastName = contactLastName;
	}


	/**
	 * @return the accountPartyName
	 */
	public String getAccountPartyName()
	{
		return accountPartyName;
	}


	/**
	 * @param accountPartyName
	 *           the accountPartyName to set
	 */
	public void setAccountPartyName(String accountPartyName)
	{
		this.accountPartyName = accountPartyName;
	}


	/**
	 * @return the eTag
	 */
	public String geteTag()
	{
		return eTag;
	}


	/**
	 * @param eTag
	 *           the eTag to set
	 */
	public void seteTag(String eTag)
	{
		this.eTag = eTag;
	}


	/**
	 * @return the contactFunctionalTitleName
	 */
	public String getContactFunctionalTitleName()
	{
		return contactFunctionalTitleName;
	}


	/**
	 * @param contactFunctionalTitleName
	 *           the contactFunctionalTitleName to set
	 */
	public void setContactFunctionalTitleName(String contactFunctionalTitleName)
	{
		this.contactFunctionalTitleName = contactFunctionalTitleName;
	}


	/**
	 * @return the individualCustomerMobile
	 */
	public String getIndividualCustomerMobile()
	{
		return individualCustomerMobile;
	}


	/**
	 * @param individualCustomerMobile
	 *           the individualCustomerMobile to set
	 */
	public void setIndividualCustomerMobile(String individualCustomerMobile)
	{
		this.individualCustomerMobile = individualCustomerMobile;
	}


	/**
	 * @return the distributionChannelCodeText
	 */
	public String getDistributionChannelCodeText()
	{
		return distributionChannelCodeText;
	}


	/**
	 * @param distributionChannelCodeText
	 *           the distributionChannelCodeText to set
	 */
	public void setDistributionChannelCodeText(String distributionChannelCodeText)
	{
		this.distributionChannelCodeText = distributionChannelCodeText;
	}


	/**
	 * @return the individualCustomerAddressCountry
	 */
	public String getIndividualCustomerAddressCountry()
	{
		return individualCustomerAddressCountry;
	}


	/**
	 * @param individualCustomerAddressCountry
	 *           the individualCustomerAddressCountry to set
	 */
	public void setIndividualCustomerAddressCountry(String individualCustomerAddressCountry)
	{
		this.individualCustomerAddressCountry = individualCustomerAddressCountry;
	}


	/**
	 * @return the accountFax
	 */
	public String getAccountFax()
	{
		return accountFax;
	}


	/**
	 * @param accountFax
	 *           the accountFax to set
	 */
	public void setAccountFax(String accountFax)
	{
		this.accountFax = accountFax;
	}


	/**
	 * @return the individualCustomerAddressStreetName
	 */
	public String getIndividualCustomerAddressStreetName()
	{
		return individualCustomerAddressStreetName;
	}


	/**
	 * @param individualCustomerAddressStreetName
	 *           the individualCustomerAddressStreetName to set
	 */
	public void setIndividualCustomerAddressStreetName(String individualCustomerAddressStreetName)
	{
		this.individualCustomerAddressStreetName = individualCustomerAddressStreetName;
	}


	/**
	 * @return the accountCountryText
	 */
	public String getAccountCountryText()
	{
		return accountCountryText;
	}


	/**
	 * @param accountCountryText
	 *           the accountCountryText to set
	 */
	public void setAccountCountryText(String accountCountryText)
	{
		this.accountCountryText = accountCountryText;
	}


	/**
	 * @return the individualCustomerOccupationCode
	 */
	public String getIndividualCustomerOccupationCode()
	{
		return individualCustomerOccupationCode;
	}


	/**
	 * @param individualCustomerOccupationCode
	 *           the individualCustomerOccupationCode to set
	 */
	public void setIndividualCustomerOccupationCode(String individualCustomerOccupationCode)
	{
		this.individualCustomerOccupationCode = individualCustomerOccupationCode;
	}


	/**
	 * @return the consistencyStatusCodeText
	 */
	public String getConsistencyStatusCodeText()
	{
		return consistencyStatusCodeText;
	}


	/**
	 * @param consistencyStatusCodeText
	 *           the consistencyStatusCodeText to set
	 */
	public void setConsistencyStatusCodeText(String consistencyStatusCodeText)
	{
		this.consistencyStatusCodeText = consistencyStatusCodeText;
	}


	/**
	 * @return the accountPreferredCommunicationMediumTypeCode
	 */
	public String getAccountPreferredCommunicationMediumTypeCode()
	{
		return accountPreferredCommunicationMediumTypeCode;
	}


	/**
	 * @param accountPreferredCommunicationMediumTypeCode
	 *           the accountPreferredCommunicationMediumTypeCode to set
	 */
	public void setAccountPreferredCommunicationMediumTypeCode(String accountPreferredCommunicationMediumTypeCode)
	{
		this.accountPreferredCommunicationMediumTypeCode = accountPreferredCommunicationMediumTypeCode;
	}


	/**
	 * @return the individualCustomerAddressState
	 */
	public String getIndividualCustomerAddressState()
	{
		return individualCustomerAddressState;
	}


	/**
	 * @param individualCustomerAddressState
	 *           the individualCustomerAddressState to set
	 */
	public void setIndividualCustomerAddressState(String individualCustomerAddressState)
	{
		this.individualCustomerAddressState = individualCustomerAddressState;
	}


	/**
	 * @return the approximateValuecontent
	 */
	public String getApproximateValuecontent()
	{
		return approximateValuecontent;
	}


	/**
	 * @param approximateValuecontent
	 *           the approximateValuecontent to set
	 */
	public void setApproximateValuecontent(String approximateValuecontent)
	{
		this.approximateValuecontent = approximateValuecontent;
	}


	/**
	 * @return the individualCustomerAcademicTitleCode
	 */
	public String getIndividualCustomerAcademicTitleCode()
	{
		return individualCustomerAcademicTitleCode;
	}


	/**
	 * @param individualCustomerAcademicTitleCode
	 *           the individualCustomerAcademicTitleCode to set
	 */
	public void setIndividualCustomerAcademicTitleCode(String individualCustomerAcademicTitleCode)
	{
		this.individualCustomerAcademicTitleCode = individualCustomerAcademicTitleCode;
	}


	/**
	 * @return the priorityCode
	 */
	public String getPriorityCode()
	{
		return priorityCode;
	}


	/**
	 * @param priorityCode
	 *           the priorityCode to set
	 */
	public void setPriorityCode(String priorityCode)
	{
		this.priorityCode = priorityCode;
	}


	/**
	 * @return the contactDepartmentName
	 */
	public String getContactDepartmentName()
	{
		return contactDepartmentName;
	}


	/**
	 * @param contactDepartmentName
	 *           the contactDepartmentName to set
	 */
	public void setContactDepartmentName(String contactDepartmentName)
	{
		this.contactDepartmentName = contactDepartmentName;
	}


	/**
	 * @return the copyInd
	 */
	public String getCopyInd()
	{
		return copyInd;
	}


	/**
	 * @param copyInd
	 *           the copyInd to set
	 */
	public void setCopyInd(String copyInd)
	{
		this.copyInd = copyInd;
	}


	/**
	 * @return the userStatusCode
	 */
	public String getUserStatusCode()
	{
		return userStatusCode;
	}


	/**
	 * @param userStatusCode
	 *           the userStatusCode to set
	 */
	public void setUserStatusCode(String userStatusCode)
	{
		this.userStatusCode = userStatusCode;
	}


	/**
	 * @return the divisionCode
	 */
	public String getDivisionCode()
	{
		return divisionCode;
	}


	/**
	 * @param divisionCode
	 *           the divisionCode to set
	 */
	public void setDivisionCode(String divisionCode)
	{
		this.divisionCode = divisionCode;
	}


	/**
	 * @return the totalMSunitCodeText
	 */
	public String getTotalMSunitCodeText()
	{
		return totalMSunitCodeText;
	}


	/**
	 * @param totalMSunitCodeText
	 *           the totalMSunitCodeText to set
	 */
	public void setTotalMSunitCodeText(String totalMSunitCodeText)
	{
		this.totalMSunitCodeText = totalMSunitCodeText;
	}


	/**
	 * @return the businessPartnerRelationshipBusinessPartnerFunctionalAreaCode
	 */
	public String getBusinessPartnerRelationshipBusinessPartnerFunctionalAreaCode()
	{
		return businessPartnerRelationshipBusinessPartnerFunctionalAreaCode;
	}


	/**
	 * @param businessPartnerRelationshipBusinessPartnerFunctionalAreaCode
	 *           the businessPartnerRelationshipBusinessPartnerFunctionalAreaCode to set
	 */
	public void setBusinessPartnerRelationshipBusinessPartnerFunctionalAreaCode(
			String businessPartnerRelationshipBusinessPartnerFunctionalAreaCode)
	{
		this.businessPartnerRelationshipBusinessPartnerFunctionalAreaCode = businessPartnerRelationshipBusinessPartnerFunctionalAreaCode;
	}


	/**
	 * @return the resultReasonCode
	 */
	public String getResultReasonCode()
	{
		return resultReasonCode;
	}


	/**
	 * @param resultReasonCode
	 *           the resultReasonCode to set
	 */
	public void setResultReasonCode(String resultReasonCode)
	{
		this.resultReasonCode = resultReasonCode;
	}


	/**
	 * @return the mainContactLifeCycleStatusCode
	 */
	public String getMainContactLifeCycleStatusCode()
	{
		return mainContactLifeCycleStatusCode;
	}


	/**
	 * @param mainContactLifeCycleStatusCode
	 *           the mainContactLifeCycleStatusCode to set
	 */
	public void setMainContactLifeCycleStatusCode(String mainContactLifeCycleStatusCode)
	{
		this.mainContactLifeCycleStatusCode = mainContactLifeCycleStatusCode;
	}


	/**
	 * @return the salesGroupID
	 */
	public String getSalesGroupID()
	{
		return salesGroupID;
	}


	/**
	 * @param salesGroupID
	 *           the salesGroupID to set
	 */
	public void setSalesGroupID(String salesGroupID)
	{
		this.salesGroupID = salesGroupID;
	}


	/**
	 * @return the totalOrderQuantityLeadunitCodeText
	 */
	public String getTotalOrderQuantityLeadunitCodeText()
	{
		return totalOrderQuantityLeadunitCodeText;
	}


	/**
	 * @param totalOrderQuantityLeadunitCodeText
	 *           the totalOrderQuantityLeadunitCodeText to set
	 */
	public void setTotalOrderQuantityLeadunitCodeText(String totalOrderQuantityLeadunitCodeText)
	{
		this.totalOrderQuantityLeadunitCodeText = totalOrderQuantityLeadunitCodeText;
	}


	/**
	 * @return the totalSSunitCode
	 */
	public String getTotalSSunitCode()
	{
		return totalSSunitCode;
	}


	/**
	 * @param totalSSunitCode
	 *           the totalSSunitCode to set
	 */
	public void setTotalSSunitCode(String totalSSunitCode)
	{
		this.totalSSunitCode = totalSSunitCode;
	}


	/**
	 * @return the companyThirdName
	 */
	public String getCompanyThirdName()
	{
		return companyThirdName;
	}


	/**
	 * @param companyThirdName
	 *           the companyThirdName to set
	 */
	public void setCompanyThirdName(String companyThirdName)
	{
		this.companyThirdName = companyThirdName;
	}


	/**
	 * @return the contactMiddleName
	 */
	public String getContactMiddleName()
	{
		return contactMiddleName;
	}


	/**
	 * @param contactMiddleName
	 *           the contactMiddleName to set
	 */
	public void setContactMiddleName(String contactMiddleName)
	{
		this.contactMiddleName = contactMiddleName;
	}


	/**
	 * @return the accountPhone
	 */
	public String getAccountPhone()
	{
		return accountPhone;
	}


	/**
	 * @param accountPhone
	 *           the accountPhone to set
	 */
	public void setAccountPhone(String accountPhone)
	{
		this.accountPhone = accountPhone;
	}


	/**
	 * @return the salesDistrictLcontent
	 */
	public String getSalesDistrictLcontent()
	{
		return salesDistrictLcontent;
	}


	/**
	 * @param salesDistrictLcontent
	 *           the salesDistrictLcontent to set
	 */
	public void setSalesDistrictLcontent(String salesDistrictLcontent)
	{
		this.salesDistrictLcontent = salesDistrictLcontent;
	}


	/**
	 * @return the individualCustomerEMail
	 */
	public String getIndividualCustomerEMail()
	{
		return individualCustomerEMail;
	}


	/**
	 * @param individualCustomerEMail
	 *           the individualCustomerEMail to set
	 */
	public void setIndividualCustomerEMail(String individualCustomerEMail)
	{
		this.individualCustomerEMail = individualCustomerEMail;
	}


	/**
	 * @return the saveInd
	 */
	public String getSaveInd()
	{
		return saveInd;
	}


	/**
	 * @param saveInd
	 *           the saveInd to set
	 */
	public void setSaveInd(String saveInd)
	{
		this.saveInd = saveInd;
	}


	/**
	 * @return the accountPostalAddressElementsStreetName
	 */
	public String getAccountPostalAddressElementsStreetName()
	{
		return accountPostalAddressElementsStreetName;
	}


	/**
	 * @param accountPostalAddressElementsStreetName
	 *           the accountPostalAddressElementsStreetName to set
	 */
	public void setAccountPostalAddressElementsStreetName(String accountPostalAddressElementsStreetName)
	{
		this.accountPostalAddressElementsStreetName = accountPostalAddressElementsStreetName;
	}


	/**
	 * @return the salesTerritoryName
	 */
	public String getSalesTerritoryName()
	{
		return salesTerritoryName;
	}


	/**
	 * @param salesTerritoryName
	 *           the salesTerritoryName to set
	 */
	public void setSalesTerritoryName(String salesTerritoryName)
	{
		this.salesTerritoryName = salesTerritoryName;
	}


	/**
	 * @return the contactFormOfAddressCode
	 */
	public String getContactFormOfAddressCode()
	{
		return contactFormOfAddressCode;
	}


	/**
	 * @param contactFormOfAddressCode
	 *           the contactFormOfAddressCode to set
	 */
	public void setContactFormOfAddressCode(String contactFormOfAddressCode)
	{
		this.contactFormOfAddressCode = contactFormOfAddressCode;
	}


	/**
	 * @return the accountPostalAddressElementsStreetPrefix
	 */
	public String getAccountPostalAddressElementsStreetPrefix()
	{
		return accountPostalAddressElementsStreetPrefix;
	}


	/**
	 * @param accountPostalAddressElementsStreetPrefix
	 *           the accountPostalAddressElementsStreetPrefix to set
	 */
	public void setAccountPostalAddressElementsStreetPrefix(String accountPostalAddressElementsStreetPrefix)
	{
		this.accountPostalAddressElementsStreetPrefix = accountPostalAddressElementsStreetPrefix;
	}


	/**
	 * @return the nameLanguageCode
	 */
	public String getNameLanguageCode()
	{
		return nameLanguageCode;
	}


	/**
	 * @param nameLanguageCode
	 *           the nameLanguageCode to set
	 */
	public void setNameLanguageCode(String nameLanguageCode)
	{
		this.nameLanguageCode = nameLanguageCode;
	}


	/**
	 * @return the individualCustomerBirthDate
	 */
	public String getIndividualCustomerBirthDate()
	{
		return individualCustomerBirthDate;
	}


	/**
	 * @param individualCustomerBirthDate
	 *           the individualCustomerBirthDate to set
	 */
	public void setIndividualCustomerBirthDate(String individualCustomerBirthDate)
	{
		this.individualCustomerBirthDate = individualCustomerBirthDate;
	}


	/**
	 * @return the contactName
	 */
	public String getContactName()
	{
		return contactName;
	}


	/**
	 * @param contactName
	 *           the contactName to set
	 */
	public void setContactName(String contactName)
	{
		this.contactName = contactName;
	}


	/**
	 * @return the lastChangedBy
	 */
	public String getLastChangedBy()
	{
		return lastChangedBy;
	}


	/**
	 * @param lastChangedBy
	 *           the lastChangedBy to set
	 */
	public void setLastChangedBy(String lastChangedBy)
	{
		this.lastChangedBy = lastChangedBy;
	}


	/**
	 * @return the accountCorrespondenceLanguageCode
	 */
	public String getAccountCorrespondenceLanguageCode()
	{
		return accountCorrespondenceLanguageCode;
	}


	/**
	 * @param accountCorrespondenceLanguageCode
	 *           the accountCorrespondenceLanguageCode to set
	 */
	public void setAccountCorrespondenceLanguageCode(String accountCorrespondenceLanguageCode)
	{
		this.accountCorrespondenceLanguageCode = accountCorrespondenceLanguageCode;
	}


	/**
	 * @return the company
	 */
	public String getCompany()
	{
		return company;
	}


	/**
	 * @param company
	 *           the company to set
	 */
	public void setCompany(String company)
	{
		this.company = company;
	}


	/**
	 * @return the salesOfficeUUID
	 */
	public String getSalesOfficeUUID()
	{
		return salesOfficeUUID;
	}


	/**
	 * @param salesOfficeUUID
	 *           the salesOfficeUUID to set
	 */
	public void setSalesOfficeUUID(String salesOfficeUUID)
	{
		this.salesOfficeUUID = salesOfficeUUID;
	}


	/**
	 * @return the accountStateText
	 */
	public String getAccountStateText()
	{
		return accountStateText;
	}


	/**
	 * @param accountStateText
	 *           the accountStateText to set
	 */
	public void setAccountStateText(String accountStateText)
	{
		this.accountStateText = accountStateText;
	}


	/**
	 * @return the objectID
	 */
	public String getObjectID()
	{
		return objectID;
	}


	/**
	 * @param objectID
	 *           the objectID to set
	 */
	public void setObjectID(String objectID)
	{
		this.objectID = objectID;
	}


	/**
	 * @return the qualificationLevelCodeText
	 */
	public String getQualificationLevelCodeText()
	{
		return qualificationLevelCodeText;
	}


	/**
	 * @param qualificationLevelCodeText
	 *           the qualificationLevelCodeText to set
	 */
	public void setQualificationLevelCodeText(String qualificationLevelCodeText)
	{
		this.qualificationLevelCodeText = qualificationLevelCodeText;
	}


	/**
	 * @return the totalSScontent
	 */
	public String getTotalSScontent()
	{
		return totalSScontent;
	}


	/**
	 * @param totalSScontent
	 *           the totalSScontent to set
	 */
	public void setTotalSScontent(String totalSScontent)
	{
		this.totalSScontent = totalSScontent;
	}


	/**
	 * @return the individualCustomerFormOfAddressCode
	 */
	public String getIndividualCustomerFormOfAddressCode()
	{
		return individualCustomerFormOfAddressCode;
	}


	/**
	 * @param individualCustomerFormOfAddressCode
	 *           the individualCustomerFormOfAddressCode to set
	 */
	public void setIndividualCustomerFormOfAddressCode(String individualCustomerFormOfAddressCode)
	{
		this.individualCustomerFormOfAddressCode = individualCustomerFormOfAddressCode;
	}


	/**
	 * @return the startDate
	 */
	public String getStartDate()
	{
		return startDate;
	}


	/**
	 * @param startDate
	 *           the startDate to set
	 */
	public void setStartDate(String startDate)
	{
		this.startDate = startDate;
	}


	/**
	 * @return the uniqueIDL
	 */
	public String getUniqueIDL()
	{
		return uniqueIDL;
	}


	/**
	 * @param uniqueIDL
	 *           the uniqueIDL to set
	 */
	public void setUniqueIDL(String uniqueIDL)
	{
		this.uniqueIDL = uniqueIDL;
	}


	/**
	 * @return the contactCorrespondenceLanguageCode
	 */
	public String getContactCorrespondenceLanguageCode()
	{
		return contactCorrespondenceLanguageCode;
	}


	/**
	 * @param contactCorrespondenceLanguageCode
	 *           the contactCorrespondenceLanguageCode to set
	 */
	public void setContactCorrespondenceLanguageCode(String contactCorrespondenceLanguageCode)
	{
		this.contactCorrespondenceLanguageCode = contactCorrespondenceLanguageCode;
	}


	/**
	 * @return the lifeCycleStatusCode
	 */
	public String getLifeCycleStatusCode()
	{
		return lifeCycleStatusCode;
	}


	/**
	 * @param lifeCycleStatusCode
	 *           the lifeCycleStatusCode to set
	 */
	public void setLifeCycleStatusCode(String lifeCycleStatusCode)
	{
		this.lifeCycleStatusCode = lifeCycleStatusCode;
	}


	/**
	 * @return the teamLead
	 */
	public String getTeamLead()
	{
		return teamLead;
	}


	/**
	 * @param teamLead
	 *           the teamLead to set
	 */
	public void setTeamLead(String teamLead)
	{
		this.teamLead = teamLead;
	}


	/**
	 * @return the ownerPartyName
	 */
	public String getOwnerPartyName()
	{
		return ownerPartyName;
	}


	/**
	 * @param ownerPartyName
	 *           the ownerPartyName to set
	 */
	public void setOwnerPartyName(String ownerPartyName)
	{
		this.ownerPartyName = ownerPartyName;
	}


	/**
	 * @return the individualCustomerNonVerbalCommunicationLanguageCode
	 */
	public String getIndividualCustomerNonVerbalCommunicationLanguageCode()
	{
		return individualCustomerNonVerbalCommunicationLanguageCode;
	}


	/**
	 * @param individualCustomerNonVerbalCommunicationLanguageCode
	 *           the individualCustomerNonVerbalCommunicationLanguageCode to set
	 */
	public void setIndividualCustomerNonVerbalCommunicationLanguageCode(
			String individualCustomerNonVerbalCommunicationLanguageCode)
	{
		this.individualCustomerNonVerbalCommunicationLanguageCode = individualCustomerNonVerbalCommunicationLanguageCode;
	}


	/**
	 * @return the prodAvailInd
	 */
	public String getProdAvailInd()
	{
		return prodAvailInd;
	}


	/**
	 * @param prodAvailInd
	 *           the prodAvailInd to set
	 */
	public void setProdAvailInd(String prodAvailInd)
	{
		this.prodAvailInd = prodAvailInd;
	}


	/**
	 * @return the individualCustomerGivenName
	 */
	public String getIndividualCustomerGivenName()
	{
		return individualCustomerGivenName;
	}


	/**
	 * @param individualCustomerGivenName
	 *           the individualCustomerGivenName to set
	 */
	public void setIndividualCustomerGivenName(String individualCustomerGivenName)
	{
		this.individualCustomerGivenName = individualCustomerGivenName;
	}


	/**
	 * @return the organisationAccountABCClassificationCode
	 */
	public String getOrganisationAccountABCClassificationCode()
	{
		return organisationAccountABCClassificationCode;
	}


	/**
	 * @param organisationAccountABCClassificationCode
	 *           the organisationAccountABCClassificationCode to set
	 */
	public void setOrganisationAccountABCClassificationCode(String organisationAccountABCClassificationCode)
	{
		this.organisationAccountABCClassificationCode = organisationAccountABCClassificationCode;
	}


	/**
	 * @return the contactFloorID
	 */
	public String getContactFloorID()
	{
		return contactFloorID;
	}


	/**
	 * @param contactFloorID
	 *           the contactFloorID to set
	 */
	public void setContactFloorID(String contactFloorID)
	{
		this.contactFloorID = contactFloorID;
	}


	/**
	 * @return the individualCustomerABCClassificationCode
	 */
	public String getIndividualCustomerABCClassificationCode()
	{
		return individualCustomerABCClassificationCode;
	}


	/**
	 * @param individualCustomerABCClassificationCode
	 *           the individualCustomerABCClassificationCode to set
	 */
	public void setIndividualCustomerABCClassificationCode(String individualCustomerABCClassificationCode)
	{
		this.individualCustomerABCClassificationCode = individualCustomerABCClassificationCode;
	}


	/**
	 * @return the businessPartnerRelationshipContactVIPReasonCode
	 */
	public String getBusinessPartnerRelationshipContactVIPReasonCode()
	{
		return businessPartnerRelationshipContactVIPReasonCode;
	}


	/**
	 * @param businessPartnerRelationshipContactVIPReasonCode
	 *           the businessPartnerRelationshipContactVIPReasonCode to set
	 */
	public void setBusinessPartnerRelationshipContactVIPReasonCode(String businessPartnerRelationshipContactVIPReasonCode)
	{
		this.businessPartnerRelationshipContactVIPReasonCode = businessPartnerRelationshipContactVIPReasonCode;
	}


	/**
	 * @return the accountLongitudeMeasure
	 */
	public String getAccountLongitudeMeasure()
	{
		return accountLongitudeMeasure;
	}


	/**
	 * @param accountLongitudeMeasure
	 *           the accountLongitudeMeasure to set
	 */
	public void setAccountLongitudeMeasure(String accountLongitudeMeasure)
	{
		this.accountLongitudeMeasure = accountLongitudeMeasure;
	}


	/**
	 * @return the individualCustomerFamilyName
	 */
	public String getIndividualCustomerFamilyName()
	{
		return individualCustomerFamilyName;
	}


	/**
	 * @param individualCustomerFamilyName
	 *           the individualCustomerFamilyName to set
	 */
	public void setIndividualCustomerFamilyName(String individualCustomerFamilyName)
	{
		this.individualCustomerFamilyName = individualCustomerFamilyName;
	}


	/**
	 * @return the salesUnitPartyUUID
	 */
	public String getSalesUnitPartyUUID()
	{
		return salesUnitPartyUUID;
	}


	/**
	 * @param salesUnitPartyUUID
	 *           the salesUnitPartyUUID to set
	 */
	public void setSalesUnitPartyUUID(String salesUnitPartyUUID)
	{
		this.salesUnitPartyUUID = salesUnitPartyUUID;
	}


	/**
	 * @return the leadDocType
	 */
	public String getLeadDocType()
	{
		return leadDocType;
	}


	/**
	 * @param leadDocType
	 *           the leadDocType to set
	 */
	public void setLeadDocType(String leadDocType)
	{
		this.leadDocType = leadDocType;
	}


	/**
	 * @return the contactAdditionalAcademicTitleCode
	 */
	public String getContactAdditionalAcademicTitleCode()
	{
		return contactAdditionalAcademicTitleCode;
	}


	/**
	 * @param contactAdditionalAcademicTitleCode
	 *           the contactAdditionalAcademicTitleCode to set
	 */
	public void setContactAdditionalAcademicTitleCode(String contactAdditionalAcademicTitleCode)
	{
		this.contactAdditionalAcademicTitleCode = contactAdditionalAcademicTitleCode;
	}


	/**
	 * @return the contactNote
	 */
	public String getContactNote()
	{
		return contactNote;
	}


	/**
	 * @param contactNote
	 *           the contactNote to set
	 */
	public void setContactNote(String contactNote)
	{
		this.contactNote = contactNote;
	}


	/**
	 * @return the individualCustomerNationalityCountryCode
	 */
	public String getIndividualCustomerNationalityCountryCode()
	{
		return individualCustomerNationalityCountryCode;
	}


	/**
	 * @param individualCustomerNationalityCountryCode
	 *           the individualCustomerNationalityCountryCode to set
	 */
	public void setIndividualCustomerNationalityCountryCode(String individualCustomerNationalityCountryCode)
	{
		this.individualCustomerNationalityCountryCode = individualCustomerNationalityCountryCode;
	}


	/**
	 * @return the accountWebsite
	 */
	public String getAccountWebsite()
	{
		return accountWebsite;
	}


	/**
	 * @param accountWebsite
	 *           the accountWebsite to set
	 */
	public void setAccountWebsite(String accountWebsite)
	{
		this.accountWebsite = accountWebsite;
	}


	/**
	 * @return the externalID
	 */
	public String getExternalID()
	{
		return externalID;
	}


	/**
	 * @param externalID
	 *           the externalID to set
	 */
	public void setExternalID(String externalID)
	{
		this.externalID = externalID;
	}


	/**
	 * @return the zTeamLeadText
	 */
	public String getZTeamLeadText()
	{
		return ZTeamLeadText;
	}


	/**
	 * @param zTeamLeadText
	 *           the zTeamLeadText to set
	 */
	public void setZTeamLeadText(String zTeamLeadText)
	{
		ZTeamLeadText = zTeamLeadText;
	}


	/**
	 * @return the lifeCycleStatusCodeText
	 */
	public String getLifeCycleStatusCodeText()
	{
		return lifeCycleStatusCodeText;
	}


	/**
	 * @param lifeCycleStatusCodeText
	 *           the lifeCycleStatusCodeText to set
	 */
	public void setLifeCycleStatusCodeText(String lifeCycleStatusCodeText)
	{
		this.lifeCycleStatusCodeText = lifeCycleStatusCodeText;
	}


	/**
	 * @return the accountPostalAddressElementsAdditionalStreetSuffixName
	 */
	public String getAccountPostalAddressElementsAdditionalStreetSuffixName()
	{
		return accountPostalAddressElementsAdditionalStreetSuffixName;
	}


	/**
	 * @param accountPostalAddressElementsAdditionalStreetSuffixName
	 *           the accountPostalAddressElementsAdditionalStreetSuffixName to set
	 */
	public void setAccountPostalAddressElementsAdditionalStreetSuffixName(
			String accountPostalAddressElementsAdditionalStreetSuffixName)
	{
		this.accountPostalAddressElementsAdditionalStreetSuffixName = accountPostalAddressElementsAdditionalStreetSuffixName;
	}


	/**
	 * @return the salesUnitPartyID
	 */
	public String getSalesUnitPartyID()
	{
		return salesUnitPartyID;
	}


	/**
	 * @param salesUnitPartyID
	 *           the salesUnitPartyID to set
	 */
	public void setSalesUnitPartyID(String salesUnitPartyID)
	{
		this.salesUnitPartyID = salesUnitPartyID;
	}


	/**
	 * @return the accountLatitudeMeasureUnitCode
	 */
	public String getAccountLatitudeMeasureUnitCode()
	{
		return accountLatitudeMeasureUnitCode;
	}


	/**
	 * @param accountLatitudeMeasureUnitCode
	 *           the accountLatitudeMeasureUnitCode to set
	 */
	public void setAccountLatitudeMeasureUnitCode(String accountLatitudeMeasureUnitCode)
	{
		this.accountLatitudeMeasureUnitCode = accountLatitudeMeasureUnitCode;
	}


	/**
	 * @return the salesTerritoryID
	 */
	public String getSalesTerritoryID()
	{
		return salesTerritoryID;
	}


	/**
	 * @param salesTerritoryID
	 *           the salesTerritoryID to set
	 */
	public void setSalesTerritoryID(String salesTerritoryID)
	{
		this.salesTerritoryID = salesTerritoryID;
	}


	/**
	 * @return the businessPartnerRelationshipEngagementScoreNumberValue
	 */
	public String getBusinessPartnerRelationshipEngagementScoreNumberValue()
	{
		return businessPartnerRelationshipEngagementScoreNumberValue;
	}


	/**
	 * @param businessPartnerRelationshipEngagementScoreNumberValue
	 *           the businessPartnerRelationshipEngagementScoreNumberValue to set
	 */
	public void setBusinessPartnerRelationshipEngagementScoreNumberValue(
			String businessPartnerRelationshipEngagementScoreNumberValue)
	{
		this.businessPartnerRelationshipEngagementScoreNumberValue = businessPartnerRelationshipEngagementScoreNumberValue;
	}


	/**
	 * @return the accountPostalAddressElementsPOBoxID
	 */
	public String getAccountPostalAddressElementsPOBoxID()
	{
		return accountPostalAddressElementsPOBoxID;
	}


	/**
	 * @param accountPostalAddressElementsPOBoxID
	 *           the accountPostalAddressElementsPOBoxID to set
	 */
	public void setAccountPostalAddressElementsPOBoxID(String accountPostalAddressElementsPOBoxID)
	{
		this.accountPostalAddressElementsPOBoxID = accountPostalAddressElementsPOBoxID;
	}


	/**
	 * @return the accountLongitudeMeasureUnitCode
	 */
	public String getAccountLongitudeMeasureUnitCode()
	{
		return accountLongitudeMeasureUnitCode;
	}


	/**
	 * @param accountLongitudeMeasureUnitCode
	 *           the accountLongitudeMeasureUnitCode to set
	 */
	public void setAccountLongitudeMeasureUnitCode(String accountLongitudeMeasureUnitCode)
	{
		this.accountLongitudeMeasureUnitCode = accountLongitudeMeasureUnitCode;
	}


	/**
	 * @return the customerId
	 */
	public String getCustomerId()
	{
		return customerId;
	}


	/**
	 * @param customerId
	 *           the customerId to set
	 */
	public void setCustomerId(String customerId)
	{
		this.customerId = customerId;
	}


	/**
	 * @return the groupCode
	 */
	public String getGroupCode()
	{
		return groupCode;
	}


	/**
	 * @param groupCode
	 *           the groupCode to set
	 */
	public void setGroupCode(String groupCode)
	{
		this.groupCode = groupCode;
	}


	/**
	 * @return the accountLifeCycleStatusCode
	 */
	public String getAccountLifeCycleStatusCode()
	{
		return accountLifeCycleStatusCode;
	}


	/**
	 * @param accountLifeCycleStatusCode
	 *           the accountLifeCycleStatusCode to set
	 */
	public void setAccountLifeCycleStatusCode(String accountLifeCycleStatusCode)
	{
		this.accountLifeCycleStatusCode = accountLifeCycleStatusCode;
	}


	/**
	 * @return the contactEMail
	 */
	public String getContactEMail()
	{
		return contactEMail;
	}


	/**
	 * @param contactEMail
	 *           the contactEMail to set
	 */
	public void setContactEMail(String contactEMail)
	{
		this.contactEMail = contactEMail;
	}


	/**
	 * @return the contactID
	 */
	public String getContactID()
	{
		return contactID;
	}


	/**
	 * @param contactID
	 *           the contactID to set
	 */
	public void setContactID(String contactID)
	{
		this.contactID = contactID;
	}


	/**
	 * @return the resultReasonCodeText
	 */
	public String getResultReasonCodeText()
	{
		return resultReasonCodeText;
	}


	/**
	 * @param resultReasonCodeText
	 *           the resultReasonCodeText to set
	 */
	public void setResultReasonCodeText(String resultReasonCodeText)
	{
		this.resultReasonCodeText = resultReasonCodeText;
	}


	/**
	 * @return the prodAdd
	 */
	public String getProdAdd()
	{
		return prodAdd;
	}


	/**
	 * @param prodAdd
	 *           the prodAdd to set
	 */
	public void setProdAdd(String prodAdd)
	{
		this.prodAdd = prodAdd;
	}

}
