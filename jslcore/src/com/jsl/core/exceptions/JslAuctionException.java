package com.jsl.core.exceptions;

public class JslAuctionException extends RuntimeException
{

	private static final long serialVersionUID = 1L;

	/** The error code. */
	private final String code;

	/** The error description. */
	private final String description;

	/** The flow name or method name in which this exception occurred. */
	private final String flowName;

	/** The request object. */
	private final Object requestObject;

	public JslAuctionException(final String code, final String description, final String flowName, final Object requestObject)
	{
		// call the parent constructor with additional default values for
		// enableSuppression and writableStackTrace
		super(prepareExceptionMessage(code, description, flowName, requestObject));
		this.code = code;
		this.description = description;
		this.flowName = flowName;
		this.requestObject = requestObject;
	}

	/**
	 * Prepare exception message.
	 *
	 * @param code
	 *           the code
	 * @param description
	 *           the description
	 * @param flowName
	 *           the flow name
	 * @param requestObject
	 *           the request object
	 * @return the string
	 */
	public static String prepareExceptionMessage(final String code, final String description, final String flowName,
			final Object requestObject)
	{
		final StringBuilder message = new StringBuilder();
		message.append("\n ************************************************************");
		message.append("\n \t\t\t JslAuctionException STARTS");
		message.append("\n\n Flowname : " + flowName);
		message.append("\n Code : " + code);
		message.append("\n Description : " + description);
		if (null != requestObject)
		{
			message.append("\n Request Data : \n " + requestObject);
		}
		message.append("\n\n \t\t\t JslAuctionException ENDS");
		message.append("\n ************************************************************");

		return message.toString();
	}

	public String getCode()
	{
		return code;
	}

	public String getDescription()
	{
		return description;
	}

	public String getFlowName()
	{
		return flowName;
	}

	public Object getRequestObject()
	{
		return requestObject;
	}

	@Override
	public String toString()
	{
		final StringBuilder builder = new StringBuilder();
		builder.append("JslAuctionException [code=");
		builder.append(code);
		builder.append(", description=");
		builder.append(description);
		builder.append(", flowName=");
		builder.append(flowName);
		builder.append("]");
		return builder.toString();
	}

}
