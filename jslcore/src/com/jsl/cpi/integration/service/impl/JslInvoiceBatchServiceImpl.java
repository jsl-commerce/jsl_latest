/**
 * 
 */
package com.jsl.cpi.integration.service.impl;

import de.hybris.platform.util.Config;

import javax.xml.soap.SOAPException;

import org.apache.log4j.Logger;

import com.jsl.cpi.integration.service.CpiReportsConnectionService;
import com.jsl.cpi.integration.service.JslInvoiceBatchService;
import com.jsl.cpi.invoice.batch.dto.ObjectFactory;
import com.jsl.cpi.invoice.batch.dto.ZFMINVBATCHHYBRIS;
import com.jsl.cpi.report.dto.CPIResponse;


/**
 * @author himanshu.sial
 *
 */
public class JslInvoiceBatchServiceImpl implements JslInvoiceBatchService
{

	private static final Logger LOG = Logger.getLogger(JslInvoiceBatchServiceImpl.class);
	private static String INVOICE_BATCH_URL_KEY = "invoice.batch.url";
	
	private CpiReportsConnectionService cpiReportsService;
	
	/* (non-Javadoc)
	 * @see com.jsl.cpi.integration.service.JslInvoiceBatchService#getBatchesFromInvoiceResponse(java.lang.String)
	 */
	@Override
	public CPIResponse getBatchesFromInvoiceResponse(String batchNumber) throws SOAPException
	{
		// XXX Auto-generated method stub
		final String url = Config.getParameter(INVOICE_BATCH_URL_KEY);
		final ObjectFactory of = new ObjectFactory();
		ZFMINVBATCHHYBRIS request = of.createZFMINVBATCHHYBRIS();
		request.setITOUTPUT(of.createZTTBATCHHYBRIS());
		request.setVBELN(batchNumber);
		
		return cpiReportsService.getResponse(url, request);
	}

	/**
	 * @return the cpiReportsService
	 */
	public CpiReportsConnectionService getCpiReportsService()
	{
		return cpiReportsService;
	}

	/**
	 * @param cpiReportsService the cpiReportsService to set
	 */
	public void setCpiReportsService(CpiReportsConnectionService cpiReportsService)
	{
		this.cpiReportsService = cpiReportsService;
	}
}
