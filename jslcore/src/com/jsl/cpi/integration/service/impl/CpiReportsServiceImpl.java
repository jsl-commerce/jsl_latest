/**
 *
 */
package com.jsl.cpi.integration.service.impl;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;

import java.math.BigDecimal;

import javax.xml.soap.SOAPException;

import org.apache.log4j.Logger;

import com.jsl.core.util.JslB2bUnitUtil;
import com.jsl.cpi.integration.service.CpiReportsConnectionService;
import com.jsl.cpi.integration.service.CpiReportsService;
import com.jsl.cpi.invoice.batch.dto.ZFMINVBATCHHYBRIS;
import com.jsl.cpi.pop.mou.dto.ZSDPOPVALUEFORCARTHYBRIS;
import com.jsl.cpi.pop.mou.dto.ZSDPOPVALUEFORCARTHYBRISResponse;
import com.jsl.cpi.report.dto.CPIResponse;
import com.jsl.cpi.report.dto.accountstatement.ZFI137HYBRIS;
import com.jsl.cpi.report.dto.customer.dashboard.ZSDCUSTDASHBOARDHYBRIS;
import com.jsl.cpi.report.dto.orderbooking.ZSD35HYBRIS;
import com.jsl.cpi.report.dto.orderbooking.ZTTORDERBOOKINGHYBRIS;
import com.jsl.cpi.report.dto.pop.plant.ObjectFactory;
import com.jsl.cpi.report.dto.pop.plant.ZPOPHYBRIS;
import com.jsl.cpi.report.dto.pop.plant.ZTTPOPHYBRIS;
import com.jsl.cpi.report.dto.pop.yard.ZSD35CHYBRIS;
import com.jsl.cpi.report.dto.sales.ZSD34HYBRIS;
import com.jsl.cpi.report.dto.vehicle.tracking.ZSDTRACKDELHYBRIS;



/**
 * @author himanshu.sial
 *
 */
public class CpiReportsServiceImpl implements CpiReportsService
{

	private static final Logger LOG = Logger.getLogger(CpiReportsServiceImpl.class);
	private CpiReportsConnectionService cpiReportsService;
	private UserService userService;

	private static String POP_PLANT_REPORT_URL_KEY = "pop.plant.report.url";
	private static String SALES_REPORT_URL_KEY = "sales.report.url";
	private static String ORDER_BOOKING_REPORT_URL_KEY = "order.booking.report.url";
	private static String CUSTOMER_DASHBOARD_URL_KEY = "customer.dashboard.report.url";
	private static String POP_YARD_REPORT_URL_KEY = "pop.yard.report.url";
	private static String ACCOUNT_STATEMENT_REPORT_URL_KEY = "account.statement.report.url";
	private static String INVOICE_BATCH_DETAILS_URL_KEY = "invoice.batch.details.url";
	private static final String DEFAULT_B2BUNIT_CODE = "default.b2b.unit.codes";
	private static final String POP_MOU_URL_KEY = "pop.mou.url";
	private static String VEHICLE_TRACKING_REPORT_URL_KEY = "vehicle.tracking.report.url";

	public CPIResponse getPopPlantReportResponse() throws SOAPException
	{
		final String url = Config.getParameter(POP_PLANT_REPORT_URL_KEY);
		final ObjectFactory of = new ObjectFactory();
		final ZTTPOPHYBRIS zttpophybris = of.createZTTPOPHYBRIS();
		final ZPOPHYBRIS request = of.createZPOPHYBRIS();
		request.setZPOPHYBRIS(zttpophybris);
		final String customerId = getCustomerId();
		LOG.info(customerId);

		request.setCUSTC(customerId);

		return cpiReportsService.getResponse(url, request);
	}

	public CPIResponse getPopYardReportResponse() throws SOAPException
	{
		final String url = Config.getParameter(POP_YARD_REPORT_URL_KEY);
		final com.jsl.cpi.report.dto.pop.yard.ObjectFactory of = new com.jsl.cpi.report.dto.pop.yard.ObjectFactory();
		final ZSD35CHYBRIS request = of.createZSD35CHYBRIS();
		request.setZSD35CHYBRIS(of.createZTTPOPHYBRIS());
		final String customerId = getCustomerId();
		LOG.info(customerId);

		request.setCUSTOMER(customerId);

		return cpiReportsService.getResponse(url, request);
	}

	public CPIResponse getAccountStatementReportResponse() throws SOAPException
	{
		final String url = Config.getParameter(ACCOUNT_STATEMENT_REPORT_URL_KEY);
		final com.jsl.cpi.report.dto.accountstatement.ObjectFactory of = new com.jsl.cpi.report.dto.accountstatement.ObjectFactory();
		final ZFI137HYBRIS request = of.createZFI137HYBRIS();
		request.setZFI37TABLE(of.createZTTFI37HYBRIS());
		final String customerId = getCustomerId();
		LOG.info(customerId);

		request.setCUSTOMER(customerId);

		return cpiReportsService.getResponse(url, request);
	}

	//Changes for Vehicle Tracking
	public CPIResponse getVehicleTrackingReportResonse() throws SOAPException
	{
		final String url = Config.getParameter(VEHICLE_TRACKING_REPORT_URL_KEY);
		final com.jsl.cpi.report.dto.vehicle.tracking.ObjectFactory of = new com.jsl.cpi.report.dto.vehicle.tracking.ObjectFactory();
		final ZSDTRACKDELHYBRIS request = of.createZSDTRACKDELHYBRIS();
		request.setITVEHTACK(of.createZSDVEHTRACKTT());
		final String customerId = getCustomerId();
		LOG.info(customerId);

		request.setCUSTOMER(customerId);

		return cpiReportsService.getResponse(url, request);
	}


	public CPIResponse getSalesReportResponse() throws SOAPException
	{
		final String url = Config.getParameter(SALES_REPORT_URL_KEY);
		final com.jsl.cpi.report.dto.sales.ObjectFactory of = new com.jsl.cpi.report.dto.sales.ObjectFactory();
		final ZSD34HYBRIS request = of.createZSD34HYBRIS();
		request.setZSD34HYBRIS(of.createZTTSALESACCOUNTHYBRIS());
		final String customerId = getCustomerId();
		LOG.info(customerId);
		request.setCUSTC(customerId);

		return cpiReportsService.getResponse(url, request);
	}

	public CPIResponse getOrderBookingReportResponse() throws SOAPException
	{
		final String url = Config.getParameter(ORDER_BOOKING_REPORT_URL_KEY);
		final com.jsl.cpi.report.dto.orderbooking.ObjectFactory of = new com.jsl.cpi.report.dto.orderbooking.ObjectFactory();
		final ZSD35HYBRIS request = of.createZSD35HYBRIS();
		final ZTTORDERBOOKINGHYBRIS hybris = of.createZTTORDERBOOKINGHYBRIS();
		request.setZSD35HYBRIS(hybris);
		final String customerId = getCustomerId();
		LOG.info(customerId);
		request.setCUSTC(customerId);

		return cpiReportsService.getResponse(url, request);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.jsl.cpi.integration.service.CpiReportsService#getCustomerDashboardResponse()
	 */
	@Override
	public CPIResponse getCustomerDashboardResponse() throws SOAPException
	{
		// XXX Auto-generated method stub
		final String url = Config.getParameter(CUSTOMER_DASHBOARD_URL_KEY);
		final com.jsl.cpi.report.dto.customer.dashboard.ObjectFactory of = new com.jsl.cpi.report.dto.customer.dashboard.ObjectFactory();
		final ZSDCUSTDASHBOARDHYBRIS request = of.createZSDCUSTDASHBOARDHYBRIS();
		request.setZFI60MHYBRIS(of.createZFITTACCSUMM());
		request.setZSD34HYBRIS(of.createZSDTTBILLSUMM());
		request.setZSD35CHYBRIS(of.createZSDTTPOPYARD());
		request.setZSD35HYBRIS(of.createZSDTTBILLSUMM());
		request.setZSD46AHYBRIS(of.createZSDTTPOPYARD());
		final String customerId = getCustomerId();
		request.setCUSTOMER(customerId);
		LOG.info(customerId);
		return cpiReportsService.getResponse(url, request);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.jsl.cpi.integration.service.CpiReportsService#gePopMouValue()
	 */
	@Override
	public BigDecimal gePopMouValue() throws SOAPException
	{
		final String url = Config.getParameter(POP_MOU_URL_KEY);
		final com.jsl.cpi.pop.mou.dto.ObjectFactory of = new com.jsl.cpi.pop.mou.dto.ObjectFactory();
		final ZSDPOPVALUEFORCARTHYBRIS request = of.createZSDPOPVALUEFORCARTHYBRIS();
		final String customerId = getCustomerId();
		request.setCUSTC(customerId);
		LOG.info(customerId);
		return ((ZSDPOPVALUEFORCARTHYBRISResponse) cpiReportsService.getResponse(url, request)).getPOPVALUE();
	}

	@Override
	public CPIResponse getInvoiceDetailsByInvoiceNumber(final String invoiceNumber) throws SOAPException
	{
		// XXX Auto-generated method stub
		final String url = Config.getParameter(INVOICE_BATCH_DETAILS_URL_KEY);
		final com.jsl.cpi.invoice.batch.dto.ObjectFactory of = new com.jsl.cpi.invoice.batch.dto.ObjectFactory();
		final ZFMINVBATCHHYBRIS request = of.createZFMINVBATCHHYBRIS();
		request.setITCHARFIELD(of.createZTTHYBBATCHCHAR());
		request.setITOUTPUT(of.createZTTBATCHHYBRIS());
		request.setVBELN(invoiceNumber);
		return cpiReportsService.getResponse(url, request);
	}

	private String getCustomerId()
	{

		final UserModel user = getUserService().getCurrentUser();

		final B2BUnitModel parentb2bUnit = JslB2bUnitUtil.getParentB2bUnit(user);

		if (parentb2bUnit != null)
		{
			return parentb2bUnit.getUid();
		}
		else
		{
			return user.getUid();
		}
	}

	/**
	 * @return the cpiReportsService
	 */
	public CpiReportsConnectionService cpiReportsConnectionServiceImpl()
	{
		return cpiReportsService;
	}

	/**
	 * @param cpiReportsService
	 *           the cpiReportsService to set
	 */
	public void setCpiReportsService(final CpiReportsConnectionService cpiReportsService)
	{
		this.cpiReportsService = cpiReportsService;
	}


	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}


	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

}
