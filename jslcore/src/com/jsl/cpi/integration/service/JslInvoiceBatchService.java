/**
 * 
 */
package com.jsl.cpi.integration.service;

import javax.xml.soap.SOAPException;

import com.jsl.cpi.report.dto.CPIResponse;


/**
 * @author himanshu.sial
 *
 */
public interface JslInvoiceBatchService
{
	public CPIResponse getBatchesFromInvoiceResponse(String batchNumber) throws SOAPException;
}
