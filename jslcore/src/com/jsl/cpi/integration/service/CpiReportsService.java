/**
 *
 */
package com.jsl.cpi.integration.service;

import java.math.BigDecimal;

import javax.xml.soap.SOAPException;

import com.jsl.cpi.report.dto.CPIResponse;


/**
 * @author himanshu.sial
 *
 */
public interface CpiReportsService
{

	public CPIResponse getPopPlantReportResponse() throws SOAPException;

	public CPIResponse getPopYardReportResponse() throws SOAPException;

	public CPIResponse getSalesReportResponse() throws SOAPException;

	public CPIResponse getOrderBookingReportResponse() throws SOAPException;

	public CPIResponse getCustomerDashboardResponse() throws SOAPException;

	public CPIResponse getAccountStatementReportResponse() throws SOAPException;

	public CPIResponse getVehicleTrackingReportResonse() throws SOAPException;

	public BigDecimal gePopMouValue() throws SOAPException;

	public CPIResponse getInvoiceDetailsByInvoiceNumber(String invoiceNumber) throws SOAPException;


}
