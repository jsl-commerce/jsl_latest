/**
 *
 */
package com.jsl.initialsetup.serviceimpl;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupParameter;

import java.util.List;

import org.apache.log4j.Logger;


/**
 * @author himanshu.sial
 *
 */
@SystemSetup(extension = "jslcore")
public class JslUpdateSetup extends AbstractSystemSetup
{

	private static final Logger LOG = Logger.getLogger(JslUpdateSetup.class);

	@SystemSetup(type = Type.ALL, process = Process.UPDATE)
	public void updateColumnsSize()
	{
		/*
		 * Connection conn = null; PreparedStatement pstmt = null; try { conn =
		 * Registry.getCurrentTenant().getDataSource().getConnection();
		 *
		 * // pstmt = conn.prepareStatement(
		 * //"alter table customercomplaint alter column p_complaintdescription nvarchar(2000)"); pstmt.execute();
		 *
		 * //pstmt =
		 * conn.prepareStatement("alter table customercomplaint alter column p_settlementremarks nvarchar(4000)"); pstmt =
		 * conn.prepareStatement("alter table b2bdocument add constraint UC_INVOICENUMBER unique(p_invoicenumber)");
		 * pstmt.execute();
		 *
		 * } catch (final SQLException e) { e.printStackTrace(); LOG.error("Unable to alter database column!"); } finally
		 * { Utilities.tryToCloseJDBC(conn, pstmt, null); }
		 *
		 */
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.commerceservices.setup.AbstractSystemSetup#getInitializationOptions()
	 */
	@Override
	public List<SystemSetupParameter> getInitializationOptions()
	{
		// XXX Auto-generated method stub
		return null;
	}
}
