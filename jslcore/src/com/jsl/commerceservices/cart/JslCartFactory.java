/**
 *
 */
package com.jsl.commerceservices.cart;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commerceservices.order.impl.CommerceCartFactory;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.user.UserService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.jsl.core.enums.JslCartType;
import com.jsl.core.serviceability.services.JslWarehouseServiceabilityService;
import com.jsl.core.util.JslB2bUnitUtil;


/**
 * @author manav.magoo
 *
 */
public class JslCartFactory extends CommerceCartFactory
{
	@Autowired
	JslWarehouseServiceabilityService jslWarehouseServiceabilityService;

	@Autowired
	UserService userService;

	private static final Logger LOG = Logger.getLogger(JslCartFactory.class);


	@Override
	protected CartModel createCartInternal()
	{
		final CartModel cart = super.createCartInternal();
		cart.setType(JslCartType.WEB);

		final UserModel user = cart.getUser();

		final B2BUnitModel parentb2bUnit = JslB2bUnitUtil.getParentB2bUnit(user);

		final AddressModel billingAddress = getBillingAddress(parentb2bUnit);
		final RegionModel region = billingAddress != null ? billingAddress.getRegion() : null;

		setWarehouse(cart, parentb2bUnit.getSalesOffice(), region);

		return cart;
	}

	private AddressModel getBillingAddress(final B2BUnitModel parentb2bUnit)
	{
		return parentb2bUnit.getAddresses().stream().filter(address -> address.getBillingAddress() == true).findAny().orElse(null);
	}

	/**
	 * @param cart
	 * @param postalCode
	 */
	private void setWarehouse(final CartModel cart, final String salesOffice, final RegionModel region)
	{

		final WarehouseModel warehouse;
		if (region != null)
		{
			warehouse = jslWarehouseServiceabilityService.getServiceableWareHouse(salesOffice, cart.getSite(), region);
		}
		else
		{
			warehouse = jslWarehouseServiceabilityService.getServiceableWareHouse(salesOffice, cart.getSite());
		}

		cart.setWarehouse(warehouse);
	}

}
