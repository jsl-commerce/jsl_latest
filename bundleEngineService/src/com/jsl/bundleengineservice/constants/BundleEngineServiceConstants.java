package com.jsl.bundleengineservice.constants;

@SuppressWarnings(
{ "deprecation", "PMD", "squid:CallToDeprecatedMethod" })
public class BundleEngineServiceConstants extends GeneratedBundleEngineServiceConstants
{
	public static final String EXTENSIONNAME = "bundleEngineService";

	private BundleEngineServiceConstants()
	{
		//empty
	}


}
