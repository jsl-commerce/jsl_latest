/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.bundleengineservice.service;

import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;

import java.util.List;

import com.jsl.bundleengineservice.rao.AuctionProductRAO;




/**
 *
 */
public interface BundleService
{

	/**
	 *
	 */
	List<BundleTemplateModel> createBundle(String bundle_name, String catalog_id, double minWeight, double maxWeight,
			List<AuctionProductRAO> productsRaoList);
}
