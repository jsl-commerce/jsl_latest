/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.bundleengineservice.service.impl;

import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.promotions.model.PromotionGroupModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.ruleengine.RuleEngineService;
import de.hybris.platform.ruleengine.RuleEvaluationContext;
import de.hybris.platform.ruleengine.RuleEvaluationResult;
import de.hybris.platform.ruleengine.enums.RuleType;
import de.hybris.platform.ruleengine.exception.RuleEngineRuntimeException;
import de.hybris.platform.ruleengine.model.AbstractRuleEngineContextModel;
import de.hybris.platform.ruleengine.strategies.RuleEngineContextFinderStrategy;
import de.hybris.platform.ruleengineservices.action.RuleActionService;
import de.hybris.platform.ruleengineservices.enums.FactContextType;
import de.hybris.platform.ruleengineservices.rao.providers.FactContextFactory;
import de.hybris.platform.ruleengineservices.rao.providers.RAOProvider;
import de.hybris.platform.ruleengineservices.rao.providers.impl.FactContext;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.jsl.bundleengineservice.service.BundleEngineService;
import com.jsl.configurablebundleservice.service.JslBundleTemplateService;


/**
 *
 */
public class DefaultBundleEngineService implements BundleEngineService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultBundleEngineService.class);


	private FactContextFactory factContextFactory;
	private RuleEngineContextFinderStrategy ruleEngineContextFinderStrategy;
	private RuleEngineService commerceRuleEngineService;
	private SessionService sessionService;
	private RuleActionService ruleActionService;
	private ModelService modelService;
	private JslBundleTemplateService jslBundleTemplateService;


	@Override
	public void updateAuctionPromotions(final Collection<PromotionGroupModel> promotionGroups, final CatalogModel catalog)
	{
		try
		{
			getJslBundleTemplateService().resetCounter();
			final RuleEvaluationResult ruleEvaluationResult = this.evaluate(catalog, promotionGroups);
			if (!ruleEvaluationResult.isEvaluationFailed())
			{
				final List<ItemModel> applyAllActionModels = this.getRuleActionService()
						.applyAllActions(ruleEvaluationResult.getResult());
				applyAllActionModels.stream().filter((item) -> {
					return item instanceof PromotionResultModel;
				}).map((item) -> {
					return (PromotionResultModel) item;
				}).collect(Collectors.toList());
			}
		}
		catch (final RuleEngineRuntimeException ex)
		{
			LOGGER.error(ex.getMessage(), ex);
		}


	}

	@Override
	public RuleEvaluationResult evaluate(final CatalogModel catalog, final Collection<PromotionGroupModel> promotionGroups)
	{
		final List<Object> facts = new ArrayList();
		facts.add(catalog);
		facts.addAll(promotionGroups);
		try
		{
			final RuleEvaluationContext context = this.prepareContext(
					this.getFactContextFactory().createFactContext(FactContextType.PROMOTION_AUCTION, facts),
					this.determineRuleEngineContext(catalog));

			return this.getCommerceRuleEngineService().evaluate(context);
		}
		catch (final IllegalStateException var7)
		{

			LOGGER.error("Promotion rule evaluation failed", var7);
			final RuleEvaluationResult result = new RuleEvaluationResult();
			result.setErrorMessage(var7.getMessage());
			result.setEvaluationFailed(true);
			return result;
		}
	}

	protected RuleEvaluationContext prepareContext(final FactContext factContext,
			final AbstractRuleEngineContextModel ruleEngineContext)
	{
		final Set<Object> convertedFacts = this.provideRAOs(factContext);
		final RuleEvaluationContext evaluationContext = new RuleEvaluationContext();
		evaluationContext.setRuleEngineContext(ruleEngineContext);
		evaluationContext.setFacts(convertedFacts);
		return evaluationContext;
	}

	protected Set<Object> provideRAOs(final FactContext factContext)
	{
		final Set<Object> result = new HashSet();
		final Iterator var4 = factContext.getFacts().iterator();

		while (var4.hasNext())
		{
			final Object fact = var4.next();
			final Iterator var6 = factContext.getProviders(fact).iterator();

			while (var6.hasNext())
			{
				final RAOProvider raoProvider = (RAOProvider) var6.next();
				result.addAll(raoProvider.expandFactModel(fact));
			}
		}

		return result;
	}


	protected AbstractRuleEngineContextModel determineRuleEngineContext(final CatalogModel catalogModel)
	{

		final Collection<CatalogVersionModel> catalogversionList = new ArrayList<>();
		catalogversionList.add(catalogModel.getActiveCatalogVersion());

		return this.getRuleEngineContextFinderStrategy()
				.getRuleEngineContextForCatalogVersions(catalogversionList, RuleType.AUCTION).orElseThrow(() -> {
					return new IllegalStateException("No rule engine context could be derived for Product Model");
				});
	}


	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected RuleActionService getRuleActionService()
	{
		return this.ruleActionService;
	}

	@Required
	public void setRuleActionService(final RuleActionService ruleActionService)
	{
		this.ruleActionService = ruleActionService;
	}

	protected SessionService getSessionService()
	{
		return this.sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	protected FactContextFactory getFactContextFactory()
	{
		return this.factContextFactory;
	}

	@Required
	public void setFactContextFactory(final FactContextFactory factContextFactory)
	{
		this.factContextFactory = factContextFactory;
	}


	protected RuleEngineContextFinderStrategy getRuleEngineContextFinderStrategy()
	{
		return this.ruleEngineContextFinderStrategy;
	}

	@Required
	public void setRuleEngineContextFinderStrategy(final RuleEngineContextFinderStrategy ruleEngineContextFinderStrategy)
	{
		this.ruleEngineContextFinderStrategy = ruleEngineContextFinderStrategy;
	}

	protected RuleEngineService getCommerceRuleEngineService()
	{
		return this.commerceRuleEngineService;
	}


	@Required
	public void setCommerceRuleEngineService(final RuleEngineService ruleEngineService)
	{
		this.commerceRuleEngineService = ruleEngineService;
	}

	public JslBundleTemplateService getJslBundleTemplateService()
	{
		return jslBundleTemplateService;
	}

	public void setJslBundleTemplateService(final JslBundleTemplateService jslBundleTemplateService)
	{
		this.jslBundleTemplateService = jslBundleTemplateService;
	}

}
