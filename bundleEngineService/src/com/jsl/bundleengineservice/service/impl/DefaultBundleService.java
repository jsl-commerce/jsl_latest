/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.bundleengineservice.service.impl;

import de.hybris.platform.catalog.CatalogService;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.jsl.bundleengineservice.rao.AuctionProductRAO;
import com.jsl.bundleengineservice.service.BundleService;
import com.jsl.configurablebundleservice.service.JslBundleTemplateService;
import com.jsl.core.services.JslProductService;


/**
 *
 */
public class DefaultBundleService implements BundleService
{

	private JslBundleTemplateService jslBundleTemplateService;
	private CatalogService catalogService;
	private JslProductService jslProductService;
	private ModelService modelService;

	private static final Logger LOG = Logger.getLogger(DefaultBundleService.class);

	@Override
	public List<BundleTemplateModel> createBundle(final String bundle_name, final String catalog_id, final double minWeight,
			final double maxWeight, final List<AuctionProductRAO> productsRaoList)
	{
		final List<BundleTemplateModel> bundleTemplatesList = new ArrayList<>();

		final CatalogModel catalog = getCatalogService().getCatalogForId(catalog_id);

		if (!productsRaoList.isEmpty())
		{
			final HashMap<Integer, List<AuctionProductRAO>> hashMapRaos = bifurcateRaosListOnWeight(maxWeight, minWeight,
					productsRaoList);

			hashMapRaos.entrySet().forEach(entry -> {
				final BundleTemplateModel bundleTemplate = getJslBundleTemplateService().createBundleTemplate(
						bundle_name + "." + entry.getKey(), getProducts(hashMapRaos.get(entry.getKey()), catalog),
						catalog.getActiveCatalogVersion());

				if (bundleTemplate != null)
				{
					bundleTemplatesList.add(bundleTemplate);
				}
			});
		}

		return bundleTemplatesList;
	}

	private List<ProductModel> getProducts(final List<AuctionProductRAO> filteredRaoList, final CatalogModel catalog)
	{
		final List<String> raoIds = new ArrayList<>();
		for (final AuctionProductRAO rao : filteredRaoList)
		{
			raoIds.add(rao.getCode());
		}

		return getJslProductService().getAuctionProductsForIds(raoIds, catalog.getActiveCatalogVersion());
	}

	/**
	 * This method return hashmap of segregated Rao's on basis of min weight and max weight every list will have weight
	 * in range of min weight and max weight
	 */
	private HashMap<Integer, List<AuctionProductRAO>> bifurcateRaosListOnWeight(final Double maxWeight, final Double minWeight,
			final List<AuctionProductRAO> raoList)
	{
		final HashMap<Integer, List<AuctionProductRAO>> hashmap = new HashMap<>();
		sortByWeight(raoList);
		int key = 0;
		if (maxWeight > 0)
		{
			double weightSum = 0.0;
			List<AuctionProductRAO> filteredList = new ArrayList<>();
			for (final AuctionProductRAO auctionRao : raoList)
			{
				weightSum += auctionRao.getWeight();

				if (weightSum <= maxWeight)
				{
					filteredList.add(auctionRao);
				}
				else if (!filteredList.isEmpty() && (weightSum - auctionRao.getWeight()) >= minWeight
						&& (weightSum - auctionRao.getWeight()) <= maxWeight)
				{

					hashmap.put(key, new ArrayList<AuctionProductRAO>(filteredList)); // list is already carrying product of max weight allowed

					filteredList = new ArrayList<>(); // Hence, making new list and adding the product in loop
					filteredList.add(auctionRao);

					weightSum = 0.0; //Weight reset
					weightSum += auctionRao.getWeight(); // weight added of product added in newely created list

					key++; // Key incremented for new code of hashmap for second iteration
				}
				else
				{
					weightSum = 0.0; //Weight reset
					filteredList.clear();
				}
			}

			if (weightSum >= minWeight && weightSum <= maxWeight && !filteredList.isEmpty()) // This case will run when list carrying products having weight less then max weight allowed
			{
				hashmap.put(key, filteredList);
			}

			return hashmap;
		}

		return hashmap;
	}


	/**
	 *
	 */
	private void sortByWeight(final List<AuctionProductRAO> raoList)
	{
		Collections.sort(raoList, new AuctionProductRAOComparator());

	}

	private class AuctionProductRAOComparator implements Comparator<AuctionProductRAO>
	{

		@Override
		public int compare(final AuctionProductRAO o1, final AuctionProductRAO o2)
		{
			if (o1.getWeight() < o2.getWeight())
			{
				return -1;
			}
			if (o1.getWeight() > o2.getWeight())
			{
				return 1;
			}
			return 0;
		}

	}


	/**
	 * @return the catalogService
	 */
	public CatalogService getCatalogService()
	{
		return catalogService;
	}

	/**
	 * @param catalogService
	 *           the catalogService to set
	 */
	@Required
	public void setCatalogService(final CatalogService catalogService)
	{
		this.catalogService = catalogService;
	}

	/**
	 * @return the jslBundleTemplateService
	 */
	public JslBundleTemplateService getJslBundleTemplateService()
	{
		return jslBundleTemplateService;
	}

	/**
	 * @param jslBundleTemplateService
	 *           the jslBundleTemplateService to set
	 */
	@Required
	public void setJslBundleTemplateService(final JslBundleTemplateService jslBundleTemplateService)
	{
		this.jslBundleTemplateService = jslBundleTemplateService;
	}

	/**
	 * @return the jslProductService
	 */
	public JslProductService getJslProductService()
	{
		return jslProductService;
	}

	/**
	 * @param jslProductService
	 *           the jslProductService to set
	 */
	@Required
	public void setJslProductService(final JslProductService jslProductService)
	{
		this.jslProductService = jslProductService;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

}
