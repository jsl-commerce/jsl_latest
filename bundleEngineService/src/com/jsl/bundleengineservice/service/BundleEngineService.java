/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.bundleengineservice.service;

import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.promotions.model.PromotionGroupModel;
import de.hybris.platform.ruleengine.RuleEvaluationResult;

import java.util.Collection;


/**
 *
 */
public interface BundleEngineService
{
	RuleEvaluationResult evaluate(CatalogModel var1, Collection<PromotionGroupModel> var2);

	void updateAuctionPromotions(Collection<PromotionGroupModel> var1, CatalogModel var2);
}
