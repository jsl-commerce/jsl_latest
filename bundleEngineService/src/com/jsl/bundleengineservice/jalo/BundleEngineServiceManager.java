package com.jsl.bundleengineservice.jalo;


import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;

import org.apache.log4j.Logger;

import com.jsl.bundleengineservice.constants.BundleEngineServiceConstants;


@SuppressWarnings("PMD")
public class BundleEngineServiceManager extends GeneratedBundleEngineServiceManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger(BundleEngineServiceManager.class.getName());

	public static final BundleEngineServiceManager getInstance()
	{
		final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (BundleEngineServiceManager) em.getExtension(BundleEngineServiceConstants.EXTENSIONNAME);
	}

}
