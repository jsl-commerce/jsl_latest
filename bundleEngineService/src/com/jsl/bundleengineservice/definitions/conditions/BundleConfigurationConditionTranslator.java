/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.bundleengineservice.definitions.conditions;

import de.hybris.platform.ruleengineservices.compiler.RuleCompilerContext;
import de.hybris.platform.ruleengineservices.compiler.RuleConditionTranslator;
import de.hybris.platform.ruleengineservices.compiler.RuleIrAttributeCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrAttributeOperator;
import de.hybris.platform.ruleengineservices.compiler.RuleIrCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrGroupCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrGroupOperator;
import de.hybris.platform.ruleengineservices.rule.data.RuleConditionData;
import de.hybris.platform.ruleengineservices.rule.data.RuleConditionDefinitionData;
import de.hybris.platform.ruleengineservices.rule.data.RuleParameterData;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jsl.bundleengineservice.enums.BundleConfigurationAttribute;
import com.jsl.bundleengineservice.rao.AuctionProductRAO;


/**
 *
 */
public class BundleConfigurationConditionTranslator implements RuleConditionTranslator
{

	private static final Logger LOG = Logger.getLogger(BundleConfigurationConditionTranslator.class);

	@Override
	public RuleIrCondition translate(final RuleCompilerContext context, final RuleConditionData condition,
			final RuleConditionDefinitionData conditionDefinition)
	{
		LOG.info("In Bundle configuration Condition Translator, rule : " + context.getRule().getName());
		final RuleParameterData minlength = condition.getParameters().get(BundleConfigurationAttribute.MINLENGTH.getValue());
		final RuleParameterData maxlength = condition.getParameters().get(BundleConfigurationAttribute.MAXLENGTH.getValue());
		final RuleParameterData minwidth = condition.getParameters().get(BundleConfigurationAttribute.MINWIDTH.getValue());
		final RuleParameterData maxwidth = condition.getParameters().get(BundleConfigurationAttribute.MAXWIDTH.getValue());
		final RuleParameterData minthickness = condition.getParameters().get(BundleConfigurationAttribute.MINTHICKNESS.getValue());
		final RuleParameterData maxthickness = condition.getParameters().get(BundleConfigurationAttribute.MAXTHICKNESS.getValue());
		final RuleParameterData location = condition.getParameters().get(BundleConfigurationAttribute.LOCATION.getValue());
		final List<String> quality = (List<String>) condition.getParameters().get(BundleConfigurationAttribute.QUALITY.getValue())
				.getValue();
		final List<String> materials = (List<String>) condition.getParameters()
				.get(BundleConfigurationAttribute.MATERIALNUM.getValue()).getValue();
		final List<String> gradegroup = (List<String>) condition.getParameters()
				.get(BundleConfigurationAttribute.GRADEGROUP.getValue()).getValue();
		final List<String> plant = (List<String>) condition.getParameters().get(BundleConfigurationAttribute.PLANT.getValue())
				.getValue();

		final String auctionProductRaoVariable = context.generateVariable(AuctionProductRAO.class);
		final RuleIrGroupCondition irGroupCondition = new RuleIrGroupCondition();
		irGroupCondition.setOperator(RuleIrGroupOperator.AND);
		final List<RuleIrCondition> children = new ArrayList<>();

		if (minlength.getValue() != null && !minlength.getValue().toString().isEmpty())
		{
			final RuleIrAttributeCondition minlengthCondition = new RuleIrAttributeCondition();
			minlengthCondition.setVariable(auctionProductRaoVariable);
			minlengthCondition.setAttribute(BundleConfigurationAttribute.LENGTH.getValue());
			minlengthCondition.setOperator(RuleIrAttributeOperator.GREATER_THAN_OR_EQUAL);
			minlengthCondition.setValue(minlength.getValue());
			children.add(minlengthCondition);
		}

		if (maxlength.getValue() != null && !maxlength.getValue().toString().isEmpty())
		{
			final RuleIrAttributeCondition maxlengthCondition = new RuleIrAttributeCondition();
			maxlengthCondition.setVariable(auctionProductRaoVariable);
			maxlengthCondition.setAttribute(BundleConfigurationAttribute.LENGTH.getValue());
			maxlengthCondition.setOperator(RuleIrAttributeOperator.LESS_THAN_OR_EQUAL);
			maxlengthCondition.setValue(maxlength.getValue());
			children.add(maxlengthCondition);
		}

		final RuleIrAttributeCondition minwidthCondition = new RuleIrAttributeCondition();
		minwidthCondition.setVariable(auctionProductRaoVariable);
		minwidthCondition.setAttribute(BundleConfigurationAttribute.WIDTH.getValue());
		minwidthCondition.setOperator(RuleIrAttributeOperator.GREATER_THAN_OR_EQUAL);
		minwidthCondition.setValue(minwidth.getValue());

		final RuleIrAttributeCondition maxwidthCondition = new RuleIrAttributeCondition();
		maxwidthCondition.setVariable(auctionProductRaoVariable);
		maxwidthCondition.setAttribute(BundleConfigurationAttribute.WIDTH.getValue());
		maxwidthCondition.setOperator(RuleIrAttributeOperator.LESS_THAN_OR_EQUAL);
		maxwidthCondition.setValue(maxwidth.getValue());


		final RuleIrAttributeCondition minthicknessCondition = new RuleIrAttributeCondition();
		minthicknessCondition.setVariable(auctionProductRaoVariable);
		minthicknessCondition.setAttribute(BundleConfigurationAttribute.THICKNESS.getValue());
		minthicknessCondition.setOperator(RuleIrAttributeOperator.GREATER_THAN_OR_EQUAL);
		minthicknessCondition.setValue(minthickness.getValue());


		final RuleIrAttributeCondition maxthicknessCondition = new RuleIrAttributeCondition();
		maxthicknessCondition.setVariable(auctionProductRaoVariable);
		maxthicknessCondition.setAttribute(BundleConfigurationAttribute.THICKNESS.getValue());
		maxthicknessCondition.setOperator(RuleIrAttributeOperator.LESS_THAN_OR_EQUAL);
		maxthicknessCondition.setValue(maxthickness.getValue());

		final RuleIrAttributeCondition materialnumCondition = new RuleIrAttributeCondition();
		materialnumCondition.setVariable(auctionProductRaoVariable);
		materialnumCondition.setAttribute(BundleConfigurationAttribute.MATERIALNUM.getValue());
		materialnumCondition.setOperator(RuleIrAttributeOperator.IN);
		materialnumCondition.setValue(materials);

		final RuleIrAttributeCondition locationCondition = new RuleIrAttributeCondition();
		locationCondition.setVariable(auctionProductRaoVariable);
		locationCondition.setAttribute(BundleConfigurationAttribute.LOCATION.getValue());
		locationCondition.setOperator(RuleIrAttributeOperator.EQUAL);
		locationCondition.setValue(location.getValue());

		final RuleIrAttributeCondition gradegroupCondition = new RuleIrAttributeCondition();
		gradegroupCondition.setVariable(auctionProductRaoVariable);
		gradegroupCondition.setAttribute(BundleConfigurationAttribute.GRADEGROUP.getValue());
		gradegroupCondition.setOperator(RuleIrAttributeOperator.IN);
		gradegroupCondition.setValue(gradegroup);

		final RuleIrAttributeCondition plantCondition = new RuleIrAttributeCondition();
		plantCondition.setVariable(auctionProductRaoVariable);
		plantCondition.setAttribute(BundleConfigurationAttribute.PLANT.getValue());
		plantCondition.setOperator(RuleIrAttributeOperator.IN);
		plantCondition.setValue(plant);

		final RuleIrAttributeCondition qualityCondition = new RuleIrAttributeCondition();
		qualityCondition.setVariable(auctionProductRaoVariable);
		qualityCondition.setAttribute(BundleConfigurationAttribute.QUALITY.getValue());
		qualityCondition.setOperator(RuleIrAttributeOperator.IN);
		qualityCondition.setValue(quality);

		final RuleIrAttributeCondition isBundledCondition = new RuleIrAttributeCondition();
		isBundledCondition.setVariable(auctionProductRaoVariable);
		isBundledCondition.setAttribute("bundled");
		isBundledCondition.setOperator(RuleIrAttributeOperator.EQUAL);
		isBundledCondition.setValue(false);

		children.add(minwidthCondition);
		children.add(maxwidthCondition);
		children.add(minthicknessCondition);
		children.add(maxthicknessCondition);
		children.add(materialnumCondition);
		children.add(locationCondition);
		children.add(gradegroupCondition);
		children.add(qualityCondition);
		children.add(isBundledCondition);
		irGroupCondition.setChildren(children);

		LOG.info("Bundle configuration Translator End for rule : " + context.getRule().getName());
		return irGroupCondition;
	}

}
