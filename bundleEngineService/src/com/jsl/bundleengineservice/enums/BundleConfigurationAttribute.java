/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.bundleengineservice.enums;

/**
 *
 */
public enum BundleConfigurationAttribute
{
	MINLENGTH("minlength"),

	MAXLENGTH("maxlength"),

	MINWIDTH("minwidth"),

	MAXWIDTH("maxwidth"),

	MINTHICKNESS("minthickness"),

	MAXTHICKNESS("maxthickness"),

	MATERIALNUM("materialNum"),

	GRADEGROUP("gradeGroup"),

	PLANT("plant"),

	LOCATION("location"),

	LENGTH("length"),

	WIDTH("width"),

	THICKNESS("thickness"),

	BUNDLED("bundled"),

	QUALITY("quality");

	private String value;

	private BundleConfigurationAttribute(final String value)
	{
		this.value = value;
	}

	/**
	 * @return value
	 */
	public String getValue()
	{
		return value;
	}
}
