/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.bundleengineservice.providers;

import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.ruleengineservices.rao.providers.ExpandedRAOProvider;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.jsl.bundleengineservice.rao.AuctionProductRAO;
import com.jsl.core.model.AuctionProductModel;


/**
 *
 */
public class AuctionProductsRaoProvider implements ExpandedRAOProvider
{

	private Converter<ProductModel, AuctionProductRAO> auctionProductRaoConverter;
	private ProductService productService;




	@Override
	public Set expandFactModel(final Object var1)
	{
		return this.expandFactModel(var1, null);
	}

	@Override
	public Set expandFactModel(final Object var1, final Collection var2)
	{
		final Set<AuctionProductRAO> facts = new HashSet<>();
		// XXX Auto-generated method stub
		if (var1 instanceof CatalogModel)
		{
			final CatalogModel catalog = (CatalogModel) var1;
			final List<ProductModel> productModelList = getProductService()
					.getAllProductsForCatalogVersion(catalog.getActiveCatalogVersion());


			for (final ProductModel productModel : productModelList)
			{
				if (productModel instanceof AuctionProductModel)
				{
					final AuctionProductRAO auctionProductRAO = this.getAuctionProductRaoConverter().convert(productModel);
					facts.add(auctionProductRAO);
				}
			}

		}
		return facts;
	}


	/**
	 * @return the auctionProductRaoConverter
	 */
	public Converter<ProductModel, AuctionProductRAO> getAuctionProductRaoConverter()
	{
		return auctionProductRaoConverter;
	}

	/**
	 * @param auctionProductRaoConverter
	 *           the auctionProductRaoConverter to set
	 */
	public void setAuctionProductRaoConverter(final Converter<ProductModel, AuctionProductRAO> auctionProductRaoConverter)
	{
		this.auctionProductRaoConverter = auctionProductRaoConverter;
	}

	/**
	 * @return the productService
	 */
	public ProductService getProductService()
	{
		return productService;
	}

	/**
	 * @param productService
	 *           the productService to set
	 */
	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}

}
