/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.bundleengineservice.definition.actions;

import de.hybris.platform.ruleengineservices.rule.evaluation.RuleActionContext;
import de.hybris.platform.ruleengineservices.rule.evaluation.RuleExecutableAction;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import com.jsl.bundleengineservice.rao.AuctionProductRAO;
import com.jsl.bundleengineservice.service.BundleService;
import com.jsl.configurablebundleservice.exception.JslBundleException;


/**
 *
 */
public class BundleConfigurationAction implements RuleExecutableAction
{

	private static final Logger LOG = Logger.getLogger(BundleConfigurationAction.class);

	BundleService bundleService;

	@Override
	public void executeAction(final RuleActionContext var1, final Map<String, Object> var2)
	{
		// XXX Auto-generated method stub
		try
		{
			LOG.info("In Bundle configuration Action, rule : ");
			final Set<AuctionProductRAO> set = var1.getValues(AuctionProductRAO.class);
			final String bundle_name = (String) var2.get("bundle_Name");
			final String catalog_Id = (String) var2.get("catalog_Id");
			final double minWeight = (double) var2.get("minWeight");
			final double maxWeight = (double) var2.get("maxWeight");

			getBundleService().createBundle(bundle_name, catalog_Id, minWeight, maxWeight,
					set.stream().collect(Collectors.toList()));

			LOG.info("In Bundle configuration Action, Bundle Created : " + bundle_name);
		}
		catch (final JslBundleException ex)
		{
			LOG.error("Exception occured for bundle creation :" + ex.getMessage(), ex);
		}

	}

	/**
	 * @return the bundleService
	 */
	public BundleService getBundleService()
	{
		return bundleService;
	}

	/**
	 * @param bundleService
	 *           the bundleService to set
	 */
	public void setBundleService(final BundleService bundleService)
	{
		this.bundleService = bundleService;
	}



}