/**
 *
 */
package com.jsl.facades.product.enquiry;

import java.util.List;
import java.util.Map;

import com.jsl.core.dto.ProductEnquiry;


/**
 * @author sunil.maurya.kumar
 *
 */
public interface JslProductEnquiryFacades
{


	Map<String, String> productEnquiryCreate(ProductEnquiry productEnquiry);

	String getProducts();

	List<ProductEnquiry> getProductEnquiryHistory();
}
