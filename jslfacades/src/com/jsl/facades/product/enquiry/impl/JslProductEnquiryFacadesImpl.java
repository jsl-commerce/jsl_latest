/**
 *
 */
package com.jsl.facades.product.enquiry.impl;

import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.google.common.base.Strings;
import com.google.common.io.Resources;
import com.jsl.core.constants.JslCoreConstants;
import com.jsl.core.dto.ProductEnquiry;
import com.jsl.core.model.ProductEnquiryModel;
import com.jsl.core.product.enquiry.service.JslProductEnquiryService;
import com.jsl.facades.exceptions.JslProductEnquiryException;
import com.jsl.facades.product.enquiry.JslProductEnquiryFacades;


/**
 * @author sunil.maurya.kumar
 *
 */
public class JslProductEnquiryFacadesImpl implements JslProductEnquiryFacades
{

	private static final Logger LOG = Logger.getLogger(JslProductEnquiryFacadesImpl.class);

	private KeyGenerator enquiryProductKeyGenerator;

	private JslProductEnquiryService jslProductEnquiryService;

	private static String productsJson;

	@Resource
	private ModelService modelService;


	@Resource(name = "jslProductEnquiryConverter")
	private Converter<ProductEnquiry, ProductEnquiryModel> jslProductEnquiryConverter;

	@Resource(name = "jslProductEnquiryHistoryConverter")
	private Converter<ProductEnquiryModel, ProductEnquiry> jslProductEnquiryHistoryConverter;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.jsl.facades.product.enquiry.JslProductEnquiryFacades#getProducts()
	 */
	@Override
	public String getProducts()
	{

		if (Strings.isNullOrEmpty(productsJson))
		{
			try
			{
				productsJson = new String(Files.readAllBytes(Paths.get(Resources.getResource("products.json").toURI())));
			}
			catch (final IOException | URISyntaxException e)
			{
				LOG.error("Error in parsing json : " + e.getMessage());
			}
		}
		return productsJson;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.jsl.facades.product.enquiry.JslProductEnquiryFacades#productEnquiryCreate(com.jsl.core.dto.ProductEnquiry)
	 */
	@Override
	public Map<String, String> productEnquiryCreate(final ProductEnquiry productEnquiry)
	{
		ProductEnquiryModel productEnquiryModel = new ProductEnquiryModel();
		productEnquiryModel = getJslProductEnquiryConverter().convert(productEnquiry);

		productEnquiry.setEnquiryId(getEnquiryProductKeyGenerator().generate().toString());

		final Map<String, String> responseMap = getJslProductEnquiryService().createLeadInEcc(productEnquiry);

		if (StringUtils.isEmpty(responseMap.get(JslCoreConstants.ECC_LEAD_ID)))
		{
			throw new JslProductEnquiryException("Error Occured while creating enquiry");
		}
		productEnquiryModel.setEnquiryNumber(productEnquiry.getEnquiryId());
		productEnquiryModel.setLeadId(responseMap.get(JslCoreConstants.ECC_LEAD_ID));
		productEnquiryModel.setKamName(responseMap.get(JslCoreConstants.KAM_ID));
		modelService.save(productEnquiryModel);
		LOG.info("...Model Saved Successfully...");
		responseMap.put(JslCoreConstants.PRODUCT_ENQUIRY_ID, productEnquiry.getEnquiryId());
		responseMap.remove(JslCoreConstants.ECC_LEAD_ID);
		responseMap.remove(JslCoreConstants.KAM_ID);
		return responseMap;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.jsl.facades.product.enquiry.JslProductEnquiryFacades#getProductEnquiryHistory()
	 */
	@Override
	public List<ProductEnquiry> getProductEnquiryHistory()
	{
		return getJslProductEnquiryHistoryConverter().convertAll(getJslProductEnquiryService().getProductEnquiriesByUser());
	}


	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * @return the jslProductEnquiryConverter
	 */
	public Converter<ProductEnquiry, ProductEnquiryModel> getJslProductEnquiryConverter()
	{
		return jslProductEnquiryConverter;
	}

	/**
	 * @param jslProductEnquiryConverter
	 *           the jslProductEnquiryConverter to set
	 */
	public void setJslProductEnquiryConverter(final Converter<ProductEnquiry, ProductEnquiryModel> jslProductEnquiryConverter)
	{
		this.jslProductEnquiryConverter = jslProductEnquiryConverter;
	}

	/**
	 * @return the enquiryProductKeyGenerator
	 */
	public KeyGenerator getEnquiryProductKeyGenerator()
	{
		return enquiryProductKeyGenerator;
	}

	/**
	 * @param enquiryProductKeyGenerator
	 *           the enquiryProductKeyGenerator to set
	 */
	public void setEnquiryProductKeyGenerator(final KeyGenerator enquiryProductKeyGenerator)
	{
		this.enquiryProductKeyGenerator = enquiryProductKeyGenerator;
	}

	/**
	 * @return the jslProductEnquiryHistoryConverter
	 */
	public Converter<ProductEnquiryModel, ProductEnquiry> getJslProductEnquiryHistoryConverter()
	{
		return jslProductEnquiryHistoryConverter;
	}

	/**
	 * @param jslProductEnquiryHistoryConverter
	 *           the jslProductEnquiryHistoryConverter to set
	 */
	public void setJslProductEnquiryHistoryConverter(
			final Converter<ProductEnquiryModel, ProductEnquiry> jslProductEnquiryHistoryConverter)
	{
		this.jslProductEnquiryHistoryConverter = jslProductEnquiryHistoryConverter;
	}

	/**
	 * @return the jslProductEnquiryService
	 */
	public JslProductEnquiryService getJslProductEnquiryService()
	{
		return jslProductEnquiryService;
	}

	/**
	 * @param jslProductEnquiryService
	 *           the jslProductEnquiryService to set
	 */
	public void setJslProductEnquiryService(final JslProductEnquiryService jslProductEnquiryService)
	{
		this.jslProductEnquiryService = jslProductEnquiryService;
	}

}
