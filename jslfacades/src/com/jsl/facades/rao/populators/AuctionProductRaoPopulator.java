/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.jsl.facades.rao.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ruleengineservices.converters.populator.ProductRaoPopulator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.beans.factory.annotation.Required;

import com.jsl.bundleengineservice.rao.AuctionProductRAO;
import com.jsl.core.model.AuctionProductModel;


/**
 *
 */
public class AuctionProductRaoPopulator implements Populator<ProductModel, AuctionProductRAO>
{
	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */

	ProductRaoPopulator productRaoPopulator;

	@Override
	public void populate(final ProductModel source, final AuctionProductRAO auctionProductRao) throws ConversionException
	{
		// XXX Auto-generated method stub
		if (source instanceof AuctionProductModel)
		{
			final AuctionProductModel auctionProductModel = (AuctionProductModel) source;

			getProductRaoPopulator().populate(auctionProductModel, auctionProductRao);

			auctionProductRao.setPlant(auctionProductModel.getPlant());
			auctionProductRao.setMaterialNum(auctionProductModel.getMaterialNum());
			auctionProductRao.setQuality(auctionProductModel.getQuality());
			auctionProductRao.setSeries(auctionProductModel.getSeries());
			auctionProductRao.setGrade(auctionProductModel.getGrade());
			auctionProductRao.setGradeGroup(auctionProductModel.getGradeGroup());
			auctionProductRao.setThickness(auctionProductModel.getThickness());
			auctionProductRao.setLength(auctionProductModel.getLength());
			auctionProductRao.setWidth(auctionProductModel.getWidth());
			auctionProductRao.setFinish(auctionProductModel.getFinish());
			auctionProductRao.setPvc(auctionProductModel.getPvc());
			auctionProductRao.setIlp(auctionProductModel.getIlp());
			auctionProductRao.setQuantity(auctionProductModel.getQuantity());
			auctionProductRao.setAgeingOfManufacturing(auctionProductModel.getAgeingOfManufacturing());
			auctionProductRao.setWeight(auctionProductModel.getWeight());
			auctionProductRao.setBundled(auctionProductModel.getBundled());
			auctionProductRao.setLocation(auctionProductModel.getLocation());
		}

	}

	/**
	 * @return the productRaoPopulator
	 */
	public ProductRaoPopulator getProductRaoPopulator()
	{
		return productRaoPopulator;
	}

	/**
	 * @param productRaoPopulator
	 *           the productRaoPopulator to set
	 */
	@Required
	public void setProductRaoPopulator(final ProductRaoPopulator productRaoPopulator)
	{
		this.productRaoPopulator = productRaoPopulator;
	}





}
