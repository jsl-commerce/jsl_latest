/**
 *
 */
package com.jsl.facades.exception;

/**
 * @author manav.magoo
 *
 */
public class JslRuntimeException extends RuntimeException
{

	public JslRuntimeException(final String var1)
	{
		super(var1);
	}

}
