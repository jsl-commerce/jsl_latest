/**
 *
 */
package com.jsl.facades.csv.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.jsl.configurablebundleservice.service.JslBundleTemplateService;
import com.jsl.core.dto.BidEventData;
import com.jsl.core.dto.BidEventDto;
import com.jsl.core.dto.BundleTemplateDto;
import com.jsl.core.dto.DefaultBundleCommonDto;
import com.jsl.core.model.AuctionEventModel;
import com.jsl.core.services.JslBidEventService;
import com.jsl.core.util.JslB2bUnitUtil;
import com.jsl.facades.auction.JslAuctionFacade;
import com.jsl.facades.csv.JslBidEventDataExportExcelFacade;
import com.jsl.jslConfigurableBundleService.model.BidEventModel;


public class JslBidEventDataExportExcelFacadeImpl implements JslBidEventDataExportExcelFacade
{

	private static final Logger LOG = Logger.getLogger(JslBidEventDataExportExcelFacadeImpl.class);
	private static final String NO_BID = "NO BID PLACED";
	private static final String DEFAULT_B2BUNIT_CODE = "default.b2b.unit.codes";

	@Autowired
	private JslBidEventService jslBidEventService;

	@Autowired
	private JslBundleTemplateService bundleTemplateService;

	private Converter<BidEventModel, BidEventData> bidEventConverter;

	@Autowired
	private UserService userService;

	@Resource(name = "jslAuctionFacade")
	private JslAuctionFacade auctionFacade;

	@Override
	public List<BidEventDto> getCompletedReport(final Map<String, Object> requestObject)
	{
		List<AuctionEventModel> auctionEventDataList = null;
		final boolean dateRange = (boolean) requestObject.get("dateRange");
		final DefaultBundleCommonDto auctionDateRange = (DefaultBundleCommonDto) requestObject.get("auctionDateRange");
		if (dateRange)
		{
			auctionEventDataList = auctionFacade.getAuctionByDateRange(auctionDateRange);
		}
		else
		{
			auctionEventDataList = auctionFacade.getAuctionByDate(auctionDateRange);
		}

		return getCompleteReportForAuction(auctionEventDataList);
	}

	@Override
	public List<BidEventDto> getCompleteReportForAuction(final List<AuctionEventModel> auctionEventList)
	{
		final List<BidEventDto> bidEventDtoList = new ArrayList<>();

		for (final AuctionEventModel auctionEvent : auctionEventList)
		{
			if (auctionEvent.getAuctionStatus().getCode().equalsIgnoreCase("COMPLETED"))
			{
				final List<BundleTemplateModel> bundleTemplateList = getBundleTemplateList(auctionEvent);

				getCompleteLotDetails(bidEventDtoList, auctionEvent, bundleTemplateList);
			}
		}

		return bidEventDtoList;
	}

	@Override
	public List<BidEventDto> getDetailedReport(final Map<String, Object> requestObject)
	{
		List<AuctionEventModel> auctionEventDataList = null;
		final boolean dateRange = (boolean) requestObject.get("dateRange");
		final DefaultBundleCommonDto auctionDateRange = (DefaultBundleCommonDto) requestObject.get("auctionDateRange");
		if (dateRange)
		{
			auctionEventDataList = auctionFacade.getAuctionByDateRange(auctionDateRange);
		}
		else
		{
			auctionEventDataList = auctionFacade.getAuctionByDate(auctionDateRange);
		}

		return getDetailedReportForAuction(auctionEventDataList);
	}

	@Override
	public List<BidEventDto> getDetailedReportForAuction(final List<AuctionEventModel> auctionEventList)
	{
		final List<BidEventDto> bidEventDtoList = new ArrayList<>();

		for (final AuctionEventModel auctionEvent : auctionEventList)
		{
			if (auctionEvent.getAuctionStatus().getCode().equalsIgnoreCase("COMPLETED"))
			{
				final List<BundleTemplateModel> bundleTemplateList = getBundleTemplateList(auctionEvent);

				getDetailedBidDetails(bidEventDtoList, auctionEvent, bundleTemplateList);
			}
		}

		return bidEventDtoList;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.jsl.facades.csv.JslBidEventDataExportExcelFacade#addSalesOrderNumber(java.util.List)
	 */
	@Override
	public List<? extends BundleTemplateDto> addSalesOrderNumber(final List<? extends BundleTemplateDto> bidEventList)
	{
		if (!bidEventList.isEmpty())
		{
			final List<Integer> auctionIdList = new ArrayList<Integer>();
			bidEventList.forEach(b -> {
				if (!auctionIdList.contains(b.getAuctionId()))
				{
					auctionIdList.add(b.getAuctionId());
				}
			});
			if (!auctionIdList.isEmpty())
			{
				final List<OrderModel> orderList = jslBidEventService.getOrdersForAuction(auctionIdList);

				populateSalesOrderNumber(orderList, bidEventList);
			}
		}
		return bidEventList;
	}

	private void getCompleteLotDetails(final List<BidEventDto> bidEventDtoList, final AuctionEventModel auctionEvent,
			final List<BundleTemplateModel> bundleTemplateList)
	{
		for (final BundleTemplateModel bundleTemplate : bundleTemplateList)
		{
			final List<BidEventModel> bidEventList = getBidDetailsForLot(auctionEvent, bundleTemplate);

			if (!CollectionUtils.isEmpty(bidEventList))
			{
				populateBidEventDtoPerProduct(bidEventDtoList, auctionEvent, bundleTemplate, bundleTemplate.getBidEvent(),
						bidEventList);
			}
			else
			{
				populateBidEventDtoPerProduct(bidEventDtoList, auctionEvent, bundleTemplate, null, bidEventList);
			}
		}
	}


	private void getDetailedBidDetails(final List<BidEventDto> bidEventDtoList, final AuctionEventModel auctionEvent,
			final List<BundleTemplateModel> bundleTemplateList)
	{
		for (final BundleTemplateModel bundleTemplate : bundleTemplateList)
		{
			final List<BidEventModel> bidEventList = getBidDetailsForLot(auctionEvent, bundleTemplate);

			for (final BidEventModel bidEventModel : bidEventList)
			{
				populateBidEventDto(bidEventDtoList, auctionEvent, bundleTemplate, bidEventModel, null, bidEventList);
			}
		}
	}

	private List<BidEventModel> getBidDetailsForLot(final AuctionEventModel auctionEvent, final BundleTemplateModel bundleTemplate)
	{
		final DefaultBundleCommonDto lotDetails = new DefaultBundleCommonDto();
		lotDetails.setAuctionEventId(String.valueOf(auctionEvent.getAuctionId()));
		lotDetails.setBundleTemplateId(bundleTemplate.getId());

		final List<BidEventModel> bidEventList = getJslBidEventService().getAllBidHistoryForBundle(lotDetails);

		return bidEventList == null ? null : sortBidList(bidEventList);
	}

	private void populateBidEventDtoPerProduct(final List<BidEventDto> bidEventDtoList, final AuctionEventModel auctionEvent,
			final BundleTemplateModel bundleTemplate, final BidEventModel h1BidData, final List<BidEventModel> bidEventList)
	{
		for (final ProductModel product : bundleTemplate.getProducts())
		{
			populateBidEventDto(bidEventDtoList, auctionEvent, bundleTemplate, h1BidData, product, bidEventList);
		}
	}


	/**
	 * @param bidEventDtoList
	 * @param auctionEvent
	 * @param bundleTemplate
	 * @param h1BidData
	 * @param product
	 */
	private void populateBidEventDto(final List<BidEventDto> bidEventDtoList, final AuctionEventModel auctionEvent,
			final BundleTemplateModel bundleTemplate, final BidEventModel h1BidData, final ProductModel product,
			final List<BidEventModel> bidEventList)
	{
		final BidEventDto bidEventDto = new BidEventDto();
		bidEventDto.setAuctionId(auctionEvent.getAuctionId());
		bidEventDto.setAuctionDate(auctionEvent.getStartDate());
		bidEventDto.setLotId(bundleTemplate.getName());
		bidEventDto.setLotName(bundleTemplate.getId());
		bidEventDto.setSbp(String.valueOf(bundleTemplate.getBaseBidPrice()));
		if (bundleTemplate.getParentTemplate() != null)
		{
			bidEventDto.setBundleParentId(bundleTemplate.getParentTemplate().getId());
		}
		if (product != null)
		{
			bidEventDto.setLocation(product.getLocation());
			bidEventDto.setMaterialNum(product.getMaterialNum());
			bidEventDto.setBatchId(product.getCode());
			bidEventDto.setGradeGroup(product.getGradeGroup());
			bidEventDto.setGrade(product.getGrade());
			bidEventDto.setThickness(product.getThickness());
			bidEventDto.setWidth(String.valueOf(product.getWidth()));
			bidEventDto.setLength(product.getLength());
			bidEventDto.setFinish(product.getFinish());
			bidEventDto.setEdge(product.getEdgeCondition());
			bidEventDto.setPvc(product.getPvc());
			bidEventDto.setQuality(product.getQuality());
			bidEventDto.setQuantity(product.getQuantity());
			bidEventDto.setAge(String.valueOf(product.getAgeingOfManufacturing()));
			bidEventDto.setPlant(product.getPlant());
			bidEventDto.setStoLoc(product.getStoreLocator());
			bidEventDto.setDescStoLoc(product.getStoreLocatorDesc());
			bidEventDto.setWeight(product.getWeight());
			bidEventDto.setQuantity(product.getQuantity());
			bidEventDto.setLocation(product.getLocation());
			bidEventDto.setBatchId(product.getCode());
		}
		else
		{
			bidEventDto.setQuantity(bundleTemplate.getQuantity());
			bidEventDto.setStatusBidType(h1BidData.getIsProxyBid() ? "Proxy" : "Normal");
			bidEventDto.setBidBasis("RsPMT");
			bidEventDto.setBidPricePerMT(String.valueOf(h1BidData.getBidPrice()));
			bidEventDto.setBidPlacedDateTime(h1BidData.getCreatedOn());
		}
		setH1Details(h1BidData, bidEventDto, String.valueOf(bidEventList.size()));
		setUser(h1BidData, bidEventDto);
		bidEventDtoList.add(bidEventDto);
	}

	private void populateSalesOrderNumber(final List<OrderModel> orderList, final List<? extends BundleTemplateDto> bideventList)
	{
		final Map<String, OrderModel> orderBundleMap = new HashMap<String, OrderModel>();
		orderList.forEach(o -> {
			orderBundleMap.put(o.getEntryGroups().get(0).getExternalReferenceId(), o);
		});

		bideventList.forEach(bid -> {
			if (orderBundleMap.get(bid.getBundleParentId()) != null)
			{
				bid.setSalesOrderNumber(orderBundleMap.get(bid.getBundleParentId()).getCode());
			}
		});
	}

	private void setH1Details(final BidEventModel h1BidData, final BidEventDto bidEventDto, final String bidsCount)
	{
		bidEventDto.setH1BidderId(h1BidData == null ? NO_BID : h1BidData.getCreatedBy().getUid());
		bidEventDto.setNoOfBids(h1BidData == null ? "0" : bidsCount);
		bidEventDto.setH1BidPricePerMT(h1BidData == null ? NO_BID : String.valueOf(h1BidData.getBidPrice()));
	}

	private void setUser(final BidEventModel h1BidData, final BidEventDto bidEventDto)
	{
		try
		{

			if (h1BidData != null)
			{
				final B2BCustomerModel user = (B2BCustomerModel) h1BidData.getCreatedBy();

				final B2BUnitModel parentb2bUnit = JslB2bUnitUtil.getParentB2bUnit(user);
				if (parentb2bUnit != null)
				{
					bidEventDto.setH1Bidder(parentb2bUnit.getDisplayName());
				}
				else
				{
					bidEventDto.setH1Bidder(user.getDisplayName());
				}
			}
			else
			{
				bidEventDto.setH1Bidder(NO_BID);
			}
		}
		catch (final Exception e)
		{
			LOG.error("Exception while setting H1 bidder JslBidEventDataExportExcelFacadeImpl.setUser()");
		}
	}

	private List<BundleTemplateModel> getBundleTemplateList(final AuctionEventModel auctionEvent)
	{
		return getBundleTemplateService().getBundleTemplatesForAuctionId(String.valueOf(auctionEvent.getAuctionId()));
	}

	private List<BidEventModel> sortBidList(final List<BidEventModel> bidEventList)
	{
		bidEventList.sort((final BidEventModel p1, final BidEventModel p2) -> p1.getBidPrice().compareTo(p2.getBidPrice()));
		return bidEventList;
	}


	public JslBidEventService getJslBidEventService()
	{
		return jslBidEventService;
	}

	public void setJslBidEventService(final JslBidEventService jslBidEventService)
	{
		this.jslBidEventService = jslBidEventService;
	}

	public JslBundleTemplateService getBundleTemplateService()
	{
		return bundleTemplateService;
	}

	public void setBundleTemplateService(final JslBundleTemplateService bundleTemplateService)
	{
		this.bundleTemplateService = bundleTemplateService;
	}

	public Converter<BidEventModel, BidEventData> getBidEventConverter()
	{
		return bidEventConverter;
	}

	public void setBidEventConverter(final Converter<BidEventModel, BidEventData> bidEventConverter)
	{
		this.bidEventConverter = bidEventConverter;
	}

	public UserService getUserService()
	{
		return userService;
	}

	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

}
