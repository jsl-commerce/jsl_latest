package com.jsl.facades.csv.impl;

import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.jsl.configurablebundleservice.service.JslBundleTemplateService;
import com.jsl.configurablebundleservice.service.impl.JslBundleTemplateExportExcelServiceImpl;
import com.jsl.core.dto.BundleTemplateData;
import com.jsl.core.dto.BundleTemplateDto;
import com.jsl.core.dto.DefaultBundleCommonDto;
import com.jsl.core.model.AuctionProductModel;
import com.jsl.facades.auction.JslAuctionFacade;
import com.jsl.facades.csv.JslBundleTemplateExportExcelFacade;
import com.jsl.jslConfigurableBundleService.model.BidEventModel;


public class JslBundleTemplateExportExcelFacadeImpl implements JslBundleTemplateExportExcelFacade
{
	private static final Logger LOG = Logger.getLogger(JslBundleTemplateExportExcelFacadeImpl.class);

	@Resource(name = "jslBundleTemplateExportExcelServiceImpl")
	private JslBundleTemplateExportExcelServiceImpl jslBundleTemplateExportExcelServiceImpl;

	@Autowired
	private JslBundleTemplateService jslBundleTemplateService;

	@Resource(name = "jslAuctionFacade")
	private JslAuctionFacade auctionFacade;

	private Converter<BundleTemplateModel, BundleTemplateData> bundleTemplateConverter;

	@Override
	public List<BundleTemplateDto> getBundleTemplatesForAuctionId(final String auctionId)
	{
		final List<BundleTemplateModel> bundleTemplateModelList = jslBundleTemplateService
				.getBundleTemplatesForAuctionId(auctionId);

		return convertBundleTemplateModelToDto(bundleTemplateModelList);
	}

	@Override
	public Map<String, Object> getCompletedBundleData(final Map<String, Object> requestObject,
			final Map<String, Object> responseObject)
	{
		List<BundleTemplateDto> bundleTemplateList = null;
		List<BundleTemplateDto> lostbundleTemplateList = null;
		List<BundleTemplateModel> bundleTemplateModelList = null;
		List<BidEventModel> lostBidList = null;
		final boolean allCompletedAuction = (boolean) requestObject.get("allCompletedAuction");
		final String flowName = "downloadCSV";
		if (allCompletedAuction)
		{
			bundleTemplateModelList = jslBundleTemplateService.getCompletedAuctionDetails(flowName);
			lostBidList = auctionFacade.getLostAuctionLotsDetailsForCurrentuser(bundleTemplateModelList, flowName);
		}
		else
		{
			final DefaultBundleCommonDto auctionDateRange = (DefaultBundleCommonDto) requestObject.get("auctionDateRange");
			bundleTemplateModelList = getCompletedBundleDataBasedOnDateRange(auctionDateRange);
			lostBidList = auctionFacade.getLostAuctionLotsDetailsForCurrentUserByDateRange(bundleTemplateModelList,
					auctionDateRange);
		}

		bundleTemplateList = convertBundleTemplateModelToDto(bundleTemplateModelList);
		lostbundleTemplateList = convertBidEventModelToDto(lostBidList);
		responseObject.put("bundleTemplateList", bundleTemplateList);
		responseObject.put("lostbundleTemplateList", lostbundleTemplateList);

		return responseObject;
	}

	@Override
	public List<BundleTemplateModel> getCompletedBundleDataBasedOnDateRange(final DefaultBundleCommonDto auctionDateRange)
	{
		List<BundleTemplateModel> bundleTemplateModelList = null;
		try
		{
			bundleTemplateModelList = jslBundleTemplateService.getCompletedAuctionDetailsByDateRange(
					auctionDateRange.getAuctionStartDate(), auctionDateRange.getAuctionEndDate());
		}
		catch (final Exception e)
		{
			LOG.error("Exception in JslBundleTemplateExportExcelFacadeImpl.getCompletedBundleDataBasedOnDateRange()");
		}

		return bundleTemplateModelList;
	}

	private List<BundleTemplateDto> convertBundleTemplateModelToDto(final List<BundleTemplateModel> bundleTemplateModelList)
	{
		double totalQuantity = 0.00;
		final List<BundleTemplateDto> bundleTemplateData = new ArrayList<>();
		for (final BundleTemplateModel bundleTemplateModel : bundleTemplateModelList)
		{
			if (bundleTemplateModel.getParentTemplate() != null)
			{
				for (final ProductModel product : bundleTemplateModel.getProducts())
				{
					final BundleTemplateDto bundleTemplateDto = new BundleTemplateDto();
					bundleTemplateDto.setLotId(bundleTemplateModel.getName());
					bundleTemplateDto.setLotName(bundleTemplateModel.getId());
					bundleTemplateDto.setBaseBidPrice(bundleTemplateModel.getBaseBidPrice());
					bundleTemplateDto.setBidWonPrice(bundleTemplateModel.getBidWonPrice());
					bundleTemplateDto.setDescription(bundleTemplateModel.getDescription());
					bundleTemplateDto.setAuctionId(bundleTemplateModel.getAuctionEvent().getAuctionId());
					bundleTemplateDto.setYourBidPrice(String.valueOf(bundleTemplateModel.getBidWonPrice()));
					bundleTemplateDto.setBidStatus("BID WON");
					bundleTemplateDto
							.setApprovalsStatus(jslBundleTemplateService.getOrderApprovalStatusForBundle(bundleTemplateModel));
					if (product instanceof AuctionProductModel)
					{
						bundleTemplateDto.setPlant(product.getPlant());
						bundleTemplateDto.setQuality(product.getQuality());
						bundleTemplateDto.setGradeGroup(product.getGradeGroup());
						bundleTemplateDto.setGrade(product.getGrade());
						bundleTemplateDto.setWeight(product.getWeight());
						bundleTemplateDto.setLength(product.getLength());
						bundleTemplateDto.setQuantity(product.getQuantity());
						bundleTemplateDto.setThickness(product.getThickness());
						bundleTemplateDto.setWidth(String.valueOf(product.getWidth()));
						bundleTemplateDto.setLocation(product.getLocation());
						bundleTemplateDto.setEdge(product.getEdgeCondition());
						bundleTemplateDto.setQuantity(product.getQuantity());
						bundleTemplateDto.setFinish(product.getFinish());
						bundleTemplateDto.setBatchId(product.getCode());
						bundleTemplateDto.setPvc(product.getPvc());
					}
					bundleTemplateData.add(bundleTemplateDto);
				}
				final BundleTemplateDto bundleTemplateDto = new BundleTemplateDto();
				bundleTemplateDto.setQuantity(bundleTemplateModel.getQuantity());
				bundleTemplateDto.setLotId("Total Lot Quantity: ");
				bundleTemplateData.add(bundleTemplateDto);
			}
			totalQuantity += bundleTemplateModel.getQuantity();
		}
		final BundleTemplateDto bundleTemplateDto = new BundleTemplateDto();
		bundleTemplateDto.setQuantity(totalQuantity);
		bundleTemplateDto.setQuality("Total Quantity: ");
		bundleTemplateData.add(bundleTemplateDto);

		return bundleTemplateData;
	}

	private List<BundleTemplateDto> convertBidEventModelToDto(final List<BidEventModel> bidEventModelList)
	{
		double totalQuantity = 0.00;
		final List<BundleTemplateDto> bundleTemplateData = new ArrayList<>();
		for (final BidEventModel bidEvent : bidEventModelList)
		{
			final BundleTemplateModel bundleTemplateModel = bidEvent.getBundleTemplate();
			if (bundleTemplateModel.getParentTemplate() != null)
			{
				for (final ProductModel product : bundleTemplateModel.getProducts())
				{
					final BundleTemplateDto bundleTemplateDto = new BundleTemplateDto();
					bundleTemplateDto.setLotId(bundleTemplateModel.getName());
					bundleTemplateDto.setLotName(bundleTemplateModel.getId());
					bundleTemplateDto.setBaseBidPrice(bundleTemplateModel.getBaseBidPrice());
					bundleTemplateDto.setBidWonPrice(bundleTemplateModel.getBidWonPrice());
					bundleTemplateDto.setDescription(bundleTemplateModel.getDescription());
					bundleTemplateDto.setAuctionId(bundleTemplateModel.getAuctionEvent().getAuctionId());
					bundleTemplateDto.setYourBidPrice(String.valueOf(bidEvent.getBidPrice()));
					bundleTemplateDto.setBidStatus("BID LOST");
					bundleTemplateDto.setApprovalsStatus("N/A");
					if (product instanceof AuctionProductModel)
					{
						bundleTemplateDto.setPlant(product.getPlant());
						bundleTemplateDto.setQuality(product.getQuality());
						bundleTemplateDto.setGradeGroup(product.getGradeGroup());
						bundleTemplateDto.setGrade(product.getGrade());
						bundleTemplateDto.setWeight(product.getWeight());
						bundleTemplateDto.setLength(product.getLength());
						bundleTemplateDto.setQuantity(product.getQuantity());
						bundleTemplateDto.setThickness(product.getThickness());
						bundleTemplateDto.setWidth(String.valueOf(product.getWidth()));
						bundleTemplateDto.setLocation(product.getLocation());
						bundleTemplateDto.setEdge(product.getEdgeCondition());
						bundleTemplateDto.setQuantity(product.getQuantity());
						bundleTemplateDto.setFinish(product.getFinish());
						bundleTemplateDto.setBatchId(product.getCode());
						bundleTemplateDto.setPvc(product.getPvc());
					}
					bundleTemplateData.add(bundleTemplateDto);
				}
				final BundleTemplateDto bundleTemplateDto = new BundleTemplateDto();
				bundleTemplateDto.setQuantity(bundleTemplateModel.getQuantity());
				bundleTemplateDto.setLotId("Total Lot Quantity: ");
				bundleTemplateData.add(bundleTemplateDto);
			}
			totalQuantity += bundleTemplateModel.getQuantity();
		}
		final BundleTemplateDto bundleTemplateDto = new BundleTemplateDto();
		bundleTemplateDto.setQuantity(totalQuantity);
		bundleTemplateDto.setQuality("Total Quantity: ");
		bundleTemplateData.add(bundleTemplateDto);

		return bundleTemplateData;
	}

	public JslBundleTemplateExportExcelServiceImpl getJslBundleTemplateExportExcelServiceImpl()
	{
		return jslBundleTemplateExportExcelServiceImpl;
	}

	public void setJslBundleTemplateExportExcelServiceImpl(
			final JslBundleTemplateExportExcelServiceImpl jslBundleTemplateExportExcelServiceImpl)
	{
		this.jslBundleTemplateExportExcelServiceImpl = jslBundleTemplateExportExcelServiceImpl;
	}

	public Converter<BundleTemplateModel, BundleTemplateData> getBundleTemplateConverter()
	{
		return bundleTemplateConverter;
	}

	public void setBundleTemplateConverter(final Converter<BundleTemplateModel, BundleTemplateData> bundleTemplateConverter)
	{
		this.bundleTemplateConverter = bundleTemplateConverter;
	}

}
