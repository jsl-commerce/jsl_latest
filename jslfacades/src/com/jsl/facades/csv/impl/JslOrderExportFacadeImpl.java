/**
 *
 */
package com.jsl.facades.csv.impl;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;

import java.io.File;
import java.text.MessageFormat;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import com.google.common.base.Strings;
import com.jsl.configurablebundleservice.order.service.JslOrderExportService;
import com.jsl.core.exceptions.JslExportOrderException;
import com.jsl.core.services.JslOrderService;
import com.jsl.facades.csv.JslOrderExportFacade;

import reactor.util.CollectionUtils;


/**
 * @author manav.magoo
 *
 */
public class JslOrderExportFacadeImpl implements JslOrderExportFacade
{

	private static final Logger LOG = Logger.getLogger(JslOrderExportFacadeImpl.class);

	JslOrderService jslOrderService;
	JslOrderExportService jslOrderExportService;

	@Override
	public boolean exportAuctionOrders(final int auctionId, final File file) throws JslExportOrderException
	{
		final List<OrderModel> orders = getJslOrderService().getAuctionOrders(auctionId);

		if (CollectionUtils.isEmpty(orders))
		{
			throw new JslExportOrderException(MessageFormat.format("No Orders present for auction id {0} !! ", auctionId));
		}

		final List<OrderModel> approvedOrderModel = orders.stream()
				.filter(o -> (o.getStatus().equals(OrderStatus.APPROVED) && Strings.isNullOrEmpty(o.getVersionID())))
				.collect(Collectors.toList());

		if (approvedOrderModel != null && !approvedOrderModel.isEmpty())
		{
			return getJslOrderExportService().exportAuctionOrders(approvedOrderModel, file);
		}
		else
		{
			throw new JslExportOrderException("No approved orders found");
		}
	}

	/**
	 * @return the jslOrderService
	 */
	public JslOrderService getJslOrderService()
	{
		return jslOrderService;
	}

	/**
	 * @param jslOrderService
	 *           the jslOrderService to set
	 */
	public void setJslOrderService(final JslOrderService jslOrderService)
	{
		this.jslOrderService = jslOrderService;
	}

	/**
	 * @return the jslOrderExportService
	 */
	public JslOrderExportService getJslOrderExportService()
	{
		return jslOrderExportService;
	}

	/**
	 * @param jslOrderExportService
	 *           the jslOrderExportService to set
	 */
	public void setJslOrderExportService(final JslOrderExportService jslOrderExportService)
	{
		this.jslOrderExportService = jslOrderExportService;
	}

	@Override
	public boolean exportZDocOrders(final String startDate, final String endDate, final File file)
	{
		try
		{
			final List<OrderModel> orders = getJslOrderService().getOrders(startDate, endDate);
			if (CollectionUtils.isEmpty(orders))
			{
				throw new JslExportOrderException(
						MessageFormat.format("No Orders present for between date range:{0} and  {1}", startDate, endDate));
			}
			return getJslOrderExportService().exportZdocOrders(orders, file);
		}
		catch (final Exception e)
		{
			LOG.error("Exception in JslOrderExportFacadeImpl.exportZDocOrders(): {}", e);
			throw new JslExportOrderException("No approved orders found");
		}
	}
}
