package com.jsl.facades.csv;

import java.util.List;
import java.util.Map;

import com.jsl.core.dto.BidEventDto;
import com.jsl.core.dto.BundleTemplateDto;
import com.jsl.core.model.AuctionEventModel;


public interface JslBidEventDataExportExcelFacade
{
	List<BidEventDto> getCompleteReportForAuction(List<AuctionEventModel> auctionEventList);

	List<BidEventDto> getDetailedReportForAuction(List<AuctionEventModel> auctionEventList);

	List<BidEventDto> getCompletedReport(Map<String, Object> requestObject);

	List<BidEventDto> getDetailedReport(Map<String, Object> requestObject);

	List<? extends BundleTemplateDto> addSalesOrderNumber(List<? extends BundleTemplateDto> bidEventList);
}
