/**
 *
 */
package com.jsl.facades.csv;

import java.io.File;

import com.jsl.core.exceptions.JslExportOrderException;


/**
 * @author manav.magoo
 *
 */
public interface JslOrderExportFacade
{
	/**
	 * @param auctionId
	 * @param file
	 * @return
	 */
	boolean exportAuctionOrders(int auctionId, File file) throws JslExportOrderException;

	boolean exportZDocOrders(String startDate, String endDate, File file);
}
