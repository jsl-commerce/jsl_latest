/**
 *
 */
package com.jsl.facades.csv;

import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;

import java.util.List;
import java.util.Map;

import com.jsl.core.dto.BundleTemplateDto;
import com.jsl.core.dto.DefaultBundleCommonDto;


/**
 * @author sunil.maurya.kumar
 *
 */
public interface JslBundleTemplateExportExcelFacade
{
	List<BundleTemplateDto> getBundleTemplatesForAuctionId(String auctionId);

	Map<String, Object> getCompletedBundleData(Map<String, Object> requestObject, Map<String, Object> responseObject);

	List<BundleTemplateModel> getCompletedBundleDataBasedOnDateRange(DefaultBundleCommonDto auctionDateRange);

}
