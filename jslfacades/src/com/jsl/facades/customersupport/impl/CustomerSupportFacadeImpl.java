/**
 *
 */
package com.jsl.facades.customersupport.impl;

import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.jsl.core.customersupport.service.CustomerSupportService;
import com.jsl.core.model.CustomerComplaintModel;
import com.jsl.core.model.InvoiceDetailsModel;
import com.jsl.core.model.JslCustomerFeedbackModel;
import com.jsl.facades.customersupport.CustomerSupportFacade;
import com.jsl.facades.exceptions.JslCustomerComplaintException;
import com.jsl.facades.supportticket.dto.CustomerComplaintDto;
import com.jsl.facades.supportticket.dto.JslCustomerFeedbackDto;


/**
 * @author Kiranraj S.
 *
 */
//@Component
public class CustomerSupportFacadeImpl implements CustomerSupportFacade
{

	private static final Logger LOG = Logger.getLogger(CustomerSupportFacadeImpl.class);

	@Autowired
	private ModelService modelService;

	@Autowired
	CustomerSupportService csService;

	@Resource(name = "jslSupportTicketHistoryConverter")
	private Converter<CustomerComplaintModel, CustomerComplaintDto> jslSupportTicketHistoryConverter;

	@Autowired
	private Converter<CustomerComplaintDto, CustomerComplaintModel> customerComplaintModelConvertor;

	@Autowired
	private Converter<CustomerComplaintModel, CustomerComplaintDto> customerComplaintDtoConvertor;

	@Resource(name = "siteConfigService")
	private SiteConfigService siteConfigService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "customerFeedbackModelConvertor")
	private Converter<JslCustomerFeedbackDto, JslCustomerFeedbackModel> jslCustomerFeedbackModelConvertor;

	@Resource(name = "customerFeedbackDtoConvertor")
	private Converter<JslCustomerFeedbackModel, JslCustomerFeedbackDto> jslCustomerFeedbackDtoConvertor;

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.jsl.facades.customersupport.CustomerSupportFacade#submitCustomerSatisfaction(com.jsl.facades.supportticket.dto
	 * .CustomerComplaintDto)
	 */
	@Override
	public boolean submitCustomerSatisfaction(final CustomerComplaintDto complaintDto) throws Exception
	{
		// XXX Auto-generated method stub
		return csService.submitCustomerSatisfaction(complaintDto);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.jsl.facades.customersupport.CustomerSupportFacade#isBatchAlreadyAdded(java.lang.String)
	 */
	@Override
	public CustomerComplaintDto isBatchAlreadyAdded(final String invoiceNumber, final String batchNumber) throws Exception
	{
		// XXX Auto-generated method stub
		final CustomerComplaintModel customerComplaintModel = csService.isBatchAlreadyAdded(invoiceNumber, batchNumber);

		if (customerComplaintModel == null)
		{
			return null;
		}
		return jslSupportTicketHistoryConverter.convert(customerComplaintModel);
	}

	@Override
	public Map<String, String> submitComplaint(final CustomerComplaintDto customerComplaint) throws Exception
	{
		Map<String, String> response = null;
		try
		{
			final CustomerComplaintModel customerComplaintModel = getCustomerComplaintModelConvertor().convert(customerComplaint,
					getModelService().create(CustomerComplaintModel.class));
			getCsService().submitInvoiceDetails((List<InvoiceDetailsModel>) customerComplaintModel.getComplaintInvoice());
			response = getCsService().submitComplaint(customerComplaintModel, customerComplaint.getAttachment());
		}
		catch (final Exception e)
		{
			LOG.error("Exception in CustomerSupportFacadeImpl.submitComplaint(): ", e);
			throw new JslCustomerComplaintException(e.getMessage());
		}
		return response;
	}

	@Override
	public Map<String, List<CustomerComplaintDto>> getSupportTicketHistory()
	{
		final List<CustomerComplaintModel> customerComplaintModellist = getCsService().getSupportTicketHistory();
		final List<CustomerComplaintDto> customerComplaintDto = new ArrayList<CustomerComplaintDto>();
		customerComplaintModellist.forEach(customerComplaintModel -> {
			customerComplaintDto.add(getCustomerComplaintDtoConvertor().convert(customerComplaintModel));
		});

		return filterComplaint(customerComplaintDto);
	}

	@Override
	public List<CustomerComplaintDto> getAllComplaintListForAdmin()
	{
		final List<CustomerComplaintDto> customerComplaintList = new ArrayList<>();
		final List<CustomerComplaintModel> complaintList = getCsService().getAllComplaintListForAdmin();
		complaintList.forEach(complaint -> {
			customerComplaintList.add(getCustomerComplaintDtoConvertor().convert(complaint));
		});

		return customerComplaintList;
	}

	@Override
	public boolean isSupportTicketAdminUser()
	{
		boolean isSupportTicketAdminUser = false;
		final String ADMIN_OBSERVER_ID = "admin.support.ticket.observer.user.id";
		String currentUserId = "";
		try
		{
			currentUserId = ((B2BCustomerModel) getUserService().getCurrentUser()).getOriginalUid();
			final String adminUserId = getSiteConfigService().getProperty(ADMIN_OBSERVER_ID);
			if (StringUtils.hasText(adminUserId) && adminUserId.equalsIgnoreCase(currentUserId))
			{
				isSupportTicketAdminUser = true;
			}
		}
		catch (final Exception e)
		{
			LOG.error("Exception while identifing admin support ticket observer user for: " + currentUserId, e);
		}

		return isSupportTicketAdminUser;

	}

	@Override
	public boolean submitCustomerFeedBack(final CustomerComplaintDto complaintDto)
	{
		return getCsService().submitCustomerFeedBack(complaintDto);
	}

	@Override
	public boolean submitCustomerFeedBackForm(final JslCustomerFeedbackDto feedBackDto) throws Exception
	{
		return getCsService().submitCustomerFeedBackForm(jslCustomerFeedbackModelConvertor.convert(feedBackDto),
				feedBackDto.getComplaintNumber());
	}

	@Override
	public List<JslCustomerFeedbackDto> getAllCustomerFeedBackFormData() throws Exception
	{
		return jslCustomerFeedbackDtoConvertor.convertAll(getCsService().getAllCustomerFeedbackFormData());
	}

	private Map<String, List<CustomerComplaintDto>> filterComplaint(final List<CustomerComplaintDto> customerComplaintList)
	{
		final Map<String, List<CustomerComplaintDto>> response = new HashMap<>();
		final List<CustomerComplaintDto> customerMaterialComplaintList = new ArrayList<>();
		final List<CustomerComplaintDto> customerServiceComplaintList = new ArrayList<>();
		for (final CustomerComplaintDto customerComplaint : customerComplaintList)
		{
			if (customerComplaint.getTypeOfComplaint() != null)
			{
				if (customerComplaint.getTypeOfComplaint().equalsIgnoreCase("service"))
				{
					customerServiceComplaintList.add(customerComplaint);
				}
				else if (customerComplaint.getTypeOfComplaint().equalsIgnoreCase("material"))
				{
					customerMaterialComplaintList.add(customerComplaint);
				}
			}
		}
		customerServiceComplaintList
				.sort((final CustomerComplaintDto c1, final CustomerComplaintDto c2) -> Integer.parseInt(c2.getComplaintNumber())
						- Integer.parseInt(c1.getComplaintNumber()));
		customerMaterialComplaintList
				.sort((final CustomerComplaintDto c1, final CustomerComplaintDto c2) -> Integer.parseInt(c2.getComplaintNumber())
						- Integer.parseInt(c1.getComplaintNumber()));
		response.put("ServiceComplaintList", customerServiceComplaintList);
		response.put("MaterialComplaintList", customerMaterialComplaintList);

		return response;
	}

	private List<CustomerComplaintDto> getServiceComplaint(final List<CustomerComplaintDto> customerComplaintList)
	{
		final List<CustomerComplaintDto> customerMaterialComplaintList = customerComplaintList.stream()
				.filter(customerMaterialComplaint -> customerComplaintList.stream().anyMatch(
						customerMaterialComplaint1 -> customerMaterialComplaint1.getTypeOfComplaint().equalsIgnoreCase("Service")))
				.collect(Collectors.toList());

		return customerMaterialComplaintList;

	}

	private List<CustomerComplaintDto> getMaterialComplaint(final List<CustomerComplaintDto> customerComplaintList)
	{
		final List<CustomerComplaintDto> customerMaterialComplaintList = customerComplaintList.stream()
				.filter(customerMaterialComplaint -> customerComplaintList.stream().anyMatch(
						customerMaterialComplaint1 -> customerMaterialComplaint1.getTypeOfComplaint().equalsIgnoreCase("Material")))
				.collect(Collectors.toList());

		return customerMaterialComplaintList;

	}

	/**
	 * @return the csService
	 */
	public CustomerSupportService getCsService()
	{
		return csService;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @return the customerComplaintModelConvertor
	 */
	public Converter<CustomerComplaintDto, CustomerComplaintModel> getCustomerComplaintModelConvertor()
	{
		return customerComplaintModelConvertor;
	}

	/**
	 * @return the jslSupportTicketHistoryConverter
	 */
	public Converter<CustomerComplaintModel, CustomerComplaintDto> getJslSupportTicketHistoryConverter()
	{
		return jslSupportTicketHistoryConverter;
	}

	/**
	 * @param jslSupportTicketHistoryConverter
	 *           the jslSupportTicketHistoryConverter to set
	 */
	public void setJslSupportTicketHistoryConverter(
			final Converter<CustomerComplaintModel, CustomerComplaintDto> jslSupportTicketHistoryConverter)
	{
		this.jslSupportTicketHistoryConverter = jslSupportTicketHistoryConverter;
	}

	/**
	 * @return the customerComplaintDtoConvertor
	 */
	public Converter<CustomerComplaintModel, CustomerComplaintDto> getCustomerComplaintDtoConvertor()
	{
		return customerComplaintDtoConvertor;
	}

	/**
	 * @return the siteConfigService
	 */
	public SiteConfigService getSiteConfigService()
	{
		return siteConfigService;
	}

	/**
	 * @param siteConfigService
	 *           the siteConfigService to set
	 */
	public void setSiteConfigService(final SiteConfigService siteConfigService)
	{
		this.siteConfigService = siteConfigService;
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	/**
	 * @return the jslCustomerFeedbackModelConvertor
	 */
	public Converter<JslCustomerFeedbackDto, JslCustomerFeedbackModel> getJslCustomerFeedbackModelConvertor()
	{
		return jslCustomerFeedbackModelConvertor;
	}

	/**
	 * @param jslCustomerFeedbackModelConvertor
	 *           the jslCustomerFeedbackModelConvertor to set
	 */
	public void setJslCustomerFeedbackModelConvertor(
			final Converter<JslCustomerFeedbackDto, JslCustomerFeedbackModel> jslCustomerFeedbackModelConvertor)
	{
		this.jslCustomerFeedbackModelConvertor = jslCustomerFeedbackModelConvertor;
	}

	/**
	 * @return the jslCustomerFeedbackDtoConvertor
	 */
	public Converter<JslCustomerFeedbackModel, JslCustomerFeedbackDto> getJslCustomerFeedbackDtoConvertor()
	{
		return jslCustomerFeedbackDtoConvertor;
	}

	/**
	 * @param jslCustomerFeedbackDtoConvertor
	 *           the jslCustomerFeedbackDtoConvertor to set
	 */
	public void setJslCustomerFeedbackDtoConvertor(
			final Converter<JslCustomerFeedbackModel, JslCustomerFeedbackDto> jslCustomerFeedbackDtoConvertor)
	{
		this.jslCustomerFeedbackDtoConvertor = jslCustomerFeedbackDtoConvertor;
	}

}
