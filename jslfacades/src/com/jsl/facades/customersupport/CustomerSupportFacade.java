/**
 *
 */
package com.jsl.facades.customersupport;

import java.util.List;
import java.util.Map;

import com.jsl.facades.supportticket.dto.CustomerComplaintDto;
import com.jsl.facades.supportticket.dto.JslCustomerFeedbackDto;


/**
 * @author Kiranraj S.
 *
 */
public interface CustomerSupportFacade
{

	public Map<String, String> submitComplaint(CustomerComplaintDto customerComplaint) throws Exception;

	public Map<String, List<CustomerComplaintDto>> getSupportTicketHistory();

	public List<CustomerComplaintDto> getAllComplaintListForAdmin();

	public boolean isSupportTicketAdminUser();

	public boolean submitCustomerFeedBack(CustomerComplaintDto complaintDto);

	public boolean submitCustomerFeedBackForm(final JslCustomerFeedbackDto feedBackDto) throws Exception;

	public List<JslCustomerFeedbackDto> getAllCustomerFeedBackFormData() throws Exception;

	public boolean submitCustomerSatisfaction(final CustomerComplaintDto complaintDto) throws Exception;

	public CustomerComplaintDto isBatchAlreadyAdded(final String invoiceNumber, final String batchNumber) throws Exception;

}
