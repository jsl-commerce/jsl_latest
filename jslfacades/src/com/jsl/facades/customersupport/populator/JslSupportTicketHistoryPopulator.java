/**
 *
 */
package com.jsl.facades.customersupport.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.jsl.core.model.CustomerComplaintModel;
import com.jsl.core.model.InvoiceDetailsModel;
import com.jsl.facades.supportticket.dto.CustomerComplaintDto;
import com.jsl.facades.supportticket.dto.InvoiceDetailsDto;

/**
 * @author himanshu.sial
 *
 */
public class JslSupportTicketHistoryPopulator implements Populator<CustomerComplaintModel, CustomerComplaintDto>
{

	@Autowired
	private Converter<InvoiceDetailsModel, InvoiceDetailsDto> invoiceDtoConvertor;

	@Override
	public void populate(final CustomerComplaintModel source, final CustomerComplaintDto target) throws ConversionException
	{
		target.setTypeOfComplaint(source.getTypeOfComplaint());
		target.setContactNumber(source.getContactNumber());
		target.setContactPersonName(source.getContactPersonName());
		target.setComplaintDescription(source.getComplaintDescription());
		target.setMaterialLocation(source.getMaterialLocation());
		target.setComplaintNumber(source.getComplaintNumber());
		target.setCustomerFeedBackComment(source.getCustomerFeedBackComment());
		target.setCustomerFeedBackRating(String.valueOf(source.getCustomerFeedBackRating()));
		target.setKamName("test name");
		target.setQualityPersonName("test QA person");
		target.setSettlementRemarks("test remark");
		final List<InvoiceDetailsDto> invoiceDetailsDtoList = poplutateInvoiceDetails(source.getComplaintInvoice());
		target.setInvoiceDetails(invoiceDetailsDtoList);
	}

	private List<InvoiceDetailsDto> poplutateInvoiceDetails(final Collection<InvoiceDetailsModel> invoiceList)
	{
		final List<InvoiceDetailsDto> invoiceDetailsDtoList = new ArrayList<>();
		if (invoiceList != null && CollectionUtils.isNotEmpty(invoiceList))
		{
			invoiceList.forEach(invoiceDetailsModel -> {
				invoiceDetailsDtoList.add(invoiceDtoConvertor.convert(invoiceDetailsModel));
			});
		}
		return invoiceDetailsDtoList;
	}

}
