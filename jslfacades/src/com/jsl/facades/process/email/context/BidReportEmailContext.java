/**
 *
 */
package com.jsl.facades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;

import com.jsl.core.model.BidDetailsExportEmailProcessModel;
import com.jsl.facades.constants.JslFacadesConstants;


/**
 * @author subhrajitk766
 *
 */
public class BidReportEmailContext extends AbstractEmailContext<StoreFrontCustomerProcessModel>
{
	public static final String DEFAULTNAME = "JSL";

	@Override
	public void init(final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(storeFrontCustomerProcessModel, emailPageModel);
		if (storeFrontCustomerProcessModel instanceof BidDetailsExportEmailProcessModel)
		{
			final BidDetailsExportEmailProcessModel model = (BidDetailsExportEmailProcessModel) storeFrontCustomerProcessModel;
			put(EMAIL, model.getEmailID());
			put(FROM_EMAIL, getConfigurationService().getConfiguration().getString(JslFacadesConstants.JSLEMAILFROM));
			put(DISPLAY_NAME, "JSL Customer Service");
			put(FROM_DISPLAY_NAME,
					getConfigurationService().getConfiguration().getString(JslFacadesConstants.JSLEMAILFROMDISPLAYNAME, DEFAULTNAME));
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext#getSite(de.hybris.platform.
	 * processengine.model.BusinessProcessModel)
	 */
	@Override
	protected BaseSiteModel getSite(final StoreFrontCustomerProcessModel businessProcessModel)
	{
		if (null != businessProcessModel.getSite())
		{
			return businessProcessModel.getSite();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext#getCustomer(de.hybris.platform.
	 * processengine.model.BusinessProcessModel)
	 */
	@Override
	protected CustomerModel getCustomer(final StoreFrontCustomerProcessModel businessProcessModel)
	{
		// XXX Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext#getEmailLanguage(de.hybris.
	 * platform.processengine.model.BusinessProcessModel)
	 */
	@Override
	protected LanguageModel getEmailLanguage(final StoreFrontCustomerProcessModel businessProcessModel)
	{
		// XXX Auto-generated method stub
		return null;
	}

}
