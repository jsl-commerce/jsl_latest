/**
 *
 */
package com.jsl.facades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;

import com.jsl.core.model.AuctionEventModel;
import com.jsl.core.model.SendCustomerOtpProcessModel;
import com.jsl.core.services.JslAuctionService;
import com.jsl.facades.constants.JslFacadesConstants;


public class SendOtpEmailContext extends AbstractEmailContext<StoreFrontCustomerProcessModel>

{
	public static final String AUCTIONID = "auctionID";
	public static final String OTP = "otp";
	public static final String DEFAULTNAME = "JSL";
	public static final String AUCTIONSTARTTIME = "auctionStartTime";
	public static final String AUCTIONENDTIME = "auctionEndTime";
	public static final String OTPEXPIRATION = "otpExpiration";
	public static final String AUCTIONGROUP = "auctionGroup";

	private JslAuctionService jslAuctionService;

	@Override
	public void init(final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(storeFrontCustomerProcessModel, emailPageModel);
		if (storeFrontCustomerProcessModel instanceof SendCustomerOtpProcessModel)
		{
			final SendCustomerOtpProcessModel model = (SendCustomerOtpProcessModel) storeFrontCustomerProcessModel;
			put(EMAIL, model.getEmail());
			put(FROM_EMAIL, getConfigurationService().getConfiguration().getString(JslFacadesConstants.JSLEMAILFROM));
			put(DISPLAY_NAME, model.getDisplayName());
			put(FROM_DISPLAY_NAME,
					getConfigurationService().getConfiguration().getString(JslFacadesConstants.JSLEMAILFROMDISPLAYNAME, DEFAULTNAME));

			final AuctionEventModel auction = getJslAuctionService().getAuctionEventForAuctionId(model.getAuctionEventId());
			put(OTP, model.getOtp());
			put(AUCTIONID, auction.getAuctionId());
			put(AUCTIONSTARTTIME, auction.getStartDate());
			put(AUCTIONENDTIME, auction.getEndDate());
			put(OTPEXPIRATION, auction.getEndDate());
			put(AUCTIONGROUP, auction.getAuctionGroup().getName());
		}

	}


	/**
	 * @return the auctionid
	 */
	public String getAuctionid()
	{
		return (String) get(AUCTIONID);
	}

	public String getAuctiongroup()
	{
		return (String) get(AUCTIONGROUP);
	}



	/**
	 * @return the otp
	 */
	public String getOtp()
	{
		return (String) get(OTP);
	}



	/**
	 * @return the defaultname
	 */
	public String getDefaultname()
	{
		return DEFAULTNAME;
	}




	@Override
	protected LanguageModel getEmailLanguage(final StoreFrontCustomerProcessModel businessProcessModel)
	{
		return businessProcessModel.getLanguage();
	}



	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext#getSite(de.hybris.platform.
	 * processengine.model.BusinessProcessModel)
	 */
	@Override
	protected BaseSiteModel getSite(final StoreFrontCustomerProcessModel businessProcessModel)
	{
		// XXX Auto-generated method stub
		return null;
	}



	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext#getCustomer(de.hybris.platform.
	 * processengine.model.BusinessProcessModel)
	 */
	@Override
	protected CustomerModel getCustomer(final StoreFrontCustomerProcessModel businessProcessModel)
	{
		// XXX Auto-generated method stub
		return null;
	}


	/**
	 * @return the jslAuctionService
	 */
	public JslAuctionService getJslAuctionService()
	{
		return jslAuctionService;
	}


	/**
	 * @param jslAuctionService
	 *           the jslAuctionService to set
	 */
	public void setJslAuctionService(final JslAuctionService jslAuctionService)
	{
		this.jslAuctionService = jslAuctionService;
	}


	/**
	 * @return the auctionstarttime
	 */
	public String getAuctionstarttime()
	{
		return (String) get(AUCTIONSTARTTIME);
	}


	/**
	 * @return the auctionendtime
	 */
	public String getAuctionendtime()
	{
		return (String) get(AUCTIONENDTIME);
	}


	/**
	 * @return the otpexpiration
	 */
	public String getOtpexpiration()
	{
		return (String) get(OTPEXPIRATION);
	}



}
