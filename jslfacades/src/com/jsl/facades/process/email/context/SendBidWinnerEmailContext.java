/**
 *
 */
package com.jsl.facades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.configurablebundleservices.bundle.BundleTemplateService;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.order.EntryGroup;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import com.jsl.core.model.AuctionEventModel;
import com.jsl.facades.constants.JslFacadesConstants;
import com.jsl.facades.exception.JslRuntimeException;

import reactor.util.CollectionUtils;


/**
 * @author manav.magoo
 *
 */
public class SendBidWinnerEmailContext extends AbstractEmailContext<OrderProcessModel>
{
	private BundleTemplateService bundleTemplateService;
	public static final String DEFAULTNAME = "JSL";

	public static final String AUCTIONID = "auctionID";
	public static final String AUCTIONSTARTTIME = "auctionStartTime";
	public static final String AUCTIONENDTIME = "auctionEndTime";
	public static final String LOCATION = "location";
	public static final String H1PRICE = "h1Price";
	public static final String LOTNAME = "lotName";

	@Override
	public void init(final OrderProcessModel orderProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(orderProcessModel, emailPageModel);

		final OrderModel order = orderProcessModel.getOrder();
		put(FROM_EMAIL, getEmail());
		put(FROM_DISPLAY_NAME,
				getConfigurationService().getConfiguration().getString(JslFacadesConstants.JSLEMAILFROMDISPLAYNAME, DEFAULTNAME));

		final List<BundleTemplateModel> bundles = new ArrayList<>();
		final List<EntryGroup> entryGroups = order.getEntryGroups();
		entryGroups.forEach(entry -> {
			bundles.add(getBundleTemplateService()
					.getBundleTemplateForCode(String.valueOf(entry.getChildren().get(0).getExternalReferenceId())));
		});

		if (!CollectionUtils.isEmpty(bundles))
		{
			final AuctionEventModel auction = bundles.get(0).getAuctionEvent();
			put(AUCTIONID, auction.getAuctionId());
			put(AUCTIONSTARTTIME, String.valueOf(auction.getStartDate()));
			put(AUCTIONENDTIME, auction.getEndDate());
			put(LOCATION, order.getEntries().get(0).getProduct().getLocation());
			put(H1PRICE, String.valueOf(bundles.get(0).getBidWonPrice().intValue()));
			put(LOTNAME, bundles.get(0).getName());
		}
		else
		{

			throw new JslRuntimeException(
					MessageFormat.format("SendBidWinnerEmailContext Exception : No Bundle found in Order : {0}!", order.getCode()));

		}

	}



	/**
	 * @return the auctionid
	 */
	public String getAuctionid()
	{
		return (String) get(AUCTIONID);
	}

	public String getLocation()
	{
		return (String) get(LOCATION);
	}



	/**
	 * @return the otp
	 */
	public String getAuctionStartTime()
	{
		return (String) get(AUCTIONSTARTTIME);
	}

	public String getH1Price()
	{
		return (String) get(H1PRICE);
	}

	public String getLotName()
	{
		return (String) get(LOTNAME);
	}


	/**
	 * @return the defaultname
	 */
	public String getDefaultname()
	{
		return DEFAULTNAME;
	}


	@Override
	protected BaseSiteModel getSite(final OrderProcessModel orderProcessModel)
	{
		return orderProcessModel.getOrder().getSite();
	}

	@Override
	protected CustomerModel getCustomer(final OrderProcessModel orderProcessModel)
	{
		return (CustomerModel) orderProcessModel.getOrder().getUser();
	}



	@Override
	protected LanguageModel getEmailLanguage(final OrderProcessModel orderProcessModel)
	{
		return orderProcessModel.getOrder().getLanguage();
	}




	/**
	 * @return the bundleTemplateService
	 */
	public BundleTemplateService getBundleTemplateService()
	{
		return bundleTemplateService;
	}



	/**
	 * @param bundleTemplateService
	 *           the bundleTemplateService to set
	 */
	public void setBundleTemplateService(final BundleTemplateService bundleTemplateService)
	{
		this.bundleTemplateService = bundleTemplateService;
	}
}
