/**
 *
 */
package com.jsl.facades.job;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.jsl.facades.auction.JslBidDetailsExportService;


/**
 * @author subhrajitk766
 *
 */
public class JslBidDetailsExportViaCronjob extends AbstractJobPerformable<CronJobModel>
{
	private static final Logger LOG = Logger.getLogger(JslBidDetailsExportViaCronjob.class);

	@Autowired
	private JslBidDetailsExportService jslBidDetailsExportService;


	@Override
	public PerformResult perform(final CronJobModel bidDeatilsModel)
	{
		PerformResult performResult = null;
		LOG.info("bidDeatilsModel>>>>>>>>>>>>" + bidDeatilsModel);
		try
		{
			jslBidDetailsExportService.createBidDetailsExport(bidDeatilsModel);
			performResult = new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
		catch (final Exception ex)
		{
			LOG.error("Exception Occurred during excel creation " + ex.getMessage());
			performResult = new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);

		}
		return performResult;


	}



}
