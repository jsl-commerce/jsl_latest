/**
 * 
 */
package com.jsl.facades.support.ticket.populator;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.jsl.core.model.JslCustomerFeedbackModel;
import com.jsl.core.util.JslB2bUnitUtil;
import com.jsl.facades.supportticket.dto.JslCustomerFeedbackDto;


/**
 * @author himanshu.sial
 *
 */
public class JslCustomerFeedbackFormDtoPopulator implements Populator<JslCustomerFeedbackModel, JslCustomerFeedbackDto>
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(JslCustomerFeedbackModel source, JslCustomerFeedbackDto target) throws ConversionException
	{
		// XXX Auto-generated method stub
		target.setComments(source.getUserComments());
		target.setCustomerName(source.getUser().getUid());
		target.setCustomerNumber(this.getCustomerId(source.getUser()));
		target.setQ1(source.getQ1());
		target.setQ2(source.getQ2());
		target.setQ3(source.getQ3());
		target.setQ4(source.getQ4());
		target.setQ5(source.getQ5());
		target.setQ6(source.getQ6());
		target.setQ7(source.getQ7());
		target.setQ8(source.getQ8());
		target.setQ9(source.getQ9());
		target.setQ10(source.getQ10());
		target.setQ11(source.getQ11());
		target.setQ12(source.getQ12());
		target.setQ13(source.getQ13());
		target.setComplaintNumber(source.getCustomerComplaint().getComplaintNumber());
		target.setFeedbackDate(source.getCreationtime() != null ? source.getCreationtime().toString() : "");
	}

	private String getCustomerId(final UserModel user)
	{

		final B2BUnitModel parentb2bUnit = JslB2bUnitUtil.getParentB2bUnit(user);

		if (parentb2bUnit != null)
		{
			return parentb2bUnit.getUid();
		}
		else
		{
			return user.getUid();
		}
	}

}
