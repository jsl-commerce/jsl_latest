/**
 * 
 */
package com.jsl.facades.support.ticket.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.jsl.core.model.JslCustomerFeedbackModel;
import com.jsl.facades.supportticket.dto.JslCustomerFeedbackDto;


/**
 * @author himanshu.sial
 *
 */
public class JslCustomerFeedbackFormModelPopulator implements Populator<JslCustomerFeedbackDto, JslCustomerFeedbackModel>
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(JslCustomerFeedbackDto source, JslCustomerFeedbackModel target) throws ConversionException
	{
		// XXX Auto-generated method stub
		target.setUserComments(source.getComments());
		target.setQ1(source.getQ1());
		target.setQ2(source.getQ2());
		target.setQ3(source.getQ3());
		target.setQ4(source.getQ4());
		target.setQ5(source.getQ5());
		target.setQ6(source.getQ6());
		target.setQ7(source.getQ7());
		target.setQ8(source.getQ8());
		target.setQ9(source.getQ9());
		target.setQ10(source.getQ10());
		target.setQ11(source.getQ11());
		target.setQ12(source.getQ12());
		target.setQ13(source.getQ13());
	}

}
