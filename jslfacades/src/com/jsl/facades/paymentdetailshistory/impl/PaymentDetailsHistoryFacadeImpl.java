/**
 *
 */
package com.jsl.facades.paymentdetailshistory.impl;

import de.hybris.platform.sap.sapinvoiceaddon.model.SapB2BDocumentModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jsl.core.model.JslPaymentTransactionModel;
import com.jsl.core.model.PaymentSaleOrderModel;
import com.jsl.core.paymentdetailshistory.service.PaymentDetailsHistoryService;
import com.jsl.facades.payment.dto.SaleOrderDetailDto;
import com.jsl.facades.payment.dto.TransactionMasterDto;
import com.jsl.facades.payment.dto.TransactionMasterSearchDto;
import com.jsl.facades.paymentdetailshistory.PaymentDetailsHistoryFacade;
import com.jsl.facades.paymentdetailshistory.dto.PaymentDetailsDto;
import com.jsl.facades.paymentdetailshistory.dto.PaymentDetailsHistoryDto;


/**
 * @author Kiranraj S.
 *
 */
public class PaymentDetailsHistoryFacadeImpl implements PaymentDetailsHistoryFacade
{
	private static final Logger LOG = Logger.getLogger(PaymentDetailsHistoryFacadeImpl.class);

	private PaymentDetailsHistoryService paymentDetailsHistoryService;

	private Converter<SapB2BDocumentModel, PaymentDetailsHistoryDto> paymentDetailsHistoryConverter;

	private Converter<PaymentSaleOrderModel, SaleOrderDetailDto> saleOrderDetailsConverter;

	private Converter<JslPaymentTransactionModel, TransactionMasterDto> transactionDetailsConverter;

	private static final String PAYMENT_TYPE = "payment";


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.jsl.facades.paymentdetailshistory.PaymentDetailsHistoryFacade#getTransactionDetailsFromDate(com.jsl.facades.
	 * payment.dto.TransactionMasterSearchDto)
	 */
	@Override
	public List<TransactionMasterDto> getTransactionDetailsFromDate(TransactionMasterSearchDto transactionMasterSearchDto)
			throws Exception
	{
		// XXX Auto-generated method stub
		return transactionDetailsConverter.convertAll(paymentDetailsHistoryService
				.getTransactionDetailsFromDate(transactionMasterSearchDto.getStartDate(), transactionMasterSearchDto.getEndDate()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jsl.facades.paymentdetailshistory.PaymentDetailsHistoryFacade#getTransactionDetails()
	 */
	@Override
	public List<TransactionMasterDto> getTransactionDetailsForCurrentUser() throws Exception
	{
		// XXX Auto-generated method stub
		return transactionDetailsConverter.convertAll(paymentDetailsHistoryService.getTransactionDetailsForCurrentUser());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jsl.facades.paymentdetailshistory.PaymentDetailsHistoryFacade#getsaleOrderDetails(java.lang.String)
	 */
	@Override
	public List<SaleOrderDetailDto> getsaleOrderDetails(final PaymentDetailsDto paymentDetails) throws Exception
	{
		// XXX Auto-generated method stub
		List<PaymentSaleOrderModel> saleOrders = null;
		saleOrders = paymentDetailsHistoryService.getSaleOrderByNumber(paymentDetails, PAYMENT_TYPE);
		return saleOrderDetailsConverter.convertAll(saleOrders);
	}

	@Override
	public List<PaymentDetailsHistoryDto> getPaymentHistoryForCurrentUser() throws Exception
	{
		final List<PaymentDetailsHistoryDto> paymentDetailsHistoryDtoList = new ArrayList<>();
		List<SapB2BDocumentModel> paymentDetailsHistoryModelList = null;
		paymentDetailsHistoryModelList = getPaymentDetailsHistoryService().getPaymentHistoryForCurrentUser();
		paymentDetailsHistoryModelList.forEach(paymentDetails -> {
			paymentDetailsHistoryDtoList.add(getPaymentDetailsHistoryConverter().convert(paymentDetails));
		});

		return paymentDetailsHistoryDtoList;
	}

	@Override
	public List<PaymentDetailsHistoryDto> getPendingPaymentDetailsForCurrentUser() throws Exception
	{
		final List<PaymentDetailsHistoryDto> paymentDetailsHistoryDtoList = new ArrayList<>();
		List<SapB2BDocumentModel> paymentDetailsHistoryModelList = null;
		paymentDetailsHistoryModelList = getPaymentDetailsHistoryService().getPendingPaymentDetailsForCurrentUser();
		paymentDetailsHistoryModelList.forEach(paymentDetails -> {
			paymentDetailsHistoryDtoList.add(getPaymentDetailsHistoryConverter().convert(paymentDetails));
		});

		return paymentDetailsHistoryDtoList;
	}

	@Override
	public List<PaymentDetailsHistoryDto> getPendingPaymentDetailsBasedOnSearchCriteria(final PaymentDetailsDto paymentDetails)
			throws Exception
	{
		final List<PaymentDetailsHistoryDto> paymentDetailsHistoryDtoList = new ArrayList<>();
		List<SapB2BDocumentModel> paymentDetailsHistoryModelList = null;
		paymentDetailsHistoryModelList = getPaymentDetailsHistoryService()
				.getPendingPaymentDetailsBasedOnSearchCriteria(paymentDetails);
		paymentDetailsHistoryModelList.forEach(paymentDetail -> {
			paymentDetailsHistoryDtoList.add(getPaymentDetailsHistoryConverter().convert(paymentDetail));
		});

		return paymentDetailsHistoryDtoList;
	}

	/**
	 * @return the paymentDetailsHistoryService
	 */
	public PaymentDetailsHistoryService getPaymentDetailsHistoryService()
	{
		return paymentDetailsHistoryService;
	}

	/**
	 * @param paymentDetailsHistoryService
	 *           the paymentDetailsHistoryService to set
	 */
	public void setPaymentDetailsHistoryService(final PaymentDetailsHistoryService paymentDetailsHistoryService)
	{
		this.paymentDetailsHistoryService = paymentDetailsHistoryService;
	}

	/**
	 * @return the paymentDetailsHistoryConverter
	 */
	public Converter<SapB2BDocumentModel, PaymentDetailsHistoryDto> getPaymentDetailsHistoryConverter()
	{
		return paymentDetailsHistoryConverter;
	}

	/**
	 * @param paymentDetailsHistoryConverter
	 *           the paymentDetailsHistoryConverter to set
	 */
	public void setPaymentDetailsHistoryConverter(
			final Converter<SapB2BDocumentModel, PaymentDetailsHistoryDto> paymentDetailsHistoryConverter)
	{
		this.paymentDetailsHistoryConverter = paymentDetailsHistoryConverter;
	}

	/**
	 * @return the saleOrderDetailsConverter
	 */
	public Converter<PaymentSaleOrderModel, SaleOrderDetailDto> getSaleOrderDetailsConverter()
	{
		return saleOrderDetailsConverter;
	}

	/**
	 * @param saleOrderDetailsConverter
	 *           the saleOrderDetailsConverter to set
	 */
	public void setSaleOrderDetailsConverter(Converter<PaymentSaleOrderModel, SaleOrderDetailDto> saleOrderDetailsConverter)
	{
		this.saleOrderDetailsConverter = saleOrderDetailsConverter;
	}

	/**
	 * @return the transactionDetailsConverter
	 */
	public Converter<JslPaymentTransactionModel, TransactionMasterDto> getTransactionDetailsConverter()
	{
		return transactionDetailsConverter;
	}

	/**
	 * @param transactionDetailsConverter
	 *           the transactionDetailsConverter to set
	 */
	public void setTransactionDetailsConverter(
			Converter<JslPaymentTransactionModel, TransactionMasterDto> transactionDetailsConverter)
	{
		this.transactionDetailsConverter = transactionDetailsConverter;
	}
}
