/**
 *
 */
package com.jsl.facades.paymentdetailshistory;

import java.util.List;

import com.jsl.facades.payment.dto.SaleOrderDetailDto;
import com.jsl.facades.payment.dto.TransactionMasterDto;
import com.jsl.facades.payment.dto.TransactionMasterSearchDto;
import com.jsl.facades.paymentdetailshistory.dto.PaymentDetailsDto;
import com.jsl.facades.paymentdetailshistory.dto.PaymentDetailsHistoryDto;


/**
 * @author Kiranraj S.
 *
 */
public interface PaymentDetailsHistoryFacade
{

	public List<PaymentDetailsHistoryDto> getPaymentHistoryForCurrentUser() throws Exception;

	public List<PaymentDetailsHistoryDto> getPendingPaymentDetailsForCurrentUser() throws Exception;

	public List<PaymentDetailsHistoryDto> getPendingPaymentDetailsBasedOnSearchCriteria(PaymentDetailsDto paymentDetails)
			throws Exception;

	public List<SaleOrderDetailDto> getsaleOrderDetails(final PaymentDetailsDto paymentDetails) throws Exception;

	public List<TransactionMasterDto> getTransactionDetailsForCurrentUser() throws Exception;

	public List<TransactionMasterDto> getTransactionDetailsFromDate(final TransactionMasterSearchDto transactionMasterSearchDto)
			throws Exception;
}
