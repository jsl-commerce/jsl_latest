/**
 *
 */
package com.jsl.facades.paymentdetailshistory.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.sap.sapinvoiceaddon.model.SapB2BDocumentModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.log4j.Logger;

import com.jsl.facades.paymentdetailshistory.dto.PaymentDetailsHistoryDto;


/**
 * @author Kiranraj S.
 *
 */
public class PaymentDetailsHistoryPopulator implements Populator<SapB2BDocumentModel, PaymentDetailsHistoryDto>
{
	private static final Logger LOG = Logger.getLogger(PaymentDetailsHistoryPopulator.class);

	private static final String COMMA = ",";
	private static final String UNIT_MM = "mm";
	private static final String EMPTY_STRING = "";

	@Override
	public void populate(final SapB2BDocumentModel source, final PaymentDetailsHistoryDto target) throws ConversionException
	{
		try
		{
			target.setCustomerNumber(source.getCustomerNumber());
			target.setDueDate(source.getDueDate() == null ? EMPTY_STRING : source.getDueDate().toString());
			target.setAmount(source.getAmount().doubleValue());
			target.setInvoiceDate(source.getInvoiceDate());
			target.setInvoiceNumber(source.getInvoiceNumber());
			target.setPaymentDate(source.getPaymentDate());
			target.setOrderNumber(source.getOrderNumber());
			target.setDocumentNumber(source.getDocumentNumber());
			target.setOpenAmount(source.getOpenAmount() != null ? source.getOpenAmount().toString() : "");
			target.setSalesOffice(source.getSalesOffice());
			target.setOurTaxNumber(source.getOurTaxNumber());
			target.setSalesDistrict(source.getSalesDistrict());
			target.setInvoiceCompanyCode(source.getInvoiceCompanyCode());
			target.setSalesGroup(source.getSalesGroup());
			target.setKamId(source.getInvoiceKamId());
			target.setKamName(source.getInvoiceKamName());
			target.setSalesOrg(source.getSalesOrg());
			target.setGrandTotal(source.getGrandTotal());
			target.setFiscalYear(source.getFiscalYear());
			target.setJslTotalRefAmount(source.getJslTotalRefAmount());
			target.setJslRefNumber(source.getJslReferenceNumber());
			target.setBusArea(source.getBusArea());
			target.setSbiRefNumber(source.getSbiReferenceNumber());
			target.setPaymentStatus(source.getPaymentStatus());
			target.setPaymentStatusDescription(source.getPaymentStatusDescription());
			target.setPaymentTypeAdvance(source.getPaymentTypeAdvance());
			target.setPaymentSource(source.getPaymentSource());
			target.setGstInvoiceNumber(source.getGstInvoiceNumber());
			target.setPlant(source.getInvoicePlant());


		}
		catch (final Exception e)
		{
			LOG.error("Exception in PaymentDetailsHistoryPopulator.populate(): ", e);
		}
	}

}
