/**
 *
 */
package com.jsl.facades.reports.service;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;


/**
 * @author s.bhambhani
 *
 */
public interface JslExcelExportService
{

	public void exportInvoices(HttpServletResponse response) throws Exception;

	public boolean exportCustomerComplaintreport(HttpServletResponse response, File file) throws Exception;
	
	public void customerFeedbackAnalysis(HttpServletResponse response) throws IOException, ConversionException, Exception;

}
