/**
 *
 */
package com.jsl.facades.reports.service.impl;

import de.hybris.platform.sap.sapinvoiceaddon.model.SapB2BDocumentModel;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.SOAPException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.jsl.core.dto.reports.AccountStatementDto;
import com.jsl.core.dto.reports.AccountStatementReportDto;
import com.jsl.core.dto.reports.OrderBookingReportDto;
import com.jsl.core.dto.reports.PopPlantReportDto;
import com.jsl.core.dto.reports.PopYardReportDto;
import com.jsl.core.dto.reports.SalesBillingDetailsReportDto;
import com.jsl.core.dto.reports.ShipmentDetailsDto;
import com.jsl.core.dto.reports.customer.dashboard.CustomerDashboardDto;
import com.jsl.core.enums.Customer360Reports;
import com.jsl.core.paymentdetailshistory.service.PaymentDetailsHistoryService;
import com.jsl.cpi.integration.service.CpiReportsService;
import com.jsl.cpi.invoice.batch.dto.ZFMINVBATCHHYBRISResponse;
import com.jsl.cpi.report.dto.accountstatement.ZFI137HYBRISResponse;
import com.jsl.cpi.report.dto.customer.dashboard.ZSDCUSTDASHBOARDHYBRISResponse;
import com.jsl.cpi.report.dto.orderbooking.ZSD35HYBRISResponse;
import com.jsl.cpi.report.dto.pop.plant.ZPOPHYBRISResponse;
import com.jsl.cpi.report.dto.pop.yard.ZSD35CHYBRISResponse;
import com.jsl.cpi.report.dto.sales.ZSD34HYBRISResponse;
import com.jsl.cpi.report.dto.vehicle.tracking.ZSDTRACKDELHYBRISResponse;
import com.jsl.facades.populators.InvoiceDtoPopulatorForCpiReponse;
import com.jsl.facades.populators.customer360.reports.CustomerReportsPopulator;
import com.jsl.facades.reports.service.JslReportServiceFacade;
import com.jsl.facades.supportticket.dto.InvoiceWithMaterialDto;



/**
 * @author himanshu.sial
 *
 */
public class JslReportServiceFacadeImpl implements JslReportServiceFacade
{

	private static final Logger LOG = Logger.getLogger(JslReportServiceFacadeImpl.class);

	private CustomerReportsPopulator customerReportsPopulator;

	private CpiReportsService cpiReportsService;

	private final String JSL = "JS03";

	private final String JSHL = "JS01";

	@Qualifier("invoiceDtoPopulatorForCpiReponse")
	@Autowired
	private InvoiceDtoPopulatorForCpiReponse invoiceDtoPopulator;

	@Autowired
	private PaymentDetailsHistoryService paymentDetailsHistoryService;

	@Override
	public Object getReportsAsJson(final Customer360Reports report) throws SOAPException
	{
		if (Customer360Reports.POPPLANTREPORT.equals(report))
		{
			return customerReportsPopulator
					.getPopPlantReportReponseDto((ZPOPHYBRISResponse) cpiReportsService.getPopPlantReportResponse());
		}
		else if (Customer360Reports.POPYARDREPORT.equals(report))
		{
			return customerReportsPopulator
					.getPopYardReportReponseDto((ZSD35CHYBRISResponse) cpiReportsService.getPopYardReportResponse());
		}
		else if (Customer360Reports.ORDERBOOKINGREPORT.equals(report))
		{
			final List<OrderBookingReportDto> orderBokingReportList = customerReportsPopulator
					.getOrderBookingReportReponseDto((ZSD35HYBRISResponse) cpiReportsService.getOrderBookingReportResponse());
			this.sortOrderBookingListByDate(orderBokingReportList);
			return orderBokingReportList;
		}
		else if (Customer360Reports.SALESBILLINGREPORT.equals(report))
		{
			return customerReportsPopulator
					.getSalesReportReponseDto((ZSD34HYBRISResponse) cpiReportsService.getSalesReportResponse());
		}
		else if (Customer360Reports.ACCOUNTSTATEMENTREPORT.equals(report))
		{
			LOG.info("<<<<<<<<<<<<<<<<<<<Response for Account Statement Report>>>>>>>>>>>>>>>>");
			final AccountStatementDto response = customerReportsPopulator
					.getAccountStatementResponseDto((ZFI137HYBRISResponse) cpiReportsService.getAccountStatementReportResponse());

			this.sortAccountStatementListByDate(response);
			return response;
		}
		else if (Customer360Reports.SHIPMENTDETAILSREPORT.equals(report))
		{
			LOG.info("<<<<<<<<<<<<<<<<<<<Response for Shipment Details Report>>>>>>>>>>>>>>>>");
			final ShipmentDetailsDto response = customerReportsPopulator
					.getShipmentDetailsResponseDto((ZSDTRACKDELHYBRISResponse) cpiReportsService.getVehicleTrackingReportResonse());

			//this.sortAccountStatementListByDate(response);
			return response;
		}
		return null;
	}



	/*
	 * (non-Javadoc)
	 *
	 * @see com.jsl.facades.reports.service.JslReportServiceFacade#getCustomerDashboard()
	 */
	@Override
	public CustomerDashboardDto getCustomerDashboard() throws SOAPException
	{
		return customerReportsPopulator
				.getCustomerDashboardDto((ZSDCUSTDASHBOARDHYBRISResponse) cpiReportsService.getCustomerDashboardResponse());
	}


	@Override
	public void exportReport(final Customer360Reports report, final String companyData, final HttpServletResponse response)
			throws IOException
	{

		ICsvBeanWriter csvWriter = null;
		try
		{

			addReponseHeader(response, report);
			csvWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);

			if (Customer360Reports.SALESBILLINGREPORT.equals(report))
			{

				final List<SalesBillingDetailsReportDto> salesBillingDetailsReportList = (List<SalesBillingDetailsReportDto>) this
						.getReportsAsJson(report);
				final String[] header =
				{ "Plant Desc", "Invoice No.", "Product Type", "Grade", "Quality", "Finish", "Thickness", "Width", "Length", "Edge",
						"GST Invoice No.", "P.O Number", "Billing Date", "Billed Quantity", "Basic Rate", "PVC", "ILP", "Vehicle No.",
						"Transporter Name", "Transporter No.", "Driver No.", "SO No.", "SO Item" };

				final String[] fieldMapping =
				{ "plantDescription", "invoiceNumber", "productType", "grade", "quality", "finish", "thickness", "width", "length",
						"edgeCondition", "gstInvoiceNumber", "poNumber", "billingDate", "billedQuantity", "basicPrice", "pvc", "ilp",
						"vehicleNumber", "transporterName", "transporterNumber", "driverNumber", "saleOrderNumber", "saleItemNumber" };
				csvWriter.writeHeader(header);
				for (final SalesBillingDetailsReportDto salesBillingReport : salesBillingDetailsReportList)
				{
					csvWriter.write(salesBillingReport, fieldMapping);
				}
			}

			else if (Customer360Reports.POPPLANTREPORT.equals(report))
			{
				final List<PopPlantReportDto> popReportList = (List<PopPlantReportDto>) this.getReportsAsJson(report);
				final String[] header =
				{ "Sales Order Number", "Item", "Product Type", "Sales Order Date", "Grade", "Thickness", "Width", "Length", "Edge",
						"Finish", "Quality", "Basic Rate", "Order Quantity", "Dispatch Quantity", "Market Balance",
						"Allocation Quantity", "O.A Number", "Customer Required Delivery Date", "Hold Status", "PW Min", "PW Max" };

				final String[] fieldMapping =
				{ "salesOrderNumber", "item", "productType", "salesOrderDate", "Grade", "thickness", "width", "length",
						"edgeCondition", "finish", "quality", "basicPrice", "orderItemQuantity", "dispatchQuantity", "marketBalance",
						"orderAmount", "orderAcceptanceNumber", "customerRequiredDeliveryDate", "holdStatus", "partWtMin",
						"partWtMax" };
				csvWriter.writeHeader(header);
				for (final PopPlantReportDto PopReport : popReportList)
				{
					csvWriter.write(PopReport, fieldMapping);
				}

			}

			else if (Customer360Reports.POPYARDREPORT.equals(report))
			{
				final List<PopYardReportDto> popReportList = (List<PopYardReportDto>) this.getReportsAsJson(report);
				final String[] header =
				{ "Sales Order Number", "Item", "Product Type", "Sales Order Date", "Grade", "Thickness", "Width", "Length", "Edge",
						"Finish", "Quality", "Basic Rate", "Order Quantity", "Dispatch Quantity", "Balance for Supply",
						"Allocation Quantity", "O.A Number", "Customer Required Delivery Date", "PW Min", "PW Max" };

				final String[] fieldMapping =
				{ "salesOrderNumber", "item", "productType", "salesOrderDate", "Grade", "thickness", "width", "length",
						"edgeCondition", "finish", "quality", "basicPrice", "orderItemQuantity", "dispatchQuantity", "marketBalance",
						"orderAmount", "orderAcceptanceNumber", "customerRequiredDeliveryDate", "partWtMin", "partWtMax" };
				csvWriter.writeHeader(header);
				for (final PopYardReportDto PopReport : popReportList)
				{
					csvWriter.write(PopReport, fieldMapping);
				}

			}
			else if (Customer360Reports.ORDERBOOKINGREPORT.equals(report))
			{
				final List<OrderBookingReportDto> OrderBookingReportList = (List<OrderBookingReportDto>) this
						.getReportsAsJson(report);
				this.sortOrderBookingListByDate(OrderBookingReportList);
				final String[] header =
				{ "Sales Document", "Item", "Product Type", "P.O Number", "P.O Date", "Quality", "Grade", "Thickness", "Width",
						"Length", "Finish", "Edge", "Document Date", "Order Quantity", "Rate" };

				final String[] fieldMapping =
				{ "salesOrderNumber", "item", "productType", "poNumber", "poDate", "quality", "grade", "thickness", "width", "length",
						"finish", "edgeCondition", "salesOrderDate", "orderItemQuantity", "basicPrice" };
				csvWriter.writeHeader(header);
				for (final OrderBookingReportDto OrderBookingReport : OrderBookingReportList)
				{
					csvWriter.write(OrderBookingReport, fieldMapping);
				}

			}

			else if (Customer360Reports.ACCOUNTSTATEMENTREPORT.equals(report))
			{
				final AccountStatementDto accountStatementResponse = (AccountStatementDto) this.getReportsAsJson(report);
				this.sortAccountStatementListByDate(accountStatementResponse);
				final String[] header =
				{ "Company Code", "Post. Date", "Document No", "Document Desc.", "Ref / InstNo", "Reference", "Spl",
						"Item Description", "Due Date", "Amount", "Cur", "Debit Amount", "Credit Amount" };

				final String[] fieldMapping =
				{ "companycode", "postDate", "documentNumber", "documentDescription", "refInstNumber", "reference", "spl",
						"itemDescription", "dueDate", "amount", "currency", "debitAmount", "creditAmount" };
				csvWriter.writeHeader(header);

				for (final AccountStatementReportDto accountStatementReport : accountStatementResponse.getAccountStatements())
				{
					if (accountStatementReport.getCompanycode().equalsIgnoreCase(companyData))
					{
						csvWriter.write(accountStatementReport, fieldMapping);
					}
					else if (companyData.equalsIgnoreCase("both"))
					{
						csvWriter.write(accountStatementReport, fieldMapping);
					}

				}

			}

		}
		catch (final Exception e)
		{
			LOG.error("Exception in file Download : " + e);
		}
		finally
		{
			if (csvWriter != null)
			{
				csvWriter.close();
			}
		}

	}

	public void exportReport(final Customer360Reports report, final String toDate, final String fromDate, final String companyData,
			final HttpServletResponse response) throws IOException
	{
		ICsvBeanWriter csvWriter = null;
		try
		{

			addReponseHeader(response, report);
			csvWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);

			if (Customer360Reports.SALESBILLINGREPORT.equals(report))
			{

				final List<SalesBillingDetailsReportDto> salesBillingDetailsReportList = (List<SalesBillingDetailsReportDto>) this
						.getReportsAsJson(report);
				final String[] header =
				{ "Plant Desc", "Invoice No.", "Product Type", "Grade", "Quality", "Finish", "Thickness", "Width", "Length", "Edge",
						"GST Invoice No.", "P.O Number", "Billing Date", "Billed Quantity", "Basic Rate", "PVC", "ILP", "Vehicle No.",
						"Transporter Name", "Transporter No.", "Driver No.", "SO No.", "SO Item" };

				final String[] fieldMapping =
				{ "plantDescription", "invoiceNumber", "productType", "grade", "quality", "finish", "thickness", "width", "length",
						"edgeCondition", "gstInvoiceNumber", "poNumber", "billingDate", "billedQuantity", "basicPrice", "pvc", "ilp",
						"vehicleNumber", "transporterName", "transporterNumber", "driverNumber", "saleOrderNumber", "saleItemNumber" };
				csvWriter.writeHeader(header);
				for (final SalesBillingDetailsReportDto salesBillingReport : salesBillingDetailsReportList)
				{
					csvWriter.write(salesBillingReport, fieldMapping);
				}
			}

			else if (Customer360Reports.POPPLANTREPORT.equals(report))
			{
				final List<PopPlantReportDto> popReportList = (List<PopPlantReportDto>) this.getReportsAsJson(report);
				final String[] header =
				{ "Sales Order Number", "Item", "Product Type", "Sales Order Date", "Grade", "Thickness", "Width", "Length", "Edge",
						"Finish", "Quality", "Basic Rate", "Order Quantity", "Dispatch Quantity", "Market Balance",
						"Allocation Quantity", "O.A Number", "Customer Required Delivery Date", "Hold Status", "PW Min", "PW Max" };

				final String[] fieldMapping =
				{ "salesOrderNumber", "item", "productType", "salesOrderDate", "Grade", "thickness", "width", "length",
						"edgeCondition", "finish", "quality", "basicPrice", "orderItemQuantity", "dispatchQuantity", "marketBalance",
						"orderAmount", "orderAcceptanceNumber", "customerRequiredDeliveryDate", "holdStatus", "partWtMin",
						"partWtMax" };
				csvWriter.writeHeader(header);
				for (final PopPlantReportDto PopReport : popReportList)
				{
					csvWriter.write(PopReport, fieldMapping);
				}

			}

			else if (Customer360Reports.POPYARDREPORT.equals(report))
			{
				final List<PopYardReportDto> popReportList = (List<PopYardReportDto>) this.getReportsAsJson(report);
				final String[] header =
				{ "Sales Order Number", "Item", "Product Type", "Sales Order Date", "Grade", "Thickness", "Width", "Length", "Edge",
						"Finish", "Quality", "Basic Rate", "Order Quantity", "Dispatch Quantity", "Balance for Supply",
						"Allocation Quantity", "O.A Number", "Customer Required Delivery Date", "PW Min", "PW Max" };

				final String[] fieldMapping =
				{ "salesOrderNumber", "item", "productType", "salesOrderDate", "Grade", "thickness", "width", "length",
						"edgeCondition", "finish", "quality", "basicPrice", "orderItemQuantity", "dispatchQuantity", "marketBalance",
						"orderAmount", "orderAcceptanceNumber", "customerRequiredDeliveryDate", "partWtMin", "partWtMax" };
				csvWriter.writeHeader(header);
				for (final PopYardReportDto PopReport : popReportList)
				{
					csvWriter.write(PopReport, fieldMapping);
				}

			}
			else if (Customer360Reports.ORDERBOOKINGREPORT.equals(report))
			{
				final List<OrderBookingReportDto> OrderBookingReportList = (List<OrderBookingReportDto>) this
						.getReportsAsJson(report);
				this.sortOrderBookingListByDate(OrderBookingReportList);
				final String[] header =
				{ "Sales Document", "Item", "Product Type", "P.O Number", "P.O Date", "Quality", "Grade", "Thickness", "Width",
						"Length", "Finish", "Edge", "Document Date", "Order Quantity", "Rate" };

				final String[] fieldMapping =
				{ "salesOrderNumber", "item", "productType", "poNumber", "poDate", "quality", "grade", "thickness", "width", "length",
						"finish", "edgeCondition", "salesOrderDate", "orderItemQuantity", "basicPrice" };
				csvWriter.writeHeader(header);
				for (final OrderBookingReportDto OrderBookingReport : OrderBookingReportList)
				{
					csvWriter.write(OrderBookingReport, fieldMapping);
				}

			}

			else if (Customer360Reports.ACCOUNTSTATEMENTREPORT.equals(report))
			{
				final AccountStatementDto accountStatementResponse = (AccountStatementDto) this.getReportsAsJson(report);
				this.filterAccountStatementListByDate(accountStatementResponse, toDate, fromDate, companyData);
				this.sortAccountStatementListByDate(accountStatementResponse);
				final String[] header =
				{ "Company Code", "Post. Date", "Document No", "Document Desc.", "Ref / InstNo", "Reference", "Spl",
						"Item Description", "Due Date", "Amount", "Cur", "Debit Amount", "Credit Amount" };

				final String[] fieldMapping =
				{ "companycode", "postDate", "documentNumber", "documentDescription", "refInstNumber", "reference", "spl",
						"itemDescription", "dueDate", "amount", "currency", "debitAmount", "creditAmount" };
				csvWriter.writeHeader(header);
				for (final AccountStatementReportDto accountStatementReport : accountStatementResponse.getAccountStatements())
				{
					csvWriter.write(accountStatementReport, fieldMapping);
				}

			}

		}
		catch (final Exception e)
		{
			LOG.error("Exception in file Download : " + e);
		}
		finally
		{
			if (csvWriter != null)
			{
				csvWriter.close();
			}
		}

	}

	@Override
	public List<InvoiceWithMaterialDto> getInvoiceDetailsByInvoiceNumber(final String invoiceNumber) throws SOAPException
	{
		List<InvoiceWithMaterialDto> invoiceDetails = null;
		try
		{
			final List<SapB2BDocumentModel> invoiceList = paymentDetailsHistoryService
					.getAllInvoiceDetailsByInvoiceNumbers(Arrays.asList(invoiceNumber));
			if (!invoiceList.isEmpty())
			{
				final ZFMINVBATCHHYBRISResponse response = (ZFMINVBATCHHYBRISResponse) cpiReportsService
						.getInvoiceDetailsByInvoiceNumber(invoiceNumber);
				invoiceDetails = invoiceDtoPopulator.populate(response, invoiceList);
			}
			else
			{
				invoiceDetails = new ArrayList<InvoiceWithMaterialDto>();
			}
		}
		catch (final Exception e)
		{
			LOG.error("Exception in getInvoiceDetailsByInvoiceNumber(): ", e);
		}
		return invoiceDetails;
	}

	private void addReponseHeader(final HttpServletResponse response, final Customer360Reports report)
	{
		final String csvFileName = new SimpleDateFormat("yyyy-MM-dd_HH:mm").format(new Date(System.currentTimeMillis()))
				+ report.getCode() + ".csv";
		final String headerKey = "Content-Disposition";
		final String headerValue = String.format("attachment; filename=\"%s\"", csvFileName);
		response.setHeader(headerKey, headerValue);
		response.setContentType("text/comma-separated-values;charset=UTF-8");
	}

	private void sortAccountStatementListByDate(final AccountStatementDto dto)
	{
		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		Collections.sort(dto.getAccountStatements(),
				(a1, a2) -> LocalDate.parse(a1.getDueDate(), formatter).compareTo(LocalDate.parse(a2.getDueDate(), formatter)));
	}

	private void filterAccountStatementListByDate(final AccountStatementDto dto, final String toDate, final String fromDate,
			final String companyData) throws ParseException
	{

		final DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		final Date toDate1 = dateFormat.parse(toDate);
		final Date fromDate1 = dateFormat.parse(fromDate);
		final List<AccountStatementReportDto> accountStatementReportList = new ArrayList<>();
		final List<AccountStatementReportDto> accountStatementReportDtos = dto.getAccountStatements();
		for (final AccountStatementReportDto reportDto : accountStatementReportDtos)
		{
			if (reportDto.getCompanycode().equalsIgnoreCase(companyData))
			{
				final Date postDate = new SimpleDateFormat("yyyy-MM-dd").parse(reportDto.getPostDate());

				if (fromDate1.equals(postDate) || toDate1.equals(postDate))
				{
					accountStatementReportList.add(reportDto);
				}
				else if ((postDate).after(fromDate1) && (postDate).before(toDate1))
				{
					accountStatementReportList.add(reportDto);
				}
			}
			else if (companyData.equalsIgnoreCase("both"))
			{
				final Date postDate = new SimpleDateFormat("yyyy-MM-dd").parse(reportDto.getPostDate());

				if (fromDate1.equals(postDate) || toDate1.equals(postDate))
				{
					accountStatementReportList.add(reportDto);
				}
				else if ((postDate).after(fromDate1) && (postDate).before(toDate1))
				{
					accountStatementReportList.add(reportDto);
				}
			}
		}
		dto.setAccountStatements(accountStatementReportList);
	}

	private void sortOrderBookingListByDate(final List<OrderBookingReportDto> list)
	{
		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		Collections.sort(list, (a1, a2) -> LocalDate.parse(a1.getSalesOrderDate(), formatter)
				.compareTo(LocalDate.parse(a2.getSalesOrderDate(), formatter)));
	}

	/**
	 * @return the customerReportsPopulator
	 */
	public CustomerReportsPopulator getCustomerReportsPopulator()
	{
		return customerReportsPopulator;
	}

	/**
	 * @param customerReportsPopulator
	 *           the customerReportsPopulator to set
	 */
	public void setCustomerReportsPopulator(final CustomerReportsPopulator customerReportsPopulator)
	{
		this.customerReportsPopulator = customerReportsPopulator;
	}

	/**
	 * @return the cpiReportsService
	 */
	public CpiReportsService getCpiReportsService()
	{
		return cpiReportsService;
	}

	/**
	 * @param cpiReportsService
	 *           the cpiReportsService to set
	 */
	public void setCpiReportsService(final CpiReportsService cpiReportsService)
	{
		this.cpiReportsService = cpiReportsService;
	}
}
