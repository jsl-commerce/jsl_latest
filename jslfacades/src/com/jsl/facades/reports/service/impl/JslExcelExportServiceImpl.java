/**
 *
 */
package com.jsl.facades.reports.service.impl;

import de.hybris.platform.sap.sapinvoiceaddon.model.SapB2BDocumentModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.jsl.configurablebundleservice.order.service.JslWorkbookHeaderService;
import com.jsl.configurablebundleservice.utils.JslBdcOrderDtoUtil;
import com.jsl.core.customersupport.dao.CustomerSupportDao;
import com.jsl.core.customersupport.dao.impl.CustomerSupportDaoImpl;
import com.jsl.core.model.CustomerComplaintModel;
import com.jsl.core.model.JslCustomerFeedbackModel;
import com.jsl.core.paymentdetailshistory.dao.impl.PaymentDetailsHistoryDaoImpl;
import com.jsl.facades.paymentdetailshistory.dto.PaymentDetailsHistoryDto;
import com.jsl.facades.reports.service.JslExcelExportService;
import com.jsl.facades.supportticket.dto.CustomerComplaintDto;
import com.jsl.facades.supportticket.dto.InvoiceDetailsDto;
import com.jsl.facades.supportticket.dto.JslCustomerFeedbackDto;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import reactor.util.CollectionUtils;



/**
 * @author s.bhambhani
 *
 */
public class JslExcelExportServiceImpl implements JslExcelExportService
{





	int orderHeaderRow = 0;
	int orderItemRow = 0;
	int columm = 0;

	private static final String EMPTY_STRING = "";
	private static final String SPLITTER = "-";
	private static final String AUCTION = "Auction";
	private static final Logger LOG = Logger.getLogger(JslExcelExportServiceImpl.class);




	JslWorkbookHeaderService jslWorkbookHeaderService;
	JslBdcOrderDtoUtil jslBdcOrderDtoUtil;


	@Autowired
	PaymentDetailsHistoryDaoImpl PaymentDetailsHistoryDao;

	@Autowired
	CustomerSupportDaoImpl CustomerSupportDao;

	CustomerSupportDao customerSupportDao;
	private Converter<JslCustomerFeedbackModel, JslCustomerFeedbackDto> customerFeedbackDtoConvertor;
	private Converter<SapB2BDocumentModel, PaymentDetailsHistoryDto> paymentDetailsHistoryConverter;
	private Converter<CustomerComplaintModel, CustomerComplaintDto> customerComplaintConverter;
	private List<InvoiceDetailsDto> customerItems;

	@Override
	public void exportInvoices(final HttpServletResponse response) throws Exception
	{
		ICsvBeanWriter csvWriter = null;
		addReponseHeader(response, "exportInvoices");
		csvWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);
		final List<PaymentDetailsHistoryDto> PaymentDetailsHistoryDto = paymentDetailsHistoryConverter
				.convertAll(PaymentDetailsHistoryDao.getallInvoices());



		final String[] header = new String[]
		{ "id", "invoiceNumber", "invoiceDate", "orderNumber", "orderDate", "deliveryNumber", "deliveryDate", "customerNumber",
				"taxNumber", "ourTaxNumber", "units", "documentNumber", "status", "paymentDate", "dueDate", "amount", "openAmount",
				"paymentMethod", "referenceNumber", "salesOffice", "salesGroup", "salesDistrict", "salesOrg", "invoiceCompanyCode",
				"kamName", "kamId" };

		csvWriter.writeHeader(header);

		final String[] fieldMapping = new String[]
		{ "id", "invoiceNumber", "invoiceDate", "orderNumber", "orderDate", "deliveryNumber", "deliveryDate", "customerNumber",
				"taxNumber", "ourTaxNumber", "units", "documentNumber", "status", "paymentDate", "dueDate", "amount", "openAmount",
				"paymentMethod", "referenceNumber", "salesOffice", "salesGroup", "salesDistrict", "salesOrg", "invoiceCompanyCode",
				"kamName", "kamId" };

		for (final PaymentDetailsHistoryDto PaymentDetailsHistory : PaymentDetailsHistoryDto)
		{
			csvWriter.write(PaymentDetailsHistory, fieldMapping);
		}
		csvWriter.close();
	}

	@Override
	public void customerFeedbackAnalysis(HttpServletResponse response) throws IOException
	{
		ICsvBeanWriter csvWriter = null;
		try
		{
			addReponseHeader(response, "FeedbackAnalysisDashboard.csv");
			csvWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);
			List<JslCustomerFeedbackDto> JslCustomerFeedbackDto = customerFeedbackDtoConvertor
					.convertAll(CustomerSupportDao.getAllCustomerFeedbackFormData());

			final String[] header = new String[]
			{ "complaintNumber", "customerNumber", "q1", "q2", "q3", "q4", "q5", "q6", "q7", "q8", "q9", "q10", "q11", "q12", "q13",
					"comments", "feedbackDate" };

			csvWriter.writeHeader(header);

			final String[] fieldMapping = new String[]
			{ "complaintNumber", "customerNumber", "q1", "q2", "q3", "q4", "q5", "q6", "q7", "q8", "q9", "q10", "q11", "q12", "q13",
					"comments", "feedbackDate" };

			for (final JslCustomerFeedbackDto JslCustomerFeedback : JslCustomerFeedbackDto)
			{
				csvWriter.write(JslCustomerFeedback, fieldMapping);
			}

		}
		catch (final Exception e)
		{
			LOG.error("Exception in file Download : " + e);
		}
		finally
		{
			if (csvWriter != null)
			{
				csvWriter.close();
			}
		}
	}



	@Override
	public boolean exportCustomerComplaintreport(final HttpServletResponse response, final File file) throws Exception
	{
		try
		{
			final List<CustomerComplaintModel> CustomerComplaint = customerSupportDao.getAllComplaintListForAdmin();
			if (CollectionUtils.isEmpty(CustomerComplaint))
			{
				LOG.error("NO Customer Complaint Found");
				return false;
			}

			else
			{
				final List<CustomerComplaintDto> CustomerComplaintList = customerComplaintConverter
						.convertAll(customerSupportDao.getAllComplaintListForAdmin());
				writeExcel(CustomerComplaintList, file);
				return true;
			}
		}
		catch (final Exception e)
		{
			LOG.error("Exception in JslExcelExportServiceImpl:", e);
			return false;
		}
	}




	public void writeExcel(final List<CustomerComplaintDto> CustomerComplaint, final File file)
			throws RowsExceededException, WriteException, IOException
	{
		this.orderHeaderRow = 0;
		this.orderItemRow = 0;
		this.columm = 0;
		final List<InvoiceDetailsDto> customerComplaintItems = null;

		final WritableWorkbook myFirstWbook = Workbook.createWorkbook(file);
		final WritableSheet excelSheet1 = myFirstWbook.createSheet("Header", 0);
		final WritableSheet excelSheet2 = myFirstWbook.createSheet("service", 1);
		final WritableSheet excelSheet3 = myFirstWbook.createSheet("material", 2);
		addCustomerComplaintExcelHeader(excelSheet1, orderHeaderRow, columm);
		addCustomerComplaintExcelService(excelSheet2, orderItemRow, columm);
		addCustomerComplaintExcelMaterial(excelSheet3, orderItemRow, columm);
		this.orderHeaderRow++;
		this.orderItemRow++;


		for (final CustomerComplaintDto customercomplaintDto : CustomerComplaint)
		{
			try
			{
				customercomplaintDto.setRefNum(String.valueOf(orderHeaderRow));
				addCustomerComplaintHeaderCell(excelSheet1, orderHeaderRow, 0, customercomplaintDto);
			}
			catch (final WriteException e)
			{
				e.printStackTrace();
			}
			if ("Material".equalsIgnoreCase(customercomplaintDto.getTypeOfComplaint()))
			{
				for (final InvoiceDetailsDto customercomplaintItem : customercomplaintDto.getInvoiceDetails())
				{
					try
					{
						addCustomerComplaintMaterialCell(excelSheet3, orderItemRow, 0, customercomplaintItem, customercomplaintDto);
					}
					catch (final WriteException e)
					{
						e.printStackTrace();
					}
					orderItemRow++;
				}
				orderHeaderRow++;
			}

			else if ("Service".equalsIgnoreCase(customercomplaintDto.getTypeOfComplaint()))
			{
				try
				{
					addCustomerComplaintServicesCell(excelSheet2, orderItemRow, 0, customercomplaintDto);
				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}
				orderItemRow++;

				orderHeaderRow++;
			}
		}

		myFirstWbook.write();
		myFirstWbook.close();
	}

	private void addCustomerComplaintExcelMaterial(final WritableSheet excelSheet, final int row, int columm)
			throws RowsExceededException, WriteException
	{

		final WritableCellFormat cFormat = new WritableCellFormat();
		final WritableFont font = new WritableFont(WritableFont.ARIAL, 14, WritableFont.BOLD);
		cFormat.setFont(font);
		excelSheet.addCell(new Label(columm, row, "ref", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Batch Number", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Grade", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Thickness", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Width", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Length", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Finish", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Edge", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Primary Defect", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "SO No", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Supply Qty", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Complaint Qty", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Plant", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Invoice Date", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "KAM Name", cFormat));


	}


	private void addCustomerComplaintExcelService(final WritableSheet excelSheet, final int row, int columm)
			throws RowsExceededException, WriteException
	{

		final WritableCellFormat cFormat = new WritableCellFormat();
		final WritableFont font = new WritableFont(WritableFont.ARIAL, 14, WritableFont.BOLD);
		cFormat.setFont(font);
		excelSheet.addCell(new Label(columm, row, "Ref", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "sub Category", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "complaint description", cFormat));

	}


	private void addCustomerComplaintExcelHeader(final WritableSheet excelSheet, final int row, int columm)
			throws RowsExceededException, WriteException
	{

		final WritableCellFormat cFormat = new WritableCellFormat();
		final WritableFont font = new WritableFont(WritableFont.ARIAL, 14, WritableFont.BOLD);
		cFormat.setFont(font);
		excelSheet.addCell(new Label(columm, row, "Ref", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Complaint Number", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Complaint Type", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Invoice Number", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Nature of Complaint", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Created Date ", cFormat));
		columm++;
		excelSheet.addCell(new Label(columm, row, "Status", cFormat));


	}


	private void addCustomerComplaintHeaderCell(final WritableSheet excelSheet1, final int orderHeaderRow, int columm,
			final CustomerComplaintDto customercomplaintDto) throws RowsExceededException, WriteException
	{

		excelSheet1.addCell(new Label(columm, orderHeaderRow, customercomplaintDto.getRefNum()));
		columm++;
		excelSheet1.addCell(new Label(columm, orderHeaderRow, customercomplaintDto.getComplaintNumber()));
		columm++;
		excelSheet1.addCell(new Label(columm, orderHeaderRow, customercomplaintDto.getTypeOfComplaint()));
		columm++;
		excelSheet1.addCell(new Label(columm, orderHeaderRow, customercomplaintDto.getInvoiceNumber()));
		columm++;
		excelSheet1.addCell(new Label(columm, orderHeaderRow, customercomplaintDto.getNatureOfComplaints()));
		columm++;
		excelSheet1.addCell(new Label(columm, orderHeaderRow, customercomplaintDto.getCreatedOn()));
		columm++;
		excelSheet1.addCell(new Label(columm, orderHeaderRow, customercomplaintDto.getStatus()));

	}


	private void addCustomerComplaintMaterialCell(final WritableSheet excelSheet2, final int orderItemRow2, int columm,
			final InvoiceDetailsDto customercomplaintItem, final CustomerComplaintDto customercomplaintDto)
			throws RowsExceededException, WriteException
	{
		customercomplaintItem.setRefNum(customercomplaintDto.getRefNum());
		excelSheet2.addCell(new Label(columm, orderItemRow2, customercomplaintItem.getRefNum()));
		columm++;
		excelSheet2.addCell(new Label(columm, orderItemRow2, customercomplaintItem.getBatchNumber()));
		columm++;
		excelSheet2.addCell(new Label(columm, orderItemRow2, customercomplaintItem.getGrade()));
		columm++;
		excelSheet2.addCell(new Label(columm, orderItemRow2, customercomplaintItem.getThickness()));
		columm++;
		excelSheet2.addCell(new Label(columm, orderItemRow2, customercomplaintItem.getWidth()));
		columm++;
		excelSheet2.addCell(new Label(columm, orderItemRow2, customercomplaintItem.getLength()));
		columm++;
		excelSheet2.addCell(new Label(columm, orderItemRow2, customercomplaintItem.getFinish()));
		columm++;
		excelSheet2.addCell(new Label(columm, orderItemRow2, customercomplaintItem.getEdge()));
		columm++;
		excelSheet2.addCell(new Label(columm, orderItemRow2, customercomplaintItem.getPrimaryDefect()));
		columm++;
		excelSheet2.addCell(new Label(columm, orderItemRow2, customercomplaintItem.getSoNumber()));
		columm++;
		excelSheet2.addCell(new Label(columm, orderItemRow2, customercomplaintItem.getSupplyQuantity()));
		columm++;
		excelSheet2.addCell(new Label(columm, orderItemRow2, customercomplaintItem.getComplaintQuantity()));
		columm++;
		excelSheet2.addCell(new Label(columm, orderItemRow2, customercomplaintItem.getPlant()));
		columm++;
		excelSheet2.addCell(new Label(columm, orderItemRow2, customercomplaintItem.getInvoiceDate()));
		columm++;
		excelSheet2.addCell(new Label(columm, orderItemRow2, customercomplaintItem.getKamName()));
	}

	private void addCustomerComplaintServicesCell(final WritableSheet excelSheet3, final int orderItemRow2, int columm,
			final CustomerComplaintDto customercomplaintDto) throws RowsExceededException, WriteException
	{
		excelSheet3.addCell(new Label(columm, orderItemRow2, customercomplaintDto.getRefNum()));
		columm++;
		excelSheet3.addCell(new Label(columm, orderItemRow2, customercomplaintDto.getNatureOfComplaints()));
		columm++;
		excelSheet3.addCell(new Label(columm, orderItemRow2, customercomplaintDto.getComplaintDescription()));


	}


	private void addReponseHeader(final HttpServletResponse response, final String csvFileName)
	{

		final String headerKey = "Content-Disposition";
		final String headerValue = String.format("attachment; filename=\"%s\"", csvFileName);
		response.setHeader(headerKey, headerValue);
		response.setContentType("text/comma-separated-values;charset=UTF-8");
	}



	/**
	 * @return the paymentDetailsHistoryConverter
	 */
	public Converter<SapB2BDocumentModel, PaymentDetailsHistoryDto> getPaymentDetailsHistoryConverter()
	{
		return paymentDetailsHistoryConverter;
	}



	/**
	 * @param paymentDetailsHistoryConverter
	 *           the paymentDetailsHistoryConverter to set
	 */
	public void setPaymentDetailsHistoryConverter(
			final Converter<SapB2BDocumentModel, PaymentDetailsHistoryDto> paymentDetailsHistoryConverter)
	{
		this.paymentDetailsHistoryConverter = paymentDetailsHistoryConverter;
	}



	/**
	 * @return the customerComplaintConverter
	 */
	public Converter<CustomerComplaintModel, CustomerComplaintDto> getCustomerComplaintConverter()
	{
		return customerComplaintConverter;
	}



	/**
	 * @param customerComplaintConverter
	 *           the customerComplaintConverter to set
	 */
	public void setCustomerComplaintConverter(
			final Converter<CustomerComplaintModel, CustomerComplaintDto> customerComplaintConverter)
	{
		this.customerComplaintConverter = customerComplaintConverter;
	}



	/**
	 * @return the paymentDetailsHistoryDao
	 */
	public PaymentDetailsHistoryDaoImpl getPaymentDetailsHistoryDao()
	{
		return PaymentDetailsHistoryDao;
	}



	/**
	 * @param paymentDetailsHistoryDao
	 *           the paymentDetailsHistoryDao to set
	 */
	public void setPaymentDetailsHistoryDao(final PaymentDetailsHistoryDaoImpl paymentDetailsHistoryDao)
	{
		PaymentDetailsHistoryDao = paymentDetailsHistoryDao;
	}



	/**
	 * @return the customerSupportDao
	 */
	public CustomerSupportDao getCustomerSupportDao()
	{
		return customerSupportDao;
	}



	/**
	 * @param customerSupportDao
	 *           the customerSupportDao to set
	 */
	public void setCustomerSupportDao(final CustomerSupportDao customerSupportDao)
	{
		this.customerSupportDao = customerSupportDao;
	}



	/**
	 * @return the customerItems
	 */
	public List<InvoiceDetailsDto> getCustomerItems()
	{
		return customerItems;
	}



	/**
	 * @param customerItems
	 *           the customerItems to set
	 */
	public void setCustomerItems(final List<InvoiceDetailsDto> customerItems)
	{
		this.customerItems = customerItems;
	}

	/**
	 * @return the customerFeedbackDtoConvertor
	 */
	public Converter<JslCustomerFeedbackModel, JslCustomerFeedbackDto> getCustomerFeedbackDtoConvertor()
	{
		return customerFeedbackDtoConvertor;
	}

	/**
	 * @param customerFeedbackDtoConvertor
	 *           the customerFeedbackDtoConvertor to set
	 */
	public void setCustomerFeedbackDtoConvertor(
			Converter<JslCustomerFeedbackModel, JslCustomerFeedbackDto> customerFeedbackDtoConvertor)
	{
		this.customerFeedbackDtoConvertor = customerFeedbackDtoConvertor;
	}





}
