/**
 *
 */
package com.jsl.facades.reports.service;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.SOAPException;

import com.jsl.core.dto.reports.customer.dashboard.CustomerDashboardDto;
import com.jsl.core.enums.Customer360Reports;
import com.jsl.facades.supportticket.dto.InvoiceWithMaterialDto;


/**
 * @author himanshu.sial
 *
 */
public interface JslReportServiceFacade
{

	public CustomerDashboardDto getCustomerDashboard() throws SOAPException;

	public void exportReport(final Customer360Reports report, String companyData, final HttpServletResponse response)
			throws IOException;

	public void exportReport(final Customer360Reports report, String toDate, String fromDate, String companyData,
			final HttpServletResponse response) throws IOException;

	public Object getReportsAsJson(final Customer360Reports report) throws SOAPException;

	public List<InvoiceWithMaterialDto> getInvoiceDetailsByInvoiceNumber(String invoiceNumber) throws SOAPException;
}
