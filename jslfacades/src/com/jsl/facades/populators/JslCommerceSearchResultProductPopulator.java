/**
 *
 */
package com.jsl.facades.populators;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


/**
 * @author manav.magoo
 *
 */
public class JslCommerceSearchResultProductPopulator implements Populator<SearchResultValueData, ProductData>
{

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(final SearchResultValueData source, final ProductData target) throws ConversionException
	{
		target.setPlant(this.<String> getValue(source, "plant"));
		target.setLocation(this.<String> getValue(source, "location"));
		target.setProductType(this.<String> getValue(source, "productType"));
		target.setQuality(this.<String> getValue(source, "quality"));
		target.setStandard(this.<Integer> getValue(source, "standard"));
		target.setSeries(this.<String> getValue(source, "series"));
		target.setGrade(this.<String> getValue(source, "grade"));
		target.setGradeGroup(this.<String> getValue(source, "gradeGroup"));
		target.setThickness(this.<Double> getValue(source, "thickness"));
		target.setWidth(this.<Double> getValue(source, "width"));
		target.setLength(this.<Double> getValue(source, "length"));
		target.setFinish(this.<String> getValue(source, "finish"));
		target.setMaterialNum(this.<String> getValue(source, "materialNum"));
		target.setEdgeCondition(this.<String> getValue(source, "edgeCondition"));
		target.setMinPartWt(this.<Double> getValue(source, "minPartWt"));
		target.setMaxPartWt(this.<Double> getValue(source, "maxPartWt"));
		target.setUsageIndicator(this.<String> getValue(source, "usageIndicator"));
		target.setIlp(this.<String> getValue(source, "ilp"));
		target.setPvc(this.<String> getValue(source, "pvc"));
		target.setTolerance(this.<Double> getValue(source, "tolerance"));
		target.setIncoterms(this.<String> getValue(source, "incoterms"));
		target.setCoilId(this.<String> getValue(source, "coilId"));
		target.setMarkingOnProd(this.<String> getValue(source, "markingOnProd"));
		target.setAgeingOfManufacturing(this.<Integer> getValue(source, "ageingOfManufacturing"));
		target.setMarkingOnSticker(this.<String> getValue(source, "markingOnSticker"));
		target.setTpi(this.<String> getValue(source, "tpi"));
		target.setQuantity(this.<Double> getValue(source, "quantity"));
		target.setWeight(this.<Double> getValue(source, "weight"));
		target.setNumOfSheets(this.<String> getValue(source, "numOfSheets"));
		target.setInspThirdParty(this.<String> getValue(source, "inspThirdParty"));
		target.setRoute(this.<String> getValue(source, "route"));
		target.setUtensilUsage(this.<String> getValue(source, "utensilUsage"));
		target.setDesiredDate(this.<String> getValue(source, "desiredDate"));
		target.setStoreLocator(this.<String> getValue(source, "storeLocator"));
		target.setStoreLocatorDesc(this.<String> getValue(source, "storeLocatorDesc"));
		target.setPhysicalStoreLocator(this.<String> getValue(source, "physicalStoreLocator"));
		target.setMinOrderQuantity(this.<Integer> getValue(source, "minOrderQuantity"));

	}

	protected <T> T getValue(final SearchResultValueData source, final String propertyName)
	{
		if (source.getValues() == null)
		{
			return null;
		}

		// DO NOT REMOVE the cast (T) below, while it should be unnecessary it is required by the javac compiler
		return (T) source.getValues().get(propertyName);
	}

}
