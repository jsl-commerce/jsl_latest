/**
 *
 */
package com.jsl.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.variants.model.VariantProductModel;

import com.jsl.core.model.AuctionProductModel;
import com.jsl.product.data.AuctionProductData;


/**
 * @author manav.magoo
 *
 */
public class AuctionProductPopulator implements Populator<ProductModel, AuctionProductData>
{
	@Override
	public void populate(final ProductModel source, final AuctionProductData target) throws ConversionException
	{
		final ProductModel baseProduct = getBaseProduct(source);

		if (baseProduct instanceof AuctionProductModel)
		{
			final AuctionProductModel auctionProductModel = (AuctionProductModel) baseProduct;
			target.setBundled(auctionProductModel.getBundled());

		}
	}

	protected ProductModel getBaseProduct(final ProductModel productModel)
	{
		ProductModel currentProduct = productModel;
		while (currentProduct instanceof VariantProductModel)
		{
			final VariantProductModel variant = (VariantProductModel) currentProduct;
			currentProduct = variant.getBaseProduct();
		}
		return currentProduct;
	}
}
