package com.jsl.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.jsl.core.dto.AuctionEventData;
import com.jsl.core.model.AuctionEventModel;


public class JslAuctionEventPopulators implements Populator<AuctionEventModel, AuctionEventData>
{

	@Override
	public void populate(final AuctionEventModel auctionEventModel, final AuctionEventData auctionEventData)
			throws ConversionException
	{
		auctionEventData.setAuctionId(auctionEventModel.getAuctionId());
		auctionEventData.setStartDate(auctionEventModel.getStartDate());
		auctionEventData.setEndDate(auctionEventModel.getEndDate());
		auctionEventData.setActive(auctionEventModel.getActive());
		auctionEventData.setAuctionGroup(auctionEventModel.getAuctionGroup().getName());
		auctionEventData.setAdditionalTime(auctionEventModel.getAdditionalTime());
		auctionEventData.setAuctionStatus(auctionEventModel.getAuctionStatus().getCode());
	}

}
