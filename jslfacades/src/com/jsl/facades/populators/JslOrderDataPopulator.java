/**
 * 
 */
package com.jsl.facades.populators;

import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import com.jsl.core.dto.WinBidData;

/**
 * @author himanshu.sial
 *
 */
public class JslOrderDataPopulator implements Populator<OrderModel, OrderData>
{

	private Converter<OrderModel, WinBidData> winBidConverter;
	
	@Override
	public void populate(final OrderModel source, final OrderData target)
	{
		target.setWinBidData(getWinBidConverter().convert(source));
	}

	/**
	 * @return the winBidConverter
	 */
	public Converter<OrderModel, WinBidData> getWinBidConverter()
	{
		return winBidConverter;
	}

	/**
	 * @param winBidConverter the winBidConverter to set
	 */
	public void setWinBidConverter(Converter<OrderModel, WinBidData> winBidConverter)
	{
		this.winBidConverter = winBidConverter;
	}
	
	
}
