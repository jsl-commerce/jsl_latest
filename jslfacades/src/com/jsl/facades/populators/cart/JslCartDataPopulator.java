/**
 *
 */
package com.jsl.facades.populators.cart;

import de.hybris.platform.commercefacades.order.converters.populator.CartPopulator;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.core.model.order.CartModel;


/**
 * @author s.bhambhani
 *
 */
public class JslCartDataPopulator<T extends CartData> extends CartPopulator<T>

{

	@Override
	public void populate(final CartModel source, final T target)
	{
		addCommon(source, target);
		addTotals(source, target);
		addEntries(source, target);
		addPromotions(source, target);
		addSavedCartData(source, target);
		addEntryGroups(source, target);
		addComments(source, target);
		target.setGuid(source.getGuid());
		target.setTotalUnitCount(calcTotalUnitCount(source));
		target.setWarehouseLocation(source.getWarehouse().getCode());


		if (source.getQuoteReference() != null)
		{
			target.setQuoteData(getQuoteConverter().convert(source.getQuoteReference()));
		}
	}
}
