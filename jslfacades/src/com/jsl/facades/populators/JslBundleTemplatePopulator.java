package com.jsl.facades.populators;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.WordUtils;
import org.apache.log4j.Logger;

import com.jsl.configurablebundleservice.service.JslBundleTemplateService;
import com.jsl.core.dto.AuctionEventData;
import com.jsl.core.dto.BundleTemplateData;
import com.jsl.core.enums.AuctionStatus;
import com.jsl.core.model.AuctionEventModel;

public class JslBundleTemplatePopulator implements Populator<BundleTemplateModel, BundleTemplateData>
{
	private static final Logger LOG = Logger.getLogger(JslBundleTemplatePopulator.class);

	private Converter<ProductModel, ProductData> productConverter;
	private Converter<AuctionEventModel, AuctionEventData> auctionConverter;
	private JslBundleTemplateService bundleTemplateService;

	@Override
	public void populate(final BundleTemplateModel bundleTemplateModel, final BundleTemplateData bundleTemplateData)
			throws ConversionException
	{
		final DecimalFormat df = new DecimalFormat("#.####");
		df.setRoundingMode(RoundingMode.CEILING);
		final List<ProductModel> products = bundleTemplateModel.getProducts();
		final List<ProductData> productDataList = new ArrayList<>(products.size());
		products.forEach(product -> {
			productDataList.add(getProductConverter().convert(product));
		});

		bundleTemplateData.setProductData(productDataList);
		bundleTemplateData.setBaseBidPrice(bundleTemplateModel.getBaseBidPrice());
		bundleTemplateData.setBundleId(bundleTemplateModel.getId());
		bundleTemplateData.setDescription(bundleTemplateModel.getDescription());
		bundleTemplateData.setBundleName(bundleTemplateModel.getName());
		bundleTemplateData.setQuantity(Double.valueOf(df.format(bundleTemplateModel.getQuantity())));
		bundleTemplateData.setAuctionEventData(getAuctionConverter().convert(bundleTemplateModel.getAuctionEvent()));
		if (bundleTemplateModel.getBidWonPrice() != null)
		{
			bundleTemplateData.setBidWonPrice(bundleTemplateModel.getBidWonPrice());
		}
		if (bundleTemplateModel.getBidEvent() != null)
		{
			bundleTemplateData.setBidEvent(bundleTemplateModel.getBidEvent());
			bundleTemplateData.setBidEventId(bundleTemplateModel.getBidEvent().getBidId());
		}
		if (bundleTemplateModel.getBaseBidPrice() != null)
		{
			bundleTemplateData.setMyBidEvent(String.valueOf(bundleTemplateModel.getBaseBidPrice()));
		}
		bundleTemplateData.setMyLastBid("0");
		setOrderStatusForBundle(bundleTemplateData);
	}

	private void setOrderStatusForBundle(final BundleTemplateData bundleTemplate)
	{
		try
		{
			final AuctionEventData auctionEvent = bundleTemplate.getAuctionEventData();
			if (auctionEvent.getAuctionStatus().equalsIgnoreCase(AuctionStatus.COMPLETED.getCode()))
			{
				final List<OrderModel> orderList = getBundleTemplateService()
						.getOrderByAuctionId(String.valueOf(auctionEvent.getAuctionId()));

				for (final OrderModel order : orderList)
				{
					if (order.getEntryGroups().get(0).getChildren().get(0).getExternalReferenceId()
							.equalsIgnoreCase(bundleTemplate.getBundleId()))
					{
						bundleTemplate.setOrderStatus(WordUtils.capitalize(order.getStatusDisplay().replaceAll("\\.", " ")));
					}
				}
			}
		}
		catch (final Exception e)
		{
			LOG.error("Exception in JslBundleTemplatePopulator.setOrderStatusForBundle()", e);
		}
	}

	public Converter<ProductModel, ProductData> getProductConverter()
	{
		return productConverter;
	}

	public void setProductConverter(final Converter<ProductModel, ProductData> productConverter)
	{
		this.productConverter = productConverter;
	}

	public Converter<AuctionEventModel, AuctionEventData> getAuctionConverter()
	{
		return auctionConverter;
	}

	public void setAuctionConverter(final Converter<AuctionEventModel, AuctionEventData> auctionConverter)
	{
		this.auctionConverter = auctionConverter;
	}

	public JslBundleTemplateService getBundleTemplateService()
	{
		return bundleTemplateService;
	}

	public void setBundleTemplateService(final JslBundleTemplateService bundleTemplateService)
	{
		this.bundleTemplateService = bundleTemplateService;
	}

}
