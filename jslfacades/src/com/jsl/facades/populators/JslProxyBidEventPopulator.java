package com.jsl.facades.populators;

import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import com.jsl.core.dto.AuctionEventData;
import com.jsl.core.dto.BundleTemplateData;
import com.jsl.core.dto.ProxyBidEventData;
import com.jsl.core.model.AuctionEventModel;
import com.jsl.jslConfigurableBundleService.model.ProxyBidRuleModel;


public class JslProxyBidEventPopulator implements Populator<ProxyBidRuleModel, ProxyBidEventData>
{

	private Converter<AuctionEventModel, AuctionEventData> auctionConverter;
	private Converter<BundleTemplateModel, BundleTemplateData> bundleTemplateConverter;


	@Override
	public void populate(final ProxyBidRuleModel proxyBidEvent, final ProxyBidEventData proxyBidEventData)
			throws ConversionException
	{
		final AuctionEventData auctionData = getAuctionConverter().convert(proxyBidEvent.getAuctionEvent());
		final BundleTemplateData bundleTemplateData = getBundleTemplateConverter().convert(proxyBidEvent.getBundleTemplate());
		proxyBidEventData.setAuctionEventData(auctionData);
		proxyBidEventData.setBundleTemplateData(bundleTemplateData);
		proxyBidEventData.setProxyBidId(proxyBidEvent.getProxyBidId());
		proxyBidEventData.setCreatedBy(proxyBidEvent.getCreatedBy().getUid());
		proxyBidEventData.setCreatedOn(proxyBidEvent.getCreatedOn());
		proxyBidEventData.setIncrementByBidPrice(proxyBidEvent.getIncrementByBidPrice());
		proxyBidEventData.setMinBidPrice(proxyBidEvent.getMinBidPrice());
		proxyBidEventData.setMaxProxyBidPrice(proxyBidEvent.getMaxProxyBidPrice());
	}

	public Converter<AuctionEventModel, AuctionEventData> getAuctionConverter()
	{
		return auctionConverter;
	}

	public void setAuctionConverter(final Converter<AuctionEventModel, AuctionEventData> auctionConverter)
	{
		this.auctionConverter = auctionConverter;
	}

	public Converter<BundleTemplateModel, BundleTemplateData> getBundleTemplateConverter()
	{
		return bundleTemplateConverter;
	}

	public void setBundleTemplateConverter(final Converter<BundleTemplateModel, BundleTemplateData> bundleTemplateConverter)
	{
		this.bundleTemplateConverter = bundleTemplateConverter;
	}

}
