/**
 *
 */
package com.jsl.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.jsl.core.model.CustomerComplaintModel;
import com.jsl.core.model.InvoiceDetailsModel;
import com.jsl.facades.supportticket.dto.CustomerComplaintDto;
import com.jsl.facades.supportticket.dto.InvoiceDetailsDto;


/**
 * @author kiranraj S
 *
 */
public class CustomerComplaintDtoPopulator implements Populator<CustomerComplaintModel, CustomerComplaintDto>
{

	private static final Logger LOG = Logger.getLogger(CustomerComplaintDtoPopulator.class);

	@Autowired
	private Converter<InvoiceDetailsModel, InvoiceDetailsDto> invoiceDtoConvertor;

	@Override
	public void populate(final CustomerComplaintModel complaintModel, final CustomerComplaintDto complaintDto)
			throws ConversionException
	{
		complaintDto.setComplaintNumber(complaintModel.getComplaintNumber());
		complaintDto.setNatureOfComplaints(complaintModel.getNatureOfComplaints());
		complaintDto.setComplaintDescription(complaintModel.getComplaintDescription());
		complaintDto.setInvoiceDetails(getInvoiceDetails((List<InvoiceDetailsModel>) complaintModel.getComplaintInvoice()));
		complaintDto.setContactNumber(complaintModel.getContactNumber());
		complaintDto.setContactPersonName(complaintModel.getContactPersonName());
		complaintDto.setMaterialLocation(complaintModel.getMaterialLocation());
		complaintDto.setPresentMaterialCondition(complaintModel.getPresentMaterialCondition());
		complaintDto.setTypeOfComplaint(complaintModel.getTypeOfComplaint());
		complaintDto.setInvoiceNumber(complaintModel.getInvoiceNumber());
		complaintDto.setStatus(complaintModel.getStatus());
		complaintDto.setSalesOffice(complaintModel.getSalesOffice());
		complaintDto.setSalesDistrict(complaintModel.getSalesDistrict());
		complaintDto.setSalesGroup(complaintModel.getSalesGroup());
		complaintDto.setInvoiceCompanyCode(complaintModel.getInvoiceCompanyCode());
		complaintDto.setCreatedOn(complaintModel.getCreationtime().toString());
		complaintDto.setCustomerFeedBackComment(complaintModel.getCustomerFeedBackComment());
		complaintDto.setCustomerFeedBackRating(String.valueOf(complaintModel.getCustomerFeedBackRating()));
		complaintDto.setKamName(complaintModel.getKamName());
		complaintDto.setSalesOrg(complaintModel.getSalesOrg());
		complaintDto.setKamId(complaintModel.getKamId());
		complaintDto.setQualityPersonName(complaintModel.getQualityPersonName());
		complaintDto.setSettlementRemarks(complaintModel.getSettlementRemarks());
		complaintDto.setFormattedCreatedOn(formateDate(complaintModel.getCreationtime().toString()));
		complaintDto.setFeedbackSubmitted(complaintModel.getFeedbackSubmitted());
		complaintDto.setInternalStatus(complaintModel.getInternalStatus());
		complaintDto.setCustomerSatisfaction(complaintModel.getCustomerSatisfaction());
		complaintDto.setSatisfactionSubmitted(complaintModel.getSatisfactionSubmitted());
	}

	private List<InvoiceDetailsDto> getInvoiceDetails(final List<InvoiceDetailsModel> invoiceModelList)
	{
		final List<InvoiceDetailsDto> invoiceDtoList = new ArrayList<>();
		try
		{
			if (null != invoiceModelList && CollectionUtils.isNotEmpty(invoiceModelList))
			{
				invoiceModelList.forEach(invoiceDetails -> {
					invoiceDtoList.add(getInvoiceDtoConvertor().convert(invoiceDetails));
				});
			}
		}
		catch (final Exception e)
		{
			LOG.error("Exception in CustomerComplaintDtoPopulator.getInvoiceDetails() while converting model to dto: ", e);
		}
		return invoiceDtoList;
	}

	private String formateDate(final String date)
	{
		final SimpleDateFormat sdf = new SimpleDateFormat("EE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH);
		final SimpleDateFormat print = new SimpleDateFormat("MM/dd/yyyy");
		Date parsedDate = null;
		String formattedDate = "";
		try
		{
			parsedDate = sdf.parse(date);
			formattedDate = print.format(parsedDate);
		}
		catch (final Exception e)
		{
		}
		return formattedDate;
	}

	/**
	 * @return the invoiceDtoConvertor
	 */
	public Converter<InvoiceDetailsModel, InvoiceDetailsDto> getInvoiceDtoConvertor()
	{
		return invoiceDtoConvertor;
	}

}
