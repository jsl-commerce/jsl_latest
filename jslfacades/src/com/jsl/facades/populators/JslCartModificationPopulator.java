/**
 *
 */
package com.jsl.facades.populators;

import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


/**
 * @author manav.magoo
 *
 */
public class JslCartModificationPopulator implements Populator<CommerceCartModification, CartModificationData>
{
	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(final CommerceCartModification source, final CartModificationData target) throws ConversionException
	{
		target.setAllowedQuantity(source.getAllowedQuantity());
		target.setMinQuanityFailed(source.getMinQuanityFailed());
		target.setMouExceeded(source.getMouExceeded());
		target.setProductTypeFailed(source.isProductTypeFailed());
	}
}
