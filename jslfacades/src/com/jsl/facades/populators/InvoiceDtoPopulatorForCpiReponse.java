/**
 *
 */
package com.jsl.facades.populators;

import de.hybris.platform.sap.sapinvoiceaddon.model.SapB2BDocumentModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;

import com.jsl.cpi.invoice.batch.dto.ZBATCHHYBRIS;
import com.jsl.cpi.invoice.batch.dto.ZFMINVBATCHHYBRISResponse;
import com.jsl.cpi.invoice.batch.dto.ZHYBBATCHCHAR;
import com.jsl.facades.invoice.material.dto.JslInvoiceMaterialDto;
import com.jsl.facades.supportticket.dto.InvoiceWithMaterialDto;


/**
 * @author Kiranraj S.
 *
 */
public class InvoiceDtoPopulatorForCpiReponse
{
	private static final Logger LOG = Logger.getLogger(InvoiceDtoPopulatorForCpiReponse.class);
	private static final String COMMA = ",";
	private static final String UNIT_MM = "mm";
	private static final String EMPTY_STRING = "";

	public List<InvoiceWithMaterialDto> populate(final ZFMINVBATCHHYBRISResponse invoiceDetails,
			final List<SapB2BDocumentModel> invoiceList)
	{
		final List<InvoiceWithMaterialDto> invoiceDetailsList = new ArrayList<>();
		final List<ZHYBBATCHCHAR> itCharField = invoiceDetails.getITCHARFIELD().getItem();
		final Map<String, List<ZHYBBATCHCHAR>> groupedItem = groupBatch(itCharField);
		final Set<String> keySet = groupedItem.keySet();
		for (final String key : keySet)
		{
			final List<JslInvoiceMaterialDto> materials = new ArrayList<JslInvoiceMaterialDto>();
			final JslInvoiceMaterialDto materialDto = new JslInvoiceMaterialDto();
			final InvoiceWithMaterialDto invoiceDetailsDto = new InvoiceWithMaterialDto();
			final List<ZHYBBATCHCHAR> itemDetails = groupedItem.get(key);
			for (final ZHYBBATCHCHAR item : itemDetails)
			{
				final List<ZBATCHHYBRIS> itemData = getInvoiceDetails(invoiceDetails.getITOUTPUT().getItem(), item.getCHARG());
				invoiceDetailsDto.setInvoiceNumber(item.getVBELN());
				if (CollectionUtils.isNotEmpty(invoiceList))
				{
					invoiceDetailsDto.setInvoiceCompanyCode(invoiceList.iterator().next().getInvoiceCompanyCode());
					invoiceDetailsDto.setSalesOffice(invoiceList.iterator().next().getSalesOffice());
					invoiceDetailsDto.setSalesGroup(invoiceList.iterator().next().getSalesGroup());
					invoiceDetailsDto.setInvoiceDate(invoiceList.iterator().next().getInvoiceDate());
					invoiceDetailsDto.setSalesOrg(invoiceList.iterator().next().getSalesOrg());
					invoiceDetailsDto.setInvoiceKamName(invoiceList.iterator().next().getInvoiceKamName());
					invoiceDetailsDto.setInvoiceKamId(invoiceList.iterator().next().getInvoiceKamId());
					materialDto.setKamName(invoiceList.iterator().next().getInvoiceKamName());
					materialDto.setKamId(invoiceList.iterator().next().getInvoiceKamId());
					materialDto.setProductCode(item.getMATNR());
					if (invoiceList.iterator().next().getInvoiceDate() != null)
					{
						materialDto.setInvoiceDate(String.valueOf(invoiceList.iterator().next().getInvoiceDate()));
					}
					else
					{
						materialDto.setInvoiceDate(String.valueOf(invoiceList.iterator().next().getDate()));
					}
				}
				if (CollectionUtils.isNotEmpty(itemData))
				{
					invoiceDetailsDto.setCustomerNumber(itemData.iterator().next().getKUNNR());
					materialDto.setPlant(itemData.iterator().next().getWERKS());
					materialDto.setSupplyQuantity(String.valueOf(itemData.iterator().next().getLFIMG()));
					materialDto.setSoNumber(itemData.iterator().next().getVBELV());
				}
				materialDto.setBatchNumber(item.getCHARG());
				populateMaterialCharvalue(materialDto, item.getATNAM(), item.getATWTB());
			}
			materials.add(materialDto);
			invoiceDetailsDto.setMaterial(materials);
			invoiceDetailsList.add(invoiceDetailsDto);
		}
		return invoiceDetailsList;
	}

	private Map<String, List<ZHYBBATCHCHAR>> groupBatch(final List<ZHYBBATCHCHAR> itemDetails)
	{
		final Map<String, List<ZHYBBATCHCHAR>> groupedItem = itemDetails.stream().collect(Collectors.groupingBy(i -> i.getCHARG()));
		return groupedItem;
	}

	private List<ZBATCHHYBRIS> getInvoiceDetails(final List<ZBATCHHYBRIS> itemDetails, final String charg)
	{
		final List<ZBATCHHYBRIS> itemList =
				//Create a Stream from the itemDetails
				itemDetails.stream().
				//filter the element to select only those which match with item
						filter(p -> p.getCHARG().equalsIgnoreCase(charg)).
						//put those filtered elements into a new List.
						collect(Collectors.toCollection(() -> new ArrayList<ZBATCHHYBRIS>()));
		return itemList;
	}

	private void populateMaterialCharvalue(final JslInvoiceMaterialDto material, final String charName, final String CharValue)
	{
		switch (charName)
		{
			case "WIDTH":
				material.setWidth(CharValue);
				break;
			case "LENGTH":
				material.setLength(CharValue);
				break;
			case "THICK":
				material.setThickness(CharValue);
				break;
			case "STANDARD":
				material.setStandard(CharValue);
				break;
			case "GRADE":
				material.setGrade(CharValue);
				break;
			case "GR_GROUP":
				material.setGradeGroup(CharValue);
				break;
			case "EDGE_CON":
				material.setEdge(CharValue);
				break;
			case "SERIES":
				material.setSeries(CharValue);
				break;
			case "FINISH":
				material.setFinish(CharValue);
				break;
			case "PROD_TYP":
				material.setProductType(CharValue);
				break;
			case "GROSS_WT":
				break;
			case "YIELD":
				break;
			case "NO_SHEET":
				break;
			case "CUST_SEG":
				break;
			case "ILP":
				break;
			case "SURF_DEC":
				break;
			case "LOCATION":
				break;
			case "QUALITY":
				break;
			case "PVC":
				break;
			case "EDGE_CON_Q":
				break;
		}
	}
}
