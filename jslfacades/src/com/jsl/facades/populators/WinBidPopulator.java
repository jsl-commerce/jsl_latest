/**
 *
 */
package com.jsl.facades.populators;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.configurablebundleservices.bundle.BundleTemplateService;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.order.EntryGroup;

import com.jsl.core.dto.WinBidData;
import com.jsl.core.util.JslB2bUnitUtil;


/**
 * @author himanshu.sial
 *
 */
public class WinBidPopulator implements Populator<OrderModel, WinBidData>
{
	BundleTemplateService bundleTemplateService;



	private boolean checkValidBundleEntry(final EntryGroup entryGroup)
	{
		if (!entryGroup.getChildren().isEmpty())
		{
			final BundleTemplateModel bundle = getBundle(entryGroup.getChildren().get(0).getExternalReferenceId());
			if (bundle != null)
			{
				return true;
			}
		}
		return false;
	}

	/**
	 *
	 */
	private BundleTemplateModel getBundle(final String bundleTemplateId)
	{
		return getBundleTemplateService().getBundleTemplateForCode(bundleTemplateId);
	}


	@Override
	public void populate(final OrderModel source, final WinBidData target)
	{
		if (!source.getEntryGroups().isEmpty())
		{

			final EntryGroup entryGroup = source.getEntryGroups().get(0);
			if (checkValidBundleEntry(entryGroup))
			{
				final BundleTemplateModel bundleTemplate = getBundle(entryGroup.getChildren().get(0).getExternalReferenceId());

				target.setBundleId(bundleTemplate.getId());
				target.setH1Price(bundleTemplate.getBidWonPrice());
				target.setBundleName(bundleTemplate.getName());
				target.setMinBidPrice(bundleTemplate.getBaseBidPrice());
				//to-do
				target.setCustomerName(getCompany(source));
			}
		}
	}

	private String getCompany(final OrderModel order)
	{
		final B2BCustomerModel user = ((B2BCustomerModel) order.getUser());
		final B2BUnitModel parentb2bUnit = JslB2bUnitUtil.getParentB2bUnit(user);
		if (parentb2bUnit != null)
		{
			return parentb2bUnit.getDisplayName();
		}
		else
		{
			return order.getUser().getUid();
		}
	}

	/**
	 * @return the bundleTemplateService
	 */
	public BundleTemplateService getBundleTemplateService()
	{
		return bundleTemplateService;
	}

	/**
	 * @param bundleTemplateService
	 *           the bundleTemplateService to set
	 */
	public void setBundleTemplateService(final BundleTemplateService bundleTemplateService)
	{
		this.bundleTemplateService = bundleTemplateService;
	}


}
