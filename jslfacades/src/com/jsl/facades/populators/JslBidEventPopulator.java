package com.jsl.facades.populators;

import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import com.jsl.core.dto.AuctionEventData;
import com.jsl.core.dto.BidEventData;
import com.jsl.core.dto.BundleTemplateData;
import com.jsl.core.model.AuctionEventModel;
import com.jsl.jslConfigurableBundleService.model.BidEventModel;


public class JslBidEventPopulator implements Populator<BidEventModel, BidEventData>
{

	private Converter<BundleTemplateModel, BundleTemplateData> bundleTemplateConverter;
	private Converter<AuctionEventModel, AuctionEventData> auctionConverter;

	@Override
	public void populate(final BidEventModel bidEventModel, final BidEventData bidEventData) throws ConversionException
	{
		bidEventData.setAuctionEventData(getAuctionConverter().convert(bidEventModel.getAuctionEvent()));
		bidEventData.setBundleTemplateData(getBundleTemplateConverter().convert(bidEventModel.getBundleTemplate()));
		bidEventData.setBidId(bidEventModel.getBidId());
		bidEventData.setBidPrice(bidEventModel.getBidPrice());
		bidEventData.setCreatedBy(bidEventModel.getCreatedBy().getUid());
		bidEventData.setCreatedOn(bidEventModel.getCreatedOn());
	}

	public Converter<BundleTemplateModel, BundleTemplateData> getBundleTemplateConverter()
	{
		return bundleTemplateConverter;
	}

	public void setBundleTemplateConverter(final Converter<BundleTemplateModel, BundleTemplateData> bundleTemplateConverter)
	{
		this.bundleTemplateConverter = bundleTemplateConverter;
	}

	public Converter<AuctionEventModel, AuctionEventData> getAuctionConverter()
	{
		return auctionConverter;
	}

	public void setAuctionConverter(final Converter<AuctionEventModel, AuctionEventData> auctionConverter)
	{
		this.auctionConverter = auctionConverter;
	}

}
