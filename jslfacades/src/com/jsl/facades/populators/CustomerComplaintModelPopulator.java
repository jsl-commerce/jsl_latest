/**
 *
 */
package com.jsl.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.jsl.core.model.CustomerComplaintModel;
import com.jsl.core.model.InvoiceDetailsModel;
import com.jsl.facades.supportticket.dto.CustomerComplaintDto;
import com.jsl.facades.supportticket.dto.InvoiceDetailsDto;


/**
 * @author Kiranraj S.
 *
 */
public class CustomerComplaintModelPopulator implements Populator<CustomerComplaintDto, CustomerComplaintModel>
{

	private static final Logger LOG = Logger.getLogger(CustomerComplaintModelPopulator.class);

	@Autowired
	private ModelService modelService;

	@Autowired
	private Converter<InvoiceDetailsDto, InvoiceDetailsModel> invoiceModelConvertor;

	@Override
	public void populate(final CustomerComplaintDto customerComplaintDto, final CustomerComplaintModel customerComplaintModel)
			throws ConversionException
	{
		customerComplaintModel.setNatureOfComplaints(customerComplaintDto.getNatureOfComplaints());
		customerComplaintModel.setComplaintDescription(customerComplaintDto.getComplaintDescription());
		customerComplaintModel.setComplaintInvoice(getInvoiceDetails(customerComplaintDto.getInvoiceDetails()));
		customerComplaintModel.setContactNumber(customerComplaintDto.getContactNumber());
		customerComplaintModel.setContactPersonName(customerComplaintDto.getContactPersonName());
		customerComplaintModel.setMaterialLocation(customerComplaintDto.getMaterialLocation());
		customerComplaintModel.setPresentMaterialCondition(customerComplaintDto.getPresentMaterialCondition());
		customerComplaintModel.setTypeOfComplaint(customerComplaintDto.getTypeOfComplaint());
		customerComplaintModel.setInvoiceNumber(customerComplaintDto.getInvoiceNumber());
		customerComplaintModel.setSalesOffice(customerComplaintDto.getSalesOffice());
		customerComplaintModel.setSalesDistrict(customerComplaintDto.getSalesDistrict());
		customerComplaintModel.setSalesGroup(customerComplaintDto.getSalesGroup());
		customerComplaintModel.setInvoiceCompanyCode(customerComplaintDto.getInvoiceCompanyCode());
		customerComplaintModel.setSalesOrg(customerComplaintDto.getSalesOrg());
		customerComplaintModel.setKamId(customerComplaintDto.getKamId());
		customerComplaintModel.setKamName(customerComplaintDto.getKamName());
	}

	private Collection<InvoiceDetailsModel> getInvoiceDetails(final List<InvoiceDetailsDto> invoiceList)
	{
		final List<InvoiceDetailsModel> invoiceModelList = new ArrayList<>();
		try
		{
			if (null != invoiceList && CollectionUtils.isNotEmpty(invoiceList))
			{
				invoiceList.forEach(invoiceDetailsDto -> {
					invoiceModelList.add(
							getInvoiceModelConvertor().convert(invoiceDetailsDto, getModelService().create(InvoiceDetailsModel.class)));
				});
			}
		}
		catch (final Exception e)
		{
			LOG.error("Exception in CustomerComplaintModelPopulator.getInvoiceDetails() while converting invoicedto to model: ", e);
		}
		return invoiceModelList;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @return the invoiceModelConvertor
	 */
	public Converter<InvoiceDetailsDto, InvoiceDetailsModel> getInvoiceModelConvertor()
	{
		return invoiceModelConvertor;
	}

}
