/**
 *
 */
package com.jsl.facades.populators.customer360.reports;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.jsl.core.dto.reports.AccountStatementDto;
import com.jsl.core.dto.reports.AccountStatementReportDto;
import com.jsl.core.dto.reports.OrderBookingReportDto;
import com.jsl.core.dto.reports.PopPlantReportDto;
import com.jsl.core.dto.reports.PopYardReportDto;
import com.jsl.core.dto.reports.SalesBillingDetailsReportDto;
import com.jsl.core.dto.reports.ShipmentDetailsDto;
import com.jsl.core.dto.reports.ShipmentDetailsReportDto;
import com.jsl.core.dto.reports.customer.dashboard.AccountSummaryDto;
import com.jsl.core.dto.reports.customer.dashboard.BillingSummaryDto;
import com.jsl.core.dto.reports.customer.dashboard.CustomerDashboardDto;
import com.jsl.core.dto.reports.customer.dashboard.OrderBookingDto;
import com.jsl.core.dto.reports.customer.dashboard.PopPlantDto;
import com.jsl.core.dto.reports.customer.dashboard.PopYardDto;
import com.jsl.cpi.report.dto.accountstatement.ZFI137HYBRISResponse;
import com.jsl.cpi.report.dto.customer.dashboard.ZFITTACCSUMM;
import com.jsl.cpi.report.dto.customer.dashboard.ZSDCUSTDASHBOARDHYBRISResponse;
import com.jsl.cpi.report.dto.customer.dashboard.ZSDTTBILLSUMM;
import com.jsl.cpi.report.dto.customer.dashboard.ZSDTTPOPYARD;
import com.jsl.cpi.report.dto.orderbooking.ZSD35HYBRISResponse;
import com.jsl.cpi.report.dto.pop.plant.ZPOPHYBRISResponse;
import com.jsl.cpi.report.dto.pop.yard.ZSD35CHYBRISResponse;
import com.jsl.cpi.report.dto.sales.ZSD34HYBRISResponse;
import com.jsl.cpi.report.dto.vehicle.tracking.ZSDTRACKDELHYBRISResponse;


/**
 * @author s.bhambhani
 *
 */
public class CustomerReportsPopulator
{

	private UserService userService;

	private static final String DEFAULT_B2BUNIT_CODE = "default.b2b.unit.codes";

	private static final String CUSTOMER_ID = "customerId";
	private static final String CUSTOMER_NAME = "customerName";

	public List<PopPlantReportDto> getPopPlantReportReponseDto(final ZPOPHYBRISResponse response)
	{
		final List<PopPlantReportDto> convertedResponseList = new ArrayList<PopPlantReportDto>();

		//TO-DO - get from response and set in list dto
		response.getZPOPHYBRIS().getItem().forEach(pop -> {
			final PopPlantReportDto dto = new PopPlantReportDto();
			dto.setSalesOrderNumber(pop.getVBELN());
			dto.setSalesOrderDate(pop.getAUDAT());
			dto.setItem(String.valueOf(Integer.parseInt(pop.getPOSNR())));
			dto.setGrade(pop.getGRADE());
			dto.setThickness(pop.getTHICKNESS());
			dto.setWidth(pop.getWIDTH());
			dto.setLength(pop.getLENGTH());
			dto.setEdgeCondition(pop.getEDGECON());
			dto.setFinish(pop.getFINISH());
			dto.setQuality(pop.getQUALITY());
			dto.setBasicPrice(String.valueOf(pop.getKBETR()));
			dto.setOrderItemQuantity(String.valueOf(pop.getKLMENG()));
			dto.setDispatchQuantity(String.valueOf(pop.getLFIMG()));
			dto.setMarketBalance(String.valueOf(pop.getOPENBAL()));
			dto.setOrderAcceptanceNumber(pop.getBSTKD());
			dto.setCustomerRequiredDeliveryDate(pop.getCUSDATE());
			dto.setOrderAmount(String.valueOf(pop.getCLABS())); //Order amount is allocated quantity.
			dto.setProductType(pop.getPRODUCTTYPE());
			dto.setHoldStatus(StringUtils.isEmpty(pop.getBEZEI()) ? pop.getBEZEI() : pop.getBEZEI().trim());
			dto.setPartWtMin(StringUtils.isEmpty(pop.getPARTWTMIN()) ? pop.getPARTWTMIN() : pop.getPARTWTMIN().trim());
			dto.setPartWtMax(StringUtils.isEmpty(pop.getPARTWTMAX()) ? pop.getPARTWTMAX() : pop.getPARTWTMAX().trim());
			convertedResponseList.add(dto);
		});

		return convertedResponseList;

	}

	//To-DO
	public AccountStatementDto getAccountStatementResponseDto(final ZFI137HYBRISResponse response)
	{

		final AccountStatementDto convertedResponseDto = new AccountStatementDto();
		final List<AccountStatementReportDto> convertedResponseList = new ArrayList<AccountStatementReportDto>();

		//TO-DO - get from response and set in list dto
		response.getZFI37TABLE().getItem().forEach(pop -> {
			final AccountStatementReportDto dto = new AccountStatementReportDto();
			dto.setAmount(String.valueOf(pop.getWRBTR()));
			dto.setCreditAmount(String.valueOf(pop.getCREDITAMT()));
			dto.setCurrency(pop.getWAERS());
			dto.setDebitAmount(String.valueOf(pop.getDEBITAMT()));
			dto.setDocumentDescription(pop.getLTEXT());
			dto.setDocumentNumber(pop.getBELNR());
			dto.setDueDate(pop.getDUEDT());
			dto.setItemDescription(pop.getSGTXT());
			dto.setPostDate(pop.getBLDAT());
			dto.setReference(pop.getXBLNR2());
			dto.setRefInstNumber(pop.getXBLNR());
			dto.setSpl(pop.getUMSKZ());
			if ("JS03".equalsIgnoreCase(pop.getBUKRS()))
			{
				dto.setCompanycode("JSL");
			}
			else if ("JS01".equalsIgnoreCase(pop.getBUKRS()))
			{
				dto.setCompanycode("JSHL");
			}
			convertedResponseList.add(dto);
		});

		convertedResponseDto.setCustomerId(getCustomerInfo().get(CUSTOMER_ID));
		convertedResponseDto.setCustomerName(getCustomerInfo().get(CUSTOMER_NAME));
		convertedResponseDto.setAccountStatements(convertedResponseList);
		return convertedResponseDto;

	}

	public ShipmentDetailsDto getShipmentDetailsResponseDto(final ZSDTRACKDELHYBRISResponse response)
	{

		final ShipmentDetailsDto convertedResponseDto = new ShipmentDetailsDto();
		final List<ShipmentDetailsReportDto> convertedResponseList = new ArrayList<ShipmentDetailsReportDto>();

		//TO-DO - get from response and set in list dto
		response.getITVEHTACK().getItem().forEach(pop -> {
			final ShipmentDetailsReportDto dto = new ShipmentDetailsReportDto();
			dto.setCompanyName(pop.getCOMPANY());
			dto.setDispatchCity(pop.getDISPATCHCITY());
			dto.setDeviceId(pop.getDEVICEID());
			dto.setTrackingUrl(pop.getURLTRACKING());
			dto.setDocumentNumber(pop.getVBELN());
			dto.setLeadDay(pop.getZLDAYS());
			dto.setExitDate(pop.getEXITDATE());
			dto.setExitTime(String.valueOf(pop.getEXITTIME()));
			dto.setTransporationZone(pop.getLZONE());
			dto.setTripStaus(pop.getTRIPSTATUS());
			dto.setTrackingInd(pop.getTRACKINGIND());
			dto.setTripStatusDate(String.valueOf(pop.getTRIPSTATUSDATE()));
			dto.setTripStatusTime(pop.getTRIPSTATUSTIME());
			dto.setTrackingNo(pop.getTRACKINGNO());
			convertedResponseList.add(dto);
		});

		convertedResponseDto.setCustomerId(getCustomerInfo().get(CUSTOMER_ID));
		convertedResponseDto.setShipmentDetails(convertedResponseList);
		return convertedResponseDto;

	}

	public List<PopYardReportDto> getPopYardReportReponseDto(final ZSD35CHYBRISResponse response)
	{
		final List<PopYardReportDto> convertedResponseList = new ArrayList<PopYardReportDto>();

		//TO-DO - get from response and set in list dto
		response.getZSD35CHYBRIS().getItem().forEach(pop -> {
			final PopYardReportDto dto = new PopYardReportDto();
			dto.setSalesOrderNumber(pop.getVBELN());
			dto.setSalesOrderDate(pop.getAUDAT());
			dto.setItem(String.valueOf(Integer.parseInt(pop.getPOSNR())));
			dto.setGrade(pop.getGRADE());
			dto.setThickness(pop.getTHICK());
			dto.setWidth(pop.getWIDTH());
			dto.setLength(pop.getLENGTH());
			dto.setEdgeCondition(pop.getEDGECON());
			dto.setFinish(pop.getFINISH());
			dto.setQuality(pop.getQUALITY());
			dto.setBasicPrice(String.valueOf(pop.getKBETR()));
			dto.setOrderItemQuantity(String.valueOf(pop.getKLMENG()));
			dto.setDispatchQuantity(String.valueOf(pop.getLFIMG()));
			dto.setMarketBalance(String.valueOf(pop.getOPENBAL()));
			dto.setOrderAcceptanceNumber(pop.getBSTKD());
			dto.setCustomerRequiredDeliveryDate(pop.getCUSDATE());
			dto.setOrderAmount(String.valueOf(pop.getCLABS())); //Order amount is allocated quantity
			dto.setProductType(pop.getPRODTYP());

			dto.setPartWtMin(StringUtils.isEmpty(pop.getPARTWTMIN()) ? pop.getPARTWTMIN() : pop.getPARTWTMIN().trim());
			dto.setPartWtMax(StringUtils.isEmpty(pop.getPARTWTMAX()) ? pop.getPARTWTMAX() : pop.getPARTWTMAX().trim());
			convertedResponseList.add(dto);
		});

		return convertedResponseList;
	}


	public List<OrderBookingReportDto> getOrderBookingReportReponseDto(final ZSD35HYBRISResponse response)
	{
		final List<OrderBookingReportDto> convertedResponseList = new ArrayList<OrderBookingReportDto>();
		response.getZSD35HYBRIS().getItem().forEach(pop -> {
			final OrderBookingReportDto dto = new OrderBookingReportDto();
			dto.setSalesOrderNumber(pop.getVBELN());
			dto.setSalesOrderDate(pop.getAUDAT());
			dto.setItem(String.valueOf(Integer.parseInt(pop.getPOSNR())));
			dto.setGrade(pop.getGRADE());
			dto.setThickness(pop.getTHICK());
			dto.setWidth(pop.getWIDTH());
			dto.setLength(pop.getLENGTH());
			dto.setEdgeCondition(pop.getEDGE());
			dto.setFinish(pop.getFINISH());
			dto.setQuality(pop.getQUALITY());
			dto.setBasicPrice(String.valueOf(pop.getZZRATE()));//Required KBETR
			dto.setOrderItemQuantity(String.valueOf(pop.getKWMENG()));
			dto.setPoNumber(pop.getBSTNK());//Required BSTKD
			dto.setPoDate(pop.getBSTDK());
			dto.setProductType(pop.getPRODUCTTYPE());

			convertedResponseList.add(dto);
		});
		return convertedResponseList;

	}

	public List<SalesBillingDetailsReportDto> getSalesReportReponseDto(final ZSD34HYBRISResponse response)
	{
		final List<SalesBillingDetailsReportDto> convertedResponseList = new ArrayList<SalesBillingDetailsReportDto>();
		response.getZSD34HYBRIS().getItem().forEach(pop -> {
			final SalesBillingDetailsReportDto dto = new SalesBillingDetailsReportDto();
			dto.setGrade(pop.getGRADE());
			dto.setThickness(pop.getTHICK());
			dto.setWidth(pop.getWIDTH());
			dto.setLength(pop.getLENGTH());
			dto.setEdgeCondition(pop.getEDGE());
			dto.setFinish(pop.getFINISH());
			dto.setQuality(pop.getQUALITY());
			dto.setPoNumber(pop.getBSTKD());
			dto.setBasicPrice(String.valueOf(pop.getZZRATE()));
			dto.setBillingDocumentNumber(pop.getVBELN());
			dto.setBilledQuantity(String.valueOf(pop.getFKIMG()));
			dto.setPlant(pop.getWERKS());
			dto.setProductType(pop.getPRODUCTTYPE());
			dto.setItem((pop.getPOSNR()));
			dto.setGstInvoiceNumber(pop.getXBLNR());
			dto.setBillingDate(pop.getFKDAT());
			dto.setPlantDescription(pop.getNAME1());
			dto.setIlp(pop.getILP());
			dto.setPvc(pop.getPVC());
			dto.setInvoiceNumber(pop.getVBELN());
			dto.setVehicleNumber(pop.getZZEXTI1());
			dto.setTransporterNumber(pop.getZZTDLNR());
			dto.setTransporterName(pop.getZZTPTNAME());
			dto.setDriverNumber(pop.getZZCONNO());
			dto.setSaleOrderNumber(pop.getSONO());
			dto.setSaleItemNumber(String.valueOf(Integer.parseInt(pop.getITMNO())));
			dto.setAmount(StringUtils.isEmpty(pop.getAMOUNT()) ? StringUtils.EMPTY : pop.getAMOUNT().trim());
			dto.setFreight(StringUtils.isEmpty(pop.getFREIGHT()) ? StringUtils.EMPTY : pop.getFREIGHT().trim());
			dto.setTaxValue(StringUtils.isEmpty(pop.getTAXVAL()) ? StringUtils.EMPTY : pop.getTAXVAL().trim());
			dto.setTotalValue(StringUtils.isEmpty(pop.getTOTVAL()) ? StringUtils.EMPTY : pop.getTOTVAL().trim());

			convertedResponseList.add(dto);
		});
		return convertedResponseList;

	}

	public CustomerDashboardDto getCustomerDashboardDto(final ZSDCUSTDASHBOARDHYBRISResponse response)
	{
		final CustomerDashboardDto customerDashboard = new CustomerDashboardDto();

		customerDashboard.setAccountSummary(this.getCustomerDashboardAccountSummaryDto(response.getZFI60MHYBRIS()));
		customerDashboard.setBillingSummary(this.getCustomerDashboardBillingSummaryDto(response.getZSD34HYBRIS()));
		customerDashboard.setOrderBooking(this.getCustomerDashboardOrderBookingDto(response.getZSD35HYBRIS()));
		customerDashboard.setPopPlant(this.getCustomerDashboardPopPlantDto(response.getZSD46AHYBRIS()));
		customerDashboard.setPopYard(this.getCustomerDashboardPopYardDto(response.getZSD35CHYBRIS()));

		return customerDashboard;
	}

	public List<AccountSummaryDto> getCustomerDashboardAccountSummaryDto(final ZFITTACCSUMM response)
	{
		final List<AccountSummaryDto> responseList = new ArrayList<AccountSummaryDto>();

		response.getItem().forEach(res -> {
			final AccountSummaryDto dto = new AccountSummaryDto();
			dto.setCompanyCode(res.getCOMPANYCODE());
			dto.setCreditLimit(String.valueOf(res.getCRLMT()));
			dto.setOsBal(String.valueOf(res.getOSBAL()));
			dto.setOverDue(String.valueOf(res.getOVERDUE()));
			dto.setOverDueLess30(String.valueOf(res.get3030()));
			dto.setOverDueBetween60(String.valueOf(res.get33060()));
			dto.setOverDueGreater60(String.valueOf(res.get3E60()));

			responseList.add(dto);
		});
		return responseList;
	}

	public List<OrderBookingDto> getCustomerDashboardOrderBookingDto(final ZSDTTBILLSUMM response)
	{
		final List<OrderBookingDto> responseList = new ArrayList<OrderBookingDto>();

		response.getItem().forEach(res -> {
			final OrderBookingDto dto = new OrderBookingDto();
			dto.setMtd(String.valueOf(res.getMTD()));
			dto.setSeries(res.getSERIES());
			dto.setYesterdayBooking(String.valueOf(res.getYESTSALE()));
			dto.setYtd(String.valueOf(res.getYTD()));

			responseList.add(dto);
		});
		return responseList;
	}

	public List<BillingSummaryDto> getCustomerDashboardBillingSummaryDto(final ZSDTTBILLSUMM response)
	{
		final List<BillingSummaryDto> responseList = new ArrayList<BillingSummaryDto>();

		response.getItem().forEach(res -> {
			final BillingSummaryDto dto = new BillingSummaryDto();
			dto.setMtd(String.valueOf(res.getMTD()));
			dto.setSeries(res.getSERIES());
			dto.setYesterdaySale(String.valueOf(res.getYESTSALE()));
			dto.setYtd(String.valueOf(res.getYTD()));

			responseList.add(dto);
		});
		return responseList;
	}

	public List<PopYardDto> getCustomerDashboardPopYardDto(final ZSDTTPOPYARD response)
	{
		final List<PopYardDto> responseList = new ArrayList<PopYardDto>();

		response.getItem().forEach(res -> {
			final PopYardDto dto = new PopYardDto();
			dto.setCrap(res.getCRAP());
			dto.setHrap(res.getHRAP());
			dto.setHrBlack(res.getHRBLACK());
			dto.setPlate(res.getPLATE());
			dto.setSeries(res.getSERIES());

			responseList.add(dto);
		});
		return responseList;
	}

	public List<PopPlantDto> getCustomerDashboardPopPlantDto(final ZSDTTPOPYARD response)
	{
		final List<PopPlantDto> responseList = new ArrayList<PopPlantDto>();

		response.getItem().forEach(res -> {
			final PopPlantDto dto = new PopPlantDto();
			dto.setCrap(res.getCRAP());
			dto.setHrap(res.getHRAP());
			dto.setHrBlack(res.getHRBLACK());
			dto.setPlate(res.getPLATE());
			dto.setSeries(res.getSERIES());

			responseList.add(dto);
		});
		return responseList;
	}

	private HashMap<String, String> getCustomerInfo()
	{

		final HashMap<String, String> map = new HashMap<String, String>();
		final String defaultB2bUnit = Config.getParameter(DEFAULT_B2BUNIT_CODE);
		final List<String> defaultB2bUnitList = Arrays.asList(defaultB2bUnit.split(","));

		final B2BCustomerModel user = ((B2BCustomerModel) getUserService().getCurrentUser());
		final Collection<PrincipalGroupModel> b2bUnits = user.getAllGroups().stream()
				.filter(event -> (event.getItemtype().equalsIgnoreCase(B2BUnitModel._TYPECODE)
						&& !defaultB2bUnitList.contains(event.getUid())))
				.collect(Collectors.toList());
		if (!b2bUnits.isEmpty())
		{
			final B2BUnitModel parentb2bUnit = (B2BUnitModel) b2bUnits.iterator().next();
			map.put(CUSTOMER_ID, parentb2bUnit.getUid());
		}
		else
		{
			map.put(CUSTOMER_ID, user.getUid());
		}

		map.put(CUSTOMER_NAME, user != null ? user.getName() : "");

		return map;
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}


}
