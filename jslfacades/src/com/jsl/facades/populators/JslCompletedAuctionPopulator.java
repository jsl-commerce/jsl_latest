package com.jsl.facades.populators;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import com.jsl.configurablebundleservice.service.JslBundleTemplateService;
import com.jsl.core.dto.JslCompletedAuctionDto;
import com.jsl.core.util.JslB2bUnitUtil;
import com.jsl.jslConfigurableBundleService.model.BidEventModel;


public class JslCompletedAuctionPopulator
{
	private static final Logger LOG = Logger.getLogger(JslCompletedAuctionPopulator.class);
	private static final String DEFAULT_B2BUNIT_CODE = "default.b2b.unit.codes";
	private static final String NA = "N/A";
	private static final String BID_LOST = "BID LOST";
	private static final String BID_WON = "BID WON";
	private static final String DEFAULT_PRICE = "0.0";

	private Converter<ProductModel, ProductData> productConverter;
	private JslBundleTemplateService bundleTemplateService;

	public List<JslCompletedAuctionDto> populate(final Map<String, Object> allLotDetails, final Map<String, Object> requestObject)
	{
		final List<JslCompletedAuctionDto> completedBundleList = new ArrayList<>();
		final boolean isAdminUser = (boolean) requestObject.get("isAdminUser");
		try
		{
			final List<BundleTemplateModel> wonBundleList = (List<BundleTemplateModel>) allLotDetails.get("wonBundleList");
			for (final BundleTemplateModel bundleTemplate : wonBundleList)
			{
				wonBundleTemplateModelToData(completedBundleList, bundleTemplate);
			}

			final List<BidEventModel> lostBidEventList = (List<BidEventModel>) allLotDetails.get("lostBidEventList");
			for (final BidEventModel bidEvent : lostBidEventList)
			{
				lostBidModelToData(completedBundleList, bidEvent);
			}
			if (!isAdminUser)
			{
				final List<BundleTemplateModel> bundleWithoutBidList = (List<BundleTemplateModel>) allLotDetails
						.get("bundleWithoutBidList");
				for (final BundleTemplateModel bundleTemplate : bundleWithoutBidList)
				{
					bundleWithoutBidModelToDto(completedBundleList, bundleTemplate);
				}

				final List<BidEventModel> bundleWithoutBidByCurrentUserList = (List<BidEventModel>) allLotDetails
						.get("bundleWithoutBidByCurrentUserList");
				for (final BidEventModel bidEvent : bundleWithoutBidByCurrentUserList)
				{
					lotWithoutBidByCurrentUser(completedBundleList, bidEvent);
				}
			}
		}
		catch (final Exception e)
		{
			LOG.error("Exception in JslCompletedAuctionPopulator.populate() ", e);
		}

		return sort(completedBundleList);
	}

	private void lostBidModelToData(final List<JslCompletedAuctionDto> completedBundleList, final BidEventModel bidEvent)
	{
		try
		{
			final BundleTemplateModel bundleTemplate = bidEvent.getBundleTemplate();
			final JslCompletedAuctionDto bundleTemplateDto = new JslCompletedAuctionDto();
			bundleTemplateDto.setAuctionID(String.valueOf(bundleTemplate.getAuctionEvent().getAuctionId()));
			bundleTemplateDto.setAuctionStartDate(bundleTemplate.getAuctionEvent().getStartDate());
			bundleTemplateDto.setBundleID(bundleTemplate.getId());
			bundleTemplateDto.setBundleName(bundleTemplate.getName());
			bundleTemplateDto.setBundleQty(bundleTemplate.getQuantity());
			bundleTemplateDto.setStartBidPrice(
					bundleTemplate.getBaseBidPrice() != null ? String.valueOf(bundleTemplate.getBaseBidPrice()) : DEFAULT_PRICE);
			bundleTemplateDto.setUserLastBidPrice(String.valueOf(bidEvent.getBidPrice()));
			bundleTemplateDto.setH1Price(String.valueOf(bundleTemplate.getBidWonPrice()));
			bundleTemplateDto.setBidStatus(BID_LOST);
			final List<ProductData> productDataList = convertProduct(bundleTemplate);
			bundleTemplateDto.setProductData(productDataList);
			bundleTemplateDto.setOrderStatus(NA);
			completedBundleList.add(bundleTemplateDto);
		}
		catch (final Exception e)
		{
			LOG.error("Exception while converting lostBidEventList BidEventModel to Data");
		}
	}

	private void wonBundleTemplateModelToData(final List<JslCompletedAuctionDto> completedBundleList,
			final BundleTemplateModel bundleTemplate)
	{
		try
		{
			final JslCompletedAuctionDto bundleTemplateDto = new JslCompletedAuctionDto();
			bundleTemplateDto.setAuctionID(String.valueOf(bundleTemplate.getAuctionEvent().getAuctionId()));
			bundleTemplateDto.setAuctionStartDate(bundleTemplate.getAuctionEvent().getStartDate());
			bundleTemplateDto.setBundleID(bundleTemplate.getId());
			bundleTemplateDto.setBundleName(bundleTemplate.getName());
			bundleTemplateDto.setBundleQty(bundleTemplate.getQuantity());
			bundleTemplateDto.setStartBidPrice(
					bundleTemplate.getBaseBidPrice() != null ? String.valueOf(bundleTemplate.getBaseBidPrice()) : DEFAULT_PRICE);
			bundleTemplateDto.setUserLastBidPrice(String.valueOf(bundleTemplate.getBidWonPrice()));
			bundleTemplateDto.setH1Price(String.valueOf(bundleTemplate.getBidWonPrice()));
			bundleTemplateDto.setBidStatus(BID_WON);
			bundleTemplateDto.setBidWonBy(getUserName(bundleTemplate));
			final List<ProductData> productDataList = convertProduct(bundleTemplate);
			bundleTemplateDto.setProductData(productDataList);
			bundleTemplateDto.setOrderStatus(getBundleTemplateService().getOrderApprovalStatusForBundle(bundleTemplate));
			completedBundleList.add(bundleTemplateDto);
		}
		catch (final Exception e)
		{
			LOG.error("Exception while converting wonBundleList BundleTemplateModel to Data");
		}
	}

	private void bundleWithoutBidModelToDto(final List<JslCompletedAuctionDto> completedBundleList,
			final BundleTemplateModel bundleTemplate)
	{
		try
		{
			final JslCompletedAuctionDto bundleTemplateDto = new JslCompletedAuctionDto();
			bundleTemplateDto.setAuctionID(String.valueOf(bundleTemplate.getAuctionEvent().getAuctionId()));
			bundleTemplateDto.setAuctionStartDate(bundleTemplate.getAuctionEvent().getStartDate());
			bundleTemplateDto.setBundleID(bundleTemplate.getId());
			bundleTemplateDto.setBundleName(bundleTemplate.getName());
			bundleTemplateDto.setBundleQty(bundleTemplate.getQuantity());
			bundleTemplateDto.setStartBidPrice(
					bundleTemplate.getBaseBidPrice() != null ? String.valueOf(bundleTemplate.getBaseBidPrice()) : DEFAULT_PRICE);
			bundleTemplateDto.setUserLastBidPrice(DEFAULT_PRICE);
			bundleTemplateDto.setH1Price(DEFAULT_PRICE);
			bundleTemplateDto.setBidStatus(NA);
			bundleTemplateDto.setBidWonBy(NA);
			final List<ProductData> productDataList = convertProduct(bundleTemplate);
			bundleTemplateDto.setProductData(productDataList);
			bundleTemplateDto.setOrderStatus(NA);
			completedBundleList.add(bundleTemplateDto);
		}
		catch (final Exception e)
		{
			LOG.error("Exception while converting wonBundleList BundleTemplateModel to Data");
		}
	}

	private void lotWithoutBidByCurrentUser(final List<JslCompletedAuctionDto> completedBundleList,
			final BidEventModel bidEvent)
	{
		try
		{
			final BundleTemplateModel bundleTemplate = bidEvent.getBundleTemplate();
			final JslCompletedAuctionDto bundleTemplateDto = new JslCompletedAuctionDto();
			bundleTemplateDto.setAuctionID(String.valueOf(bundleTemplate.getAuctionEvent().getAuctionId()));
			bundleTemplateDto.setAuctionStartDate(bundleTemplate.getAuctionEvent().getStartDate());
			bundleTemplateDto.setBundleID(bundleTemplate.getId());
			bundleTemplateDto.setBundleName(bundleTemplate.getName());
			bundleTemplateDto.setBundleQty(bundleTemplate.getQuantity());
			bundleTemplateDto.setStartBidPrice(
					bundleTemplate.getBaseBidPrice() != null ? String.valueOf(bundleTemplate.getBaseBidPrice()) : DEFAULT_PRICE);
			bundleTemplateDto.setUserLastBidPrice(DEFAULT_PRICE);
			bundleTemplateDto.setH1Price(String.valueOf(bundleTemplate.getBidWonPrice()));
			bundleTemplateDto.setBidStatus(BID_LOST);
			final List<ProductData> productDataList = convertProduct(bundleTemplate);
			bundleTemplateDto.setProductData(productDataList);
			bundleTemplateDto.setOrderStatus(NA);
			completedBundleList.add(bundleTemplateDto);
		}
		catch (final Exception e)
		{
			LOG.error("Exception while converting lostBidEventList BidEventModel to Data");
		}
	}

	private List<ProductData> convertProduct(final BundleTemplateModel bundleTemplate)
	{
		final List<ProductModel> products = bundleTemplate.getProducts();
		final List<ProductData> productDataList = new ArrayList<>(products.size());
		products.forEach(product -> {
			productDataList.add(getProductConverter().convert(product));
		});

		return productDataList;
	}

	private List<JslCompletedAuctionDto> sort(final List<JslCompletedAuctionDto> completedBundleList)
	{
		return completedBundleList.stream().sorted(Comparator.comparing(JslCompletedAuctionDto::getAuctionID).reversed()
				.thenComparing(JslCompletedAuctionDto::getBundleID)).collect(Collectors.toList());
	}

	private String getUserName(final BundleTemplateModel bundleTemplate)
	{
		String userName = null;
		try
		{
			final BidEventModel bidEvent = bundleTemplate.getBidEvent();
			if (bidEvent != null)
			{
				final B2BCustomerModel user = (B2BCustomerModel) bidEvent.getCreatedBy();
				final B2BUnitModel parentb2bUnit = JslB2bUnitUtil.getParentB2bUnit(user);
				if (parentb2bUnit != null)
				{
					userName = parentb2bUnit.getDisplayName();
				}
				else
				{
					userName = user.getDisplayName();
				}
			}
		}
		catch (final Exception e)
		{
			LOG.error("Exception while setting H1 bidder JslCompletedAuctionPopulator.getUserName()");
		}

		return userName;
	}

	public Converter<ProductModel, ProductData> getProductConverter()
	{
		return productConverter;
	}

	public void setProductConverter(final Converter<ProductModel, ProductData> productConverter)
	{
		this.productConverter = productConverter;
	}

	public JslBundleTemplateService getBundleTemplateService()
	{
		return bundleTemplateService;
	}

	public void setBundleTemplateService(final JslBundleTemplateService bundleTemplateService)
	{
		this.bundleTemplateService = bundleTemplateService;
	}

}
