/**
 *
 */
package com.jsl.facades.populators;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.variants.model.VariantProductModel;


/**
 * @author s.bhambhani
 *
 */
public class JslProductPopulator implements Populator<ProductModel, ProductData>
{
	@Override
	public void populate(final ProductModel source, final ProductData target) throws ConversionException
	{
		final ProductModel baseProduct = getBaseProduct(source);
		target.setPlant(baseProduct.getPlant());
		target.setLocation(baseProduct.getLocation());
		target.setProductType(baseProduct.getProductType());
		target.setQuality(baseProduct.getQuality());
		target.setStandard(baseProduct.getStandard());
		target.setSeries(baseProduct.getSeries());
		target.setGrade(baseProduct.getGrade());
		target.setGradeGroup(baseProduct.getGradeGroup());
		target.setThickness(baseProduct.getThickness());
		target.setWidth(baseProduct.getWidth());
		target.setLength(baseProduct.getLength());
		target.setFinish(baseProduct.getFinish());
		target.setMaterialNum(baseProduct.getMaterialNum());
		target.setEdgeCondition(baseProduct.getEdgeCondition());
		target.setMinPartWt(baseProduct.getMinPartWt());
		target.setMaxPartWt(baseProduct.getMaxPartWt());
		target.setUsageIndicator(baseProduct.getUsageIndicator());
		target.setIlp(baseProduct.getIlp());
		target.setPvc(baseProduct.getPvc());
		target.setTolerance(baseProduct.getTolerance());
		target.setIncoterms(baseProduct.getIncoterms());
		target.setCoilId(baseProduct.getCoilId());
		target.setMarkingOnProd(baseProduct.getMarkingOnProd());
		target.setAgeingOfManufacturing(baseProduct.getAgeingOfManufacturing());
		target.setMarkingOnSticker(baseProduct.getMarkingOnSticker());
		target.setTpi(baseProduct.getTpi());
		target.setQuantity(baseProduct.getQuantity());
		target.setWeight(baseProduct.getWeight());
		target.setNumOfSheets(baseProduct.getNumOfSheets());
		target.setInspThirdParty(baseProduct.getInspThirdParty());
		target.setRoute(baseProduct.getRoute());
		target.setUtensilUsage(baseProduct.getUtensilUsage());
		target.setDesiredDate(baseProduct.getDesiredDate());
		target.setStoreLocator(baseProduct.getStoreLocator());
		target.setStoreLocatorDesc(baseProduct.getStoreLocatorDesc());
		target.setPhysicalStoreLocator(baseProduct.getPhysicalStoreLocator());
		target.setPhysicalStoreLocatorDesc(baseProduct.getPhysicalStoreLocatorDesc());
	}


	protected ProductModel getBaseProduct(final ProductModel productModel)
	{
		ProductModel currentProduct = productModel;
		while (currentProduct instanceof VariantProductModel)
		{
			final VariantProductModel variant = (VariantProductModel) currentProduct;
			currentProduct = variant.getBaseProduct();
		}
		return currentProduct;
	}
}

