/**
 *
 */
package com.jsl.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.jsl.core.model.InvoiceDetailsModel;
import com.jsl.facades.supportticket.dto.InvoiceDetailsDto;


/**
 * @author Kiranraj S.
 *
 */
public class InvoiceModelPopulator implements Populator<InvoiceDetailsDto, InvoiceDetailsModel>
{

	@Override
	public void populate(final InvoiceDetailsDto invoiceDetailsDto, final InvoiceDetailsModel invoiceDetailsModel)
			throws ConversionException
	{
		invoiceDetailsModel.setBatchNumber(invoiceDetailsDto.getBatchNumber());
		invoiceDetailsModel.setComplaintQuantity(invoiceDetailsDto.getComplaintQuantity());
		invoiceDetailsModel.setInvoiceDate(invoiceDetailsDto.getInvoiceDate());
		invoiceDetailsModel.setKamName(invoiceDetailsDto.getKamName());
		invoiceDetailsModel.setPrimaryDefect(invoiceDetailsDto.getPrimaryDefect());
		invoiceDetailsModel.setSoNum(invoiceDetailsDto.getSoNumber());
		invoiceDetailsModel.setSupplyQuantity(invoiceDetailsDto.getSupplyQuantity());
		invoiceDetailsModel.setEdge(invoiceDetailsDto.getEdge());
		invoiceDetailsModel.setFinish(invoiceDetailsDto.getFinish());
		invoiceDetailsModel.setGrade(invoiceDetailsDto.getGrade());
		invoiceDetailsModel.setLength(invoiceDetailsDto.getLength());
		invoiceDetailsModel.setPlant(invoiceDetailsDto.getPlant());
		invoiceDetailsModel.setKamId(invoiceDetailsDto.getKamId());
		invoiceDetailsModel.setPrimaryDefect(invoiceDetailsDto.getPrimaryDefect());
		invoiceDetailsModel.setProductCode(invoiceDetailsDto.getProductCode());
		invoiceDetailsModel.setThickness(invoiceDetailsDto.getThickness());
		invoiceDetailsModel.setWidth(invoiceDetailsDto.getWidth());
		invoiceDetailsModel.setCompanyCode(invoiceDetailsDto.getCompanyCode());
		invoiceDetailsModel.setUsage(invoiceDetailsDto.getUsage());
		invoiceDetailsModel.setStandard(invoiceDetailsDto.getStandard());
		invoiceDetailsModel.setGradeGroup(invoiceDetailsDto.getGradeGroup());
		invoiceDetailsModel.setSeries(invoiceDetailsDto.getSeries());
	}

}
