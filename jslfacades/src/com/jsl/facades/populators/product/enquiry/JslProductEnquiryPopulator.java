/**
 *
 */
package com.jsl.facades.populators.product.enquiry;



import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.List;

import com.jsl.core.dto.EnquiryEntry;
import com.jsl.core.dto.ProductEnquiry;
import com.jsl.core.model.EnquiryEntryModel;
import com.jsl.core.model.ProductEnquiryModel;


/**
 * @author sunil.maurya.kumar
 *
 */
public class JslProductEnquiryPopulator implements Populator<ProductEnquiry, ProductEnquiryModel>
{

	private UserService userService;




	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}



	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}



	@Override
	public void populate(final ProductEnquiry source, final ProductEnquiryModel target) throws ConversionException
	{

		final UserModel currentUser = getUserService().getCurrentUser();

		final List<EnquiryEntry> enquiryEntryDataList = source.getEnquiryEntries();
		final List<EnquiryEntryModel> enquiryEntryModelList = new ArrayList<EnquiryEntryModel>();
		for (final EnquiryEntry enquiryEntryData : enquiryEntryDataList)
		{
			final EnquiryEntryModel enquiryEntryModel = new EnquiryEntryModel();

			enquiryEntryModel.setProductType(enquiryEntryData.getProductType());
			enquiryEntryModel.setSeries(enquiryEntryData.getSeries());
			enquiryEntryModel.setGrade(enquiryEntryData.getGrade());
			enquiryEntryModel.setFinish(enquiryEntryData.getFinish());
			enquiryEntryModel.setThickness(enquiryEntryData.getThickness());
			enquiryEntryModel.setWidth(enquiryEntryData.getWidth());
			enquiryEntryModel.setLength(enquiryEntryData.getLength());
			enquiryEntryModel.setEdgeCondition(enquiryEntryData.getEdgeCondition());
			enquiryEntryModel.setIlp(enquiryEntryData.getIlp());
			enquiryEntryModel.setInspection(enquiryEntryData.getInspection());
			enquiryEntryModel.setPvc(enquiryEntryData.getPvc());
			enquiryEntryModel.setSpecialRemarks(enquiryEntryData.getSpecialRemarks());
			enquiryEntryModel.setQuantity(enquiryEntryData.getQuantity());
			enquiryEntryModelList.add(enquiryEntryModel);

		}

		target.setUserComments(source.getComments());
		target.setEntryEnquiry(enquiryEntryModelList);
		target.setUser(currentUser);
	}



}
