/**
 *
 */
package com.jsl.facades.populators.product.enquiry;



import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.List;

import com.jsl.core.dto.EnquiryEntry;
import com.jsl.core.dto.ProductEnquiry;
import com.jsl.core.model.EnquiryEntryModel;
import com.jsl.core.model.ProductEnquiryModel;


/**
 * @author sunil.maurya.kumar
 *
 */
public class JslProductEnquiryHistoryPopulator implements Populator<ProductEnquiryModel, ProductEnquiry>
{

	@Override
	public void populate(final ProductEnquiryModel source, final ProductEnquiry target) throws ConversionException
	{

		final List<EnquiryEntryModel> enquiryEntryModelList = (List<EnquiryEntryModel>) source.getEntryEnquiry();
		final List<EnquiryEntry> enquiryEntryDataList = new ArrayList<EnquiryEntry>();

		enquiryEntryModelList.forEach(enquiryEntryModel -> {
			final EnquiryEntry enquiryEntryData = new EnquiryEntry();
			enquiryEntryData.setProductType(enquiryEntryModel.getProductType());
			enquiryEntryData.setSeries(enquiryEntryModel.getSeries());
			enquiryEntryData.setGrade(enquiryEntryModel.getGrade());
			enquiryEntryData.setFinish(enquiryEntryModel.getFinish());
			enquiryEntryData.setThickness(enquiryEntryModel.getThickness());
			enquiryEntryData.setWidth(enquiryEntryModel.getWidth());
			enquiryEntryData.setLength(enquiryEntryModel.getLength());
			enquiryEntryData.setEdgeCondition(enquiryEntryModel.getEdgeCondition());
			enquiryEntryData.setIlp(enquiryEntryModel.getIlp());
			enquiryEntryData.setInspection(enquiryEntryModel.getInspection());
			enquiryEntryData.setPvc(enquiryEntryModel.getPvc());
			enquiryEntryData.setSpecialRemarks(enquiryEntryModel.getSpecialRemarks());
			enquiryEntryData.setQuantity(enquiryEntryModel.getQuantity());

			enquiryEntryDataList.add(enquiryEntryData);

		});

		target.setCreatedDate(source.getCreationtime());
		target.setComments(source.getUserComments());
		target.setEnquiryEntries(enquiryEntryDataList);
		target.setEnquiryId(source.getEnquiryNumber());
		target.setKamName(source.getKamName());
	}
}
