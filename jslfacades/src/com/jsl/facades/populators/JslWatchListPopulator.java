/**
 *
 */
package com.jsl.facades.populators;

import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import com.jsl.core.dto.AuctionEventData;
import com.jsl.core.dto.BundleTemplateData;
import com.jsl.core.dto.WatchListData;
import com.jsl.core.model.AuctionEventModel;
import com.jsl.jslConfigurableBundleService.model.WatchListModel;


public class JslWatchListPopulator implements Populator<WatchListModel, WatchListData>
{

	private Converter<BundleTemplateModel, BundleTemplateData> bundleTemplateConverter;
	private Converter<AuctionEventModel, AuctionEventData> auctionConverter;

	@Override
	public void populate(final WatchListModel watchListModel, final WatchListData watchListData) throws ConversionException
	{
		watchListData.setAuctionEventData(getAuctionConverter().convert(watchListModel.getAuctionEvent()));
		watchListData.setBundleTemplateData(getBundleTemplateConverter().convert(watchListModel.getBundleTemplate()));
		watchListData.setWatchListId(watchListModel.getWatchListId());
		watchListData.setCreatedBy(watchListModel.getCreatedBy().getUid());
	}

	public Converter<BundleTemplateModel, BundleTemplateData> getBundleTemplateConverter()
	{
		return bundleTemplateConverter;
	}

	public void setBundleTemplateConverter(final Converter<BundleTemplateModel, BundleTemplateData> bundleTemplateConverter)
	{
		this.bundleTemplateConverter = bundleTemplateConverter;
	}

	public Converter<AuctionEventModel, AuctionEventData> getAuctionConverter()
	{
		return auctionConverter;
	}

	public void setAuctionConverter(final Converter<AuctionEventModel, AuctionEventData> auctionConverter)
	{
		this.auctionConverter = auctionConverter;
	}

}
