/**
 *
 */
package com.jsl.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.jsl.core.model.InvoiceDetailsModel;
import com.jsl.facades.supportticket.dto.InvoiceDetailsDto;


/**
 * @author Kiranraj S.
 *
 */
public class InvoiceDtoPopulator implements Populator<InvoiceDetailsModel, InvoiceDetailsDto>
{

	@Override
	public void populate(final InvoiceDetailsModel invoiceDetailsModel, final InvoiceDetailsDto invoiceDetailsDto)
			throws ConversionException
	{
		invoiceDetailsDto.setBatchNumber(invoiceDetailsModel.getBatchNumber());
		invoiceDetailsDto.setComplaintQuantity(invoiceDetailsModel.getComplaintQuantity());
		invoiceDetailsDto.setInvoiceDate(invoiceDetailsModel.getInvoiceDate());
		invoiceDetailsDto.setKamName(invoiceDetailsModel.getKamName());
		invoiceDetailsDto.setKamId(invoiceDetailsModel.getKamId());
		invoiceDetailsDto.setPrimaryDefect(invoiceDetailsModel.getPrimaryDefect());
		invoiceDetailsDto.setSoNumber(invoiceDetailsModel.getSoNum());
		invoiceDetailsDto.setSupplyQuantity(invoiceDetailsModel.getSupplyQuantity());
		invoiceDetailsDto.setCompanyCode(invoiceDetailsModel.getCompanyCode());
		invoiceDetailsDto.setEdge(invoiceDetailsModel.getEdge());
		invoiceDetailsDto.setFinish(invoiceDetailsModel.getFinish());
		invoiceDetailsDto.setGrade(invoiceDetailsModel.getGrade());
		invoiceDetailsDto.setLength(invoiceDetailsModel.getLength());
		invoiceDetailsDto.setPlant(invoiceDetailsModel.getPlant());
		invoiceDetailsDto.setProductCode(invoiceDetailsModel.getProductCode());
		invoiceDetailsDto.setThickness(invoiceDetailsModel.getThickness());
		invoiceDetailsDto.setWidth(invoiceDetailsModel.getWidth());
		invoiceDetailsDto.setProductCode(invoiceDetailsModel.getProductCode());
		invoiceDetailsDto.setGradeGroup(invoiceDetailsModel.getGradeGroup());
		invoiceDetailsDto.setUsage(invoiceDetailsModel.getUsage());
		invoiceDetailsDto.setStandard(invoiceDetailsModel.getStandard());
		invoiceDetailsDto.setSeries(invoiceDetailsModel.getSeries());
	}

}
