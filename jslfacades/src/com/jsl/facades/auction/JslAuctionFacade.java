/**
 *
 */
package com.jsl.facades.auction;

import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;
import java.util.Map;

import com.jsl.core.dto.AuctionEventData;
import com.jsl.core.dto.BundleTemplateData;
import com.jsl.core.dto.DefaultAuctionCommonDto;
import com.jsl.core.dto.DefaultBundleCommonDto;
import com.jsl.core.dto.WatchListData;
import com.jsl.core.model.AuctionEventModel;
import com.jsl.jslConfigurableBundleService.model.BidEventModel;


/**
 * @author Hybris
 *
 */
public interface JslAuctionFacade
{
	public List<AuctionEventModel> getActiveAuctionList();

	public List<BundleTemplateModel> getAuctionList();

	public List<BundleTemplateData> getOngoingAuctionDetails(String auctionId);

	public Map<String, Object> getCompletedAuctionDetails(Map<String, Object> requestObject,
			Map<String, Object> responseObject);

	public List<AuctionEventData> getUpcomingAuctionDetails();

	public List<DefaultBundleCommonDto> getMaxBidForCurrentUser(String auctionEventId);

	public List<DefaultBundleCommonDto> getProxyBidForCurrentUser(String auctionId);

	public DefaultAuctionCommonDto getAllMaxBidPrice(String auctionEvent);

	public Map<String, Object> placeBid(DefaultAuctionCommonDto bidEvent, Map<String, Object> placeBidResponse);

	public Map<String, Object> setProxy(List<DefaultBundleCommonDto> bidEventList, Map<String, Object> setProxyResponse);

	public List<WatchListData> getWatchListDetails(String auctionID);

	public boolean removeFromWatchList(List<DefaultBundleCommonDto> toRemovefromWatchList);

	public int addToWatchList(List<DefaultBundleCommonDto> toRemovefromWatchList);

	public List<BundleTemplateModel> getCompletedAuctionDetailsBasedOnDateRange(DefaultBundleCommonDto auctionDateRange);

	public List<ProductModel> getProductListByBundleId(String bundleId);

	public List<BundleTemplateData> getBundleListByAuctionId(String auctionId);

	public List<AuctionEventData> getLiveAuctionList();

	public List<AuctionEventData> checkOtpStatus(String identityValue, List<AuctionEventData> auctionEventList);

	public long updateAuctionEventWithAdditionalTime(AuctionEventModel auctionEvent);

	public AuctionEventModel getAuctionEventForAuctionId(String auctionId);

	public Map<String, Object> getAllBidHistoryForLot(DefaultBundleCommonDto lotDetails);

	public boolean isAdminObserverUser(String currentUserId);

	public List<AuctionEventData> getAllAuctionList();

	public List<AuctionEventModel> getAuctionByDateRange(DefaultBundleCommonDto auctionDateRange);

	public List<AuctionEventModel> getAuctionByDate(DefaultBundleCommonDto auctionDateRange);

	public List<BidEventModel> getLastBidEventForBundle(String bundleId);

	public AuctionEventData getAuctionEventDataForAuctionId(String auctionId);

	public List<BidEventModel> getLostAuctionLotsDetailsForCurrentuser(List<BundleTemplateModel> bundleTemplateList,
			String flowName);

	public List<BundleTemplateData> convertBundleModelToData(List<BundleTemplateModel> bundleTemplateList);

	public List<BidEventModel> getLostAuctionLotsDetailsForCurrentUserByDateRange(List<BundleTemplateModel> bundleTemplateList,
			DefaultBundleCommonDto auctionDateRange);

	public List<BundleTemplateModel> getCompletedAuctionDetailsForAdmin(Map<String, Object> requestObject,
			Map<String, Object> responseObject);

	public List<BundleTemplateModel> getCompletedAuctionDetailsForAdminByDateRange(Map<String, Object> requestObject,
			Map<String, Object> responseObject);

}
