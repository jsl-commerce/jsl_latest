/**
 *
 */
package com.jsl.facades.auction;

import de.hybris.platform.cronjob.model.CronJobModel;


/**
 * @author subhrajitk766
 *
 */
public interface JslBidDetailsExportService
{
	public void createBidDetailsExport(final CronJobModel bidDetailsExportCronjobModel);

}
