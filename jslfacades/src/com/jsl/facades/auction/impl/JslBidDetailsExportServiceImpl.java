/**
 *
 */
package com.jsl.facades.auction.impl;

import de.hybris.platform.acceleratorservices.email.EmailService;
import de.hybris.platform.acceleratorservices.model.email.EmailAttachmentModel;
import de.hybris.platform.catalog.CatalogService;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.user.UserService;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;

import com.jsl.core.dto.BidEventDto;
import com.jsl.core.dto.DefaultBundleCommonDto;
import com.jsl.core.model.BidDetailsExportEmailProcessModel;
import com.jsl.core.services.JslEmailService;
import com.jsl.facades.auction.JslBidDetailsExportService;
import com.jsl.facades.csv.JslBidEventDataExportExcelFacade;
//import com.jsl.model.process.email.actions.BidDetailsExportViaCronJobModel;


/**
 * @author PWC
 *
 */

public class JslBidDetailsExportServiceImpl implements JslBidDetailsExportService
{
	private static final Logger LOG = Logger.getLogger(JslBidDetailsExportServiceImpl.class);
	public static final String BID_REPORT_USER_EMAIL = "jsl.bid.report.export.emil.id";
	public static final String EXCEL_MIME_TYPE = "application/vnd.ms-excel";

	@Autowired
	private JslBidEventDataExportExcelFacade jslBidEventDataExportExcelFacade;

	@Autowired
	private EmailService emailService;

	@Autowired
	private ModelService modelService;

	@Autowired
	private BusinessProcessService businessProcessService;

	@Autowired
	private CMSSiteService cmsSiteService;

	@Autowired
	private UserService userService;

	@Autowired
	private FlexibleSearchService flexibleSearchService;

	@Autowired
	private CatalogService catalogService;

	@Autowired
	private ConfigurationService configurationService;

	@Autowired
	private MediaService mediaService;

	@Resource(name = "emailService")
	private JslEmailService jslEmailService;




	@Override
	public void createBidDetailsExport(final CronJobModel bidDetailsExportCronjobModel)
	{
		LOG.info("Inside class JslBidDetailsExportServiceImpl and within method createBidDetailsExport");
		try
		{
			List<BidEventDto> bidEventDataList = null;
			final Map<String, Object> requestObject = new HashMap<>();
			final DefaultBundleCommonDto auctionDateRange = new DefaultBundleCommonDto();

			final Date today = new Date();
			final GregorianCalendar cal = new GregorianCalendar();
			cal.setTime(today);
			cal.add(Calendar.DATE, -120);

			final Date oldDate = cal.getTime();

			final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-M-dd");

			final String startDate = formatter.format(oldDate);
			final String endDate = formatter.format(today);

			auctionDateRange.setAuctionStartDate(startDate);
			auctionDateRange.setAuctionEndDate(endDate);
			requestObject.put("dateRange", Boolean.TRUE);
			requestObject.put("auctionDateRange", auctionDateRange);
			LOG.info("before calling dao>>>>>>>>>>>>>>>>>>");


			bidEventDataList = jslBidEventDataExportExcelFacade.getCompletedReport(requestObject);


			LOG.info("bidEventDataList>>>>>>>>>>>" + bidEventDataList);
			if (CollectionUtils.isNotEmpty(bidEventDataList))
			{
				jslBidEventDataExportExcelFacade.addSalesOrderNumber(bidEventDataList);
			}
			final String[] header =
			{ "AuctionId", "SalesOrderNumber", "AuctionDate", "LotID", "Location", "MaterialNum", "BatchId", "GradeGroup", "Grade",
					"Thickness", "Width", "Length", "Finish", "Edge", "PVC", "Quality", "Quantity", "Age", "Plant", "StoLoc",
					"DescStoLoc", "SBP", "H1Bidder", "H1BidPricePerMT", "NoOfBids" };



			final String excelFileName = "Auctions List" + "_"
					+ new SimpleDateFormat("DD_MM_YYYY_HH:mm").format(new Date(System.currentTimeMillis())) + ".xlsx";
			final File excelFile = new File(excelFileName);
			final XSSFWorkbook workbook = new XSSFWorkbook();
			final XSSFSheet sheet = workbook.createSheet("Auction Detailed Report");

			int rowCount = 0;
			int columnCount = 0;
			final Row row = sheet.createRow(rowCount++);
			for (final Object field : header)
			{
				final Cell cell = row.createCell(columnCount);
				if (field instanceof String)
				{
					cell.setCellValue((String) field);
				}
				columnCount++;
			}
			if (CollectionUtils.isNotEmpty(bidEventDataList))
			{
				for (final BidEventDto bidEvent : bidEventDataList)
				{
					final Row rowForData = sheet.createRow(rowCount++);
					createReport(bidEvent, rowForData);
				}
			}
			else
			{
				LOG.info("No bid Details is avilable for the report");
			}

			final FileOutputStream out = new FileOutputStream(excelFile); // file name with path
			workbook.write(out);
			out.close();
			createAttachmentAndSendMail(out, excelFileName, excelFile);//send the output file in the directory
		}

		catch (final Exception ex)
		{
			LOG.error("Exception caught while creating the excel for AUction Report" + ex);
		}


	}



	private static void createReport(final BidEventDto bidEvent, final Row row) // creating cells for each row
	{
		Cell cell = row.createCell(0);
		cell.setCellValue(null == bidEvent.getAuctionId() ? "" : bidEvent.getAuctionId().toString());

		cell = row.createCell(1);
		cell.setCellValue(null == bidEvent.getSalesOrderNumber() ? "" : bidEvent.getSalesOrderNumber());

		final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd MMMM yyyy zzzz");

		cell = row.createCell(2);
		if (null != bidEvent.getAuctionDate())
		{
			cell.setCellValue(dateFormatter.format(bidEvent.getAuctionDate()));
		}
		else
		{
			cell.setCellValue(bidEvent.getAuctionDate());
		}

		cell = row.createCell(3);
		cell.setCellValue(null == bidEvent.getLotId() ? "" : bidEvent.getLotId());

		cell = row.createCell(4);
		cell.setCellValue(null == bidEvent.getLocation() ? "" : bidEvent.getLocation());

		cell = row.createCell(5);
		cell.setCellValue(null == bidEvent.getMaterialNum() ? "" : bidEvent.getMaterialNum());

		cell = row.createCell(6);
		cell.setCellValue(null == bidEvent.getBatchId() ? "" : bidEvent.getBatchId());

		cell = row.createCell(7);
		cell.setCellValue(null == bidEvent.getGradeGroup() ? "" : bidEvent.getGradeGroup());

		cell = row.createCell(8);
		cell.setCellValue(null == bidEvent.getGrade() ? "" : bidEvent.getGrade());

		cell = row.createCell(9);
		cell.setCellValue(null == bidEvent.getThickness() ? "" : bidEvent.getThickness().toString());

		cell = row.createCell(10);
		cell.setCellValue(null == bidEvent.getWidth() ? "" : bidEvent.getWidth().toString());


		cell = row.createCell(11);
		cell.setCellValue(null == bidEvent.getLength() ? "" : bidEvent.getLength().toString());

		cell = row.createCell(12);
		cell.setCellValue(null == bidEvent.getFinish() ? "" : bidEvent.getFinish());

		cell = row.createCell(13);
		cell.setCellValue(null == bidEvent.getEdge() ? "" : bidEvent.getEdge());

		cell = row.createCell(14);
		cell.setCellValue(null == bidEvent.getPvc() ? "" : bidEvent.getPvc());

		cell = row.createCell(15);
		cell.setCellValue(null == bidEvent.getQuality() ? "" : bidEvent.getQuality());

		cell = row.createCell(16);
		cell.setCellValue(null == bidEvent.getQuantity() ? "" : bidEvent.getQuantity().toString());

		cell = row.createCell(17);
		cell.setCellValue(null == bidEvent.getAge() ? "" : bidEvent.getAge());

		cell = row.createCell(18);
		cell.setCellValue(null == bidEvent.getPlant() ? "" : bidEvent.getPlant());

		cell = row.createCell(19);
		cell.setCellValue(null == bidEvent.getStoLoc() ? "" : bidEvent.getStoLoc());

		cell = row.createCell(20);
		cell.setCellValue(null == bidEvent.getDescStoLoc() ? "" : bidEvent.getDescStoLoc());

		cell = row.createCell(21);
		cell.setCellValue(null == bidEvent.getSbp() ? "" : bidEvent.getSbp());

		cell = row.createCell(22);
		cell.setCellValue(null == bidEvent.getH1Bidder() ? "" : bidEvent.getH1Bidder());

		cell = row.createCell(23);
		cell.setCellValue(null == bidEvent.getH1BidPricePerMT() ? "" : bidEvent.getH1BidPricePerMT());

		cell = row.createCell(24);
		cell.setCellValue(null == bidEvent.getNoOfBids() ? "" : bidEvent.getNoOfBids());
	}

	/**
	 * @param excelFileName
	 * @param excelFile
	 * @param out
	 * @throws FileNotFoundException
	 */
	private void createAttachmentAndSendMail(final FileOutputStream outputStream, final String excelFileName, final File excelFile)
			throws FileNotFoundException
	{
		final CMSSiteModel currentSite = getCurrentSite();

		LOG.info("CMS SITE found " + currentSite.getUid());
		final BidDetailsExportEmailProcessModel bidDetailsExportEmailProcessModel = (BidDetailsExportEmailProcessModel) businessProcessService
				.createProcess("sendBidReportProcess-" + System.currentTimeMillis(), "sendBidReportProcess");

		bidDetailsExportEmailProcessModel.setEmailID(configurationService.getConfiguration().getString(BID_REPORT_USER_EMAIL));
		bidDetailsExportEmailProcessModel.setSite(currentSite);
		bidDetailsExportEmailProcessModel.setLanguage(currentSite.getDefaultLanguage());
		bidDetailsExportEmailProcessModel.setCurrency(currentSite.getStores().get(0).getDefaultCurrency());
		bidDetailsExportEmailProcessModel.setStore(currentSite.getStores().get(0));

		final DataInputStream inputStream = new DataInputStream(new FileInputStream(excelFile));
		final EmailAttachmentModel attachment = jslEmailService.createEmailAttachment(inputStream, excelFileName, EXCEL_MIME_TYPE);

		bidDetailsExportEmailProcessModel.setEmailAttachment(attachment);
		modelService.save(bidDetailsExportEmailProcessModel);
		businessProcessService.startProcess(bidDetailsExportEmailProcessModel);

	}




	/**
	 * @return
	 */
	private CMSSiteModel getCurrentSite()
	{
		final String GET_SITE_FOR_EMAIL = "select {pk} from {cmssite}  where {uid} ='jsl'";
		SearchResult<CMSSiteModel> result = null;
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_SITE_FOR_EMAIL);
		result = flexibleSearchService.search(query);
		return result.getResult().get(0);

	}
}

