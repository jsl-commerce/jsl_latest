/**
 *
 */
package com.jsl.facades.auction.impl;

import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.configurablebundleservices.model.BundleTemplateModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.jsl.configurablebundleservice.service.JslBundleTemplateService;
import com.jsl.core.constants.JslAuctionEnum;
import com.jsl.core.dto.AuctionEventData;
import com.jsl.core.dto.BidEventData;
import com.jsl.core.dto.BundleTemplateData;
import com.jsl.core.dto.DefaultAuctionCommonDto;
import com.jsl.core.dto.DefaultBundleCommonDto;
import com.jsl.core.dto.JslCompletedAuctionDto;
import com.jsl.core.dto.JslOTPValidationDto;
import com.jsl.core.dto.WatchListData;
import com.jsl.core.enums.OTPGeneratedForEnum;
import com.jsl.core.enums.OTPStatus;
import com.jsl.core.enums.OTPUserIdentificationType;
import com.jsl.core.exceptions.JslAuctionException;
import com.jsl.core.model.AuctionEventModel;
import com.jsl.core.model.AuctionUserGroupModel;
import com.jsl.core.model.JSLUserOTPGenerationModel;
import com.jsl.core.services.JslAuctionService;
import com.jsl.core.services.JslBidEventService;
import com.jsl.core.services.JslProxyBidRuleService;
import com.jsl.facades.auction.JslAuctionFacade;
import com.jsl.facades.otp.JslUserOTPFacade;
import com.jsl.facades.populators.JslCompletedAuctionPopulator;
import com.jsl.jslConfigurableBundleService.model.BidEventModel;
import com.jsl.jslConfigurableBundleService.model.WatchListModel;


public class JslAuctionFacadeImpl implements JslAuctionFacade
{
	private static final Logger LOG = Logger.getLogger(JslAuctionFacadeImpl.class);

	@Resource(name = "siteConfigService")
	private SiteConfigService siteConfigService;

	private JslBundleTemplateService bundleTemplateService;

	private JslAuctionService jslAuctionService;

	private JslBidEventService jslBidEventService;

	private JslProxyBidRuleService jslProxyBidRuleService;

	private Converter<AuctionEventModel, AuctionEventData> auctionConverter;

	private Converter<BundleTemplateModel, BundleTemplateData> bundleTemplateConverter;

	private Converter<BidEventModel, BidEventData> bidEventConverter;

	private Converter<WatchListModel, WatchListData> watchListConverter;

	private JslCompletedAuctionPopulator completedAuctionConvertor;

	private UserService userService;

	@Resource(name = "jslUserOTPFacade")
	@Autowired
	private JslUserOTPFacade userOtpFacade;

	@Override
	public List<AuctionEventModel> getActiveAuctionList()
	{
		return getJslAuctionService().getActiveAuctionList();
	}

	@Override
	public List<BundleTemplateModel> getAuctionList()
	{
		List<BundleTemplateModel> bundleTemplateList = null;
		bundleTemplateList = getBundleTemplateService().getAuctionList();

		return bundleTemplateList;
	}

	@Override
	public List<AuctionEventData> getLiveAuctionList()
	{
		final List<AuctionEventData> auctionEventDataList = new ArrayList<>();
		final List<AuctionEventModel> auctionEventModelList = getJslAuctionService().getLiveAuctionList();
		auctionEventModelList.forEach(auctionEventModel -> {
			auctionEventDataList.add(getAuctionConverter().convert(auctionEventModel));
		});

		return auctionEventDataList;
	}

	@Override
	public List<AuctionEventData> getAllAuctionList()
	{
		final List<AuctionEventData> auctionEventDataList = new ArrayList<>();
		final List<AuctionEventModel> auctionEventModelList = getJslAuctionService().getAllAuctionList();
		auctionEventModelList.forEach(auctionEventModel -> {
			if (this.isAuctionPresentForUser(auctionEventModel))
			{
				auctionEventDataList.add(getAuctionConverter().convert(auctionEventModel));
			}
		});

		return auctionEventDataList;
	}

	@Override
	public List<BundleTemplateData> getOngoingAuctionDetails(final String auctionId)
	{
		final List<BundleTemplateData> bundleTemplateDataList = new ArrayList<>();
		List<BundleTemplateModel> bundleTemplateList = null;

		bundleTemplateList = getBundleTemplateService().getOngoingAuctionDetails(auctionId);

		bundleTemplateList.forEach(bundleTemplate -> {
			bundleTemplateDataList.add(getBundleTemplateConverter().convert(bundleTemplate));
		});

		try
		{
			getLastBidAndProxyBid(auctionId, bundleTemplateDataList);
		}
		catch (final ModelNotFoundException e)
		{
			LOG.error("JslAuctionFacadeImpl.class getOngoingAuctionDetails() Bid and/or ProxyBid not found", e);
		}
		LOG.debug(
				"JslAuctionFacadeImpl.class getOngoingAuctionDetails() bundleTemplateDataList size: " + bundleTemplateList.size());
		try
		{
			bundleTemplateDataList
					.sort((final BundleTemplateData b1, final BundleTemplateData b2) -> b1.getBundleId().compareTo(b2.getBundleId()));
		}
		catch (final Exception e)
		{
			LOG.error("Exception while sorting list JslAuctionFacadeImpl.class getOngoingAuctionDetails()", e);
		}

		return bundleTemplateDataList;
	}

	@Override
	public Map<String, Object> getCompletedAuctionDetails(final Map<String, Object> requestObject,
			final Map<String, Object> responseObject)
	{
		List<JslCompletedAuctionDto> completedBundleList = null;
		final boolean isAdminUser = (boolean) requestObject.get("isAdminUser");
		if (isAdminUser)
		{
			getCompletedAuctionForAdmin(requestObject, responseObject);
		}
		else
		{
			getCompletedAuctionForCurrentUser(requestObject, responseObject);
		}
		completedBundleList = getCompletedAuctionConvertor().populate(responseObject, requestObject);
		responseObject.put("completedAuctionList", completedBundleList);

		return responseObject;
	}

	@Override
	public List<AuctionEventData> getUpcomingAuctionDetails()
	{
		final List<AuctionEventData> auctionEventDataList = new ArrayList<>();
		final List<AuctionEventModel> auctionEventModelList = getJslAuctionService().getUpcomingAuctionDetails();
		auctionEventModelList.forEach(auctionEventModel -> {
			if (this.isAuctionPresentForUser(auctionEventModel))
			{
				auctionEventDataList.add(getAuctionConverter().convert(auctionEventModel));
			}
		});

		return auctionEventDataList;
	}

	@Override
	public List<DefaultBundleCommonDto> getMaxBidForCurrentUser(final String auctionEventId)
	{
		final AuctionEventModel auctionEvent = getAuctionEventForAuctionId(auctionEventId);
		return getJslBidEventService().getMaxBidForCurrentUser(auctionEvent);
	}

	@Override
	public List<DefaultBundleCommonDto> getProxyBidForCurrentUser(final String auctionEventId)
	{
		final AuctionEventModel auctionEvent = getAuctionEventForAuctionId(auctionEventId);
		return getJslProxyBidRuleService().getProxyBidForCurrentUser(auctionEvent);
	}

	@Override
	public DefaultAuctionCommonDto getAllMaxBidPrice(final String auctionEventId)
	{
		final DefaultAuctionCommonDto defaultAuctionCommonDto = new DefaultAuctionCommonDto();
		final AuctionEventModel auctionEvent = getAuctionEventForAuctionId(auctionEventId);
		defaultAuctionCommonDto.setAuctionEventId(String.valueOf(auctionEvent.getAuctionId()));
		defaultAuctionCommonDto.setAuctionStatus(auctionEvent.getAuctionStatus().getCode());
		if (auctionEvent.getAdditionalTime() != null)
		{
			defaultAuctionCommonDto.setAdditionalTime(String.valueOf(auctionEvent.getAdditionalTime()));
		}
		else
		{
			defaultAuctionCommonDto.setAdditionalTime("0");
		}
		final List<DefaultBundleCommonDto> defaultBundleCommonDto = getJslBidEventService().getAllMaxBidPrice(auctionEvent);
		defaultAuctionCommonDto.setDefaultBundleCommonDto(defaultBundleCommonDto);
		defaultAuctionCommonDto.setCurrentTime(System.currentTimeMillis());

		return defaultAuctionCommonDto;
	}

	@Override
	public Map<String, Object> placeBid(final DefaultAuctionCommonDto bidEvent, final Map<String, Object> placeBidResponse)
	{
		boolean isBidPlaced = false;
		try
		{
			for (final DefaultBundleCommonDto defaultBundleCommonDto : bidEvent.getDefaultBundleCommonDto())
			{
				final BundleTemplateModel bundleTemplate = getBundleTemplateForBundleId(defaultBundleCommonDto.getBundleTemplateId(),
						defaultBundleCommonDto.getAuctionEventId());
				isBidPlaced = getJslBidEventService().placeBid(bundleTemplate, defaultBundleCommonDto.getBidPrice());
			}
			placeBidResponse.put("responseCode", JslAuctionEnum.SUCCESS.getResponseCode());
		}
		catch (final JslAuctionException e)
		{
			placeBidResponse.put("responseCode", e.getCode());
		}

		return placeBidResponse;
	}

	@Override
	public Map<String, Object> setProxy(final List<DefaultBundleCommonDto> proxyBidPriceList,
			final Map<String, Object> setProxyResponse)
	{
		boolean isProxySet = false;
		try
		{
			for (final DefaultBundleCommonDto proxyBidPrice : proxyBidPriceList)
			{
				final BundleTemplateModel bundleTemplate = getBundleTemplateForBundleId(proxyBidPrice.getBundleTemplateId(),
						proxyBidPrice.getAuctionEventId());
				isProxySet = getJslProxyBidRuleService().setProxy(bundleTemplate, proxyBidPrice.getBidPrice());
			}
			setProxyResponse.put("responseCode", JslAuctionEnum.SUCCESS.getResponseCode());
		}
		catch (final JslAuctionException e)
		{
			setProxyResponse.put("responseCode", e.getCode());
		}
		return setProxyResponse;
	}


	@Override
	public List<WatchListData> getWatchListDetails(final String auctionID)
	{
		final List<WatchListData> watchListDataList = new ArrayList<>();
		List<WatchListModel> watchList = null;

		watchList = getBundleTemplateService().getWatchListDetails(auctionID);

		watchList.forEach(watchListEventModel -> {
			watchListDataList.add(getWatchListConverter().convert(watchListEventModel));
		});

		try
		{
			getLastbidAndProxyBid(auctionID, watchListDataList);
		}
		catch (final ModelNotFoundException e)
		{
			LOG.error("JslAuctionFacadeImpl.class getWatchListDetails() Bid and/or ProxyBid not found", e);
		}

		try
		{
			watchListDataList.sort((final WatchListData w1, final WatchListData w2) -> w1.getBundleTemplateData().getBundleId()
					.compareTo(w2.getBundleTemplateData().getBundleId()));
		}
		catch (final Exception e)
		{
			LOG.error("Exception while sorting list JslAuctionFacadeImpl.class getWatchListDetails()", e);
		}

		return watchListDataList;
	}

	@Override
	public int addToWatchList(final List<DefaultBundleCommonDto> addToWatchList)
	{
		int isAdded = 0;
		for (final DefaultBundleCommonDto watchList : addToWatchList)
		{
			final BundleTemplateModel bundleTemplate = getBundleTemplateForBundleId(watchList.getBundleTemplateId(),
					watchList.getAuctionEventId());
			isAdded = getBundleTemplateService().addToWatchList(bundleTemplate, watchList.getLotIndex());
		}
		return isAdded;
	}

	@Override
	public boolean removeFromWatchList(final List<DefaultBundleCommonDto> toRemovefromWatchList)
	{
		boolean isRemoved = false;
		for (final DefaultBundleCommonDto watchList : toRemovefromWatchList)
		{
			final BundleTemplateModel bundleTemplate = getBundleTemplateForBundleId(watchList.getBundleTemplateId(),
					watchList.getAuctionEventId());
			getBundleTemplateService().removeFromWatchList(bundleTemplate);
			isRemoved = true;
		}

		return isRemoved;
	}

	@Override
	public List<BundleTemplateModel> getCompletedAuctionDetailsBasedOnDateRange(final DefaultBundleCommonDto auctionDateRange)
	{
		return getBundleTemplateService().getCompletedAuctionDetailsByDateRange(auctionDateRange.getAuctionStartDate(),
				auctionDateRange.getAuctionEndDate());
	}

	@Override
	public List<ProductModel> getProductListByBundleId(final String bundleId)
	{
		return getBundleTemplateService().getProductListByBundleId(bundleId);
	}

	@Override
	public List<BundleTemplateData> getBundleListByAuctionId(final String auctionId)
	{
		final List<BundleTemplateData> bundleTemplateDataList = new ArrayList<>();

		List<BundleTemplateModel> bundleTemplateList = null;
		bundleTemplateList = getBundleTemplateService().getBundleTemplatesForAuctionId(auctionId);

		bundleTemplateList.forEach(bundleTemplateModel -> {
			bundleTemplateDataList.add(getBundleTemplateConverter().convert(bundleTemplateModel));
		});

		return bundleTemplateDataList;
	}

	@Override
	public List<AuctionEventData> checkOtpStatus(final String identityValue, final List<AuctionEventData> auctionEventList)
	{
		boolean otpAccepted = false;
		final List<AuctionEventData> otpAcceptedAuctionList = new ArrayList<>();
		for (final AuctionEventData auctionEvent : auctionEventList)
		{
			final JslOTPValidationDto jslOTPValidationDto = new JslOTPValidationDto();
			jslOTPValidationDto.setAuctionId(String.valueOf(auctionEvent.getAuctionId()));
			final JSLUserOTPGenerationModel jslUserOTPGenerationModel = getUserOtpFacade()
					.getUserAuctionOTPGeneratedRecordByOTPStatus(OTPUserIdentificationType.EMAIL_ID, identityValue,
							OTPGeneratedForEnum.AUCTION, jslOTPValidationDto, OTPStatus.ACCEPTED.getCode());
			if (null != jslUserOTPGenerationModel)
			{
				otpAccepted = true;
				otpAcceptedAuctionList.add(auctionEvent);
			}
		}

		return otpAcceptedAuctionList;
	}

	@Override
	public long updateAuctionEventWithAdditionalTime(final AuctionEventModel auctionEvent)
	{
		return getJslAuctionService().updateAuctionEventWithAdditionalTime(auctionEvent);
	}

	@Override
	public AuctionEventModel getAuctionEventForAuctionId(final String auctionId)
	{
		return getJslAuctionService().getAuctionEventForAuctionId(auctionId);
	}

	@Override
	public AuctionEventData getAuctionEventDataForAuctionId(final String auctionId)
	{
		return getAuctionConverter().convert(getJslAuctionService().getAuctionEventForAuctionId(auctionId));
	}

	@Override
	public Map<String, Object> getAllBidHistoryForLot(final DefaultBundleCommonDto lotDetails)
	{
		return getJslBidEventService().getAllBidHistoryForLot(lotDetails);
	}

	@Override
	public boolean isAdminObserverUser(final String currentUserId)
	{
		boolean isAdminObserverUser = false;
		final String ADMIN_OBSERVER_ID = "admin.observer.user.id";
		try
		{
			final String adminUserId = getSiteConfigService().getProperty(ADMIN_OBSERVER_ID);
			if (StringUtils.hasText(adminUserId) && adminUserId.equalsIgnoreCase(currentUserId))
			{
				isAdminObserverUser = true;
			}
		}
		catch (final Exception e)
		{
			LOG.error("Exception while identifing admin observer user for: " + currentUserId, e);
		}

		return isAdminObserverUser;

	}

	@Override
	public List<AuctionEventModel> getAuctionByDateRange(final DefaultBundleCommonDto auctionDateRange)
	{
		final List<AuctionEventData> auctionEventDataList = new ArrayList<>();
		final List<AuctionEventModel> auctionEventList = getJslAuctionService().getAuctionByDateRange(auctionDateRange);
		auctionEventList.forEach(auctionEventModel -> {
			auctionEventDataList.add(getAuctionConverter().convert(auctionEventModel));
		});

		return auctionEventList;
	}

	@Override
	public List<AuctionEventModel> getAuctionByDate(final DefaultBundleCommonDto auctionDateRange)
	{
		final List<AuctionEventData> auctionEventDataList = new ArrayList<>();
		final List<AuctionEventModel> auctionEventList = getJslAuctionService().getAuctionByDate(auctionDateRange);
		auctionEventList.forEach(auctionEventModel -> {
			auctionEventDataList.add(getAuctionConverter().convert(auctionEventModel));
		});

		return auctionEventList;
	}

	@Override
	public List<BidEventModel> getLastBidEventForBundle(final String bundleId)
	{
		return getJslBidEventService().getLastBidEventForBundle(bundleId);
	}

	@Override
	public List<BidEventModel> getLostAuctionLotsDetailsForCurrentuser(final List<BundleTemplateModel> bundleTemplateList,
			final String flowName)
	{
		return getJslBidEventService().getLostAuctionLotsDetailsForCurrentuser(bundleTemplateList, flowName);
	}

	@Override
	public List<BidEventModel> getLostAuctionLotsDetailsForCurrentUserByDateRange(
			final List<BundleTemplateModel> bundleTemplateList, final DefaultBundleCommonDto auctionDateRange)
	{
		return getJslBidEventService().getLostAuctionLotsDetailsForCurrentUserByDateRange(bundleTemplateList, auctionDateRange);
	}

	@Override
	public List<BundleTemplateData> convertBundleModelToData(final List<BundleTemplateModel> bundleTemplateList)
	{
		final List<BundleTemplateData> bundleTemplateDataList = new ArrayList<>();

		bundleTemplateList.forEach(bundleTemplate -> {
			bundleTemplateDataList.add(getBundleTemplateConverter().convert(bundleTemplate));
		});

		return bundleTemplateDataList;
	}

	@Override
	public List<BundleTemplateModel> getCompletedAuctionDetailsForAdmin(final Map<String, Object> requestObject,
			final Map<String, Object> responseObject)
	{
		return getBundleTemplateService().getCompletedAuctionDetailsForAdmin();
	}

	@Override
	public List<BundleTemplateModel> getCompletedAuctionDetailsForAdminByDateRange(final Map<String, Object> requestObject,
			final Map<String, Object> responseObject)
	{
		final DefaultBundleCommonDto auctionDateRange = (DefaultBundleCommonDto) requestObject.get("auctionDateRange");

		return getBundleTemplateService().getCompletedAuctionDetailsForAdminByDateRange(auctionDateRange.getAuctionStartDate(),
				auctionDateRange.getAuctionEndDate());
	}

	private void getCompletedAuctionForCurrentUser(final Map<String, Object> requestObject,
			final Map<String, Object> responseObject)
	{
		final List<BundleTemplateModel> wonBundleList = new ArrayList<BundleTemplateModel>();
		List<BundleTemplateModel> wonBundleListTemp = null;
		Map<AuctionEventModel, List<BundleTemplateModel>> wonBundleMap = null;

		final List<BidEventModel> lostBidEventList = new ArrayList<BidEventModel>();
		List<BidEventModel> lostBidEventListTemp = null;
		Map<AuctionEventModel, List<BidEventModel>> lostBundleMap = null;

		final List<BundleTemplateModel> bundleWithoutBidList = new ArrayList<>();
		final List<BidEventModel> bundleWithoutBidByCurrentUserList = new ArrayList<BidEventModel>();
		Map<AuctionEventModel, List<BidEventModel>> bundleWithoutBidByCurrentUserMap = null;
		final String flowName = "completedPage";
		final DefaultBundleCommonDto auctionDateRange = (DefaultBundleCommonDto) requestObject.get("auctionDateRange");
		if (auctionDateRange.isDateRange() && StringUtils.hasText(auctionDateRange.getAuctionStartDate()))
		{
			wonBundleListTemp = getCompletedAuctionDetailsBasedOnDateRange(auctionDateRange);
			lostBidEventListTemp = getLostAuctionLotsDetailsForCurrentUserByDateRange(wonBundleListTemp, auctionDateRange);
			wonBundleMap = wonBundleListTemp.stream().collect(Collectors.groupingBy(BundleTemplateModel::getAuctionEvent));
			lostBundleMap = lostBidEventListTemp.stream().collect(Collectors.groupingBy(BidEventModel::getAuctionEvent));

			wonBundleMap.forEach((k, v) -> {
				if (isAuctionPresentForUser(k))
				{
					wonBundleList.addAll(v);
				}
			});

			lostBundleMap.forEach((k, v) -> {
				if (isAuctionPresentForUser(k))
				{
					lostBidEventList.addAll(v);
				}
			});
			/*
			 * bundleWithoutBidList = getBundleTemplateService().getCompletedAuctionLotWithoutBidByDateRange(
			 * auctionDateRange.getAuctionStartDate(), auctionDateRange.getAuctionEndDate());
			 */
			responseObject.put("dateRange", true);
			responseObject.put("wonBundleList", wonBundleList);
			responseObject.put("lostBidEventList", lostBidEventList);
			responseObject.put("bundleWithoutBidList", bundleWithoutBidList);
			final List<BundleTemplateModel> bidPlacedByUser = getAllBidPlacedByCurrentUser(responseObject);
			bundleWithoutBidByCurrentUserMap = getJslBidEventService()
					.getBundleOnWhichUserDidNotPlaceBidByDateRange(bidPlacedByUser, auctionDateRange).stream()
					.collect(Collectors.groupingBy(BidEventModel::getAuctionEvent));
			bundleWithoutBidByCurrentUserMap.forEach((k, v) -> {
				if (isAuctionPresentForUser(k))
				{
					bundleWithoutBidByCurrentUserList.addAll(v);
				}
			});
			responseObject.put("bundleWithoutBidByCurrentUserList", bundleWithoutBidByCurrentUserList);
		}
		else
		{
			wonBundleListTemp = getBundleTemplateService().getCompletedAuctionDetails(flowName);
			lostBidEventListTemp = getLostAuctionLotsDetailsForCurrentuser(wonBundleList, flowName);
			//bundleWithoutBidList = getBundleTemplateService().getCompletedAuctionLotWithoutBid();

			wonBundleMap = wonBundleListTemp.stream().collect(Collectors.groupingBy(BundleTemplateModel::getAuctionEvent));
			lostBundleMap = lostBidEventListTemp.stream().collect(Collectors.groupingBy(BidEventModel::getAuctionEvent));

			wonBundleMap.forEach((k, v) -> {
				if (isAuctionPresentForUser(k))
				{
					wonBundleList.addAll(v);
				}
			});

			lostBundleMap.forEach((k, v) -> {
				if (isAuctionPresentForUser(k))
				{
					lostBidEventList.addAll(v);
				}
			});

			responseObject.put("dateRange", false);
			responseObject.put("wonBundleList", wonBundleList);
			responseObject.put("lostBidEventList", lostBidEventList);
			responseObject.put("bundleWithoutBidList", bundleWithoutBidList);
			final List<BundleTemplateModel> bidPlacedByUser = getAllBidPlacedByCurrentUser(responseObject);
			bundleWithoutBidByCurrentUserMap = getJslBidEventService().getBundleOnWhichUserDidNotPlaceBid(bidPlacedByUser).stream()
					.collect(Collectors.groupingBy(BidEventModel::getAuctionEvent));
			bundleWithoutBidByCurrentUserMap.forEach((k, v) -> {
				if (isAuctionPresentForUser(k))
				{
					bundleWithoutBidByCurrentUserList.addAll(v);
				}
			});
			responseObject.put("bundleWithoutBidByCurrentUserList", bundleWithoutBidByCurrentUserList);
		}
	}

	private void getCompletedAuctionForAdmin(final Map<String, Object> requestObject, final Map<String, Object> responseObject)
	{
		List<BundleTemplateModel> wonBundleList = null;
		final DefaultBundleCommonDto auctionDateRange = (DefaultBundleCommonDto) requestObject.get("auctionDateRange");
		if (auctionDateRange.isDateRange() && StringUtils.hasText(auctionDateRange.getAuctionStartDate()))
		{
			wonBundleList = getCompletedAuctionDetailsForAdminByDateRange(requestObject, responseObject);
			responseObject.put("dateRange", true);
			responseObject.put("wonBundleList", wonBundleList);
			responseObject.put("lostBidEventList", Collections.emptyList());
		}
		else
		{
			wonBundleList = getCompletedAuctionDetailsForAdmin(requestObject, responseObject);
			responseObject.put("dateRange", false);
			responseObject.put("wonBundleList", wonBundleList);
			responseObject.put("lostBidEventList", Collections.emptyList());
		}
	}

	private boolean checkAuctionAdditionalTime(final AuctionEventModel auctionEvent)
	{
		boolean isValid = false;
		final long currentTimeinMillis = System.currentTimeMillis();
		final Long lastModifiedTimeInMillis = auctionEvent.getUpdatedOn();
		final long startTime = auctionEvent.getStartDate().getTime();
		final long additionalTime = auctionEvent.getAdditionalTime();
		if (null != lastModifiedTimeInMillis)
		{
			final long timeDiff = currentTimeinMillis - lastModifiedTimeInMillis.longValue();
			if (timeDiff >= 1000)
			{
				isValid = true;
			}
		}
		else
		{
			isValid = true;
		}

		return isValid;
	}

	private List<BundleTemplateModel> getBundleTemplateForAuctionId(final String auctionId)
	{
		return getBundleTemplateService().getBundleTemplatesForAuctionId(auctionId);
	}

	private BundleTemplateModel getBundleTemplateForBundleId(final String bundleId, final String auctionId)
	{
		return getBundleTemplateService().getBundleTemplateForBundleId(bundleId, auctionId);
	}

	private void getLastBidAndProxyBid(final String auctionId, final List<BundleTemplateData> bundleTemplateDataList)
	{
		final List<DefaultBundleCommonDto> bidEventList = getMaxBidForCurrentUser(auctionId);
		final List<DefaultBundleCommonDto> proxyBidList = getProxyBidForCurrentUser(auctionId);
		final DefaultAuctionCommonDto h1PriceList = getAllMaxBidPrice(auctionId);

		for (final BundleTemplateData bundleTemplateData : bundleTemplateDataList)
		{
			for (final DefaultBundleCommonDto bidEvent : bidEventList)
			{
				if (bidEvent.getBundleTemplateId().equalsIgnoreCase(bundleTemplateData.getBundleId()))
				{
					bundleTemplateData.setMyBidEvent(bidEvent.getBidPrice());
					bundleTemplateData.setMyLastBid(bidEvent.getBidPrice());
				}
			}
			for (final DefaultBundleCommonDto proxyBid : proxyBidList)
			{
				if (proxyBid.getBundleTemplateId().equalsIgnoreCase(bundleTemplateData.getBundleId()))
				{
					bundleTemplateData.setProxyBidEvent(proxyBid.getBidPrice());
					bundleTemplateData.setMyBidEvent(proxyBid.getBidPrice());
				}
			}
			for (final DefaultBundleCommonDto h1Price : h1PriceList.getDefaultBundleCommonDto())
			{
				if (h1Price.getBundleTemplateId().equalsIgnoreCase(bundleTemplateData.getBundleId()))
				{
					bundleTemplateData.setH1Price(h1Price.getBidPrice());
				}
			}
		}
	}

	private void getLastbidAndProxyBid(final String auctionID, final List<WatchListData> watchListDataList)
	{
		final List<DefaultBundleCommonDto> bidEventList = getMaxBidForCurrentUser(auctionID);
		final List<DefaultBundleCommonDto> proxyBidList = getProxyBidForCurrentUser(auctionID);
		final DefaultAuctionCommonDto h1PriceList = getAllMaxBidPrice(auctionID);

		for (final WatchListData watchListData : watchListDataList)
		{
			for (final DefaultBundleCommonDto bidEvent : bidEventList)
			{
				if (bidEvent.getBundleTemplateId().equalsIgnoreCase(watchListData.getBundleTemplateData().getBundleId()))
				{
					watchListData.getBundleTemplateData().setMyBidEvent(bidEvent.getBidPrice());
					watchListData.getBundleTemplateData().setMyLastBid(bidEvent.getBidPrice());
				}
			}
			for (final DefaultBundleCommonDto proxyBid : proxyBidList)
			{
				if (proxyBid.getBundleTemplateId().equalsIgnoreCase(watchListData.getBundleTemplateData().getBundleId()))
				{
					watchListData.getBundleTemplateData().setProxyBidEvent(proxyBid.getBidPrice());
					watchListData.getBundleTemplateData().setMyBidEvent(proxyBid.getBidPrice());
				}
			}
			for (final DefaultBundleCommonDto h1Price : h1PriceList.getDefaultBundleCommonDto())
			{
				if (h1Price.getBundleTemplateId().equalsIgnoreCase(watchListData.getBundleTemplateData().getBundleId()))
				{
					watchListData.getBundleTemplateData().setH1Price(h1Price.getBidPrice());
				}
			}
		}
	}

	private List<BundleTemplateModel> getAllBidPlacedByCurrentUser(final Map<String, Object> responseObject)
	{
		final List<BundleTemplateModel> wonBundleList = (List<BundleTemplateModel>) responseObject.get("wonBundleList");
		final List<BidEventModel> lostBidEventList = (List<BidEventModel>) responseObject.get("lostBidEventList");
		final List<BundleTemplateModel> lostBundleList = new ArrayList<>();
		for (final BidEventModel bidEvent : lostBidEventList)
		{
			lostBundleList.add(bidEvent.getBundleTemplate());
		}
		return Stream.concat(lostBundleList.stream(), wonBundleList.stream()).collect(Collectors.toList());
	}

	private boolean isAuctionPresentForUser(final AuctionEventModel auctionModel)
	{
		final B2BCustomerModel user = ((B2BCustomerModel) getUserService().getCurrentUser());
		final Collection<PrincipalGroupModel> b2bUnits = user.getAllGroups().stream()
				.filter(event -> (event.getItemtype().equalsIgnoreCase(AuctionUserGroupModel._TYPECODE)))
				.collect(Collectors.toList());
		return b2bUnits.contains(auctionModel.getAuctionGroup());
	}

	public JslBundleTemplateService getBundleTemplateService()
	{
		return bundleTemplateService;
	}

	public void setBundleTemplateService(final JslBundleTemplateService bundleTemplateService)
	{
		this.bundleTemplateService = bundleTemplateService;
	}

	public JslAuctionService getJslAuctionService()
	{
		return jslAuctionService;
	}

	public void setJslAuctionService(final JslAuctionService jslAuctionService)
	{
		this.jslAuctionService = jslAuctionService;
	}

	public JslBidEventService getJslBidEventService()
	{
		return jslBidEventService;
	}

	public void setJslBidEventService(final JslBidEventService jslBidEventService)
	{
		this.jslBidEventService = jslBidEventService;
	}

	public Converter<AuctionEventModel, AuctionEventData> getAuctionConverter()
	{
		return auctionConverter;
	}

	public void setAuctionConverter(final Converter<AuctionEventModel, AuctionEventData> auctionConverter)
	{
		this.auctionConverter = auctionConverter;
	}

	public Converter<BundleTemplateModel, BundleTemplateData> getBundleTemplateConverter()
	{
		return bundleTemplateConverter;
	}

	public void setBundleTemplateConverter(final Converter<BundleTemplateModel, BundleTemplateData> bundleTemplateConverter)
	{
		this.bundleTemplateConverter = bundleTemplateConverter;
	}

	public Converter<BidEventModel, BidEventData> getBidEventConverter()
	{
		return bidEventConverter;
	}

	public void setBidEventConverter(final Converter<BidEventModel, BidEventData> bidEventConverter)
	{
		this.bidEventConverter = bidEventConverter;
	}

	public Converter<WatchListModel, WatchListData> getWatchListConverter()
	{
		return watchListConverter;
	}

	public void setWatchListConverter(final Converter<WatchListModel, WatchListData> watchListConverter)
	{
		this.watchListConverter = watchListConverter;
	}

	public JslUserOTPFacade getUserOtpFacade()
	{
		return userOtpFacade;
	}

	public void setUserOtpFacade(final JslUserOTPFacade userOtpFacade)
	{
		this.userOtpFacade = userOtpFacade;
	}

	public JslProxyBidRuleService getJslProxyBidRuleService()
	{
		return jslProxyBidRuleService;
	}

	public void setJslProxyBidRuleService(final JslProxyBidRuleService jslProxyBidRuleService)
	{
		this.jslProxyBidRuleService = jslProxyBidRuleService;
	}

	public SiteConfigService getSiteConfigService()
	{
		return siteConfigService;
	}

	public void setSiteConfigService(final SiteConfigService siteConfigService)
	{
		this.siteConfigService = siteConfigService;
	}

	public JslCompletedAuctionPopulator getCompletedAuctionConvertor()
	{
		return completedAuctionConvertor;
	}

	public void setCompletedAuctionConvertor(final JslCompletedAuctionPopulator completedAuctionConvertor)
	{
		this.completedAuctionConvertor = completedAuctionConvertor;
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

}
