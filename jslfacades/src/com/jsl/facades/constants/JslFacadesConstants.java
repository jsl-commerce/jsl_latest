/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.jsl.facades.constants;

/**
 * Global class for all JslFacades constants.
 */
public class JslFacadesConstants extends GeneratedJslFacadesConstants
{
	public static final String EXTENSIONNAME = "jslfacades";
	public static final String JSLEMAILFROMDISPLAYNAME = "jsl.mail.from.name";
	public static final String JSLEMAILFROM = "mail.from";
	public static final String JSLAUCTIONEMAILSUBJECT = "jsl.auction.mail.subject";
	public static final String JSLAUCTIONEMAILBODY = "jsl.auction.mail.body";
	public static final String JSLAUCTIONEMAILBODYFOOTER = "jsl.auction.mail.body.footer";

	private JslFacadesConstants()
	{
		//empty
	}
}
