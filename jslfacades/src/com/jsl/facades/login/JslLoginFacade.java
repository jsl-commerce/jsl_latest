/**
 *
 */
package com.jsl.facades.login;

import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;


/**
 * @author sunil.maurya.kumar
 *
 */
public interface JslLoginFacade extends CustomerFacade
{
	boolean isJslUser(final CustomerData customer);

	boolean isJsslUser(final CustomerData customer);

	boolean isJsltUser(final CustomerData customer);

}
