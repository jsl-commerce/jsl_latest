/**
 *
 */
package com.jsl.facades.login.impl;

import de.hybris.platform.commercefacades.customer.impl.DefaultCustomerFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Set;

import org.apache.log4j.Logger;

import com.jsl.facades.login.JslLoginFacade;


/**
 * This class is to define routing rule based on customergroup login.
 *
 */
public class DefaultJslLoginFacade extends DefaultCustomerFacade implements JslLoginFacade
{

	private static final Logger LOG = Logger.getLogger(DefaultJslLoginFacade.class);
	private final static String JSL = "JslGroups";
	private final static String JSSL = "JsslGroups";
	private final static String JSLT = "JsltGroups";

	private UserService userService;

	/**
	 * @return the userService
	 */
	@Override
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	@Override
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.jsl.facades.login.JslLoginFacade#isJslUser(de.hybris.platform.commercefacades.user.data.CustomerData)
	 */
	@Override
	public boolean isJslUser(final CustomerData customer)
	{
		final UserModel user = userService.getUserForUID(customer.getUid());
		final Set<PrincipalGroupModel> groups = user.getAllGroups();



		if (!groups.isEmpty())
		{

			for (final PrincipalGroupModel userGroup : groups)
			{

				if (userGroup.getUid().equalsIgnoreCase(JSL))
				{

					return true;
				}
			}
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.jsl.facades.login.JslLoginFacade#isJsslUser(de.hybris.platform.commercefacades.user.data.CustomerData)
	 */
	@Override
	public boolean isJsslUser(final CustomerData customer)
	{
		final UserModel user = userService.getUserForUID(customer.getUid());
		final Set<PrincipalGroupModel> groups = user.getAllGroups();



		if (!groups.isEmpty())
		{

			for (final PrincipalGroupModel userGroup : groups)
			{

				if (userGroup.getUid().equalsIgnoreCase(JSSL))
				{

					return true;
				}
			}
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.jsl.facades.login.JslLoginFacade#isJsltUser(de.hybris.platform.commercefacades.user.data.CustomerData)
	 */
	@Override
	public boolean isJsltUser(final CustomerData customer)
	{
		final UserModel user = userService.getUserForUID(customer.getUid());
		final Set<PrincipalGroupModel> groups = user.getAllGroups();

		if (!groups.isEmpty())
		{

			for (final PrincipalGroupModel userGroup : groups)
			{

				if (userGroup.getUid().equalsIgnoreCase(JSLT))
				{

					return true;
				}
			}
		}
		return false;
	}





}
