/**
 *
 */
package com.jsl.facades.payment.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.jsl.core.model.PaymentSaleOrderModel;
import com.jsl.facades.payment.dto.SaleOrderDetailDto;

/**
 * @author himanshu.sial
 *
 */
public class JslSaleOrderDetailsPopulator implements Populator<PaymentSaleOrderModel, SaleOrderDetailDto>
{

	/* (non-Javadoc)
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(final PaymentSaleOrderModel source, final SaleOrderDetailDto target) throws ConversionException
	{
		// XXX Auto-generated method stub
		target.setCompanyCode(source.getCompanyCode());
		target.setCustomerNumber(source.getCustomerNumber());
		target.setJslRefAmount(source.getJslRefAmount());
		target.setJslRefNumber(source.getJslRefNumber());
		target.setPaymentDate(source.getPaymentDate());
		target.setPaymentStatus(source.getPaymentStatus());
		target.setPaymentStatusDescription(source.getPaymentStatusDescription());
		target.setPurchaseOrderNumber(source.getPurchaseOrderNumber());
		target.setSaleOrderAmount(source.getSaleOrderAmount());
		target.setSaleOrderDate(source.getSaleOrderDate());
		target.setSaleOrderNumber(source.getSaleOrderNumber());
		target.setSbiRefNumber(source.getSbiRefNumber());
		target.setStatus(source.getStatus());
	}


}
