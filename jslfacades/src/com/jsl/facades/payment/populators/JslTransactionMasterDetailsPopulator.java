/**
 *
 */
package com.jsl.facades.payment.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.sap.sapinvoiceaddon.model.SapB2BDocumentModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import com.jsl.core.model.JslPaymentTransactionModel;
import com.jsl.core.model.PaymentSaleOrderModel;
import com.jsl.core.paymentdetailshistory.service.PaymentDetailsHistoryService;
import com.jsl.facades.payment.dto.SaleOrderDetailDto;
import com.jsl.facades.payment.dto.TransactionMasterDto;
import com.jsl.facades.paymentdetailshistory.dto.PaymentDetailsHistoryDto;


/**
 * @author s.bhambhani
 *
 */
public class JslTransactionMasterDetailsPopulator implements Populator<JslPaymentTransactionModel, TransactionMasterDto>
{

	private PaymentDetailsHistoryService paymentDetailsHistoryService;

	private Converter<SapB2BDocumentModel, PaymentDetailsHistoryDto> paymentDetailsHistoryConverter;

	private Converter<PaymentSaleOrderModel, SaleOrderDetailDto> saleOrderDetailsConverter;

	private static final Logger LOG = Logger.getLogger(JslTransactionMasterDetailsPopulator.class);

	private static final String TRANSACTION_TYPE = "transaction";

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(final JslPaymentTransactionModel source, final TransactionMasterDto target) throws ConversionException
	{

		target.setCompanyCode(source.getCompanyCode());
		target.setCustomerNumber(source.getCustomerNumber());
		target.setInvoiceNumbers(source.getInvoiceNumbers());
		target.setJslRefAmount(source.getJslRefAmount());
		target.setJslRefNumber(source.getJslRefNumber());
		target.setPaymentDate(source.getPaymentDate());
		target.setPaymentStatus(source.getPaymentStatus());
		target.setPaymentStatusDescription(source.getPaymentStatusDescription());
		target.setPurchaseOrderNumber(source.getPurchaseOrderNumber());
		target.setSaleOrderNumber(source.getSaleOrderNumber());
		target.setSbiRefNumber(source.getSbiRefNumber());
		target.setPaymentTypeAdv(source.getPaymentTypeAdv());
		target.setPaymentNote(source.getPaymentNote());
		target.setBankAccNumber(source.getBankAccNumber());
		target.setCustomerName(source.getCustomerName());
		target.setPaymentSource(source.getPaymentSource());
		target.setPendingPaymentDate(source.getPendingPaymentDate());
		target.setPaymentMode(source.getPaymentMode());
		target.setPostedToEcc(source.getPostedToEcc());
		if ("N".equalsIgnoreCase(source.getPaymentTypeAdv()))
		{
			final List<String> invoiceNumbers = Arrays.asList(source.getInvoiceNumbers().split(","));
			try
			{
				target.setInvoiceList(paymentDetailsHistoryConverter
						.convertAll(paymentDetailsHistoryService.getAllInvoicesAdminByInvoiceNumbers(invoiceNumbers)));
			}
			catch (final Exception e)
			{
				// XXX Auto-generated catch block
				LOG.error("Error while converting invoices", e);
				throw new ConversionException("Error while converting invoices", e);
			}
		}
		else if ("Y".equalsIgnoreCase(source.getPaymentTypeAdv()))
		{
			try
			{
				final List<PaymentSaleOrderModel> saleOrders = paymentDetailsHistoryService
						.getAdminSaleOrderDetailsFromNumber(Arrays.asList(source.getSaleOrderNumber()));
				target.setSaleOrder(saleOrders.isEmpty() ? null : saleOrderDetailsConverter.convert(saleOrders.get(0)));
			}
			catch (final Exception e)
			{
				// XXX Auto-generated catch block
				LOG.error("Error while converting saleorder", e);
				throw new ConversionException("Error while converting saleorder", e);
			}
		}
	}

	/**
	 * @return the paymentDetailsHistoryService
	 */
	public PaymentDetailsHistoryService getPaymentDetailsHistoryService()
	{
		return paymentDetailsHistoryService;
	}

	/**
	 * @param paymentDetailsHistoryService
	 *           the paymentDetailsHistoryService to set
	 */
	public void setPaymentDetailsHistoryService(final PaymentDetailsHistoryService paymentDetailsHistoryService)
	{
		this.paymentDetailsHistoryService = paymentDetailsHistoryService;
	}

	/**
	 * @return the paymentDetailsHistoryConverter
	 */
	public Converter<SapB2BDocumentModel, PaymentDetailsHistoryDto> getPaymentDetailsHistoryConverter()
	{
		return paymentDetailsHistoryConverter;
	}

	/**
	 * @param paymentDetailsHistoryConverter
	 *           the paymentDetailsHistoryConverter to set
	 */
	public void setPaymentDetailsHistoryConverter(
			final Converter<SapB2BDocumentModel, PaymentDetailsHistoryDto> paymentDetailsHistoryConverter)
	{
		this.paymentDetailsHistoryConverter = paymentDetailsHistoryConverter;
	}

	/**
	 * @return the saleOrderDetailsConverter
	 */
	public Converter<PaymentSaleOrderModel, SaleOrderDetailDto> getSaleOrderDetailsConverter()
	{
		return saleOrderDetailsConverter;
	}

	/**
	 * @param saleOrderDetailsConverter
	 *           the saleOrderDetailsConverter to set
	 */
	public void setSaleOrderDetailsConverter(final Converter<PaymentSaleOrderModel, SaleOrderDetailDto> saleOrderDetailsConverter)
	{
		this.saleOrderDetailsConverter = saleOrderDetailsConverter;
	}

}
