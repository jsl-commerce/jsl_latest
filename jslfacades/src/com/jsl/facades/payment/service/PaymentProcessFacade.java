/**
 *
 */
package com.jsl.facades.payment.service;

import com.jsl.core.payment.dto.JslPaymentRequestDto;
import com.jsl.core.payment.dto.JslPaymentResponse;
import com.jsl.facades.payment.dto.TransactionMasterDto;
import com.jsl.facades.paymentdetailshistory.dto.PaymentDetailsDto;


/**
 * @author himanshu.sial
 *
 */
public interface PaymentProcessFacade
{

	public JslPaymentRequestDto processPayment(PaymentDetailsDto paymentDetailsDto) throws Exception;

	public JslPaymentRequestDto processPendingPayment(TransactionMasterDto transactionMasterDto) throws Exception;

	public void processPaymentReponse(JslPaymentResponse jslPaymentResponse) throws Exception;

}
