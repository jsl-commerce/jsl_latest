/**
 *
 */
package com.jsl.facades.payment.service.impl;

import org.apache.log4j.Logger;

import com.jsl.core.payment.dto.JslPaymentRequestDto;
import com.jsl.core.payment.dto.JslPaymentResponse;
import com.jsl.core.payment.service.JslPaymentService;
import com.jsl.facades.payment.dto.TransactionMasterDto;
import com.jsl.facades.payment.service.PaymentProcessFacade;
import com.jsl.facades.paymentdetailshistory.dto.PaymentDetailsDto;


/**
 * @author himanshu.sial
 *
 */
public class PaymentProcessFacadeImpl implements PaymentProcessFacade
{

	private static final Logger LOG = Logger.getLogger(PaymentProcessFacadeImpl.class);

	private JslPaymentService jslPaymentService;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.jsl.facades.payment.service.PaymentProcessFacade#processPendingPayment(com.jsl.facades.payment.dto.
	 * TransactionMasterDto)
	 */
	@Override
	public JslPaymentRequestDto processPendingPayment(final TransactionMasterDto transactionMasterDto) throws Exception
	{
		// XXX Auto-generated method stub
		return jslPaymentService.processPendingPayment(transactionMasterDto);
	}

	@Override
	public JslPaymentRequestDto processPayment(final PaymentDetailsDto paymentDetailsDto) throws Exception
	{
		return jslPaymentService.processPayment(paymentDetailsDto);
	}

	public void processPaymentReponse(final JslPaymentResponse jslPaymentResponse) throws Exception
	{
		jslPaymentService.processPaymentReponse(jslPaymentResponse);
	}

	/**
	 * @return the jslPaymentService
	 */
	public JslPaymentService getJslPaymentService()
	{
		return jslPaymentService;
	}

	/**
	 * @param jslPaymentService
	 *           the jslPaymentService to set
	 */
	public void setJslPaymentService(final JslPaymentService jslPaymentService)
	{
		this.jslPaymentService = jslPaymentService;
	}

}
