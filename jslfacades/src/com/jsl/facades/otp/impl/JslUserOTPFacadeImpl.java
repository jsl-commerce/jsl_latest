/**
 *
 */
package com.jsl.facades.otp.impl;

import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.jsl.core.dto.JslOTPGenerationDto;
import com.jsl.core.dto.JslOTPValidationDto;
import com.jsl.core.enums.OTPGeneratedForEnum;
import com.jsl.core.enums.OTPUserIdentificationType;
import com.jsl.core.model.JSLUserOTPGenerationModel;
import com.jsl.core.services.JslUserOTPService;
import com.jsl.facades.otp.JslUserOTPFacade;


/**
 * @author suyash.trivedi
 *
 */
public class JslUserOTPFacadeImpl implements JslUserOTPFacade
{
	private final static Logger LOG = Logger.getLogger(JslUserOTPFacadeImpl.class);

	private JslUserOTPService jslUserOTPService;
	private EventService eventService;
	@Resource
	private BaseStoreService baseStoreService;
	@Resource
	private BaseSiteService baseSiteService;
	@Resource
	private UserService userService;
	@Resource
	private CommonI18NService commonI18NService;


	/**
	 * @return the jslUserOTPService
	 */
	public JslUserOTPService getJslUserOTPService()
	{
		return jslUserOTPService;
	}

	/**
	 * @return the eventService
	 */
	public EventService getEventService()
	{
		return eventService;
	}

	/**
	 * @param eventService
	 *           the eventService to set
	 */
	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}

	/**
	 * @param jslUserOTPService
	 *           the jslUserOTPService to set
	 */
	public void setJslUserOTPService(final JslUserOTPService jslUserOTPService)
	{
		this.jslUserOTPService = jslUserOTPService;
	}

	public void generateOTP(final OTPGeneratedForEnum generatedModule, final OTPUserIdentificationType identityType,
			final String identityValue, final JslOTPGenerationDto jslOTPGenerationDto)
	{
		getJslUserOTPService().generateOTP(generatedModule, identityType, identityValue, jslOTPGenerationDto);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, String> validateOTP(final String otp, final OTPUserIdentificationType identityType,
			final String identityValue, final OTPGeneratedForEnum generatedModule, final long enteredOTPTime,
			final JslOTPValidationDto jslOTPValidationDto)
	{
		return getJslUserOTPService().validateOTP(otp, identityType, identityValue, generatedModule, enteredOTPTime,
				jslOTPValidationDto);
	}

	@Override
	public JSLUserOTPGenerationModel getUserAuctionOTPGeneratedRecord(final OTPUserIdentificationType identityType,
			final String identityValue, final OTPGeneratedForEnum generatedModule, final JslOTPValidationDto jslOTPValidationDto)
	{

		return getJslUserOTPService().getUserAuctionOTPGeneratedRecord(identityType, identityValue, generatedModule,
				jslOTPValidationDto);
	}

	@Override
	public JSLUserOTPGenerationModel getUserAuctionOTPGeneratedRecordByOTPStatus(final OTPUserIdentificationType identityType,
			final String identityValue, final OTPGeneratedForEnum generatedModule, final JslOTPValidationDto jslOTPValidationDto,
			final String status)
	{

		return getJslUserOTPService().getUserAuctionOTPGeneratedRecordByOTPStatus(identityType, identityValue, generatedModule,
				jslOTPValidationDto, status);
	}

}
