/**
 *
 */
package com.jsl.facades.customer.impl;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toCollection;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commercefacades.customer.impl.DefaultCustomerFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.jsl.core.services.JslAuctionService;
import com.jsl.core.services.JslAuctionUserGroupService;
import com.jsl.core.services.JslUserService;
import com.jsl.core.util.JslB2bUnitUtil;
import com.jsl.facades.customer.JslCustomerFacade;


/**
 * @author manav.magoo
 *
 */
public class JslCustomerFacadeImpl extends DefaultCustomerFacade implements JslCustomerFacade
{

	private static final Logger LOG = Logger.getLogger(JslCustomerFacadeImpl.class);

	private JslUserService jslUserService;
	private static final String ADMIN_OBSERVER_ID = "admin.observer.user.id";

	private JslAuctionService jslAuctionService;
	private JslAuctionUserGroupService jslAuctionUserGroupService;

	@Override
	public void setloginActive(final boolean active)
	{
		final UserModel currentUser = getCurrentUser();

		getJslUserService().setLastLogin(currentUser.getUid());

		setLogin(currentUser.getUid(), active);

	}


	/**
	 * @param currentUser
	 */
	@Override
	public void setLogin(final String userId, final boolean active)
	{
		getJslUserService().setLogin(userId, active);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.jsl.facades.customer.JslCustomerFacade#getCurrentlyLoginCustomers()
	 */
	@Override
	public List<CustomerData> getLiveCustomers()
	{
		final String adminUserId = Config.getParameter(ADMIN_OBSERVER_ID);
		final List<CustomerData> customers = new ArrayList<>();

		getJslUserService().getLiveUsers().stream().forEach(user -> {

			if (user instanceof B2BCustomerModel && !user.getUid().equalsIgnoreCase(adminUserId))
			{
				final CustomerData customer = getCustomerConverter().convert(user);
				final B2BUnitModel parentb2bUnit = JslB2bUnitUtil.getParentB2bUnit(user);
				customer.setName(parentb2bUnit != null ? parentb2bUnit.getDisplayName() : "");
				customer.setLastLogin(user.getLastLogin());
				customers.add(customer);
			}
		});

		return customers;
	}


	@Override
	public List<CustomerData> getAuctionLiveCustomers()
	{
		final List<CustomerData> customers = new ArrayList<>();
		final String adminUserId = Config.getParameter(ADMIN_OBSERVER_ID);

		getJslAuctionService().getLiveAuctionList().stream().forEach(auction -> {

			if (auction.getStartDate().before(Calendar.getInstance().getTime()) && auction.getEndDate().after(Calendar.getInstance().getTime()))
			{
				final Collection<CustomerModel> eligibleAuctionCustomers = getJslAuctionUserGroupService()
						.getCustomers(auction.getAuctionGroup().getUid());

				getJslUserService().getLiveUsers().stream().forEach(user -> {
					if (user instanceof B2BCustomerModel && !user.getUid().equalsIgnoreCase(adminUserId)
							&& eligibleAuctionCustomers.contains(user))
					{
						final B2BUnitModel parentb2bUnit = JslB2bUnitUtil.getParentB2bUnit(user);
						final CustomerData customer = getCustomerConverter().convert(user);
						customer.setName(parentb2bUnit != null ? parentb2bUnit.getDisplayName() : "");
						customer.setLastLogin(user.getLastLogin());
						customer.setAuctionId(auction.getAuctionId().toString());
						customer.setAuctionUserGroup(auction.getAuctionGroup().getUid());
						customers.add(customer);
					}
				});

			}
		});
		return customers;
	}

	private List<CustomerData> removeDuplicateCustomer(final List<CustomerData> customers)
	{
		List<CustomerData> customerList = Collections.EMPTY_LIST;
		try
		{
			customerList = customers.stream().collect(
					collectingAndThen(toCollection(() -> new TreeSet<>(comparing(CustomerData::getCustomerId))), ArrayList::new));
		}
		catch (final Exception e)
		{
			LOG.error("Exception in JslCustomerFacadeImpl.removeDuplicateCustomer(): ", e);
			return customers;
		}
		return customerList;
	}

	/**
	 * @return the jslUserService
	 */
	public JslUserService getJslUserService()
	{
		return jslUserService;
	}

	/**
	 * @param jslUserService
	 *           the jslUserService to set
	 */
	@Required
	public void setJslUserService(final JslUserService jslUserService)
	{
		this.jslUserService = jslUserService;
	}


	/**
	 * @return the jslAuctionService
	 */
	public JslAuctionService getJslAuctionService()
	{
		return jslAuctionService;
	}


	/**
	 * @param jslAuctionService
	 *           the jslAuctionService to set
	 */
	public void setJslAuctionService(final JslAuctionService jslAuctionService)
	{
		this.jslAuctionService = jslAuctionService;
	}


	/**
	 * @return the jslAuctionUserGroupService
	 */
	public JslAuctionUserGroupService getJslAuctionUserGroupService()
	{
		return jslAuctionUserGroupService;
	}


	/**
	 * @param jslAuctionUserGroupService
	 *           the jslAuctionUserGroupService to set
	 */
	public void setJslAuctionUserGroupService(final JslAuctionUserGroupService jslAuctionUserGroupService)
	{
		this.jslAuctionUserGroupService = jslAuctionUserGroupService;
	}
}
