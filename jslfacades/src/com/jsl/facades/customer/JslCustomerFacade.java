/**
 *
 */
package com.jsl.facades.customer;

import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.List;


/**
 * @author manav.magoo
 *
 */
public interface JslCustomerFacade extends CustomerFacade
{

	/**
	 * @return
	 * @throws ConversionException
	 */
	List<CustomerData> getLiveCustomers();


	List<CustomerData> getAuctionLiveCustomers();


	/**
	 * @param active
	 */
	void setloginActive(boolean active);



	/**
	 * @param userId
	 * @param active
	 */
	void setLogin(String userId, boolean active);
}
