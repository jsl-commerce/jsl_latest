/**
 *
 */
package com.jsl.facades.order.service;

import de.hybris.platform.b2bacceleratorfacades.order.B2BOrderFacade;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BOrderApprovalData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.workflow.enums.WorkflowActionType;

import java.util.List;


/**
 * @author himanshu.sial
 *
 */
public interface JslB2BOrderFacade extends B2BOrderFacade
{

	SearchPageData<B2BOrderApprovalData> getFilteredPagedOrdersForApproval(WorkflowActionType[] actionTypes,
			PageableData pageableData, String startDate, String endDate, String orderStatus, String auctionId);

	public List<String> getAuctionIds();
}
