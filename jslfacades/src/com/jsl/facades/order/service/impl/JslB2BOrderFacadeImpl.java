/**
 *
 */
package com.jsl.facades.order.service.impl;

import de.hybris.platform.b2bacceleratorfacades.order.data.B2BOrderApprovalData;
import de.hybris.platform.b2bacceleratorfacades.order.impl.DefaultB2BOrderFacade;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.workflow.enums.WorkflowActionType;
import de.hybris.platform.workflow.model.WorkflowActionModel;

import java.util.ArrayList;
import java.util.List;

import com.jsl.core.daos.JslPagedB2BWorkflowActionDao;
import com.jsl.facades.order.service.JslB2BOrderFacade;


/**
 * @author himanshu.sial
 *
 */
public class JslB2BOrderFacadeImpl extends DefaultB2BOrderFacade implements JslB2BOrderFacade
{

	private JslPagedB2BWorkflowActionDao jslPagedB2BWorkflowActionDao;
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.jsl.facades.order.service.JslB2BOrderFacade#getFilteredPagedOrdersForApproval(de.hybris.platform.workflow.
	 * enums.WorkflowActionType[], de.hybris.platform.commerceservices.search.pagedata.PageableData, java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public SearchPageData<B2BOrderApprovalData> getFilteredPagedOrdersForApproval(final WorkflowActionType[] actionTypes,
			final PageableData pageableData, final String startDate, final String endDate, final String orderStatus,
			final String auctionId)
	{
		final SearchPageData<WorkflowActionModel> actions = getJslPagedB2BWorkflowActionDao()
				.findFilteredPagedWorkflowActionsByUserAndActionTypes(getUserService().getCurrentUser(), actionTypes, pageableData,
						startDate, endDate, orderStatus, auctionId);
		return convertPageData(actions, getB2bOrderApprovalDataConverter());
	}

	@Override
	public List<String> getAuctionIds()
	{
		final List<String> auctionIdList = new ArrayList<String>();


		getJslPagedB2BWorkflowActionDao().getAuctionIds().forEach(a -> {
			auctionIdList.add(String.valueOf(a.getAuctionId()));
		});

		return auctionIdList;
	}

	/**
	 * @return the jslPagedB2BWorkflowActionDao
	 */
	public JslPagedB2BWorkflowActionDao getJslPagedB2BWorkflowActionDao()
	{
		return jslPagedB2BWorkflowActionDao;
	}

	/**
	 * @param jslPagedB2BWorkflowActionDao
	 *           the jslPagedB2BWorkflowActionDao to set
	 */
	public void setJslPagedB2BWorkflowActionDao(final JslPagedB2BWorkflowActionDao jslPagedB2BWorkflowActionDao)
	{
		this.jslPagedB2BWorkflowActionDao = jslPagedB2BWorkflowActionDao;
	}

}
