/**
 *
 */
package com.jsl.facade.emails;

import de.hybris.platform.acceleratorservices.email.impl.DefaultEmailGenerationService;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.model.email.EmailAttachmentModel;
import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.jsl.core.model.BidDetailsExportEmailProcessModel;


/**
 * @author subhrajitk766
 *
 */
public class JslAttachmentEmailGenerationService extends DefaultEmailGenerationService
{

	@Autowired
	private ModelService modelService;

	@Override
	public EmailMessageModel generate(final BusinessProcessModel businessProcessModel, final EmailPageModel emailPageModel)
			throws RuntimeException
	{

		// call super.generate() to fill other fields
		final EmailMessageModel emailMessage = super.generate(businessProcessModel, emailPageModel);

		// retrieve attachments from business process and add them to the email message

		if (businessProcessModel instanceof BidDetailsExportEmailProcessModel)
		{
			final BidDetailsExportEmailProcessModel bidprocessModel = (BidDetailsExportEmailProcessModel) businessProcessModel;
			if (bidprocessModel.getEmailAttachment() != null)
			{
				final EmailAttachmentModel attachment = bidprocessModel.getEmailAttachment();
				final List<EmailAttachmentModel> attachmentList = new ArrayList<EmailAttachmentModel>();
				attachmentList.add(attachment);

				emailMessage.setAttachments(attachmentList);
				modelService.saveAll(emailMessage);
			}
		}

		return emailMessage;
	}
}

